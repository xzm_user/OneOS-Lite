/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with 
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on 
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the 
 * specific language governing permissions and limitations under the License.
 *
 * @file        interrupt_gcc.S
 *
 * @brief       This file provides context interrupt functions related to the RISC-V architecture.
 *
 * @revision
 * Date         Author          Notes
 * 2021-12-18   OneOS Team      First version.
 ***********************************************************************************************************************
 */
#include "oneos_config.h"
#include "riscv_bits.h"

.equ    ARCH_MSUBM_PTYP,    (0x3 << 8)
.equ    OS_TASK_STATE_RUNNING,   0x0004              /* Task Status Flag (RUNNING) */

.extern g_os_current_task
.extern g_os_next_task
.extern os_task_switch_interrupt_flag
.extern os_task_switch_notify
.extern os_arch_fault_exception

/* save necessary registers */
.macro IRQ_SAVE_CONTEXT
#ifdef ARCH_RISCV_FPU
    addi  sp, sp, -64 * REGBYTES
#else
    addi  sp, sp, -32 * REGBYTES
#endif
    STORE x1,   2 * REGBYTES(sp)
    STORE x4,   4 * REGBYTES(sp)
    STORE x5,   5 * REGBYTES(sp)
    STORE x6,   6 * REGBYTES(sp)
    STORE x7,   7 * REGBYTES(sp)
    STORE x8,   8 * REGBYTES(sp)
    STORE x9,   9 * REGBYTES(sp)
    STORE x10, 10 * REGBYTES(sp)
    STORE x11, 11 * REGBYTES(sp)
    STORE x12, 12 * REGBYTES(sp)
    STORE x13, 13 * REGBYTES(sp)
    STORE x14, 14 * REGBYTES(sp)
    STORE x15, 15 * REGBYTES(sp)
    STORE x16, 16 * REGBYTES(sp)
    STORE x17, 17 * REGBYTES(sp)
    STORE x18, 18 * REGBYTES(sp)
    STORE x19, 19 * REGBYTES(sp)
    STORE x20, 20 * REGBYTES(sp)
    STORE x21, 21 * REGBYTES(sp)
    STORE x22, 22 * REGBYTES(sp)
    STORE x23, 23 * REGBYTES(sp)
    STORE x24, 24 * REGBYTES(sp)
    STORE x25, 25 * REGBYTES(sp)
    STORE x26, 26 * REGBYTES(sp)
    STORE x27, 27 * REGBYTES(sp)
    STORE x28, 28 * REGBYTES(sp)
    STORE x29, 29 * REGBYTES(sp)
    STORE x30, 30 * REGBYTES(sp)
    STORE x31, 31 * REGBYTES(sp)

#ifdef ARCH_RISCV_FPU
/* save FPU necessary registers */
    csrr t0, mstatus
    li   t1, (0x3 << 13)
    and  t2, t0, t1
    bne  t2, t1, irq_skip_fpu_save_caller       /* if mstatus_fs is indicating the "Dirty", save the FPU registers. */

    FSTORE f0,  32 * REGBYTES(sp)
    FSTORE f1,  33 * REGBYTES(sp)
    FSTORE f2,  34 * REGBYTES(sp)
    FSTORE f3,  35 * REGBYTES(sp)
    FSTORE f4,  36 * REGBYTES(sp)
    FSTORE f5,  37 * REGBYTES(sp)
    FSTORE f6,  38 * REGBYTES(sp)
    FSTORE f7,  39 * REGBYTES(sp)
    FSTORE f8,  40 * REGBYTES(sp)
    FSTORE f9,  41 * REGBYTES(sp)
    FSTORE f10, 42 * REGBYTES(sp)
    FSTORE f11, 43 * REGBYTES(sp)
    FSTORE f12, 44 * REGBYTES(sp)
    FSTORE f13, 45 * REGBYTES(sp)
    FSTORE f14, 46 * REGBYTES(sp)
    FSTORE f15, 47 * REGBYTES(sp)
    FSTORE f16, 48 * REGBYTES(sp)
    FSTORE f17, 49 * REGBYTES(sp)
    FSTORE f18, 50 * REGBYTES(sp)
    FSTORE f19, 51 * REGBYTES(sp)
    FSTORE f20, 52 * REGBYTES(sp)
    FSTORE f21, 53 * REGBYTES(sp)
    FSTORE f22, 54 * REGBYTES(sp)
    FSTORE f23, 55 * REGBYTES(sp)
    FSTORE f24, 56 * REGBYTES(sp)
    FSTORE f25, 57 * REGBYTES(sp)
    FSTORE f26, 58 * REGBYTES(sp)
    FSTORE f27, 59 * REGBYTES(sp)
    FSTORE f28, 60 * REGBYTES(sp)
    FSTORE f29, 61 * REGBYTES(sp)
    FSTORE f30, 62 * REGBYTES(sp)
    FSTORE f31, 63 * REGBYTES(sp)

irq_skip_fpu_save_caller:
#endif
.endm


/* restore necessary registers */
.macro IRQ_RESTORE_CONTEXT
#ifdef ARCH_RISCV_FPU
/* restore FPU necessary registers */
    csrr t2, mstatus
    li   t0, (0x3 << 13)
    and  t1, t2, t0
    bne  t1, t0, irq_skip_fpu_restore_caller    /* if mstatus_fs is indicating the "Dirty", restore the FPU registers. */

    FLOAD f0,  32 * REGBYTES(sp)
    FLOAD f1,  33 * REGBYTES(sp)
    FLOAD f2,  34 * REGBYTES(sp)
    FLOAD f3,  35 * REGBYTES(sp)
    FLOAD f4,  36 * REGBYTES(sp)
    FLOAD f5,  37 * REGBYTES(sp)
    FLOAD f6,  38 * REGBYTES(sp)
    FLOAD f7,  39 * REGBYTES(sp)
    FLOAD f8,  40 * REGBYTES(sp)
    FLOAD f9,  41 * REGBYTES(sp)
    FLOAD f10, 42 * REGBYTES(sp)
    FLOAD f11, 43 * REGBYTES(sp)
    FLOAD f12, 44 * REGBYTES(sp)
    FLOAD f13, 45 * REGBYTES(sp)
    FLOAD f14, 46 * REGBYTES(sp)
    FLOAD f15, 47 * REGBYTES(sp)
    FLOAD f16, 48 * REGBYTES(sp)
    FLOAD f17, 49 * REGBYTES(sp)
    FLOAD f18, 50 * REGBYTES(sp)
    FLOAD f19, 51 * REGBYTES(sp)
    FLOAD f20, 52 * REGBYTES(sp)
    FLOAD f21, 53 * REGBYTES(sp)
    FLOAD f22, 54 * REGBYTES(sp)
    FLOAD f23, 55 * REGBYTES(sp)
    FLOAD f24, 56 * REGBYTES(sp)
    FLOAD f25, 57 * REGBYTES(sp)
    FLOAD f26, 58 * REGBYTES(sp)
    FLOAD f27, 59 * REGBYTES(sp)
    FLOAD f28, 60 * REGBYTES(sp)
    FLOAD f29, 61 * REGBYTES(sp)
    FLOAD f30, 62 * REGBYTES(sp)
    FLOAD f31, 63 * REGBYTES(sp)

irq_skip_fpu_restore_caller:
#endif
    LOAD  x1,   2 * REGBYTES(sp)
    LOAD  x4,   4 * REGBYTES(sp)
    LOAD  x5,   5 * REGBYTES(sp)
    LOAD  x6,   6 * REGBYTES(sp)
    LOAD  x7,   7 * REGBYTES(sp)
    LOAD  x8,   8 * REGBYTES(sp)
    LOAD  x9,   9 * REGBYTES(sp)
    LOAD  x10, 10 * REGBYTES(sp)
    LOAD  x11, 11 * REGBYTES(sp)
    LOAD  x12, 12 * REGBYTES(sp)
    LOAD  x13, 13 * REGBYTES(sp)
    LOAD  x14, 14 * REGBYTES(sp)
    LOAD  x15, 15 * REGBYTES(sp)
    LOAD  x16, 16 * REGBYTES(sp)
    LOAD  x17, 17 * REGBYTES(sp)
    LOAD  x18, 18 * REGBYTES(sp)
    LOAD  x19, 19 * REGBYTES(sp)
    LOAD  x20, 20 * REGBYTES(sp)
    LOAD  x21, 21 * REGBYTES(sp)
    LOAD  x22, 22 * REGBYTES(sp)
    LOAD  x23, 23 * REGBYTES(sp)
    LOAD  x24, 24 * REGBYTES(sp)
    LOAD  x25, 25 * REGBYTES(sp)
    LOAD  x26, 26 * REGBYTES(sp)
    LOAD  x27, 27 * REGBYTES(sp)
    LOAD  x28, 28 * REGBYTES(sp)
    LOAD  x29, 29 * REGBYTES(sp)
    LOAD  x30, 30 * REGBYTES(sp)
    LOAD  x31, 31 * REGBYTES(sp)

#ifdef ARCH_RISCV_FPU
    addi  sp, sp, 64 * REGBYTES
#else
    addi  sp, sp, 32 * REGBYTES
#endif
.endm


.global SW_handler
.align 6
SW_handler:
    /* save registers */
    IRQ_SAVE_CONTEXT

    /* get current task struct pointer*/    
    la    t0, g_os_current_task                 /* t0 = &g_os_current_task */
    LOAD  t2, 0(t0)                             /* t2 = g_os_current_task */
    
    /* save mepc and old task sp */
    csrr  a0, mepc
    STORE a0, 1 * REGBYTES(sp)                 /*save current_task return pc(mepc) into sp*/
    STORE sp, 0 * REGBYTES(t2)                 /* g_os_current_task->stack_top = "old sp" */

    /* clear current task running state */
    li    a0, OS_TASK_STATE_RUNNING
    not   a0, a0                                /* a0 = (~OS_TASK_STATE_RUNNING) */
    lh    t1, 3 * REGBYTES(t2)                  /* t1 = g_os_current_task->state */
    and   t1, t1, a0                            /* t1 &= (~OS_TASK_STATE_RUNNING) */
    sh    t1, 3 * REGBYTES(t2)                  /* g_os_current_task->state = t1 */

    /* Check either task stack during task switching. */
#ifdef OS_TASK_SWITCH_NOTIFY
    jal os_task_switch_notify
#endif

switch_to_task:
    /* switch task: g_os_current_task = g_os_next_task */ 
    la    t1, g_os_next_task                    /* t1 = &g_os_next_task */
    LOAD  t1, 0(t1)                             /* t1 = g_os_next_task */
    la    t0, g_os_current_task                 /* t0 = &g_os_current_task */
    STORE t1, 0(t0)                             /* (*t0) = t1 = g_os_next_task */

    /* Set switch to task running state */
    lh    t0, 3 * REGBYTES(t1)                  /* t0 = g_os_next_task->state */
    ori   t0, t0, OS_TASK_STATE_RUNNING         /* t0 |= (OS_TASK_STATE_RUNNING) */
    sh    t0, 3 * REGBYTES(t1)                  /* g_os_next_task->state = t0 */

    /* restore sp */
    LOAD  sp, 0 * REGBYTES(t1)                 /* sp = g_os_next_task->stack_top */


    /* restore mstatus and mepc */
    LOAD  t0,  0 * REGBYTES(sp)                 /* t0 = CSR_MSTATUS */
    li    t1,  0x00001800                      /* set the privilege as machine mode. */
    or    t0,  t0, t1
    csrs  mstatus, t0

    LOAD  t0,  1 * REGBYTES(sp)
    csrw  mepc, t0
    
restore_context:
    IRQ_RESTORE_CONTEXT

    mret

