/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        arch_exception.c
 *
 * @brief       This file provides exception handler functions related to the RISC-V architecture.
 *
 * @revision
 * Date         Author          Notes
 * 2021-12-18   OneOS Team      First version.
 ***********************************************************************************************************************
 */
#include <os_types.h>
#include <os_util.h>
#include <arch_task.h>
#include <arch_interrupt.h>
#include <os_task.h>

static char *arch_exception_names[] = {
    "[0]Instruction address misaligned",
    "[1]Instruction access fault",
    "[2]Illegal instruction",
    "[3]Breakpoint",
    "[4]Load address misaligned",
    "[5]Load access fault",
    "[6]Store address misaligned",
    "[7]Store access fault",
    "[8]Environment call from U-mode",
    "[9]Environment call from S-mode",
    "[10]Reserved",
    "[11]Environment call from M-mode",
    "[12]Instruction page fault",
    "[13]Load page fault",
    "[14]Reserved",
    "[15]Store page fault",
};

/**
 ***********************************************************************************************************************
 * @brief           This function handles hard fault exception.
 *
 * @param[in]       stack_frame     The start address of the stack frame when the exception occurs.
 * @param[in]       msp             Interrupt stack pointer.
 * @param[in]       psp             Currently running task stack pointer.
 *
 * @return          No return value.
 ***********************************************************************************************************************
 */
void os_arch_fault_exception(int vector, void *stack_frame)
{
#ifdef  STACK_TRACE_EN
        //_arch_exception_stack_show(stack_frame, msp , psp);

        while(1);
#else

    struct os_hw_stack_frame   *stack_common;
    os_ubase_t msubm_ptyp;

    stack_common = (struct os_hw_stack_frame *)stack_frame;

    /* Stack frame with floating point storage */
        os_kprintf("mstatus: 0x%08x\r\n", stack_common->mstatus);
        os_kprintf("mepc   : 0x%08x\r\n", stack_common->epc);
        os_kprintf("ra     : 0x%08x\r\n", stack_common->ra);
        os_kprintf("mcause : 0x%08x\r\n", stack_common->gp);
        os_kprintf("msubm  : 0x%08x\r\n", stack_common->tp);
        os_kprintf("t0     : 0x%08x\r\n", stack_common->t0);
        os_kprintf("t1     : 0x%08x\r\n", stack_common->t1);
        os_kprintf("t2     : 0x%08x\r\n", stack_common->t2);
        os_kprintf("s0     : 0x%08x\r\n", stack_common->s0_fp);
        os_kprintf("s1     : 0x%08x\r\n", stack_common->s1);
        os_kprintf("a0     : 0x%08x\r\n", stack_common->a0);
        os_kprintf("a1     : 0x%08x\r\n", stack_common->a1);
        os_kprintf("a2     : 0x%08x\r\n", stack_common->a2);
        os_kprintf("a3     : 0x%08x\r\n", stack_common->a3);
        os_kprintf("a4     : 0x%08x\r\n", stack_common->a4);
        os_kprintf("a5     : 0x%08x\r\n", stack_common->a5);
        os_kprintf("a6     : 0x%08x\r\n", stack_common->a6);
        os_kprintf("a7     : 0x%08x\r\n", stack_common->a7);
        os_kprintf("s2     : 0x%08x\r\n", stack_common->s2);
        os_kprintf("s3     : 0x%08x\r\n", stack_common->s3);
        os_kprintf("s4     : 0x%08x\r\n", stack_common->s4);
        os_kprintf("s5     : 0x%08x\r\n", stack_common->s5);
        os_kprintf("s6     : 0x%08x\r\n", stack_common->s6);
        os_kprintf("s7     : 0x%08x\r\n", stack_common->s7);
        os_kprintf("s8     : 0x%08x\r\n", stack_common->s8);
        os_kprintf("s9     : 0x%08x\r\n", stack_common->s9);
        os_kprintf("s10    : 0x%08x\r\n", stack_common->s10);
        os_kprintf("s11    : 0x%08x\r\n", stack_common->s11);
        os_kprintf("t3     : 0x%08x\r\n", stack_common->t3);
        os_kprintf("t4     : 0x%08x\r\n", stack_common->t4);
        os_kprintf("t5     : 0x%08x\r\n", stack_common->t5);
        os_kprintf("t6     : 0x%08x\r\n", stack_common->t6);
    #ifdef ARCH_RISCV_FPU
        os_kprintf("ft0    : 0x%08x\r\n", stack_common->f0);
        os_kprintf("ft1    : 0x%08x\r\n", stack_common->f1);
        os_kprintf("ft2    : 0x%08x\r\n", stack_common->f2);
        os_kprintf("ft3    : 0x%08x\r\n", stack_common->f3);
        os_kprintf("ft4    : 0x%08x\r\n", stack_common->f4);
        os_kprintf("ft5    : 0x%08x\r\n", stack_common->f5);
        os_kprintf("ft6    : 0x%08x\r\n", stack_common->f6);
        os_kprintf("ft7    : 0x%08x\r\n", stack_common->f7);
        os_kprintf("fs0    : 0x%08x\r\n", stack_common->f8);
        os_kprintf("fs1    : 0x%08x\r\n", stack_common->f9);
        os_kprintf("fa0    : 0x%08x\r\n", stack_common->f10);
        os_kprintf("fa1    : 0x%08x\r\n", stack_common->f11);
        os_kprintf("fa2    : 0x%08x\r\n", stack_common->f12);
        os_kprintf("fa3    : 0x%08x\r\n", stack_common->f13);
        os_kprintf("fa4    : 0x%08x\r\n", stack_common->f14);
        os_kprintf("fa5    : 0x%08x\r\n", stack_common->f15);
        os_kprintf("fa6    : 0x%08x\r\n", stack_common->f16);
        os_kprintf("fa7    : 0x%08x\r\n", stack_common->f17);
        os_kprintf("fs2    : 0x%08x\r\n", stack_common->f18);
        os_kprintf("fs3    : 0x%08x\r\n", stack_common->f19);
        os_kprintf("fs4    : 0x%08x\r\n", stack_common->f20);
        os_kprintf("fs5    : 0x%08x\r\n", stack_common->f21);
        os_kprintf("fs6    : 0x%08x\r\n", stack_common->f22);
        os_kprintf("fs7    : 0x%08x\r\n", stack_common->f23);
        os_kprintf("fs8    : 0x%08x\r\n", stack_common->f24);
        os_kprintf("fs9    : 0x%08x\r\n", stack_common->f25);
        os_kprintf("fs10   : 0x%08x\r\n", stack_common->f26);
        os_kprintf("fs11   : 0x%08x\r\n", stack_common->f27);
        os_kprintf("ft8    : 0x%08x\r\n", stack_common->f28);
        os_kprintf("ft9    : 0x%08x\r\n", stack_common->f29);
        os_kprintf("ft10   : 0x%08x\r\n", stack_common->f30);
        os_kprintf("ft11   : 0x%08x\r\n", stack_common->f31);
    #endif

    msubm_ptyp = stack_common->tp & 0x300;
    /* Exception generated in task context. */
    if (msubm_ptyp == 0)
    {
        os_kprintf("exception generated in task, task name: %s\r\n", os_task_self()->name);
    }
    /* Exception generated in interrupt context. */
    else if (msubm_ptyp == 0x100)
    {
        os_kprintf("Exception generated in interrupt.\r\n");
    }
    /* Exception generated in other exception. */
    else
    {
        os_kprintf("Exception generated in other exception.\r\n");
    }

    if (vector == 0xfff)
    {
        os_kprintf("Exception type is: NMI\r\n");
    }
    else
    {
        os_kprintf("Exception name is: %s\r\n", arch_exception_names[vector]);
    }

    while (1);
#endif  /* STACK_TRACE_EN */

}

