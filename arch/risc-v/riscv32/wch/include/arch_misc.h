/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        arch_misc.h
 *
 * @brief       This file provides external declarations of architecture-related functions.
 *
 * @revision
 * Date         Author          Notes
 * 2021-12-18   OneOS Team      First version.
 ***********************************************************************************************************************
 */

#ifndef __ARCH_MISC_H__
#define __ARCH_MISC_H__

#include <os_types.h>

#ifdef __cplusplus
extern "C" {
#endif

extern os_int32_t os_ffs(os_uint32_t value);
extern os_int32_t os_fls(os_uint32_t value);
extern void      *os_get_current_task_sp(void);

#ifdef __cplusplus
}
#endif

#endif /* __ARCH_MISC_H__ */
