
#include <oneos_config.h>
#include <os_errno.h>
#include <os_task.h>
#include "shell.h"

#ifdef OS_USING_MINI_PRINT_DEMO
#include "mini_print.h"
static void mini_print(void *para)
{
    char t0 = 01;
    unsigned char t1 = 2;
    short t2 = 3;
    unsigned short t3 = 4;
    int t4 = 5;
    unsigned int t5 = 6;

    static int thread_num = 0;
    int tmp;
    thread_num++;
    tmp = thread_num;

    while (1)
    {
        MINI_STR_BUF_WRITE_STRU("hello %d", tmp);
        MINI_STR_BUF_WRITE_STRU("hello %d %d %d %d %d", 0, 1, 2, 3, 4);
        MINI_STR_BUF_WRITE_STRU("hello %d %d %d %d %d %d", t0, t1, t2, t3, t4, t5);
        os_task_msleep(1);
    }

    return;
}

static void mini_print_out(void *para)
{
    while (1)
    {
        mini_print_str_stru_buf();
        os_task_msleep(1);
    }

    return;
}

static void mini_print_demo(int argc, char **argv)
{
    os_task_t *task = OS_NULL;
    static os_task_t *task_out = OS_NULL;

    task = os_task_create("mini_print", mini_print, OS_NULL, 512, 15);
    if (!task)
    {
        return;
    }
    os_task_startup(task);

    if (!task_out)
    {
        task_out = os_task_create("mini_print_out", mini_print_out, OS_NULL, 512, 15);
        if (!task_out)
        {
            return;
        }
        os_task_startup(task_out);
    }
    return;
}

SH_CMD_EXPORT(mini_print_demo, mini_print_demo, "easy mini str print demo");
#endif
