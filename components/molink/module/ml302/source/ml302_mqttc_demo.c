#include <stdio.h>
#include <string.h>
#include <os_errno.h>
#include <dlog.h>
#include <serial.h>
#include "mo_factory.h"
#include "mo_common.h"
#include "mo_api.h"

#define MOLINK_LOG_TAG "molink"

#define MODULE_NAME                   "ml302"
#define MODULE_AT_PARSER_RECV_BUF_LEN (1500)
#define MODULE_AT_DEVICE_NAME         "uart6"
#define MODULE_AT_DEVICE_RATE         (115200)

#define TEST_HOST       "mq.tongxinmao.com"
#define TEST_PORT       18830
#define TEST_CLIENT_ID  "MQTT_TEST_Client"
#define SUBSCRIBE_TOPIC "test"
#define PUBLISH_TOPIC   "test"

static struct serial_configure uart_config = OS_SERIAL_CONFIG_DEFAULT;

/* create module */
static int molink_module_create(void)
{
    mo_object_t *module = OS_NULL;
    mo_parser_config_t parser_config = {0};

    os_device_t *device = os_device_find(MODULE_AT_DEVICE_NAME);
    if (OS_NULL == device)
    {
        LOG_E(MOLINK_LOG_TAG, "Can not find %s device!", MODULE_AT_DEVICE_NAME);
        return OS_ERROR;
    }

    uart_config.baud_rate = MODULE_AT_DEVICE_RATE;
    os_device_control(device, OS_DEVICE_CTRL_CONFIG, &uart_config);

    parser_config.parser_name = MODULE_NAME;
    parser_config.parser_device = device;
    parser_config.recv_buff_len = MODULE_AT_PARSER_RECV_BUF_LEN;

    module = mo_create(MODULE_NAME, MODULE_TYPE_ML302, &parser_config);
    if (OS_NULL == module)
    {
        LOG_E(MOLINK_LOG_TAG, "Can not find %s interface device!", MODULE_AT_DEVICE_NAME);
        return OS_ERROR;
    }

#ifdef MOLINK_USING_MULTI_MODULES
    /* set default module instance */
    mo_set_default(module);
#endif

    /* get default module instance */
    mo_object_t *temp_module = mo_get_default();
    if (OS_NULL == temp_module)
    {
        LOG_E(MOLINK_LOG_TAG, "Get default module failed!");
        mo_destroy(module, MODULE_TYPE_ML302);
        return OS_ERROR;
    }

    LOG_I(MOLINK_LOG_TAG, "Create module success");

    return OS_EOK;
}

/* destroy module */
static int molink_module_destroy(void)
{
    mo_object_t *module = OS_NULL;
    int result;

    module = mo_get_default();
    if (OS_NULL == module)
    {
        LOG_E(MOLINK_LOG_TAG, "Get default module failed!");
        return OS_ERROR;
    }

    result = mo_destroy(module, MODULE_TYPE_ML302);
    if (OS_ERROR == result)
    {
        LOG_E(MOLINK_LOG_TAG, "Destroy module %s failed!", MODULE_NAME);
        return OS_ERROR;
    }

    LOG_I(MOLINK_LOG_TAG, "Destroy module success");

    return OS_EOK;
}

static void test_mqttc_handler(mqttc_msg_data_t *data)
{
    os_kprintf("Message arrived on topic %.*s: %.*s\n",
               data->topic_name.len,
               data->topic_name.data,
               data->message.payload_len,
               data->message.payload);
}

static void molink_mqtt_client_start(void)
{
    mo_object_t *module = mo_get_default();
    if (NULL == module)
    {
        LOG_E(MOLINK_LOG_TAG, "Get default module failed!");
        return;
    }

    mqttc_create_opts_t create_opts = {.address = {.data = TEST_HOST, .len = strlen(TEST_HOST)},
                                       .port = TEST_PORT,
                                       .command_timeout = 5000,
                                       .max_msgs = 10};

    mo_mqttc_t *mqttc = mo_mqttc_create(module, &create_opts);
    if (OS_NULL == mqttc)
    {
        mo_mqttc_destroy(mqttc);
        LOG_E(MOLINK_LOG_TAG, "mqtt client create failed");
        return;
    }

    mqttc_conn_opts_t conn_opts = {.client_id =
                                       {
                                           .data = TEST_CLIENT_ID,
                                           .len = strlen(TEST_CLIENT_ID),
                                       },
                                   .mqtt_version = 4,
                                   .keep_alive = 60,
                                   .clean_session = 1,
                                   .will_flag = 0,
                                   .will_opts = {0},
                                   .username.data = "",
                                   .username.len = 0,
                                   .password = "",
                                   .password.len = 0};

    os_err_t result = mo_mqttc_connect(mqttc, &conn_opts);
    if (result != OS_EOK)
    {
        mo_mqttc_destroy(mqttc);
        LOG_E(MOLINK_LOG_TAG, "mqtt client connect failed");
        return;
    }

#if defined(MOLINK_USING_MQTTC_TASK)
    result = mo_mqttc_start_task(mqttc);
    if (result != OS_EOK)
    {
        LOG_E(MOLINK_LOG_TAG, "start mqttc task failed");
        goto exit;
    }
#endif

    result = mo_mqttc_subscribe(mqttc, SUBSCRIBE_TOPIC, MQTTC_QOS_0, test_mqttc_handler);
    if (result != OS_EOK)
    {
        LOG_E(MOLINK_LOG_TAG, "mqtt client subscribe failed");
        goto exit;
    }

    for (int i = 0; i < 10; i++)
    {
        char payload[64] = {0};
        sprintf(payload, "{message number %d}", i);

        mqttc_msg_t msg = {.qos = MQTTC_QOS_1, .retained = 0, .payload = payload, .payload_len = strlen(payload)};

        result = mo_mqttc_publish(mqttc, PUBLISH_TOPIC, &msg);

#if !defined(MOLINK_USING_MQTTC_TASK)
        if ((result = mo_mqttc_yield(mqttc, 1000)) != 0)
        {
            LOG_E(MOLINK_LOG_TAG, "Return code from yield is %d", result);
        }
#endif
        os_task_msleep(1000);
    }

exit:
    if (mo_mqttc_isconnect(mqttc) == OS_TRUE)
    {
        mo_mqttc_disconnect(mqttc);
    }

    /* sleep wait ml302 mqttc disconnect urc be received and processed */
    os_task_msleep(3000);
    mo_mqttc_destroy(mqttc);
}

#ifdef OS_USING_SHELL
#include <shell.h>
SH_CMD_EXPORT(molink_module_create, molink_module_create, "molink module create");
SH_CMD_EXPORT(molink_module_destroy, molink_module_destroy, "molink module destroy");
SH_CMD_EXPORT(molink_mqtt_client_start, molink_mqtt_client_start, "molink mqtt client start");
#endif
