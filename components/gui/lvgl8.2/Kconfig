menu "LVGL basic menu"
    config OS_LV_BUFF_LINES
        int "LVGL buff lines"
        range 10 500
        default 50
    
    config LV_DISP_DEF_REFR_PERIOD
        int "LVGL display refresh period(ms)"
        range 10 100
        default 30
    
    config LV_INDEV_DEF_READ_PERIOD
        int "Input device read period(ms)"
        range 10 100
        default 30
    
    config OS_LV_BUFF_DOUBLE
        bool "Enable LVGL two buff"
        default n
    
    config OS_USING_PERF_MONITOR
        bool "Enable CPU usage and FPS count Show"
        default n
    
    config OS_USING_MEM_MONITOR
        bool "Show the used memory and the memory fragmentation"
        default n

    config LV_CONF_MINIMAL
        bool "LVGL minimal configuration."
        default n

    config OS_USING_GPU_STM32_DMA2D
        bool "Enable graphics acceleration"
        #select OS_GRAPHIC_GPU_ENABLE
        default n
        
    config OS_USING_GPU_STM32_DMA2D_CMSIS_INCLUDE
        string "Include file of STM32 DMA2D,ex:stm32f469xx.h"
        depends on OS_USING_GPU_STM32_DMA2D


    config OS_USING_GUI_LVGL_GIF
        bool "Enable LVGL GIF"
        default n
    
    config OS_USING_GUI_LVGL_PNG
        bool "Enable LVGL PNG"
        default n
    
    config OS_USING_GUI_LVGL_JPG
        bool "Enable LVGL JPG"
        default n
    
    config OS_USING_GUI_LVGL_BMP
        bool "Enable LVGL BMP"
        default n
    
    config OS_USING_GUI_LVGL_QRCODE
        bool "Enable LVGL QRCODE"
        default n
endmenu

menu "LVGL Logging"
    config OS_USING_LVGL_LOG
        bool "Enable the log module"
        default n
endmenu

menu "Widget usage"
    config _LV_USE_ARC
        bool "Arc."
        default y if !LV_CONF_MINIMAL
    config _LV_USE_BAR
        bool "Bar."
        default y if !LV_CONF_MINIMAL
    config _LV_USE_BTN
        bool "Button."
        default y if !LV_CONF_MINIMAL
    config _LV_USE_BTNMATRIX
        bool "Button matrix."
        default y if !LV_CONF_MINIMAL
    config _LV_USE_CANVAS
        bool "Canvas. Dependencies: lv_img."
        default y if !LV_CONF_MINIMAL
    config _LV_USE_CHECKBOX
        bool "Check Box"
        default y if !LV_CONF_MINIMAL
    config _LV_USE_DROPDOWN
        bool "Drop down list. Requires: lv_label."
        select _LV_USE_LABEL
        default y if !LV_CONF_MINIMAL
    config _LV_USE_IMG
        bool "Image. Requires: lv_label."
        select _LV_USE_LABEL
        default y if !LV_CONF_MINIMAL
    config _LV_USE_LABEL
        bool "Label."
        default y if !LV_CONF_MINIMAL
    config _LV_LABEL_TEXT_SELECTION
        bool "Enable selecting text of the label."
        depends on _LV_USE_LABEL
        default y
    config _LV_LABEL_LONG_TXT_HINT
        bool "Store extra some info in labels (12 bytes) to speed up drawing of very long texts."
        depends on _LV_USE_LABEL
        default y
    config _LV_USE_LINE
        bool "Line."
        default y if !LV_CONF_MINIMAL
    config _LV_USE_ROLLER
        bool "Roller. Requires: lv_label."
        select _LV_USE_LABEL
        default y if !LV_CONF_MINIMAL
    config _LV_ROLLER_INF_PAGES
        int "Number of extra 'pages' when the controller is infinite."
        default 7
        depends on _LV_USE_ROLLER
    config _LV_USE_SLIDER
        bool "Slider. Requires: lv_bar."
        select _LV_USE_BAR
        default y if !LV_CONF_MINIMAL
    config _LV_USE_SWITCH
        bool "Switch."
        default y if !LV_CONF_MINIMAL
    config _LV_USE_TEXTAREA
        bool "Text area. Requires: lv_label."
        select _LV_USE_LABEL
        default y if !LV_CONF_MINIMAL
    config _LV_TEXTAREA_DEF_PWD_SHOW_TIME
        int "Text area def. pwd show time [ms]."
        default 1500
        depends on _LV_USE_TEXTAREA
    config _LV_USE_TABLE
        bool "Table."
        default y if !LV_CONF_MINIMAL
endmenu

menu "Extra Widgets"
    config _LV_USE_ANIMIMG
        bool "Anim image."
        default y if !LV_CONF_MINIMAL
    config _LV_USE_CALENDAR
        bool "Calendar."
        default y if !LV_CONF_MINIMAL
    config _LV_CALENDAR_WEEK_STARTS_MONDAY
        bool "Calendar week starts monday."
        depends on _LV_USE_CALENDAR
    config _LV_USE_CALENDAR_HEADER_ARROW
        bool "Use calendar header arrow"
        depends on _LV_USE_CALENDAR
        default y
    config _LV_USE_CALENDAR_HEADER_DROPDOWN
        bool "Use calendar header dropdown"
        depends on _LV_USE_CALENDAR
        default y
    config _LV_USE_CHART
        bool "Chart."
        default y if !LV_CONF_MINIMAL
    config _LV_USE_COLORWHEEL
        bool "Colorwheel."
        default y if !LV_CONF_MINIMAL
    config _LV_USE_IMGBTN
        bool "Imgbtn."
        default y if !LV_CONF_MINIMAL
    config _LV_USE_KEYBOARD
        bool "Keyboard."
        default y if !LV_CONF_MINIMAL
    config _LV_USE_LED
        bool "LED."
        default y if !LV_CONF_MINIMAL
    config _LV_USE_LIST
        bool "List."
        default y if !LV_CONF_MINIMAL
    config _LV_USE_MENU
        bool "Menu."
        default y if !LV_CONF_MINIMAL
    config _LV_USE_METER
        bool "Meter."
        default y if !LV_CONF_MINIMAL
    config _LV_USE_MSGBOX
        bool "Msgbox."
        default y if !LV_CONF_MINIMAL
    config _LV_USE_SPINBOX
        bool "Spinbox."
        default y if !LV_CONF_MINIMAL
    config _LV_USE_SPINNER
        bool "Spinner."
        default y if !LV_CONF_MINIMAL
    config _LV_USE_TABVIEW
        bool "Tabview."
        default y if !LV_CONF_MINIMAL
    config _LV_USE_TILEVIEW
        bool "Tileview"
        default y if !LV_CONF_MINIMAL
    config _LV_USE_WIN
        bool "Win"
        default y if !LV_CONF_MINIMAL
    config _LV_USE_SPAN
        bool "span"
        default y if !LV_CONF_MINIMAL
    config _LV_SPAN_SNIPPET_STACK_SIZE
        int "Maximum number of span descriptor"
        default 64
        depends on _LV_USE_SPAN
endmenu




menu "Themes"
    config _LV_USE_THEME_DEFAULT
        bool "A simple, impressive and very complete theme"
        default y if !LV_CONF_MINIMAL
    config _LV_THEME_DEFAULT_DARK
        bool "Yes to set dark mode, No to set light mode"
        depends on _LV_USE_THEME_DEFAULT
    config _LV_THEME_DEFAULT_GROW
        bool "Enable grow on press"
        default y
        depends on _LV_USE_THEME_DEFAULT
    config _LV_THEME_DEFAULT_TRANSITION_TIME
        int "Default transition time in [ms]"
        default 80
        depends on _LV_USE_THEME_DEFAULT
    config _LV_USE_THEME_BASIC
        bool "A very simple theme that is a good starting point for a custom theme"
        default y if !LV_CONF_MINIMAL
endmenu



menu "Layouts"
    config _LV_USE_FLEX
        bool "A layout similar to Flexbox in CSS."
        default y if !LV_CONF_MINIMAL
    config _LV_USE_GRID
        bool "A layout similar to Grid in CSS."
        default y if !LV_CONF_MINIMAL
endmenu



menu "Text Settings"
    choice LV_TXT_ENC
        prompt "Select a character encoding for strings"
        help
            Select a character encoding for strings. Your IDE or editor should have the same character encoding.
        default _LV_TXT_ENC_UTF8 if !LV_CONF_MINIMAL
        default _LV_TXT_ENC_ASCII if LV_CONF_MINIMAL

        config _LV_TXT_ENC_UTF8
            bool "UTF8"
        config _LV_TXT_ENC_ASCII
            bool "ASCII"
    endchoice
endmenu




menu "Font usage"
    menu "Enable built-in fonts"
        config _LV_FONT_MONTSERRAT_8
            bool "Enable Montserrat 8"
        config _LV_FONT_MONTSERRAT_10
            bool "Enable Montserrat 10"
        config _LV_FONT_MONTSERRAT_12
            bool "Enable Montserrat 12"
        config _LV_FONT_MONTSERRAT_14
            bool "Enable Montserrat 14"
            default y if !LV_CONF_MINIMAL
        config _LV_FONT_MONTSERRAT_16
            bool "Enable Montserrat 16"
        config _LV_FONT_MONTSERRAT_18
            bool "Enable Montserrat 18"
        config _LV_FONT_MONTSERRAT_20
            bool "Enable Montserrat 20"
        config _LV_FONT_MONTSERRAT_22
            bool "Enable Montserrat 22"
        config _LV_FONT_MONTSERRAT_24
            bool "Enable Montserrat 24"
        config _LV_FONT_MONTSERRAT_26
            bool "Enable Montserrat 26"
        config _LV_FONT_MONTSERRAT_28
            bool "Enable Montserrat 28"
        config _LV_FONT_MONTSERRAT_30
            bool "Enable Montserrat 30"
        config _LV_FONT_MONTSERRAT_32
            bool "Enable Montserrat 32"
        config _LV_FONT_MONTSERRAT_34
            bool "Enable Montserrat 34"
        config _LV_FONT_MONTSERRAT_36
            bool "Enable Montserrat 36"
        config _LV_FONT_MONTSERRAT_38
            bool "Enable Montserrat 38"
        config _LV_FONT_MONTSERRAT_40
            bool "Enable Montserrat 40"
        config _LV_FONT_MONTSERRAT_42
            bool "Enable Montserrat 42"
        config _LV_FONT_MONTSERRAT_44
            bool "Enable Montserrat 44"
        config _LV_FONT_MONTSERRAT_46
            bool "Enable Montserrat 46"
        config _LV_FONT_MONTSERRAT_48
            bool "Enable Montserrat 48"

        config _LV_FONT_MONTSERRAT_12_SUBPX
            bool "Enable Montserrat 12 sub-pixel"
        config _LV_FONT_MONTSERRAT_28_COMPRESSED
            bool "Enable Montserrat 28 compressed"
        config _LV_FONT_DEJAVU_16_PERSIAN_HEBREW
            bool "Enable Dejavu 16 Persian, Hebrew, Arabic letters"
        config _LV_FONT_SIMSUN_16_CJK
            bool "Enable Simsun 16 CJK"

        config _LV_FONT_UNSCII_8
            bool "Enable UNSCII 8 (Perfect monospace font)"
            default y if LV_CONF_MINIMAL
        config _LV_FONT_UNSCII_16
            bool "Enable UNSCII 16 (Perfect monospace font)"

        config _LV_FONT_CUSTOM
            bool "Enable the custom font"
        config _LV_FONT_CUSTOM_DECLARE
            string "Header to include for the custom font"
            depends on _LV_FONT_CUSTOM
    endmenu

    choice _LV_FONT_DEFAULT
        prompt "Select theme default title font"
        default _LV_FONT_DEFAULT_MONTSERRAT_14 if !LV_CONF_MINIMAL
        default _LV_FONT_DEFAULT_UNSCII_8 if LV_CONF_MINIMAL
        help
            Select theme default title font

        config _LV_FONT_DEFAULT_MONTSERRAT_8
            bool "Montserrat 8"
            select _LV_FONT_MONTSERRAT_8
        config _LV_FONT_DEFAULT_MONTSERRAT_12
            bool "Montserrat 12"
            select _LV_FONT_MONTSERRAT_12
        config _LV_FONT_DEFAULT_MONTSERRAT_14
            bool "Montserrat 14"
            select _LV_FONT_MONTSERRAT_14
        config _LV_FONT_DEFAULT_MONTSERRAT_16
            bool "Montserrat 16"
            select _LV_FONT_MONTSERRAT_16
        config _LV_FONT_DEFAULT_MONTSERRAT_18
            bool "Montserrat 18"
            select _LV_FONT_MONTSERRAT_18
        config _LV_FONT_DEFAULT_MONTSERRAT_20
            bool "Montserrat 20"
            select _LV_FONT_MONTSERRAT_20
        config _LV_FONT_DEFAULT_MONTSERRAT_22
            bool "Montserrat 22"
            select _LV_FONT_MONTSERRAT_22
        config _LV_FONT_DEFAULT_MONTSERRAT_24
            bool "Montserrat 24"
            select _LV_FONT_MONTSERRAT_24
        config _LV_FONT_DEFAULT_MONTSERRAT_26
            bool "Montserrat 26"
            select _LV_FONT_MONTSERRAT_26
        config _LV_FONT_DEFAULT_MONTSERRAT_28
            bool "Montserrat 28"
            select _LV_FONT_MONTSERRAT_28
        config _LV_FONT_DEFAULT_MONTSERRAT_30
            bool "Montserrat 30"
            select _LV_FONT_MONTSERRAT_30
        config _LV_FONT_DEFAULT_MONTSERRAT_32
            bool "Montserrat 32"
            select _LV_FONT_MONTSERRAT_32
        config _LV_FONT_DEFAULT_MONTSERRAT_34
            bool "Montserrat 34"
            select _LV_FONT_MONTSERRAT_34
        config _LV_FONT_DEFAULT_MONTSERRAT_36
            bool "Montserrat 36"
            select _LV_FONT_MONTSERRAT_36
        config _LV_FONT_DEFAULT_MONTSERRAT_38
            bool "Montserrat 38"
            select _LV_FONT_MONTSERRAT_38
        config _LV_FONT_DEFAULT_MONTSERRAT_40
            bool "Montserrat 40"
            select _LV_FONT_MONTSERRAT_40
        config _LV_FONT_DEFAULT_MONTSERRAT_42
            bool "Montserrat 42"
            select _LV_FONT_MONTSERRAT_42
        config _LV_FONT_DEFAULT_MONTSERRAT_44
            bool "Montserrat 44"
            select _LV_FONT_MONTSERRAT_44
        config _LV_FONT_DEFAULT_MONTSERRAT_46
            bool "Montserrat 46"
            select _LV_FONT_MONTSERRAT_46
        config _LV_FONT_DEFAULT_MONTSERRAT_48
            bool "Montserrat 48"
            select _LV_FONT_MONTSERRAT_48
        config _LV_FONT_DEFAULT_MONTSERRAT_12_SUBPX
            bool "Montserrat 12 sub-pixel"
            select _LV_FONT_MONTSERRAT_12_SUBPX
        config _LV_FONT_DEFAULT_MONTSERRAT_28_COMPRESSED
            bool "Montserrat 28 compressed"
            select _LV_FONT_MONTSERRAT_28_COMPRESSED
        config _LV_FONT_DEFAULT_DEJAVU_16_PERSIAN_HEBREW
            bool "Dejavu 16 Persian, Hebrew, Arabic letters"
            select _LV_FONT_DEJAVU_16_PERSIAN_HEBREW
        config _LV_FONT_DEFAULT_SIMSUN_16_CJK
            bool "Simsun 16 CJK"
            select _LV_FONT_SIMSUN_16_CJK
        config _LV_FONT_DEFAULT_UNSCII_8
            bool "UNSCII 8 (Perfect monospace font)"
            select _LV_FONT_UNSCII_8
        config _LV_FONT_DEFAULT_UNSCII_16
            bool "UNSCII 16 (Perfect monospace font)"
            select _LV_FONT_UNSCII_16
    endchoice
endmenu






menuconfig OS_USING_GUI_LVGL_EXAMPLES
    bool "Enable LVGL examples"
    default n

    if OS_USING_GUI_LVGL_EXAMPLES
    config OS_USING_GUI_EX_SCROLL
        bool "Enable LVGL example scroll"
        default n
    config OS_USING_GUI_EX_ANIM
        bool "Enable LVGL example anim"
        default n
    config OS_USING_GUI_EX_EVENT
        bool "Enable LVGL example event"
        default n
    config OS_USING_GUI_EX_START
        bool "Enable LVGL example start"
        default n
    config OS_USING_GUI_EX_FLEX
        bool "Enable LVGL example flex"
        default n
    config OS_USING_GUI_EX_GRID
        bool "Enable LVGL example grid"
        default n
    config OS_USING_GUI_EX_QRCODE
        bool "Enable LVGL example qrcode"
        select OS_USING_GUI_LVGL_QRCODE
        default n
    endif


menuconfig OS_USING_GUI_LVGL_DEMO
    bool "Enable LVGL Demo"
    default n

    if OS_USING_GUI_LVGL_DEMO
    config LV_USE_DEMO_BENCHMARK
        int "Enable LVGL demo benchmark"
        range 0 1
        default 0
    
    config LV_USE_DEMO_MUSIC
        int "Enable LVGL demo music"
        range 0 1
        default 0
    
    config LV_USE_DEMO_KEYPAD_AND_ENCODER
        int "Enable LVGL demo keypad and encoder"
        range 0 1
        default 0
    
    config LV_USE_DEMO_STRESS
        int "Enable LVGL demo stress"
        range 0 1
        default 0
    
    config LV_USE_DEMO_WIDGETS
        int "Enable LVGL demo widgets"
        range 0 1
        default 0
    endif

