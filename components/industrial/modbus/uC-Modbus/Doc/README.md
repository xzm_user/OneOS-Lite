# uC/Modbus 使用文档

## 组件概述

µC/Modbus 为实现 Modbus 提供了嵌入式解决方案，Modbus 是一种用于连接工业电子设备的工业通信协议。

Modbus 协议由预定义数据包（在此称为“帧”）中的数据接收和传输组成。Modbus 协议使用两种类型的帧，ASCII 帧和远程终端单元 (RTU) 帧。ASCII 帧是基于 ASCII 十六进制字符的帧，而 RTU 帧是严格的二进制实现。ASCII 模式更容易实现和调试，但提供的数据传输速度大约是 RTU 模式的一半。 µC/Modbus对两种模式均支持。µC/Modbus 可以支持任意数量的通信通道，且每个通道的操作模式可以是 ASCII 或 RTU，并且可以在每个“通道”基础上进行选择。

## 常用API介绍

### 1 µC/Modbus-初始化

| 接口           | 说明                                                         |
| -------------- | ------------------------------------------------------------ |
| MB_CfgCh       | 用于配置 Modbus 通道, 必须在`MB_Init()`调用之后调用。        |
| MB_ChToPortMap | 更改通道的“逻辑”映射，通常不需要使用此功能。                 |
| MB_Exit        | 不再运行 µC/Modbus，则应调用。                               |
| MB_Init        | 初始化 µC/Modbus                                             |
| MB_ModeSet     | 更改通道的 Modbus 模式，通常不需要使用此功能。               |
| MB_NodeAddrSet | 更改通道将响应的“节点地址”，通常不需要使用此功能。           |
| MB_WrEnSet     | 启用或禁用对产品中参数写入，即通道设为只读通道。通常不需要使用此功能。 |

### 2 µC/Modbus-主站

| 接口                     | 说明                                 |
| ------------------------ | ------------------------------------ |
| MBM_FC01_CoilRd          | 读取从站上的线圈                     |
| MBM_FC02_DIRd            | 读取从站上的离散输入                 |
| MBM_FC03_HoldingRegRd    | 读取从站上的 16 位保持寄存器         |
| MBM_FC03_HoldingRegRdFP  | 读取从站 上的32 位浮点寄存器         |
| MBM_FC04_InRegRd         | 读取从站上的 16 位输入寄存器寄存器   |
| MBM_FC05_CoilWr          | 写入从站上的单个线圈                 |
| MBM_FC06_HoldingRegWr    | 写入从站上的单个 16 位保持寄存器     |
| MBM_FC06_HoldingRegWrFP  | 写入从站上的单个 32 位浮点保持寄存器 |
| MBM_FC08_Diag            | 对从站执行诊断检查                   |
| MBM_FC15_CoilWr          | 读取从站上的多个线圈                 |
| MBM_FC16_HoldingRegWrN   | 读取从站上的多个 16 位保持寄存器     |
| MBM_FC16_HoldingRegWrNFP | 读取从站上的多个 32 位浮点保持寄存器 |

### 3 µC/Modbus-从站

µC/Modbus-S 通过定义在`mb_data.c`来访问， 具体来说您在此文件中提供的函数由 µC/Modbus-S 调用以读取和写入线圈、整数、浮点值等。因此，用户需要根据产品实现一下接口，由用户决定如何访问数据。

| 接口              | 说明                     |
| ----------------- | ------------------------ |
| MB_CoilRd         | 返回单个线圈的值         |
| MB_CoilWr         | 改变单个线圈的值         |
| MB_DIRd           | 读取单个离散输入的值     |
| MB_FileRd         | 从文件中读取单个整数值   |
| MB_FileWr         | 将单个整数值写入文件     |
| MB_HoldingRegRd   | 读取单个保持寄存器的值   |
| MB_HoldingRegRdFP | 读取单个保持寄存器的值   |
| MB_HoldingRegWr   | 写入单个保持寄存器值     |
| MB_HoldingRegWrFP | 写入单个浮点保持寄存器值 |
| MB_InRegRd        | 读取单个输入寄存器的值   |
| MB_InRegRdFP      | 读取单个输入寄存器的值   |

## 使用举例

µC/Modbus 通过简单地调用`MB_Init()`并将 Modbus RTU 频率指定为参数来初始化。初始化后，您只需配置每个 Modbus 通道（使用`MB_CfgCh()`），如下例所示。在这里，我们的产品具有三个 Modbus 端口：一个以 9600 波特通信的 Modbus RTU 端口和一个以 19200 波特通信的 Modbus ASCII 端口和一个以 19200 波特通信的 Modbus ASCII Master 端口。

```
MB_Init(1000); // 以 1000 Hz 初始化 uC/Modbus，RTU 定时器

MB_CfgCh( 0x01, // ... 该从属通道的 Modbus 节点#
         MODBUS_SLAVE, // ... 这是一个 SLAVE
            0, // ... 0 当从属
         MODBUS_MODE_RTU, // ... Modbus 模式（_ASCII 或 _RTU）
            1, // ... 指定 UART #1
         9600, // ... 波特率   
            8, // ... 数据位数 7 或 8 
         MODBUS_PARITY_NONE,// ...奇偶校验：_NONE、_ODD 或 _EVEN 
          1, // ... 停止位的数量 1 或 2
         MODBUS_WR_EN); // ... 启用 (_EN) 或禁用 (_DIS) 写入

MB_CfgCh( 0x02, // ... 该从属通道的 Modbus 节点#
         MODBUS_SLAVE, // ... 这是一个 SLAVE
            0, // ... 0 当从属
         MODBUS_MODE_ASCII, // ... Modbus 模式（_ASCII 或 _RTU）
            2, // ... 指定 UART #2
        19200, // ... 波特率
            8, // ... 数据位数 7 或 8
         MODBUS_PARITY_NONE,// ...奇偶校验：_NONE、_ODD 或 _EVEN
            1, // ... 停止位的数量 1 或 2
         MODBUS_WR_EN); // ... 启用 (_EN) 或禁用 (_DIS) 写入
         
MB_CfgCh( 0x03, // ... 该从属通道的 Modbus 节点#
         MODBUS_MASTER, // ... 这是一个 MASTER
         500, // ... 等待从站响应的500ms超时
         MODBUS_MODE_ASCII, // ... Modbus 模式（_ASCII 或 _RTU）
            3, // ... 指定 UART #3 
        19200, // ... 波特率
            8, // ... 数据位数 7 或 8
         MODBUS_PARITY_NONE,// ...奇偶校验：_NONE、_ODD 或 _EVEN
            1, // ... 停止位的数量 1 或 2
         MODBUS_WR_EN); // ... 启用 (_EN) 或禁用 (_DIS) 写入
         
```

## 参考资料

For the complete documentation, visit <https://doc.micrium.com/display/ucos/>
