/**
 ***********************************************************************************************************************
 * Copyright (c) 2022, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        mb_test.c
 *
 * @brief       
 *
 * @revision
 * Date         Author          Notes
 * 2022-01-10   OneOS Team      First Version
 ***********************************************************************************************************************
 */
#include "oneos_config.h"
#define MB_TEST_ENABLE
#ifdef MB_TEST_ENABLE
#include "mb.h"
#include "shell.h"
#include "stdlib.h"
#include "string.h"

static os_uint8_t sg_master_slave;
static os_uint8_t sg_mb_mode;
static os_uint8_t sg_node_id;
static MODBUS_CH *sg_mb_pch = OS_NULL;

static const char *sg_mb_cmd_help = "help:\r\n"
                                    "     mb_test exit\r\n"
                                    "     mb_test init <master/slave> <rtu/ascii> <node_id> [port_num] \r\n"
                                    "     mb_test write \r\n"
                                    "     mb_test read \r\n";

static int mb_cmd_init(int n, char **arg)
{
    os_uint8_t master_slave;
    os_uint8_t mb_mode;
    os_uint8_t node_id;
    os_uint8_t port_num;

    os_uint8_t index;
    const char *cmd;

    if (n < 5)
    {
        return -1;
    }

    index = 2;
    cmd = arg[index++];

    if (!strcmp(cmd, "master"))
    {
        master_slave = MODBUS_MASTER;
    }
    else if (!strcmp(cmd, "slave"))
    {
        master_slave = MODBUS_SLAVE;
    }
    else
    {
        return -1;
    }

    cmd = arg[index++];

    if (!strcmp(cmd, "rtu"))
    {
        mb_mode = MODBUS_MODE_RTU;
    }
    else if (!strcmp(cmd, "ascii"))
    {
        mb_mode = MODBUS_MODE_ASCII;
    }
    else
    {
        return -1;
    }

    cmd = arg[index++];
    node_id = atoi(cmd);

    if (n > 5)
    {
        cmd = arg[index++];
        port_num = atoi(cmd);
    }

    if (sg_mb_pch)
    {
        os_kprintf("mb node %d already init.\r\n", sg_node_id);
        return 0;
    }

    MB_Init(50);
    sg_mb_pch = MB_CfgCh(node_id, master_slave, 500, mb_mode, port_num, 115200, 8, MODBUS_PARITY_NONE, 1, MODBUS_WR_EN);
    if (!sg_mb_pch->Handle)
    {
        MB_Exit();
        sg_mb_pch = OS_NULL;
        os_kprintf("mb node %d init faild.\r\n", node_id);
        return -1;
    }

    sg_master_slave = master_slave;
    sg_mb_mode = mb_mode;
    sg_node_id = node_id;
    sg_mb_mode = sg_mb_mode;
    os_kprintf("mb node %d init ok.\r\n", sg_node_id);
    return 0;
}

static int mb_cmd_exit(int n, char **arg)
{
    if (sg_mb_pch)
    {
        MB_Exit();
        sg_mb_pch = OS_NULL;
        os_kprintf("mb exit ok.\r\n");
    }
    return 0;
}

static int mb_cmd_write(int n, char **arg)
{
    if (sg_mb_pch == OS_NULL)
    {
        os_kprintf("mb must init master first.\r\n");
        return -1;
    }

    if (sg_master_slave == MODBUS_MASTER)
    {
        os_uint16_t coil = 100;

        if (n > 2)
        {
            coil = atoi(arg[2]);
        }

        MBM_FC06_HoldingRegWr(sg_mb_pch, sg_node_id, 0, coil);
    }

    return 0;
}

static int mb_cmd_read(int n, char **arg)
{
    os_uint16_t coil;

    if (sg_mb_pch == OS_NULL)
    {
        os_kprintf("mb must init master first.\r\n");
        return 0;
    }

    if (sg_master_slave == MODBUS_MASTER)
    {
        MBM_FC03_HoldingRegRd(sg_mb_pch, sg_node_id, 0, &coil, 1);
        os_kprintf("HoldingReg = %d\r\n", coil);
    }

    return 0;
}

static int mb_test(int n, char **arg)
{
    int ret;
    const char *cmd = arg[1];

    if (n < 2)
    {
        return -1;
    }

    if (!strcmp(cmd, "exit"))
    {
        ret = mb_cmd_exit(n, arg);
    }
    else if (!strcmp(cmd, "init"))
    {
        ret = mb_cmd_init(n, arg);
    }
    else if (!strcmp(cmd, "read"))
    {
        ret = mb_cmd_read(n, arg);
    }
    else if (!strcmp(cmd, "write"))
    {
        ret = mb_cmd_write(n, arg);
    }
    else
    {
        ret = -1;
    }

    if (ret < 0)
    {
        os_kprintf("%s", sg_mb_cmd_help);
    }

    return ret;
}

SH_CMD_EXPORT(mb_test, mb_test, "mb_test");
#endif
