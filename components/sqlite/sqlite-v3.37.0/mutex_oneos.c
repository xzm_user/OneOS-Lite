#ifdef SQLITE_MUTEX_ONEOS /* This file is used on oneos only */

#include <os_mutex.h>

/*
** The sqlite3_mutex.id, sqlite3_mutex.nRef, and sqlite3_mutex.owner fields
** are necessary under two condidtions:  (1) Debug builds and (2) using
** home-grown mutexes.  Encapsulate these conditions into a single #define.
*/
#if defined(SQLITE_DEBUG) || defined(SQLITE_HOMEGROWN_RECURSIVE_MUTEX)
#define SQLITE_MUTEX_NREF 1
#else
#define SQLITE_MUTEX_NREF 0
#endif

/*
** Each recursive mutex is an instance of the following structure.
*/
struct sqlite3_mutex
{
    os_mutex_t mutex;                                        /* Mutex controlling the lock */
#if SQLITE_MUTEX_NREF || defined(SQLITE_ENABLE_API_ARMOR)    //???
    int id;                                                  /* Mutex type */
#endif
#if SQLITE_MUTEX_NREF
    volatile int nRef;        /* Number of entrances */
    volatile pthread_t owner; /* Thread that is within this mutex */
    int trace;                /* True to trace changes */
#endif
};

/*
** The sqlite3_mutex_held() and sqlite3_mutex_notheld() routine are
** intended for use only inside assert() statements.  On some platforms,
** there might be race conditions that can cause these routines to
** deliver incorrect results.  In particular, if pthread_equal() is
** not an atomic operation, then these routines might delivery
** incorrect results.  On most platforms, pthread_equal() is a
** comparison of two integers and is therefore atomic.  But we are
** told that HPUX is not such a platform.  If so, then these routines
** will not always work correctly on HPUX.
**
** On those platforms where pthread_equal() is not atomic, SQLite
** should be compiled without -DSQLITE_DEBUG and with -DNDEBUG to
** make sure no assert() statements are evaluated and hence these
** routines are never called.
*/
#if !defined(NDEBUG) || defined(SQLITE_DEBUG)
static int oneos_mutex_held(sqlite3_mutex *p)
{
    if (p != NULL)
    {
        if (p->mutex.owner == os_task_self() && (p->mutex.lock_count > 0))
        {
            return 1;
        }

        return 0;
    }

    return 1;
}

static int oneos_mutex_noheld(sqlite3_mutex *p)
{
    if (oneos_mutex_held(p))
    {
        return 0;
    }

    return 1;
}
#endif /* not NDEBUG or SQLITE_DEBUG */

/*
** Try to provide a memory barrier operation, needed for initialization
** and also for the implementation of xShmBarrier in the VFS in cases
** where SQLite is compiled without mutexes.
*/
SQLITE_PRIVATE void sqlite3MemoryBarrier(void)
{
}

/*
** Initialize and deinitialize the mutex subsystem.
*/
static sqlite3_mutex static_mutexes[12] = {0};

static int oneos_mutex_init(void)
{
    int ret;

    for (int i = 0; i < sizeof(static_mutexes) / sizeof(static_mutexes[0]); i++)
    {
        ret = os_mutex_init(&static_mutexes[i].mutex, "sqlmtx", OS_TRUE);
        if (ret != OS_EOK)
        {
            return SQLITE_ERROR;
        }
    }

    return SQLITE_OK;
}

static int oneos_mutex_end(void)
{
    int ret;

    for (int i = 0; i < sizeof(static_mutexes) / sizeof(static_mutexes[0]); i++)
    {
        ret = os_mutex_deinit(&static_mutexes[i].mutex);
        static_mutexes[i].mutex.owner = 0;         //???
        static_mutexes[i].mutex.lock_count = 0;    //???
        if (ret != OS_EOK)
        {
            return SQLITE_ERROR;
        }
    }

    return SQLITE_OK;
}

/*
** The sqlite3_mutex_alloc() routine allocates a new
** mutex and returns a pointer to it.  If it returns NULL
** that means that a mutex could not be allocated.  SQLite
** will unwind its stack and return an error.  The argument
** to sqlite3_mutex_alloc() is one of these integer constants:
**
** <ul>
** <li>  SQLITE_MUTEX_FAST
** <li>  SQLITE_MUTEX_RECURSIVE
** <li>  SQLITE_MUTEX_STATIC_MAIN
** <li>  SQLITE_MUTEX_STATIC_MEM
** <li>  SQLITE_MUTEX_STATIC_OPEN
** <li>  SQLITE_MUTEX_STATIC_PRNG
** <li>  SQLITE_MUTEX_STATIC_LRU
** <li>  SQLITE_MUTEX_STATIC_PMEM
** <li>  SQLITE_MUTEX_STATIC_APP1
** <li>  SQLITE_MUTEX_STATIC_APP2
** <li>  SQLITE_MUTEX_STATIC_APP3
** <li>  SQLITE_MUTEX_STATIC_VFS1
** <li>  SQLITE_MUTEX_STATIC_VFS2
** <li>  SQLITE_MUTEX_STATIC_VFS3
** </ul>
**
** The first two constants cause sqlite3_mutex_alloc() to create
** a new mutex.  The new mutex is recursive when SQLITE_MUTEX_RECURSIVE
** is used but not necessarily so when SQLITE_MUTEX_FAST is used.
** The mutex implementation does not need to make a distinction
** between SQLITE_MUTEX_RECURSIVE and SQLITE_MUTEX_FAST if it does
** not want to.  But SQLite will only request a recursive mutex in
** cases where it really needs one.  If a faster non-recursive mutex
** implementation is available on the host platform, the mutex subsystem
** might return such a mutex in response to SQLITE_MUTEX_FAST.
**
** The other allowed parameters to sqlite3_mutex_alloc() each return
** a pointer to a static preexisting mutex.  Six static mutexes are
** used by the current version of SQLite.  Future versions of SQLite
** may add additional static mutexes.  Static mutexes are for internal
** use by SQLite only.  Applications that use SQLite mutexes should
** use only the dynamic mutexes returned by SQLITE_MUTEX_FAST or
** SQLITE_MUTEX_RECURSIVE.
**
** Note that if one of the dynamic mutex parameters (SQLITE_MUTEX_FAST
** or SQLITE_MUTEX_RECURSIVE) is used then sqlite3_mutex_alloc()
** returns a different mutex on every call.  But for the static
** mutex types, the same mutex is returned on every call that has
** the same type number.
*/
static sqlite3_mutex *oneos_mutex_alloc(int type)
{
    sqlite3_mutex *p = NULL;

    switch (type)
    {
    case SQLITE_MUTEX_RECURSIVE:
        p = sqlite3Malloc(sizeof(sqlite3_mutex));
        if (p != NULL)
        {
#ifdef SQLITE_HOMEGROWN_RECURSIVE_MUTEX
            /* If recursive mutexes are not available, we will have to
            ** build our own.  See below. */
            // oneos support recursive mutex
#else
            /* Use a recursive mutex if it is available */
            os_mutex_init(&p->mutex, "sqlmtx", OS_TRUE);
#endif
#if SQLITE_MUTEX_NREF || defined(SQLITE_ENABLE_API_ARMOR)
            p->id = SQLITE_MUTEX_RECURSIVE;
#endif
        }
        break;

    case SQLITE_MUTEX_FAST:
        p = sqlite3Malloc(sizeof(sqlite3_mutex));
        if (p != NULL)
        {
            os_mutex_init(&p->mutex, "sqlmtx", OS_FALSE);
#if SQLITE_MUTEX_NREF || defined(SQLITE_ENABLE_API_ARMOR)
            p->id = SQLITE_MUTEX_FAST;
#endif
        }
        break;

    default:
#ifdef SQLITE_ENABLE_API_ARMOR
        if (type - 2 < 0 || type - 2 >= ArraySize(static_mutexes))
        {
            (void)SQLITE_MISUSE_BKPT;
            return 0;
        }
#endif
        p = &static_mutexes[type - 2];

        break;
    }

#if SQLITE_MUTEX_NREF || defined(SQLITE_ENABLE_API_ARMOR)
    assert(p == NULL || p->id == type);
#endif

    return p;
}

static void oneos_mutex_free(sqlite3_mutex *p)
{
    assert(p != NULL);

#if SQLITE_ENABLE_API_ARMOR
    if (p->id == SQLITE_MUTEX_FAST || p->id == SQLITE_MUTEX_RECURSIVE)
#endif
    {
        os_mutex_deinit(&p->mutex);
        sqlite3_free(p);
    }
#ifdef SQLITE_ENABLE_API_ARMOR
    else
    {
        (void)SQLITE_MISUSE_BKPT;
    }
#endif
}

/*
** The sqlite3_mutex_enter() and sqlite3_mutex_try() routines attempt
** to enter a mutex.  If another thread is already within the mutex,
** sqlite3_mutex_enter() will block and sqlite3_mutex_try() will return
** SQLITE_BUSY.  The sqlite3_mutex_try() interface returns SQLITE_OK
** upon successful entry.  Mutexes created using SQLITE_MUTEX_RECURSIVE can
** be entered multiple times by the same thread.  In such cases the,
** mutex must be exited an equal number of times before another thread
** can enter.  If the same thread tries to enter any other kind of mutex
** more than once, the behavior is undefined.
*/
static void oneos_mutex_enter(sqlite3_mutex *p)
{
#if SQLITE_MUTEX_NREF || defined(SQLITE_ENABLE_API_ARMOR)
#if !defined(NDEBUG) || defined(SQLITE_DEBUG)
    assert(p->id == SQLITE_MUTEX_RECURSIVE || oneos_mutex_noheld(p));
#endif
#endif

#ifdef SQLITE_HOMEGROWN_RECURSIVE_MUTEX
    /* If recursive mutexes are not available, then we have to grow
     ** our own.  This implementation assumes that pthread_equal()
     ** is atomic - that it cannot be deceived into thinking self
     ** and p->owner are equal if p->owner changes between two values
     ** that are not equal to self while the comparison is taking place.
     ** This implementation also assumes a coherent cache - that
     ** separate processes cannot read different values from the same
     ** address at the same time.  If either of these two conditions
     ** are not met, then the mutexes will fail and problems will result.
     */
    // oneos support recursive mutex
#else
    /* Use the built-in recursive mutexes if they are available.
     */
    os_mutex_recursive_lock(&p->mutex, OS_WAIT_FOREVER);
#if SQLITE_MUTEX_NREF
    // nothing
#endif
#endif
}

static int oneos_mutex_try(sqlite3_mutex *p)
{
    assert(p != NULL);
    int rc = SQLITE_OK;
#ifdef SQLITE_HOMEGROWN_RECURSIVE_MUTEX
    /* If recursive mutexes are not available, then we have to grow
    ** our own.  This implementation assumes that pthread_equal()
    ** is atomic - that it cannot be deceived into thinking self
    ** and p->owner are equal if p->owner changes between two values
    ** that are not equal to self while the comparison is taking place.
    ** This implementation also assumes a coherent cache - that
    ** separate processes cannot read different values from the same
    ** address at the same time.  If either of these two conditions
    ** are not met, then the mutexes will fail and problems will result.
    */
    // oneos support recursive mutex
#else
    /* Use the built-in recursive mutexes if they are available.
     */
    int mutex_type = 0;
    os_err_t mutex_error = 0;
    os_mutex_t *mutex = &p->mutex;

    mutex_type = mutex->is_recursive;

    os_schedule_lock();

    if ((mutex->owner == os_task_self()) && (OS_TRUE != mutex_type))
    {
        os_schedule_unlock();
        return SQLITE_ERROR;
    }

    os_schedule_unlock();

    if (OS_TRUE != mutex_type)
    {
        mutex_error = os_mutex_lock(&p->mutex, OS_NO_WAIT);
    }
    else
    {
        mutex_error = os_mutex_recursive_lock(&p->mutex, OS_NO_WAIT);
    }

    if (OS_EOK != mutex_error)
    {
        rc = SQLITE_BUSY;
    }
    else
    {
        rc = SQLITE_OK;
#if SQLITE_MUTEX_NREF
        // nothing
#endif
    }
#endif

    return rc;
}

static void oneos_mutex_leave(sqlite3_mutex *p)
{
    assert(p != 0);
#if SQLITE_MUTEX_NREF || defined(SQLITE_ENABLE_API_ARMOR)
    assert(p->id == SQLITE_MUTEX_RECURSIVE);
#endif
#if !defined(NDEBUG) || defined(SQLITE_DEBUG)
    assert(oneos_mutex_held(p));
#endif

#if SQLITE_MUTEX_NREF
    // nothing
#endif

#ifdef SQLITE_HOMEGROWN_RECURSIVE_MUTEX
    // oneos support recursive mutex
#else
    os_mutex_recursive_unlock(&p->mutex);
#endif
}

SQLITE_PRIVATE sqlite3_mutex_methods const *sqlite3DefaultMutex(void)
{
    static const sqlite3_mutex_methods sMutex = {oneos_mutex_init,
                                                 oneos_mutex_end,
                                                 oneos_mutex_alloc,
                                                 oneos_mutex_free,
                                                 oneos_mutex_enter,
                                                 oneos_mutex_try,
                                                 oneos_mutex_leave,
#ifdef SQLITE_DEBUG
                                                 oneos_mutex_held,
                                                 oneos_mutex_noheld
#else
                                                 0,
                                                 0
#endif
    };

    return &sMutex;
}

#endif /* SQLITE_MUTEX_ONEOS */
