/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        sqlite3_config_oneos.h
 *
 * @brief       oneos config for sqlite3.
 *
 * @revision
 * Date         Author          Notes
 * 2021-12-13   OneOS team      First Version
 ***********************************************************************************************************************
 */

#ifndef _SQLITE3_CONFIG_ONEOS_H_
#define _SQLITE3_CONFIG_ONEOS_H_

#define SQLITE_MINIMUM_FILE_DESCRIPTOR 0
#define SQLITE_OMIT_LOAD_EXTENSION     0
#define SQLITE_OMIT_AUTOINIT
#define SQLITE_OMIT_WAL   1
#define SQLITE_TEMP_STORE 1
#define SQLITE_THREADSAFE 1
#define NDEBUG
#define SQLITE_OS_OTHER 1
#define SQLITE_OS_ONEOS 1

#endif /* _SQLITE3_CONFIG_ONEOS_H_ */
