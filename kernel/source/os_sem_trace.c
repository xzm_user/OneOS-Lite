/**
 ***********************************************************************************************************************
 * Copyright (c) 2022, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        os_sys_trace.c
 *
 * @brief       This file implements the semaphore functions.
 *
 * @revision
 * Date         Author          Notes
 * 2022-03-22   OneOS team      First Version
 ***********************************************************************************************************************
 */
#include <os_stddef.h>
#include <os_errno.h>
#include <os_types.h>
#include <os_ipc_hook.h>
#include "os_kernel_internal.h"
#include <os_sem.h>
#include <os_ipc_trace.h>
#include <shell.h>

#include <os_sem_trace.h>

#ifdef OS_USING_SEM_TRACE

#ifndef OS_SEM_TRACE_NUM
#define OS_SEM_TRACE_NUM 5
#endif

#if defined(OS_USING_SHELL)
/* this description is for shell show sem trace */
#define SEM_TRACE_DESC_NUM 6
static const char *gs_sem_trace_desc[SEM_TRACE_DESC_NUM] =
    {"Post(Success)", "Wait(Success)", "Wait(Failure)", "Wait(Blocking)", "Post(Full)", "Post(Awaking)"};
#endif

static os_ipc_trace_t gs_os_sem_trace[OS_SEM_TRACE_NUM];

static void os_sem_trace_hook(os_sem_t *sem, void *priv, os_ubase_t len)
{
    os_ubase_t          i = 0;
    os_ipc_trace_item_t trace_data;

    if ((OS_NULL == sem) || (OS_KOBJ_INITED != sem->object_inited))
    {
        return;
    }

    if (OS_NULL != priv)
    {
        trace_data.desc = *(os_ubase_t *)priv;
    }
    trace_data.task = os_task_self();
    trace_data.irq  = os_irq_num();
    trace_data.irq |= ((os_is_irq_active() ? 1 : 0) << 31);    // TODO: adjust in aarch64

    for (i = 0; i < OS_SEM_TRACE_NUM; i++)
    {
        ipc_trace_hook(&gs_os_sem_trace[i], (os_ubase_t)sem, &trace_data);
    }
}

#ifdef OS_SEM_WAIT_HOOK

void os_sem_wait_success_hook(os_sem_t *sem)
{
    if ((OS_NULL == sem) || (OS_KOBJ_INITED != sem->object_inited))
    {
        return;
    }

    os_ubase_t desc = 1;
    os_sem_trace_hook(sem, &desc, sizeof(os_ubase_t));
}

void os_sem_wait_fail_hook(os_sem_t *sem)
{
    if ((OS_NULL == sem) || (OS_KOBJ_INITED != sem->object_inited))
    {
        return;
    }

    os_ubase_t desc = 2;
    os_sem_trace_hook(sem, &desc, sizeof(os_ubase_t));
}

void os_sem_wait_block_hook(os_sem_t *sem)
{
    if ((OS_NULL == sem) || (OS_KOBJ_INITED != sem->object_inited))
    {
        return;
    }

    os_ubase_t desc = 3;
    os_sem_trace_hook(sem, &desc, sizeof(os_ubase_t));
}

OS_IPC_HOOK_DEFINE(sem, wait, success, os_sem_t *);
OS_IPC_HOOK_DEFINE(sem, wait, fail, os_sem_t *);
OS_IPC_HOOK_DEFINE(sem, wait, block, os_sem_t *);

#endif

#ifdef OS_SEM_POST_HOOK

void os_sem_post_success_hook(os_sem_t *sem)
{
    if ((OS_NULL == sem) || (OS_KOBJ_INITED != sem->object_inited))
    {
        return;
    }

    os_ubase_t desc = 0;
    os_sem_trace_hook(sem, &desc, sizeof(os_ubase_t));
}

void os_sem_post_full_hook(os_sem_t *sem)
{
    if ((OS_NULL == sem) || (OS_KOBJ_INITED != sem->object_inited))
    {
        return;
    }

    os_ubase_t desc = 4;
    os_sem_trace_hook(sem, &desc, sizeof(os_ubase_t));
}

void os_sem_post_awake_hook(os_sem_t *sem)
{
    if ((OS_NULL == sem) || (OS_KOBJ_INITED != sem->object_inited))
    {
        return;
    }

    os_ubase_t desc = 5;
    os_sem_trace_hook(sem, &desc, sizeof(os_ubase_t));
}

OS_IPC_HOOK_DEFINE(sem, post, success, os_sem_t *);
OS_IPC_HOOK_DEFINE(sem, post, full, os_sem_t *);
OS_IPC_HOOK_DEFINE(sem, post, wake, os_sem_t *);

#endif

os_err_t os_sem_hook_init(void)
{
#ifdef OS_SEM_WAIT_HOOK
    OS_IPC_HOOK_ADD(sem, wait, success, os_sem_wait_success_hook);
    OS_IPC_HOOK_ADD(sem, wait, fail, os_sem_wait_fail_hook);
    OS_IPC_HOOK_ADD(sem, wait, block, os_sem_wait_block_hook);
#endif

#ifdef OS_SEM_POST_HOOK
    OS_IPC_HOOK_ADD(sem, post, success, os_sem_post_success_hook);
    OS_IPC_HOOK_ADD(sem, post, full, os_sem_post_full_hook);
    OS_IPC_HOOK_ADD(sem, post, wake, os_sem_post_awake_hook);
#endif

    return OS_EOK;
}
OS_PREV_INIT(os_sem_hook_init, OS_INIT_SUBLEVEL_MIDDLE);

#if defined(OS_USING_SHELL) && defined(OS_USING_SEM_TRACE) && defined(OS_USING_IPC_TRACE)
#include <stdio.h>

static os_err_t os_sem_trace_add(os_int32_t argc, char **argv)
{
    os_ubase_t i       = 0;
    os_ubase_t ipc_obj = 0;

    if ((argc < 2) || (OS_NULL == argv[1]))
    {
        os_kprintf("Sem Trace : Invalid Parameter\r\n");
        return OS_EINVAL;
    }

    ipc_obj = strtoul(argv[1], 0, 0);

    if (!os_sem_is_exist((os_sem_t *)ipc_obj))
    {
        return OS_EINVAL;
    }

    for (i = 0; i < OS_SEM_TRACE_NUM; i++)
    {
        if (gs_os_sem_trace[i].ipc_obj == ipc_obj)
        {
            os_kprintf("Sem Trace : Trace Already Exist\r\n");
            return OS_ERROR;
        }

        if (!gs_os_sem_trace[i].ipc_obj)
        {
            break;
        }
    }

    if (i >= OS_SEM_TRACE_NUM)
    {
        os_kprintf("Sem Trace : Trace Full\r\n");
        return OS_ERROR;
    }

    ipc_trace_rec_init(&gs_os_sem_trace[i]);
    ipc_trace_rec_add_kobj(&gs_os_sem_trace[i], ipc_obj);

    return OS_EOK;
}
SH_CMD_EXPORT(sem_trace_add, os_sem_trace_add, "Add Sem trace");

static os_err_t os_sem_trace_del(os_int32_t argc, char **argv)
{
    os_ubase_t i       = 0;
    os_ubase_t ipc_obj = 0;

    if (argc < 2)
    {
        os_kprintf("Sem Trace : Invalid Parameter\r\n");
        return OS_EINVAL;
    }

    ipc_obj = strtoul(argv[1], 0, 0);

    if (!ipc_obj)
    {
        os_kprintf("Sem Trace : Invalid Kobject\r\n");
        return OS_ERROR;
    }

    for (i = 0; i < OS_SEM_TRACE_NUM; i++)
    {
        if (gs_os_sem_trace[i].ipc_obj == ipc_obj)
        {
            ipc_trace_rec_del_kobj(&gs_os_sem_trace[i]);
            return OS_EOK;
        }
    }

    os_kprintf("Sem Trace : Invalid Kobject\r\n");
    return OS_ERROR;
}
SH_CMD_EXPORT(sem_trace_del, os_sem_trace_del, "Del Sem trace");

static void os_sem_trace_show_one(os_ipc_trace_t *ipct)
{
    os_ubase_t          col = 0;
    os_ipc_trace_item_t trace_data;

    if (OS_NULL == ipct)
    {
        os_kprintf("Sem TRACE : Invalid Parameter\r\n");
        return;
    }

    if (!ipct->ipc_obj)
        return;

    os_kprintf("Kobj : 0x%x | Name : %s | Current Record:%d\r\n",
               ipct->ipc_obj,
               ((os_sem_t *)ipct->ipc_obj)->name,
               ipct->trace.valid_cnt);

    while (OS_ERROR != ipc_trace_rec_dequeue(ipct, &trace_data))
    {
        col = trace_data.desc % SEM_TRACE_DESC_NUM;
        if (trace_data.irq & (1ul << 31))
        {
            os_kprintf("%-20s  -  IRQ(%d)\r\n", gs_sem_trace_desc[col], trace_data.irq & ~(1ul << 31));
        }
        else
        {
            os_kprintf("%-20s  -  %s(0x%x)\r\n", gs_sem_trace_desc[col], trace_data.task->name, trace_data.task);
        }
    }
}

static os_err_t os_sem_trace_show(os_int32_t argc, char **argv)
{
    os_ubase_t i = 0;

    for (i = 0; i < OS_SEM_TRACE_NUM; i++)
    {
        os_sem_trace_show_one(&gs_os_sem_trace[i]);
    }
    return OS_EOK;
}
SH_CMD_EXPORT(sem_trace_show, os_sem_trace_show, "Show Sem trace");

#endif /* OS_USING_SEMAPHORE */

#endif
