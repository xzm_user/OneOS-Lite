/* SPDX-License-Identifier: (GPL-2.0+ OR BSD-3-Clause) */
/*
 * Copyright (C) STMicroelectronics 2021 - All Rights Reserved
 * Author: STM32CubeMX code generation for STMicroelectronics.
 */

/* For more information on Device Tree configuration, please refer to
 * https://wiki.st.com/stm32mpu/wiki/Category:Device_tree_configuration
 */

/dts-v1/;
#include <dt-bindings/pinctrl/stm32-pinfunc.h>

#include "stm32mp157.dtsi"
#include "stm32mp15xd.dtsi"
#include "stm32mp15xxaa-pinctrl.dtsi"
#include "stm32mp15-m4-srm.dtsi"

/* USER CODE BEGIN includes */
/* USER CODE END includes */

/ {
	model = "STMicroelectronics custom STM32CubeMX board - openstlinux-5.10-dunfell-mp1-21-03-31";
	compatible = "st,stm32mp157d-cubemx_config-mx", "st,stm32mp157";

	memory@c0000000 {
		device_type = "memory";
		reg = <0xc0000000 0x20000000>;

		/* USER CODE BEGIN memory */
		/* USER CODE END memory */
	};

	reserved-memory {
		#address-cells = <1>;
		#size-cells = <1>;
		ranges;

		/* USER CODE BEGIN reserved-memory */
		/* USER CODE END reserved-memory */
	};

	/* USER CODE BEGIN root */
	/* USER CODE END root */

	clocks {
		/* USER CODE BEGIN clocks */
		/* USER CODE END clocks */

#ifndef CONFIG_TFABOOT
		clk_lsi: clk-lsi {
			clock-frequency = <32000>;
		};
		clk_hsi: clk-hsi {
			clock-frequency = <64000000>;
		};
		clk_csi: clk-csi {
			clock-frequency = <4000000>;
		};
		clk_lse: clk-lse {
			clock-frequency = <32768>;
		};
		clk_hse: clk-hse {
			clock-frequency = <24000000>;
		};
#endif	/*CONFIG_TFABOOT*/
	};

}; /*root*/

&pinctrl {
	u-boot,dm-pre-reloc;

	eth1_pins_mx: eth1_mx-0 {
		pins {
			pinmux = <STM32_PINMUX('G', 5, AF11)>; /* ETH1_CLK125 */
			bias-disable;
			drive-push-pull;
			slew-rate = <2>;
		};
	};

	eth1_sleep_pins_mx: eth1_sleep_mx-0 {
		pins {
			pinmux = <STM32_PINMUX('G', 5, ANALOG)>; /* ETH1_CLK125 */
		};
	};

	hdmi_cec_pins_mx: hdmi_cec_mx-0 {
		pins {
			pinmux = <STM32_PINMUX('B', 6, AF5)>; /* CEC */
			bias-disable;
			drive-open-drain;
			slew-rate = <0>;
		};
	};

	hdmi_cec_sleep_pins_mx: hdmi_cec_sleep_mx-0 {
		pins {
			pinmux = <STM32_PINMUX('B', 6, ANALOG)>; /* CEC */
		};
	};

	i2c1_pins_mx: i2c1_mx-0 {
		pins {
			pinmux = <STM32_PINMUX('D', 12, AF5)>, /* I2C1_SCL */
					 <STM32_PINMUX('F', 15, AF5)>; /* I2C1_SDA */
			bias-disable;
			drive-open-drain;
			slew-rate = <0>;
		};
	};

	i2c1_sleep_pins_mx: i2c1_sleep_mx-0 {
		pins {
			pinmux = <STM32_PINMUX('D', 12, ANALOG)>, /* I2C1_SCL */
					 <STM32_PINMUX('F', 15, ANALOG)>; /* I2C1_SDA */
		};
	};

	m4_adc_pins_mx: m4_adc_mx-0 {
		pins {
			pinmux = <STM32_PINMUX('A', 5, ANALOG)>; /* ADC1_INP19 */
		};
	};

	m4_dac1_pins_mx: m4_dac1_mx-0 {
		pins {
			pinmux = <STM32_PINMUX('A', 4, ANALOG)>; /* DAC1_OUT1 */
		};
	};

	m4_i2c5_pins_mx: m4_i2c5_mx-0 {
		pins {
			pinmux = <STM32_PINMUX('A', 11, RSVD)>, /* I2C5_SCL */
					 <STM32_PINMUX('A', 12, RSVD)>; /* I2C5_SDA */
		};
	};

	m4_tim8_pwm_pins_mx: m4_tim8_pwm_mx-0 {
		pins {
			pinmux = <STM32_PINMUX('C', 7, RSVD)>; /* TIM8_CH2 */
		};
	};

	m4_uart5_pins_mx: m4_uart5_mx-0 {
		pins {
			pinmux = <STM32_PINMUX('B', 12, RSVD)>, /* UART5_RX */
					 <STM32_PINMUX('B', 13, RSVD)>; /* UART5_TX */
		};
	};

	m4_usart3_pins_mx: m4_usart3_mx-0 {
		pins {
			pinmux = <STM32_PINMUX('D', 8, RSVD)>, /* USART3_TX */
					 <STM32_PINMUX('D', 9, RSVD)>; /* USART3_RX */
		};
	};

	sai2a_pins_mx: sai2a_mx-0 {
		pins {
			pinmux = <STM32_PINMUX('E', 0, AF10)>, /* SAI2_MCLK_A */
					 <STM32_PINMUX('I', 5, AF10)>, /* SAI2_SCK_A */
					 <STM32_PINMUX('I', 6, AF10)>, /* SAI2_SD_A */
					 <STM32_PINMUX('I', 7, AF10)>; /* SAI2_FS_A */
			bias-disable;
			drive-push-pull;
			slew-rate = <0>;
		};
	};

	sai2a_sleep_pins_mx: sai2a_sleep_mx-0 {
		pins {
			pinmux = <STM32_PINMUX('E', 0, ANALOG)>, /* SAI2_MCLK_A */
					 <STM32_PINMUX('I', 5, ANALOG)>, /* SAI2_SCK_A */
					 <STM32_PINMUX('I', 6, ANALOG)>, /* SAI2_SD_A */
					 <STM32_PINMUX('I', 7, ANALOG)>; /* SAI2_FS_A */
		};
	};

	sai2b_pins_mx: sai2b_mx-0 {
		pins {
			pinmux = <STM32_PINMUX('E', 11, AF10)>; /* SAI2_SD_B */
			bias-disable;
			drive-push-pull;
			slew-rate = <0>;
		};
	};

	sai2b_sleep_pins_mx: sai2b_sleep_mx-0 {
		pins {
			pinmux = <STM32_PINMUX('E', 11, ANALOG)>; /* SAI2_SD_B */
		};
	};

	/* USER CODE BEGIN pinctrl */
	/* USER CODE END pinctrl */
};

&pinctrl_z {
	u-boot,dm-pre-reloc;

	i2c4_pins_z_mx: i2c4_mx-0 {
		u-boot,dm-pre-reloc;
		pins {
			u-boot,dm-pre-reloc;
			pinmux = <STM32_PINMUX('Z', 4, AF6)>, /* I2C4_SCL */
					 <STM32_PINMUX('Z', 5, AF6)>; /* I2C4_SDA */
			bias-disable;
			drive-open-drain;
			slew-rate = <0>;
		};
	};

	i2c4_sleep_pins_z_mx: i2c4_sleep_mx-0 {
		u-boot,dm-pre-reloc;
		pins {
			u-boot,dm-pre-reloc;
			pinmux = <STM32_PINMUX('Z', 4, ANALOG)>, /* I2C4_SCL */
					 <STM32_PINMUX('Z', 5, ANALOG)>; /* I2C4_SDA */
		};
	};

	m4_spi1_pins_z_mx: m4_spi1_mx-0 {
		pins {
			pinmux = <STM32_PINMUX('Z', 0, RSVD)>, /* SPI1_SCK */
					 <STM32_PINMUX('Z', 1, RSVD)>, /* SPI1_MISO */
					 <STM32_PINMUX('Z', 2, RSVD)>; /* SPI1_MOSI */
		};
	};

	/* USER CODE BEGIN pinctrl_z */
	/* USER CODE END pinctrl_z */
};

&m4_rproc{
	/*Restriction: "memory-region" property is not managed - please to use User-Section if needed*/
	mboxes = <&ipcc 2>;
	mbox-names = "shutdown";
	status = "okay";

	/* USER CODE BEGIN m4_rproc */
	/* USER CODE END m4_rproc */

	m4_system_resources{
		status = "okay";

		/* USER CODE BEGIN m4_system_resources */
		/* USER CODE END m4_system_resources */
	};
};

&bsec{
	status = "okay";

	/* USER CODE BEGIN bsec */
	/* USER CODE END bsec */
};

&cec{
	pinctrl-names = "default", "sleep";
	pinctrl-0 = <&hdmi_cec_pins_mx>;
	pinctrl-1 = <&hdmi_cec_sleep_pins_mx>;
	status = "okay";

	/* USER CODE BEGIN cec */
	/* USER CODE END cec */
};

&crc1{
	status = "okay";

	/* USER CODE BEGIN crc1 */
	/* USER CODE END crc1 */
};

&dma1{
	status = "okay";

	/* USER CODE BEGIN dma1 */
	/* USER CODE END dma1 */
};

&dma2{
	status = "disabled";

	/* USER CODE BEGIN dma2 */
	/* USER CODE END dma2 */
};

&dmamux1{

	dma-masters = <&dma1>;
	dma-channels = <8>;

	status = "okay";

	/* USER CODE BEGIN dmamux1 */
	/* USER CODE END dmamux1 */
};

&dts{
	status = "okay";

	/* USER CODE BEGIN dts */
	/* USER CODE END dts */
};

&ethernet0{
	pinctrl-names = "default", "sleep";
	pinctrl-0 = <&eth1_pins_mx>;
	pinctrl-1 = <&eth1_sleep_pins_mx>;
	status = "okay";

	/* USER CODE BEGIN ethernet0 */
	/* USER CODE END ethernet0 */
};

&gpu{
	status = "okay";

	/* USER CODE BEGIN gpu */
	/* USER CODE END gpu */
};

&hash1{
	u-boot,dm-pre-reloc;
	status = "okay";

	/* USER CODE BEGIN hash1 */
	/* USER CODE END hash1 */
};

&hsem{
	status = "okay";

	/* USER CODE BEGIN hsem */
	/* USER CODE END hsem */
};

&i2c1{
	pinctrl-names = "default", "sleep";
	pinctrl-0 = <&i2c1_pins_mx>;
	pinctrl-1 = <&i2c1_sleep_pins_mx>;
	status = "okay";

	/* USER CODE BEGIN i2c1 */
	/* USER CODE END i2c1 */
};

&i2c4{
	u-boot,dm-pre-reloc;
	pinctrl-names = "default", "sleep";
	pinctrl-0 = <&i2c4_pins_z_mx>;
	pinctrl-1 = <&i2c4_sleep_pins_z_mx>;
	status = "okay";

	/* USER CODE BEGIN i2c4 */
	/* USER CODE END i2c4 */
};

&ipcc{
	status = "okay";

	/* USER CODE BEGIN ipcc */
	/* USER CODE END ipcc */
};

&iwdg2{
	status = "okay";

	/* USER CODE BEGIN iwdg2 */
	/* USER CODE END iwdg2 */
};

&m4_adc{
	pinctrl-names = "default";
	pinctrl-0 = <&m4_adc_pins_mx>;
	status = "okay";

	/* USER CODE BEGIN m4_adc */
	/* USER CODE END m4_adc */
};

&m4_dac{
	pinctrl-names = "default";
	pinctrl-0 = <&m4_dac1_pins_mx>;
	status = "okay";

	/* USER CODE BEGIN m4_dac */
	/* USER CODE END m4_dac */
};

&m4_dma2{
	status = "okay";

	/* USER CODE BEGIN m4_dma2 */
	/* USER CODE END m4_dma2 */
};

&m4_i2c5{
	pinctrl-names = "default";
	pinctrl-0 = <&m4_i2c5_pins_mx>;
	status = "okay";

	/* USER CODE BEGIN m4_i2c5 */
	/* USER CODE END m4_i2c5 */
};

&m4_spi1{
	pinctrl-names = "default";
	pinctrl-0 = <&m4_spi1_pins_z_mx>;
	status = "okay";

	/* USER CODE BEGIN m4_spi1 */
	/* USER CODE END m4_spi1 */
};

&m4_timers1{
	status = "okay";

	/* USER CODE BEGIN m4_timers1 */
	/* USER CODE END m4_timers1 */
};

&m4_timers2{
	status = "okay";

	/* USER CODE BEGIN m4_timers2 */
	/* USER CODE END m4_timers2 */
};

&m4_timers6{
	status = "okay";

	/* USER CODE BEGIN m4_timers6 */
	/* USER CODE END m4_timers6 */
};

&m4_timers7{
	status = "okay";

	/* USER CODE BEGIN m4_timers7 */
	/* USER CODE END m4_timers7 */
};

&m4_timers8{
	pinctrl-names = "default";
	pinctrl-0 = <&m4_tim8_pwm_pins_mx>;
	status = "okay";

	/* USER CODE BEGIN m4_timers8 */
	/* USER CODE END m4_timers8 */
};

&m4_uart5{
	pinctrl-names = "default";
	pinctrl-0 = <&m4_uart5_pins_mx>;
	status = "okay";

	/* USER CODE BEGIN m4_uart5 */
	/* USER CODE END m4_uart5 */
};

&m4_usart3{
	pinctrl-names = "default";
	pinctrl-0 = <&m4_usart3_pins_mx>;
	status = "okay";

	/* USER CODE BEGIN m4_usart3 */
	/* USER CODE END m4_usart3 */
};

&mdma1{
	status = "okay";

	/* USER CODE BEGIN mdma1 */
	/* USER CODE END mdma1 */
};

&pwr_regulators{
	status = "okay";

	/* USER CODE BEGIN pwr_regulators */
	/* USER CODE END pwr_regulators */
};

&rcc{
	u-boot,dm-pre-reloc;
	status = "okay";

	/* USER CODE BEGIN rcc */
	/* USER CODE END rcc */
};

&rng1{
	status = "okay";

	/* USER CODE BEGIN rng1 */
	/* USER CODE END rng1 */
};

&rtc{
	status = "okay";

	/* USER CODE BEGIN rtc */
	/* USER CODE END rtc */
};

&sai2{
	pinctrl-names = "default", "sleep";
	pinctrl-0 = <&sai2a_pins_mx>, <&sai2b_pins_mx>;
	pinctrl-1 = <&sai2a_sleep_pins_mx>, <&sai2b_sleep_pins_mx>;
	status = "okay";

	/* USER CODE BEGIN sai2 */
	/* USER CODE END sai2 */

	sai2a:audio-controller@4400b004{
		status = "okay";

		/* USER CODE BEGIN sai2a */
		/* USER CODE END sai2a */
	};

	sai2b:audio-controller@4400b024{
		status = "okay";

		/* USER CODE BEGIN sai2b */
		/* USER CODE END sai2b */
	};
};

&tamp{
	status = "okay";

	/* USER CODE BEGIN tamp */
	/* USER CODE END tamp */
};

&usbh_ehci{
	status = "okay";

	/* USER CODE BEGIN usbh_ehci */
	/* USER CODE END usbh_ehci */
};

&usbh_ohci{
	status = "okay";

	/* USER CODE BEGIN usbh_ohci */
	/* USER CODE END usbh_ohci */
};

&usbotg_hs{
	u-boot,dm-pre-reloc;
	status = "okay";

	/* USER CODE BEGIN usbotg_hs */
	/* USER CODE END usbotg_hs */
};

&usbphyc{
	u-boot,dm-pre-reloc;
	status = "okay";

	/* USER CODE BEGIN usbphyc */
	/* USER CODE END usbphyc */
};

&usbphyc_port0{
	u-boot,dm-pre-reloc;
	status = "okay";

	/* USER CODE BEGIN usbphyc_port0 */
	/* USER CODE END usbphyc_port0 */
};

&usbphyc_port1{
	u-boot,dm-pre-reloc;
	status = "okay";

	/* USER CODE BEGIN usbphyc_port1 */
	/* USER CODE END usbphyc_port1 */
};

&vrefbuf{
	status = "okay";

	/* USER CODE BEGIN vrefbuf */
	/* USER CODE END vrefbuf */
};

/* USER CODE BEGIN addons */
/* USER CODE END addons */

