#include "oneos_config.h"
#include <driver.h>
#include <bus/bus.h>

#include "apm32_hal.h"

#ifdef BSP_USING_USART
#include "drv_usart.h"
const struct apm32_usart_info usart1_info = {
    .tx_pin_port = GPIOA,
    .tx_pin_info =
        {
            .pin   = GPIO_PIN_9,
            .speed = GPIO_SPEED_50MHz,
            .mode  = GPIO_MODE_AF,
        },
    .tx_gpioPinSource = GPIO_PIN_SOURCE_9,
    .tx_gpioAf        = GPIO_AF_USART1,

    .rx_pin_port = GPIOA,
    .rx_pin_info =
        {
            .pin   = GPIO_PIN_10,
            .speed = GPIO_SPEED_50MHz,
            .mode  = GPIO_MODE_AF,
        },
    .rx_gpioPinSource = GPIO_PIN_SOURCE_10,
    .rx_gpioAf        = GPIO_AF_USART1,

    .gpio_PeriphClock  = RCM_EnableAHB1PeriphClock,
    .gpio_Periph       = RCM_AHB1_PERIPH_GPIOA,
    .usart_PeriphClock = RCM_EnableAPB2PeriphClock,
    .usart_Periph      = RCM_APB2_PERIPH_USART1,

    .usart_def_cfg =
        {
            .baudRate     = 115200,
            .wordLength   = USART_WORD_LEN_8B,
            .stopBits     = USART_STOP_BIT_1,
            .parity       = USART_PARITY_NONE,
            .mode         = USART_MODE_TX_RX,
            .hardwareFlow = USART_HARDWARE_FLOW_NONE,
        },

    .husart     = USART1,
    .irq_type   = USART1_IRQn,
    .irq_PrePri = 1,
    .irq_SubPri = 0,
};

OS_HAL_DEVICE_DEFINE("USART_HandleTypeDef", "usart1", usart1_info);

const struct apm32_usart_info usart2_info = {
    .tx_pin_port = GPIOA,
    .tx_pin_info =
        {
            .pin   = GPIO_PIN_2,
            .speed = GPIO_SPEED_50MHz,
            .mode  = GPIO_MODE_AF,
        },
    .tx_gpioPinSource = GPIO_PIN_SOURCE_2,
    .tx_gpioAf        = GPIO_AF_USART2,

    .rx_pin_port = GPIOA,
    .rx_pin_info =
        {
            .pin   = GPIO_PIN_3,
            .speed = GPIO_SPEED_50MHz,
            .mode  = GPIO_MODE_AF,
        },
    .rx_gpioPinSource = GPIO_PIN_SOURCE_3,
    .rx_gpioAf        = GPIO_AF_USART2,

    .gpio_PeriphClock  = RCM_EnableAHB1PeriphClock,
    .gpio_Periph       = RCM_AHB1_PERIPH_GPIOA,
    .usart_PeriphClock = RCM_EnableAPB1PeriphClock,
    .usart_Periph      = RCM_APB1_PERIPH_USART2,

    .usart_def_cfg =
        {
            .baudRate     = 115200,
            .wordLength   = USART_WORD_LEN_8B,
            .stopBits     = USART_STOP_BIT_1,
            .parity       = USART_PARITY_NONE,
            .mode         = USART_MODE_TX_RX,
            .hardwareFlow = USART_HARDWARE_FLOW_NONE,
        },

    .husart     = USART2,
    .irq_type   = USART2_IRQn,
    .irq_PrePri = 1,
    .irq_SubPri = 0,
};

OS_HAL_DEVICE_DEFINE("USART_HandleTypeDef", "usart2", usart2_info);
#endif
