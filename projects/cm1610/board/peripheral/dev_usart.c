/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        dev_usart.c
 *
 * @brief       This file implements usart driver for cm32
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include "drv_usart.h"

#ifdef BSP_USING_UARTA
struct cm32_usart_info uart1_info = {
    .uart  = UARTA,
    .irqno = uart_handler_IRQn,

    .gpio_port        = GPIOD,
    .gpio_pin_mode_tx = GPIO_MODE_UART_TXD,
    .gpio_pin_mode_rx = GPIO_MODE_UART_RXD,
    .gpio_pin_tx      = GPIO_Pin_0,
    .gpio_pin_rx      = GPIO_Pin_1,

    .dma_support = 0,
};
OS_HAL_DEVICE_DEFINE("Usart_Type", "uarta", uart1_info);
#endif

#ifdef BSP_USING_UARTB
struct cm32_usart_info uart2_info = {
    .uart  = UARTB,
    .irqno = uartb_handler_IRQn,

    .gpio_port        = GPIOA,
    .gpio_pin_mode_tx = GPIO_MODE_UARTB_TXD,
    .gpio_pin_mode_rx = GPIO_MODE_UARTB_RXD,
    .gpio_pin_tx      = GPIO_Pin_5,
    .gpio_pin_rx      = GPIO_Pin_7,

    .dma_support = 0,
};
OS_HAL_DEVICE_DEFINE("Usart_Type", "uartb", uart2_info);
#endif
