/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with 
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on 
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the 
 * specific language governing permissions and limitations under the License.
 *
 * @file        memset.c
 *
 * @brief       This file provides C library function memset.
 *
 * @revision
 * Date         Author          Notes
 * 2020-10-20   OneOS Team      First version.
 ***********************************************************************************************************************
 */

#include <string.h>

/**
 ***********************************************************************************************************************
 * @brief           This function will set the content of memory to specified value.
 *
 * @param[out]      buff            Pointer to the start of the buffer.
 * @param[in]       val             The value to fill the buffer with
 * @param[in]       count           The size of the buffer.
 *
 * @return          The address of the buffer filled with specified value.
 ***********************************************************************************************************************
 */
void *__wrap_memset(void *dst, int val, unsigned long count)
{
    unsigned char *dst_buf;

    dst_buf = (unsigned char *)dst;

    while (count--)
    {
        *dst_buf = (unsigned char)val;
        dst_buf++;
    }

    return dst;
}

