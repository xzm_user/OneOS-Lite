
#include <os_types.h>
#include <os_stddef.h>
#include <stdlib.h>


int atoi (const char *s)
{
  return (int) strtol (s, OS_NULL, 10);
}

