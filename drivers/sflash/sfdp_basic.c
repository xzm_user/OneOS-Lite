/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        sfdp_basic.c
 *
 * @brief       This file provides functions for sfdp basic.
 *
 * @revision
 * Date         Author          Notes
 * 2021-11-11   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include <device.h>
#include <os_memory.h>
#include <os_errno.h>
#include <arch_interrupt.h>
#include <os_task.h>
#include <os_util.h>
#include <os_assert.h>
#include <string.h>
#include <driver.h>
#include <pin.h>

#include "sflash.h"
#include "sfdp.h"
#include "sfdp_basic.h"

#ifndef OS_SFLASH_SUPPORTED_CMDS_NR
#define OS_SFLASH_SUPPORTED_CMDS_NR 20
#endif

#define BIT_SHIFT(_val, _shift, _bits) ((_val >> _shift) & ((1 << _bits) - 1))

static void os_sfdp_append_sflash_info(struct os_sflash_info *info, struct os_xspi_message_cfg *cfg)
{
    OS_ASSERT(info->supported_cmds != OS_NULL);
    OS_ASSERT(info->supported_cmds_nr < OS_SFLASH_SUPPORTED_CMDS_NR);

    ((struct os_xspi_message_cfg *)(info->supported_cmds))[info->supported_cmds_nr++] = *cfg;
}

static void os_sfdp_decode_basic_capacity(struct os_sfdp *sfdp, struct os_sfdp_param *param)
{
    os_uint32_t *table = param->table;

    os_uint32_t capacity = table[2 - 1];

    if (capacity & (1UL << 31))
    {
        sfdp->info.capacity = (1 << BIT_SHIFT(capacity, 0, 30)) / 8;
    }
    else
    {
        sfdp->info.capacity = (capacity + 1) / 8;
    }

    OS_SFLASH_LOG("capacity: %dMB\r\n", sfdp->info.capacity / 1024 / 1024);
}

static void os_sfdp_decode_basic_addr_size(struct os_sfdp *sfdp, struct os_sfdp_param *param)
{
    os_uint32_t *table = param->table;

    int addr_bytes = BIT_SHIFT(table[1 - 1], 17, 2);

    if (addr_bytes == 0)
    {
        sfdp->info.addr_bytes = 3;
    }
    else if (addr_bytes == 1)
    {
        sfdp->info.addr_bytes = 4;
    }
    else if (addr_bytes == 2)
    {
        sfdp->info.addr_bytes = 4;
    }
}

static void os_sfdp_decode_basic_status_busy(struct os_sfdp *sfdp, struct os_sfdp_param *param)
{
    os_uint32_t *table = param->table;

    if (param->head.length < 14)
        return;

    if ((BIT_SHIFT(table[14 - 1], 2, 6) & (1 << 1)) == 0)
        return;

    struct os_xspi_message_cfg cfg = {
        .type              = OS_XSPI_MESSAGE_CFG_TYPE_STATUS,
        .instruction       = 0x70,
        .instruction_lines = 1,
        .data_lines        = 1,
        .priv              = (7 << 4) | 0, /* busy bit, busy status */
    };

    os_sfdp_append_sflash_info(&sfdp->info, &cfg);
}

static void os_sfdp_decode_basic_read_114(struct os_sflash_info *info, os_uint32_t *table)
{
    if (BIT_SHIFT(table[1 - 1], 22, 1) == 0)
        return;

    os_uint8_t instruction  = BIT_SHIFT(table[3 - 1], 24, 8);
    os_uint8_t mode_clocks  = BIT_SHIFT(table[3 - 1], 21, 3);
    os_uint8_t dummy_clocks = BIT_SHIFT(table[3 - 1], 16, 5);

    struct os_xspi_message_cfg cfg = {
        .type              = OS_XSPI_MESSAGE_CFG_TYPE_READ,
        .instruction       = instruction,
        .instruction_lines = 1,
        .address_size      = info->addr_bytes,
        .address_lines     = 1,
        .dummy_cycles      = mode_clocks + dummy_clocks,
        .data_lines        = 4,
    };

    os_sfdp_append_sflash_info(info, &cfg);

    OS_SFLASH_LOG("support 114 fast read[%02x], dummy:%d\r\n", instruction, cfg.dummy_cycles);
}

static void os_sfdp_decode_basic_read_144(struct os_sflash_info *info, os_uint32_t *table)
{
    if (BIT_SHIFT(table[1 - 1], 21, 1) == 0)
        return;

    os_uint8_t instruction  = BIT_SHIFT(table[3 - 1], 8, 8);
    os_uint8_t mode_clocks  = BIT_SHIFT(table[3 - 1], 5, 3);
    os_uint8_t dummy_clocks = BIT_SHIFT(table[3 - 1], 0, 5);

    struct os_xspi_message_cfg cfg = {
        .type              = OS_XSPI_MESSAGE_CFG_TYPE_READ,
        .instruction       = instruction,
        .instruction_lines = 1,
        .address_size      = info->addr_bytes,
        .address_lines     = 4,
        .dummy_cycles      = mode_clocks + dummy_clocks,
        .data_lines        = 4,
    };

    os_sfdp_append_sflash_info(info, &cfg);

    OS_SFLASH_LOG("support 144 fast read[%02x], dummy:%d\r\n", instruction, cfg.dummy_cycles);
}

static void os_sfdp_decode_basic_read_122(struct os_sflash_info *info, os_uint32_t *table)
{
    if (BIT_SHIFT(table[1 - 1], 20, 1) == 0)
        return;

    os_uint8_t instruction  = BIT_SHIFT(table[4 - 1], 24, 8);
    os_uint8_t mode_clocks  = BIT_SHIFT(table[4 - 1], 21, 3);
    os_uint8_t dummy_clocks = BIT_SHIFT(table[4 - 1], 16, 5);

    struct os_xspi_message_cfg cfg = {
        .type              = OS_XSPI_MESSAGE_CFG_TYPE_READ,
        .instruction       = instruction,
        .instruction_lines = 1,
        .address_size      = info->addr_bytes,
        .address_lines     = 2,
        .dummy_cycles      = mode_clocks + dummy_clocks,
        .data_lines        = 2,
    };

    os_sfdp_append_sflash_info(info, &cfg);

    OS_SFLASH_LOG("support 122 fast read[%02x], dummy:%d\r\n", instruction, cfg.dummy_cycles);
}

static void os_sfdp_decode_basic_read_112(struct os_sflash_info *info, os_uint32_t *table)
{
    if (BIT_SHIFT(table[1 - 1], 16, 1) == 0)
        return;

    os_uint8_t instruction  = BIT_SHIFT(table[4 - 1], 8, 8);
    os_uint8_t mode_clocks  = BIT_SHIFT(table[4 - 1], 5, 3);
    os_uint8_t dummy_clocks = BIT_SHIFT(table[4 - 1], 0, 5);

    struct os_xspi_message_cfg cfg = {
        .type              = OS_XSPI_MESSAGE_CFG_TYPE_READ,
        .instruction       = instruction,
        .instruction_lines = 1,
        .address_size      = info->addr_bytes,
        .address_lines     = 1,
        .dummy_cycles      = mode_clocks + dummy_clocks,
        .data_lines        = 2,
    };

    os_sfdp_append_sflash_info(info, &cfg);

    OS_SFLASH_LOG("support 112 fast read[%02x], dummy:%d\r\n", instruction, cfg.dummy_cycles);
}

static void os_sfdp_decode_basic_read_444(struct os_sflash_info *info, os_uint32_t *table)
{
    if (BIT_SHIFT(table[5 - 1], 4, 1) == 0)
        return;

    os_uint8_t instruction  = BIT_SHIFT(table[7 - 1], 24, 8);
    os_uint8_t mode_clocks  = BIT_SHIFT(table[7 - 1], 21, 3);
    os_uint8_t dummy_clocks = BIT_SHIFT(table[7 - 1], 16, 5);

    struct os_xspi_message_cfg cfg = {
        .type              = OS_XSPI_MESSAGE_CFG_TYPE_READ,
        .instruction       = instruction,
        .instruction_lines = 4,
        .address_size      = info->addr_bytes,
        .address_lines     = 4,
        .dummy_cycles      = mode_clocks + dummy_clocks,
        .data_lines        = 4,
    };

    os_sfdp_append_sflash_info(info, &cfg);

    OS_SFLASH_LOG("support 444 fast read[%02x], dummy:%d\r\n", instruction, cfg.dummy_cycles);
}

static void os_sfdp_decode_basic_read_222(struct os_sflash_info *info, os_uint32_t *table)
{
    if (BIT_SHIFT(table[5 - 1], 0, 1) == 0)
        return;

    os_uint8_t instruction  = BIT_SHIFT(table[6 - 1], 24, 8);
    os_uint8_t mode_clocks  = BIT_SHIFT(table[6 - 1], 21, 3);
    os_uint8_t dummy_clocks = BIT_SHIFT(table[6 - 1], 16, 5);

    struct os_xspi_message_cfg cfg = {
        .type              = OS_XSPI_MESSAGE_CFG_TYPE_READ,
        .instruction       = instruction,
        .instruction_lines = 2,
        .address_size      = info->addr_bytes,
        .address_lines     = 2,
        .dummy_cycles      = mode_clocks + dummy_clocks,
        .data_lines        = 2,
    };

    os_sfdp_append_sflash_info(info, &cfg);

    OS_SFLASH_LOG("support 222 fast read[%02x], dummy:%d\r\n", instruction, cfg.dummy_cycles);
}

static void os_sfdp_decode_basic_read_118(struct os_sflash_info *info, os_uint32_t *table)
{
    if (BIT_SHIFT(table[17 - 1], 24, 8) == 0)
        return;

    os_uint8_t instruction  = BIT_SHIFT(table[17 - 1], 24, 8);
    os_uint8_t mode_clocks  = BIT_SHIFT(table[17 - 1], 21, 3);
    os_uint8_t dummy_clocks = BIT_SHIFT(table[17 - 1], 16, 5);

    struct os_xspi_message_cfg cfg = {
        .type              = OS_XSPI_MESSAGE_CFG_TYPE_READ,
        .instruction       = instruction,
        .instruction_lines = 1,
        .address_size      = info->addr_bytes,
        .address_lines     = 8,
        .dummy_cycles      = mode_clocks + dummy_clocks,
        .data_lines        = 8,
    };

    os_sfdp_append_sflash_info(info, &cfg);

    OS_SFLASH_LOG("support 118 fast read[%02x], dummy:%d\r\n", instruction, cfg.dummy_cycles);
}

static void os_sfdp_decode_basic_read_188(struct os_sflash_info *info, os_uint32_t *table)
{
    if (BIT_SHIFT(table[17 - 1], 8, 8) == 0)
        return;

    os_uint8_t instruction  = BIT_SHIFT(table[17 - 1], 8, 8);
    os_uint8_t mode_clocks  = BIT_SHIFT(table[17 - 1], 5, 3);
    os_uint8_t dummy_clocks = BIT_SHIFT(table[17 - 1], 0, 5);

    struct os_xspi_message_cfg cfg = {
        .type              = OS_XSPI_MESSAGE_CFG_TYPE_READ,
        .instruction       = instruction,
        .instruction_lines = 1,
        .address_size      = info->addr_bytes,
        .address_lines     = 1,
        .dummy_cycles      = mode_clocks + dummy_clocks,
        .data_lines        = 4,
    };

    os_sfdp_append_sflash_info(info, &cfg);

    OS_SFLASH_LOG("support 188 fast read[%02x], dummy:%d\r\n", instruction, cfg.dummy_cycles);
}

static void os_sfdp_decode_basic_read(struct os_sfdp *sfdp, struct os_sfdp_param *param)
{
    if (param->head.major != 1)
        return;

    /* basic 1.0 */
    os_sfdp_decode_basic_read_114(&sfdp->info, param->table);
    os_sfdp_decode_basic_read_144(&sfdp->info, param->table);
    os_sfdp_decode_basic_read_122(&sfdp->info, param->table);
    os_sfdp_decode_basic_read_112(&sfdp->info, param->table);
    os_sfdp_decode_basic_read_444(&sfdp->info, param->table);
    os_sfdp_decode_basic_read_222(&sfdp->info, param->table);

    if (param->head.minor < 5)
        return;

    /* basic 1.5 */

    if (param->head.length < 17)
        return;

    os_sfdp_decode_basic_read_118(&sfdp->info, param->table);
    os_sfdp_decode_basic_read_188(&sfdp->info, param->table);
}

static void os_sfdp_decode_basic_write(struct os_sfdp *sfdp, struct os_sfdp_param *param)
{
    if (param->head.length < 11)
        return;

    os_uint32_t *table = param->table;

    os_uint32_t page_size = 1 << BIT_SHIFT(table[11 - 1], 4, 4);

    sfdp->info.page_size = page_size;
}

static void os_sfdp_decode_basic_erase_type(struct os_sflash_info *info, os_uint8_t instruction, os_uint32_t erase_size)
{
    if (erase_size == 0)
        return;

    erase_size = 1 << erase_size;

    struct os_xspi_message_cfg cfg = {
        .type              = OS_XSPI_MESSAGE_CFG_TYPE_ERASE,
        .instruction       = instruction,
        .instruction_lines = 1,
        .address_size      = info->addr_bytes,
        .address_lines     = 1,
        .priv              = erase_size,
    };

    os_sfdp_append_sflash_info(info, &cfg);

    OS_SFLASH_LOG("erase type [%02x], size:0x%x\r\n", instruction, erase_size);
}

static void os_sfdp_decode_basic_erase(struct os_sfdp *sfdp, struct os_sfdp_param *param)
{
    os_uint32_t *table = param->table;

    os_sfdp_decode_basic_erase_type(&sfdp->info, BIT_SHIFT(table[8 - 1], 8, 8), BIT_SHIFT(table[8 - 1], 0, 8));
    os_sfdp_decode_basic_erase_type(&sfdp->info, BIT_SHIFT(table[8 - 1], 24, 8), BIT_SHIFT(table[8 - 1], 16, 8));
    os_sfdp_decode_basic_erase_type(&sfdp->info, BIT_SHIFT(table[9 - 1], 8, 8), BIT_SHIFT(table[9 - 1], 0, 8));
    os_sfdp_decode_basic_erase_type(&sfdp->info, BIT_SHIFT(table[9 - 1], 24, 8), BIT_SHIFT(table[9 - 1], 16, 8));
}

int os_sfdp_decode_basic(struct os_sfdp *sfdp, struct os_sfdp_param *param)
{
    OS_SFLASH_LOG("sfdp decode basic param...\r\n");
    OS_SFLASH_LOG("basic rev: %d.%d\r\n", param->head.major, param->head.minor);
    OS_SFLASH_LOG("basic len: %d DWORDS\r\n", param->head.length);

    sfdp->info.supported_cmds    = os_calloc(OS_SFLASH_SUPPORTED_CMDS_NR, sizeof(struct os_xspi_message_cfg));
    sfdp->info.supported_cmds_nr = 0;

    OS_ASSERT(sfdp->info.supported_cmds != OS_NULL);

    os_sfdp_decode_basic_capacity(sfdp, param);
    os_sfdp_decode_basic_addr_size(sfdp, param);
    os_sfdp_decode_basic_status_busy(sfdp, param);
    os_sfdp_decode_basic_read(sfdp, param);
    os_sfdp_decode_basic_write(sfdp, param);
    os_sfdp_decode_basic_erase(sfdp, param);

    return 0;
}

/* sfdp init flash */

static void os_sfdp_decode_basic_enter_4bytes_addressing(struct os_sflash     *sflash,
                                                         struct os_sfdp       *sfdp,
                                                         struct os_sfdp_param *param)
{
    /* decode 4 bytes addressing */
    if (param->head.length < 16)
    {
        /* enter 4 bytes addressing */
        struct os_xspi_message xmsg, *xmessage = &xmsg;

        sflash_write_unlock(sflash);

        memset(xmessage, 0, sizeof(struct os_xspi_message));

        xmessage_instruction(xmessage)       = 0xb7;
        xmessage_instruction_lines(xmessage) = 1;

        os_sfbus_transfer(sflash, xmessage);

        return;
    }

    os_uint32_t *table = param->table;

    OS_UNUSED os_uint32_t enter_4bytes_addr = BIT_SHIFT(table[16 - 1], 24, 8);
    OS_UNUSED os_uint32_t exit_4bytes_addr  = BIT_SHIFT(table[16 - 1], 14, 10);

    OS_SFLASH_LOG("4 bytes addressing: 0x%x, 0x%x\r\n", enter_4bytes_addr, exit_4bytes_addr);

    /* enter 4 bytes addressing */
    struct os_xspi_message xmsg, *xmessage = &xmsg;

    if (enter_4bytes_addr & 1)
    {
        sflash_write_unlock(sflash);

        memset(xmessage, 0, sizeof(struct os_xspi_message));

        xmessage_instruction(xmessage)       = 0xb7;
        xmessage_instruction_lines(xmessage) = 1;

        os_sfbus_transfer(sflash, xmessage);
    }
}

static void os_sfdp_decode_basic_qer(struct os_sflash *sflash, struct os_sfdp *sfdp, struct os_sfdp_param *param)
{
    /* decode qer */
    if (param->head.length < 15)
        return;

    os_uint32_t *table = param->table;

    os_uint32_t qer = BIT_SHIFT(table[15 - 1], 20, 3);

    OS_SFLASH_LOG("qer: 0x%x\r\n", qer);

    if (qer == 0)
        return;

    /* enable qer */
    struct os_xspi_message xmsg, *xmessage = &xmsg;

    os_uint8_t reg_value[2];

    if (qer == 4)
    {
        /* 1. read status reg1 */
        memset(xmessage, 0, sizeof(struct os_xspi_message));

        xmessage_instruction(xmessage)       = 0x05;
        xmessage_instruction_lines(xmessage) = 1;

        xmessage_data_lines(xmessage) = 1;
        xmessage_data_size(xmessage)  = 1;
        xmessage_data_dir(xmessage)   = OS_XSPI_DATA_DIR_DEVICE_TO_HOST;
        xmessage_data(xmessage)       = &reg_value[0];

        os_sfbus_transfer(sflash, xmessage);

        /* 2. read status reg2 */
        memset(xmessage, 0, sizeof(struct os_xspi_message));

        xmessage_instruction(xmessage)       = 0x35;
        xmessage_instruction_lines(xmessage) = 1;

        xmessage_data_lines(xmessage) = 1;
        xmessage_data_size(xmessage)  = 1;
        xmessage_data_dir(xmessage)   = OS_XSPI_DATA_DIR_DEVICE_TO_HOST;
        xmessage_data(xmessage)       = &reg_value[1];

        os_sfbus_transfer(sflash, xmessage);

        /* 3. modify status reg2 bit1 */
        reg_value[1] |= 1 << 1;

        /* 4. write status reg2 */
        memset(xmessage, 0, sizeof(struct os_xspi_message));

        xmessage_instruction(xmessage)       = 0x35;
        xmessage_instruction_lines(xmessage) = 1;

        xmessage_data_lines(xmessage) = 1;
        xmessage_data_size(xmessage)  = 2;
        xmessage_data_dir(xmessage)   = OS_XSPI_DATA_DIR_HOST_TO_DEVICE;
        xmessage_data(xmessage)       = reg_value;

        os_sfbus_transfer(sflash, xmessage);
    }
}

/* clang-format off */
static void os_sfdp_decode_basic_enter_mode_044(struct os_sflash *sflash, struct os_sfdp *sfdp, struct os_sfdp_param *param)
{
    if (param->head.length < 15)
        return;

    os_uint32_t *table = param->table;

    OS_UNUSED os_uint32_t mode_044_support = BIT_SHIFT(table[15 - 1], 9, 1);
    OS_UNUSED os_uint32_t mode_044_enter   = BIT_SHIFT(table[15 - 1], 16, 4);
    OS_UNUSED os_uint32_t mode_044_exit    = BIT_SHIFT(table[15 - 1], 10, 6);

    if (mode_044_support == 1)
    {
        OS_SFLASH_LOG("support 044 mode: 0x%x, 0x%x\r\n", mode_044_enter, mode_044_exit);
    }
}

static void os_sfdp_decode_basic_enter_mode_444(struct os_sflash *sflash, struct os_sfdp *sfdp, struct os_sfdp_param *param)
{
    /* decode mode 444 sequences */
    if (param->head.length < 15)
        return;

    os_uint32_t *table = param->table;

    OS_UNUSED os_uint32_t mode_444_enter = BIT_SHIFT(table[15 - 1], 4, 5);
    OS_UNUSED os_uint32_t mode_444_exit  = BIT_SHIFT(table[15 - 1], 0, 4);

    OS_SFLASH_LOG("444 mode sequences: 0x%x, 0x%x\r\n", mode_444_enter, mode_444_exit);

    /* enter mode 444 */
    struct os_xspi_message xmsg, *xmessage = &xmsg;

    if (mode_444_enter & 1)
    {
        memset(xmessage, 0, sizeof(struct os_xspi_message));

        xmessage_instruction(xmessage)       = 0x38;
        xmessage_instruction_lines(xmessage) = 1;

        os_sfbus_transfer(sflash, xmessage);
    }
}
/* clang-format on */

void os_sfdp_basic_init(struct os_sflash *sflash, struct os_sfdp *sfdp, struct os_sfdp_param *param)
{
    /* Enter 4-Byte Addressing */
    if (sfdp->info.addr_bytes == 4)
    {
        os_sfdp_decode_basic_enter_4bytes_addressing(sflash, sfdp, param);
    }

    /* Quad Enable Requirements */
    if (sflash->cmds.read.data_lines == 4)
    {
        os_sfdp_decode_basic_qer(sflash, sfdp, param);
    }

    /* 0-4-4 mode enable sequences */
    if (sflash->cmds.read.instruction_lines == 0 && sflash->cmds.read.address_lines == 4 &&
        sflash->cmds.read.data_lines == 4)
    {
        os_sfdp_decode_basic_enter_mode_044(sflash, sfdp, param);
    }

    /* 4-4-4 mode enable sequences */
    if (sflash->cmds.read.instruction_lines == 4 && sflash->cmds.read.address_lines == 4 &&
        sflash->cmds.read.data_lines == 4)
    {
        os_sfdp_decode_basic_enter_mode_444(sflash, sfdp, param);
    }
}
