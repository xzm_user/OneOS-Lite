/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        can.c
 *
 * @brief       This file provides functions for registering can device.
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */
#include <os_errno.h>
#include <os_assert.h>
#include <os_memory.h>
#include <string.h>
#include <dlog.h>
#include "dma_ram.h"

#include <net_dev.h>

#define EXT_LVL DBG_EXT_INFO
#define EXT_TAG "net_dev"
#include <drv_log.h>

OS_WEAK os_err_t os_net_protocol_lwip_init(struct os_net_device *net_dev)
{
    return OS_EOK;
}
OS_WEAK os_err_t os_net_protocol_lwip_deinit(struct os_net_device *net_dev)
{
    return OS_EOK;
}

static os_err_t os_net_device_init(struct os_net_device *net_dev)
{
    if (net_dev->ops->init)
    {
        if (net_dev->ops->init(net_dev) != OS_EOK)
        {
            return OS_ERROR;
        }
    }

#ifdef OS_NET_PROTOCOL_LWIP
    if (os_net_protocol_lwip_init(net_dev) != OS_EOK)
    {
        return OS_ERROR;
    }
#endif

    return OS_EOK;
}

static os_err_t os_net_device_deinit(struct os_net_device *net_dev)
{
    if (net_dev->ops->deinit)
    {
        if (net_dev->ops->deinit(net_dev) != OS_EOK)
        {
            return OS_ERROR;
        }
    }

#ifdef OS_NET_PROTOCOL_LWIP
    if (os_net_protocol_lwip_deinit(net_dev) != OS_EOK)
    {
        return OS_ERROR;
    }
#endif

    return OS_EOK;
}

os_err_t os_net_linkchange(struct os_net_device *net_dev, os_bool_t status)
{
    if (net_dev->info.linkstatus != status)
    {
        net_dev->info.linkstatus = status;

        if ((net_dev) && (net_dev->protocol_ops) && (net_dev->protocol_ops->linkchange))
        {
            return net_dev->protocol_ops->linkchange(net_dev, status);
        }
    }

    return OS_ENOSYS;
}

#ifndef OS_NET_DRV_TO_PROTOCOL
os_net_xfer_data_t *os_net_get_buff(os_uint32_t size)
{
    os_net_xfer_data_t *data = OS_NULL;

    data = os_dma_malloc_align(size + sizeof(os_net_xfer_data_t), 4);
    if (data)
    {
        data->buff = (os_uint8_t *)data + sizeof(os_net_xfer_data_t);
        data->size = size;
    }

    return data;
}

os_net_xfer_data_t *os_net_free_buff(os_net_xfer_data_t *buff)
{
    if (buff != OS_NULL)
    {
        os_dma_free_align(buff);
    }

    return OS_NULL;
}
#endif

static os_err_t get_mac_byte(char *string, os_uint8_t *value)
{
    os_uint8_t i = 0;

    for (i = 0; i < 2; i++)
    {
        if (((*string) >= 'A') && ((*string) <= 'F'))
        {
            *value += (os_uint8_t)(*string - 55);
        }
        else if (((*string) >= 'a') && ((*string) <= 'f'))
        {
            *value += (os_uint8_t)(*string - 87);
        }
        else if (((*string) >= '0') && ((*string) <= '9'))
        {
            *value += (os_uint8_t)(*string - 48);
        }
        else
        {
            return OS_ERROR;
        }

        if (i == 0)
        {
            *value = (*value) * 16;
        }

        string++;
    }

    return OS_EOK;
}

os_err_t os_net_get_mac_string2addr(char *string, os_uint8_t *addr)
{
    os_uint8_t i     = 0;
    os_uint8_t value = 0;

    if (string && addr)
    {
        for (i = 0; i < OS_NET_MAC_LENGTH - 1; i++)
        {
            if (*(string + 2 + 3 * i) != '.')
            {
                return OS_ERROR;
            }
        }

        for (i = 0; i < OS_NET_MAC_LENGTH; i++)
        {
            value = 0;

            if (get_mac_byte(string, &value) != OS_EOK)
            {
                return OS_ERROR;
            }

            *addr = value;

            addr++;
            string += 3;
        }

        return OS_EOK;
    }

    return OS_ERROR;
}

os_err_t os_net_set_filter(struct os_net_device *net_dev, os_uint8_t *addr, os_bool_t enable)
{
    if ((net_dev) && (net_dev->ops->set_filter) && (addr))
    {
        return net_dev->ops->set_filter(net_dev, addr, enable);
    }

    return OS_ENOSYS;
}

os_err_t os_net_get_macaddr(struct os_net_device *net_dev, os_uint8_t *addr)
{
    if ((net_dev) && (net_dev->ops->get_mac) && (addr))
    {
        return net_dev->ops->get_mac(net_dev, addr);
    }

    return OS_ENOSYS;
}

os_err_t os_net_tx_data(struct os_net_device *net_dev, os_net_data_t *data)
{
    os_err_t err = OS_ENOSYS;

    if (net_dev->info.xfer_flag & OS_NET_XFER_TX_TASK)
    {
        if ((net_dev) && (data))
        {
            return os_mb_send(net_dev->tx_mb, (os_ubase_t)data, OS_NO_WAIT);
        }
    }
    else
    {
        if ((net_dev) && (net_dev->ops->send))
        {
            err = net_dev->ops->send(net_dev, data);
        }

#ifndef OS_NET_DRV_TO_PROTOCOL
        data = os_net_free_buff(data);
#endif

        return err;
    }

    return OS_ENOSYS;
}

os_err_t os_net_rx_report_data(struct os_net_device *net_dev, os_net_data_t *data)
{
    os_err_t err;

    if ((net_dev) && (net_dev->protocol_ops) && (net_dev->protocol_ops->recv) && (data))
    {
        err = net_dev->protocol_ops->recv(net_dev, data);
    }

#ifndef OS_NET_DRV_TO_PROTOCOL
    data = os_net_free_buff(data);
#endif

    return err;
}

os_err_t os_net_rx_report_irq(struct os_net_device *net_dev)
{
    if (net_dev->rx_sem)
    {
        os_sem_post(net_dev->rx_sem);
        return OS_EOK;
    }

    return OS_ENOSYS;
}

static void os_net_tx_task_entry(void *parameter)
{
    os_net_data_t *data = OS_NULL;

    struct os_net_device *net_dev = (struct os_net_device *)parameter;

    while (1)
    {
        if (os_mb_recv(net_dev->tx_mb, (os_ubase_t *)&data, OS_WAIT_FOREVER) == OS_EOK)
        {
            if ((net_dev->ops->send) && (data))
            {
                net_dev->ops->send(net_dev, data);
            }

#ifndef OS_NET_DRV_TO_PROTOCOL
            data = os_net_free_buff(data);
#endif
        }
    }
}

static void os_net_rx_task_entry(void *parameter)
{
    os_net_data_t *data = OS_NULL;

    struct os_net_device *net_dev = (struct os_net_device *)parameter;

    while (1)
    {
        os_sem_wait(net_dev->rx_sem, OS_WAIT_FOREVER);

        if (net_dev->ops->recv == OS_NULL)
        {
            break;
        }

        data = net_dev->ops->recv(net_dev);
        if (data != OS_NULL)
        {
            os_net_rx_report_data(net_dev, data);
        }
    }
}

os_err_t os_net_device_register(struct os_net_device *net_dev, const char *name)
{
    char tx_name[OS_NAME_MAX] = "tx_";
    char rx_name[OS_NAME_MAX] = "rx_";

    if ((net_dev == OS_NULL) || (net_dev->ops == OS_NULL))
    {
        goto __init_exit;
    }

    if (os_net_device_init(net_dev) != OS_EOK)
    {
        goto __init_exit;
    }

    if (net_dev->info.xfer_flag & OS_NET_XFER_TX_TASK)
    {
        strcat(tx_name, name);

        net_dev->tx_task =
            os_task_create(tx_name, os_net_tx_task_entry, net_dev, OS_NET_TX_TASK_STACK_SIZE, OS_NET_TX_TASK_PREORITY);
        if (net_dev->tx_task == OS_EOK)
        {
            LOG_E(EXT_TAG, "%s task create failed!", tx_name);
            goto __init_exit;
        }

        net_dev->tx_mb = os_mb_create(tx_name, OS_NET_TX_MB_MAX_NUM);
        if (net_dev->tx_mb == OS_NULL)
        {
            LOG_E(EXT_TAG, "%s mb create failed!", tx_name);
            goto __init_exit;
        }

        os_task_startup(net_dev->tx_task);
    }

    if (net_dev->info.xfer_flag & OS_NET_XFER_RX_TASK)
    {
        strcat(rx_name, name);

        net_dev->rx_task = os_task_create(rx_name,
                                          os_net_rx_task_entry,
                                          net_dev,
                                          OS_NET_RX_TASK_STACK_SIZE * 2,
                                          OS_NET_RX_TASK_PREORITY);
        if (net_dev->rx_task == OS_NULL)
        {
            LOG_E(EXT_TAG, "%s task create failed!", rx_name);
            goto __init_exit;
        }

        net_dev->rx_sem = os_sem_create(rx_name, 0, OS_SEM_MAX_VALUE);
        if (net_dev->rx_sem == OS_NULL)
        {
            LOG_E(EXT_TAG, "%s sem create failed!", rx_name);
            goto __init_exit;
        }

        os_task_startup(net_dev->rx_task);
    }

    net_dev->dev.type = OS_DEVICE_TYPE_NETIF;
    if (os_device_register(&net_dev->dev, name) != OS_EOK)
    {
        return OS_ERROR;
    }

    return OS_EOK;

__init_exit:
    if (net_dev->tx_task)
    {
        os_task_destroy(net_dev->tx_task);
    }
    if (net_dev->tx_mb)
    {
        os_mb_destroy(net_dev->tx_mb);
    }
    if (net_dev->rx_task)
    {
        os_task_destroy(net_dev->rx_task);
    }
    if (net_dev->rx_sem)
    {
        os_sem_destroy(net_dev->rx_sem);
    }

    os_net_device_deinit(net_dev);

    return OS_ERROR;
}

os_err_t os_net_device_unregister(struct os_net_device *net_dev)
{
    if (net_dev->tx_task)
    {
        os_task_destroy(net_dev->tx_task);
    }
    if (net_dev->tx_mb)
    {
        os_mb_destroy(net_dev->tx_mb);
    }
    if (net_dev->rx_task)
    {
        os_task_destroy(net_dev->rx_task);
    }
    if (net_dev->rx_sem)
    {
        os_sem_destroy(net_dev->rx_sem);
    }

    return os_net_device_deinit(net_dev);
}
