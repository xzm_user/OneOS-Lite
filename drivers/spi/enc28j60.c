/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        enc28j60.c
 *
 * @brief       This file provides functions for enc28j60.
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include <os_event.h>
#include "enc28j60.h"
#include <pin.h>
#include <enc28j60.h>
#include "drv_spi.h"
#include <drv_gpio.h>
#include "board.h"

#define DRV_EXT_LVL DBG_EXT_INFO
#define DRV_EXT_TAG "drv.eth"
#include <dlog.h>

static uint8_t spi_read_op(struct os_spi_device *spi_device, uint8_t op, uint8_t address);
static void    spi_write_op(struct os_spi_device *spi_device, uint8_t op, uint8_t address, uint8_t data);

static uint8_t spi_read(struct os_spi_device *spi_device, uint8_t address);
static void    spi_write(struct os_spi_device *spi_device, os_uint8_t address, os_uint8_t data);

static void     enc28j60_clkout(struct os_spi_device *spi_device, os_uint8_t clk);
static void     enc28j60_set_bank(struct os_spi_device *spi_device, uint8_t address);
static uint32_t enc28j60_interrupt_disable(struct os_spi_device *spi_device);
static void     enc28j60_interrupt_enable(struct os_spi_device *spi_device, uint32_t level);

static uint16_t  enc28j60_phy_read(struct os_spi_device *spi_device, os_uint8_t address);
static void      enc28j60_phy_write(struct os_spi_device *spi_device, os_uint8_t address, uint16_t data);
static os_bool_t enc28j60_check_link_status(struct os_spi_device *spi_device);

#define enc28j60_lock(dev)   os_mutex_lock(((struct os_dev_enc28j60 *)dev)->lock, OS_WAIT_FOREVER);
#define enc28j60_unlock(dev) os_mutex_unlock(((struct os_dev_enc28j60 *)dev)->lock);

static uint8_t Enc28j60Bank;

/* clang-format off */
static void _delay_us(uint32_t us)
{
    volatile uint32_t len;
    for (; us > 0; us--)
        for (len = 0; len < 20; len++);
}
/* clang-format on */

static uint8_t spi_read_op(struct os_spi_device *spi_device, uint8_t op, uint8_t address)
{
    uint8_t  send_buffer[2];
    uint8_t  recv_buffer[1];
    uint32_t send_size = 1;

    send_buffer[0] = op | (address & ADDR_MASK);
    send_buffer[1] = 0xFF;

    /* Do dummy read if needed (for mac and mii, see datasheet page 29). */
    if (address & 0x80)
    {
        send_size = 2;
    }

    os_spi_send_then_recv(spi_device, send_buffer, send_size, recv_buffer, 1);
    return (recv_buffer[0]);
}

static void spi_write_op(struct os_spi_device *spi_device, uint8_t op, uint8_t address, uint8_t data)
{
    uint8_t buffer[2];

    buffer[0] = op | (address & ADDR_MASK);
    buffer[1] = data;
    os_spi_send(spi_device, buffer, 2);
}

static void enc28j60_clkout(struct os_spi_device *spi_device, os_uint8_t clk)
{
    /* Setup clkout: 2 is 12.5MHz: */
    spi_write(spi_device, ECOCON, clk & 0x7);
}

static void enc28j60_set_bank(struct os_spi_device *spi_device, uint8_t address)
{
    /* Set the bank (if needed) .*/
    if ((address & BANK_MASK) != Enc28j60Bank)
    {
        /* Set the bank. */
        spi_write_op(spi_device, ENC28J60_BIT_FIELD_CLR, ECON1, (ECON1_BSEL1 | ECON1_BSEL0));
        spi_write_op(spi_device, ENC28J60_BIT_FIELD_SET, ECON1, (address & BANK_MASK) >> 5);
        Enc28j60Bank = (address & BANK_MASK);
    }
}

static uint8_t spi_read(struct os_spi_device *spi_device, uint8_t address)
{
    /* Set the bank. */
    enc28j60_set_bank(spi_device, address);
    /* Do the read. */
    return spi_read_op(spi_device, ENC28J60_READ_CTRL_REG, address);
}

static void spi_write(struct os_spi_device *spi_device, os_uint8_t address, os_uint8_t data)
{
    /* Set the bank. */
    enc28j60_set_bank(spi_device, address);
    /* Do the write. */
    spi_write_op(spi_device, ENC28J60_WRITE_CTRL_REG, address, data);
}

static os_err_t enc28j60_wait_busy(struct os_spi_device *spi_device)
{
    int retry = 10000;

    while (--retry > 0)
    {
        if (!(spi_read(spi_device, MISTAT) & MISTAT_BUSY))
        {
            return OS_EOK;
        }

        _delay_us(10);
    }

    LOG_E(DRV_EXT_TAG, "enc28j60 wait timeout.");
    return OS_EBUSY;
}

static uint16_t enc28j60_phy_read(struct os_spi_device *spi_device, os_uint8_t address)
{
    uint16_t value;

    /* Set the right address and start the register read operation. */
    spi_write(spi_device, MIREGADR, address);
    spi_write(spi_device, MICMD, MICMD_MIIRD);

    _delay_us(15);

    /* Wait until the PHY read completes. */
    if (enc28j60_wait_busy(spi_device) == OS_EBUSY)
    {
        return 0xffff;
    }

    /* Reset reading bit */
    spi_write(spi_device, MICMD, 0x00);

    value = spi_read(spi_device, MIRDL) | spi_read(spi_device, MIRDH) << 8;

    return (value);
}

static void enc28j60_phy_write(struct os_spi_device *spi_device, os_uint8_t address, uint16_t data)
{
    /* Set the PHY register address. */
    spi_write(spi_device, MIREGADR, address);

    /* Write the PHY data. */
    spi_write(spi_device, MIWRL, data);
    spi_write(spi_device, MIWRH, data >> 8);

    /* Wait until the PHY write completes. */
    enc28j60_wait_busy(spi_device);
}

static uint32_t enc28j60_interrupt_disable(struct os_spi_device *spi_device)
{
    uint32_t level;

    /* Switch to bank 0 */
    enc28j60_set_bank(spi_device, EIE);

    /* Get last interrupt level */
    level = spi_read(spi_device, EIE);
    /* Disable interrutps */
    spi_write_op(spi_device, ENC28J60_BIT_FIELD_CLR, EIE, level);

    return level;
}

static void enc28j60_interrupt_enable(struct os_spi_device *spi_device, uint32_t level)
{
    /* Switch to bank 0 */
    enc28j60_set_bank(spi_device, EIE);
    spi_write_op(spi_device, ENC28J60_BIT_FIELD_SET, EIE, level);
}

static os_bool_t enc28j60_check_link_status(struct os_spi_device *spi_device)
{
    uint16_t reg;

    reg = enc28j60_phy_read(spi_device, PHSTAT2);

    if (reg & PHSTAT2_LSTAT)
    {
        return OS_TRUE;
    }
    else
    {
        return OS_FALSE;
    }
}

void enc28j60_isr(void *parameter)
{
    struct os_dev_enc28j60 *enc28j60 = (struct os_dev_enc28j60 *)parameter;

    os_sem_post(enc28j60->rx_sem);
}

static os_err_t enc28j60_init(struct os_net_device *net_dev)
{
    struct os_dev_enc28j60 *enc28j60   = (struct os_dev_enc28j60 *)net_dev;
    struct os_spi_device   *spi_device = enc28j60->spi_device;
    os_uint8_t              reg_val;

    enc28j60_lock(enc28j60);

    spi_write_op(spi_device, ENC28J60_SOFT_RESET, 0, ENC28J60_SOFT_RESET);
    os_task_msleep(OS_TICK_PER_SECOND / 50); /* delay 20ms */

    /* Rx start */
    spi_write(spi_device, ERXSTL, RXSTART_INIT & 0xFF);
    spi_write(spi_device, ERXSTH, RXSTART_INIT >> 8);
    /* RX end */
    spi_write(spi_device, ERXNDL, RXSTOP_INIT & 0xFF);
    spi_write(spi_device, ERXNDH, RXSTOP_INIT >> 8);

    /* TX start */
    spi_write(spi_device, ETXSTL, TXSTART_INIT & 0xFF);
    spi_write(spi_device, ETXSTH, TXSTART_INIT >> 8);
    /* TX end */
    spi_write(spi_device, ETXNDL, TXSTOP_INIT & 0xFF);
    spi_write(spi_device, ETXNDH, TXSTOP_INIT >> 8);

    reg_val = ERXFCON_UCEN | ERXFCON_CRCEN | ERXFCON_BCEN | ERXFCON_HTEN;

    spi_write(spi_device, ERXFCON, reg_val);

    /*
     * do bank 2 stuff
     * enable MAC receive
     */
    spi_write(spi_device, MACON1, MACON1_MARXEN | MACON1_TXPAUS | MACON1_RXPAUS);
    /* Enable automatic padding to 60bytes and CRC operations */
    /* spi_write_op(ENC28J60_BIT_FIELD_SET, MACON3, MACON3_PADCFG0|MACON3_TXCRCEN|MACON3_FRMLNEN); */
    spi_write_op(spi_device,
                 ENC28J60_BIT_FIELD_SET,
                 MACON3,
                 MACON3_PADCFG0 | MACON3_TXCRCEN | MACON3_FRMLNEN | MACON3_FULDPX);

    /* set inter-frame gap (back-to-back) */
    /* spi_write(MABBIPG, 0x12); */
    spi_write(spi_device, MABBIPG, 0x15);

    spi_write(spi_device, MACON4, MACON4_DEFER);
    spi_write(spi_device, MACLCON2, 63);

    /* set inter-frame gap (non-back-to-back) */
    spi_write(spi_device, MAIPGL, 0x12);
    spi_write(spi_device, MAIPGH, 0x0C);

    /*
     * Set the maximum packet size which the controller will accept
     * Do not send packets longer than MAX_FRAMELEN:
     */
    spi_write(spi_device, MAMXFLL, MAX_FRAMELEN & 0xFF);
    spi_write(spi_device, MAMXFLH, MAX_FRAMELEN >> 8);

    /*
     * do bank 3 stuff
     * write MAC address
     * NOTE: MAC address in ENC28J60 is byte-backward
     */
    spi_write(spi_device, MAADR0, enc28j60->dev_addr[5]);
    spi_write(spi_device, MAADR1, enc28j60->dev_addr[4]);
    spi_write(spi_device, MAADR2, enc28j60->dev_addr[3]);
    spi_write(spi_device, MAADR3, enc28j60->dev_addr[2]);
    spi_write(spi_device, MAADR4, enc28j60->dev_addr[1]);
    spi_write(spi_device, MAADR5, enc28j60->dev_addr[0]);

    /* Output off */
    spi_write(spi_device, ECOCON, 0x00);

    /* enc28j60_phy_write(PHCON1, 0x00); */
    enc28j60_phy_write(spi_device, PHCON1, PHCON1_PDPXMD); /* Full duplex */
    /* No loopback of transmitted frames */
    enc28j60_phy_write(spi_device, PHCON2, PHCON2_HDLDIS);
    /* Enable PHY link changed interrupt. */
    enc28j60_phy_write(spi_device, PHIE, PHIE_PGEIE | PHIE_PLNKIE);

    enc28j60_set_bank(spi_device, ECON2);
    spi_write_op(spi_device, ENC28J60_BIT_FIELD_SET, ECON2, ECON2_AUTOINC);

    /* Switch to bank 0 */
    enc28j60_set_bank(spi_device, ECON1);
    /* Enable all interrutps */
    spi_write_op(spi_device, ENC28J60_BIT_FIELD_SET, EIE, 0xFF);
    /* Enable packet reception */
    spi_write_op(spi_device, ENC28J60_BIT_FIELD_SET, ECON1, ECON1_RXEN);

    /* Clock out */
    enc28j60_clkout(spi_device, 2);

    enc28j60_phy_write(spi_device, PHLCON, 0xD76); /* 0x476 */
    os_task_msleep(OS_TICK_PER_SECOND / 50);       /* Delay 20ms */

    enc28j60_unlock(enc28j60);
    return OS_EOK;
}

static os_err_t enc28j60_deinit(struct os_net_device *net_dev)
{
    return OS_EOK;
}

static os_err_t enc28j60_send(struct os_net_device *net_dev, os_net_xfer_data_t *data)
{
    uint8_t     cmd = ENC28J60_WRITE_BUF_MEM;
    os_uint32_t level;

    struct os_dev_enc28j60 *enc28j60 = (struct os_dev_enc28j60 *)net_dev;

    if (data->size > MAX_TX_PACKAGE_SIZE)
    {
        LOG_E(DRV_EXT_TAG, "enc28j60 send size  %d more than limit %d!", data->size, MAX_TX_PACKAGE_SIZE);
        return OS_ERROR;
    }

    os_sem_wait(enc28j60->tx_sem, OS_WAIT_FOREVER);

    enc28j60_lock(enc28j60);

    level = enc28j60_interrupt_disable(enc28j60->spi_device);

    spi_write(enc28j60->spi_device, EWRPTL, TXSTART_INIT & 0xFF);
    spi_write(enc28j60->spi_device, EWRPTH, TXSTART_INIT >> 8);

    spi_write_op(enc28j60->spi_device, ENC28J60_WRITE_BUF_MEM, 0, 0x00);

    if (os_spi_send_then_send(enc28j60->spi_device, &cmd, 1, data->buff, data->size) != OS_EOK)
    {
        os_sem_post(enc28j60->tx_sem);
        LOG_E(DRV_EXT_TAG, "enc28j60 send size more than limit!");
    }

    spi_write(enc28j60->spi_device, ETXSTL, TXSTART_INIT & 0xFF);
    spi_write(enc28j60->spi_device, ETXSTH, TXSTART_INIT >> 8);

    spi_write(enc28j60->spi_device, ETXNDL, (TXSTART_INIT + data->size) & 0xFF);
    spi_write(enc28j60->spi_device, ETXNDH, (TXSTART_INIT + data->size) >> 8);

    spi_write_op(enc28j60->spi_device, ENC28J60_BIT_FIELD_SET, ECON1, ECON1_TXRTS);

    if ((spi_read(enc28j60->spi_device, EIR) & EIR_TXERIF))
    {
        spi_write_op(enc28j60->spi_device, ENC28J60_BIT_FIELD_CLR, ECON1, ECON1_TXRST);
    }

    enc28j60_interrupt_enable(enc28j60->spi_device, level);

    enc28j60_unlock(enc28j60);

    return OS_EOK;
}

static os_err_t enc28j60_get_macaddr(struct os_net_device *net_dev, os_uint8_t *addr)
{
    struct os_dev_enc28j60 *enc28j60 = (struct os_dev_enc28j60 *)net_dev;

    memcpy(addr, enc28j60->dev_addr, OS_NET_MAC_LENGTH);

    return OS_EOK;
}

static os_err_t enc28j60_set_filter(struct os_net_device *net_dev, os_uint8_t *addr, os_bool_t enable)
{
#if 0
    os_uint8_t configVal = 0;
    os_uint32_t crc = 0;
    os_uint32_t index = 0;

    struct os_dev_enc28j60 *enc28j60 = (struct os_dev_enc28j60 *)net_dev;

    crc = os_net_calc_filter_hash(addr);
    configVal = ((os_uint32_t)1U << (crc & 0x3U));
    index = crc >> 3;
        
    if (enable)
    {
        configVal = configVal | spi_read(enc28j60->spi_device, EHT0 + index);
        spi_write(enc28j60->spi_device, EHT0 + index, configVal);
    }
    else
    {
        configVal = (~configVal) & spi_read(enc28j60->spi_device, EHT0 + index);
        spi_write(enc28j60->spi_device, EHT0 + index, configVal);
    }
#else
    struct os_dev_enc28j60 *enc28j60 = (struct os_dev_enc28j60 *)net_dev;

    spi_write(enc28j60->spi_device, EHT0, 0xFF);
    spi_write(enc28j60->spi_device, EHT1, 0xFF);
    spi_write(enc28j60->spi_device, EHT2, 0xFF);
    spi_write(enc28j60->spi_device, EHT3, 0xFF);
    spi_write(enc28j60->spi_device, EHT4, 0xFF);
    spi_write(enc28j60->spi_device, EHT5, 0xFF);
#endif
    return OS_EOK;
}

static void enc28j60_rx_task_entry(void *parameter)
{
    struct os_dev_enc28j60 *enc28j60 = (struct os_dev_enc28j60 *)parameter;

    uint8_t     eir, eir_clr;
    uint32_t    pk_counter;
    os_uint32_t level;
    os_uint32_t len;
    os_uint16_t rxstat;
    uint8_t     cmd           = ENC28J60_READ_BUF_MEM;
    os_uint16_t NextPacketPtr = RXSTART_INIT;

    while (1)
    {
        os_sem_wait(enc28j60->rx_sem, OS_WAIT_FOREVER);

        enc28j60_lock(enc28j60);

        level = enc28j60_interrupt_disable(enc28j60->spi_device);

        eir = spi_read(enc28j60->spi_device, EIR);

        while (eir & ~EIR_PKTIF)
        {
            eir_clr = 0;

            if (eir & EIR_PKTIF)
            {
                enc28j60_set_bank(enc28j60->spi_device, EIE);

                spi_write_op(enc28j60->spi_device, ENC28J60_BIT_FIELD_CLR, EIE, EIE_PKTIE);

                eir_clr |= EIR_PKTIF;
            }

            if (eir & EIR_DMAIF)
            {
                eir_clr |= EIR_DMAIF;
            }

            if (eir & EIR_LINKIF)
            {
                os_bool_t link_status;

                link_status = enc28j60_check_link_status(enc28j60->spi_device);

                enc28j60_phy_read(enc28j60->spi_device, PHIR);
                eir_clr |= EIR_LINKIF;

                os_net_linkchange(&enc28j60->net_dev, link_status);
            }

            if (eir & EIR_TXIF)
            {
                enc28j60_set_bank(enc28j60->spi_device, EIR);
                spi_write_op(enc28j60->spi_device, ENC28J60_BIT_FIELD_CLR, EIR, EIR_TXIF);

                os_sem_post(enc28j60->tx_sem);

                eir_clr |= EIR_TXIF;
            }

            if (eir & EIR_WOLIF)
            {
                eir_clr |= EIR_WOLIF;
            }

            if ((eir & EIR_TXERIF) != 0)
            {
                enc28j60_set_bank(enc28j60->spi_device, ECON1);
                spi_write_op(enc28j60->spi_device, ENC28J60_BIT_FIELD_SET, ECON1, ECON1_TXRST);
                spi_write_op(enc28j60->spi_device, ENC28J60_BIT_FIELD_CLR, ECON1, ECON1_TXRST);
                eir_clr |= EIR_TXERIF;
            }

            if ((eir & EIR_RXERIF) != 0)
            {
                enc28j60_set_bank(enc28j60->spi_device, ECON1);
                spi_write_op(enc28j60->spi_device, ENC28J60_BIT_FIELD_SET, ECON1, ECON1_RXRST);
                spi_write_op(enc28j60->spi_device, ENC28J60_BIT_FIELD_CLR, ECON1, ECON1_RXRST);

                enc28j60_set_bank(enc28j60->spi_device, ECON1);

                spi_write_op(enc28j60->spi_device, ENC28J60_BIT_FIELD_SET, ECON1, ECON1_RXEN);
                eir_clr |= EIR_RXERIF;
            }

            enc28j60_set_bank(enc28j60->spi_device, EIR);
            spi_write_op(enc28j60->spi_device, ENC28J60_BIT_FIELD_CLR, EIR, eir_clr);

            eir = spi_read(enc28j60->spi_device, EIR);
        }

        pk_counter = spi_read(enc28j60->spi_device, EPKTCNT);
        if (pk_counter)
        {
            spi_write(enc28j60->spi_device, ERDPTL, NextPacketPtr & 0xFF);
            spi_write(enc28j60->spi_device, ERDPTH, NextPacketPtr >> 8);

            NextPacketPtr = spi_read_op(enc28j60->spi_device, ENC28J60_READ_BUF_MEM, 0);
            NextPacketPtr |= spi_read_op(enc28j60->spi_device, ENC28J60_READ_BUF_MEM, 0) << 8;

            len = spi_read_op(enc28j60->spi_device, ENC28J60_READ_BUF_MEM, 0);
            len |= spi_read_op(enc28j60->spi_device, ENC28J60_READ_BUF_MEM, 0) << 8;

            len -= 4;

            rxstat = spi_read_op(enc28j60->spi_device, ENC28J60_READ_BUF_MEM, 0);
            rxstat |= ((os_uint16_t)spi_read_op(enc28j60->spi_device, ENC28J60_READ_BUF_MEM, 0)) << 8;

            if ((rxstat & 0x80) != 0)
            {
                os_net_xfer_data_t *data = os_net_get_buff(len);

                if (data)
                {
                    if (os_spi_send_then_recv(enc28j60->spi_device, &cmd, 1, data->buff, data->size) == OS_EOK)
                    {
                        os_net_rx_report_data(&enc28j60->net_dev, data);
                    }
                    else
                    {
                        LOG_E(DRV_EXT_TAG, "enc28j60 recv failed!");
                    }
                }
                else
                {
                    LOG_E(DRV_EXT_TAG, "no memory");
                }
            }

            spi_write(enc28j60->spi_device, ERXRDPTL, (NextPacketPtr));
            spi_write(enc28j60->spi_device, ERXRDPTH, (NextPacketPtr) >> 8);

            spi_write_op(enc28j60->spi_device, ENC28J60_BIT_FIELD_SET, ECON2, ECON2_PKTDEC);
        }
        else
        {
            enc28j60_set_bank(enc28j60->spi_device, ECON1);

            spi_write_op(enc28j60->spi_device, ENC28J60_BIT_FIELD_SET, ECON1, ECON1_RXEN);

            level |= EIE_PKTIE;
        }

        enc28j60_interrupt_enable(enc28j60->spi_device, level);

        enc28j60_unlock(enc28j60);
    }
}

const static struct os_net_device_ops net_dev_ops = {
    .init       = enc28j60_init,
    .deinit     = enc28j60_deinit,
    .send       = enc28j60_send,
    .get_mac    = enc28j60_get_macaddr,
    .set_filter = enc28j60_set_filter,
};

int os_hw_enc28j60_init(void)
{
    os_uint16_t reg_value = 0;

    struct os_dev_enc28j60 *enc28j60 = os_calloc(1, sizeof(struct os_dev_enc28j60));
    if (enc28j60 == OS_NULL)
    {
        LOG_E(DRV_EXT_TAG, "enc28j60 mem call failed!");
        goto __exit;
    }

    if (os_hw_spi_device_attach(BSP_ENC28J60_SPI_BUS, BSP_ENC28J60_SPI_DEV, BSP_ENC28J60_SPI_CS) != OS_EOK)
    {
        LOG_E(DRV_EXT_TAG, "os_hw_spi_device_attach failed!");
        goto __exit;
    }

    os_pin_mode(BSP_ENC28J60_RST, PIN_MODE_OUTPUT);
    os_pin_write(BSP_ENC28J60_RST, PIN_LOW);
    os_task_msleep(10);
    os_pin_write(BSP_ENC28J60_RST, PIN_HIGH);

    enc28j60->spi_device = (struct os_spi_device *)os_device_open_s(BSP_ENC28J60_SPI_DEV);
    if (enc28j60->spi_device == OS_NULL)
    {
        LOG_E(DRV_EXT_TAG, "enc28j60->spi_device open failed!");
        goto __exit;
    }

    enc28j60->spi_cfg.data_width = 8;
    enc28j60->spi_cfg.mode       = OS_SPI_MODE_0 | OS_SPI_MSB;
    enc28j60->spi_cfg.max_hz     = 20 * 1000 * 1000;
    os_spi_configure(enc28j60->spi_device, &enc28j60->spi_cfg);

    spi_write_op(enc28j60->spi_device, ENC28J60_SOFT_RESET, 0, ENC28J60_SOFT_RESET);
    os_task_msleep(1);

    enc28j60->emac_rev = spi_read(enc28j60->spi_device, EREVID);
    if (enc28j60->emac_rev == 0xff)
    {
        LOG_E(DRV_EXT_TAG, "enc28j60 HwRevID invalid!");
        goto __exit;
    }

    reg_value         = enc28j60_phy_read(enc28j60->spi_device, PHHID2);
    enc28j60->phy_rev = reg_value & 0x0F;
    enc28j60->phy_pn  = (reg_value >> 4) & 0x3F;
    enc28j60->phy_id  = (enc28j60_phy_read(enc28j60->spi_device, PHHID1) | ((reg_value >> 10) << 16)) << 3;
    if (enc28j60->phy_id != 0x00280418)
    {
        LOG_E(DRV_EXT_TAG, "enc28j60 PHY ID not correct!");
        goto __exit;
    }

    if (os_net_get_mac_string2addr(BSP_ENC28J60_MAC_ADDR, enc28j60->dev_addr) != OS_EOK)
    {
        LOG_E(DRV_EXT_TAG, "enc28j60 mac config err!");
        goto __exit;
    }

    if ((enc28j60->dev_addr[0] != 0x00) || (enc28j60->dev_addr[1] != 0x04) || (enc28j60->dev_addr[2] != 0xA3))
    {
        LOG_E(DRV_EXT_TAG, "enc28j60 mac must be 00.04.A3.XX.XX.XX!");
        goto __exit;
    }

    enc28j60->tx_sem = os_sem_create("enc28j60_txsem", 1, 1);
    if (enc28j60->tx_sem == OS_NULL)
    {
        LOG_E(DRV_EXT_TAG, "enc28j60 tx_sem create failed!");
        goto __exit;
    }

    enc28j60->rx_sem = os_sem_create("enc28j60_rxsem", 0, 1);
    if (enc28j60->rx_sem == OS_NULL)
    {
        LOG_E(DRV_EXT_TAG, "enc28j60 rx_sem create failed!");
        goto __exit;
    }

    enc28j60->lock = os_mutex_create("enc28j60_rxsem", OS_FALSE);
    if (enc28j60->lock == OS_NULL)
    {
        LOG_E(DRV_EXT_TAG, "enc28j60 lock create failed!");
        goto __exit;
    }

    enc28j60->rx_task = os_task_create("enc28j60_rxtask",
                                       enc28j60_rx_task_entry,
                                       enc28j60,
                                       BSP_ENC28J60_RX_TASK_STACK_SIZE,
                                       BSP_ENC28J60_RX_TASK_PRIORITY);
    if (enc28j60->rx_task == OS_NULL)
    {
        LOG_E(DRV_EXT_TAG, "enc28j60 rx_task create failed!");
        goto __exit;
    }

    enc28j60->net_dev.info.MTU       = MAX_TX_PACKAGE_SIZE;
    enc28j60->net_dev.info.MRU       = MAX_TX_PACKAGE_SIZE;
    enc28j60->net_dev.info.mode      = net_dev_mode_sta;
    enc28j60->net_dev.info.intf_type = net_dev_intf_ether;
    enc28j60->net_dev.info.xfer_flag = OS_NET_XFER_TX_TASK;
    enc28j60->net_dev.ops            = &net_dev_ops;
    if (os_net_device_register(&enc28j60->net_dev, "enc28j60") != OS_EOK)
    {
        LOG_E(DRV_EXT_TAG, "os_net_device_register failed");
        goto __exit;
    }

    os_task_startup(enc28j60->rx_task);

    os_pin_mode(BSP_ENC28J60_IRQ, PIN_MODE_INPUT_PULLUP);
    os_pin_attach_irq(BSP_ENC28J60_IRQ, PIN_IRQ_MODE_FALLING, (void (*)(void *))enc28j60_isr, enc28j60);
    os_pin_irq_enable(BSP_ENC28J60_IRQ, PIN_IRQ_ENABLE);

    LOG_I(DRV_EXT_TAG, "enc28j60 attach success.");

    return OS_EOK;
__exit:
    if (enc28j60)
    {
        os_free(enc28j60);
    }

    if (enc28j60->tx_sem)
    {
        os_sem_destroy(enc28j60->tx_sem);
    }

    if (enc28j60->rx_sem)
    {
        os_sem_destroy(enc28j60->rx_sem);
    }

    if (enc28j60->lock)
    {
        os_mutex_destroy(enc28j60->lock);
    }

    if (enc28j60->rx_task)
    {
        os_task_destroy(enc28j60->rx_task);
    }

    return OS_EOK;
}
OS_CMPOENT_INIT(os_hw_enc28j60_init, OS_INIT_SUBLEVEL_LOW);
