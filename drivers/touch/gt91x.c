/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        gt91x.c
 *
 * @brief       gt91x
 *
 * @details     gt91x
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include <os_task.h>
#include <device.h>
#include <os_clock.h>
#include <i2c.h>
#include <string.h>
#include <stdlib.h>
#include <os_errno.h>
#include <os_memory.h>
#include <pin/pin.h>
#include <string.h>
#include <drv_gpio.h>

#include "touch.h"
#include "gt91x.h"

#define DBG_TAG "gt91x"

const static os_uint8_t CTP_CFG_GT9157[] = {
    0x00, 0x20, 0x03, 0xE0, 0x01, 0x05, 0x3C, 0x00, 0x01, 0x08, 0x28, 0x0C, 0x50, 0x32, 0x03, 0x05, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x17, 0x19, 0x1E, 0x14, 0x8B, 0x2B, 0x0D, 0x33, 0x35, 0x0C, 0x08, 0x00, 0x00, 0x00, 0x9A,
    0x03, 0x11, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x32, 0x00, 0x00, 0x00, 0x20, 0x58, 0x94, 0xC5, 0x02, 0x00,
    0x00, 0x00, 0x04, 0xB0, 0x23, 0x00, 0x93, 0x2B, 0x00, 0x7B, 0x35, 0x00, 0x69, 0x41, 0x00, 0x5B, 0x4F, 0x00, 0x5B,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0x04,
    0x06, 0x08, 0x0A, 0x0C, 0x0E, 0x10, 0x12, 0x14, 0x16, 0x18, 0x1A, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0x04, 0x06, 0x08, 0x0A, 0x0C, 0x0F, 0x10, 0x12,
    0x13, 0x16, 0x18, 0x1C, 0x1D, 0x1E, 0x1F, 0x20, 0x21, 0x22, 0x24, 0x26, 0xFF, 0xFF, 0xFF, 0xFF, 0x00, 0x00, 0x00,
    0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x48, 0x01};

const static os_uint8_t CTP_CFG_GT911[] = {
    0x00, 0x20, 0x03, 0xe0, 0x01, 0x05, 0x3d, 0x00, 0x01, 0x08, 0x1e, 0x05, 0x3c, 0x3c, 0x03, 0x05, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x1a, 0x1c, 0x1e, 0x14, 0x8a, 0x2a, 0x0c, 0x2a, 0x28, 0xeb, 0x04, 0x00, 0x00, 0x01, 0x61,
    0x03, 0x2c, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x14, 0x3c, 0x94, 0xc5, 0x02, 0x08,
    0x00, 0x00, 0x04, 0xb7, 0x16, 0x00, 0x9f, 0x1b, 0x00, 0x8b, 0x22, 0x00, 0x7b, 0x2b, 0x00, 0x70, 0x36, 0x00, 0x70,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x18, 0x16,
    0x14, 0x12, 0x10, 0x0e, 0x0c, 0x0a, 0x08, 0x06, 0x04, 0x02, 0xff, 0xff, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x24, 0x22, 0x21, 0x20, 0x1f, 0x1e, 0x1d, 0x1c, 0x18, 0x16,
    0x13, 0x12, 0x10, 0x0f, 0x0a, 0x08, 0x06, 0x04, 0x02, 0x00, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x0a, 0x00};

const static os_uint8_t CTP_CFG_GT5688[] = {
    0x97, 0xE0, 0x01, 0x10, 0x01, 0x05, 0x0D, 0x00, 0x01, 0x00, 0x00, 0x05, 0x5A, 0x46, 0x53, 0x11, 0x00, 0x00, 0x11,
    0x11, 0x14, 0x14, 0x14, 0x22, 0x0A, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x53, 0x00, 0x14, 0x00, 0x00, 0x84,
    0x00, 0x00, 0x3C, 0x00, 0x00, 0x64, 0x1E, 0x28, 0x87, 0x27, 0x08, 0x32, 0x34, 0x05, 0x0D, 0x20, 0x33, 0x60, 0x11,
    0x02, 0x24, 0x00, 0x00, 0x64, 0x80, 0x80, 0x14, 0x02, 0x00, 0x00, 0x54, 0x89, 0x68, 0x85, 0x6D, 0x82, 0x72, 0x80,
    0x76, 0x7D, 0x7B, 0x7B, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xF0, 0x50, 0x3C, 0xFF, 0xFF, 0x07, 0x00, 0x00,
    0x00, 0x02, 0x14, 0x14, 0x03, 0x04, 0x00, 0x21, 0x64, 0x0A, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x32, 0x20, 0x50,
    0x3C, 0x3C, 0x00, 0x00, 0x00, 0x00, 0x00, 0x0D, 0x06, 0x0C, 0x05, 0x0B, 0x04, 0x0A, 0x03, 0xFF, 0xFF, 0xFF, 0xFF,
    0xFF, 0xFF, 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0xFF, 0xFF, 0xFF,
    0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x3C, 0x00, 0x05, 0x1E, 0x00, 0x02, 0x2A, 0x1E, 0x19, 0x14, 0x02, 0x00, 0x03, 0x0A, 0x05,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0xFF, 0xFF, 0x86, 0x22, 0x03, 0x00, 0x00, 0x33, 0x00, 0x0F, 0x00,
    0x00, 0x00, 0x50, 0x3C, 0x50, 0x00, 0x00, 0x00, 0x00, 0x2A, 0x01};

const static os_uint8_t CTP_CFG_GT917S[] = {
    0x97, 0x20, 0x03, 0xE0, 0x01, 0x0A, 0x35, 0x04, 0x00, 0x69, 0x09, 0x0F, 0x50, 0x32, 0x33, 0x11, 0x00, 0x32, 0x11,
    0x11, 0x28, 0x8C, 0xAA, 0xDC, 0x58, 0x04, 0x00, 0x00, 0x1E, 0x3C, 0x00, 0x00, 0x00, 0x31, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x40, 0x32, 0x00, 0x00, 0x50, 0x38, 0x00, 0x8D, 0x20, 0x16, 0x4E, 0x4C, 0x7C, 0x05, 0x28, 0x3E, 0x28, 0x0D,
    0x43, 0x24, 0x00, 0x01, 0x39, 0x6B, 0xC0, 0x94, 0x84, 0x2D, 0x00, 0x54, 0xB0, 0x41, 0x9D, 0x49, 0x8D, 0x52, 0x7F,
    0x5A, 0x75, 0x62, 0x6C, 0x42, 0x50, 0x14, 0x00, 0x00, 0x00, 0x00, 0xF0, 0x50, 0x3C, 0x88, 0x88, 0x27, 0x50, 0x3C,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x50, 0x00, 0x02, 0x78, 0x0A, 0x50, 0xFF, 0xE4,
    0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x3C, 0xB0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x56, 0xA2, 0x07, 0x50, 0x1E, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F, 0x10,
    0x12, 0x15, 0x16, 0x17, 0x18, 0x19, 0x1A, 0x1B, 0x1D, 0x1F, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
    0xFF, 0x1F, 0x1E, 0x1D, 0x1C, 0x1B, 0x1A, 0x19, 0x18, 0x17, 0x15, 0x14, 0x13, 0x12, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
    0xFF, 0xFF, 0xFF, 0xFF, 0x00, 0x30, 0x7F, 0x7F, 0x7F, 0xFF, 0x54, 0x64, 0x00, 0x80, 0x46, 0x07, 0x50, 0x3C, 0x32,
    0x14, 0x0A, 0x64, 0x32, 0x00, 0x00, 0x00, 0x00, 0x11, 0x02, 0x62, 0x32, 0x03, 0x14, 0x50, 0x0C, 0xE2, 0x14, 0x50,
    0x00, 0x54, 0x10, 0x00, 0x32, 0xA2, 0x07, 0x64, 0x53, 0xB6, 0x00};

const static os_uint8_t CTP_CFG_GT9147[] = {
    0X60, 0XE0, 0X01, 0X20, 0X03, 0X05, 0X35, 0X00, 0X02, 0X08, 0X1E, 0X08, 0X50, 0X3C, 0X0F, 0X05, 0X00, 0X00, 0XFF,
    0X67, 0X50, 0X00, 0X00, 0X18, 0X1A, 0X1E, 0X14, 0X89, 0X28, 0X0A, 0X30, 0X2E, 0XBB, 0X0A, 0X03, 0X00, 0X00, 0X02,
    0X33, 0X1D, 0X00, 0X00, 0X00, 0X00, 0X00, 0X00, 0X00, 0X32, 0X00, 0X00, 0X2A, 0X1C, 0X5A, 0X94, 0XC5, 0X02, 0X07,
    0X00, 0X00, 0X00, 0XB5, 0X1F, 0X00, 0X90, 0X28, 0X00, 0X77, 0X32, 0X00, 0X62, 0X3F, 0X00, 0X52, 0X50, 0X00, 0X52,
    0X00, 0X00, 0X00, 0X00, 0X00, 0X00, 0X00, 0X00, 0X00, 0X00, 0X00, 0X00, 0X00, 0X00, 0X00, 0X00, 0X00, 0X00, 0X00,
    0X00, 0X00, 0X00, 0X00, 0X0F, 0X0F, 0X03, 0X06, 0X10, 0X42, 0XF8, 0X0F, 0X14, 0X00, 0X00, 0X00, 0X00, 0X1A, 0X18,
    0X16, 0X14, 0X12, 0X10, 0X0E, 0X0C, 0X0A, 0X08, 0X00, 0X00, 0X00, 0X00, 0X00, 0X00, 0X00, 0X00, 0X00, 0X00, 0X00,
    0X00, 0X00, 0X00, 0X00, 0X00, 0X00, 0X00, 0X00, 0X00, 0X29, 0X28, 0X24, 0X22, 0X20, 0X1F, 0X1E, 0X1D, 0X0E, 0X0C,
    0X0A, 0X08, 0X06, 0X05, 0X04, 0X02, 0X00, 0XFF, 0X00, 0X00, 0X00, 0X00, 0X00, 0X00, 0X00, 0X00, 0X00, 0X00, 0X00,
    0XFF, 0XFF, 0XFF, 0XFF, 0XFF, 0XFF, 0XFF, 0XFF, 0XFF, 0XFF, 0XFF, 0XFF, 0XFF};
struct gt91x_touch
{
    os_touch_t                touch_device;
    struct os_i2c_bus_device *i2c_bus;
    os_uint16_t               i2c_addr;
    os_uint16_t               id;
    struct chip_info          chip;

    os_uint8_t           cru_num;
    os_uint8_t           pre_num;
    struct os_touch_data cru_data[GT91X_MAX_TOUCH];
    struct os_touch_data pre_data[GT91X_MAX_TOUCH];
};

static os_err_t gt91x_write_reg(struct gt91x_touch *gt91x, os_uint8_t *write_data, os_uint8_t write_len)
{
    os_uint8_t retries = 5;

    struct os_i2c_msg msgs;

    msgs.addr  = gt91x->i2c_addr;
    msgs.flags = OS_I2C_WR;
    msgs.buf   = write_data;
    msgs.len   = write_len;

    while (retries)
    {
        if (os_i2c_transfer(gt91x->i2c_bus, &msgs, 1) == 1)
        {
            return OS_EOK;
        }
        retries--;
    }

    return OS_ERROR;
}

static os_err_t gt91x_read_regs(struct gt91x_touch *gt91x,
                                os_uint8_t         *cmd_buf,
                                os_uint8_t          cmd_len,
                                os_uint8_t         *read_buf,
                                os_uint8_t          read_len)
{
    os_uint8_t retries = 3;

    struct os_i2c_msg msgs[2];

    msgs[0].addr  = gt91x->i2c_addr;
    msgs[0].flags = OS_I2C_WR;
    msgs[0].buf   = cmd_buf;
    msgs[0].len   = cmd_len;

    msgs[1].addr  = gt91x->i2c_addr;
    msgs[1].flags = OS_I2C_RD;
    msgs[1].buf   = read_buf;
    msgs[1].len   = read_len;

    while (retries)
    {
        if (os_i2c_transfer(gt91x->i2c_bus, msgs, 2) == 2)
        {
            return OS_EOK;
        }
        retries--;
    }

    return OS_ERROR;
}

static os_err_t gt91x_get_product_id(struct gt91x_touch *gt91x, os_uint8_t *read_data, os_uint8_t read_len)
{
    os_uint8_t cmd_buf[2];

    cmd_buf[0] = (os_uint8_t)(GT91X_PRODUCT_ID >> 8);
    cmd_buf[1] = (os_uint8_t)(GT91X_PRODUCT_ID & 0xff);

    if (gt91x_read_regs(gt91x, cmd_buf, 2, read_data, read_len) != OS_EOK)
    {
        LOG_E(DBG_TAG, "read id failed");
        return OS_ERROR;
    }

    return OS_EOK;
}

static os_err_t gt91x_get_info(struct gt91x_touch *gt91x, struct os_touch_info *info)
{
    os_uint8_t opr_buf[7] = {0};
    os_uint8_t cmd_buf[2];

    cmd_buf[0] = (os_uint8_t)(gt91x->chip.cfg_addr >> 8);
    cmd_buf[1] = (os_uint8_t)(gt91x->chip.cfg_addr & 0xff);

    if (gt91x_read_regs(gt91x, cmd_buf, 2, opr_buf, 7) != OS_EOK)
    {
        LOG_E(DBG_TAG, "read id failed");

        return OS_ERROR;
    }

    info->range_x   = (opr_buf[2] << 8) + opr_buf[1];
    info->range_y   = (opr_buf[4] << 8) + opr_buf[3];
    info->point_num = opr_buf[5] & 0x0f;

    return OS_EOK;
}

static os_err_t gt91x_soft_reset(struct gt91x_touch *gt91x)
{
    os_uint8_t buf[3];

    buf[0] = (os_uint8_t)(GT91X_COMMAND >> 8);
    buf[1] = (os_uint8_t)(GT91X_COMMAND & 0xFF);
    buf[2] = 0x02;

    if (gt91x_write_reg(gt91x, buf, 3) != OS_EOK)
    {
        LOG_E(DBG_TAG, "soft reset gt91x failed");
        return OS_ERROR;
    }
    os_task_msleep(10);

    buf[2] = 0x00;

    if (gt91x_write_reg(gt91x, buf, 3) != OS_EOK)
    {
        LOG_E(DBG_TAG, "soft reset gt91x end failed");
        return OS_ERROR;
    }

    return OS_EOK;
}

static os_err_t gt9xx_read_config(struct gt91x_touch *gt91x)
{
    uint8_t cfg_cmd[2] = {0};
    uint8_t reg_data   = 0;
    uint8_t retry      = 5;

    cfg_cmd[0] = gt91x->chip.cfg_addr >> 8;
    cfg_cmd[1] = gt91x->chip.cfg_addr & 0xff;

    while (retry)
    {
        if (gt91x_read_regs(gt91x, cfg_cmd, 2, &reg_data, 1) == OS_EOK)
        {
            return OS_EOK;
        }
        retry--;
    }

    return OS_ERROR;
}

static os_err_t gt91x_int_enable(struct gt91x_touch *gt91x)
{
    return os_pin_irq_enable(OS_GT91X_IRQ_PIN, PIN_IRQ_ENABLE);
}

static os_err_t gt91x_int_disable(struct gt91x_touch *gt91x)
{
    return os_pin_irq_enable(OS_GT91X_IRQ_PIN, PIN_IRQ_DISABLE);
}

static os_uint8_t gt91x_touch_up_down(struct gt91x_touch *gt91x, struct os_touch_data *data)
{
    os_uint8_t i     = 0;
    os_uint8_t j     = 0;
    os_uint8_t index = 0;

    if (gt91x->cru_num > 1)
    {
        return 0;
    }

    if (gt91x->cru_num < gt91x->pre_num)
    {
        for (i = 0; i < gt91x->pre_num; i++)
        {
            for (j = 0; j < gt91x->cru_num; j++)
            {
                if (gt91x->cru_data[i].track_id == gt91x->cru_data[j].track_id)
                {
                    break;
                }
            }

            if (j == gt91x->cru_num)
            {
                gt91x->pre_data[i].event = OS_TOUCH_EVENT_UP;
                memcpy(&data[index], &gt91x->pre_data[i], sizeof(struct os_touch_data));
                index++;
            }
        }
    }

    for (i = 0; i < gt91x->cru_num; i++)
    {
        for (j = 0; j < gt91x->pre_num; j++)
        {
            if (gt91x->cru_data[i].track_id == gt91x->cru_data[j].track_id)
            {
                gt91x->cru_data[i].event = OS_TOUCH_EVENT_MOVE;
                break;
            }
        }

        if (j == gt91x->pre_num)
        {
            gt91x->cru_data[i].event = OS_TOUCH_EVENT_DOWN;
        }

        memcpy(&data[index], &gt91x->cru_data[i], sizeof(struct os_touch_data));
        index++;
    }

    return index;
}

static os_size_t gt91x_read_point(struct os_touch_device *touch, struct os_touch_data *data, os_size_t read_num)
{
    os_uint8_t off_set                                                = 0;
    os_uint8_t read_index                                             = 0;
    os_uint8_t touch_num                                              = 0;
    os_uint8_t point_status                                           = 0;
    os_uint8_t status_cmd[3]                                          = {0};
    os_uint8_t data_cmd[2]                                            = {0};
    os_uint8_t point_data[GT91X_POINT_INFO_NUM * GT91X_MAX_TOUCH + 1] = {0};

    struct gt91x_touch *gt91x = (struct gt91x_touch *)touch;

    status_cmd[0] = GT91X_READ_STATUS >> 8;
    status_cmd[1] = GT91X_READ_STATUS & 0xFF;
    status_cmd[2] = 0;

    /* point status register */
    data_cmd[0] = (GT91X_POINT1_REG >> 8) & 0xFF;
    data_cmd[1] = (GT91X_POINT1_REG & 0xFF);

    if (gt91x_read_regs(gt91x, status_cmd, GT91X_ADDR_LENGTH, point_data, 10) != OS_EOK)
    {
        LOG_E(DBG_TAG, "read point failed");
        read_num = 0;
        goto exit_;
    }
    point_status = point_data[0];
    if (point_status == 0) /* no data */
    {
        read_num = 0;
        goto exit_;
    }

    if ((point_status & 0x80) == 0) /* data is not ready */
    {
        read_num = 0;
        goto exit_;
    }

    touch_num = point_status & 0x0f; /* get point num */

    if ((touch_num > GT91X_MAX_TOUCH) || (touch_num == 0)) /* point num is not correct */
    {
        read_num = 0;
        goto exit_;
    }

    /* read point data */
    if (gt91x_read_regs(gt91x, data_cmd, GT91X_ADDR_LENGTH, point_data, touch_num * GT91X_POINT_INFO_NUM + 1) != OS_EOK)
    {
        LOG_E(DBG_TAG, "read point failed");
        read_num = 0;
        goto exit_;
    }

    for (read_index = 0; read_index < GT91X_MAX_TOUCH; read_index++)
    {
        if (read_index < touch_num)
        {
            off_set                                  = read_index * 8;
            gt91x->cru_data[read_index].track_id     = point_data[off_set] & 0x0f;
            gt91x->cru_data[read_index].x_coordinate = point_data[off_set + 1] | (point_data[off_set + 2] << 8); /* x */
            gt91x->cru_data[read_index].y_coordinate = point_data[off_set + 3] | (point_data[off_set + 4] << 8); /* y */
            gt91x->cru_data[read_index].width     = point_data[off_set + 5] | (point_data[off_set + 6] << 8); /* size */
            gt91x->cru_data[read_index].timestamp = os_touch_get_ts();
        }
        else
        {
            gt91x->cru_data[read_index].track_id     = 0;
            gt91x->cru_data[read_index].x_coordinate = 0; /* x */
            gt91x->cru_data[read_index].y_coordinate = 0; /* y */
            gt91x->cru_data[read_index].width        = 0; /* size */
            gt91x->cru_data[read_index].timestamp    = 0;
        }
    }

    gt91x->cru_num = touch_num;

    gt91x_touch_up_down(gt91x, data);

exit_:
    if (gt91x_write_reg(gt91x, status_cmd, 3) != OS_EOK)
    {
        LOG_E(DBG_TAG, "gt91x read end failed");
    }
    return read_num;
}

static os_err_t gt91x_control(struct os_touch_device *device, int cmd, void *data)
{
    os_uint8_t  i         = 0;
    os_uint16_t check_sum = 0;
    os_err_t    err       = OS_EOK;
    os_uint16_t buff;

    os_uint8_t *cfg_table = OS_NULL;

    struct gt91x_touch *gt91x = (struct gt91x_touch *)device;

    cfg_table = os_calloc(1, GT91X_CONFIG_MAX_LENGTH);
    if (cfg_table == OS_NULL)
    {
        LOG_E(DBG_TAG, "memery call failed");
        return OS_ERROR;
    }

    memcpy(cfg_table, gt91x->chip.cfg_table, gt91x->chip.cfg_length);

    switch (cmd)
    {
    case OS_TOUCH_CTRL_GET_ID:
        err = gt91x_get_product_id(gt91x, data, 4);
        goto __ctl_end;
    case OS_TOUCH_CTRL_GET_INFO:
        err = gt91x_get_info(gt91x, data);
        break;
    case OS_TOUCH_CTRL_SET_MODE:

        break;
    case OS_TOUCH_CTRL_SET_X_RANGE:
        buff         = *(os_uint16_t *)data;
        cfg_table[1] = buff & 0xFF;
        cfg_table[2] = buff >> 8;
        break;
    case OS_TOUCH_CTRL_SET_Y_RANGE:
        buff         = *(os_uint16_t *)data;
        cfg_table[3] = buff & 0xFF;
        cfg_table[4] = buff >> 8;
        break;
    case OS_TOUCH_CTRL_SET_USER_X_RANGE:

        break;
    case OS_TOUCH_CTRL_SET_USER_Y_RANGE:

        break;
    case OS_TOUCH_CTRL_SET_X_TO_Y:
        cfg_table[6] |= (1 << 3);
        break;
    case OS_TOUCH_CTRL_DISABLE_INT:
        err = gt91x_int_disable(gt91x);
        goto __ctl_end;
    case OS_TOUCH_CTRL_ENABLE_INT:
        err = gt91x_int_enable(gt91x);
        goto __ctl_end;
    default:
        break;
    }

    switch (gt91x->chip.type)
    {
    case GT911:
        for (i = 0; i < gt91x->chip.cfg_length - 2; i++)
        {
            check_sum += cfg_table[i];
        }

        cfg_table[gt91x->chip.cfg_length - 2] = (~(check_sum & 0xFF)) + 1;
        cfg_table[gt91x->chip.cfg_length - 1] = 1;

        break;
    case GT9157:
        for (i = 0; i < gt91x->chip.cfg_length; i++)
        {
            check_sum += cfg_table[i];
        }

        cfg_table[gt91x->chip.cfg_length]     = (~(check_sum & 0xFF)) + 1;
        cfg_table[gt91x->chip.cfg_length + 1] = 1;
        break;
    case GT917S:
        for (i = 0; i < (gt91x->chip.cfg_length - 3); i += 2)
        {
            check_sum += (cfg_table[i] << 8) + cfg_table[i + 1];
        }

        check_sum = 0 - check_sum;

        cfg_table[gt91x->chip.cfg_length - 3] = (check_sum >> 8) & 0xFF;
        cfg_table[gt91x->chip.cfg_length - 2] = check_sum & 0xFF;
        cfg_table[gt91x->chip.cfg_length - 1] = 0x01;
        break;
    case GT5688:
        for (i = 0; i < (gt91x->chip.cfg_length - 3); i += 2)
        {
            check_sum += (cfg_table[i] << 8) + cfg_table[i + 1];
        }

        check_sum = 0 - check_sum;

        cfg_table[gt91x->chip.cfg_length - 3] = (check_sum >> 8) & 0xFF;
        cfg_table[gt91x->chip.cfg_length - 2] = check_sum & 0xFF;
        cfg_table[gt91x->chip.cfg_length - 1] = 0x01;
        break;
    default:
        break;
    }

    if (gt91x_write_reg(gt91x, cfg_table, gt91x->chip.cfg_length + 2) != OS_EOK)
    {
        LOG_E(DBG_TAG, "write cfg failed");
        goto __ctl_end;
    }

    os_task_msleep(100);

__ctl_end:
    os_free(cfg_table);

    return err;
}

static struct os_touch_ops touch_ops = {
    .touch_readpoint = gt91x_read_point,
    .touch_control   = gt91x_control,
};

static void gt91x_irq_handler(void *args)
{
    os_touch_irq_notify((os_touch_t *)args);
}

static os_err_t gt9xx_hw_init(struct gt91x_touch *gt91x)
{
    char touch_id[5] = {0};

    os_pin_mode(OS_GT91X_RST_PIN, PIN_MODE_OUTPUT);
    os_pin_mode(OS_GT91X_IRQ_PIN, PIN_MODE_OUTPUT);

    os_pin_write(OS_GT91X_RST_PIN, PIN_LOW);
    os_pin_write(OS_GT91X_IRQ_PIN, PIN_LOW);

    os_task_msleep(10);

    if (gt91x->i2c_addr == 0x5D)
    {
        os_pin_write(OS_GT91X_IRQ_PIN, PIN_LOW);
    }
    else
    {
        os_pin_write(OS_GT91X_IRQ_PIN, PIN_HIGH);
    }

    os_task_msleep(1);
    os_pin_write(OS_GT91X_RST_PIN, PIN_HIGH);
    os_task_msleep(8);
    os_pin_write(OS_GT91X_IRQ_PIN, PIN_LOW);
    os_task_msleep(100);
    os_pin_mode(OS_GT91X_IRQ_PIN, PIN_MODE_INPUT);
    os_task_msleep(100);

    if (gt91x_get_product_id(gt91x, (os_uint8_t *)touch_id, 4) != OS_EOK)
    {
        LOG_E(DBG_TAG, "gt91x_get_product_id failed");
        return OS_ERROR;
    }

    if (!memcmp(touch_id, "911", 3))
    {
        gt91x->chip.type       = GT911;
        gt91x->chip.cfg_addr   = 0x8047;
        gt91x->chip.cfg_table  = CTP_CFG_GT911;
        gt91x->chip.cfg_length = sizeof(CTP_CFG_GT911);
    }
    else if (!memcmp(touch_id, "9157", 4))
    {
        gt91x->chip.type       = GT9157;
        gt91x->chip.cfg_addr   = 0x8047;
        gt91x->chip.cfg_table  = CTP_CFG_GT9157;
        gt91x->chip.cfg_length = sizeof(CTP_CFG_GT9157);
    }
    else if (!memcmp(touch_id, "917S", 4))
    {
        gt91x->chip.type       = GT917S;
        gt91x->chip.cfg_addr   = 0x8050;
        gt91x->chip.cfg_table  = CTP_CFG_GT917S;
        gt91x->chip.cfg_length = sizeof(CTP_CFG_GT917S);
    }
    else if (!memcmp(touch_id, "5688", 4))
    {
        gt91x->chip.type       = GT5688;
        gt91x->chip.cfg_addr   = 0x8050;
        gt91x->chip.cfg_table  = CTP_CFG_GT5688;
        gt91x->chip.cfg_length = sizeof(CTP_CFG_GT5688);
    }
    else if (!memcmp(touch_id, "1158", 4))
    {
        gt91x->chip.type       = GT9147;
        gt91x->chip.cfg_addr   = 0x8050;
        gt91x->chip.cfg_table  = CTP_CFG_GT9147;
        gt91x->chip.cfg_length = sizeof(CTP_CFG_GT9147);
    }
    else
    {
        LOG_E(DBG_TAG, "touch_id %s not support", touch_id);
        return OS_ERROR;
    }

    if (gt9xx_read_config(gt91x) != OS_EOK)
    {
        LOG_E(DBG_TAG, "gt9xx_read_config failed");
        return OS_ERROR;
    }

    gt91x_soft_reset(gt91x);

    gt91x_control(&gt91x->touch_device, OS_TOUCH_CTRL_SET_X_RANGE, &gt91x->touch_device.info.range_x);
    gt91x_control(&gt91x->touch_device, OS_TOUCH_CTRL_SET_Y_RANGE, &gt91x->touch_device.info.range_y);

    return OS_EOK;
}

static int os_hw_gt91x_init(void)
{
    struct gt91x_touch *gt91x = os_calloc(1, sizeof(struct gt91x_touch));
    OS_ASSERT(gt91x);

    gt91x->i2c_addr = OS_GT91X_I2C_ADDR;
    gt91x->i2c_bus  = os_i2c_bus_device_find(OS_GT91X_I2C_BUS_NAME);
    OS_ASSERT(gt91x->i2c_bus);

    if (os_device_open((os_device_t *)gt91x->i2c_bus) != OS_EOK)
    {
        LOG_E(DBG_TAG, "open device failed");
        return OS_EOK;
    }

    if (gt9xx_hw_init(gt91x) != OS_EOK)
    {
        LOG_E(DBG_TAG, "gt9xx_hw_init failed");
        return OS_EOK;
    }

    gt91x->i2c_bus->addr = OS_GT91X_I2C_ADDR;

    /* register touch device */
    gt91x->touch_device.info.type      = OS_TOUCH_TYPE_CAPACITANCE;
    gt91x->touch_device.info.vendor    = OS_TOUCH_VENDOR_GT;
    gt91x->touch_device.info.point_num = GT91X_MAX_TOUCH;
    gt91x->touch_device.info.range_x   = OS_GRAPHIC_LCD_WIDTH;
    gt91x->touch_device.info.range_y   = OS_GRAPHIC_LCD_HEIGHT;
    gt91x->touch_device.ops            = &touch_ops;

    os_hw_touch_register(&gt91x->touch_device, "touch", gt91x);

    os_pin_attach_irq(OS_GT91X_IRQ_PIN, PIN_IRQ_MODE_RISING, gt91x_irq_handler, (void *)&gt91x->touch_device);
    os_pin_irq_enable(OS_GT91X_IRQ_PIN, PIN_IRQ_ENABLE);

    return OS_EOK;
}

OS_CMPOENT_INIT(os_hw_gt91x_init, OS_INIT_SUBLEVEL_LOW);
