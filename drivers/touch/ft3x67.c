/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        ft3x67.c
 *
 * @brief       ft3x67
 *
 * @details     ft3x67
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include <os_task.h>
#include <device.h>
#include <os_clock.h>
#include <i2c.h>
#include <string.h>
#include <stdlib.h>
#include <os_errno.h>
#include <os_memory.h>
#include <pin/pin.h>
#include <string.h>
#include <drv_gpio.h>
#include <drv_log.h>

#include "touch.h"
#include "ft3x67.h"

struct ft3x67_touch
{
    os_touch_t                touch_device;
    struct os_i2c_bus_device *i2c_bus;
    os_uint16_t               i2c_addr;
    os_uint16_t               id;
    /* field holding the current number of simultaneous active touches */
    os_uint8_t currActiveTouchNb;
    /* field holding the touch index currently managed */
    os_uint8_t currActiveTouchIdx;
};

/****************************** ft3x67 DRV functions ******************************/
static void TS_IO_Write(struct ft3x67_touch *ft3x67, uint8_t reg, uint8_t value)
{
    unsigned char buff[2] = {reg, value};
    os_i2c_master_send(ft3x67->i2c_bus, ft3x67->i2c_addr, 0, buff, 2);
}

static uint8_t TS_IO_Read(struct ft3x67_touch *ft3x67, uint8_t reg)
{
    unsigned char value;
    os_i2c_master_send(ft3x67->i2c_bus, ft3x67->i2c_addr, 0, &reg, 1);
    os_i2c_master_recv(ft3x67->i2c_bus, ft3x67->i2c_addr, 0, &value, 1);
    return value;
}

static uint16_t TS_IO_ReadMultiple(struct ft3x67_touch *ft3x67, uint8_t reg, uint8_t *buffer, uint16_t length)
{
    os_i2c_master_send(ft3x67->i2c_bus, ft3x67->i2c_addr, 0, &reg, 1);
    os_i2c_master_recv(ft3x67->i2c_bus, ft3x67->i2c_addr, 0, buffer, length);
    return 0;
}

static void TS_IO_Delay(uint32_t delay)
{
    os_task_msleep(delay);
}

static void ft3x67_Reset(struct ft3x67_touch *ft3x67)
{
    /* Do nothing */
    /* No software reset sequence available in FT3X67 IC */
}

static uint16_t ft3x67_ReadID(struct ft3x67_touch *ft3x67)
{
    /* Return the device ID value */
    return (TS_IO_Read(ft3x67, FT3X67_CHIP_ID_REG));
}

static void ft3x67_TS_EnableIT(struct ft3x67_touch *ft3x67)
{
    /* Set interrupt trigger mode in FT3X67_GMODE_REG */
    TS_IO_Write(ft3x67, FT3X67_GMODE_REG, FT3X67_G_MODE_INTERRUPT_TRIGGER);
}

static void ft3x67_TS_DisableIT(struct ft3x67_touch *ft3x67)
{
    /* Set interrupt polling mode in FT3X67_GMODE_REG */
    TS_IO_Write(ft3x67, FT3X67_GMODE_REG, FT3X67_G_MODE_INTERRUPT_POLLING);
}

static uint32_t ft3x67_TS_Configure(struct ft3x67_touch *ft3x67)
{
    uint32_t status = FT3X67_STATUS_OK;

    /* Disable gesture feature */
    TS_IO_Write(ft3x67, FT3X67_GESTURE_ENABLE_REG, FT3X67_GESTURE_DISABLE);

    return (status);
}

static void ft3x67_TS_Start(struct ft3x67_touch *ft3x67)
{
    /* Minimum static configuration of FT3X67 */
    ft3x67_TS_Configure(ft3x67);

    /* By default set FT3X67 IC in Polling mode : no INT generation on FT3X67 for new touch available */
    /* Note TS_INT is active low                                                                      */
    // ft3x67_TS_DisableIT(ft3x67);

    ft3x67_TS_EnableIT(ft3x67);
}

static uint8_t ft3x67_TS_DetectTouch(struct ft3x67_touch *ft3x67)
{
    volatile uint8_t nbTouch = 0U;

    /* Read register FT3X67_TD_STAT_REG to check number of touches detection */
    nbTouch = TS_IO_Read(ft3x67, FT3X67_TD_STAT_REG);
    nbTouch &= FT3X67_TD_STAT_MASK;

    if (nbTouch > FT3X67_MAX_DETECTABLE_TOUCH)
    {
        /* If invalid number of touch detected, set it to zero */
        nbTouch = 0U;
    }

    /* Update ft3x67 driver internal global : current number of active touches */
    ft3x67->currActiveTouchNb = nbTouch;

    /* Reset current active touch index on which to work on */
    ft3x67->currActiveTouchIdx = 0U;

    return (nbTouch);
}

static void ft3x67_TS_GetXY(struct ft3x67_touch *ft3x67, uint16_t *X, uint16_t *Y, uint16_t *event)
{
    uint8_t regAddress = 0U;
    uint8_t dataxy[4U];

    // if(ft3x67->currActiveTouchIdx < ft3x67->currActiveTouchNb)
    {
        switch (ft3x67->currActiveTouchIdx)
        {
        case 0U:
            regAddress = FT3X67_P1_XH_REG;
            break;

        case 1U:
            regAddress = FT3X67_P2_XH_REG;
            break;

        default:
            break;
        } /* end switch(ft3x67_handle.currActiveTouchIdx) */

        /* Read X and Y positions */
        TS_IO_ReadMultiple(ft3x67, regAddress, dataxy, sizeof(dataxy));

        /* Send back ready X position to caller */
        *X = ((dataxy[0U] & FT3X67_TOUCH_POS_MSB_MASK) << 8U) | dataxy[1U];

        /* Send back ready Y position to caller */
        *Y = ((dataxy[2U] & FT3X67_TOUCH_POS_MSB_MASK) << 8U) | dataxy[3U];

        *event = (dataxy[0U] & FT3X67_TOUCH_EVT_FLAG_MASK) >> FT3X67_TOUCH_EVT_FLAG_SHIFT;

        /* Increment current touch index */
        ft3x67->currActiveTouchIdx++;
    }
}

static void ft3x67_TS_GestureConfig(struct ft3x67_touch *ft3x67, uint32_t Activation)
{
    if (Activation == FT3X67_GESTURE_ENABLE)
    {
        /* Enable gesture feature. */
        TS_IO_Write(ft3x67, FT3X67_GESTURE_FLAG_REG, FT3X67_GEST_ALL_FLAGS_ENABLE);
        TS_IO_Write(ft3x67, FT3X67_GESTURE_ENABLE_REG, FT3X67_GESTURE_ENABLE);
    }
    else
    {
        /* Disable gesture feature. */
        TS_IO_Write(ft3x67, FT3X67_GESTURE_FLAG_REG, FT3X67_GEST_ALL_FLAGS_DISABLE);
        TS_IO_Write(ft3x67, FT3X67_GESTURE_ENABLE_REG, FT3X67_GESTURE_DISABLE);
    }
}

static void ft3x67_TS_GetGestureID(struct ft3x67_touch *ft3x67, uint32_t *pGestureId)
{
    volatile uint8_t ucReadData = 0U;

    ucReadData  = TS_IO_Read(ft3x67, FT3X67_GEST_ID_REG);
    *pGestureId = ucReadData;
}

static void ft3x67_TS_GetTouchInfo(struct ft3x67_touch *ft3x67,
                                   uint32_t             touchIdx,
                                   uint32_t            *pWeight,
                                   uint32_t            *pArea,
                                   uint32_t            *pEvent)
{
    volatile uint8_t ucReadData      = 0U;
    uint8_t          regAddressXHigh = 0U;

    if (touchIdx < ft3x67->currActiveTouchNb)
    {
        switch (touchIdx)
        {
        case 0U:
            regAddressXHigh = FT3X67_P1_XH_REG;
            break;

        case 1U:
            regAddressXHigh = FT3X67_P2_XH_REG;
            break;

        default:
            break;
        } /* end switch(touchIdx) */

        /* Read Event Id of touch index */
        ucReadData = TS_IO_Read(ft3x67, regAddressXHigh);
        *pEvent    = (ucReadData & FT3X67_TOUCH_EVT_FLAG_MASK) >> FT3X67_TOUCH_EVT_FLAG_SHIFT;

        /* Weight and area of touch index not supported by FT3X67 */
        *pWeight = 0;
        *pArea   = 0;
    }
}

void ft3x67_TS_ClearIT(struct ft3x67_touch *ft3x67, uint32_t IO_Pin)
{
    /* Clear the IO IT pending bit(s) by acknowledging */
    /* it cleans automatically also the Global IRQ_GPIO */
    /* normally this function is called under interrupt */

    uint8_t pin_0_7, pin_8_15, pin_16_23;

    ft3x67->i2c_addr = 0x42;

    pin_0_7   = IO_Pin & 0x0000ff;
    pin_8_15  = IO_Pin >> 8;
    pin_8_15  = pin_8_15 & 0x00ff;
    pin_16_23 = IO_Pin >> 16;

    if (pin_0_7)
    {
        TS_IO_Write(ft3x67, MFXSTM32L152_REG_ADR_IRQ_GPI_ACK1, pin_0_7);
    }

    if (pin_8_15)
    {
        TS_IO_Write(ft3x67, MFXSTM32L152_REG_ADR_IRQ_GPI_ACK2, pin_8_15);
    }

    if (pin_16_23)
    {
        TS_IO_Write(ft3x67, MFXSTM32L152_REG_ADR_IRQ_GPI_ACK3, pin_16_23);
    }

    ft3x67->i2c_addr = 0x38;
}

static os_size_t ft3x67_read_point(struct os_touch_device *touch, struct os_touch_data *data, os_size_t read_num)
{
    uint16_t input_x  = 0;
    uint16_t input_y  = 0;
    uint16_t input_w  = 0;
    uint16_t data_num = 0;
    uint16_t event    = FT3X67_TOUCH_EVT_FLAG_NO_EVENT;

    static uint16_t last_x = 0;
    static uint16_t last_y = 0;

    struct ft3x67_touch *ft3x67 = (struct ft3x67_touch *)touch;

    ft3x67_TS_ClearIT(ft3x67, MFXSTM32L152_TS_ClearIT);

    if (0 == ft3x67_TS_DetectTouch(ft3x67))
    {
        if (0 != last_x || 0 != last_x)
        {
            data->event        = OS_TOUCH_EVENT_UP;
            data->timestamp    = os_touch_get_ts();
            data->width        = input_w;
            data->x_coordinate = last_x;
            data->y_coordinate = last_y;
            data->track_id     = 0;

            last_x = 0;
            last_y = 0;

            return 1;
        }
        else
            return 0;
    }

    ft3x67_TS_GetXY(ft3x67, &input_x, &input_y, &event);

    // os_kprintf("ft3x67_read_point:%d,%d,event:%d \r\n",input_x,input_y,event);

    switch (event)
    {
    case FT3X67_TOUCH_EVT_FLAG_PRESS_DOWN:
        data->event = OS_TOUCH_EVENT_DOWN;
        break;
    case FT3X67_TOUCH_EVT_FLAG_LIFT_UP:
        data->event = OS_TOUCH_EVENT_UP;
        break;
    case FT3X67_TOUCH_EVT_FLAG_CONTACT:
        data->event = OS_TOUCH_EVENT_MOVE;
        break;
    default:
        data->event = OS_TOUCH_EVENT_NONE;
        return 0;
    }

    data->timestamp    = os_touch_get_ts();
    data->width        = input_w;
    data->x_coordinate = input_x;
    data->y_coordinate = input_y;
    data->track_id     = 0;

    last_x = input_x;
    last_y = input_y;

    return 1;
}

static os_err_t ft3x67_control(struct os_touch_device *device, int cmd, void *data)
{
    struct ft3x67_touch *ft3x67 = (struct ft3x67_touch *)device;

    switch (cmd)
    {
    case OS_TOUCH_CTRL_GET_ID:
        break;
    case OS_TOUCH_CTRL_GET_INFO:
        *(struct os_touch_info *)data = device->info;
        break;
    case OS_TOUCH_CTRL_SET_MODE: /* change int trig type */
        break;
    case OS_TOUCH_CTRL_ENABLE_INT:
        os_pin_irq_enable(OS_FT3X67_IRQ_PIN, PIN_IRQ_ENABLE);
        break;
    case OS_TOUCH_CTRL_DISABLE_INT:
        os_pin_irq_enable(OS_FT3X67_IRQ_PIN, PIN_IRQ_DISABLE);
        break;
    default:
        break;
    }

    return OS_EOK;
}

static void ft3x67_irq_handler(void *args)
{
    os_touch_irq_notify((os_touch_t *)args);
}

static struct os_touch_ops ft3x67_touch_ops = {
    .touch_readpoint = ft3x67_read_point,
    .touch_control   = ft3x67_control,
};

static int os_hw_ft3x67_init(void)
{
    struct ft3x67_touch *ft3x67 = os_calloc(1, sizeof(struct ft3x67_touch));
    OS_ASSERT(ft3x67);

    ft3x67->i2c_addr = FT3X67_RT_I2C_ADDRESS;
    ft3x67->i2c_bus  = os_i2c_bus_device_find(OS_FT3X67_I2C_BUS_NAME);
    OS_ASSERT(ft3x67->i2c_bus);

    ft3x67->id = ft3x67_ReadID(ft3x67);
    if (ft3x67->id != FT3X67_ID_VALUE)
    {
        ft3x67->i2c_addr = 0x38;
        ft3x67->id       = ft3x67_ReadID(ft3x67);
    }

    ft3x67_TS_Start(ft3x67);

    os_touch_t *touch_device = &ft3x67->touch_device;
    /* register touch device */
    touch_device->info.type      = OS_TOUCH_TYPE_CAPACITANCE;
    touch_device->info.vendor    = OS_TOUCH_VENDOR_UNKNOWN;
    touch_device->info.point_num = 1;
    touch_device->info.range_x   = FT3X67_MAX_WIDTH;
    touch_device->info.range_y   = FT3X67_MAX_HEIGHT;
    touch_device->ops            = &ft3x67_touch_ops;

    os_hw_touch_register(touch_device, "touch", ft3x67);

    /* set irq handle */
    os_pin_mode(OS_FT3X67_IRQ_PIN, PIN_MODE_INPUT_PULLUP);
    os_pin_attach_irq(OS_FT3X67_IRQ_PIN, PIN_IRQ_MODE_RISING, ft3x67_irq_handler, (void *)touch_device);
    os_pin_irq_enable(OS_FT3X67_IRQ_PIN, PIN_IRQ_DISABLE);

    return 0;
}

OS_CMPOENT_INIT(os_hw_ft3x67_init, OS_INIT_SUBLEVEL_LOW);
