/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        plug.h
 *
 * @brief       Header file for plug interface.
 *
 * @revision
 * Date         Author          Notes
 * 2020-10-25   OneOS Team      First version.
 ***********************************************************************************************************************
 */

#ifndef __OS_PLUG_H__
#define __OS_PLUG_H__

#include <oneos_config.h>
#include <os_types.h>
#include <os_stddef.h>
#include <os_list.h>
#include <os_sem.h>
#include <os_errno.h>
#include <os_util.h>

typedef struct os_plug os_plug_t;

struct os_plug
{
    os_plug_t *upper;

    char tag[OS_NAME_MAX + 1];
    char name[OS_NAME_MAX + 1];

    os_ubase_t ref_count;

    os_sem_t sem;

    os_list_node_t list;

    os_err_t (*halt)(os_plug_t *plug);
    os_err_t (*release)(os_plug_t *plug);
};

os_err_t   os_plug_in(os_plug_t *plug, const char *tag, const char *name);
void       os_plug_out(os_plug_t *plug);
os_plug_t *os_plug_get(const char *tag, const char *name);
void       os_plug_put(os_plug_t *plug);

#endif
