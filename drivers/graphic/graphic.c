/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_lcd_mipi.c
 *
 * @brief       This file implements lcd mipi driver for stm32.
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include <board.h>
#include <drv_cfg.h>
#include <string.h>
#include <os_sem.h>
#include <os_memory.h>
#include <graphic/graphic.h>

static volatile os_uint8_t g_frame_id = 0;

static void os_graphic_frame_set(struct os_device *dev, os_uint8_t *fb)
{
    os_graphic_t *graphic = (os_graphic_t *)dev;

    if (fb)
        graphic->info.framebuffer_curr = fb;
}

static void os_graphic_frame_fill(struct os_device *dev, os_graphic_area_t *area)
{
    os_uint32_t        i, j;
    os_color_t        *pos;
    os_graphic_t      *graphic = (os_graphic_t *)dev;
    os_graphic_info_t *info    = &(graphic->info);

    os_uint32_t dst = (os_uint32_t)info->framebuffer_curr + area->y * graphic->info.bytes_per_line +
                      area->x * graphic->info.bytes_per_pixel;
    os_uint32_t src        = (os_uint32_t)area->buffer;
    os_uint32_t dst_offset = info->bytes_per_pixel * info->width;
    os_uint32_t src_offset = info->bytes_per_pixel * area->w;

    if (src != 0)
    {
        for (i = 0; i < area->h; i++)
        {
            memcpy((void *)dst, (void *)src, src_offset);
            dst += dst_offset;
            src += src_offset;
        }
    }
    else
    {
        for (i = 0; i < area->h; i++)
        {
            pos = (os_color_t *)(dst + i * graphic->info.bytes_per_line);
            for (j = 0; j < area->w; j++)
            {
                *pos = area->color;
                pos++;
            }
        }
    }
}

static void os_graphic_frame_next(struct os_device *dev, os_uint8_t **fb)
{
    os_graphic_t *graphic = (os_graphic_t *)dev;

    if (graphic->info.framebuffer_num == 2)
        *fb = graphic->info.framebuffer[g_frame_id];
    else if (graphic->info.framebuffer_num == 1)
        *fb = graphic->info.framebuffer[0];
    else
        *fb = OS_NULL;
}

os_err_t os_graphic_add_framebuffer(struct os_device *dev, os_uint8_t *fb, os_uint32_t size)
{
    os_graphic_t      *graphic = (os_graphic_t *)dev;
    os_graphic_info_t *info    = &graphic->info;

    if (info->framebuffer_num == 2)
        return OS_ERROR;

    // The size must be full screen
    if (size != info->framebuffer_size)
        return OS_ERROR;

    memset(fb, 0, size);

    info->framebuffer[info->framebuffer_num] = fb;
    info->framebuffer_num++;

    // Set framebuffer_curr if add the first framebuffer.
    if (info->framebuffer_num == 1)
        info->framebuffer_curr = fb;

    os_kprintf("Add framebuffer %d\r\n", info->framebuffer_num);
    return OS_EOK;
}

static os_err_t os_graphic_control(os_device_t *device, int cmd, void *args)
{
    os_graphic_t *graphic;
    os_err_t      ret = OS_EOK;

    OS_ASSERT(device);

    graphic = (os_graphic_t *)device;

    switch (cmd)
    {
    case OS_GRAPHIC_CTRL_POWERON:
        if (graphic->ops->display_on)
        {
            graphic->ops->display_on(device, OS_TRUE);
        }
        break;
    case OS_GRAPHIC_CTRL_POWEROFF:
        if (graphic->ops->display_on)
        {
            graphic->ops->display_on(device, OS_FALSE);
        }
        break;
    case OS_GRAPHIC_CTRL_GET_INFO:
        memcpy(args, &graphic->info, sizeof(os_graphic_info_t));
        break;
    case OS_GRAPHIC_CTRL_DISP_AREA:
        if (graphic->ops->display_area)
        {
            graphic->ops->display_area(device, (os_graphic_area_t *)args);
        }
        break;
    case OS_GRAPHIC_CTRL_FRAME_NEXT:
        os_graphic_frame_next(device, (os_uint8_t **)args);
        break;
    case OS_GRAPHIC_CTRL_FRAME_SET:
        os_graphic_frame_set(device, (os_uint8_t *)args);
        break;
    case OS_GRAPHIC_CTRL_FRAME_FILL:
        os_graphic_frame_fill(device, (os_graphic_area_t *)args);
        break;
    case OS_GRAPHIC_CTRL_FRAME_FLUSH:
        if (graphic->ops->frame_flush)
        {
            graphic->ops->frame_flush(device);
            g_frame_id = 1 - g_frame_id;
        }
        break;

#ifdef OS_GRAPHIC_DRAW_ENABLE
    case OS_GRAPHIC_CTRL_DRAW_POINT:
        if (graphic->draw_ops->draw_point)
        {
            graphic->draw_ops->draw_point(graphic, (os_graphic_point_t *)args);
        }
        break;
    case OS_GRAPHIC_CTRL_DRAW_LINE:
        if (graphic->draw_ops->draw_line)
        {
            graphic->draw_ops->draw_line(graphic, (os_graphic_line_t *)args);
        }
        break;
    case OS_GRAPHIC_CTRL_DRAW_RECT:
        if (graphic->draw_ops->draw_rect)
        {
            graphic->draw_ops->draw_rect(graphic, (os_graphic_rect_t *)args);
        }
        break;
    case OS_GRAPHIC_CTRL_DRAW_CIRCLE:
        if (graphic->draw_ops->draw_circle)
        {
            graphic->draw_ops->draw_circle(graphic, (os_graphic_circle_t *)args);
        }
        break;
    case OS_GRAPHIC_CTRL_DRAW_SOLID_RECT:
        if (graphic->draw_ops->draw_solid_rect)
        {
            graphic->draw_ops->draw_solid_rect(graphic, (os_graphic_rect_t *)args);
        }
        break;
    case OS_GRAPHIC_CTRL_DRAW_SOLID_CIRCLE:
        if (graphic->draw_ops->draw_solid_circle)
        {
            graphic->draw_ops->draw_solid_circle(graphic, (os_graphic_circle_t *)args);
        }
        break;
    case OS_GRAPHIC_CTRL_DRAW_IMAGE:
        if (graphic->draw_ops->draw_image)
        {
            graphic->draw_ops->draw_image(graphic, (os_graphic_area_t *)args);
        }
        break;
    case OS_GRAPHIC_CTRL_DRAW_CHAR:
        if (graphic->draw_ops->draw_char)
        {
            graphic->draw_ops->draw_char(graphic, (os_uint32_t)args);
        }
        break;
    case OS_GRAPHIC_CTRL_DRAW_STRING:
        if (graphic->draw_ops->draw_string)
        {
            graphic->draw_ops->draw_string(graphic, (os_uint8_t *)args);
        }
        break;
    case OS_GRAPHIC_CTRL_DRAW_SET_TXT_CURSOR:
        if (graphic->draw_ops->set_txt_cursor)
        {
            graphic->draw_ops->set_txt_cursor(graphic, (os_graphic_pos_t *)args);
        }
        break;
    case OS_GRAPHIC_CTRL_DRAW_SET_TXT_COLOR:
        if (graphic->draw_ops->set_txt_color)
        {
            graphic->draw_ops->set_txt_color(graphic, (os_color_t *)args);
        }
        break;
    case OS_GRAPHIC_CTRL_DRAW_ADD_FONT:
        if (graphic->draw_ops->add_font)
        {
            ret = graphic->draw_ops->add_font(graphic, (os_graphic_font_t *)args);
        }
        break;
    case OS_GRAPHIC_CTRL_DRAW_SET_FONT:
        if (graphic->draw_ops->set_font)
        {
            ret = graphic->draw_ops->set_font(graphic, (char *)args);
        }
        break;
    case OS_GRAPHIC_CTRL_DRAW_CLEAR:
        if (graphic->draw_ops->clear)
        {
            graphic->draw_ops->clear(graphic, (os_color_t *)args);
        }
        break;
#endif
#ifdef OS_GRAPHIC_GPU_ENABLE
    case OS_GRAPHIC_CTRL_GPU_BLEND:
        if (graphic->ops->gpu_blend)
        {
            graphic->ops->gpu_blend(device, (struct os_device_gpu_info *)args);
        }
        break;
    case OS_GRAPHIC_CTRL_GPU_ALPHAMAP_BLEND:
        if (graphic->ops->gpu_blend)
        {
            graphic->ops->gpu_alphamap_blend(device, (struct os_device_gpu_info *)args);
        }
        break;
    case OS_GRAPHIC_CTRL_GPU_BLIT:
        if (graphic->ops->gpu_blit)
        {
            graphic->ops->gpu_blit(device, (struct os_device_gpu_info *)args);
        }
        break;
    case OS_GRAPHIC_CTRL_GPU_WAIT:
        if (graphic->ops->gpu_wait_finish)
        {
            graphic->ops->gpu_wait_finish(device);
        }
        break;
#endif
    default:
        break;
    }

    return ret;
}

const static struct os_device_ops graphic_ops = {
    .control = os_graphic_control,
};

#ifdef OS_GRAPHIC_DRAW_ENABLE

#define OS_GRAPHIC_CHR_BUF_SIZE 64
static os_list_node_t     g_font_list = OS_LIST_INIT(g_font_list);
static os_graphic_font_t *g_font_set  = OS_NULL;
static os_color_t         g_txt_color;
static os_graphic_pos_t   g_txt_cusor = {0, 0};
static os_uint8_t         g_char_buf[OS_GRAPHIC_CHR_BUF_SIZE];

static void os_graphic_draw_point(os_graphic_t *graphic, os_graphic_point_t *point)
{
    os_graphic_area_t area;
    os_uint8_t       *fb;

    if (graphic->info.framebuffer_num > 0)
    {
        fb = graphic->info.framebuffer_curr;
        OS_ASSERT(fb);
        fb += point->y * graphic->info.bytes_per_line + point->x * graphic->info.bytes_per_pixel;
        *(os_color_t *)fb = point->color;
    }
    else
    {
        area.x      = point->x;
        area.y      = point->y;
        area.w      = 1;
        area.h      = 1;
        area.color  = point->color;
        area.buffer = OS_NULL;
        graphic->ops->display_area(&graphic->parent, &area);
    }
}

static void os_graphic_draw_line(os_graphic_t *graphic, os_graphic_line_t *line)
{
    os_uint8_t        *fb = 0;
    os_int32_t         i = 0, xerr = 0, yerr = 0;
    os_int32_t         delta_x, delta_y, distance, incx, incy, row, col;
    os_int32_t         len;
    os_color_t        *pos;
    os_graphic_area_t  area;
    os_graphic_point_t point;

    if (line->y1 == line->y2)
    {
        len = (line->x2 > line->x1) ? (line->x2 - line->x1) : (line->x1 - line->x2);

        if (graphic->info.framebuffer_num > 0)
        {
            fb = graphic->info.framebuffer_curr;
            OS_ASSERT(fb);
            fb += line->y1 * graphic->info.bytes_per_line + line->x1 * graphic->info.bytes_per_pixel;
            pos = (os_color_t *)fb;

            for (i = 0; i < len; i++)
            {
                *pos = line->color;
                pos++;
            }
        }
        else
        {
            area.x      = line->x1;
            area.y      = line->y1;
            area.w      = line->x2 - line->x1;
            area.h      = 1;
            area.color  = line->color;
            area.buffer = OS_NULL;
            graphic->ops->display_area(&graphic->parent, &area);
        }
        return;
    }

    delta_x = line->x2 - line->x1;
    delta_y = line->y2 - line->y1;
    row     = line->x1;
    col     = line->y1;
    if (delta_x > 0)
        incx = 1;
    else if (delta_x == 0)
        incx = 0;
    else
    {
        incx    = -1;
        delta_x = -delta_x;
    }
    if (delta_y > 0)
        incy = 1;
    else if (delta_y == 0)
        incy = 0;
    else
    {
        incy    = -1;
        delta_y = -delta_y;
    }
    if (delta_x > delta_y)
        distance = delta_x;
    else
        distance = delta_y;
    for (i = 0; i <= distance + 1; i++)
    {
        point.color = line->color;
        point.x     = row;
        point.y     = col;
        os_graphic_draw_point(graphic, &point);

        xerr += delta_x;
        yerr += delta_y;
        if (xerr > distance)
        {
            xerr -= distance;
            row += incx;
        }
        if (yerr > distance)
        {
            yerr -= distance;
            col += incy;
        }
    }
}

static void os_graphic_draw_rect(os_graphic_t *graphic, os_graphic_rect_t *rect)
{
    os_graphic_line_t line;

    os_int16_t x1 = rect->x;
    os_int16_t x2 = rect->x + rect->w;
    os_int16_t y1 = rect->y;
    os_int16_t y2 = rect->y + rect->h;

    line.color = rect->color;
    line.x1    = x1;
    line.y1    = y1;
    line.x2    = x2;
    line.y2    = y1;
    os_graphic_draw_line(graphic, &line);

    line.x1 = x1;
    line.y1 = y2;
    line.x2 = x2;
    line.y2 = y2;
    os_graphic_draw_line(graphic, &line);

    line.x1 = x1;
    line.y1 = y1;
    line.x2 = x1;
    line.y2 = y2;
    os_graphic_draw_line(graphic, &line);

    line.x1 = x2;
    line.y1 = y1;
    line.x2 = x2;
    line.y2 = y2;
    os_graphic_draw_line(graphic, &line);
}

static void os_graphic_draw_circle(os_graphic_t *graphic, os_graphic_circle_t *circle)
{
    os_graphic_point_t point;
    os_int16_t         x  = circle->x;
    os_int16_t         y  = circle->y;
    os_int16_t         a  = 0;
    os_int16_t         b  = circle->r;
    os_int16_t         di = 3 - (circle->r << 1);

    while (a <= b)
    {
        point.color = circle->color;
        point.x     = circle->x - b;
        point.y     = y - a;
        os_graphic_draw_point(graphic, &point);
        point.x = x + b;
        point.y = y - a;
        os_graphic_draw_point(graphic, &point);
        point.x = x - a;
        point.y = y + b;
        os_graphic_draw_point(graphic, &point);
        point.x = x - b;
        point.y = y - a;
        os_graphic_draw_point(graphic, &point);
        point.x = x - a;
        point.y = y - b;
        os_graphic_draw_point(graphic, &point);
        point.x = x + b;
        point.y = y + a;
        os_graphic_draw_point(graphic, &point);
        point.x = x + a;
        point.y = y - b;
        os_graphic_draw_point(graphic, &point);
        point.x = x + a;
        point.y = y + b;
        os_graphic_draw_point(graphic, &point);
        point.x = x - b;
        point.y = y + a;
        os_graphic_draw_point(graphic, &point);

        a++;

        /* Bresenham */
        if (di < 0)
            di += 4 * a + 6;
        else
        {
            di += 10 + 4 * (a - b);
            b--;
        }

        point.x = x + a;
        point.y = y + b;
        os_graphic_draw_point(graphic, &point);
    }
}
static void os_graphic_draw_solid_rect(os_graphic_t *graphic, os_graphic_rect_t *rect)
{
    os_int32_t        i, j;
    os_uint8_t       *fb = 0;
    os_color_t       *pos;
    os_graphic_area_t area;

    if (graphic->info.framebuffer_num > 0)
    {
        fb = graphic->info.framebuffer_curr;
        OS_ASSERT(fb);
        fb += rect->y * graphic->info.bytes_per_line + rect->x * graphic->info.bytes_per_pixel;

        for (i = 0; i < rect->h; i++)
        {
            pos = (os_color_t *)(fb + i * graphic->info.bytes_per_line);
            for (j = 0; j < rect->w; j++)
            {
                *pos = rect->color;
                pos++;
            }
        }
    }
    else
    {
        area.x      = rect->x;
        area.y      = rect->y;
        area.w      = rect->w;
        area.h      = rect->h;
        area.color  = rect->color;
        area.buffer = OS_NULL;
        graphic->ops->display_area(&graphic->parent, &area);
    }
}

static void os_graphic_draw_solid_circle(os_graphic_t *graphic, os_graphic_circle_t *circle)
{
    int32_t           r  = circle->r;
    int32_t           x  = 0;
    int32_t           dx = 1;
    int32_t           dy = r + r;
    int32_t           p  = -(r >> 1);
    os_graphic_line_t line;

    line.color = circle->color;
    line.x1    = circle->x - r;
    line.y1    = circle->y;
    line.x2    = line.x1 + dy;
    line.y2    = line.y1;
    os_graphic_draw_line(graphic, &line);

    while (x < r)
    {
        if (p >= 0)
        {
            line.x1 = circle->x - x;
            line.y1 = circle->y + r;
            line.x2 = line.x1 + dx - 1;
            line.y2 = line.y1;
            os_graphic_draw_line(graphic, &line);
            line.y1 = circle->y - r;
            line.y2 = line.y1;
            os_graphic_draw_line(graphic, &line);
            dy -= 2;
            p -= dy;
            r--;
        }

        dx += 2;
        p += dx;
        x++;

        line.x1 = circle->x - r;
        line.y1 = circle->y + x;
        line.x2 = line.x1 + dy;
        line.y2 = line.y1;
        os_graphic_draw_line(graphic, &line);
        line.y1 = circle->y - x;
        line.y2 = line.y1;
        os_graphic_draw_line(graphic, &line);
    }
}

static void os_graphic_draw_image(os_graphic_t *graphic, os_graphic_area_t *area)
{
    if (graphic->info.framebuffer_num > 0)
    {
        os_graphic_frame_fill(&graphic->parent, area);
    }
    else
    {
        graphic->ops->display_area(&graphic->parent, area);
    }
}

static void os_graphic_draw_char(os_graphic_t *graphic, os_int32_t index)
{
    os_int32_t         i, w, row, col, offset;
    os_uint8_t         tmp;
    os_color_t        *pos;
    os_graphic_point_t point;

    if (g_font_set == OS_NULL)
        return;

    w = g_font_set->width / 8;
    if (g_font_set->width % 8)
        w += 1;

    g_font_set->get_char(index, g_char_buf, OS_GRAPHIC_CHR_BUF_SIZE);

    if (graphic->info.framebuffer_num > 0)
    {
        offset = g_txt_cusor.y * graphic->info.bytes_per_line + g_txt_cusor.x * graphic->info.bytes_per_pixel;

        for (row = 0; row < g_font_set->height; row++)
        {
            pos = (os_color_t *)(graphic->info.framebuffer_curr + offset + row * graphic->info.bytes_per_line);
            for (col = 0; col < w; col++)
            {
                tmp = g_char_buf[row * w + col];
                for (i = 0; i < 8; i++)
                {
                    if (tmp & 0x80)
                    {
                        *pos = g_txt_color;
                    }
                    pos++;
                    tmp <<= 1;
                }
            }
        }
    }
    else
    {
        point.color = g_txt_color;
        for (row = 0; row < g_font_set->height; row++)
        {
            point.x = g_txt_cusor.x;
            point.y = g_txt_cusor.y + row;
            for (col = 0; col < w; col++)
            {
                tmp = g_char_buf[row * w + col];
                for (i = 0; i < 8; i++)
                {
                    if (tmp & 0x80)
                    {
                        os_graphic_draw_point(graphic, &point);
                    }
                    point.x += 1;
                    tmp <<= 1;
                }
            }
        }
    }
}

static void os_graphic_draw_string(os_graphic_t *graphic, os_uint8_t *str)
{
    os_uint32_t i = 0, index;

    while (str[i] != '\0')
    {
        switch (g_font_set->bytes)
        {
        case 1:
            index = str[i];
            i++;
            break;
        case 2:
            index = str[i] << 8 | str[i + 1];
            i += 2;
            break;
        default:
            return;
        }

        os_graphic_draw_char(graphic, index);

        g_txt_cusor.x += g_font_set->width;
        if (g_txt_cusor.x + g_font_set->width > graphic->info.width)
            return;
    }
}

static void os_graphic_set_txt_cursor(os_graphic_t *graphic, struct os_graphic_pos *pos)
{
    g_txt_cusor.x = pos->x;
    g_txt_cusor.y = pos->y;
}
static void os_graphic_set_txt_color(os_graphic_t *graphic, os_color_t *color)
{
    g_txt_color = *color;
}

static os_err_t os_graphic_add_font(os_graphic_t *graphic, os_graphic_font_t *font)
{
    os_graphic_font_t *f = (os_graphic_font_t *)os_malloc(sizeof(os_graphic_font_t));
    f->name              = font->name;
    f->width             = font->width;
    f->height            = font->height;
    f->bytes             = font->bytes;
    f->get_char          = font->get_char;

    os_list_add(&g_font_list, &f->list);

    if (g_font_set == OS_NULL)
    {
        g_font_set = f;
    }

    return OS_EOK;
}

static os_err_t os_graphic_set_font(os_graphic_t *graphic, char *name)
{
    os_graphic_font_t *font;

    os_list_for_each_entry(font, &g_font_list, os_graphic_font_t, list)
    {
        if (0 == strncmp(font->name, name, OS_NAME_MAX))
        {
            g_font_set = font;
            return OS_EOK;
        }
    }

    return OS_ERROR;
}

static void os_graphic_clear(os_graphic_t *graphic, os_color_t *color)
{
    os_graphic_rect_t rect;

    rect.x     = 0;
    rect.y     = 0;
    rect.w     = graphic->info.width;
    rect.h     = graphic->info.height;
    rect.color = *color;
    os_graphic_draw_solid_rect(graphic, &rect);
}

const static struct os_graphic_draw_ops draw_ops = {
    .draw_point        = os_graphic_draw_point,
    .draw_line         = os_graphic_draw_line,
    .draw_rect         = os_graphic_draw_rect,
    .draw_circle       = os_graphic_draw_circle,
    .draw_solid_rect   = os_graphic_draw_solid_rect,
    .draw_solid_circle = os_graphic_draw_solid_circle,
    .draw_image        = os_graphic_draw_image,
    .draw_char         = os_graphic_draw_char,
    .draw_string       = os_graphic_draw_string,
    .set_txt_cursor    = os_graphic_set_txt_cursor,
    .set_txt_color     = os_graphic_set_txt_color,
    .set_font          = os_graphic_set_font,
    .add_font          = os_graphic_add_font,
    .clear             = os_graphic_clear,
};
#endif

os_err_t os_graphic_register(const char *name, os_graphic_t *graphic)
{
    OS_ASSERT(graphic != OS_NULL);
    OS_ASSERT(graphic->ops != OS_NULL);

    graphic->parent.type = OS_DEVICE_TYPE_GRAPHIC;
    graphic->parent.ops  = &graphic_ops;
#ifdef OS_GRAPHIC_DRAW_ENABLE
    graphic->draw_ops = &draw_ops;
#endif

    graphic->info.width           = OS_GRAPHIC_LCD_WIDTH;
    graphic->info.height          = OS_GRAPHIC_LCD_HEIGHT;
    graphic->info.pixel_format    = OS_GRAPHIC_LCD_FORMAT;
    graphic->info.bits_per_pixel  = OS_GRAPHIC_LCD_DEPTH;
    graphic->info.bytes_per_pixel = OS_GRAPHIC_LCD_DEPTH / 8;
    graphic->info.bytes_per_line  = OS_GRAPHIC_LCD_WIDTH * (OS_GRAPHIC_LCD_DEPTH / 8);

    graphic->info.framebuffer[0]   = OS_NULL;
    graphic->info.framebuffer[1]   = OS_NULL;
    graphic->info.framebuffer_curr = OS_NULL;
    graphic->info.framebuffer_num  = 0;
    graphic->info.framebuffer_size = graphic->info.bytes_per_line * graphic->info.height;

    os_device_register(&graphic->parent, name);

    return OS_EOK;
}
