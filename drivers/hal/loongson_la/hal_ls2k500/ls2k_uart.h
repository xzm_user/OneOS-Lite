/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        ls2k_uart.h
 *
 * @brief       This file implements uart driver for loongson ls2k500
 *
 * @revision
 * Date         Author          Notes
 * 2020-11-17   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#ifndef __LOONGSON_UART_H
#define __LOONGSON_UART_H
#include <interrupt.h>

#define LS2K_UART2_IRQ (EXTINT_IRQ(2))
#define LS2K_UART3_IRQ (EXTINT_IRQ(3))

#define LS2K_UART2_BASE (0x800000001ff40800)
#define LS2K_UART3_BASE (0x800000001ff40c00)

#define LS2K_UART_LSR_TE  (1 << 6)
#define LS2K_UART_LSR_TFE (1 << 5)

#define LS2K_UART_DAT_OFFSET  (0)
#define LS2K_UART_DL_L_OFFSET (0)
#define LS2K_UART_IER_OFFSET  (1)
#define LS2K_UART_DL_H_OFFSET (1)
#define LS2K_UART_IIR_OFFSET  (2)
#define LS2K_UART_FCR_OFFSET  (2)
#define LS2K_UART_DL_D_OFFSET (2)
#define LS2K_UART_LCR_OFFSET  (3)
#define LS2K_UART_MCR_OFFSET  (4)
#define LS2K_UART_LSR_OFFSET  (5)
#define LS2K_UART_MSR_OFFSET  (6)

#define IER_IRxE 0x1
#define IER_ITxE 0x2
#define IER_ILE  0x4
#define IER_IME  0x8

#define LSR_RCV_FIFO 0x80
#define LSR_TSRE     0x40
#define LSR_TXRDY    0x20
#define LSR_BI       0x10
#define LSR_FE       0x08
#define LSR_PE       0x04
#define LSR_OE       0x02
#define LSR_RXRDY    0x01
#define LSR_RCV_MASK 0x1f

#define IIR_IMASK     0xf
#define IIR_RXTOUT    0xc
#define IIR_RLS       0x6
#define IIR_RXRDY     0x4
#define IIR_TXRDY     0x2
#define IIR_NOPEND    0x1
#define IIR_MLSC      0x0
#define IIR_FIFO_MASK 0xc0

#define FCR_TL_OFFSET  (0x6)
#define FCR_TL_BYTE_1  (0)
#define FCR_TL_BYTE_4  (1)
#define FCR_TL_BYTE_8  (2)
#define FCR_TL_BYTE_14 (3)

#define LCR_BEC_OFFSET (0)
#define LCR_BEC_MASK   (0x3)
#define LCR_BEC_BITS_5 (0)
#define LCR_BEC_BITS_6 (1)
#define LCR_BEC_BITS_7 (2)
#define LCR_BEC_BITS_8 (3)

#define LCR_SB_OFFSET (2)
#define LCR_SB_BITS_1 (0)
#define LCR_SB_BITS_2 (1)

#define LCR_PE_OFFSET  (3)
#define LCR_EPS_OFFSET (4)

#define LCR_DLAB_OFFSET (7)

#define LS_DATA_BITS_5 5
#define LS_DATA_BITS_6 6
#define LS_DATA_BITS_7 7
#define LS_DATA_BITS_8 8

#define LS_STOP_BITS_1 0
#define LS_STOP_BITS_2 1
#define LS_STOP_BITS_3 2
#define LS_STOP_BITS_4 3

#define LS_PARITY_NONE 0
#define LS_PARITY_ODD  1
#define LS_PARITY_EVEN 2

#define LS_BAUD_RATE_2400    2400
#define LS_BAUD_RATE_4800    4800
#define LS_BAUD_RATE_9600    9600
#define LS_BAUD_RATE_19200   19200
#define LS_BAUD_RATE_38400   38400
#define LS_BAUD_RATE_57600   57600
#define LS_BAUD_RATE_115200  115200
#define LS_BAUD_RATE_230400  230400
#define LS_BAUD_RATE_460800  460800
#define LS_BAUD_RATE_921600  921600
#define LS_BAUD_RATE_2000000 2000000
#define LS_BAUD_RATE_3000000 3000000

typedef struct
{
    void         *UARTx;
    unsigned int  baudrate;
    unsigned int  databits;
    unsigned int  stopbits;
    unsigned int  parity;
    unsigned char rx_enable;
} ls2k_uart_info_t;

unsigned int uart_init(ls2k_uart_info_t *uart_info_p);
void         uart_putc(void *uartx, unsigned char ch);
void         uart_print(void *uartx, const char *str);
unsigned int ls2k_uart_getc(void *uart_base);
unsigned int uart_tx_interrupt_enable(ls2k_uart_info_t *uart_info_p);
unsigned int uart_tx_interrupt_disable(ls2k_uart_info_t *uart_info_p);
void         uart_data_transmit(void *uartx, unsigned char ch);

#endif
