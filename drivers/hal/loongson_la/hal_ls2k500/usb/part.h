#ifndef __PARTE_H
#define __PARTE_H

#include <block/block_device.h>
#include "usb.h"

/* Interface types */
#define IF_TYPE_UNKNOWN		0
#define IF_TYPE_IDE		1
#define IF_TYPE_SCSI		2
#define IF_TYPE_ATAPI		3
#define IF_TYPE_USB		4
#define IF_TYPE_DOC		5
#define IF_TYPE_MMC		6

/* Part types */
#define PART_TYPE_UNKNOWN	0x00
#define PART_TYPE_MAC		0x01
#define PART_TYPE_DOS		0x02
#define PART_TYPE_ISO		0x03
#define PART_TYPE_AMIGA		0x04

/* device types */
#define DEV_TYPE_UNKNOWN    0xff    /* not connected */
#define DEV_TYPE_HARDDISK   0x00    /* harddisk */
#define DEV_TYPE_TAPE       0x01    /* Tape */
#define DEV_TYPE_CDROM      0x05    /* CD-ROM */
#define DEV_TYPE_OPDISK     0x07    /* optical disk */

#define SECTOR_SIZE 0x200
#define MAX_PARTS 8

typedef struct DiskPartitionTable {
    struct DiskPartitionTable* Next;
    struct DiskPartitionTable* logical;
    unsigned char id;
    unsigned char bootflag;
    unsigned char tag;
    unsigned int sec_begin;
    unsigned int size;
    unsigned int sec_end;
    void* fs;
}DiskPartitionTable;

/*GPT header*/
typedef union {
    struct  {
        unsigned char magic[8];
        unsigned int version;
        unsigned int headersize;
        unsigned int crc32;
        unsigned int unused1;
        unsigned long primary;
        unsigned long backup;
        unsigned long start;
        unsigned long end;
        unsigned char guid[16];
        unsigned long partitions;
        unsigned int maxpart;
        unsigned int partentry_size;
        unsigned int partentry_crc32;
    };
    unsigned char gpt[512];
} gpt_header;

/*GPT patition table*/
typedef union {
    struct {
        char type[16];
        char guid[16];
        unsigned long start;
        unsigned long end;
        unsigned long attrib;
        char name[72];
    };
    unsigned char ent[128];
} gpt_partentry;

typedef struct block_dev_desc {
    int		if_type;	/* type of the interface */
    int	        dev;	  	/* device number */
    unsigned char	part_type;  	/* partition type */
    unsigned char	target;		/* target SCSI ID */
    unsigned char	lun;		/* target LUN */
    unsigned char	type;		/* device type */
    unsigned char	removable;	/* removable device */
#ifdef CONFIG_LBA48
    unsigned char	lba48;		/* device can use 48bit addr (ATA/ATAPI v7) */
#endif
    unsigned int	lba;	  	/* number of blocks */
    unsigned long	blksz;		/* block size */
    unsigned char	vendor [40+1]; 	/* IDE model, SCSI Vendor */
    unsigned char	product[20+1];	/* IDE Serial no, SCSI product */
    unsigned char	revision[8+1];	/* firmware revision */
    unsigned long	(*block_read)(int dev,
            unsigned long start,
            unsigned long blkcnt,
            unsigned long *buffer,
            unsigned char *cp);
    DiskPartitionTable* part[MAX_PARTS];
}block_dev_desc_t;

struct usb_blk_device
{
    os_blk_device_t blk_dev;
    int device;
    block_dev_desc_t *blk_desc;
    DiskPartitionTable *part;
};

int dev_part_read(int device, DiskPartitionTable** ppTable);

#endif //__PARTE_H

