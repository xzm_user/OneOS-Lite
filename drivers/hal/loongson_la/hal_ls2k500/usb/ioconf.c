#include "la_device.h"
#include "board.h"

#define NORM FSTATE_NOTFOUND
#define STAR FSTATE_STAR
#define DNRM FSTATE_DNOTFOUND
#define DSTR FSTATE_DSTAR

extern struct cfdriver lohci_cd;
extern struct cfattach lohci_ca;

#ifdef BSP_USING_USB_STORAGE
extern struct cfdriver usb_cd;
extern struct cfattach usb_ca;
#endif

/* locators */
static int loc[8] = {
    -1, -1, 0x1f060000, 0x1f020000, 0x1f030000, 0x1f040000, 0x1f050000, 0x1f058000,
};

short pv[19] = {
    0, -1, 3, -1, 1, 14, -1, 9, -1, 13, -1, 2, -1, 0, -1, 5,
    -1, 8, -1,
};

struct cfdata cfdata[] = {
    /* attachment       driver        unit  state loc     flags parents nm ivstubs starunit1 */
    {&lohci_ca,		&lohci_cd,	 0, NORM, loc+  7,    0, pv+ 3, 1, 0,    0},    
#ifdef BSP_USING_USB_STORAGE
    {&usb_ca,		&usb_cd,	 0, STAR,     loc,    0, pv+ 0, 0, 0,    0},
#endif
    {0,		0,	 0, 0,     0,    0,  0, 0, 0,    0},
};

short cfroots[] = {
    0 /* mainbus0 */,
    -1
};

int cfroots_size = 2;

