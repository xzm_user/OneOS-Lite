/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        ls2k_uart.c
 *
 * @brief       This file implements uart driver for loongson ls2k500
 *
 * @revision
 * Date         Author          Notes
 * 2020-11-17   OneOS Team      First Version
 ***********************************************************************************************************************
 */
#include <stdio.h>
#include <stdarg.h>
#include "ls2k_uart.h"
#include <ls2k500_regs.h>
#include <ls2k_clk.h>
#include <os_util.h>

#define DECIMAL_DIGIT_FOR_FLOAT (100000UL)

void calc_dl_value(int baudrate, unsigned char *dl_h, unsigned char *dl_l, unsigned char *dl_d)
{
    unsigned long dl  = 0;
    unsigned long clk = (unsigned long)clk_get_apb_freq();

    clk *= DECIMAL_DIGIT_FOR_FLOAT;
    dl    = clk / (baudrate * 16);
    *dl_h = (unsigned char)((dl / DECIMAL_DIGIT_FOR_FLOAT >> 8) & 0xff);
    *dl_l = (unsigned char)((dl / DECIMAL_DIGIT_FOR_FLOAT) & 0xff);
    *dl_d = (unsigned char)(((dl % DECIMAL_DIGIT_FOR_FLOAT) * 0x100 / DECIMAL_DIGIT_FOR_FLOAT) & 0xff);

    return;
}

void dl_access_enable(void *base)
{
    unsigned char val = 0;

    val = readb(base + LS2K_UART_LCR_OFFSET);
    val |= (1 << LCR_DLAB_OFFSET);
    writeb(val, base + LS2K_UART_LCR_OFFSET);
    return;
}

void dl_access_disable(void *base)
{

    unsigned char val = 0;

    val = readb(base + LS2K_UART_LCR_OFFSET);
    val &= ~(1 << LCR_DLAB_OFFSET);
    writeb(val, base + LS2K_UART_LCR_OFFSET);
    return;
}
void uart_frequency_division_conf(void *base, unsigned char dl_h, unsigned char dl_l, unsigned char dl_d)
{
    writeb(dl_h, base + LS2K_UART_DL_H_OFFSET);
    writeb(dl_l, base + LS2K_UART_DL_L_OFFSET);
    writeb(dl_d, base + LS2K_UART_DL_D_OFFSET);
    return;
}

unsigned int uart_hw_conf(void *base, unsigned int databits, unsigned int stopbits, unsigned int parity)
{
    unsigned char val = 0;

    if (databits < LS_DATA_BITS_5 || databits > LS_DATA_BITS_8)
    {
        return 1;
    }

    if (databits == LS_DATA_BITS_5 && stopbits != LS_STOP_BITS_1)
    {
        return 1;
    }

    if (stopbits != LS_STOP_BITS_1 && stopbits != LS_STOP_BITS_2)
    {
        return 1;
    }

    if (parity != LS_PARITY_NONE)
    {
        val |= (1 << LCR_PE_OFFSET);
        val |= ((parity - LS_PARITY_ODD) << LCR_EPS_OFFSET);
    }
    val |= ((stopbits << LCR_SB_OFFSET) & (1 << LCR_SB_OFFSET));
    val |= ((databits - LS_DATA_BITS_5) & LCR_BEC_MASK);

    writeb(val, base + LS2K_UART_LCR_OFFSET);

    return 0;
}

unsigned int uart_init(ls2k_uart_info_t *uart_info_p)
{
    void         *uart_base = uart_info_p->UARTx;
    unsigned char dl_h = 0, dl_l = 0, dl_d = 0;

    if (uart_info_p->baudrate > LS_BAUD_RATE_460800)
    {
        return 1;
    }

    calc_dl_value(uart_info_p->baudrate, &dl_h, &dl_l, &dl_d);
    dl_access_enable(uart_base);
    uart_frequency_division_conf(uart_base, dl_h, dl_l, dl_d);
    dl_access_disable(uart_base);

    if (uart_hw_conf(uart_base, uart_info_p->databits, uart_info_p->stopbits, uart_info_p->parity))
    {
        return 1;
    }

    writeb((FCR_TL_BYTE_8 << FCR_TL_OFFSET), uart_base + LS2K_UART_IIR_OFFSET);

    if (uart_info_p->rx_enable)
    {
        writeb(IER_IRxE, uart_base + LS2K_UART_IER_OFFSET);
    }
    return 0;
}

unsigned int uart_tx_interrupt_enable(ls2k_uart_info_t *uart_info_p)
{
    unsigned char reg_val   = 0;
    void         *uart_base = uart_info_p->UARTx;

    reg_val = readb(uart_base + LS2K_UART_IER_OFFSET);
    reg_val |= IER_ITxE;
    writeb(reg_val, uart_base + LS2K_UART_IER_OFFSET);
    return 0;
}

unsigned int uart_tx_interrupt_disable(ls2k_uart_info_t *uart_info_p)
{
    unsigned char reg_val   = 0;
    void         *uart_base = uart_info_p->UARTx;

    reg_val = readb(uart_base + LS2K_UART_IER_OFFSET);
    reg_val &= ~(IER_ITxE);
    writeb(reg_val, uart_base + LS2K_UART_IER_OFFSET);
    return 0;
}

unsigned char uart_is_transmit_empty(void *uartx)
{
    void         *uart_base = uartx;
    unsigned char status    = readb(uart_base + LS2K_UART_LSR_OFFSET);

    if (status & (LS2K_UART_LSR_TE | LS2K_UART_LSR_TFE))
    {
        return 1;
    }
    else
    {
        return 0;
    }
}

unsigned int ls2k_uart_getc(void *uart_base)
{
    unsigned int value = 0;

    if (LSR_RXRDY & readb(uart_base + LS2K_UART_LSR_OFFSET))
    {
        value = (unsigned int)readb(uart_base + LS2K_UART_DAT_OFFSET);
        value &= 0xFF;
        return value;
    }
    return (unsigned int)-1;
}

void uart_putc(void *uartx, unsigned char ch)
{
    void *uart_base = uartx;

    while (0 == uart_is_transmit_empty(uartx))
        ;

    writeb(ch, uart_base + LS2K_UART_DAT_OFFSET);
    return;
}

void uart_data_transmit(void *uartx, unsigned char ch)
{
    void *uart_base = uartx;

    writeb(ch, uart_base + LS2K_UART_DAT_OFFSET);

    return;
}

void uart_print(void *uartx, const char *str)
{
    while ('\0' != *str)
    {
        uart_putc(uartx, *str);
        str++;
    }
    return;
}
