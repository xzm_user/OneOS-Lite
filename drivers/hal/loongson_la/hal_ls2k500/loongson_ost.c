/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        loongson_ost.c
 *
 * @brief       This file provides ostick function.
 *
 * @revision
 * Date         Author          Notes
 * 2020-11-17   OneOS Team      First version.
 ***********************************************************************************************************************
 */

#include <loongson_ost.h>

unsigned int calc_const_freq(void)
{
    unsigned int res;
    unsigned int base_freq;
    unsigned int cfm, cfd;

    res = read_cfg(LOONGARCH_CPUCFG2);
    if (!(res & CPUCFG2_LLFTP))
        return 0;

    base_freq = read_cfg(LOONGARCH_CPUCFG4);
    res       = read_cfg(LOONGARCH_CPUCFG5);
    cfm       = res & 0xffff;
    cfd       = (res >> 16) & 0xffff;

    if (!base_freq || !cfm || !cfd)
        return 0;
    else
        return (base_freq * cfm / cfd);
}

int loongson_ost_config(unsigned int ticks_per_tick)
{
    unsigned long timer_config;

    timer_config = ticks_per_tick & CSR_TCFG_VAL;
    timer_config |= (CSR_TCFG_PERIOD | CSR_TCFG_EN);
    tcfg_csrwr(timer_config);

    return 0;
}

void loongson_ost_clearirq(void)
{
    ticlr_csrwr(CSR_TINTCLR_TI);
}
