/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        interrupt.c
 *
 * @brief       This file provides ostick function.
 *
 * @revision
 * Date         Author          Notes
 * 2020-11-17   OneOS Team      First version.
 ***********************************************************************************************************************
 */
#include <stddef.h>
#include <interrupt.h>
#include <string.h>
#include <os_util.h>
#include <arch_misc.h>

extern void               hw_normal_exception_entry(void);
static struct os_irq_desc isr_table[INTERRUPTS_MAX];

static void os_hw_interrupt_handler(int vector, void *param)
{
    os_kprintf("Unhandled interrupt %d occured!!!\n", vector);
}

os_int32_t os_fls64(os_uint64_t value)
{
    os_int32_t pos;

    pos = 64;

    if (!value)
    {
        pos = 0;
    }
    else
    {

        if (!(value & 0xFFFFFFFF00000000UL))
        {
            value <<= 32;
            pos -= 32;
        }

        if (!(value & 0xFFFF000000000000UL))
        {
            value <<= 16;
            pos -= 16;
        }

        if (!(value & 0xFF00000000000000UL))
        {
            value <<= 8;
            pos -= 8;
        }

        if (!(value & 0xF000000000000000UL))
        {
            value <<= 4;
            pos -= 4;
        }

        if (!(value & 0xC000000000000000UL))
        {
            value <<= 2;
            pos -= 2;
        }

        if (!(value & 0x8000000000000000UL))
        {
            value <<= 1;
            pos -= 1;
        }
    }

    return pos;
}

void os_interrupt_dispatch(void)
{

    void            *param;
    os_isr_handler_t irq_func;
    os_ubase_t       value           = 0;
    unsigned long    irq_state       = 0;
    unsigned int     irq_state_local = 0;
    int              irq             = 0;

    value = estate_csrrd();

    if (value & CAUSEF_TI)
    {
        extern void os_hw_ost_handler(void);
        os_hw_ost_handler();
        return;
    }

    if (value & CAUSEF_IP2)
    {

        irq_state = readq(CORE_EXTISR0);
        writeq(irq_state, CORE_EXTISR0);

        if (irq_state)
        {
            irq = os_fls64(irq_state) - 1;
            irq_state &= ~(1 << irq);
        }
    }

    if (value & CAUSEF_IP3)
    {

        irq_state = readq(CORE_EXTISR1);
        writeq(irq_state, CORE_EXTISR1);

        if (irq_state)
        {
            irq = os_fls64(irq_state) - 1;
            irq_state &= ~(1 << irq);
            irq += EXTINT_LOW_NUM;
        }
    }

    if (value & CAUSEF_IP4)
    {

        irq_state_local = readl(CORE_LIOISR0);
        writel(irq_state_local, LIOINT_ICLR0);
        writel(irq_state_local, LIOINT_IEN0);

        if (irq_state_local)
        {
            irq = os_fls(irq_state_local) - 1;
            irq_state_local &= ~(1 << irq);
            irq += EXTINT_TOTAL_NUM;
        }
    }

    if (value & CAUSEF_IP5)
    {

        irq_state_local = readl(CORE_LIOISR1);
        writel(irq_state_local, LIOINT_ICLR1);
        writel(irq_state_local, LIOINT_IEN1);

        if (irq_state_local)
        {
            irq = os_fls(irq_state_local) - 1;
            irq_state_local &= ~(1 << irq);
            irq += (EXTINT_TOTAL_NUM + LIOINT_LOW_NUM);
        }
    }

    if (isr_table[irq].handler == os_hw_interrupt_handler)
    {
        if ((irq >= EXTINT_TOTAL_NUM) && (irq < (EXTINT_TOTAL_NUM + LIOINT_LOW_NUM)))
        {
            if (isr_table[irq - EXTINT_TOTAL_NUM].handler != os_hw_interrupt_handler)
            {
                irq -= EXTINT_TOTAL_NUM;
            }
        }

        if ((irq >= (EXTINT_TOTAL_NUM + LIOINT_LOW_NUM)) && (irq < (EXTINT_TOTAL_NUM + LIOINT_TOTAL_NUM)))
        {

            if (isr_table[irq - (EXTINT_TOTAL_NUM + LIOINT_LOW_NUM)].handler != os_hw_interrupt_handler)
            {
                irq -= (EXTINT_TOTAL_NUM + LIOINT_LOW_NUM);
            }
        }
    }

    if (irq >= INTERRUPTS_MAX)
        os_kprintf("max interrupt, irq=%d\n", irq);

    /* do interrupt */
    irq_func = isr_table[irq].handler;
    param    = isr_table[irq].param;
    (*irq_func)(irq, param);

    return;
}

void os_hw_exception_init(void)
{
    os_ubase_t val = 0;

    val = ecfg_csrrd();
    val |= (CSR_ECFG_LIE_MASK << CSR_ECFG_LIE_OFFSET);
    val &= ~(CSR_ECFG_VS_MASK << CSR_ECFG_VS_OFFSET);
    ecfg_csrwr(val);
    eentry_csrwr((os_ubase_t)hw_normal_exception_entry);

    return;
}

void hw_extioint_enable(void)
{
    writel((readl(GENERAL_CONFIGURE_REG0) | (1 << GENERAL_CONFIGURE_EXTINT_ENABLE)), GENERAL_CONFIGURE_REG0);
}

void extioint_mask_irq(int vector)
{
    if (vector < EXTINT_LOW_NUM && vector >= 0)
    {
        writeq((readq(EXTINT_ICLR0) | (0x1UL << vector)), EXTINT_ICLR0);
    }
    else if (vector < EXTINT_TOTAL_NUM && vector >= EXTINT_LOW_NUM)
    {
        writeq((readq(EXTINT_ICLR1) | (0x1UL << (vector - EXTINT_LOW_NUM))), EXTINT_ICLR1);
    }

    else if (vector < (EXTINT_TOTAL_NUM + LIOINT_LOW_NUM) && vector >= EXTINT_TOTAL_NUM)
    {
        writel((readl(LIOINT_ICLR0) | (0x1 << (vector - EXTINT_TOTAL_NUM))), LIOINT_ICLR0);
    }
    else if (vector < (EXTINT_TOTAL_NUM + LIOINT_TOTAL_NUM) && vector >= (EXTINT_TOTAL_NUM + LIOINT_LOW_NUM))
    {
        writel((readl(LIOINT_ICLR1) | (0x1 << (vector - (EXTINT_TOTAL_NUM + LIOINT_LOW_NUM)))), LIOINT_ICLR1);
    }
}

void extioint_umask_irq(int vector)
{
    if (vector < EXTINT_LOW_NUM && vector >= 0)
    {
        writeq((readq(EXTINT_IEN0) | (0x1UL << vector)), EXTINT_IEN0);
    }
    else if (vector < EXTINT_TOTAL_NUM && vector >= EXTINT_LOW_NUM)
    {
        writeq((readq(EXTINT_IEN1) | (0x1UL << (vector - EXTINT_LOW_NUM))), EXTINT_IEN1);
    }
    else if (vector < (EXTINT_TOTAL_NUM + LIOINT_LOW_NUM) && vector >= EXTINT_TOTAL_NUM)
    {
        writel((readl(LIOINT_IEN0) | (0x1 << (vector - EXTINT_TOTAL_NUM))), LIOINT_IEN0);
    }
    else if (vector < (EXTINT_TOTAL_NUM + LIOINT_TOTAL_NUM) && vector >= (EXTINT_TOTAL_NUM + LIOINT_LOW_NUM))
    {
        writel((readl(LIOINT_IEN1) | (0x1 << (vector - (EXTINT_TOTAL_NUM + LIOINT_LOW_NUM)))), LIOINT_IEN1);
    }
}

void hw_extioint_route(void)
{
    unsigned int irq = 0;

    writeb(0x1, EXTINT_MAP0);
    writeb(0x1, EXTINT_MAP1);
    writeb(0x2, EXTINT_MAP2);
    writeb(0x2, EXTINT_MAP3);

    for (irq = 0; irq < LIOINT_LOW_NUM; irq++)
    {
        writeb(0x40, LIOINT_BASE + irq);
    }

    for (irq = LIOINT_LOW_NUM; irq < LIOINT_TOTAL_NUM; irq++)
    {
        writeb(0x80, LIOINT_BASE + irq);
    }

    writel(0xFFFFFFFF, LIOINT_ICLR0);
    writel(0xFFFFFFFF, LIOINT_ICLR1);
}

void os_hw_interrupt_mask(int vector)
{
    extioint_mask_irq(vector);
    return;
}

void os_hw_interrupt_umask(int vector)
{
    extioint_umask_irq(vector);
    return;
}

os_isr_handler_t os_hw_interrupt_install(int vector, os_isr_handler_t handler, void *param, const char *name)
{
    os_isr_handler_t old_handler = NULL;

    if (vector < INTERRUPTS_MAX)
    {
        old_handler               = isr_table[vector].handler;
        isr_table[vector].handler = handler;
        isr_table[vector].param   = param;
    }

    return old_handler;
}

void os_hw_interrupt_init(void)
{
    unsigned int idx = 0;

    memset(isr_table, 0x00, sizeof(isr_table));

    for (idx = 0; idx < INTERRUPTS_MAX; idx++)
    {
        isr_table[idx].handler = os_hw_interrupt_handler;
    }

    hw_extioint_enable();
    hw_extioint_route();

    return;
}
