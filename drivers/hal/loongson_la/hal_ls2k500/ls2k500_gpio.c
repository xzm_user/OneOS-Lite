#include <ls2k500_gpio.h>
#include <ls2k500_regs.h>

unsigned long gpio_oen_reg[] =
    {GPIO_OEN_BASE_0, GPIO_OEN_BASE_1, GPIO_OEN_BASE_2, GPIO_OEN_BASE_3, GPIO_OEN_BASE_4, GPIO_OEN_BASE_5};
unsigned long gpio_out_reg[] =
    {GPIO_OUT_BASE_0, GPIO_OUT_BASE_1, GPIO_OUT_BASE_2, GPIO_OUT_BASE_3, GPIO_OUT_BASE_4, GPIO_OUT_BASE_5};
unsigned long gpio_in_reg[] =
    {GPIO_IN_BASE_0, GPIO_IN_BASE_1, GPIO_IN_BASE_2, GPIO_IN_BASE_3, GPIO_IN_BASE_4, GPIO_IN_BASE_5};

void gpio_set_func(enum gpio_function func, unsigned int pin)
{
    unsigned long reg    = 0;
    unsigned int  value  = 0;
    unsigned int  offset = 0;

    if (pin >= LS2k500_GPIO_NUM)
    {
        return;
    }

    reg    = ((pin / GPIO_MUX_CONFIG_PINS_PER_REG) * sizeof(unsigned int)) + GPIO_MUX_CONFIG_BASE;
    offset = (pin % GPIO_MUX_CONFIG_PINS_PER_REG) * GPIO_MUX_CONFIG_BITS;
    value  = readl(reg);
    value &= ~(GPIO_MUX_CONFIG_MASK << offset);
    value |= (func << offset);
    writel(value, reg);

    return;
}

void gpio_set_direction(enum gpio_direction dir, unsigned int pin)
{
    unsigned int reg_index = 0;
    unsigned int value     = 0;
    unsigned int offset    = 0;

    if (pin >= LS2k500_GPIO_NUM)
    {
        return;
    }

    reg_index = (pin / GPIO_OEN_PINS_PER_REG);
    offset    = (pin % GPIO_OEN_PINS_PER_REG) * GPIO_OEN_BITS;
    value     = readl(gpio_oen_reg[reg_index]);
    value &= ~(GPIO_OEN_MASK << offset);
    value |= (dir << offset);
    writel(value, gpio_oen_reg[reg_index]);

    return;
}

void gpio_port_set_value(int value, unsigned int pin)
{
    unsigned int reg_index = 0;
    unsigned int reg_value = 0;
    unsigned int offset    = 0;

    if (pin >= LS2k500_GPIO_NUM)
    {
        return;
    }

    reg_index = (pin / GPIO_OUT_PINS_PER_REG);
    offset    = (pin % GPIO_OUT_PINS_PER_REG) * GPIO_OUT_BITS;
    reg_value = readl(gpio_out_reg[reg_index]);
    reg_value &= ~(GPIO_OUT_MASK << offset);
    reg_value |= (value << offset);
    writel(reg_value, gpio_out_reg[reg_index]);

    return;
}

int gpio_port_get_value(unsigned int pin)
{
    unsigned int reg_index = 0;
    unsigned int value     = 0;
    unsigned int offset    = 0;

    if (pin >= LS2k500_GPIO_NUM)
    {
        return 0;
    }

    reg_index = (pin / GPIO_IN_PINS_PER_REG);
    offset    = (pin % GPIO_IN_PINS_PER_REG) * GPIO_IN_BITS;
    value     = readl(gpio_in_reg[reg_index]);

    return (int)((value >> offset) & GPIO_IN_MASK);
}

void gpio_set_interrupt_enable(unsigned int pin)
{
    unsigned long reg    = 0;
    unsigned int  value  = 0;
    unsigned int  offset = 0;

    if (pin >= LS2k500_GPIO_IRQ_NUM)
    {
        return;
    }

    reg    = ((pin / GPIO_INTEN_PINS_PER_REG) * sizeof(unsigned int)) + GPIO_INTEN_BASE;
    offset = (pin % GPIO_INTEN_PINS_PER_REG) * GPIO_INTEN_BITS;
    value  = readl(reg);
    value |= (1 << offset);
    writel(value, reg);

    return;
}

void gpio_set_interrupt_disable(unsigned int pin)
{

    unsigned long reg    = 0;
    unsigned int  value  = 0;
    unsigned int  offset = 0;

    if (pin >= LS2k500_GPIO_IRQ_NUM)
    {
        return;
    }

    reg    = ((pin / GPIO_INTEN_PINS_PER_REG) * sizeof(unsigned int)) + GPIO_INTEN_BASE;
    offset = (pin % GPIO_INTEN_PINS_PER_REG) * GPIO_INTEN_BITS;
    value  = readl(reg);
    value &= ~(1 << offset);
    writel(value, reg);

    return;
}
