/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_eth.c
 *
 * @brief       This file implements eth driver.
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */
#include "board.h"
#include <os_memory.h>
#include <string.h>
#include <bus/bus.h>

#include "fsl_enet.h"
#include "fsl_gpio.h"
#include "fsl_phy.h"
#include "fsl_cache.h"
#include "fsl_iomuxc.h"
#include "fsl_common.h"
#include "fsl_phy.h"
#include "fsl_enet_mdio.h"
#include "fsl_ocotp.h"

#include "net_dev.h"
#include <drv_gpio.h>
#include <drv_eth.h>

#define DRV_EXT_LVL DBG_EXT_INFO
#define DRV_EXT_TAG "drv.eth"
#include <dlog.h>

#ifndef PHY_INIT_COUNT
#define PHY_INIT_COUNT (10)
#endif

#ifndef PHY_AUTONEGO_TIMEOUT_COUNT
#define PHY_AUTONEGO_TIMEOUT_COUNT (100000)
#endif

#ifdef BSP_USING_ETH1
AT_NONCACHEABLE_SECTION_ALIGN(enet_rx_bd_struct_t g_rxBuffDescrip1[ETH1_RXBUFF_NUM], ENET_BUFF_ALIGNMENT);
AT_NONCACHEABLE_SECTION_ALIGN(enet_tx_bd_struct_t g_txBuffDescrip1[ETH1_TXBUFF_NUM], ENET_BUFF_ALIGNMENT);

AT_NONCACHEABLE_SECTION_ALIGN(
    uint8_t g_rxDataBuff1[ETH1_RXBUFF_NUM][SDK_SIZEALIGN(ENET_FRAME_MAX_FRAMELEN, ENET_BUFF_ALIGNMENT)],
    ENET_BUFF_ALIGNMENT);
AT_NONCACHEABLE_SECTION_ALIGN(
    uint8_t g_txDataBuff1[ETH1_TXBUFF_NUM][SDK_SIZEALIGN(ENET_FRAME_MAX_FRAMELEN, ENET_BUFF_ALIGNMENT)],
    ENET_BUFF_ALIGNMENT);

enet_buffer_config_t buffConfig1 = {
    ETH1_RXBUFF_NUM,
    ETH1_TXBUFF_NUM,
    SDK_SIZEALIGN(ENET_FRAME_MAX_FRAMELEN, ENET_BUFF_ALIGNMENT),
    SDK_SIZEALIGN(ENET_FRAME_MAX_FRAMELEN, ENET_BUFF_ALIGNMENT),
    &g_rxBuffDescrip1[0],
    &g_txBuffDescrip1[0],
    &g_rxDataBuff1[0][0],
    &g_txDataBuff1[0][0],
    true,
    true,
    NULL,
};

static const struct nxp_eth_info eth1_info = {
    .base          = ENET,
    .phyAddr       = ETH1_PHY_ADDR,
    .reset_pin     = ETH1_RESET_PIN,
    .int_pin       = ETH1_INT_PIN,
    .buffConfig    = &buffConfig1,
    .sysClock_name = kCLOCK_IpgClk,

#ifdef ETH_USING_KSZ8081
    .ops  = &phyksz8081_ops,
    .type = KSZ8081,
#endif

#ifdef ETH_USING_LAN8720
    .ops  = &phylan8720_ops,
    .type = LAN8720,
#endif
};
OS_HAL_DEVICE_DEFINE("ETH_Type", "eth1", eth1_info);
#endif

#ifdef BSP_USING_ETH2
AT_NONCACHEABLE_SECTION_ALIGN(enet_rx_bd_struct_t g_rxBuffDescrip2[ETH2_RXBUFF_NUM], ENET_BUFF_ALIGNMENT);
AT_NONCACHEABLE_SECTION_ALIGN(enet_tx_bd_struct_t g_txBuffDescrip2[ETH2_TXBUFF_NUM], ENET_BUFF_ALIGNMENT);

AT_NONCACHEABLE_SECTION_ALIGN(
    uint8_t g_rxDataBuff2[ETH2_RXBUFF_NUM][SDK_SIZEALIGN(ENET_FRAME_MAX_FRAMELEN, ENET_BUFF_ALIGNMENT)],
    ENET_BUFF_ALIGNMENT);
AT_NONCACHEABLE_SECTION_ALIGN(
    uint8_t g_txDataBuff2[ETH2_TXBUFF_NUM][SDK_SIZEALIGN(ENET_FRAME_MAX_FRAMELEN, ENET_BUFF_ALIGNMENT)],
    ENET_BUFF_ALIGNMENT);

enet_buffer_config_t buffConfig2 = {
    ETH2_RXBUFF_NUM,
    ETH2_TXBUFF_NUM,
    SDK_SIZEALIGN(ENET_FRAME_MAX_FRAMELEN, ENET_BUFF_ALIGNMENT),
    SDK_SIZEALIGN(ENET_FRAME_MAX_FRAMELEN, ENET_BUFF_ALIGNMENT),
    &g_rxBuffDescrip2[0],
    &g_txBuffDescrip2[0],
    &g_rxDataBuff2[0][0],
    &g_txDataBuff2[0][0],
    true,
    true,
    NULL,
};

static const struct nxp_eth_info eth2_info = {
    .base          = ENET2,
    .phyAddr       = ETH2_PHY_ADDR,
    .reset_pin     = ETH2_RESET_PIN,
    .int_pin       = ETH2_INT_PIN,
    .buffConfig    = &buffConfig2,
    .sysClock_name = kCLOCK_IpgClk,

#ifdef ETH_USING_KSZ8081
    .ops  = &phyksz8081_ops,
    .type = KSZ8081,
#endif

#ifdef ETH_USING_LAN8720
    .ops  = &phylan8720_ops,
    .type = LAN8720,
#endif
};
OS_HAL_DEVICE_DEFINE("ETH_Type", "eth2", eth2_info);
#endif

struct os_imxrt_eth
{
    struct os_net_device net_dev;

    struct nxp_eth_info *eth_info;
    mdio_handle_t        mdioHandle;
    phy_handle_t         phyHandle;
    enet_handle_t        enet_handle;

    enet_mii_speed_t  speed;
    enet_mii_duplex_t duplex;
    os_uint32_t       sysClock;

    os_timer_t *poll_link_timer;

    os_uint8_t dev_addr[OS_NET_MAC_LENGTH];
    os_sem_t  *tx_sem;
    os_task_t *monitor_task;
};

void _enet_rx_callback(struct os_imxrt_eth *imxrt_eth)
{
    ENET_DisableInterrupts(imxrt_eth->eth_info->base, kENET_RxFrameInterrupt);

    os_net_rx_report_irq(&imxrt_eth->net_dev);
}

void _enet_tx_callback(struct os_imxrt_eth *imxrt_eth)
{
    os_sem_post(imxrt_eth->tx_sem);
}

void _enet_callback(ENET_Type         *base,
                    enet_handle_t     *handle,
                    enet_event_t       event,
                    enet_frame_info_t *frameInfo,
                    void              *userData)
{
    switch (event)
    {
    case kENET_RxEvent:
        _enet_rx_callback((struct os_imxrt_eth *)userData);
        break;

    case kENET_TxEvent:
        _enet_tx_callback((struct os_imxrt_eth *)userData);
        break;

    case kENET_ErrEvent:
        LOG_D(DRV_EXT_TAG, "kENET_ErrEvent");
        break;

    case kENET_WakeUpEvent:
        LOG_D(DRV_EXT_TAG, "kENET_WakeUpEvent");
        break;

    case kENET_TimeStampEvent:
        LOG_D(DRV_EXT_TAG, "kENET_TimeStampEvent");
        break;

    case kENET_TimeStampAvailEvent:
        LOG_D(DRV_EXT_TAG, "kENET_TimeStampAvailEvent");
        break;

    default:
        LOG_D(DRV_EXT_TAG, "unknow error");
        break;
    }
}

static void _phy_reset_by_gpio(struct os_imxrt_eth *imxrt_eth)
{
    if (imxrt_eth->eth_info->reset_pin >= 0)
    {
        os_pin_mode(imxrt_eth->eth_info->reset_pin, PIN_MODE_OUTPUT);

        if (imxrt_eth->eth_info->int_pin >= 0)
        {
            os_pin_mode(imxrt_eth->eth_info->int_pin, PIN_MODE_OUTPUT);
            os_pin_write(imxrt_eth->eth_info->int_pin, 1);
        }

        os_pin_write(imxrt_eth->eth_info->reset_pin, 0);
        os_task_msleep(500);
        os_pin_write(imxrt_eth->eth_info->reset_pin, 1);
    }
}

os_err_t imxrt_eth_init(struct os_net_device *net_dev)
{
    status_t    status     = 0;
    os_uint32_t count_init = PHY_INIT_COUNT;
    os_uint32_t count      = 0;
    bool        link       = false;
    bool        autonego   = false;

    enet_config_t config;
    phy_config_t  phyConfig = {0};

    struct os_imxrt_eth *imxrt_eth = (struct os_imxrt_eth *)net_dev;

    clock_enet_pll_config_t pll_config = {.enableClkOutput = true, .loopDivider = 1};

#ifdef ENET
    if (imxrt_eth->eth_info->base == ENET)
    {
        CLOCK_InitEnetPll(&pll_config);
        IOMUXC_EnableMode(IOMUXC_GPR, kIOMUXC_GPR_ENET1TxClkOutputDir, true);
    }
#endif
#ifdef ENET2
    if (imxrt_eth->eth_info->base == ENET2)
    {
        pll_config.enableClkOutput1 = true;
        pll_config.loopDivider1     = 1;
        CLOCK_InitEnetPll(&pll_config);
        IOMUXC_EnableMode(IOMUXC_GPR, kIOMUXC_GPR_ENET2TxClkOutputDir, true);
    }
#endif

    _phy_reset_by_gpio(imxrt_eth);

    imxrt_eth->mdioHandle.ops                  = &enet_ops;
    imxrt_eth->mdioHandle.resource.base        = imxrt_eth->eth_info->base;
    imxrt_eth->mdioHandle.resource.csrClock_Hz = CLOCK_GetFreq(imxrt_eth->eth_info->sysClock_name);

    imxrt_eth->phyHandle.phyAddr    = imxrt_eth->eth_info->phyAddr;
    imxrt_eth->phyHandle.mdioHandle = &imxrt_eth->mdioHandle;
    imxrt_eth->phyHandle.ops        = imxrt_eth->eth_info->ops;

    phyConfig.phyAddr = imxrt_eth->eth_info->phyAddr;
    phyConfig.autoNeg = true;
    phyConfig.duplex  = kPHY_FullDuplex;
    phyConfig.speed   = kPHY_Speed100M;

    status = PHY_Init(&imxrt_eth->phyHandle, &phyConfig);
    if (status != kStatus_Success)
    {
        LOG_E(DRV_EXT_TAG, "PHY_Init failed!");
        return OS_ERROR;
    }

    LOG_D(DRV_EXT_TAG, "Wait for PHY link up...");
    /* Wait for auto-negotiation success and link up */
    count = PHY_AUTONEGO_TIMEOUT_COUNT;

    do
    {
        status = PHY_GetAutoNegotiationStatus(&imxrt_eth->phyHandle, &autonego);
        if ((status != kStatus_Success) || (!autonego))
        {
            continue;
        }
        else
        {
            break;
        }

    } while (--count);

    if (!count)
    {
        LOG_E(DRV_EXT_TAG, "PHY Auto-negotiation failed!");
        return OS_ERROR;
    }

    if (imxrt_eth->eth_info->type == LAN8720)
    {
        os_task_msleep(350);
    }
    else
    {
        os_task_msleep(100);
    }

    count = PHY_AUTONEGO_TIMEOUT_COUNT;

    do
    {
        status = PHY_GetLinkStatus(&imxrt_eth->phyHandle, &link);
        if ((status != kStatus_Success) || (!link))
        {
            continue;
        }
        else
        {
            break;
        }
    } while (count--);

    if (!count)
    {
        LOG_E(DRV_EXT_TAG, "PHY_GetLinkStatus failed!");
        return OS_ERROR;
    }

    if (imxrt_eth->sysClock != 0)
    {
        ENET_Deinit(imxrt_eth->eth_info->base);
    }
    else
    {
        imxrt_eth->sysClock = CLOCK_GetFreq(imxrt_eth->eth_info->sysClock_name);
    }

    ENET_GetDefaultConfig(&config);

    status = PHY_GetLinkSpeedDuplex(&imxrt_eth->phyHandle,
                                    (phy_speed_t *)&imxrt_eth->speed,
                                    (phy_duplex_t *)&imxrt_eth->duplex);

    config.interrupt = kENET_TxFrameInterrupt | kENET_RxFrameInterrupt;
    config.miiSpeed  = imxrt_eth->speed;
    config.miiDuplex = imxrt_eth->duplex;
    config.callback  = _enet_callback;
    config.userData  = imxrt_eth;

    status = ENET_Init(imxrt_eth->eth_info->base,
                       &imxrt_eth->enet_handle,
                       &config,
                       imxrt_eth->eth_info->buffConfig,
                       &imxrt_eth->dev_addr[0],
                       imxrt_eth->sysClock);
    if (status != kStatus_Success)
    {
        LOG_E(DRV_EXT_TAG, "ENET_Init failed!");
        return OS_ERROR;
    }

    ENET_ActiveRead(imxrt_eth->eth_info->base);

    return OS_EOK;
}

os_err_t imxrt_eth_deinit(struct os_net_device *net_dev)
{
    struct os_imxrt_eth *imxrt_eth = (struct os_imxrt_eth *)net_dev;

    ENET_Deinit(imxrt_eth->eth_info->base);

    return OS_EOK;
}

static os_err_t imxrt_eth_set_filter(struct os_net_device *net_dev, os_uint8_t *addr, os_bool_t enable)
{
    struct os_imxrt_eth *imxrt_eth = (struct os_imxrt_eth *)net_dev;

    if (enable)
    {
        ENET_AddMulticastGroup(imxrt_eth->eth_info->base, addr);
    }
    else
    {
        ENET_LeaveMulticastGroup(imxrt_eth->eth_info->base, addr);
    }

    return OS_EOK;
}

static os_err_t imxrt_eth_get_macaddr(struct os_net_device *net_dev, os_uint8_t *addr)
{
    struct os_imxrt_eth *imxrt_eth = (struct os_imxrt_eth *)net_dev;

    memcpy(addr, imxrt_eth->dev_addr, OS_NET_MAC_LENGTH);

    return OS_EOK;
}

static os_err_t imxrt_eth_send(struct os_net_device *net_dev, os_net_xfer_data_t *data)
{
    struct os_imxrt_eth *imxrt_eth = (struct os_imxrt_eth *)net_dev;

    if (data->size > ENET_FRAME_TX_LEN_LIMITATION(imxrt_eth->eth_info->base))
    {
        LOG_E(DRV_EXT_TAG, "ENET_SendFrame failed!");
        return OS_ERROR;
    }

    os_sem_wait(imxrt_eth->tx_sem, OS_WAIT_FOREVER);

    if (ENET_SendFrame(imxrt_eth->eth_info->base, &imxrt_eth->enet_handle, data->buff, data->size, 0, false, NULL) !=
        kStatus_Success)
    {
        LOG_E(DRV_EXT_TAG, "ENET_SendFrame failed!");
        return OS_ERROR;
    }

    return OS_EOK;
}

static os_net_xfer_data_t *imxrt_eth_recv(struct os_net_device *net_dev)
{
    os_uint8_t  i             = 0;
    os_uint8_t  rx_buffer_num = 0;
    os_uint32_t length        = 0;
    os_uint32_t ts            = 0;
    status_t    status;

    enet_data_error_stats_t error;

    os_net_xfer_data_t *data = OS_NULL;

    struct os_imxrt_eth *imxrt_eth = (struct os_imxrt_eth *)net_dev;

    status = ENET_GetRxFrameSize(&imxrt_eth->enet_handle, (uint32_t *)&length, 0);
    if ((length == 0) && (status != kStatus_Success))
    {
        LOG_D(DRV_EXT_TAG, "no frame!");
        goto __recv_exit;
    }

    data = os_net_get_buff(length);
    if (data == OS_NULL)
    {
        LOG_E(DRV_EXT_TAG, "no memory");
        goto __recv_exit;
    }

    if (ENET_ReadFrame(imxrt_eth->eth_info->base,
                       &imxrt_eth->enet_handle,
                       data->buff,
                       data->size,
                       0,
                       (uint32_t *)&ts) != kStatus_Success)
    {
        ENET_GetRxErrBeforeReadFrame(&imxrt_eth->enet_handle, &error, 0);
        ENET_ReadFrame(imxrt_eth->eth_info->base, &imxrt_eth->enet_handle, OS_NULL, 0, 0, OS_NULL);

        LOG_E(DRV_EXT_TAG, "A frame read failed!");
        os_net_free_buff(data);
        data = OS_NULL;
    }

__recv_exit:
    ENET_EnableInterrupts(imxrt_eth->eth_info->base, kENET_RxFrameInterrupt);

    return data;
}

static void phy_monitor_task_entry(void *parameter)
{
    status_t     status;
    phy_speed_t  speed;
    phy_duplex_t duplex;
    bool         link     = false;
    bool         new_link = false;

    struct os_imxrt_eth *imxrt_eth = (struct os_imxrt_eth *)parameter;

    while (1)
    {
        status = PHY_GetLinkStatus(&imxrt_eth->phyHandle, &new_link);
        if ((status == kStatus_Success) && (link != new_link))
        {
            link = new_link;

            if (link)
            {
                PHY_GetLinkSpeedDuplex(&imxrt_eth->phyHandle, &speed, &duplex);

                if ((imxrt_eth->speed != (enet_mii_speed_t)speed) || (imxrt_eth->duplex != (enet_mii_duplex_t)duplex))
                {
                    imxrt_eth->speed  = (enet_mii_speed_t)speed;
                    imxrt_eth->duplex = (enet_mii_duplex_t)duplex;

                    imxrt_eth_init(&imxrt_eth->net_dev);
                }
            }

            os_net_linkchange(&imxrt_eth->net_dev, link);
        }

        os_task_msleep(1000);
    }
}

const static struct os_net_device_ops net_dev_ops = {
    .init       = imxrt_eth_init,
    .deinit     = imxrt_eth_deinit,
    .recv       = imxrt_eth_recv,
    .send       = imxrt_eth_send,
    .get_mac    = imxrt_eth_get_macaddr,
    .set_filter = imxrt_eth_set_filter,
};

static int imxrt_eth_probe(const os_driver_info_t *drv, const os_device_info_t *dev)
{
    os_err_t    state;
    os_uint32_t unique_id[2];

    os_task_t *monitor_task = OS_NULL;

    struct os_imxrt_eth *imxrt_eth = os_calloc(1, sizeof(struct os_imxrt_eth));
    if (imxrt_eth == OS_NULL)
    {
        LOG_E(DRV_EXT_TAG, "imxrt_eth memory call failed!");
        return OS_ENOMEM;
    }

    imxrt_eth->eth_info = (struct nxp_eth_info *)dev->info;

    OCOTP_Init(OCOTP, 500000000U);
    unique_id[0] = OCOTP_ReadFuseShadowRegister(OCOTP, 1);
    unique_id[1] = OCOTP_ReadFuseShadowRegister(OCOTP, 2);
    OCOTP_Deinit(OCOTP);

    /* NXP (Freescale) MAC OUI */
    imxrt_eth->dev_addr[0] = 0x00;
    imxrt_eth->dev_addr[1] = 0x04;
    imxrt_eth->dev_addr[2] = 0x9F;
    /* generate MAC addr from unique ID (only for test). */
    imxrt_eth->dev_addr[3] |= (os_uint8_t)(unique_id[0] & 0xFF);
    imxrt_eth->dev_addr[4] |= (os_uint8_t)((unique_id[0] >> 8) & 0xFF);
    imxrt_eth->dev_addr[5] |= (os_uint8_t)((unique_id[0] >> 16) & 0xFF);
    imxrt_eth->dev_addr[5] += imxrt_eth->eth_info->phyAddr;

    imxrt_eth->tx_sem = os_sem_create("tx_sem", 1, 1);
    if (imxrt_eth->tx_sem == OS_NULL)
    {
        LOG_E(DRV_EXT_TAG, "os_sem_create failed!");
        goto __exit;
    }

    imxrt_eth->net_dev.info.MTU       = ENET_FRAME_MAX_FRAMELEN;
    imxrt_eth->net_dev.info.MRU       = ENET_FRAME_MAX_FRAMELEN;
    imxrt_eth->net_dev.info.mode      = net_dev_mode_sta;
    imxrt_eth->net_dev.info.intf_type = net_dev_intf_ether;
    imxrt_eth->net_dev.info.xfer_flag = OS_NET_XFER_TX_TASK | OS_NET_XFER_RX_TASK;
    imxrt_eth->net_dev.ops            = &net_dev_ops;

    if (os_net_device_register(&imxrt_eth->net_dev, dev->name) != OS_EOK)
    {
        LOG_E(DRV_EXT_TAG, "os_net_device_register failed!");
        goto __exit;
    }

    imxrt_eth->monitor_task = os_task_create("phy",
                                             phy_monitor_task_entry,
                                             imxrt_eth,
                                             ETH_MONITOR_TASK_STACK_SIEZ,
                                             ETH_MONITOR_TASK_PRIORITY);
    if (imxrt_eth->monitor_task == OS_NULL)
    {
        LOG_E(DRV_EXT_TAG, "monitor_task create failed!");
        os_net_device_unregister(&imxrt_eth->net_dev);
        goto __exit;
    }

    os_task_startup(imxrt_eth->monitor_task);

    return state;
__exit:
    if (imxrt_eth->tx_sem)
    {
        os_sem_destroy(imxrt_eth->tx_sem);
    }

    if (imxrt_eth->monitor_task)
    {
        os_task_destroy(imxrt_eth->monitor_task);
    }

    if (imxrt_eth)
    {
        os_free(imxrt_eth);
    }

    return OS_EOK;
}

OS_DRIVER_INFO imxrt_eth_driver = {
    .name  = "ETH_Type",
    .probe = imxrt_eth_probe,
};

OS_DRIVER_DEFINE(imxrt_eth_driver, DEVICE, OS_INIT_SUBLEVEL_LOW);
