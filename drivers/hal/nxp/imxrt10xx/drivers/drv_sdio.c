/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        sdio.c
 *
 * @brief       This file provides operation functions for sdio.
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */
#include <oneos_config.h>
#include <os_list.h>
#include <os_clock.h>
#include <os_task.h>
#include <os_util.h>
#include <os_memory.h>
#include <os_assert.h>
#include <os_errno.h>
#include <device.h>
#include <dlog.h>

#include <drv_sdio.h>
#include <fsl_sdio.h>

#define DRV_EXT_LVL DBG_EXT_INFO
#define DRV_EXT_TAG "drv.sdio"
#include <drv_log.h>

void imxrt_sdio_insert_callback(bool isInserted, void *userData)
{
}

static void imxrt_sdio_cardinterruptcb(void *userData)
{
    sdio_card_t *card = (sdio_card_t *)userData;
    SDMMCHOST_EnableCardInt(card->host, false);
    //    os_sem_post(&sdio_device->sem);
}

#if 0
static void os_imxrt_sdio_irq_task_entry(void *parameter)
{
    os_imxrt_sdio_device_t *sdio_device = parameter;
    while(1)
    {
        if (os_sem_wait(&sdio_device->sem, OS_WAIT_FOREVER) == OS_EOK)
        {
            SDIO_HandlePendingIOInterrupt(&sdio_device->card);
            SDMMCHOST_EnableCardInt(sdio_device->card.host, true);
        }
    }
}

os_err_t imxrt_sdio_require_irq(sdio_func_num_t func)
{
    os_imxrt_sdio_device_t *sdio_device = OS_NULL;
    if (func == OS_NULL)
    {
        LOG_E(DRV_EXT_TAG, "sdio_func is null!");
        return OS_ERROR;
    }
    sdio_device = (os_imxrt_sdio_device_t *)func;

    SDIO_EnableIOInterrupt(&sdio_device->card, (sdio_func_num_t)func->func_num, OS_TRUE);
    SDIO_SetIOIRQHandler(&sdio_device->card, (sdio_func_num_t)func->func_num, os_imxrt_sdio_irq_handler);
    
    sdio_device->task = os_task_create("os_imxrt_sdio_task", os_imxrt_sdio_irq_task_entry, sdio_device, 1024, 3, 5);
    if (sdio_device->task == OS_NULL)
    {
        LOG_E(DRV_EXT_TAG, "hi_sdio_rx_task creat failed!");
        return OS_ERROR;
    }

    if (os_task_startup(sdio_device->task) != OS_EOK)
    {
        LOG_E(DRV_EXT_TAG, "os_task_startup failed!");
        os_task_destroy(sdio_device->task);
        return OS_ERROR;
    }

    return OS_EOK;
}
#endif

static os_err_t imxrt_sdio_init(struct imxrt_sdio_device *sdio_dev)
{
    sdio_dev->imxrt_usdhc->insert_callback = imxrt_sdio_insert_callback;
    sdio_dev->imxrt_usdhc->insert_userData = sdio_dev;

    sdio_dev->imxrt_usdhc->sdioInt.cardInterrupt = imxrt_sdio_cardinterruptcb;
    sdio_dev->imxrt_usdhc->sdioInt.userData      = sdio_dev->imxrt_usdhc->sdio_card;

    if (sdio_dev->imxrt_usdhc->ops->init == OS_NULL)
    {
        return OS_ERROR;
    }

    if (sdio_dev->imxrt_usdhc->ops->init(sdio_dev->imxrt_usdhc) != OS_EOK)
    {
        return OS_ERROR;
    }

    if (SDIO_Init(sdio_dev->imxrt_usdhc->sdio_card) != kStatus_Success)
    {
        LOG_E(DRV_EXT_TAG, "sdio card init fail");
        return OS_ERROR;
    }

    //    SDIO_EnableIOInterrupt(sdio_dev->imxrt_usdhc->sdio_card, kSDIO_FunctionNum1, true);
    //    SDIO_SetIOIRQHandler(sdio_dev->imxrt_usdhc->sdio_card, kSDIO_FunctionNum1, DEMO_SDIO_IO1_IRQ_Handler);

    return OS_EOK;
}

sdio_card_t *imxrt_sdio_get_card(char *name)
{
    struct imxrt_sdio_device *sdio_dev = OS_NULL;

    sdio_dev = (struct imxrt_sdio_device *)os_device_open_s(name);
    if (sdio_dev == OS_NULL)
    {
        return OS_NULL;
    }

    return sdio_dev->imxrt_usdhc->sdio_card;
}

int imxrt_usdhc_sdio_register(struct os_imxrt_usdhc *imxrt_usdhc)
{
    int  ret;
    char dev_name[6] = "sdio0";

    struct imxrt_sdio_device *sdio_dev = os_calloc(1, sizeof(struct imxrt_sdio_device));
    if (sdio_dev == OS_NULL)
    {
        LOG_E(DRV_EXT_TAG, "sdio_dev memory call failed!");
        return OS_ENOMEM;
    }

    sdio_dev->imxrt_usdhc = imxrt_usdhc;

    if (imxrt_sdio_init(sdio_dev) != OS_EOK)
    {
        return OS_ERROR;
    }

    if (imxrt_usdhc->usdhc_info->base == USDHC1)
    {
        dev_name[4] = '0';
    }
    else if (imxrt_usdhc->usdhc_info->base == USDHC2)
    {
        dev_name[4] = '1';
    }

    if (os_device_register(&sdio_dev->dev, dev_name) != OS_EOK)
    {
        return OS_ERROR;
    }

    LOG_D(DRV_EXT_TAG, "g_os_imxrt_sdio_device success!");
    return OS_EOK;
}
