/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_gpio.c
 *
 * @brief       This file implements gpio driver for imxrt.
 *
 * @revision
 * Date         Author          Notes
 * 2020-09-01   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include <os_task.h>
#include <drv_cfg.h>
#include "drv_gpio.h"
#include "board.h"
#include "fsl_gpio.h"
#include "fsl_iomuxc.h"

#define DRV_LVL DBG_EXT_INFO
#define DRV_TAG "drv.gpio"
#include <drv_log.h>

/* clang-format off */
#if defined(FSL_SDK_DISABLE_DRIVER_CLOCK_CONTROL) && FSL_SDK_DISABLE_DRIVER_CLOCK_CONTROL
    #error "Please don't define 'FSL_SDK_DISABLE_DRIVER_CLOCK_CONTROL'!"
#endif

const os_int8_t reg_offset[] = 
{
#if defined(SOC_IMXRT1015_SERIES)
    38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 64, 65, 66, 67, 68, 69,
    -1, -1, -1, -1,  0,  1,  2,  3,  4,  5, -1, -1, -1, -1, -1, -1, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, -1, -1, -1, -1,
    28, 29, 30, 31, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88,
#elif defined(SOC_IMXRT1020_SERIES)
    42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73,
     0,  1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31,
    32, 33, 34, 35, 36, 37, 38, 39, 40, 41, -1, -1, -1, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92,
#else /* 1050 & 1060 & 1064 series*/
    42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73,
    74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99,100,101,102,103,104,105,
   112,113,114,115,116,117,118,119,120,121,122,123,106,107,108,109,110,111, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, -1, -1, -1, -1,
     0,  1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31,
     0,  1,  2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
#endif
};

static const struct imxrt_prot_info port_tab[] =
{
#ifdef GPIO1
    {GPIO1, GPIO1_Combined_0_15_IRQn, GPIO1_Combined_16_31_IRQn},
#endif

#ifdef GPIO2
    {GPIO2, GPIO2_Combined_0_15_IRQn, GPIO2_Combined_16_31_IRQn},
#endif

#ifdef GPIO3
    {GPIO3, GPIO3_Combined_0_15_IRQn, GPIO3_Combined_16_31_IRQn},
#endif

#ifdef GPIO4
    {GPIO4, GPIO4_Combined_0_15_IRQn, GPIO4_Combined_16_31_IRQn},
#endif

#ifdef GPIO5
    {GPIO5, GPIO5_Combined_0_15_IRQn, GPIO5_Combined_16_31_IRQn},
#endif

#ifdef GPIO6
    {GPIO6, GPIO6_7_8_9_IRQn, GPIO6_7_8_9_IRQn},
#endif

#ifdef GPIO7
    {GPIO7, GPIO6_7_8_9_IRQn, GPIO6_7_8_9_IRQn},
#endif

#ifdef GPIO8
    {GPIO8, GPIO6_7_8_9_IRQn, GPIO6_7_8_9_IRQn},
#endif

#ifdef GPIO9
    {GPIO9, GPIO6_7_8_9_IRQn, GPIO6_7_8_9_IRQn}
#endif
};

static struct os_pin_irq_hdr hdr_tab[PIN_MAX_NUM] ={0};

static void imxrt_gpio_isr(os_int16_t prot_index, os_int8_t pin_start, GPIO_Type *base)
{
    os_uint8_t i;
    os_uint32_t index;
    os_uint32_t isr_status;
    
    isr_status = GPIO_PortGetInterruptFlags(base) & base->IMR;

    for (i = pin_start; i <= PORT_PIN_NUM ; i++)
    {
        if (isr_status & (1 << i))
        {
            GPIO_PortClearInterruptFlags(base, (1 << i));
            
            index = prot_index * PORT_PIN_NUM + i;
            
            if (hdr_tab[index].hdr != OS_NULL)
            {
                hdr_tab[index].hdr(hdr_tab[index].args);
            }
        }
    }
}

#ifdef GPIO1
GPIOX_IRQHandler(1)
#endif

#ifdef GPIO2
GPIOX_IRQHandler(2)
#endif

#ifdef GPIO3
GPIOX_IRQHandler(3)
#endif

#ifdef GPIO4
GPIOX_IRQHandler(4)
#endif

#ifdef GPIO5
GPIOX_IRQHandler(5)
#endif

#ifdef GPIO6
GPIOX_IRQHandler(6)
#endif

#ifdef GPIO7
GPIOX_IRQHandler(7)
#endif

#ifdef GPIO8
GPIOX_IRQHandler(8)
#endif

#ifdef GPIO9
GPIOX_IRQHandler(9)
#endif

void imxrt_pin_config_param(os_base_t pin, struct imxrt_gpio_mode_config *mode_config)
{
    os_uint16_t port;
    struct imxrt_gpio_config_param config_param;
    
    port    = pin >> 5;
    
    mode_config->value = mode_config->slew_rate | mode_config->drv_strength | mode_config->speed | mode_config->od_enable
                        | mode_config->pk_enable | mode_config->pk_select | mode_config->pull_mode | mode_config->hys_enable;
    
#ifdef GPIO5
    if (port_tab[port].base != GPIO5)
    {
        config_param.muxRegister    = IMXRT_GPIO_MUX_BASE + reg_offset[pin] * 4;
        config_param.configRegister = IMXRT_GPIO_CONFIG_BASE + reg_offset[pin] * 4;
    }
    else
    {
        config_param.muxRegister    = IMXRT_GPIO5_MUX_BASE + reg_offset[pin] * 4;
        config_param.configRegister = IMXRT_GPIO5_CONFIG_BASE + reg_offset[pin] * 4;
    }
#else
    config_param.muxRegister    = IMXRT_GPIO_MUX_BASE + reg_offset[pin] * 4;
    config_param.configRegister = IMXRT_GPIO_CONFIG_BASE + reg_offset[pin] * 4;
#endif

    config_param.muxMode            = 0x5U;
    config_param.inputRegister      = 0;
    config_param.inputDaisy         = 0;
    
    IOMUXC_SetPinConfig(config_param.muxRegister, config_param.muxMode, config_param.inputRegister, config_param.inputDaisy, config_param.configRegister, mode_config->value);
}

static os_err_t imxrt_pin_check(os_base_t pin)
{
    if ((pin < 0) || (pin >= PIN_MAX_NUM) || (reg_offset[pin] == -1))
    {
        LOG_D(DRV_TAG, "invalid pin, os pin: %d", pin);
        return OS_ERROR;
    }

    return OS_EOK;
}

static void imxrt_pin_mode(os_device_t *dev, os_base_t pin, os_base_t mode)
{
    os_uint16_t port, pin_num;
    
    struct imxrt_gpio_mode_config mode_config;
    struct imxrt_gpio_config_param config_param;
    struct _gpio_pin_config pin_config = {kGPIO_DigitalInput, 1, kGPIO_NoIntmode};
    
    port    = pin >> 5;
    pin_num = pin & 31;
    
    if (imxrt_pin_check(pin) != OS_EOK)
    {
        return;
    }
    
    switch (mode)
    {
    case PIN_MODE_OUTPUT:
        pin_config.direction        = kGPIO_DigitalOutput;

        mode_config.slew_rate       = fast;
        mode_config.drv_strength    = r0_4;
        mode_config.speed           = medium_100mhz_1;
        mode_config.od_enable       = od_disable;
        mode_config.pk_enable       = pk_enable;
        mode_config.pk_select       = keep;
        mode_config.pull_mode       = pull_down_100k;
        mode_config.hys_enable      = hys_disable;
    break;
    case PIN_MODE_OUTPUT_OD:
        pin_config.direction        = kGPIO_DigitalOutput;
        
        mode_config.slew_rate       = fast;
        mode_config.drv_strength    = r0_4;
        mode_config.speed           = medium_100mhz_1;
        mode_config.od_enable       = od_enable;
        mode_config.pk_enable       = pk_enable;
        mode_config.pk_select       = keep;
        mode_config.pull_mode       = pull_down_100k;
        mode_config.hys_enable      = hys_disable;
    break;
    case PIN_MODE_INPUT:
        pin_config.direction        = kGPIO_DigitalInput;
        
        mode_config.slew_rate       = slow;
        mode_config.drv_strength    = r0_4;
        mode_config.speed           = medium_100mhz_1;
        mode_config.od_enable       = od_disable;
        mode_config.pk_enable       = pk_enable;
        mode_config.pk_select       = pull;
        mode_config.pull_mode       = pull_down_100k;
        mode_config.hys_enable      = hys_disable;
    break;
    case PIN_MODE_INPUT_PULLDOWN:
        pin_config.direction        = kGPIO_DigitalInput;
        
        mode_config.slew_rate       = slow;
        mode_config.drv_strength    = r0_4;
        mode_config.speed           = medium_100mhz_1;
        mode_config.od_enable       = od_disable;
        mode_config.pk_enable       = pk_enable;
        mode_config.pk_select       = pull;
        mode_config.pull_mode       = pull_down_100k;
        mode_config.hys_enable      = hys_disable;
    break;
    case PIN_MODE_INPUT_PULLUP:
        pin_config.direction        = kGPIO_DigitalInput;
        
        mode_config.slew_rate       = slow;
        mode_config.drv_strength    = r0_4;
        mode_config.speed           = medium_100mhz_1;
        mode_config.od_enable       = od_disable;
        mode_config.pk_enable       = pk_enable;
        mode_config.pk_select       = pull;
        mode_config.pull_mode       = pull_up_100k;
        mode_config.hys_enable      = hys_disable;
    break;
    case PIN_MODE_DISABLE:
        pin_config.direction        = kGPIO_DigitalInput;
        
        mode_config.slew_rate       = slow;
        mode_config.drv_strength    = disable;
        mode_config.speed           = medium_100mhz_1;
        mode_config.od_enable       = od_disable;
        mode_config.pk_enable       = pk_disable;
        mode_config.pk_select       = keep;
        mode_config.pull_mode       = pull_down_100k;
        mode_config.hys_enable      = hys_disable;
    break;
    }

    mode_config.value = mode_config.slew_rate | mode_config.drv_strength | mode_config.speed | mode_config.od_enable
                        | mode_config.pk_enable | mode_config.pk_select | mode_config.pull_mode | mode_config.hys_enable;
    
#ifdef GPIO5
    if (port_tab[port].base != GPIO5)
    {
        config_param.muxRegister    = IMXRT_GPIO_MUX_BASE + reg_offset[pin] * 4;
        config_param.configRegister = IMXRT_GPIO_CONFIG_BASE + reg_offset[pin] * 4;
    }
    else
    {
        config_param.muxRegister    = IMXRT_GPIO5_MUX_BASE + reg_offset[pin] * 4;
        config_param.configRegister = IMXRT_GPIO5_CONFIG_BASE + reg_offset[pin] * 4;
    }
#else
    config_param.muxRegister    = IMXRT_GPIO_MUX_BASE + reg_offset[pin] * 4;
    config_param.configRegister = IMXRT_GPIO_CONFIG_BASE + reg_offset[pin] * 4;
#endif

    config_param.muxMode            = 0x5U;
    config_param.inputRegister      = 0;
    config_param.inputDaisy         = 0;

    IOMUXC_SetPinMux(config_param.muxRegister, config_param.muxMode, config_param.inputRegister, config_param.inputDaisy, config_param.configRegister, 0);
    IOMUXC_SetPinConfig(config_param.muxRegister, config_param.muxMode, config_param.inputRegister, config_param.inputDaisy, config_param.configRegister, mode_config.value);

    GPIO_PinInit(port_tab[port].base, pin_num, &pin_config);
}

static int imxrt_pin_read(os_device_t *dev, os_base_t pin)
{
    int value;
    os_int8_t port, pin_num;

    value = PIN_LOW;
    port = pin >> 5;
    pin_num = pin & 31;

    if (imxrt_pin_check(pin) != OS_EOK)
    {
        return value;
    }

    return GPIO_PinReadPadStatus(port_tab[port].base, pin_num);
}

static void imxrt_pin_write(os_device_t *dev, os_base_t pin, os_base_t value)
{
    os_int8_t port, pin_num;

    port = pin >> 5;
    pin_num = pin & 31;

    if (imxrt_pin_check(pin) != OS_EOK)
    {
        return;
    }

    GPIO_PinWrite(port_tab[port].base, pin_num, value);
}

static os_err_t imxrt_pin_attach_irq(struct os_device *device, os_int32_t pin,
                                     os_uint32_t mode, void (*hdr)(void *args), void *args)
{
    os_base_t level;
    os_int8_t port, pin_num;

    port = pin >> 5;
    pin_num = pin & 31;

    if (imxrt_pin_check(pin) != OS_EOK)
    {
        return OS_ENOSYS;
    }

    level = os_irq_lock();
    if (hdr_tab[pin].pin == pin &&
        hdr_tab[pin].hdr == hdr &&
        hdr_tab[pin].mode == mode &&
        hdr_tab[pin].args == args)
    {
        os_irq_unlock(level);
        return OS_EOK;
    }

    hdr_tab[pin].pin  = pin;
    hdr_tab[pin].hdr  = hdr;
    hdr_tab[pin].mode = mode;
    hdr_tab[pin].args = args;
    os_irq_unlock(level);

    return OS_EOK;
}

static os_err_t imxrt_pin_detach_irq(struct os_device *device, os_int32_t pin)
{
    os_base_t level;
    os_int8_t port, pin_num;

    port = pin >> 5;
    pin_num = pin & 31;

    if (imxrt_pin_check(pin) != OS_EOK)
    {
        return OS_ENOSYS;
    }

    level = os_irq_lock();
    if (hdr_tab[pin].pin == -1)
    {
        os_irq_unlock(level);
        return OS_EOK;
    }
    hdr_tab[pin].pin = -1;
    hdr_tab[pin].hdr = OS_NULL;
    hdr_tab[pin].mode = 0;
    hdr_tab[pin].args = OS_NULL;
    os_irq_unlock(level);

    return OS_EOK;
}

static os_err_t imxrt_pin_irq_enable(struct os_device *device, os_base_t pin, os_uint32_t enabled)
{
    IRQn_Type               IRQn;
    gpio_interrupt_mode_t   int_mode;
    const struct imxrt_prot_info *port_info;
    
    os_int8_t port, pin_num, irq_index;

    port = pin >> 5;
    pin_num = pin & 31;

    if (imxrt_pin_check(pin) != OS_EOK)
    {
        return OS_ENOSYS;
    }

    if (hdr_tab[pin].pin == -1)
    {
        LOG_D(DRV_TAG, "os pin: %d callback function not initialized!", pin);
        return OS_ENOSYS;
    }

    if (enabled == PIN_IRQ_ENABLE)
    {
        switch (hdr_tab[pin].mode)
        {
        case PIN_IRQ_MODE_RISING:
            int_mode = kGPIO_IntRisingEdge;
            break;
        case PIN_IRQ_MODE_FALLING:
            int_mode = kGPIO_IntFallingEdge;
            break;
        case PIN_IRQ_MODE_RISING_FALLING:
            int_mode = kGPIO_IntRisingOrFallingEdge;
            break;
        case PIN_IRQ_MODE_HIGH_LEVEL:
            int_mode = kGPIO_IntHighLevel;
            break;
        case PIN_IRQ_MODE_LOW_LEVEL:
            int_mode = kGPIO_IntLowLevel;
            break;
        default:
            int_mode = kGPIO_IntRisingEdge;
            break;
        }
        
        port_info = &port_tab[port];
        
        if (pin_num <= 15)
        {
            IRQn = port_info->irqn0;
        }
        else
        {
            IRQn = port_info->irqn1;
        }
       
        GPIO_PortClearInterruptFlags(port_tab[port].base, 1U << pin_num);

        GPIO_PinSetInterruptConfig(port_tab[port].base, pin_num, int_mode);
        GPIO_PortEnableInterrupts(port_tab[port].base, 1U << pin_num);
        
        NVIC_SetPriority(IRQn, NVIC_EncodePriority(NVIC_GetPriorityGrouping(), 5, 0));

        EnableIRQ(IRQn);
    }
    else if (enabled == PIN_IRQ_DISABLE)
    {
        GPIO_PortDisableInterrupts(port_tab[port].base, 1U << pin_num);
    }
    else
    {
        return OS_EINVAL;
    }

    return OS_EOK;
}
const static struct os_pin_ops imxrt_pin_ops =
{
    imxrt_pin_mode,
    imxrt_pin_write,
    imxrt_pin_read,
    imxrt_pin_attach_irq,
    imxrt_pin_detach_irq,
    imxrt_pin_irq_enable
};

int os_hw_pin_init(void)
{
    int i = 0;
    int ret = OS_EOK;
    
    CLOCK_EnableClock(kCLOCK_Iomuxc);
    CLOCK_EnableClock(kCLOCK_IomuxcSnvs);

    for (i = 0;i < PIN_MAX_NUM;i++)
    {
        hdr_tab[i].pin = -1;
    }
    
    ret = os_device_pin_register(0, &imxrt_pin_ops, OS_NULL);

    return ret;
}
/* clang-format on */
