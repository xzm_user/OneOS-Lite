/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_tempmon.c
 *
 * @brief       This file implements tempmon driver for imxrt.
 *
 * @revision
 * Date         Author          Notes
 * 2020-09-01   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include <board.h>
#include <os_errno.h>
#include <os_assert.h>
#include <os_memory.h>
#include <string.h>

#include "fsl_common.h"
#include "fsl_tempmon.h"
#include "drv_tempmon.h"

#define DRV_EXT_LVL DBG_EXT_INFO
#define DRV_EXT_TAG "drv.tempmon"
#include <drv_log.h>

struct os_imxrt_tempmon
{
    os_device_t              dev;
    struct nxp_tempmon_info *info;
    os_uint8_t               status;
};

struct os_imxrt_tempmon *imxrt_tempmon = OS_NULL;

void TEMP_LOW_HIGH_IRQHandler(void)
{
    os_device_notify(&imxrt_tempmon->dev, OS_IMXRT_TEMPMON_SET_LOW_ALARM | OS_IMXRT_TEMPMON_SET_HIGH_ALARM, 0);

    SDK_ISR_EXIT_BARRIER;
}

void TEMP_PANIC_IRQHandler(void)
{
    os_device_notify(&imxrt_tempmon->dev, OS_IMXRT_TEMPMON_SET_PANIC_ALARM, 0);

    SDK_ISR_EXIT_BARRIER;
}

static os_err_t imxrt_tempmon_init(os_device_t *dev)
{
    struct os_imxrt_tempmon *imxrt_tempmon = (struct os_imxrt_tempmon *)dev;

    TEMPMON_StartMeasure(imxrt_tempmon->info->base);

    return OS_EOK;
}

static os_err_t imxrt_tempmon_deinit(os_device_t *dev)
{
    struct os_imxrt_tempmon *imxrt_tempmon = (struct os_imxrt_tempmon *)dev;

    TEMPMON_StopMeasure(imxrt_tempmon->info->base);

    return OS_EOK;
}

static os_ssize_t imxrt_tempmon_read(os_device_t *dev, os_off_t pos, void *buffer, os_size_t size)
{
    float temperature = 0U;

    struct os_imxrt_tempmon *imxrt_tempmon = (struct os_imxrt_tempmon *)dev;

    if (size != 4)
    {
        LOG_E(DRV_EXT_TAG, "%s read need float type!!", dev->name);
        return 0;
    }

    temperature = TEMPMON_GetCurrentTemperature(imxrt_tempmon->info->base);

    memcpy(buffer, &temperature, size);

    return size;
}

static os_err_t imxrt_tempmon_control(os_device_t *dev, int cmd, void *arg)
{
    os_uint32_t temperature = *((os_uint32_t *)arg);

    struct os_imxrt_tempmon *imxrt_tempmon = (struct os_imxrt_tempmon *)dev;

    switch (cmd)
    {
    case OS_IMXRT_TEMPMON_SET_LOW_ALARM:
        TEMPMON_SetTempAlarm(imxrt_tempmon->info->base, temperature, kTEMPMON_LowAlarmMode);
        EnableIRQ(TEMP_LOW_HIGH_IRQn);
        break;
    case OS_IMXRT_TEMPMON_SET_HIGH_ALARM:
        TEMPMON_SetTempAlarm(imxrt_tempmon->info->base, temperature, kTEMPMON_HighAlarmMode);
        EnableIRQ(TEMP_LOW_HIGH_IRQn);
        break;
    case OS_IMXRT_TEMPMON_SET_PANIC_ALARM:
        TEMPMON_SetTempAlarm(imxrt_tempmon->info->base, temperature, kTEMPMON_PanicAlarmMode);
        EnableIRQ(TEMP_PANIC_IRQn);
        break;
    default:
        break;
    }

    return OS_EOK;
}

static const struct os_device_ops imxrt_tempmon_ops = {
    .init    = imxrt_tempmon_init,
    .deinit  = imxrt_tempmon_deinit,
    .read    = imxrt_tempmon_read,
    .write   = OS_NULL,
    .control = imxrt_tempmon_control,
};

static int imxrt_tempmon_probe(const os_driver_info_t *drv, const os_device_info_t *dev)
{
    imxrt_tempmon = os_calloc(1, sizeof(struct os_imxrt_tempmon));

    OS_ASSERT(imxrt_tempmon);

    imxrt_tempmon->info    = (struct nxp_tempmon_info *)dev->info;
    imxrt_tempmon->dev.ops = &imxrt_tempmon_ops;

    if (os_device_register(&imxrt_tempmon->dev, dev->name) != OS_EOK)
    {
        LOG_E(DRV_EXT_TAG, "%s register failed!", dev->name);
    }

    return OS_EOK;
}

OS_DRIVER_INFO imxrt_tempmon_driver = {
    .name  = "TEMPMON_Type",
    .probe = imxrt_tempmon_probe,
};

OS_DRIVER_DEFINE(imxrt_tempmon_driver, DEVICE, OS_INIT_SUBLEVEL_HIGH);
