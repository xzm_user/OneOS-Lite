/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_gpio.h
 *
 * @brief       This file implements gpio driver for imxrt.
 *
 * @revision
 * Date         Author          Notes
 * 2020-09-01   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#ifndef DRV_GPIO_H__
#define DRV_GPIO_H__

#include <drv_common.h>
#include "board.h"

#if defined(SOC_IMXRT1015_SERIES)
#define IMXRT_GPIO_MUX_BASE    0x401f8024
#define IMXRT_GPIO_CONFIG_BASE 0x401f8198
#define PORT_MAX_NUM           3
#elif defined(SOC_IMXRT1020_SERIES)
#define IMXRT_GPIO_MUX_BASE    0x401f8014
#define IMXRT_GPIO_CONFIG_BASE 0x401f8188
#define PORT_MAX_NUM           3
#else /* 1050 & 1060 & 1064 series*/
#define IMXRT_GPIO_MUX_BASE    0x401f8014
#define IMXRT_GPIO_CONFIG_BASE 0x401f8204
#define PORT_MAX_NUM           5
#endif

#define IMXRT_GPIO5_MUX_BASE    0x400A8000
#define IMXRT_GPIO5_CONFIG_BASE 0x400A8018

#define PORT_PIN_NUM 32
#define PIN_MAX_NUM  (PORT_MAX_NUM * PORT_PIN_NUM)

#define GET_PIN(PORTx, PIN) (32 * (PORTx - 1) + (PIN & 31)) /* PORTx:1,2,3,4,5 */

/* Slew Rate */
#define SRE_0_SLOW_SLEW_RATE IOMUXC_SW_PAD_CTL_PAD_SRE(0)
#define SRE_1_FAST_SLEW_RATE IOMUXC_SW_PAD_CTL_PAD_SRE(1)

/* Drive Strength */
#define DSE_0_OUTPUT_DRIVER_DISABLED IOMUXC_SW_PAD_CTL_PAD_DSE(0)
/* R0 260 Ohm @ 3.3V, 150Ohm@1.8V, 240 Ohm for DDR */
#define DSE_1_R0_1 IOMUXC_SW_PAD_CTL_PAD_DSE(1)
#define DSE_2_R0_2 IOMUXC_SW_PAD_CTL_PAD_DSE(2)
#define DSE_3_R0_3 IOMUXC_SW_PAD_CTL_PAD_DSE(3)
#define DSE_4_R0_4 IOMUXC_SW_PAD_CTL_PAD_DSE(4)
#define DSE_5_R0_5 IOMUXC_SW_PAD_CTL_PAD_DSE(5)
#define DSE_6_R0_6 IOMUXC_SW_PAD_CTL_PAD_DSE(6)
#define DSE_7_R0_7 IOMUXC_SW_PAD_CTL_PAD_DSE(7)

/* Speed */
#define SPEED_0_LOW_50MHz     IOMUXC_SW_PAD_CTL_PAD_SPEED(0)
#define SPEED_1_MEDIUM_100MHz IOMUXC_SW_PAD_CTL_PAD_SPEED(1)
#define SPEED_2_MEDIUM_100MHz IOMUXC_SW_PAD_CTL_PAD_SPEED(2)
#define SPEED_3_MAX_200MHz    IOMUXC_SW_PAD_CTL_PAD_SPEED(3)

/* Open Drain Enable */
#define ODE_0_OPEN_DRAIN_DISABLED IOMUXC_SW_PAD_CTL_PAD_ODE(0)
#define ODE_1_OPEN_DRAIN_ENABLED  IOMUXC_SW_PAD_CTL_PAD_ODE(1)

/* Pull - Keep Enable */
#define PKE_0_PULL_KEEPER_DISABLED IOMUXC_SW_PAD_CTL_PAD_PKE(0)
#define PKE_1_PULL_KEEPER_ENABLED  IOMUXC_SW_PAD_CTL_PAD_PKE(1)

/* Pull - Keep Select */
#define PUE_0_KEEPER_SELECTED IOMUXC_SW_PAD_CTL_PAD_PUE(0)
#define PUE_1_PULL_SELECTED   IOMUXC_SW_PAD_CTL_PAD_PUE(1)

/* Pull Up - Down Config */
#define PUS_0_100K_OHM_PULL_DOWN IOMUXC_SW_PAD_CTL_PAD_PUS(0)
#define PUS_1_47K_OHM_PULL_UP    IOMUXC_SW_PAD_CTL_PAD_PUS(1)
#define PUS_2_100K_OHM_PULL_UP   IOMUXC_SW_PAD_CTL_PAD_PUS(2)
#define PUS_3_22K_OHM_PULL_UP    IOMUXC_SW_PAD_CTL_PAD_PUS(3)

/* Hyst. Enable */
#define HYS_0_HYSTERESIS_DISABLED IOMUXC_SW_PAD_CTL_PAD_HYS(0)
#define HYS_1_HYSTERESIS_ENABLED  IOMUXC_SW_PAD_CTL_PAD_HYS(1)

typedef enum gpio_slew_rate
{
    slow = SRE_0_SLOW_SLEW_RATE,
    fast = SRE_1_FAST_SLEW_RATE,
} gpio_slew_rate_t;

typedef enum gpio_drv_strength
{
    disable = DSE_0_OUTPUT_DRIVER_DISABLED,
    r0_1    = DSE_1_R0_1,
    r0_2    = DSE_2_R0_2,
    r0_3    = DSE_3_R0_3,
    r0_4    = DSE_4_R0_4,
    r0_5    = DSE_5_R0_5,
    r0_6    = DSE_6_R0_6,
    r0_7    = DSE_7_R0_7,
} gpio_drv_strength_t;

typedef enum gpio_speed
{
    low_50mhz       = SPEED_0_LOW_50MHz,
    medium_100mhz_0 = SPEED_1_MEDIUM_100MHz,
    medium_100mhz_1 = SPEED_2_MEDIUM_100MHz,
    max_200mhz      = SPEED_3_MAX_200MHz,
} gpio_speed_t;

typedef enum gpio_open_drain
{
    od_enable  = ODE_1_OPEN_DRAIN_ENABLED,
    od_disable = ODE_0_OPEN_DRAIN_DISABLED,
} gpio_open_drain_t;

typedef enum gpio_pull_keep
{
    pk_enable  = PKE_1_PULL_KEEPER_ENABLED,
    pk_disable = PKE_0_PULL_KEEPER_DISABLED,
} gpio_pull_keep_t;

typedef enum gpio_pk_select
{
    keep = PUE_0_KEEPER_SELECTED,
    pull = PUE_1_PULL_SELECTED,
} gpio_pk_select_t;

typedef enum gpio_pull_mode
{
    pull_down_100k = PUS_0_100K_OHM_PULL_DOWN,
    pull_up_47k    = PUS_1_47K_OHM_PULL_UP,
    pull_up_100k   = PUS_2_100K_OHM_PULL_UP,
    pull_up_22k    = PUS_3_22K_OHM_PULL_UP,
} gpio_pull_mode_t;

typedef enum gpio_hys
{
    hys_enable  = HYS_1_HYSTERESIS_ENABLED,
    hys_disable = HYS_0_HYSTERESIS_DISABLED,
} gpio_hys_t;

struct imxrt_gpio_config_param
{
    uint32_t muxRegister;
    uint32_t muxMode;
    uint32_t inputRegister;
    uint32_t inputDaisy;
    uint32_t configRegister;
};

struct imxrt_prot_info
{
    GPIO_Type *base;
    IRQn_Type  irqn0;
    IRQn_Type  irqn1;
};

struct imxrt_gpio_mode_config
{
    gpio_slew_rate_t    slew_rate;
    gpio_drv_strength_t drv_strength;
    gpio_speed_t        speed;
    gpio_open_drain_t   od_enable;
    gpio_pull_keep_t    pk_enable;
    gpio_pk_select_t    pk_select;
    gpio_pull_mode_t    pull_mode;
    gpio_hys_t          hys_enable;

    os_uint32_t value;
};

#define GPIOX_IRQHandler(_PORT_)                                                                                       \
    void GPIO##_PORT_##_Combined_0_15_IRQHandler(void)                                                                 \
    {                                                                                                                  \
        os_base_t level;                                                                                               \
        level = os_irq_lock();                                                                                         \
        imxrt_gpio_isr(_PORT_ - 1, 0, GPIO##_PORT_);                                                                   \
        os_irq_unlock(level);                                                                                          \
        SDK_ISR_EXIT_BARRIER;                                                                                          \
    }                                                                                                                  \
    void GPIO##_PORT_##_Combined_16_31_IRQHandler(void)                                                                \
    {                                                                                                                  \
        os_base_t level;                                                                                               \
        level = os_irq_lock();                                                                                         \
        imxrt_gpio_isr(_PORT_ - 1, 15, GPIO##_PORT_);                                                                  \
        os_irq_unlock(level);                                                                                          \
        SDK_ISR_EXIT_BARRIER;                                                                                          \
    }

int  os_hw_pin_init(void);
void imxrt_pin_config_param(os_base_t pin, struct imxrt_gpio_mode_config *mode_config);

#endif
