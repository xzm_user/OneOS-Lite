/*
 * Copyright 2017-2020 NXP
 * All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "flexspi_nor_config.h"
#include "board.h"

/* Component ID definition, used by tools. */
#ifndef FSL_COMPONENT_ID
#define FSL_COMPONENT_ID "platform.drivers.xip_board"
#endif

/*******************************************************************************
 * Code
 ******************************************************************************/
#if defined(XIP_BOOT_HEADER_ENABLE) && (XIP_BOOT_HEADER_ENABLE == 1)
#if defined(__CC_ARM) || defined(__ARMCC_VERSION) || defined(__GNUC__)
__attribute__((section(".boot_hdr.conf"), used))
#elif defined(__ICCARM__)
#pragma location = ".boot_hdr.conf"
#endif

#ifdef BSP_NOR_FLASH_S26KS512SDPBHI02
const flexspi_nor_config_t extern_flash_config = {
    .memConfig =
        {
            .tag                = FLEXSPI_CFG_BLK_TAG,
            .version            = FLEXSPI_CFG_BLK_VERSION,
            .readSampleClkSrc   = kFlexSPIReadSampleClk_ExternalInputFromDqsPad,
            .csHoldTime         = 3u,
            .csSetupTime        = 3u,
            .columnAddressWidth = 3u,
            // Enable DDR mode, Wordaddassable, Safe configuration, Differential clock
            .controllerMiscOption =
                (1u << kFlexSpiMiscOffset_DdrModeEnable) | (1u << kFlexSpiMiscOffset_WordAddressableEnable) |
                (1u << kFlexSpiMiscOffset_SafeConfigFreqEnable) | (1u << kFlexSpiMiscOffset_DiffClkEnable),
            .sflashPadType = kSerialFlash_8Pads,
            .serialClkFreq = kFlexSpiSerialClk_133MHz,
            .sflashA1Size  = IMXRT_FLASH_SIZE,
            .dataValidTime = {16u, 16u},
            .lookupTable =
                {
                    // Read LUTs
                    FLEXSPI_LUT_SEQ(CMD_DDR, FLEXSPI_8PAD, 0xA0, RADDR_DDR, FLEXSPI_8PAD, 0x18),
                    FLEXSPI_LUT_SEQ(CADDR_DDR, FLEXSPI_8PAD, 0x10, DUMMY_DDR, FLEXSPI_8PAD, 0x06),
                    FLEXSPI_LUT_SEQ(READ_DDR, FLEXSPI_8PAD, 0x04, STOP, FLEXSPI_1PAD, 0x0),
                },
        },
    .pageSize           = IMXRT_FLASH_PAGE_SIZE,
    .sectorSize         = IMXRT_FLASH_SECTOR_SIZE,
    .blockSize          = IMXRT_FLASH_BLOCK_SIZE,
    .isUniformBlockSize = true,
};
#endif

#if defined(BSP_NOR_FLASH_W25QXX) || defined(BSP_NOR_FLASH_W25QXX_LESS_256)
const flexspi_nor_config_t extern_flash_config =
{
    .memConfig =
        {
            .tag                        = FLEXSPI_CFG_BLK_TAG,
            .version                    = FLEXSPI_CFG_BLK_VERSION,
            .csHoldTime                 = 3U,
            .csSetupTime                = 3U,
            .deviceType                 = kFlexSpiDeviceType_SerialNOR,
            .sflashPadType              = kSerialFlash_4Pads,
            .serialClkFreq              = kFlexSpiSerialClk_133MHz,
            .sflashA1Size               = IMXRT_FLASH_SIZE,
            
#ifdef SOC_IMXRT1052
            .readSampleClkSrc           = kFlexSPIReadSampleClk_LoopbackInternally,
            .deviceModeCfgEnable        = 1u,
            .deviceModeType             = 1u,   /*Quad enable cmd*/
            .deviceModeSeq.seqNum       = 1u,
            .deviceModeSeq.seqId        = 4u,
            .deviceModeArg              = 0x200,/*SET QE=1��S9��*/
#else
            .readSampleClkSrc           = kFlexSPIReadSampleClk_LoopbackFromDqsPad,
#endif
            
            .lookupTable =
                {
#ifdef BSP_NOR_FLASH_W25QXX
                    [4U * NOR_CMD_LUT_SEQ_IDX_READ + 0U] = FLEXSPI_LUT_SEQ(CMD_SDR, FLEXSPI_1PAD, 0xEC, RADDR_SDR, FLEXSPI_4PAD, 0x20),
                    [4U * NOR_CMD_LUT_SEQ_IDX_READ + 1U] = FLEXSPI_LUT_SEQ(DUMMY_SDR, FLEXSPI_4PAD, 0x06, READ_SDR, FLEXSPI_4PAD, 0x04),
                    
                    [4U * NOR_CMD_LUT_SEQ_IDX_READSTATUS + 0U] = FLEXSPI_LUT_SEQ(CMD_SDR, FLEXSPI_1PAD, 0x05, READ_SDR, FLEXSPI_1PAD, 0x04),
#endif

#ifdef BSP_NOR_FLASH_W25QXX_LESS_256
                    [4U * NOR_CMD_LUT_SEQ_IDX_READ + 0U] = FLEXSPI_LUT_SEQ(CMD_SDR, FLEXSPI_1PAD, 0xEB, RADDR_SDR, FLEXSPI_4PAD, 0x18),
                    [4U * NOR_CMD_LUT_SEQ_IDX_READ + 1U] = FLEXSPI_LUT_SEQ(DUMMY_SDR, FLEXSPI_4PAD, 0x06, READ_SDR, FLEXSPI_4PAD, 0x04),
                
                    [4U * NOR_CMD_LUT_SEQ_IDX_READSTATUS + 0U] = FLEXSPI_LUT_SEQ(CMD_SDR, FLEXSPI_1PAD, 0x05, READ_SDR, FLEXSPI_1PAD, 0x04),
#endif
                },
        },
    .pageSize           = IMXRT_FLASH_PAGE_SIZE,
    .sectorSize         = IMXRT_FLASH_SECTOR_SIZE,
    .blockSize          = IMXRT_FLASH_BLOCK_SIZE,
    .isUniformBlockSize = false,
};
#endif

#endif /* XIP_BOOT_HEADER_ENABLE */
