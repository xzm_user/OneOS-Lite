/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_lcd.c
 *
 * @brief       This file implements lcd driver for imxrt.
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include <board.h>
#include <drv_cfg.h>
#include <string.h>
#include <os_memory.h>
#include <graphic/graphic.h>
#include "drv_gpio.h"
#include "fsl_pxp.h"
#include "fsl_elcdif.h"

#define LOG_TAG "drv.lcd"
#include <drv_log.h>

#define LCD_BL_GPIO     GPIO1
#define LCD_BL_GPIO_PIN 15

#define PS_PIXEL_FORMAT     kPXP_PsPixelFormatRGB565
#define OUTPUT_PIXEL_FORMAT kPXP_OutputPixelFormatRGB565

struct imxrt_lcd
{
    os_graphic_t           graphic;
    struct nxp_lcdif_info *lcdif;
};

static volatile os_uint8_t imxrt_lcd_frame_done = 1;

#ifdef OS_GRAPHIC_GPU_ENABLE
static volatile os_uint8_t pxp_run_flag = 0;

void pxp_blend_setup_output_buffer(os_uint8_t               *dest_buffer,
                                   int                       dbpl,
                                   pxp_output_pixel_format_t format,
                                   os_int32_t                w,
                                   os_int32_t                h)
{
    /* Output configure. */
    const pxp_output_buffer_config_t output_buffer_conf = {
        .pixelFormat    = format,
        .interlacedMode = kPXP_OutputProgressive,
        .buffer0Addr    = (uint32_t)dest_buffer,
        .buffer1Addr    = 0U,
        .pitchBytes     = dbpl,
        .width          = w,
        .height         = h,
    };

    PXP_SetOutputBufferConfig(PXP, &output_buffer_conf);
}

void pxp_blend_setup_process_buffer(const os_uint8_t *ps_buffer, os_int32_t pbpl, os_int32_t w, os_int32_t h)
{
    /* PS configure. */
    const pxp_ps_buffer_config_t ps_buffer_conf = {
        .pixelFormat = PS_PIXEL_FORMAT,
        .swapByte    = false,
        .bufferAddr  = (uint32_t)ps_buffer,
        .bufferAddrU = 0U,
        .bufferAddrV = 0U,
        .pitchBytes  = pbpl,
    };

    PXP_SetProcessSurfaceBufferConfig(PXP, &ps_buffer_conf);
    PXP_SetProcessSurfacePosition(PXP, 0, 0, w - 1, h - 1);
}

void pxp_blend_setup_alpha_buffer(const os_uint8_t     *as_buffer,
                                  os_int32_t            abpl,
                                  pxp_as_pixel_format_t format,
                                  os_int32_t            w,
                                  os_int32_t            h)
{
    /* AS config. */
    const pxp_as_buffer_config_t as_buffer_conf = {
        .pixelFormat = format,
        .bufferAddr  = (uint32_t)as_buffer,
        .pitchBytes  = abpl,
    };
    PXP_SetAlphaSurfaceBufferConfig(PXP, &as_buffer_conf);
    PXP_SetAlphaSurfacePosition(PXP, 0, 0, w - 1, h - 1);
}

void pxp_blend_setup_alpha_blend_config(os_int32_t const_alpha)
{
    const_alpha                               = (const_alpha * 255) >> 8;
    const pxp_as_blend_config_t as_blend_conf = {.alpha       = const_alpha,
                                                 .invertAlpha = false,
                                                 .alphaMode   = kPXP_AlphaMultiply,
                                                 .ropMode     = kPXP_RopMergeAs};

    PXP_SetAlphaSurfaceBlendConfig(PXP, &as_blend_conf);
}

void pxp_wait_finish(struct os_device *dev)
{
    if (!pxp_run_flag)
        return;

    while (!(kPXP_CompleteFlag & PXP_GetStatusFlags(PXP)))
    {
        /* Wait for pxp process complete. */
    }

    PXP_ClearStatusFlags(PXP, kPXP_CompleteFlag);

    pxp_run_flag = 0;
}

static void pxp_run()
{
    PXP_Start(PXP);
    pxp_run_flag = 1;
}

void pxp_blend(struct os_device *dev, struct os_device_gpu_info *gpu_info)
{
    os_graphic_t      *graphic = (os_graphic_t *)dev;
    os_graphic_info_t *info    = &graphic->info;
    os_int32_t         pxp_format;

    switch (gpu_info->src_format)
    {
    case OS_GRAPHIC_PIXEL_FORMAT_ARGB8888:
        pxp_format = kPXP_AsPixelFormatARGB8888;
        break;
    case OS_GRAPHIC_PIXEL_FORMAT_RGB888:
        pxp_format = kPXP_AsPixelFormatRGB888;
        break;
    case OS_GRAPHIC_PIXEL_FORMAT_RGB565:
        pxp_format = kPXP_AsPixelFormatRGB565;
        break;
    default:
        os_kprintf("Unsupport pxp format\r\n");
        OS_ASSERT(0);
        break;
    }
    pxp_wait_finish(dev);
    pxp_blend_setup_process_buffer(gpu_info->dest, info->bytes_per_line, gpu_info->width, gpu_info->height);
    pxp_blend_setup_alpha_buffer(gpu_info->src, gpu_info->sbpl, pxp_format, gpu_info->width, gpu_info->height);
    pxp_blend_setup_output_buffer(gpu_info->dest,
                                  info->bytes_per_line,
                                  OUTPUT_PIXEL_FORMAT,
                                  gpu_info->width,
                                  gpu_info->height);
    pxp_blend_setup_alpha_blend_config(gpu_info->alpha);
    pxp_run();
}

static void pxp_blit(struct os_device *dev, struct os_device_gpu_info *gpu_info)
{
    os_graphic_t      *graphic = (os_graphic_t *)dev;
    os_graphic_info_t *info    = &graphic->info;

    pxp_wait_finish(dev);
    pxp_blend_setup_process_buffer(gpu_info->dest, info->bytes_per_line, gpu_info->width, gpu_info->height);
    // disable alpha surface (pure color conversion from process buffer to output buffer)
    PXP_SetAlphaSurfacePosition(PXP, 0xFFFFU, 0xFFFFU, 0U, 0U);
    pxp_blend_setup_output_buffer(gpu_info->dest,
                                  info->bytes_per_line,
                                  kPXP_OutputPixelFormatRGB888P,
                                  gpu_info->width,
                                  gpu_info->height);
    pxp_run();
}

static void pxp_init(void)
{
    PXP_Init(PXP);

    PXP_SetProcessSurfaceBackGroundColor(PXP, 0U);

    /* Disable CSC1, it is enabled by default. */
    PXP_EnableCsc1(PXP, false);
}
#endif

void LCDIF_IRQHandler(void)
{
    uint32_t intStatus;

    intStatus = ELCDIF_GetInterruptStatus(LCDIF);

    ELCDIF_ClearInterruptStatus(LCDIF, intStatus);

    if (intStatus & kELCDIF_CurFrameDone)
    {
        imxrt_lcd_frame_done = 1;
    }

    SDK_ISR_EXIT_BARRIER;
    __DSB();
}

static void imxrt_lcd_display_on(struct os_device *dev, os_bool_t on_off)
{
    GPIO_PinWrite(LCD_BL_GPIO, LCD_BL_GPIO_PIN, on_off);
}

static void imxrt_lcd_flush(struct os_device *dev)
{
    os_graphic_t           *graphic = (os_graphic_t *)dev;
    struct os_graphic_info *info    = &graphic->info;

    DCACHE_CleanInvalidateByRange((uint32_t)(info->framebuffer_curr), info->framebuffer_size);
    ELCDIF_SetNextBufferAddr(LCDIF, (uint32_t)(info->framebuffer_curr));

    imxrt_lcd_frame_done = 0;
    while (!imxrt_lcd_frame_done)
    {
    }
}

const static struct os_graphic_ops ops = {
    .display_on   = imxrt_lcd_display_on,
    .display_area = OS_NULL,
    .frame_flush  = imxrt_lcd_flush,

#ifdef OS_GRAPHIC_GPU_ENABLE
    .gpu_blend          = pxp_blend,
    .gpu_alphamap_blend = OS_NULL,
    .gpu_blit           = pxp_blit,
    .gpu_wait_finish    = pxp_wait_finish,
#endif
};

static int imxrt_lcd_probe(const os_driver_info_t *drv, const os_device_info_t *dev)
{
    struct imxrt_lcd *lcd;
    os_uint32_t       size;

    lcd = os_calloc(1, sizeof(struct imxrt_lcd));
    OS_ASSERT(lcd);

    lcd->lcdif       = (struct nxp_lcdif_info *)dev->info;
    lcd->graphic.ops = &ops;

#ifdef OS_GRAPHIC_GPU_ENABLE
    pxp_init();
#endif

    EnableIRQ(LCDIF_IRQn);
    ELCDIF_RgbModeStart(LCDIF_PERIPHERAL);

    // Backlight
    const gpio_pin_config_t config = {
        kGPIO_DigitalOutput,
        1,
        kGPIO_NoIntmode,
    };
    GPIO_PinInit(LCD_BL_GPIO, LCD_BL_GPIO_PIN, &config);

    os_graphic_register("lcd", &lcd->graphic);

    size = OS_GRAPHIC_LCD_WIDTH * OS_GRAPHIC_LCD_HEIGHT * OS_GRAPHIC_LCD_DEPTH / 8;
    os_graphic_add_framebuffer(&lcd->graphic.parent, (os_uint8_t *)lcd->lcdif->config->bufferAddr, size);
    os_graphic_add_framebuffer(&lcd->graphic.parent, (os_uint8_t *)(lcd->lcdif->config->bufferAddr + size), size);

    os_kprintf("imxrt lcd probe\r\n");

    return OS_EOK;
}

OS_DRIVER_INFO imxrt_lcd_driver = {
    .name  = "LCDIF_Type",
    .probe = imxrt_lcd_probe,
};

OS_DRIVER_DEFINE(imxrt_lcd_driver, DEVICE, OS_INIT_SUBLEVEL_MIDDLE);
