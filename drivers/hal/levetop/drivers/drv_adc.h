/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_adc.h
 *
 * @brief       This file provides functions declaration for lt776 adc driver.
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#ifndef __DRV_ADC_H__
#define __DRV_ADC_H__

#include <board.h>
#include <adc_drv.h>
#include <sys.h>

typedef enum
{
    ADC_CLK_DIV_1 = 0,
    ADC_CLK_DIV_2,
    ADC_CLK_DIV_3,
    ADC_CLK_DIV_4,
    ADC_CLK_DIV_5,
    ADC_CLK_DIV_6,
    ADC_CLK_DIV_7,
    ADC_CLK_DIV_8,
    ADC_CLK_DIV_9,
    ADC_CLK_DIV_10,
    ADC_CLK_DIV_11,
    ADC_CLK_DIV_12,
    ADC_CLK_DIV_13,
    ADC_CLK_DIV_14,
    ADC_CLK_DIV_15,
    ADC_CLK_DIV_16
} ADC_ClkDivTypeDef;

typedef struct
{
    os_uint8_t channel;    /*< ADC通道号，参照ADC_ChannelTypeDef定义 */
    os_uint8_t resolution; /*< ADC采样精度设置，参照ADC_ResTypeDef定义 */
    os_uint8_t clk_div;    /*< ADC时钟分频 - QCLK = SYS_CLK/clk_div */
    os_uint8_t stab_time;  /*< ADC转化开始稳定时间约2us - 2*(10^-6)*(1/QCLK) */
    os_uint8_t smp_time;   /*< ADC采样时间 - (smp_time + 2)*QCLKs */

    os_uint8_t vref;     /*< ADC基准电压源选择，参照ADC_VrefTypeDef定义 */
    os_uint8_t align;    /*< ADC采样值对其方式，参照ADC_AlignTypeDef定义 */
    os_uint8_t conv;     /*< ADC采样转换模式，参照ADC_ConvModeTypeDef定义 */
    os_uint8_t overrun;  /*< ADC采样转换模式，参照ADC_OverrunManagTypeDef定义 */
    os_uint8_t int_kind; /*< ADC采样中断类型选择 */
} ADC_InitTypeDef;

struct lt776_adc
{
    struct os_adc_device adc;
    os_uint8_t           status;
};

#endif
