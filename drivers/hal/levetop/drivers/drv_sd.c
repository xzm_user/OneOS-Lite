/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_sd.c
 *
 * @brief       This file provides sd card device register.
 *
 * @revision
 * Date         Author          Notes
 * 2022-02-17   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include <drv_sd.h>

#define DRV_EXT_TAG "drv.sd"
#include <drv_log.h>

static os_err_t lt776_sd_init(struct lt776_sd_device *sd_dev)
{
#if defined(__CC_ARM) || defined(__CLANG_ARM)
    if (HAL_SD_EnumCard() != SD_OK)
#elif defined(__GNUC__)
    if (InitSdCard() != SD_OK)
#endif
    {
        LOG_E(DRV_EXT_TAG, "lt776_sd_init failed");
        return OS_ERROR;
    }

    return OS_EOK;
}

static int os_lt776_sd_read_block(os_blk_device_t *blk, os_uint32_t block_addr, os_uint8_t *buff, os_uint32_t block_nr)
{
    OS_ASSERT(blk != OS_NULL);
    struct lt776_sd_device *sd_dev = (struct lt776_sd_device *)blk;

#if defined(__CC_ARM) || defined(__CLANG_ARM)
    os_uint8_t res = HAL_SD_ReadBlock(block_addr, (os_uint32_t *)buff, sd_dev->info->block_size, block_nr);
    if (res != SD_OK)
    {
        HAL_SD_Init();
        res = HAL_SD_ReadBlock(block_addr, (os_uint32_t *)buff, sd_dev->info->block_size, block_nr);
    }
#elif defined(__GNUC__)
    os_uint8_t res = ReadSdCardBlock(block_addr, (os_uint32_t *)buff, sd_dev->info->block_size, block_nr);
    if (res != SD_OK)
    {
        InitSdCard();
        res = ReadSdCardBlock(block_addr, (os_uint32_t *)buff, sd_dev->info->block_size, block_nr);
    }
#endif

    if (res != SD_OK)
    {
        LOG_E(DRV_EXT_TAG, "read addr: %d, count: %d", block_addr, block_nr);
        return OS_ERROR;
    }

    return OS_EOK;
}

static int
os_lt776_sd_write_block(os_blk_device_t *blk, os_uint32_t block_addr, const os_uint8_t *buff, os_uint32_t block_nr)
{
    OS_ASSERT(blk != OS_NULL);
    struct lt776_sd_device *sd_dev = (struct lt776_sd_device *)blk;

#if defined(__CC_ARM) || defined(__CLANG_ARM)
    os_uint8_t res = HAL_SD_WriteBlock(block_addr, (os_uint32_t *)buff, sd_dev->info->block_size, block_nr);
    if (res != SD_OK)
    {
        HAL_SD_Init();
        res = HAL_SD_WriteBlock(block_addr, (os_uint32_t *)buff, sd_dev->info->block_size, block_nr);
    }
#elif defined(__GNUC__)
    os_uint8_t res = WriteSdCardBlock(block_addr, (os_uint32_t *)buff, sd_dev->info->block_size, block_nr);
    if (res != SD_OK)
    {
        InitSdCard();
        res = WriteSdCardBlock(block_addr, (os_uint32_t *)buff, sd_dev->info->block_size, block_nr);
    }
#endif

    if (res != SD_OK)
    {
        LOG_E(DRV_EXT_TAG, "write addr: %d, count: %d", block_addr, block_nr);
        return OS_ERROR;
    }

    return OS_EOK;
}

const static struct os_blk_ops sd_blk_ops = {
    .read_block  = os_lt776_sd_read_block,
    .write_block = os_lt776_sd_write_block,
};

static os_bool_t IsSdcardInserted(void)
{
    os_bool_t sdDect = OS_FALSE;
    if (!(CPM->PADWKINTCR & 2))    //通过检测wakeup脚高低电平。如果有SD卡插入(0), 则
    {
        if (!sdDect)
        {
            os_kprintf("Sdcard is inserted!!!\r\n");
        }
        sdDect = OS_TRUE;
    }
    else    //否则对应位写1 来清除该位的数据
    {
        if (sdDect)
        {
            os_kprintf("Sdcard is plugged out!!!\r\n");
        }
        CPM->PADWKINTCR |= 0x02;
        sdDect = OS_FALSE;
    }

    return sdDect;
}

static int lt776_sd_probe(const os_driver_info_t *drv, const os_device_info_t *dev)
{
    if (!IsSdcardInserted())
    {
        return OS_ENOMEM;
    }

    struct lt776_sd_device *sd_dev = os_calloc(1, sizeof(struct lt776_sd_device));
    if (sd_dev == OS_NULL)
    {
        LOG_E(DRV_EXT_TAG, "sd_dev memory call failed!");
        return OS_ENOMEM;
    }

    if (lt776_sd_init(sd_dev) != OS_EOK)
    {
        return OS_ERROR;
    }

    sd_dev->info = (struct lt776_sd_info *)dev->info;

    sd_dev->blk_dev.geometry.block_size = sd_dev->info->block_size;
    sd_dev->blk_dev.geometry.capacity   = sd_dev->info->capacity;
    sd_dev->blk_dev.blk_ops             = &sd_blk_ops;

    return block_device_register(&sd_dev->blk_dev, dev->name);
}

OS_DRIVER_INFO lt776_sd_driver = {
    .name  = "SD_Type",
    .probe = lt776_sd_probe,
};

OS_DRIVER_DEFINE(lt776_sd_driver, PREV, OS_INIT_SUBLEVEL_LOW);
