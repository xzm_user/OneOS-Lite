/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_graphic.h
 *
 * @brief       This file provides functions declaration for graphic driver.
 *
 * @revision
 * Date         Author          Notes
 * 2020-11-17   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#ifndef __DRV_GRAPHIC_H__
#define __DRV_GRAPHIC_H__

#include <graphic/graphic.h>
#include <drv_gpio.h>
#include <clcd_drv.h>
#include <sdram_drv.h>

#define BACKLIGHT_PWR_PIN PIN(46)
#define SCREEN_SIZE       (OS_GRAPHIC_LCD_WIDTH * OS_GRAPHIC_LCD_HEIGHT * OS_GRAPHIC_LCD_DEPTH >> 3)
#define LCDC_ADDR0        (0x80000000)
#define LCDC_ADDR1        (0x80000000 + 1 * SCREEN_SIZE)

typedef void (*clcd_callback_t)(void *);

/**
 * @brief CLCD 初始化时序结构体定义
 *
 */
typedef struct
{
    volatile os_uint16_t flags;  /**< 使能标志位       */
    volatile os_uint16_t width;  /**< 每行像素点       */
    volatile os_uint16_t height; /**< 垂直像素点       */
    volatile os_uint16_t hbp;    /**< 水平前沿周期     */
    volatile os_uint16_t hfp;    /**< 水平后沿周期     */
    volatile os_uint16_t hsync;  /**< 水平同步脉冲宽度 */
    volatile os_uint8_t  vbp;    /**< 垂直前沿周期     */
    volatile os_uint8_t  vfp;    /**< 垂直后沿周期     */
    volatile os_uint8_t  vsync;  /**< 垂直同步脉冲宽度 */
} CLCDTiming_InitTypeDef;

/**
 * @brief CLCD 初始化配置结构体定义
 *
 */
typedef struct
{
    volatile os_uint32_t control;    //.
    volatile os_uint32_t fb0_addr;   /**< frame0 memory address */
    volatile os_uint32_t fb1_addr;   /**< frame1 memory address, only used when double buffer is enabled. */
    volatile float       fps;        /**< lcd panel actual frame rate.*/
} CLCDCfg_InitTypeDef;

/**
 * @brief CLCD 初始化结构体定义
 *
 */
typedef struct
{
    CLCDTiming_InitTypeDef clcd_timeing; /**< 时序      */
    FunctionalStateTypeDef enableIE;     /**< 中断使能  */
    os_uint32_t            clcd_clkdiv;  /**< CLCD时钟分频  */
    CLCDCfg_InitTypeDef    clcd_cfg;     /**< 配置          */
} CLCD_InitTypeDef;

/**
 * @brief CLCD  句柄结构体定义
 *
 */
typedef struct
{
    CLCD_TypeDef    *instance; /**< CLCD实例寄存器 */
    CLCD_InitTypeDef init;     /**< CLCD初始化结构体 */
    clcd_callback_t  callback; /**< 回调函数*/
} CLCD_HandleTypeDef;

struct lt776_lcd
{
    os_graphic_t       graphic;
    CLCD_HandleTypeDef hclcd;
};

os_uint32_t GetSdramAddr(os_uint8_t curAddr, os_uint8_t copySreen, CLCD_TypeDef *hclcd);

#endif
