/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_uart.h
 *
 * @brief       This file provides functions declaration for uart driver.
 *
 * @revision
 * Date         Author          Notes
 * 2020-11-17   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#ifndef __DRV_UART_H__
#define __DRV_UART_H__

#include "board.h"
#include "uart_drv.h"
#include "lt776_dma.h"
#include "lt776_reg.h"

struct lt776_uart_info
{
    UART_TypeDef      *huart;
    UART_MODE          uart_mode;
    IRQn_Type          irqn;
    os_uint32_t        dma_addr_start;
    os_uint32_t        dma_base_addr;
    DMAC_HandleTypeDef hdma;
};

#endif
