/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_pwm.h
 *
 * @brief       This file provides functions declaration for lt776 pwm driver.
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#ifndef __DRV_PWM_H__
#define __DRV_PWM_H__

#include <board.h>
#include <type.h>
#include <lt776_reg.h>
#include <pwm_drv.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef void (*pwm_callback_t)(void *);

/**
 * @brief PWM 方向枚举类型
 *
 */
typedef enum
{
    PWM_INPUT = 0,
    PWM_OUTPUT,
} PWM_DirectionTypeDef;

/**
 * @brief PWM 输出初始化结构体定义
 *
 */
typedef struct
{
    uint8_t                deadzone;       /**< PWM:死区翻转值 		 */
    uint8_t                prescaler;      /**< PWM:PWM预分频值 		*/
    uint16_t               period;         /**< PWM:频率周期				*/
    uint16_t               width;          /**< PWM:占空比宽度 		 */
    PWM_CLKDIV             clkdiv;         /**< PWM:时钟分频系数		*/
    FunctionalStateTypeDef timerInv;       /**< PWM:通道翻转使能	 */
    FunctionalStateTypeDef autoload;       /**< PWM:自动加载模式使能*/
    FunctionalStateTypeDef pullup;         /**< PWM:拉高使能				*/
    BitActionTypeDef       level;          /**< PWM:初始电平				*/
    FunctionalStateTypeDef timerInterrupt; /**< PWM:产生period个波形后是否产生中断*/
} PWMOutput_TypeDef;

/**
 * @brief PWM 输出初始化结构体定义
 *
 */
typedef struct
{
    uint8_t                deadzone;      /**< PWM:死区翻转值 		 */
    uint8_t                prescaler;     /**< PWM:PWM预分频值 		*/
    uint16_t               period;        /**< PWM:频率周期				*/
    PWM_CLKDIV             clkdiv;        /**< PWM:时钟分频系数		*/
    uint32_t               edgeinterrupt; /**< PWM:边沿中断				*/
    FunctionalStateTypeDef CHxInv;        /**< PWM:通道翻转使能	 */
    FunctionalStateTypeDef autoload;      /**< PWM:自动加载模式使能*/
} PWMInput_TypeDef;

/**
 * @brief PWM GPIO初始化结构体定义
 *
 */
typedef struct
{
    FunctionalStateTypeDef pullup; /**< PWM:拉高使能		 */
    BitActionTypeDef       level;  /**< PWM:初始电平				*/
} PWMGpio_TypeDef;

/**
 * @brief PWM 初始化结构体定义
 *
 */
typedef struct
{
    uint8_t channel;   /**< PWM通道：0-3 			 */
    uint8_t direction; /**< PWM方向：0-1 			 */
    union
    {
        PWMInput_TypeDef  in;   /**< PWM输入捕获参数配置 */
        PWMOutput_TypeDef out;  /**< PWM输出波形参数配置 */
        PWMGpio_TypeDef   gpio; /**< PWMGPIO功能参数配置 */
    };
} PWM_InitTypeDef;

/**
 * @brief PWM	句柄结构体定义
 *
 */
typedef struct
{
    PWM_TypeDef    *instance;        /**< pwm实例寄存器 */
    PWM_InitTypeDef init;            /**< pwm初始化结构体 */
    pwm_callback_t  RisingCallback;  /**< 上升沿中断回调函数*/
    pwm_callback_t  FallingCallback; /**< 下降沿中断回调函数*/
    pwm_callback_t  CaptureCallback; /**< 捕获中断回调函数*/
} PWM_HandleTypeDef;

struct lt776_pwm_info
{
    PWM_HandleTypeDef hpwm;
};

struct lt776_pwm
{
    struct os_pwm_device   pwm;
    struct lt776_pwm_info *info;
};

#ifdef __cplusplus
}
#endif

#endif /* __PWM_H__*/
