/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the \"License\ you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an \"AS IS\" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * \@file        drv_uart.c
 *
 * \@brief       This file implements uart driver for lt776.
 *
 * \@revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */
#include <sys.h>
#include <drv_uart.h>
#include <lt776_dma.h>

static os_list_node_t lt776_uart_list = OS_LIST_INIT(lt776_uart_list);

struct lt776_uart
{
    struct os_serial_device serial;
    struct lt776_uart_info *info;

    soft_dma_t sdma;

    os_uint8_t *rx_buff;
    os_uint32_t rx_size;
    os_uint32_t rx_index;

    const os_uint8_t *tx_buff;
    os_uint32_t       tx_size;
    os_uint32_t       tx_index;

    os_list_node_t list;
};

void dma_irq_handle(DMAC_TypeDef *dmac)
{
    struct lt776_uart *uart = OS_NULL;

    os_list_for_each_entry(uart, &lt776_uart_list, struct lt776_uart, list)
    {
        if (uart->info->hdma.instance == dmac)
        {
            if (DRV_DMAC_GetRawStatus(uart->info->hdma.instance, uart->info->hdma.init.channel_num) == BIT_SET)
            {
                soft_dma_full_irq(&uart->sdma);
                DRV_DMAC_ClearRawStatus(uart->info->hdma.instance, uart->info->hdma.init.channel_num);
            }
        }
        return;
    }
}

void DMA1_IRQHandler(void)
{
    dma_irq_handle(DMAC1);
    return;
}

static void uart_isr(UART_TypeDef *huart)
{
    struct lt776_uart *uart;

    os_list_for_each_entry(uart, &lt776_uart_list, struct lt776_uart, list)
    {
        if (huart == uart->info->huart)
        {
            /* rx */
            if (_uart_get_fifo_flag(uart->info->huart, UART_FIFO_FLAG_RTOS) == UART_FIFO_FLAG_RTOS)
            {
                while (_uart_get_fifo_flag(uart->info->huart, UART_FIFO_FLAG_R_EMPTY) == RESET)
                {
                    uart->rx_buff[uart->rx_index++] = _uart_get_data(uart->info->huart);
                }
                soft_dma_timeout_irq(&uart->sdma);
            }
            else if (_uart_get_fifo_flag(uart->info->huart, UART_FIFO_FLAG_RFTS) == UART_FIFO_FLAG_RFTS)
            {
                while (_uart_get_fifo_flag(uart->info->huart, UART_FIFO_FLAG_R_EMPTY) == RESET)
                {
                    uart->rx_buff[uart->rx_index++] = _uart_get_data(uart->info->huart);
                }
                soft_dma_full_irq(&uart->sdma);
            }

            /* tx */
            if (_uart_get_tdre_it_flag(uart->info->huart) != RESET)
            {
                _uart_clear_tx_it_flag(uart->info->huart);
                if (uart->tx_size > 0)
                {
                    if (uart->tx_index < uart->tx_size)
                    {
                        uart->info->huart->DRL = uart->tx_buff[uart->tx_index++];
                    }
                    else
                    {
                        uart->tx_size = 0;
                        _uart_dis_tdre_it(uart->info->huart);
                        os_hw_serial_isr_txdone((struct os_serial_device *)uart);
                    }
                }
            }

            return;
        }
    }
}

void UART1_IRQHandler(void)
{
    uart_isr(UART1);
}

void UART2_IRQHandler(void)
{
    uart_isr(UART2);
}

void UART3_IRQHandler(void)
{
    uart_isr(UART3);
}

void UART4_IRQHandler(void)
{
    uart_isr(UART4);
}

static os_uint32_t lt776_sdma_int_get_index(soft_dma_t *dma)
{
    struct lt776_uart *uart = os_container_of(dma, struct lt776_uart, sdma);

    return uart->rx_index;
}

static os_err_t lt776_sdma_int_start(soft_dma_t *dma, void *buff, os_uint32_t size)
{
    struct lt776_uart *uart = os_container_of(dma, struct lt776_uart, sdma);

    uart->rx_buff  = buff;
    uart->rx_size  = size;
    uart->rx_index = 0;

    _uart_fifo_rx_timeout_it_en(uart->info->huart);
    _uart_fifo_rx_it_en(uart->info->huart);

    return OS_EOK;
}

static os_uint32_t lt776_sdma_int_stop(soft_dma_t *dma)
{
    struct lt776_uart *uart = os_container_of(dma, struct lt776_uart, sdma);

    _uart_fifo_rx_timeout_it_dis(uart->info->huart);
    _uart_fifo_rx_it_dis(uart->info->huart);

    return uart->rx_index;
}

static os_uint32_t lt776_sdma_dma_get_index(soft_dma_t *dma)
{
    struct lt776_uart *uart = os_container_of(dma, struct lt776_uart, sdma);

    return *(os_uint32_t *)(uart->info->dma_base_addr + 0x8 + 0x58 * uart->info->hdma.init.channel_num) -
           uart->info->dma_addr_start;
}

static os_err_t lt776_sdma_dma_init(soft_dma_t *dma)
{
    struct lt776_uart *uart = os_container_of(dma, struct lt776_uart, sdma);

    DMAC_Deinit(&uart->info->hdma);

    NVIC_Init(3, 3, DMA1_IRQn, 2);
    NVIC_SetPriority(DMA1_IRQn, 0x03U);

    uart->info->hdma.init.dst_msize       = DMAC_MSIZE_1;
    uart->info->hdma.init.src_msize       = DMAC_MSIZE_1;
    uart->info->hdma.init.dst_inc         = DMA_DINC_INC;
    uart->info->hdma.init.src_inc         = DMA_SINC_NO_CHANGE;
    uart->info->hdma.init.dst_tr_width    = DMAC_TR_WIDTH_8BITS;
    uart->info->hdma.init.src_tr_width    = DMAC_TR_WIDTH_8BITS;
    uart->info->hdma.init.transfer_type   = DMAC_TRANSFERTYPE_P2M_CONTROLLER_DMA;
    uart->info->hdma.init.peripheral_type = DMAC_PERIPHERAL_UART1_RX;
    uart->info->hdma.init.transfer_lli    = DMA_LLP_DIS;

    return OS_EOK;
}

static os_err_t lt776_sdma_dma_start(soft_dma_t *dma, void *buff, os_uint32_t size)
{
    struct lt776_uart *uart = os_container_of(dma, struct lt776_uart, sdma);

    uart->rx_buff = buff;
    uart->rx_size = size;

    uart->info->hdma.init.src_addr      = (os_uint32_t)&uart->info->huart->DRL;
    uart->info->hdma.init.dst_addr      = (os_uint32_t)buff;
    uart->info->hdma.init.transfer_size = size;
    DMAC_Init(&uart->info->hdma);

    /*Enable DMAC*/
    _dmac_en(uart->info->hdma.instance);

    /* Disable the selected DMA Channelx */
    _dmac_ch_dis(uart->info->hdma.instance, uart->info->hdma.init.channel_num);

    _dmac_int_en_en(uart->info->hdma.instance, uart->info->hdma.init.channel_num);
    _dmac_raw_interrupt_en(uart->info->hdma.instance, uart->info->hdma.init.channel_num);

    uart->info->dma_addr_start =
        *(os_uint32_t *)(uart->info->dma_base_addr + 0x8 + 0x58 * uart->info->hdma.init.channel_num);

    /* Enable the selected DMA Channelx */
    _dmac_ch_en(uart->info->hdma.instance, uart->info->hdma.init.channel_num);
    _uart_rx_dma_en(uart->info->huart);

    return OS_EOK;
}

static os_uint32_t lt776_sdma_dma_stop(soft_dma_t *dma)
{
    struct lt776_uart *uart = os_container_of(dma, struct lt776_uart, sdma);

    _dmac_ch_dis(uart->info->hdma.instance, uart->info->hdma.init.channel_num);
    _uart_rx_dma_dis(uart->info->huart);

    return lt776_sdma_dma_get_index(dma);
}

static void lt776_usart_sdma_callback(soft_dma_t *dma)
{
    struct lt776_uart *uart = os_container_of(dma, struct lt776_uart, sdma);

    os_hw_serial_isr_rxdone((struct os_serial_device *)uart);
}

static void lt776_usart_sdma_init(struct lt776_uart *uart, dma_ring_t *ring)
{
    soft_dma_t *dma = &uart->sdma;

    soft_dma_stop(dma);

    memset(&dma->hard_info, 0, sizeof(dma->hard_info));

    dma->hard_info.max_size     = 64 * 1024;
    dma->hard_info.flag         = HARD_DMA_FLAG_HALF_IRQ | HARD_DMA_FLAG_FULL_IRQ;
    dma->hard_info.data_timeout = uart_calc_byte_timeout_us(uart->serial.config.baud_rate);

    if (uart->info->uart_mode == UART_INT_MODE)
    {
        dma->hard_info.mode = HARD_DMA_MODE_NORMAL;

        dma->ops.get_index = lt776_sdma_int_get_index;
        dma->ops.dma_init  = OS_NULL;
        dma->ops.dma_start = lt776_sdma_int_start;
        dma->ops.dma_stop  = lt776_sdma_int_stop;
    }
    else
    {
        dma->hard_info.mode = HARD_DMA_MODE_NORMAL;
        dma->ops.get_index  = lt776_sdma_dma_get_index;
        dma->ops.dma_init   = lt776_sdma_dma_init;
        dma->ops.dma_start  = lt776_sdma_dma_start;
        dma->ops.dma_stop   = lt776_sdma_dma_stop;
    }

    dma->cbs.dma_half_callback    = lt776_usart_sdma_callback;
    dma->cbs.dma_full_callback    = lt776_usart_sdma_callback;
    dma->cbs.dma_timeout_callback = lt776_usart_sdma_callback;

    soft_dma_init(dma);
    soft_dma_start(dma, ring);
    soft_dma_irq_enable(&uart->sdma, OS_TRUE);
}

static os_err_t lt776_uart_fifo_init(UART_TypeDef *huart)
{
    _reg_clear(huart->FCR);
    /* 1/8 full */
    _uart_set_rx_fifo_level(huart, 0);

    _uart_fifo_rx_timeout_it_en(huart);
    _uart_fifo_rx_it_en(huart);
    _uart_fifo_rx_en(huart);

    return OS_EOK;
}

static os_err_t lt776_uart_init(struct os_serial_device *serial, struct serial_configure *cfg)
{
    struct lt776_uart *uart            = OS_NULL;
    UART_InitTypeDef   UART_InitStruct = {0};

    OS_ASSERT(serial != OS_NULL);
    OS_ASSERT(cfg != OS_NULL);

    uart = os_container_of(serial, struct lt776_uart, serial);
    OS_ASSERT(uart != OS_NULL);

    switch (cfg->parity)
    {
    case PARITY_NONE:
        UART_InitStruct.Parity = UART_PARITY_MODE_NONE;
        break;
    case PARITY_ODD:
        UART_InitStruct.Parity = UART_PARITY_MODE_ODD;
        break;
    case PARITY_EVEN:
        UART_InitStruct.Parity = UART_PARITY_MODE_EVEN;
        break;
    default:
        return OS_EINVAL;
    }

    switch (cfg->data_bits)
    {
    case DATA_BITS_8:
        UART_InitStruct.WordLength = UART_WORDLENGTH_8B;
        break;
    case DATA_BITS_9:
        UART_InitStruct.WordLength = UART_WORDLENGTH_9B;
        break;
    default:
        return OS_EINVAL;
    }

    UART_InitStruct.IPSFreq   = g_ips_clk;
    UART_InitStruct.BaudRate  = cfg->baud_rate;
    UART_InitStruct.UART_Mode = uart->info->uart_mode;
    DRV_UART_Init(uart->info->huart, UART_InitStruct);

    lt776_uart_fifo_init(uart->info->huart);

    NVIC_Init(3, 3, uart->info->irqn, 2);
    NVIC_SetPriority(uart->info->irqn, 0x03U);

    lt776_usart_sdma_init(uart, &serial->rx_fifo->ring);
    return OS_EOK;
}

static os_err_t lt776_uart_deinit(struct os_serial_device *serial)
{
    OS_ASSERT(serial != OS_NULL);

    struct lt776_uart *uart = os_container_of(serial, struct lt776_uart, serial);

    soft_dma_stop(&uart->sdma);

    DRV_UART_Deinit(uart->info->huart);

    return OS_EOK;
}

static int lt776_uart_start_send(struct os_serial_device *serial, const os_uint8_t *buff, os_size_t size)
{
    struct lt776_uart *uart;

    OS_ASSERT(serial != OS_NULL);
    uart = os_container_of(serial, struct lt776_uart, serial);

    uart->tx_index = 0;
    uart->tx_size  = size;
    uart->tx_buff  = buff;

    _uart_en_tdre_it(uart->info->huart);
    return size;
}

static int lt776_uart_poll_send(struct os_serial_device *serial, const os_uint8_t *buff, os_size_t size)
{
    os_size_t i;
    os_base_t level;

    struct lt776_uart *uart = os_container_of(serial, struct lt776_uart, serial);
    OS_ASSERT(serial != OS_NULL);

    _uart_dis_tdre_it(uart->info->huart);
    _uart_dis_tc_it(uart->info->huart);

    for (i = 0; i < size; i++)
    {
        level = os_irq_lock();

        while (_uart_get_tdre_it_flag(uart->info->huart) == RESET)
        {
            ;
        }
        uart->info->huart->DRL = buff[i];

        os_irq_unlock(level);
    }

    return size;
}

static const struct os_uart_ops lt776_uart_ops = {
    .init       = lt776_uart_init,
    .deinit     = lt776_uart_deinit,
    .start_send = lt776_uart_start_send,
    .poll_send  = lt776_uart_poll_send,
};

static int lt776_uart_probe(const os_driver_info_t *drv, const os_device_info_t *dev)
{
    os_base_t level;

    struct serial_configure config = OS_SERIAL_CONFIG_DEFAULT;

    struct lt776_uart *uart;

    uart = os_calloc(1, sizeof(struct lt776_uart));
    OS_ASSERT(uart);

    uart->info                      = (struct lt776_uart_info *)dev->info;
    struct os_serial_device *serial = &uart->serial;

    level = os_irq_lock();
    os_list_add_tail(&lt776_uart_list, &uart->list);
    os_irq_unlock(level);

    serial->ops    = &lt776_uart_ops;
    serial->config = config;

    /* register uart device */
    os_hw_serial_register(serial, dev->name, uart);

    return OS_EOK;
}

OS_DRIVER_INFO lt776_uart_driver = {
    .name  = "Uart_Type",
    .probe = lt776_uart_probe,
};

OS_DRIVER_DEFINE(lt776_uart_driver, PREV, OS_INIT_SUBLEVEL_HIGH);
