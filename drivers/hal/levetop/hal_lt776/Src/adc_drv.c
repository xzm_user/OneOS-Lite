/**
    **********************************************************************************
             Copyright(c) 2021 Levetop Semiconductor Co. Ltd.
                      All Rights Reserved
  **********************************************************************************
  * @file    adc_drv.c
  * @author  Product application department
  * @version V1.0
  * @date    2021.11.01
  * @brief   ADC模块DRV层驱动.
  * @note 这个文件为应用层提供了操作ADC的接口:
  *
  @verbatim
  ===================================================================================
                                  ##### 更新 #####
  ===================================================================================
  [..] 更新时间:
  [..] 更新人:
  [..] 更新内容:
        1. nothing.
  @endverbatim
  */

#include "adc_drv.h"

/*** 常量定义 *******************************************************************/
/* global variables */

/*** 全局变量定义 ***************************************************************/
/*** volatile **********/

/*** 函数定义 *******************************************************************/
/**
 * @brief ADC模块功能开启
 *
 * @param[in] @ref NONE
 * @return @ref NONE
 */
void DRV_ADC_ModuleEn(void)
{
    _adc_set_enable_cmd;
    while (!_adc_chk_ready_flag)
        ;
}

/**
 * @brief ADC模块功能关闭
 *
 * @param[in] @ref NONE
 * @return @ref NONE
 */
void DRV_ADC_ModuleDis(void)
{
    _adc_set_disable_cmd;
    while (_adc_chk_cr_disable != 0)
        ;
}

/**
 * @brief ADC模块转换功能开启
 *
 * @param[in] @ref NONE
 * @return @ref NONE
 */
void DRV_ADC_ConvStart(void)
{
    _adc_set_start_conversion_cmd;
}

/**
 * @brief ADC模块转换功能关闭
 *
 * @param[in] @ref NONE
 * @return @ref NONE
 */
void DRV_ADC_ConvStop(void)
{
    volatile uint32_t tmp;
    _adc_set_stop_conversion_cmd;
    do
    {
        tmp = _adc_chk_cr_disable;
    } while (tmp & ADC_STOP_CONVERSION_CMD_EN);
}
/**
 * @brief ADC模块转换功能关闭
 *
 * @param[in] @ref channel通道编号，取值如下宏定义
 *- ADC_CHANNEL_0
 *- ADC_CHANNEL_2
 *- ADC_VOUT1
 *- ADC_VOUT3
 *- ADC_CHANNEL_IN_0
 *- ADC_CHANNEL_IN_2
 *- ADC_CHANNEL_IN_4
 *- ADC_CHANNEL_IN_6
 *- ADC_CHANNEL_1
 *- ADC_VREF1V
 *- ADC_VOUT2
 *- ADC_DAC_OUT
 *- ADC_CHANNEL_IN_1
 *- ADC_CHANNEL_IN_3
 *- ADC_CHANNEL_IN_5
 *- ADC_CHANNEL_IN_7
 * @return @ref NONE
 */
void DRV_ADC_ChannelSel(uint8_t channel)
{
    _adc_set_channel_sel_value(channel);
}

/**
 * @brief ADC模块中断处理函数
 *
 * @param[in] @ref NONE
 * @return @ref NONE
 */
void DRV_ADC_IRQHandler(void)
{
    _adc_clr_all_int_flag;
}

/************************ (C) COPYRIGHT LEVETOP *****END OF FILE**********************/
