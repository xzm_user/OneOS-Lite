/**
    **********************************************************************************
             Copyright(c) 2021 Levetop Semiconductor Co. Ltd.
                      All Rights Reserved
  **********************************************************************************
  * @file    usb_drv.c
  * @author  Product application department
  * @version V1.0
  * @date    2021.11.01
  * @brief   USB驱动.
  *
  @verbatim
  ===================================================================================
                                  ##### 更新 #####
  ===================================================================================
  [..] 更新时间:
  [..] 更新人:
  [..] 更新内容:
        1. nothing.
  @endverbatim
 */
#include "string.h"
#include "cpm_drv.h"
#include "usb_drv.h"
//#include "common.h"
#include "sys.h"
#include "iomacros.h"

__IO uint16_t gUSBC_PacketSize;
__IO uint32_t gUSBC_NewAddress;
__IO uint8_t gUSBC_RxINT_Flag;
__IO uint8_t gUSBC_TxINT_Flag;

// 0 is Ep0 发送第一包数据
// 1 is EP0 发送从第二包开始的数据
static __IO uint8_t g_Ep0DataStage;
static __IO uint8_t g_Version;         // USB版本号
static __IO uint8_t gUSBC_SuspendMode; //支持挂起标志位
static __IO uint8_t gUSBC_DMAINTR;     // DMA传输中断标志

static const uint32_t USB_FIFO_TAB[8] = {
    USB_FIFO_EP0_ADDR,
    USB_FIFO_EP1_ADDR,
    USB_FIFO_EP2_ADDR,
    USB_FIFO_EP3_ADDR,
    USB_FIFO_EP4_ADDR,
    USB_FIFO_EP5_ADDR,
    USB_FIFO_EP6_ADDR,
    USB_FIFO_EP7_ADDR,
};

static void USBC_BusReset(void);

/*************************************************
Function: USBC_VarInit
Description: USB变量初始化
Output: -None
Return: -None
*************************************************/
void __weak USBC_VarInit(void)
{
}

/*************************************************
Function: USBC_Init
Description: USB初始化
Input: -husb :USB初始化句柄
Output: -None
Return: -None
*************************************************/
void USBC_Init(USBC_HandleTypeDef *husb)
{
    g_Ep0DataStage = husb->Ep0DataStage;

    g_Version = husb->version;

    gUSBC_PacketSize = USB_MAX_PACKET_SIZE_V11;

    USBC_VarInit();
    gUSBC_SuspendMode = 0;

    gUSBC_DMAINTR = 0;

    DRV_CPM_UsbPhyInit(husb->osc);
    /* Setup USB register */
    // enable usb common interrupt
    // 0		1		2		3		4		5		6		7 (bit)
    // Susp	Resume	Reset	SOF		Conn	Discon	SessReq	VBusErr
    gUSBC_CommonReg->INTRUSBE = USB_INTERRUPT_RESET |
                                USB_INTERRUPT_CONNECT |
                                USB_INTERRUPT_DISCON |
                                USB_INTERRUPT_SUSPEND |
                                USB_INTERRUPT_RESUME;

    // enable ep0 and ep1 tx interrupts,clear other tx interrupts
    gUSBC_CommonReg->INTRTXE_L = CONTROL_EP |
                                 (1 << BULKIN_EP) |
                                 (1 << BULKIN_INT);

    gUSBC_CommonReg->INTRRXE_L = (1 << BULKOUT_EP);

    // ensure ep0 control/status regesters appeare in the memory map.
    gUSBC_CommonReg->EINDEX = CONTROL_EP;

    USBC_SetFIFOAddr(USB_FIFO_TAB[0], 0);

    // Enable Soft connection
    if (g_Version == 1)
        gUSBC_CommonReg->UCSR = USB_POWER_SOFT_CONN | USB_POWER_HS_ENAB;
    else
        gUSBC_CommonReg->UCSR = USB_POWER_SOFT_CONN;
    // gUSBC_CommonReg->UCSR  = USB_POWER_SOFT_CONN|USB_POWER_ENAB_SUSP;
    // USBC_ClearRx();

    NVIC_Init(3, 3, USBC_IRQn, 2);
}

/*************************************************
Function: USBC_SetFIFOAddr
Description: USB设置FIFO的地址
Input: -FIFOAddr     ：FIFO的地址
       -mode         : 模式
Output: -None
Return: -None
*************************************************/
void USBC_SetFIFOAddr(uint32_t FIFOAddr, uint8_t mode)
{
    if (mode == 0)
    {
        // reset to default fifo address
        gUSBC_ControlReg->TX_fifoadd_L = ((FIFOAddr >> 3) & 0x00FF);
        gUSBC_ControlReg->TX_fifoadd_H = ((FIFOAddr >> 11) & 0x00FF);
        gUSBC_ControlReg->RX_fifoadd_L = ((FIFOAddr >> 3) & 0x00FF);
        gUSBC_ControlReg->RX_fifoadd_H = ((FIFOAddr >> 11) & 0x00FF);
    }
    else if (mode == 1)
    {
        // Set Tx fifo Address
        gUSBC_ControlReg->TX_fifoadd_L = ((FIFOAddr >> 3) & 0x00FF);
        gUSBC_ControlReg->TX_fifoadd_H = ((FIFOAddr >> 11) & 0x00FF);
    }
    else if (mode == 2)
    {
        // Set Rx fifo Address
        gUSBC_ControlReg->RX_fifoadd_L = ((FIFOAddr >> 3) & 0x00FF);
        gUSBC_ControlReg->RX_fifoadd_H = ((FIFOAddr >> 11) & 0x00FF);
    }
}

/*************************************************
Function: USBC_ClearRx
Description: USB清除接收状态
Input: -None
Output: -None
Return: -None
*************************************************/
void USBC_ClearRx(void)
{
    uint8_t ucReg = 0;

    ucReg = gUSBC_IndexReg->RXCSR_L;

    ucReg &= ~DEV_RXCSR_RXPKTRDY; // Clear RxPktRdy

    gUSBC_IndexReg->RXCSR_L = ucReg;
}

/*************************************************
Function: USBC_SetTx
Description: USB设置发送状态
Input: -None
Output: -None
Return: -None
*************************************************/
void USBC_SetTx(void)
{
    uint8_t ucReg = 0;

    ucReg = gUSBC_IndexReg->TXCSR_L;

    ucReg |= DEV_TXCSR_TXPKTRDY; // Set TxPktRdy

    gUSBC_IndexReg->TXCSR_L = ucReg;
}

/*************************************************
Function: USBC_ReadEPxData
Description: 从ENDPOINTx的FIFO寄存器中读取数据
Input : - usbcEPx :端口号
            - src     :接收数据buff
            - Length  :接收的数据长度
Output: 无
Return: 无
Others: 无
*************************************************/
void USBC_ReadEPxData(uint8_t usbcEPx, uint8_t *src, uint16_t Length)
{
    uint16_t i = 0;

    for (i = 0; i < Length; i++)
    {
        src[i] = IO_READ8((uint32_t)&gUSBC_FIFOReg->FIFO_ENDPOINTx[usbcEPx]);
    }
}

/*************************************************
Function: USBC_WriteEPxData
Description: 数据写到ENDPOINTx的FIFO寄存器中
Input : - usbcEPx :端口号
            - dest    :要发送的数据buff
            - Length  :发送的数据长度
Output: 无
Return: 无
Others: 无
*************************************************/
void USBC_WriteEPxData(uint8_t usbcEPx, uint8_t *dest, uint16_t Length)
{
    uint16_t i;
    for (i = 0; i < Length; i++)
    {
        IO_WRITE8((uint32_t)&gUSBC_FIFOReg->FIFO_ENDPOINTx[usbcEPx], dest[i]);
    }
}

/*************************************************
Function: USBC_ReadEPxDataByDMA
Description: 通过DMA从ENDPOINTx的FIFO寄存器中读取数据
Input : - usbcEPx :端口号
            - src     :接收数据buff
            - Length  :接收的数据长度
Output: 无
Return: 无
Others: 无
*************************************************/
void USBC_ReadEPxDataByDMA(uint8_t usbcEPx, uint8_t *src, uint16_t Length)
{
    gUSBC_DMAReg->USB_DMAReg[usbcEPx - 1].DMA_ADDR = (uint32_t)src;
    gUSBC_DMAReg->USB_DMAReg[usbcEPx - 1].DMA_COUNT = Length;
    gUSBC_DMAReg->USB_DMAReg[usbcEPx - 1].DMA_CNTL = DEV_CNTL_DMAEN |
                                                     DEV_CNTL_INTERE |
                                                     DEV_CNTL_EP(usbcEPx) |
                                                     DEV_CNTL_BURSTMODE(0);

    while (!(gUSBC_DMAINTR & DEV_INTR_CHANNEL(usbcEPx)))
        ;

    gUSBC_DMAINTR = 0;
}

/*************************************************
Function: USBC_WriteEPxDataByDMA
Description: 通过DMA将数据写到ENDPOINTx的FIFO寄存器中
Input : - usbcEPx :端口号
            - dest    :要发送的数据buff
            - Length  :发送的数据长度
Output: 无
Return: 无
Others: 无
*************************************************/
void USBC_WriteEPxDataByDMA(uint8_t usbcEPx, uint8_t *dest, uint16_t Length)
{
    gUSBC_DMAReg->USB_DMAReg[usbcEPx - 1].DMA_ADDR = (uint32_t)dest;

    gUSBC_DMAReg->USB_DMAReg[usbcEPx - 1].DMA_COUNT = Length;

    gUSBC_DMAReg->USB_DMAReg[usbcEPx - 1].DMA_CNTL = DEV_CNTL_DMAEN |
                                                     DEV_CNTL_DIRECTION_READ |
                                                     DEV_CNTL_INTERE |
                                                     DEV_CNTL_EP(usbcEPx) |
                                                     DEV_CNTL_BURSTMODE(0);
    while (!(gUSBC_DMAINTR & DEV_INTR_CHANNEL(usbcEPx)))
        ;
    gUSBC_DMAINTR = 0;
}

/*************************************************
Function: USBC_EP0SendStall
Description: 设置EPORT0位空闲态。
Input :无
Output: 无
Return: 无
Others: 无
*************************************************/
void USBC_EP0SendStall(void)
{
    uint8_t ucReg = gUSBC_IndexReg->E0CSR_L;

    ucReg |= DEV_CSR0_SENDSTALL;

    gUSBC_IndexReg->E0CSR_L = ucReg;
}

/*************************************************
Function: USBC_EPxSendStall
Description: 设置EPORTx位空闲态。
Input :无
Output: 无
Return: 无
Others: 无
*************************************************/
void USBC_EPxSendStall(uint8_t EPx)
{
    uint8_t ucReg;

    gUSBC_CommonReg->EINDEX = EPx;
    if (EPx == BULKIN_EP)
    {

        ucReg = gUSBC_IndexReg->TXCSR_L;

        ucReg |= DEV_TXCSR_SEND_STALL;

        gUSBC_IndexReg->TXCSR_L = ucReg;
    }
    else
    {
        ucReg = gUSBC_IndexReg->RXCSR_L;

        ucReg |= DEV_RXCSR_SEND_STALL;

        gUSBC_IndexReg->RXCSR_L = ucReg;
    }
}

/*************************************************
Function: write_ep0_buf
Description:将数据写入到EPORT0的FIFO寄存器中，写完之后进入中断
Input: -src   :要发送的数据的起始地址
       -Length:要发送的数据长度
       -Status:包状态
Output: 无
Return: 无
Others: 无
*************************************************/
void USBC_WriteEP0Data(uint8_t *src, uint16_t Length, uint8_t Status)
{
    uint8_t ucReg = gUSBC_IndexReg->E0CSR_L;

    USBC_WriteEPxData(CONTROL_EP, src, Length);

    // set DataEnd =1
    if (Status == PACKET_END)
    {
        ucReg |= DEV_CSR0_DATAEND;
    }
    // set TxPktRdy =1
    ucReg |= DEV_CSR0_TXPKTRDY;

    gUSBC_IndexReg->E0CSR_L = ucReg;
}

/*************************************************
Function: USBC_ReceiveData
Description: USB接收数据
Input: -usbcEPx  :接收端口号
       -buf      :接收数据指针
Output: -None
Return: -uint16_t  :接收数据长度
*************************************************/
uint16_t USBC_ReceiveData(uint8_t usbcEPx, uint8_t *buf)
{
    uint16_t uiRxCount = 0;
    uint8_t ucRegLow;
    uint8_t EPx;

    if ((usbcEPx == CONTROL_EP) || (usbcEPx > INDEX_EP7))
        return 0;
    //备份端点
    EPx = gUSBC_CommonReg->EINDEX;

    // access DATA_OUT_EP register map
    gUSBC_CommonReg->EINDEX = usbcEPx;

    ucRegLow = gUSBC_IndexReg->RXCSR_L;

    // Clear sentstall and restart data toggle.
    if (ucRegLow & DEV_RXCSR_SENT_STALL)
    {
        // clear SendStall bit
        ucRegLow &= ~DEV_RXCSR_SEND_STALL;

        ucRegLow |= DEV_RXCSR_CLR_DATA_TOG;
        // set ClrDataTog
        gUSBC_IndexReg->RXCSR_L = ucRegLow;
    }

    // Start receive data packet
    if (ucRegLow & DEV_RXCSR_RXPKTRDY)
    {
        uiRxCount = gUSBC_IndexReg->RXCOUNTR_H;
        uiRxCount <<= 8;
        uiRxCount += gUSBC_IndexReg->RXCOUNTR_L;
#ifdef USB_DMA
        if (((((unsigned int)buf) & 0xf0000000) == 0x20000000) || ((((unsigned int)buf) & 0xf0000000) == 0x80000000))
        {
            USBC_ReadEPxDataByDMA(usbcEPx, buf, uiRxCount);
        }
        else
        {
            USBC_ReadEPxData(usbcEPx, buf, uiRxCount);
        }
#else
        USBC_ReadEPxData(usbcEPx, buf, uiRxCount);
#endif
        USBC_ClearRx();
    }

    //还原端点
    gUSBC_CommonReg->EINDEX = EPx;

    return uiRxCount;
}

/*************************************************
Function: USBC_EP0SendData
Description: USB EP0发送数据
Input: -usbcEPx  :发送端口号
       -buf      :发送数据指针
Output: -None
Return: -uint16_t  :发送数据长度
*************************************************/
uint16_t USBC_EP0SendData(uint16_t requestLen, uint8_t *databuf, uint16_t dataLen)
{
    uint16_t SendLen = 0;

    requestLen = (requestLen > dataLen) ? dataLen : requestLen; // host可能会发送长度字节为FF的请求

    //关闭EP0中断
    gUSBC_CommonReg->INTRTXE_L &= ~USB_INTERRUPT_EP0;

    if (dataLen > USB_MAX_PACKET_SIZE_EP0)
    {
        if (requestLen > USB_MAX_PACKET_SIZE_EP0)
        {
            while ((requestLen - SendLen) > USB_MAX_PACKET_SIZE_EP0)
            {
                USBC_WriteEP0Data(databuf + SendLen, USB_MAX_PACKET_SIZE_EP0, PACKET_MID);
                while ((gUSBC_CommonReg->INTRTX_L & USB_INTERRUPT_EP0) == 0x00)
                    ;
                SendLen += USB_MAX_PACKET_SIZE_EP0;
            }
        }
    }
    USBC_WriteEP0Data(databuf + SendLen, requestLen - SendLen, PACKET_END);

    //打开EP0中断
    gUSBC_CommonReg->INTRTXE_L |= USB_INTERRUPT_EP0;

    return requestLen;
}

/*************************************************
Function: USBC_SetTxSize
Description: 设置FIFO中待发送的数据长度
Input:-Size ：待发送数据的长度
        NOTE: SIZE = 0表示设置USB协议中发送数据长度0~0xFFFF
Output: 无
Return: 无
Others: 无
*************************************************/
void USBC_SetTxSize(uint16_t Size)
{
    if (Size > gUSBC_PacketSize)
    {
        gUSBC_IndexReg->TXMAXP_L = gUSBC_PacketSize;
        gUSBC_IndexReg->TXMAXP_H = gUSBC_PacketSize >> 8;
    }
    else
    {
        gUSBC_IndexReg->TXMAXP_L = Size;
        gUSBC_IndexReg->TXMAXP_H = Size >> 8;
    }
}

/*******************************************************************************
 * Function Name  : USBC_SendData
 * Description    : USB发送函数
 * Input          : - usbcEPx: usb端口号，取值：INDEX_EPx; x为1~7
 *                  - buf：要发送数据的缓冲；
 *                  - len：要发送数据的长度；
 *
 * Output         : None
 * Return         : 0：发送成功    other：发送失败
 ******************************************************************************/
uint8_t USBC_SendData(uint8_t usbcEPx, uint8_t *buf, uint16_t len)
{
    uint8_t EPx;
    if ((usbcEPx == CONTROL_EP) || (usbcEPx > INDEX_EP7))
        return 1;

    //备份端点
    EPx = gUSBC_CommonReg->EINDEX;

    //检测发送端口的发送情况
    gUSBC_CommonReg->EINDEX = BULKIN_EP;

    if (gUSBC_IndexReg->RXCSR_L & (DEV_RXCSR_RXPKTRDY | DEV_RXCSR_FIFOFULL))
        return 1;
    //检查发送缓冲是否有数据，等待为空；
    while ((gUSBC_IndexReg->TXCSR_L & DEV_TXCSR_TXPKTRDY) == DEV_TXCSR_TXPKTRDY)
        ;

    gUSBC_CommonReg->EINDEX = usbcEPx;

    USBC_SetTxSize(len);
    // write data into TX FIFO and wait for send
#ifdef USB_DMA
    if (((((unsigned int)buf) & 0xf0000000) == 0x20000000) || ((((unsigned int)buf) & 0xf0000000) == 0x80000000))
    {
        USBC_WriteEPxDataByDMA(usbcEPx, buf, len);
    }
    else
    {
        USBC_WriteEPxData(usbcEPx, buf, len);
    }
#else
    USBC_WriteEPxData(usbcEPx, buf, len);
#endif
    // set TxPktRdy=1
    USBC_SetTx();

    //还原端点
    gUSBC_CommonReg->EINDEX = EPx;
    return 0;
}

/*************************************************
Function: USBDev_Suspend
Description: USB暂停
Input:无
Output: 无
Return: 无
Others: 无
*************************************************/
static void USBC_Suspend(void)
{
    uint8_t ucMode;

    if (gUSBC_SuspendMode)
    {
        ucMode = gUSBC_CommonReg->UCSR;
        ucMode |= USB_POWER_ENAB_SUSP;
        gUSBC_CommonReg->UCSR = ucMode;

        // Enable USB FILE Clock
        ucMode = gUSBC_CommonReg->UCSR;
        ucMode &= ~USB_POWER_ENAB_SUSP;
        gUSBC_CommonReg->UCSR = ucMode;
    }
}

/*************************************************
Function: USBC_BusReset
Description: USB复位
Input:无
Output: 无
Return: 无
Others: 无
*************************************************/
static void USBC_BusReset(void)
{
    __IO uint8_t EPx;

    /*初始化协议变量*/
    USBC_VarInit();

    /*Interrupt Var*/
    gUSBC_RxINT_Flag = 0;
    gUSBC_TxINT_Flag = 0;
    gUSBC_DMAINTR = 0;

    gUSBC_CommonReg->FADDRR = 0;

    for (EPx = 1; EPx < 8; EPx++)
    {
        // access DATA_OUT_EP register map
        gUSBC_CommonReg->EINDEX = EPx;

        // set FIFO size
        gUSBC_ControlReg->TXFIFOSZ = 0x03;
        gUSBC_ControlReg->RXFIFOSZ = 0x03;

        // set fifo offset address
        USBC_SetFIFOAddr(USB_FIFO_TAB[EPx], 0);

        gUSBC_IndexReg->TXCSR_L = DEV_TXCSR_CLR_DATA_TOG;
        gUSBC_IndexReg->TXCSR_H = 0;

        gUSBC_IndexReg->TXMAXP_L = gUSBC_PacketSize;
        gUSBC_IndexReg->TXMAXP_H = gUSBC_PacketSize >> 8;

        gUSBC_IndexReg->RXCSR_L = DEV_RXCSR_CLR_DATA_TOG;
        gUSBC_IndexReg->RXCSR_H = 0x0;

        gUSBC_IndexReg->RXMAXP_L = gUSBC_PacketSize;
        gUSBC_IndexReg->RXMAXP_H = gUSBC_PacketSize >> 8;
        //=================================

        // Flush Tx Ep FIFO
        gUSBC_IndexReg->TXCSR_L = DEV_TXCSR_FLUSH_FIFO;

        // Flush Rx Ep FIFO
        gUSBC_IndexReg->RXCSR_L = DEV_RXCSR_FLUSH_FIFO;
    }
}

/*************************************************
Function: USBC_Ep0Handler
Description: USB EP0中断处理
Input:无
Output: 无
Return: 无
Others: 无
*************************************************/
void __weak USBC_Ep0Handler(void)
{
}

/*************************************************
Function: USBC_Ep0Process
Description: USB EP0中断处理
Input:无
Output: 无
Return: 无
Others: 无
*************************************************/
void __weak USBC_Ep0Process(void)
{
    uint8_t ucReg = 0;

    // enable ep0 register map to be accessed
    gUSBC_CommonReg->EINDEX = CONTROL_EP;

    ucReg = gUSBC_IndexReg->E0CSR_L;

    // clear SentStall bit
    if (ucReg & DEV_CSR0_SENTSTALL)
    {
        ucReg &= ~(DEV_CSR0_SENTSTALL);
    }
    // clear SetupEnd bit
    if (ucReg & DEV_CSR0_SETUPEND)
    {
        ucReg |= DEV_CSR0_SERVICE_SETUPEND;
    }
    gUSBC_IndexReg->E0CSR_L = ucReg;

    // if RxPktRdy=1,Data come into ep0 buf
    if ((ucReg & DEV_CSR0_RXPKTRDY) || (g_Ep0DataStage))
    {
        USBC_Ep0Handler();
    }
}

/*************************************************
Function: USB_IRQHandler
Description: USB 中断处理
Input:无
Output: 无
Return: 无
Others: 无
*************************************************/
void USB_IRQHandler(void)
{
    uint8_t ComINTFlag, TxINTFlag, RxINTFlag;
    uint8_t uUSBC_INDEX_EPx;

    uUSBC_INDEX_EPx = gUSBC_CommonReg->EINDEX;

    /*获取中断状态*/
    ComINTFlag = gUSBC_CommonReg->INTRUSB;

    /*获取Tx状态*/
    TxINTFlag = gUSBC_CommonReg->INTRTX_L;

    /*获取Rx状态*/
    RxINTFlag = gUSBC_CommonReg->INTRRX_L;

    /*获取DMA中断的逻辑通道号*/
    gUSBC_DMAINTR = gUSBC_DMAReg->DMA_INTR;

    /*处理相关状态*/
    /*处理命令中断事物*/
    if (ComINTFlag & USB_INTERRUPT_RESET) //复位请求中断
    {
        USBC_BusReset();
    }

    if (ComINTFlag & USB_INTERRUPT_SUSPEND) //挂起中断事物
    {
        USBC_Suspend();
    }

    /*处理Tx中断事务*/
    if (TxINTFlag & USB_INTERRUPT_EP0)
    {
        if (gUSBC_NewAddress)
        {
            gUSBC_CommonReg->FADDRR = gUSBC_NewAddress;
            gUSBC_NewAddress = 0;
        }
        USBC_Ep0Process();
    }
    /*接收数据中断处理*/
    if (RxINTFlag)
    {
        gUSBC_RxINT_Flag = RxINTFlag;
    }
    /*发送数据中断处理*/
    if (TxINTFlag & (~USB_INTERRUPT_EP0))
    {
        gUSBC_TxINT_Flag = TxINTFlag;
    }

    gUSBC_CommonReg->EINDEX = uUSBC_INDEX_EPx;
}
