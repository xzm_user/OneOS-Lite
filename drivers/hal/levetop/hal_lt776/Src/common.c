#include "common.h"
#include "type.h"

#if !(defined(__ARMCC_VERSION) && (__ARMCC_VERSION >= 6010050) || defined(__GNUC__))
uint32_t enter_critical_sr_save()
{
    register uint32_t __regPriMask __asm("primask");
    return (__regPriMask);
}

void exit_critical_sr_restore(uint32_t primask)
{
    register uint32_t __regPriMask __asm("primask");
    __regPriMask = (primask);
}

uint32_t Read_VEC()
{
    uint32_t vec_num;
    register uint32_t _reg_IPSR __asm("ipsr");

    vec_num = (_reg_IPSR & 0x1FF) - 16;

    return vec_num;
}
#endif
