/**
  **********************************************************************************
             Copyright(c) 2021 Levetop Semiconductor Co. Ltd.
                      All Rights Reserved
  **********************************************************************************
  * @file    dac_drv.c
  * @author  Product application department
  * @version V1.0
  * @date    2020.02.20
  * @brief   DAC模块DRIVER层驱动.
  * @note 这个文件为应用层提供了操作DAC的接口:
  *
  @verbatim
  ===================================================================================
                                  ##### 更新 #####
  ===================================================================================
  [..] 更新时间:
  [..] 更新人:
  [..] 更新内容:
        1. nothing.
  @endverbatim
  */

#include "dac_drv.h"

/*** 全局变量定义 ***************************************************************/

/*** volatile **********/

/*** @ static **********/

/*** 常量定义 *******************************************************************/

/*** 函数定义 *******************************************************************/

/**
 *@brief DRV_DAC_Init:DAC的驱动层初始化接口
 *
 *@param[in] DAC_InitTypeDef DAC初始化结构体相关
 *
 *@return NONE
 */
void DRV_DAC_Init(DAC_InitTypeDef Init)
{
    if (Init.DataFomat > LEFTALIGNED_12BITS)
    {
        _dac_set_right_align();
        if (Init.DataFomat == RIGHTALIGNED_8BITS)
        {
            _dac_set_eight_res();
        }
        else
        {
            _dac_set_twelve_res();
        }
    }
    else
    {
        _dac_set_right_align();
        if (Init.DataFomat == LEFTALIGNED_8BITS)
        {
            _dac_set_eight_res();
        }
        else
        {
            _dac_set_twelve_res();
        }
    }

    _dac_clr_trigger_mode();
    _dac_set_trigger_mode(Init.TriggerMode, Init.ExtTriggerMode);

    if (Init.boDmaEnable)
    {
        _dac_en_dma();
    }

    _dac_diable_ext_verf();

    _dac_en();
}

/**
 *@brief DRV_DAC_Deinit:DAC的驱动层反初始化接口
 *
 *@param[in] NONE
 *
 *@return NONE
 */
void DRV_DAC_Deinit(void)
{
    _dac_clr_trigger_mode();
    _dac_dis();
}

/**
 *@brief DRV_DAC_SendData
           DAC的驱动层数据发送接口
*
*@param[in] data,待发送数据
*
*@return NONE
*/
void DRV_DAC_SendData(uint8_t data)
{
    _dac_set_data(data);
}

/**
 *@brief DRV_DAC_SW_Trig:DAC软中断触发
 *
 *
 *@return NONE
 */
void DRV_DAC_SW_Trig(void)
{
    _dac_set_sw_trig();
    while (0 == _dac_get_fsr_flag())
        ;
}

/**
 *@brief DRV_DAC_GetData:
 *
 *@param[out] data,获取当前DAC中的值
 *
 *@return NONE
 */
void DRV_DAC_GetData(uint16_t *data)
{
    *data = _dac_get_data();
}
