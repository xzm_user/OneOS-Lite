/**
    **********************************************************************************
             Copyright(c) 2020 Levetop Semiconductor Co., Ltd.
                      All Rights Reserved
  **********************************************************************************
  * @file    rtc_drv.c
  * @author  Product application department
  * @version V1.0
  * @date    2020.02.25
  * @brief   rtc驱动.
  * @note 这个文件提供了drv层rtc接口:
  *
  @verbatim
  ===================================================================================
                                  ##### 更新 #####
  ===================================================================================
  [..] 更新时间:
  [..] 更新人:
  [..] 更新内容:
        1. nothing.
  @endverbatim
 */

#include "rtc_drv.h"

/*** 全局变量定义 ***************************************************************/
/*** volatile **********/

/*** @ static **********/

/*** 常量定义 *******************************************************************/

/*** 函数定义 *******************************************************************/

/**
 * @brief RTC 中断服务函数
 *
 * @return NONE
 */
void DRV_RTC_IRQHandler(void)
{
    uint32_t prcsr;
    uint8_t  alarm_wakeup_flg = 1;

    prcsr = _rtc_prcsr_get_val;

    // //printf("\r\nRTC INT[0x%08x]\r\n", prcsr);
    if (((prcsr & RTC_PRCSR_ALARM_INTF) == RTC_PRCSR_ALARM_INTF) &&
        ((prcsr & RTC_PRCSR_ALARM_IEN) == RTC_PRCSR_ALARM_IEN))    // alarm interrupt
    {
        _rtc_prcsr_alarm_pulse_clr_flag;
        //		g_rtc_int_sta |= Ala_intf;
        alarm_wakeup_flg = 0;
        // printf("RTC Alarm\r\n");
    }

    if (((prcsr & RTC_PRCSR_DAY_INTF) == RTC_PRCSR_DAY_INTF) &&
        ((prcsr & RTC_PRCSR_DAY_IEN) == RTC_PRCSR_DAY_IEN))    // day interrupt
    {
        _rtc_prcsr_day_pulse_clr_flag;
        //		g_rtc_int_sta |= Day_intf;
        alarm_wakeup_flg = 0;
        // printf("RTC Day Alarm\r\n");
    }

    if (((prcsr & RTC_PRCSR_HOUR_INTF) == RTC_PRCSR_HOUR_INTF) &&
        ((prcsr & RTC_PRCSR_HOUR_IEN) == RTC_PRCSR_HOUR_IEN))    // hour interrupt
    {
        _rtc_prcsr_hour_pulse_clr_flag;
        //		g_rtc_int_sta |= Hou_intf;
        alarm_wakeup_flg = 0;

        // printf("RTC Hour Alarm\r\n");
    }

    if (((prcsr & RTC_PRCSR_MINUTE_INTF) == RTC_PRCSR_MINUTE_INTF) &&
        ((prcsr & RTC_PRCSR_MINUTE_IEN) == RTC_PRCSR_MINUTE_IEN))    // minute interrupt
    {
        _rtc_prcsr_min_pulse_clr_flag;
        //		g_rtc_int_sta |= Min_intf;
        alarm_wakeup_flg = 0;
        // printf("RTC Minute Alarm\r\n");
    }

    if (((prcsr & RTC_PRCSR_SECOND_INTF) == RTC_PRCSR_SECOND_INTF) &&
        ((prcsr & RTC_PRCSR_SECOND_IEN) == RTC_PRCSR_SECOND_IEN))    // second interrupt
    {
        // tm tim;

        _rtc_prcsr_sec_pulse_clr_flag;
        //		g_rtc_int_sta |= Sec_intf;
        // //printf("RTC Second\r\n");
        // RTC_GetTime(&tim);
        // //printf("RTC Second INT[%d]\r\n", tim.second);
        alarm_wakeup_flg = 0;

        // printf("RTC Second Alarm\r\n");
    }

    if (alarm_wakeup_flg)
    {
        // printf("\r\nRTC alarm wakeup from sleep mode\r\n");
        _rtc_prkeyr_key_set;    //输入key
        _rtc_prcsr_alarm_pulse_ie_dis;
        _rtc_prcsr_direct_control_dis;    // set dir = 0;
    }
}

/**
 * @brief RTC 设置时间计数
 *
 * @param[in] Time 时间结构体
 * @return NONE
 */
void DRV_RTC_SetTimeCounter(RTC_TimeTypeDef *Time)
{
    _rtc_prt1r_counter_days_set(Time->days);
    _rtc_prt2r_counter_set(Time->hours, Time->minutes, Time->seconds);
}

/**
 * @brief RTC 设置报警时间计数
 *
 * @param[in] Time 时间结构体
 * @return NONE
 */
void DRV_RTC_SetAlarmTimeCounter(RTC_TimeTypeDef *Time)
{
    _rtc_pra1r_alarm_day_counter_set(Time->days);
    _rtc_pra2r_alarm_hour_counter_set(Time->hours);
    _rtc_pra2r_alarm_min_counter_set(Time->minutes);
    _rtc_pra2r_alarm_sec_counter_set(Time->seconds);
}

/**
 * @brief RTC 获取时间计数
 *
 * @param[in] Time 时间结构体
 * @return NONE
 */
void DRV_RTC_GetTimeCounter(RTC_TimeTypeDef *Time)
{
    Time->days    = _rtc_prtcr_days_counter_get;
    Time->hours   = _rtc_prtcr_hours_counter_get;
    Time->minutes = _rtc_prtcr_mins_counter_get;
    Time->seconds = _rtc_prtcr_secs_counter_get;

    //    //printf("DRV time : D-%d [%d:%d:%d]\n", Time->days, Time->hours, Time->minutes, Time->seconds);
}
