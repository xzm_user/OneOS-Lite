/**********************************************************
 * @Copyright (c) 2021   Levetop Semiconductor Co., Ltd.
 * @File			wdt_drv.c
 * @Author		Jason Zhao
 * @Date			2021/12/5
 * @Time			8:43:20
 * @Version
 * @Brief
 **********************************************************/
#include "wdt_drv.h"

/**
 *@brief WDT:初始化
 *
 *@return NONE
 */
void DRV_WDT_Init(uint16_t WMRCounterVal)
{
    WDT->WMR = WMRCounterVal;
    WDT->WCR = WDT_EN;
}

/**
 *@brief WDT:喂狗
 *
 *@return NONE
 */
void DRV_WDT_FeedDog(void)
{
    WDT->WSR = 0x5555;
    WDT->WSR = 0xAAAA;
}

/**
 *@brief WDT:启动
 *
 *@return NONE
 */
void DRV_WDT_Open(void)
{
    WDT->WCR |= WDT_EN;
}
/**
 *@brief WDT:禁用
 *
 *@return NONE
 */
void DRV_WDT_Close(void)
{
    WDT->WCR &= ~WDT_EN;
}

/**
 *@brief 获取Watch-dog counter值
 *
 * @retval WDT计数器
 */
uint32_t DRV_WDT_GetCount(void)
{
    return (WDT->WCNTR);
}
