/**********************************************************
* @Copyright (c) 2021   Levetop Semiconductor Co., Ltd.
* @File			sdram_drv.c
* @Author		Jason Zhao
* @Date			2021/12/1
* @Time			17:30:58
* @Version		
* @Brief			
**********************************************************/
#include "sdram_drv.h"

void InitSdram(void)
{
	SDRAM_TypeDef *psdram= (SDRAM_TypeDef *)(SDRAM_BASE_ADDR) ;

	psdram->EXN_MODE_REG = ((T_CL <<4) | SDR_BL);
	psdram->SCTLR |= 0x40000;
	psdram->SCONR &= 0x3fff0000;
	psdram->SCONR |= (((uint32_t)SDR_BURST<<30) | 
										(0<<13)|
										((COL_WIDTH-1)<<9) | 
										((ROW_WIDTH-1)<<5) | 
										(1<<3));
	
	psdram->STMG0R = (((T_RC-1)<<22) | 
										((T_XSR-1)<<18)|
										((T_RCAR-1)<<14)|
										((T_WR-1)<<12)|
										((T_RP-1)<<9)|
										((T_RCD-1)<<6)| 
										((T_RAS-1)<<2)|
										(0<<0));
						
	
	psdram->SCTLR &= 0xffffe37;
	psdram->SREFR =(g_sdram_clk/1000 * 16)/4096;
}


