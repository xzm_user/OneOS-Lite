/**********************************************************
 * @Copyright (c) 2021   Levetop Semiconductor Co., Ltd.
 * @File			dma.c
 * @Author		Jason Zhao
 * @Date			2021/12/8
 * @Time			11:33:16
 * @Version
 * @Brief
 **********************************************************/
#include "lt776_dma.h"
#include "os_clock.h"
/**
 * @brief  设置DMA传输参数
 *
 * @param [in] hedmac DMAC句柄指针
 * @param [in] SrcAddress DMA的源地址
 * @param [in] DstAddress DMA的目的地址
 * @param [in] DataLength 传输的数据长度
 * @retval 无
 */
static void DMAC_SetConfig(DMAC_HandleTypeDef *hdma, u32 SrcAddress, u32 DstAddress, u32 DataLength)
{
    if (hdma->init.transfer_lli == DMA_LLP_DIS)
    {
        /* Configure DMA Channel data length */
        _dmac_set_transfer_length(hdma->instance, hdma->init.channel_num, DataLength);

        /* Configure DMA Channel source address */
        _dmac_set_source_address(hdma->instance, hdma->init.channel_num, SrcAddress);

        /* Configure DMA Channel destination address */
        _dmac_set_destination_address(hdma->instance, hdma->init.channel_num, DstAddress);
    }
    else
    {
        DMAC_LLITypeDef pdma_lli;

        pdma_lli.dst_addr = DstAddress;
        pdma_lli.src_addr = SrcAddress;
        pdma_lli.len      = DataLength;

        DRV_DMAC_LliRegInit(hdma->instance, hdma->init.channel_num, &pdma_lli);
    }

    /*memory to peripheral, dmac controller*/
    if (hdma->init.transfer_type == DMAC_TRANSFERTYPE_M2P_CONTROLLER_DMA)
    {
        /*硬件握手*/
        _dmac_src_hs_soft(hdma->instance, hdma->init.channel_num);
        _dmac_dst_hs_hard(hdma->instance, hdma->init.channel_num);

        /*目标外围设备*/
        _dmac_set_dst_peripheral(hdma->instance, hdma->init.channel_num, hdma->init.peripheral_type);
    }
    /*peripheral to memory, dmac controlller*/
    else if (hdma->init.transfer_type == DMAC_TRANSFERTYPE_P2M_CONTROLLER_DMA)
    {
        /*硬件握手*/
        _dmac_src_hs_hard(hdma->instance, hdma->init.channel_num);
        _dmac_dst_hs_soft(hdma->instance, hdma->init.channel_num);

        /*目标外围设备*/
        _dmac_set_src_peripheral(hdma->instance, hdma->init.channel_num, hdma->init.peripheral_type);
    }
    /*memory to memory, dmac controller*/
    else if (hdma->init.transfer_type == DMAC_TRANSFERTYPE_M2M_CONTROLLER_DMA)
    {
        /*硬件握手*/
        _dmac_src_hs_soft(hdma->instance, hdma->init.channel_num);
        _dmac_dst_hs_soft(hdma->instance, hdma->init.channel_num);
    }
    else
    {
        ;
    }
}
/**
 * @brief 复位dmac.
 *
 * @param [in] hdmac DMAC句柄指针
 * @return
 * - OK;
 * - ERROR;
 */
StatusTypeDef DMAC_Deinit(DMAC_HandleTypeDef *hdma)
{
    /* Check the DMA handle allocation */
    if (hdma == NULL)
    {
        return ERROR;
    }

    /* Check the DMA peripheral state */
    if (hdma->state == DMA_STATE_BUSY)
    {
        return ERROR;
    }

    /* Disable the selected DMA Channelx */
    _dmac_ch_dis(hdma->instance, hdma->init.channel_num);

    /* Reset DMA Channel control register */
    _dmac_clr_dmac_ctrln(hdma->instance, hdma->init.channel_num);

    /* Clear all flags */
    /* Clear raw interrupt status flag */
    _dmac_clr_raw_interrupt_status(hdma->instance, hdma->init.channel_num);

    /* Clear block interrupt status flag */
    _dmac_clr_block_interrupt_status(hdma->instance, hdma->init.channel_num);

    /* Clear error interrupt status flag */

    /* Initialize the DMA state */
    hdma->state = DMA_STATE_RESET;

    /* Release lock */
    __UNLOCK(hdma);

    return OK;
}
/**
 * @brief dmac初始化.
 *
 * @param [in] hdmac DMAC句柄指针
 * @return
 * - OK;
 * - ERROR;
 */
StatusTypeDef DMAC_Init(DMAC_HandleTypeDef *hdma)
{
    /* Check the DMA handle allocation */
    if (hdma == NULL)
    {
        return ERROR;
    }

    if (hdma->init.channel_num > DMAC_CHANNEL_3)
        return ERROR;

    if (hdma->state == DMA_STATE_RESET)
    {
        /* Allocate lock resource and initialize it */
        hdma->lock = UNLOCKED;
    }

    /* Change DMA peripheral state */
    hdma->state = DMA_STATE_BUSY;

    /* Clear SRC_MSIZE, DEST_MSIZE, SINC, DINC, SRC_TR_WIDTH, DST_TR_WIDTH bits */
    _dmac_clr_dmac_ctrln(hdma->instance, hdma->init.channel_num);

    DRV_DMAC_CtrlRegConfig(hdma->instance,
                           hdma->init.channel_num,
                           hdma->init.transfer_type,
                           hdma->init.src_msize,
                           hdma->init.dst_msize,
                           hdma->init.src_inc,
                           hdma->init.dst_inc,
                           hdma->init.src_tr_width,
                           hdma->init.dst_tr_width);

    DMAC_SetConfig(hdma, hdma->init.src_addr, hdma->init.dst_addr, hdma->init.transfer_size);

    /* Initialize the DMA state*/
    hdma->state = DMA_STATE_READY;

    return OK;
}
/**
 * @brief 开启非中断模式DMA传输.
 *
 * @param [in] hedmac DMAC句柄指针
 * @return
 * - OK;
 * - ERROR;
 */

StatusTypeDef DMAC_StartPolling(DMAC_HandleTypeDef *hdma)
{
    /*Enable DMAC*/
    _dmac_en(hdma->instance);

    /* Disable the selected DMA Channelx */
    _dmac_ch_dis(hdma->instance, hdma->init.channel_num);

    /*采用查询的方式*/
    //	DRV_DMAC_EnTfrIt(hdma->instance,hdma->init.channel_num);
    _dmac_int_en_en(hdma->instance, hdma->init.channel_num);

    _dmac_raw_interrupt_en(hdma->instance, hdma->init.channel_num);

    /* Enable the selected DMA Channelx */
    _dmac_ch_en(hdma->instance, hdma->init.channel_num);

    return OK;
}

/*
 * @brief 轮询普通模式传输完成.
 *
 * @param [in] hdmac EDMAC句柄指针
 * @param [in] CompleteLevel DMA完成级别
 * @param [in] Timeout 超时时间
 * @return
 * - OK;
 * - ERROR;
 *-
 */
StatusTypeDef DMAC_PollForTransfer(DMAC_HandleTypeDef *hdma, u32 timeoutMs)
{
    u32              tickMs;
    u32              tickMsStart;
    u32              tickMsEnd;
    BitActionTypeDef raw_status;

    /* Transfer Complete flag */
    raw_status = DRV_DMAC_GetRawStatus(hdma->instance, hdma->init.channel_num);

    /* Get tick */
    tickMsStart = os_tick_get();
    tickMsEnd   = tickMsStart + timeoutMs;

    while (raw_status == BIT_RESET)
    {
        raw_status = DRV_DMAC_GetRawStatus(hdma->instance, hdma->init.channel_num);
        /* Check for the Timeout */
        if (timeoutMs)
        {
            tickMs = os_tick_get();
            if ((tickMsStart < tickMsEnd) && ((tickMs < tickMsStart) || (tickMs > tickMsEnd)))
            {
                /* Change the DMA state */
                hdma->state = DMA_STATE_TIMEOUT;
                /* Process Unlocked */
                __UNLOCK(hdma);

                return TIMEOUT;
            }
            else if ((tickMs > tickMsEnd) && (tickMs < tickMsStart))
            {
                /* Change the DMA state */
                hdma->state = DMA_STATE_TIMEOUT;
                /* Process Unlocked */
                __UNLOCK(hdma);

                return TIMEOUT;
            }
        }
    }

    /* Clear the transfer complete flag */
    DRV_DMAC_ClearRawStatus(hdma->instance, hdma->init.channel_num);

    /* The selected Channelx EN bit is cleared (DMA is disabled and
    all transfers are complete) */
    hdma->state = DMA_STATE_READY;

    /* Process unlocked */
    __UNLOCK(hdma);

    return OK;
}

void DMAC_M2MPollTransfer(DMAC_HandleTypeDef *hdma, u8 *dst, u8 *src)
{
    hdma->init.dst_addr = (u32)dst;
    hdma->init.src_addr = (u32)src;
    /*初始化DMAC*/
    DMAC_Init(hdma);
    /*开启非中断模式DMAC传输*/
    DMAC_StartPolling(hdma);
    DMAC_PollForTransfer(hdma, 100);
}
