/**********************************************************
 * @Copyright (c) 2021   Levetop Semiconductor Co., Ltd.
 * @File			dma.h
 * @Author		Jason Zhao
 * @Date			2021/12/8
 * @Time			11:33:0
 * @Version
 * @Brief
 **********************************************************
 * Modification History
 *
 *
 **********************************************************/
#ifndef __DMA_H__
#define __DMA_H__

#include "lt776.h"
#include "lt776_reg.h"
#include "dma_drv.h"
#include "sys.h"

#define DMAC_NUMBER_CHANNELS 4

#define DMA_SINC_INC       0 /**< DMAC Source Address Increment */
#define DMA_SINC_DEC       1 /**< DMAC Source Address Decrement */
#define DMA_SINC_NO_CHANGE 2 /**< DMAC Source Address No Change */

#define DMA_DINC_INC       0 /**< DMAC Destination Address Increment */
#define DMA_DINC_DEC       1 /**< DMAC Destination Address Decrement */
#define DMA_DINC_NO_CHANGE 2 /**< DMAC Destination Address No Change */
#define DMA_LLP_DIS        0 /**< 源端和目的端都不使用LLP */
#define DMA_LLP_SRC_EN     1 /**< 源端使用LLP */
#define DMA_LLP_DST_EN     2 /**< 目的端使用LLP */
#define DMA_LLP_SRC_DST_EN 3 /**< 源端和目的端都使用LLP */

/**
 * @brief DNAC 状态定义
 *
 */
typedef enum
{
    DMA_STATE_RESET   = 0x00, /**< DMA not yet initialized or disabled */
    DMA_STATE_READY   = 0x01, /**< DMA initialized and ready for use	 */
    DMA_STATE_BUSY    = 0x02, /**< DMA process is ongoing 						 */
    DMA_STATE_TIMEOUT = 0x03, /**< DMA timeout state									 */
    DMA_STATE_ERROR   = 0x04, /**< DMA error state										 */
} DMAC_StateTypeDef;

/**
 * @brief  HAL dmac channel index structure definition
 */
typedef enum
{

    DMAC_CHANNEL_0 = 0x00,
    DMAC_CHANNEL_1,
    DMAC_CHANNEL_2,
    DMAC_CHANNEL_3

} DMAC_ChannelIndexTypeDef;

/**
 * @brief  HAL dmac burst transfer length structure definition
 */
typedef enum
{
    DMAC_MSIZE_1 = 0x00,
    DMAC_MSIZE_4,
    DMAC_MSIZE_8,
    DMAC_MSIZE_16,
    DMAC_MSIZE_32,
    DMAC_MSIZE_64,
    DMAC_MSIZE_128,
    DMAC_MSIZE_256,

} DMAC_BurstTransLengthTypedef;

/**
 * @brief DNAC 通信宽度定义
 *
 */
typedef enum
{
    DMAC_TR_WIDTH_8BITS = 0x00,
    DMAC_TR_WIDTH_16BITS,
    DMAC_TR_WIDTH_32BITS,
    DMAC_TR_WIDTH_64BITS,
    DMAC_TR_WIDTH_128BITS,
    DMAC_TR_WIDTH_256BITS,
} DMAC_TransWidthTypeDef;

/**
 * @brief DMAC Transfer type of DMA controller
 */
typedef enum
{
    DMAC_TRANSFERTYPE_M2M_CONTROLLER_DMA = ((0UL)), /**< Memory to memory - DMA control */
    DMAC_TRANSFERTYPE_M2P_CONTROLLER_DMA = ((1UL)), /**< Memory to peripheral - DMA control */
    DMAC_TRANSFERTYPE_P2M_CONTROLLER_DMA = ((2UL)), /**< Peripheral to memory - DMA control */
    DMAC_TRANSFERTYPE_P2P_CONTROLLER_DMA = ((3UL)), /**< Source peripheral to destination peripheral - DMA control */
    DMAC_TRANSFERTYPE_P2M_CONTROLLER_PERIPHERAL =
        ((4UL)), /**< Source peripheral to destination peripheral - destination peripheral control */
    DMAC_TRANSFERTYPE_P2P_CONTROLLER_SRCPERIPHERAL = ((5UL)), /**< Memory to peripheral - peripheral control */
    DMAC_TRANSFERTYPE_M2P_CONTROLLER_PERIPHERAL    = ((6UL)), /**< Peripheral to memory - peripheral control */
    DMAC_TRANSFERTYPE_P2P_CONTROLLER_DSTPERIPHERAL =
        ((7UL)) /**< Source peripheral to destination peripheral - source peripheral control */
} DMAC_FlowControlTypeDef;

typedef enum
{
    DMAC_PERIPHERAL_SPI1_TX  = 0X00,
    DMAC_PERIPHERAL_SPI2_TX  = 0X01,
    DMAC_PERIPHERAL_SPI3_TX  = 0X02,
    DMAC_PERIPHERAL_SPI1_RX  = 0X03,
    DMAC_PERIPHERAL_SPI2_RX  = 0X04,
    DMAC_PERIPHERAL_SPI3_RX  = 0X05,
    DMAC_PER3PHERAL_QADC     = 0x06,
    DMAC_PER3PHERAL_MCC1     = 0x07,
    DMAC_PERIPHERAL_MCC2     = 0x08,
    DMAC_PERIPHERAL_MCC3     = 0x09,
    DMAC_PERIPHERAL_UART1_TX = 0x0A,
    DMAC_PERIPHERAL_UART1_RX = 0x0B,
    DMAC_PERIPHERAL_UART3_TX = 0x0C,
    DMAC_PERIPHERAL_UART3_RX = 0x0D,
} DMAC_PeripheralTypeDef;
typedef enum
{
    DMAC2_PERIPHERAL_UART2_RX = 0X00,
    DMAC2_PERIPHERAL_UART2_TX = 0X01,
    DMAC2_PERIPHERAL_SSI1_RX  = 0X02,
    DMAC2_PERIPHERAL_SSI1_TX  = 0X03,
    DMAC2_PERIPHERAL_SSI2_RX  = 0X04,
    DMAC2_PERIPHERAL_SSI2_TX  = 0X05,
    DMAC2_PERIPHERAL_I2S_RX   = 0X06,
    DMAC2_PERIPHERAL_I2S_TX   = 0X07,
    DMAC_PERIPHERAL_DAC       = 0X08,
    DMAC_PERIPHERAL_DCMI      = 0X09,
    DMAC2_PERIPHERAL_UART4_RX = 0X0A,
    DMAC2_PERIPHERAL_UART4_TX = 0X0B,
    DMAC_PERIPHERAL_PWMT      = 0X0C,
    DMAC_PERIPHERAL_DCMI_DVT  = 0X0D,
} DMAC2_PeripheralTypeDef;

/**
 * @brief  HAL Lock structures definition
 */
typedef enum
{
    UNLOCKED = 0x00,
    LOCKED   = 0x01
} LockTypeDef;

#if (USE_RTOS == 1)
#error " USE_RTOS should be 0 in the current HAL release "
#else
#define __LOCK(__HANDLE__)                                                                                             \
    do                                                                                                                 \
    {                                                                                                                  \
        if ((__HANDLE__)->lock == LOCKED)                                                                              \
        {                                                                                                              \
            return BUSY;                                                                                               \
        }                                                                                                              \
        else                                                                                                           \
        {                                                                                                              \
            (__HANDLE__)->lock = LOCKED;                                                                               \
        }                                                                                                              \
    } while (0)

#define __UNLOCK(__HANDLE__)                                                                                           \
    do                                                                                                                 \
    {                                                                                                                  \
        (__HANDLE__)->lock = UNLOCKED;                                                                                 \
    } while (0)
#endif /* USE_RTOS */

/**
 *@brief DMA Init 结构体定义
 *
 */
typedef struct
{
    u32 channel_num; /**< DMA channel number, should be in
                      *	range from 0 to 7.
                      *	Note: DMA channel 0 has the highest priority
                      *	and DMA channel 7 the lowest priority.
                      */

    u32 transfer_lli; /*源端和目的端LLP标识，
                       * should be one of the following:
                       * - DMAC_LLP_DIS
                       * - DMAC_LLP_SRC_EN
                       * - DMAC_LLP_DST_EN
                       * - DMAC_LLP_SRC_DST_EN
                       */

    u32 src_msize;
    u32 dst_msize;

    u32 src_tr_width; /**< Transfer width - used for TransferType is DMAC_TRANSFERTYPE_M2M only */
    u32 dst_tr_width;

    u32 src_inc; /**< Specifies whether the Source address register should be incremented or not.
                                                                This parameter can be a value of @ref
                    DMA_Peripheral_incremented_mode */

    u32 dst_inc; /**< Specifies whether the Destination address register should be incremented or not.
                                                                 This parameter can be a value of @ref
                    DMA_Memory_incremented_mode */

    u32 transfer_type; /**< Transfer Type, should be one of the following:
                        * - DMAC_TRANSFERTYPE_M2M: Memory to memory - DMA control
                        * - DMAC_TRANSFERTYPE_M2P: Memory to peripheral - DMA control
                        * - DMAC_TRANSFERTYPE_P2M: Peripheral to memory - DMA control
                        * - DMAC_TRANSFERTYPE_P2P: Source peripheral to destination peripheral - DMA control*/

    u32 peripheral_type; /*Transfer Type, should be one of the following:
                                                         DMAC_PeripheralTypeDef
                      */
    u32 src_addr;        /**< Physical Source Address, used in case TransferType is chosen as
                          *	 DMAC_TRANSFERTYPE_M2M or DMAC_TRANSFERTYPE_M2P */
    u32 dst_addr;        /**< Physical Destination Address, used in case TransferType is chosen as
                          *	 DMAC_TRANSFERTYPE_M2M or DMAC_TRANSFERTYPE_P2M */
    u32 transfer_size;   /**< Length/Size of transfer */

} DMAC_InitTypeDef;

typedef void (*dmac_callback_t)(void);

/**
 *@brief DMA handler 结构体定义
 *
 */
typedef struct
{
    DMAC_TypeDef     *instance;         /**< register base address       */
    DMAC_InitTypeDef  init;             /**< init required parameters	  */
    LockTypeDef       lock;             /**< DMA locking object          */
    DMAC_StateTypeDef state;            /**< DMA transfer state          */
    dmac_callback_t   dma_raw_callback; /**< DMA callback                */
    dmac_callback_t   dma_block_callback;
    dmac_callback_t   dma_err_callback;

} DMAC_HandleTypeDef;

extern StatusTypeDef DMAC_PollForTransfer(DMAC_HandleTypeDef *hdma, u32 timeoutMs);
extern void          DMAC_M2MPollTransfer(DMAC_HandleTypeDef *hdma, u8 *dst, u8 *src);
extern StatusTypeDef DMAC_Init(DMAC_HandleTypeDef *hdma);
extern StatusTypeDef DMAC_Deinit(DMAC_HandleTypeDef *hdma);
extern StatusTypeDef DMAC_StartPolling(DMAC_HandleTypeDef *hdma);

#endif /* __DMA_H__*/
