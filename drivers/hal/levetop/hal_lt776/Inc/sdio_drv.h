/**********************************************************
 * @Copyright (c) 2021   Levetop Semiconductor Co., Ltd.
 * @File			sdio_drv.h
 * @Author		Jason Zhao
 * @Date			2021/12/13
 * @Time			19:25:7
 * @Version
 * @Brief
 **********************************************************
 * Modification History
 *
 *
 **********************************************************/
#ifndef __SDIO_DRV_H__
#define __SDIO_DRV_H__

#if defined(__CC_ARM) || defined(__CLANG_ARM)
#include "lt776_reg.h"
#include "sdmmc_info.h"

/*!@brief sdio device support maximum IO number */
#ifndef FSL_SDIO_MAX_IO_NUMS
#define FSL_SDIO_MAX_IO_NUMS (7U)
#endif

/* Macro to change a value to a given size aligned value */
#define SDK_SIZEALIGN(var, alignbytes) \
    ((unsigned int)((var) + ((alignbytes)-1)) & (unsigned int)(~(unsigned int)((alignbytes)-1)))

/* @brief SDMMC global data buffer size, word unit*/
#define SDMMC_GLOBAL_BUFFER_SIZE (128U)

/* @brief L1 DCACHE line size in byte. */
#define FSL_FEATURE_L1DCACHE_LINESIZE_BYTE (32)
#define SDMMC_DATA_BUFFER_ALIGN_CACHE FSL_FEATURE_L1DCACHE_LINESIZE_BYTE

/* @brief SDIO retry times */
#define SDIO_RETRY_TIMES (1000U)

/**
 * @brief sdio card struct
 *
 * */
typedef struct _sdio_card sdio_card_t;
/**
 * @brief sdio io handler
 *
 * */
typedef void (*sdio_io_irq_handler_t)(sdio_card_t *card, uint32_t func);
/**
 * @}
 */

/*** 结构体、枚举变量定义 *****************************************************/
/** @defgroup SDCARD_Exported_Types Exported Types
 *
 * @{
 */

/**
 *@brief SDCARD状态定义.
 *
 */
typedef enum
{
    /* SDIO specific error defines */
    SD_CMD_CRC_FAIL = (1),           /* Command response received (but CRC check failed) */
    SD_DATA_CRC_FAIL = (2),          /* Data bock sent/received (CRC check Failed) */
    SD_CMD_RSP_TIMEOUT = (3),        /* Command response timeout */
    SD_DATA_TIMEOUT = (4),           /* Data time out */
    SD_TX_UNDERRUN = (5),            /* Transmit FIFO under-run */
    SD_RX_OVERRUN = (6),             /* Receive FIFO over-run */
    SD_START_BIT_ERR = (7),          /* Start bit not detected on all data signals in widE bus mode */
    SD_CMD_OUT_OF_RANGE = (8),       /* CMD's argument was out of range.*/
    SD_ADDR_MISALIGNED = (9),        /* Misaligned address */
    SD_BLOCK_LEN_ERR = (10),         /* Transferred block length is not allowed for the card or the number of transferred bytes
                                        does not match the block length */
    SD_ERASE_SEQ_ERR = (11),         /* An error in the sequence of erase command occurs.*/
    SD_BAD_ERASE_PARAM = (12),       /* An Invalid selection for erase groups */
    SD_WRITE_PROT_VIOLATION = (13),  /* Attempt to program a write protect block */
    SD_LOCK_UNLOCK_FAILED = (14),    /* Sequence or password error has been detected in unlock command or if there was an
                                        attempt to access a locked card */
    SD_COM_CRC_FAILED = (15),        /* CRC check of the previous command failed */
    SD_ILLEGAL_CMD = (16),           /* Command is not legal for the card state */
    SD_CARD_ECC_FAILED = (17),       /* Card internal ECC was applied but failed to correct the data */
    SD_CC_ERROR = (18),              /* Internal card controller error */
    SD_GENERAL_UNKNOWN_ERROR = (19), /* General or Unknown error */
    SD_STREAM_READ_UNDERRUN = (20),  /* The card could not sustain data transfer in stream read operation. */
    SD_STREAM_WRITE_OVERRUN = (21),  /* The card could not sustain data programming in stream mode */
    SD_CID_CSD_OVERWRITE = (22),     /* CID/CSD overwrite error */
    SD_WP_ERASE_SKIP = (23),         /* only partial address space was erased */
    SD_CARD_ECC_DISABLED = (24),     /* Command has been executed without using internal ECC */
    SD_ERASE_RESET =
        (25),                /* Erase sequence was cleared before executing because an out of erase sequence command was received */
    SD_AKE_SEQ_ERROR = (26), /* Error in sequence of authentication. */
    SD_INVALID_VOLTRANGE = (27),
    SD_ADDR_OUT_OF_RANGE = (28),
    SD_SWITCH_ERROR = (29),
    SD_SDIO_DISABLED = (30),
    SD_SDIO_FUNCTION_BUSY = (31),
    SD_SDIO_FUNCTION_FAILED = (32),
    SD_SDIO_UNKNOWN_FUNCTION = (33),

    /* Standard error defines */
    SD_INTERNAL_ERROR,
    SD_NOT_CONFIGURED,
    SD_REQUEST_PENDING,
    SD_REQUEST_NOT_APPLICABLE,
    SD_INVALID_PARAMETER,
    SD_UNSUPPORTED_FEATURE,
    SD_UNSUPPORTED_HW,
    SD_ERROR,
    SD_OK,
} HAL_SDCARD_StateTypeDef;

/**
 *@brief SDCARD传输状态定义.
 *
 */
typedef enum
{
    SD_NO_TRANSFER = 0,
    SD_TRANSFER_IN_PROGRESS
} SDTransferState;

/**
 *@brief SDCARD传输信息定义.
 *
 */
typedef struct
{
    uint16_t TransferredBytes;
    HAL_SDCARD_StateTypeDef TransferError;
    uint8_t padding;
} SDLastTransferInfo;

/**
 *@brief SDCARD Specific Data.
 *
 */
typedef struct
{
    __IO uint8_t CSDStruct;           /* CSD structure */
    __IO uint8_t SysSpecVersion;      /* System specification version */
    __IO uint8_t Reserved1;           /* Reserved */
    __IO uint8_t TAAC;                /* Data read access-time 1 */
    __IO uint8_t NSAC;                /* Data read access-time 2 in CLK cycles */
    __IO uint8_t MaxBusClkFrec;       /* Max. bus clock frequency */
    __IO uint16_t CardComdClasses;    /* Card command classes */
    __IO uint8_t RdBlockLen;          /* Max. read data block length */
    __IO uint8_t PartBlockRead;       /* Partial blocks for read allowed */
    __IO uint8_t WrBlockMisalign;     /* Write block misalignment */
    __IO uint8_t RdBlockMisalign;     /* Read block misalignment */
    __IO uint8_t DSRImpl;             /* DSR implemented */
    __IO uint8_t Reserved2;           /* Reserved */
    __IO uint32_t DeviceSize;         /* Device Size */
    __IO uint8_t MaxRdCurrentVDDMin;  /* Max. read current @ VDD min */
    __IO uint8_t MaxRdCurrentVDDMax;  /* Max. read current @ VDD max */
    __IO uint8_t MaxWrCurrentVDDMin;  /* Max. write current @ VDD min */
    __IO uint8_t MaxWrCurrentVDDMax;  /* Max. write current @ VDD max */
    __IO uint8_t DeviceSizeMul;       /* Device size multiplier */
    __IO uint8_t EraseGrSize;         /* Erase group size */
    __IO uint8_t EraseGrMul;          /* Erase group size multiplier */
    __IO uint8_t WrProtectGrSize;     /* Write protect group size */
    __IO uint8_t WrProtectGrEnable;   /* Write protect group enable */
    __IO uint8_t ManDeflECC;          /* Manufacturer default ECC */
    __IO uint8_t WrSpeedFact;         /* Write speed factor */
    __IO uint8_t MaxWrBlockLen;       /* Max. write data block length */
    __IO uint8_t WriteBlockPaPartial; /* Partial blocks for write allowed */
    __IO uint8_t Reserved3;           /* Reserded */
    __IO uint8_t ContentProtectAppli; /* Content protection application */
    __IO uint8_t FileFormatGrouop;    /* File format group */
    __IO uint8_t CopyFlag;            /* Copy flag (OTP) */
    __IO uint8_t PermWrProtect;       /* Permanent write protection */
    __IO uint8_t TempWrProtect;       /* Temporary write protection */
    __IO uint8_t FileFormat;          /* File Format */
    __IO uint8_t ECC;                 /* ECC code */
    __IO uint8_t CSD_CRC;             /* CSD CRC */
    __IO uint8_t Reserved4;           /* always 1*/
} SD_CSD;

/**
 *@brief SDCARD Identification Data.
 *
 */
typedef struct
{
    __IO uint8_t ManufacturerID; /* ManufacturerID */
    __IO uint16_t OEM_AppliID;   /* OEM/Application ID */
    __IO uint32_t ProdName1;     /* Product Name part1 */
    __IO uint8_t ProdName2;      /* Product Name part2*/
    __IO uint8_t ProdRev;        /* Product Revision */
    __IO uint32_t ProdSN;        /* Product Serial Number */
    __IO uint8_t Reserved1;      /* Reserved1 */
    __IO uint16_t ManufactDate;  /* Manufacturing Date */
    __IO uint8_t CID_CRC;        /* CID CRC */
    __IO uint8_t Reserved2;      /* always 1 */
} SD_CID;

/**
 *@brief SDCARD information.
 *
 */
typedef struct
{
    SD_CSD SD_csd;
    SD_CID SD_cid;
    long long CardCapacity; /* Card Capacity */
    uint32_t CardBlockSize; /* Card Block Size */
    uint32_t CardBlocks;    /* Card Blocks*/
    uint16_t RCA;
    uint8_t CardType;
} SD_CardInfo;

/**
 *  @brief card operation voltage
 *
 */
typedef enum _sdmmc_operation_voltage
{
    kCARD_OperationVoltageNone = 0U, /**< indicate current voltage setting is not setting by suser*/
    kCARD_OperationVoltage330V = 1U, /**< card operation voltage around 3.3v */
    kCARD_OperationVoltage300V = 2U, /**< card operation voltage around 3.0v */
    kCARD_OperationVoltage180V = 3U, /**< card operation voltage around 1.8v */
} sdmmc_operation_voltage_t;

/**
 * @brief sdio io read/write direction
 *
 */
typedef enum _sdio_io_direction
{
    kSDIO_IORead = 0U,  /**< io read */
    kSDIO_IOWrite = 1U, /**< io write */
} sdio_io_direction_t;

/**
 * @brief SDIO card state
 *
 * Define the card structure including the necessary fields to identify and describe the card.
 */
struct _sdio_card
{
    //    SDMMCHOST_CONFIG host;         /**< Host information */
    //    sdiocard_usr_param_t usrParam; /**< user parameter */
    bool noInternalAlign; /**< use this flag to disable sdmmc align. If disable, sdmmc will not make sure the
                         data buffer address is word align, otherwise all the transfer are align to low level driver */
    bool isHostReady;     /**< use this flag to indicate if need host re-init or not*/
    bool memPresentFlag;  /**< indicate if memory present */

    uint32_t busClock_Hz;                       /**< SD bus clock frequency united in Hz */
    uint32_t relativeAddress;                   /**< Relative address of the card */
    uint8_t sdVersion;                          /**< SD version */
    sd_timing_mode_t currentTiming;             /**< current timing mode */
    sd_driver_strength_t driverStrength;        /**< driver strength */
    sd_max_current_t maxCurrent;                /**< card current limit */
    sdmmc_operation_voltage_t operationVoltage; /**< card operation voltage */

    uint8_t sdioVersion;         /**< SDIO version */
    uint8_t cccrVersioin;        /**< CCCR version */
    uint8_t ioTotalNumber;       /**< total number of IO function */
    uint32_t cccrflags;          /**< Flags in _sd_card_flag */
    uint32_t io0blockSize;       /**< record the io0 block size*/
    uint32_t ocr;                /**< Raw OCR content, only 24bit avalible for SDIO card */
    uint32_t commonCISPointer;   /**< point to common CIS */
    sdio_common_cis_t commonCIS; /**< CIS table */

    /* io registers/IRQ handler */
    sdio_fbr_t ioFBR[FSL_SDIO_MAX_IO_NUMS];                   /**< FBR table */
    sdio_func_cis_t funcCIS[FSL_SDIO_MAX_IO_NUMS];            /**< function CIS table*/
    sdio_io_irq_handler_t ioIRQHandler[FSL_SDIO_MAX_IO_NUMS]; /**< io IRQ handler */
    uint8_t ioIntIndex;                                       /**< used to record current enabled io interrupt index */
    uint8_t ioIntNums;                                        /**< used to record total enabled io interrupt numbers  */
};

/*** 函数声明 ******************************************************************/
/** @defgroup SDCARD_Exported_Functions Exported Functions
 * @{
 */
HAL_SDCARD_StateTypeDef HAL_SD_EnumCard(void);
HAL_SDCARD_StateTypeDef
HAL_SD_ReadBlock(uint32_t addr, uint32_t *readbuff, uint16_t BlockSize, uint32_t NumberOfBlocks);
HAL_SDCARD_StateTypeDef
HAL_SD_WriteBlock(uint32_t addr, uint32_t *writebuff, uint16_t BlockSize, uint32_t NumberOfBlocks);
HAL_SDCARD_StateTypeDef HAL_SD_Init(void);

#elif defined(__GNUC__)
#include "type.h"
#include "lt776_reg.h"

/**
 *@brief SDCARD状态定义.
 *
 */
typedef enum
{
    /* SDIO specific error defines */
    SD_CMD_CRC_FAIL = (1),           /* Command response received (but CRC check failed) */
    SD_DATA_CRC_FAIL = (2),          /* Data bock sent/received (CRC check Failed) */
    SD_CMD_RSP_TIMEOUT = (3),        /* Command response timeout */
    SD_DATA_TIMEOUT = (4),           /* Data time out */
    SD_TX_UNDERRUN = (5),            /* Transmit FIFO under-run */
    SD_RX_OVERRUN = (6),             /* Receive FIFO over-run */
    SD_START_BIT_ERR = (7),          /* Start bit not detected on all data signals in widE bus mode */
    SD_CMD_OUT_OF_RANGE = (8),       /* CMD's argument was out of range.*/
    SD_ADDR_MISALIGNED = (9),        /* Misaligned address */
    SD_BLOCK_LEN_ERR = (10),         /* Transferred block length is not allowed for the card or the number of transferred bytes does not match the block length */
    SD_ERASE_SEQ_ERR = (11),         /* An error in the sequence of erase command occurs.*/
    SD_BAD_ERASE_PARAM = (12),       /* An Invalid selection for erase groups */
    SD_WRITE_PROT_VIOLATION = (13),  /* Attempt to program a write protect block */
    SD_LOCK_UNLOCK_FAILED = (14),    /* Sequence or password error has been detected in unlock command or if there was an attempt to access a locked card */
    SD_COM_CRC_FAILED = (15),        /* CRC check of the previous command failed */
    SD_ILLEGAL_CMD = (16),           /* Command is not legal for the card state */
    SD_CARD_ECC_FAILED = (17),       /* Card internal ECC was applied but failed to correct the data */
    SD_CC_ERROR = (18),              /* Internal card controller error */
    SD_GENERAL_UNKNOWN_ERROR = (19), /* General or Unknown error */
    SD_STREAM_READ_UNDERRUN = (20),  /* The card could not sustain data transfer in stream read operation. */
    SD_STREAM_WRITE_OVERRUN = (21),  /* The card could not sustain data programming in stream mode */
    SD_CID_CSD_OVERWRITE = (22),     /* CID/CSD overwrite error */
    SD_WP_ERASE_SKIP = (23),         /* only partial address space was erased */
    SD_CARD_ECC_DISABLED = (24),     /* Command has been executed without using internal ECC */
    SD_ERASE_RESET = (25),           /* Erase sequence was cleared before executing because an out of erase sequence command was received */
    SD_AKE_SEQ_ERROR = (26),         /* Error in sequence of authentication. */
    SD_INVALID_VOLTRANGE = (27),
    SD_ADDR_OUT_OF_RANGE = (28),
    SD_SWITCH_ERROR = (29),
    SD_SDIO_DISABLED = (30),
    SD_SDIO_FUNCTION_BUSY = (31),
    SD_SDIO_FUNCTION_FAILED = (32),
    SD_SDIO_UNKNOWN_FUNCTION = (33),

    /* Standard error defines */
    SD_INTERNAL_ERROR,
    SD_NOT_CONFIGURED,
    SD_REQUEST_PENDING,
    SD_REQUEST_NOT_APPLICABLE,
    SD_INVALID_PARAMETER,
    SD_UNSUPPORTED_FEATURE,
    SD_UNSUPPORTED_HW,
    SD_ERROR,
    SD_OK,
} SdStateTypeDef;
/**
 *@brief SDCARD Specific Data.
 *
 */
typedef struct
{
    __IO u8 CSDStruct;           /* CSD structure */
    __IO u8 SysSpecVersion;      /* System specification version */
    __IO u8 Reserved1;           /* Reserved */
    __IO u8 TAAC;                /* Data read access-time 1 */
    __IO u8 NSAC;                /* Data read access-time 2 in CLK cycles */
    __IO u8 MaxBusClkFrec;       /* Max. bus clock frequency */
    __IO u16 CardComdClasses;    /* Card command classes */
    __IO u8 RdBlockLen;          /* Max. read data block length */
    __IO u8 PartBlockRead;       /* Partial blocks for read allowed */
    __IO u8 WrBlockMisalign;     /* Write block misalignment */
    __IO u8 RdBlockMisalign;     /* Read block misalignment */
    __IO u8 DSRImpl;             /* DSR implemented */
    __IO u8 Reserved2;           /* Reserved */
    __IO u32 DeviceSize;         /* Device Size */
    __IO u8 MaxRdCurrentVDDMin;  /* Max. read current @ VDD min */
    __IO u8 MaxRdCurrentVDDMax;  /* Max. read current @ VDD max */
    __IO u8 MaxWrCurrentVDDMin;  /* Max. write current @ VDD min */
    __IO u8 MaxWrCurrentVDDMax;  /* Max. write current @ VDD max */
    __IO u8 DeviceSizeMul;       /* Device size multiplier */
    __IO u8 EraseGrSize;         /* Erase group size */
    __IO u8 EraseGrMul;          /* Erase group size multiplier */
    __IO u8 WrProtectGrSize;     /* Write protect group size */
    __IO u8 WrProtectGrEnable;   /* Write protect group enable */
    __IO u8 ManDeflECC;          /* Manufacturer default ECC */
    __IO u8 WrSpeedFact;         /* Write speed factor */
    __IO u8 MaxWrBlockLen;       /* Max. write data block length */
    __IO u8 WriteBlockPaPartial; /* Partial blocks for write allowed */
    __IO u8 Reserved3;           /* Reserded */
    __IO u8 ContentProtectAppli; /* Content protection application */
    __IO u8 FileFormatGrouop;    /* File format group */
    __IO u8 CopyFlag;            /* Copy flag (OTP) */
    __IO u8 PermWrProtect;       /* Permanent write protection */
    __IO u8 TempWrProtect;       /* Temporary write protection */
    __IO u8 FileFormat;          /* File Format */
    __IO u8 ECC;                 /* ECC code */
    __IO u8 CSD_CRC;             /* CSD CRC */
    __IO u8 Reserved4;           /* always 1*/
} SD_CSD;

/**
 *@brief SDCARD Identification Data.
 *
 */
typedef struct
{
    __IO u8 ManufacturerID; /* ManufacturerID */
    __IO u16 OEM_AppliID;   /* OEM/Application ID */
    __IO u32 ProdName1;     /* Product Name part1 */
    __IO u8 ProdName2;      /* Product Name part2*/
    __IO u8 ProdRev;        /* Product Revision */
    __IO u32 ProdSN;        /* Product Serial Number */
    __IO u8 Reserved1;      /* Reserved1 */
    __IO u16 ManufactDate;  /* Manufacturing Date */
    __IO u8 CID_CRC;        /* CID CRC */
    __IO u8 Reserved2;      /* always 1 */
} SD_CID;
typedef struct
{
    SD_CSD SD_csd;
    SD_CID SD_cid;
    long long CardCapacity; /* Card Capacity */
    u32 CardBlockSize;      /* Card Block Size */
    u32 CardBlocks;         /* Card Blocks*/
    u16 RCA;
    u8 CardType;
} SD_CardInfo;

extern SdStateTypeDef InitSdCard(void);
extern SdStateTypeDef ReInitSdCard(void);
extern SdStateTypeDef ReadSdCardBlock(u32 addr, u32 *readbuff, u16 BlockSize, u32 NumberOfBlocks);
extern SdStateTypeDef WriteSdCardBlock(u32 addr, u32 *writebuff, u16 BlockSize, u32 NumberOfBlocks);
#endif

#endif /* __SDIO_DRV_H__*/
