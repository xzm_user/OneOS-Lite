/**
  ******************************************************************************
             Copyright(c) 2021 Levetop Semiconductor Co. Ltd.
                      All Rights Reserved
  ******************************************************************************
  * @file    iomacros.h
  * @author  Product application department
  * @version V1.0
  * @date    2021.11.01
  * @brief   Header file of iomacros.
  *
  ******************************************************************************
*/

#ifndef __IOMACROS_H__
#define __IOMACROS_H__

#include "type.h"
#include <string.h>

//#define FILE_OUT

/*****************************************************************************/
/*bit operation*/
#define _bit(x) ((uint32_t)1 << (x))
#define _bit_set(value, bit) ((value) |= (bit))
#define _bit_clr(value, bit) ((value) &= ~(bit))
#define _bit_get(value, bit) ((value) & (bit))
#define _reg_chk(value, bit) ((value) & (bit))
#define _bit_and(value, bit) ((value) &= (bit))

/*****************************************************************************/
#define _reg_write(reg, val) ((reg) = (val))
#define _reg_read(reg) ((reg))
#define _reg_clear(reg) ((reg) = (0x0))
#define _reg_modify(reg, clearmask, setmask) _reg_write((reg), (((_reg_read(reg)) & (clearmask)) | (setmask)))

/*****************************************************************************/
#define IO_READ8(p) (*(volatile uint8_t *)(p))
#define IO_WRITE8(p, v) (*((volatile uint8_t *)(p)) = ((uint8_t)(v)))
#define IO_READ16(p) (*(volatile uint16_t *)(p))
#define IO_WRITE16(p, v) (*((volatile uint16_t *)(p)) = (uint16_t)(v))
#define IO_READ32(p) (*(volatile uint32_t *)(p))
#define IO_WRITE32(p, v) (*((volatile uint32_t *)(p)) = (uint32_t)(v))
/*****************************************************************************/

#define _memcpy memcpy
#define _memset memset
#endif /* __IOMACROS_H__ */
