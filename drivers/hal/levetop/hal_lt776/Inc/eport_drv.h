/**
  ******************************************************************************
             Copyright(c) 2021 Levetop Semiconductor Co. Ltd.
                      All Rights Reserved
  ******************************************************************************
  * @file    eport_drv.h
  * @author  Product application department
  * @version V1.0
  * @date    2021.11.01
  * @brief   Header file of eport DRV module.
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __EPORT_DRV_H__
#define __EPORT_DRV_H__

#ifdef __cplusplus
extern "C"
{
#endif

/* Includes ------------------------------------------------------------------*/

//#include "interrupt.h"
#include "cpm_drv.h"
//#include "debug.h"
#include "eport_drv.h"
//#include "ioctrl_drv.h"

#ifdef FILE_OUT
/**@defgroup DRV 3 HAL driver
  *
  *@{
  */

/** @defgroup DRV_EPORT EPORT
  *
  *@{
  */
#endif
/*** 宏定义 *******************************************************************/
#ifdef FILE_OUT
/** @defgroup DRV_EPORT_Exported_Macros Exported Macros
  *
  * @{
  */
#endif

#define PIN_NUM_IS(pin) (pin & 0x01 ? 0 : (pin & 0x02 ? 1 : (pin & 0x04 ? 2 : (pin & 0x08 ? 3 : (pin & 0x10 ? 4 : (pin & 0x20 ? 5 : (pin & 0x40 ? 6 : 7)))))))

/* EPPAR */
#define _eport_eppar_trigger_level_set(eportx, pins) _bit_clr(eportx->EPPAR, (0x03 << pins * 2))
#define _eport_eppar_trigger_rising_set(eportx, pins) _reg_modify(eportx->EPPAR, ~(0x3 << pins * 2), (0x01 << pins * 2))
#define _eport_eppar_trigger_falling_set(eportx, pins) _reg_modify(eportx->EPPAR, ~(0x3 << pins * 2), (0x02 << pins * 2))
#define _eport_eppar_trigger_falling_rising_set(eportx, pins) _reg_modify(eportx->EPPAR, ~(0x3 << pins * 2), (0x03 << pins * 2))
#define _eport_eppar_pin0_trigger_set(eportx, val) _reg_write(eportx->EPPAR, (uint16_t)val << EPORT_EPPAR_EPPA0)
#define _eport_eppar_pin1_trigger_set(eportx, val) _reg_write(eportx->EPPAR, (uint16_t)val << EPORT_EPPAR_EPPA1)
#define _eport_eppar_pin2_trigger_set(eportx, val) _reg_write(eportx->EPPAR, (uint16_t)val << EPORT_EPPAR_EPPA2)
#define _eport_eppar_pin3_trigger_set(eportx, val) _reg_write(eportx->EPPAR, (uint16_t)val << EPORT_EPPAR_EPPA3)
#define _eport_eppar_pin4_trigger_set(eportx, val) _reg_write(eportx->EPPAR, (uint16_t)val << EPORT_EPPAR_EPPA4)
#define _eport_eppar_pin5_trigger_set(eportx, val) _reg_write(eportx->EPPAR, (uint16_t)val << EPORT_EPPAR_EPPA5)
#define _eport_eppar_pin6_trigger_set(eportx, val) _reg_write(eportx->EPPAR, (uint16_t)val << EPORT_EPPAR_EPPA6)
#define _eport_eppar_pin7_trigger_set(eportx, val) _reg_write(eportx->EPPAR, (uint16_t)val << EPORT_EPPAR_EPPA7)

/* EPDDR */
#define _eport_epddr_pins_output_set(eportx, pins) _bit_set(eportx->EPDDR, pins << EPORT_EPDDR_EPDD)
#define _eport_epddr_pins_input_set(eportx, pins) _bit_clr(eportx->EPDDR, pins << EPORT_EPDDR_EPDD)
#define _eport_epddr_pin0_output_set(eportx) _bit_set(eportx->EPDDR, EPORT_EPDDR_EPDD0)
#define _eport_epddr_pin0_input_set(eportx) _bit_clr(eportx->EPDDR, EPORT_EPDDR_EPDD0)
#define _eport_epddr_pin1_output_set(eportx) _bit_set(eportx->EPDDR, EPORT_EPDDR_EPDD1)
#define _eport_epddr_pin1_input_set(eportx) _bit_clr(eportx->EPDDR, EPORT_EPDDR_EPDD1)
#define _eport_epddr_pin2_output_set(eportx) _bit_set(eportx->EPDDR, EPORT_EPDDR_EPDD2)
#define _eport_epddr_pin2_input_set(eportx) _bit_clr(eportx->EPDDR, EPORT_EPDDR_EPDD2)
#define _eport_epddr_pin3_output_set(eportx) _bit_set(eportx->EPDDR, EPORT_EPDDR_EPDD3)
#define _eport_epddr_pin3_input_set(eportx) _bit_clr(eportx->EPDDR, EPORT_EPDDR_EPDD3)
#define _eport_epddr_pin4_output_set(eportx) _bit_set(eportx->EPDDR, EPORT_EPDDR_EPDD4)
#define _eport_epddr_pin4_input_set(eportx) _bit_clr(eportx->EPDDR, EPORT_EPDDR_EPDD4)
#define _eport_epddr_pin5_output_set(eportx) _bit_set(eportx->EPDDR, EPORT_EPDDR_EPDD5)
#define _eport_epddr_pin5_input_set(eportx) _bit_clr(eportx->EPDDR, EPORT_EPDDR_EPDD5)
#define _eport_epddr_pin6_output_set(eportx) _bit_set(eportx->EPDDR, EPORT_EPDDR_EPDD6)
#define _eport_epddr_pin6_input_set(eportx) _bit_clr(eportx->EPDDR, EPORT_EPDDR_EPDD6)
#define _eport_epddr_pin7_output_set(eportx) _bit_set(eportx->EPDDR, EPORT_EPDDR_EPDD7)
#define _eport_epddr_pin7_input_set(eportx) _bit_clr(eportx->EPDDR, EPORT_EPDDR_EPDD7)

/* EPIER */
#define _eport_epier_pins_it_en(eportx, pins) _bit_set(eportx->EPIER, pins << EPORT_EPIER_EPIE)
#define _eport_epier_pins_it_dis(eportx, pins) _bit_clr(eportx->EPIER, pins << EPORT_EPIER_EPIE)
#define _eport_epier_pin0_it_en(eportx) _bit_set(eportx->EPIER, EPORT_EPIER_EPIE0)
#define _eport_epier_pin0_it_dis(eportx) _bit_clr(eportx->EPIER, EPORT_EPIER_EPIE0)
#define _eport_epier_pin1_it_en(eportx) _bit_set(eportx->EPIER, EPORT_EPIER_EPIE1)
#define _eport_epier_pin1_it_dis(eportx) _bit_clr(eportx->EPIER, EPORT_EPIER_EPIE1)
#define _eport_epier_pin2_it_en(eportx) _bit_set(eportx->EPIER, EPORT_EPIER_EPIE2)
#define _eport_epier_pin2_it_dis(eportx) _bit_clr(eportx->EPIER, EPORT_EPIER_EPIE2)
#define _eport_epier_pin3_it_en(eportx) _bit_set(eportx->EPIER, EPORT_EPIER_EPIE3)
#define _eport_epier_pin3_it_dis(eportx) _bit_clr(eportx->EPIER, EPORT_EPIER_EPIE3)
#define _eport_epier_pin4_it_en(eportx) _bit_set(eportx->EPIER, EPORT_EPIER_EPIE4)
#define _eport_epier_pin4_it_dis(eportx) _bit_clr(eportx->EPIER, EPORT_EPIER_EPIE4)
#define _eport_epier_pin5_it_en(eportx) _bit_set(eportx->EPIER, EPORT_EPIER_EPIE5)
#define _eport_epier_pin5_it_dis(eportx) _bit_clr(eportx->EPIER, EPORT_EPIER_EPIE5)
#define _eport_epier_pin6_it_en(eportx) _bit_set(eportx->EPIER, EPORT_EPIER_EPIE6)
#define _eport_epier_pin6_it_dis(eportx) _bit_clr(eportx->EPIER, EPORT_EPIER_EPIE6)
#define _eport_epier_pin7_it_en(eportx) _bit_set(eportx->EPIER, EPORT_EPIER_EPIE7)
#define _eport_epier_pin7_it_dis(eportx) _bit_clr(eportx->EPIER, EPORT_EPIER_EPIE7)

/* EPDR load值，pin为输出时，输出此值*/
#define _eport_epdr_pins_load_set(eportx, pins) _bit_set(eportx->EPDR, pins << EPORT_EPDR_EPD)
#define _eport_epdr_pins_load_clr(eportx, pins) _bit_clr(eportx->EPDR, pins << EPORT_EPDR_EPD)
#define _eport_epdr_pin0_load_set(eportx) _bit_set(eportx->EPDR, EPORT_EPDR_EPD0)
#define _eport_epdr_pin0_load_clr(eportx) _bit_clr(eportx->EPDR, EPORT_EPDR_EPD0)
#define _eport_epdr_pin1_load_set(eportx) _bit_set(eportx->EPDR, EPORT_EPDR_EPD1)
#define _eport_epdr_pin1_load_clr(eportx) _bit_clr(eportx->EPDR, EPORT_EPDR_EPD1)
#define _eport_epdr_pin2_load_set(eportx) _bit_set(eportx->EPDR, EPORT_EPDR_EPD2)
#define _eport_epdr_pin2_load_clr(eportx) _bit_clr(eportx->EPDR, EPORT_EPDR_EPD2)
#define _eport_epdr_pin3_load_set(eportx) _bit_set(eportx->EPDR, EPORT_EPDR_EPD3)
#define _eport_epdr_pin3_load_clr(eportx) _bit_clr(eportx->EPDR, EPORT_EPDR_EPD3)
#define _eport_epdr_pin4_load_set(eportx) _bit_set(eportx->EPDR, EPORT_EPDR_EPD4)
#define _eport_epdr_pin4_load_clr(eportx) _bit_clr(eportx->EPDR, EPORT_EPDR_EPD4)
#define _eport_epdr_pin5_load_set(eportx) _bit_set(eportx->EPDR, EPORT_EPDR_EPD5)
#define _eport_epdr_pin5_load_clr(eportx) _bit_clr(eportx->EPDR, EPORT_EPDR_EPD5)
#define _eport_epdr_pin6_load_set(eportx) _bit_set(eportx->EPDR, EPORT_EPDR_EPD6)
#define _eport_epdr_pin6_load_clr(eportx) _bit_clr(eportx->EPDR, EPORT_EPDR_EPD6)
#define _eport_epdr_pin7_load_set(eportx) _bit_set(eportx->EPDR, EPORT_EPDR_EPD7)
#define _eport_epdr_pin7_load_clr(eportx) _bit_clr(eportx->EPDR, EPORT_EPDR_EPD7)

/* EPPDR 只读，pin当前值*/
#define _eport_eppdr_pins_data_get(eportx, pins) _bit_get(eportx->EPPDR, pins << EPORT_EPPDR_EPPD)
#define _eport_eppdr_pin0_data_get(eportx) _bit_get(eportx->EPPDR, EPORT_EPPDR_EPPD0)
#define _eport_eppdr_pin1_data_get(eportx) _bit_get(eportx->EPPDR, EPORT_EPPDR_EPPD1)
#define _eport_eppdr_pin2_data_get(eportx) _bit_get(eportx->EPPDR, EPORT_EPPDR_EPPD2)
#define _eport_eppdr_pin3_data_get(eportx) _bit_get(eportx->EPPDR, EPORT_EPPDR_EPPD3)
#define _eport_eppdr_pin4_data_get(eportx) _bit_get(eportx->EPPDR, EPORT_EPPDR_EPPD4)
#define _eport_eppdr_pin5_data_get(eportx) _bit_get(eportx->EPPDR, EPORT_EPPDR_EPPD5)
#define _eport_eppdr_pin6_data_get(eportx) _bit_get(eportx->EPPDR, EPORT_EPPDR_EPPD6)
#define _eport_eppdr_pin7_data_get(eportx) _bit_get(eportx->EPPDR, EPORT_EPPDR_EPPD7)

/* EPFR 只读，pin中断标志位*/
#define _eport_epfr_pins_flag_get(eportx, pin) _bit_get(eportx->EPFR, pin << EPORT_EPFR_EPF)
#define _eport_epfr_pins_flag_clr(eportx, pin) _bit_set(eportx->EPFR, pin << EPORT_EPFR_EPF)
#define _eport_epFr_pin0_flag_get(eportx) _bit_get(eportx->EPFR, EPORT_EPFR_EPF0)
#define _eport_epFr_pin1_flag_get(eportx) _bit_get(eportx->EPFR, EPORT_EPFR_EPF1)
#define _eport_epFr_pin2_flag_get(eportx) _bit_get(eportx->EPFR, EPORT_EPFR_EPF2)
#define _eport_epFr_pin3_flag_get(eportx) _bit_get(eportx->EPFR, EPORT_EPFR_EPF3)
#define _eport_epFr_pin4_flag_get(eportx) _bit_get(eportx->EPFR, EPORT_EPFR_EPF4)
#define _eport_epFr_pin5_flag_get(eportx) _bit_get(eportx->EPFR, EPORT_EPFR_EPF5)
#define _eport_epFr_pin6_flag_get(eportx) _bit_get(eportx->EPFR, EPORT_EPFR_EPF6)
#define _eport_epFr_pin7_flag_get(eportx) _bit_get(eportx->EPFR, EPORT_EPFR_EPF7)

/* EPPUER */
#define _eport_eppuer_pins_pull_up_en(eportx, pins) _bit_set(eportx->EPPUER, pins << EPORT_EPPUER_EPPUE)
#define _eport_eppuer_pins_pull_up_dis(eportx, pins) _bit_clr(eportx->EPPUER, pins << EPORT_EPPUER_EPPUE)
#define _eport_eppuer_pin0_pull_up_en(eportx) _bit_set(eportx->EPPUER, EPORT_EPPUER_EPPUE0)
#define _eport_eppuer_pin0_pull_up_dis(eportx) _bit_clr(eportx->EPPUER, EPORT_EPPUER_EPPUE0)
#define _eport_eppuer_pin1_pull_up_en(eportx) _bit_set(eportx->EPPUER, EPORT_EPPUER_EPPUE1)
#define _eport_eppuer_pin1_pull_up_dis(eportx) _bit_clr(eportx->EPPUER, EPORT_EPPUER_EPPUE1)
#define _eport_eppuer_pin2_pull_up_en(eportx) _bit_set(eportx->EPPUER, EPORT_EPPUER_EPPUE2)
#define _eport_eppuer_pin2_pull_up_dis(eportx) _bit_clr(eportx->EPPUER, EPORT_EPPUER_EPPUE2)
#define _eport_eppuer_pin3_pull_up_en(eportx) _bit_set(eportx->EPPUER, EPORT_EPPUER_EPPUE3)
#define _eport_eppuer_pin3_pull_up_dis(eportx) _bit_clr(eportx->EPPUER, EPORT_EPPUER_EPPUE3)
#define _eport_eppuer_pin4_pull_up_en(eportx) _bit_set(eportx->EPPUER, EPORT_EPPUER_EPPUE4)
#define _eport_eppuer_pin4_pull_up_dis(eportx) _bit_clr(eportx->EPPUER, EPORT_EPPUER_EPPUE4)
#define _eport_eppuer_pin5_pull_up_en(eportx) _bit_set(eportx->EPPUER, EPORT_EPPUER_EPPUE5)
#define _eport_eppuer_pin5_pull_up_dis(eportx) _bit_clr(eportx->EPPUER, EPORT_EPPUER_EPPUE5)
#define _eport_eppuer_pin6_pull_up_en(eportx) _bit_set(eportx->EPPUER, EPORT_EPPUER_EPPUE6)
#define _eport_eppuer_pin6_pull_up_dis(eportx) _bit_clr(eportx->EPPUER, EPORT_EPPUER_EPPUE6)
#define _eport_eppuer_pin7_pull_up_en(eportx) _bit_set(eportx->EPPUER, EPORT_EPPUER_EPPUE7)
#define _eport_eppuer_pin7_pull_up_dis(eportx) _bit_clr(eportx->EPPUER, EPORT_EPPUER_EPPUE7)

/* EPLPR */
#define _eport_eplpr_pins_high_level_set(eportx, pins) _bit_set(eportx->EPLPR, pins << EPORT_EPLPR_EPLP)
#define _eport_eplpr_pins_low_level_set(eportx, pins) _bit_clr(eportx->EPLPR, pins << EPORT_EPLPR_EPLP)
#define _eport_eplpr_pin0_high_level_set(eportx) _bit_set(eportx->EPLPR, EPORT_EPLPR_EPLP0)
#define _eport_eplpr_pin0_low_level_set(eportx) _bit_clr(eportx->EPLPR, EPORT_EPLPR_EPLP0)
#define _eport_eplpr_pin1_high_level_set(eportx) _bit_set(eportx->EPLPR, EPORT_EPLPR_EPLP1)
#define _eport_eplpr_pin1_low_level_set(eportx) _bit_clr(eportx->EPLPR, EPORT_EPLPR_EPLP1)
#define _eport_eplpr_pin2_high_level_set(eportx) _bit_set(eportx->EPLPR, EPORT_EPLPR_EPLP2)
#define _eport_eplpr_pin2_low_level_set(eportx) _bit_clr(eportx->EPLPR, EPORT_EPLPR_EPLP2)
#define _eport_eplpr_pin3_high_level_set(eportx) _bit_set(eportx->EPLPR, EPORT_EPLPR_EPLP3)
#define _eport_eplpr_pin3_low_level_set(eportx) _bit_clr(eportx->EPLPR, EPORT_EPLPR_EPLP3)
#define _eport_eplpr_pin4_high_level_set(eportx) _bit_set(eportx->EPLPR, EPORT_EPLPR_EPLP4)
#define _eport_eplpr_pin4_low_level_set(eportx) _bit_clr(eportx->EPLPR, EPORT_EPLPR_EPLP4)
#define _eport_eplpr_pin5_high_level_set(eportx) _bit_set(eportx->EPLPR, EPORT_EPLPR_EPLP5)
#define _eport_eplpr_pin5_low_level_set(eportx) _bit_clr(eportx->EPLPR, EPORT_EPLPR_EPLP5)
#define _eport_eplpr_pin6_high_level_set(eportx) _bit_set(eportx->EPLPR, EPORT_EPLPR_EPLP6)
#define _eport_eplpr_pin6_low_level_set(eportx) _bit_clr(eportx->EPLPR, EPORT_EPLPR_EPLP6)
#define _eport_eplpr_pin7_high_level_set(eportx) _bit_set(eportx->EPLPR, EPORT_EPLPR_EPLP7)
#define _eport_eplpr_pin7_low_level_set(eportx) _bit_clr(eportx->EPLPR, EPORT_EPLPR_EPLP7)

/* EPODER */
#define _eport_epoder_pins_output_od_set(eportx, pins) _bit_set(eportx->EPODER, pins << EPORT_EPODER_EPODE)
#define _eport_epoder_pins_output_cmos_set(eportx, pins) _bit_clr(eportx->EPODER, pins << EPORT_EPODER_EPODE)
#define _eport_epoder_pin0_high_level_set(eportx) _bit_set(eportx->EPODER, EPORT_EPODER_EPODE0)
#define _eport_epoder_pin0_low_level_set(eportx) _bit_clr(eportx->EPODER, EPORT_EPODER_EPODE0)
#define _eport_epoder_pin1_high_level_set(eportx) _bit_set(eportx->EPODER, EPORT_EPODER_EPODE1)
#define _eport_epoder_pin1_low_level_set(eportx) _bit_clr(eportx->EPODER, EPORT_EPODER_EPODE1)
#define _eport_epoder_pin2_high_level_set(eportx) _bit_set(eportx->EPODER, EPORT_EPODER_EPODE2)
#define _eport_epoder_pin2_low_level_set(eportx) _bit_clr(eportx->EPODER, EPORT_EPODER_EPODE2)
#define _eport_epoder_pin3_high_level_set(eportx) _bit_set(eportx->EPODER, EPORT_EPODER_EPODE3)
#define _eport_epoder_pin3_low_level_set(eportx) _bit_clr(eportx->EPODER, EPORT_EPODER_EPODE3)
#define _eport_epoder_pin4_high_level_set(eportx) _bit_set(eportx->EPODER, EPORT_EPODER_EPODE4)
#define _eport_epoder_pin4_low_level_set(eportx) _bit_clr(eportx->EPODER, EPORT_EPODER_EPODE4)
#define _eport_epoder_pin5_high_level_set(eportx) _bit_set(eportx->EPODER, EPORT_EPODER_EPODE5)
#define _eport_epoder_pin5_low_level_set(eportx) _bit_clr(eportx->EPODER, EPORT_EPODER_EPODE5)
#define _eport_epoder_pin6_high_level_set(eportx) _bit_set(eportx->EPODER, EPORT_EPODER_EPODE6)
#define _eport_epoder_pin6_low_level_set(eportx) _bit_clr(eportx->EPODER, EPORT_EPODER_EPODE6)
#define _eport_epoder_pin7_high_level_set(eportx) _bit_set(eportx->EPODER, EPORT_EPODER_EPODE7)
#define _eport_epoder_pin7_low_level_set(eportx) _bit_clr(eportx->EPODER, EPORT_EPODER_EPODE7)

/**
  * @}
  */

/*** 结构体、枚举变量定义 *****************************************************/
#ifdef FILE_OUT
/** @defgroup DRV_EPORT_Exported_Types Exported Types
  *
  * @{
  */
#endif

  typedef enum
  {
    EPORT_TRIGGER_NULL = 0,
    EPORT_HIGH_LEVEL_TRIGGER,
    EPORT_LOW_LEVEL_TRIGGER,
    EPORT_RISING_TRIGGER,
    EPORT_FALLING_TRIGGER,
    EPORT_RISINGFALLING_TRIGGER
  } EPORT_IntModeDef;

  /**
 * @brief eport 管脚定义
 *
 */
  typedef enum
  {
    EPORT_PIN_BIT0 = 0x01,
    EPORT_PIN_BIT1 = 0x02,
    EPORT_PIN_BIT2 = 0x04,
    EPORT_PIN_BIT3 = 0x08,
    EPORT_PIN_BIT4 = 0x10,
    EPORT_PIN_BIT5 = 0x20,
    EPORT_PIN_BIT6 = 0x40,
    EPORT_PIN_BIT7 = 0x80
  } EPORT_PinTypeDef;

  typedef enum
  {
    EPORT_OUTPUT_MODE_OPEN_DRAIN = 0,
    EPORT_OUTPUT_MODE_CMOS
  } EPORT_OutputModeTypeDef;
		
/**
* @brief  GPIO输入/输出定义
*
*/
typedef enum
{
	GPIO_DIR_IN,
	GPIO_DIR_OUT,
	GPIo_DIR_TRIGATE,
} GPIO_DirTypeDef,EPORT_DirTypeDef;
/**
* @brief  GPIO输入模式定义
*
*/
typedef enum
{
	GPIO_INPUT_MODE_CMOS,
	GPIO_INPUT_MODE_SCHMITT,
} GPIO_InputModeTypeDef;
/**
* @brief  GPIO输出模式定义
*
*/
typedef enum
{
	GPIO_OUTPUT_MODE_OPEN_DRAIN,
	GPIO_OUTPUT_MODE_CMOS
} GPIO_OutputModeTypeDef;
/**
* @brief  上下拉模式定义
*
*/
typedef enum
{
	GPIO_PULL_MODE_NOPULL,
	GPIO_PULL_MODE_UP,
	GPIO_PULL_MODE_DOWN,
} GPIO_PullModeTypeDef;
/**
* @brief  GPIO反转速度定义
*
*/
typedef enum
{
	GPIO_SLEW_RATE_FALST,
	GPIO_SLEW_RATE_SLOW,
} GPIO_SlewRateTypeDef;
/**
* @brief  GPIO驱动电流定义
*
*/
typedef enum
{
	GPIO_DRIVE_STRENGTH_2MA,
	GPIO_DRIVE_STRENGTH_4MA,
	GPIO_DRIVE_STRENGTH_8MA,
	GPIO_DRIVE_STRENGTH_12MA,
} GPIO_DriveStrengthTypeDef;
/**
* @brief  GPIO初始化结构体定义
*
*/
typedef struct
{
	uint8_t Pin;
	uint8_t Dir;
	uint8_t PullMode;
	uint8_t OutputMode;
	uint8_t InputMode;
	uint8_t SlewRateSel;
	uint8_t DriveStrength;

} GPIO_InitTypeDef;



/**
  * @}
  */

/*** 全局变量声明 **************************************************************/
#ifdef FILE_OUT
/** @defgroup DRV_EPORT_Exported_Variables Exported Variables
  *
  * @{
  */
#endif

/**
  * @}
  */

/*** 函数声明 ******************************************************************/
#ifdef FILE_OUT
/** @defgroup DRV_EPORT_Exported_Functions Exported Functions
  * @{
  */
#endif

  extern void DRV_EPORT_IRQHandler(EPORT_TypeDef *pEport, uint8_t Pinx);
  extern void DRV_EPORT_SetTriggerMode(EPORT_TypeDef *pEport, uint8_t Pins, EPORT_IntModeDef TriggerMode);
  extern void DRV_EPORT_SetDirection(EPORT_TypeDef *pEport, uint8_t Pins, EPORT_DirTypeDef Dir);
  extern void DRV_EPORT_SetIT(EPORT_TypeDef *pEport, uint8_t Pins, FunctionalState Status);
  extern void DRV_EPORT_WritePinsLevel(EPORT_TypeDef *pEport, uint8_t Pins, BitActionTypeDef Level);
  extern uint8_t DRV_EPORT_ReadPinsLevel(EPORT_TypeDef *pEport, uint8_t pins);
  extern uint8_t DRV_EPORT_ReadITFlag(EPORT_TypeDef *pEport, uint8_t pins);
  extern void DRV_EPORT_SetPullUp(EPORT_TypeDef *pEport, uint8_t Pins, FunctionalState Status);
  extern void DRV_EPORT_SetOpenDrain(EPORT_TypeDef *pEport, uint8_t Pins, EPORT_OutputModeTypeDef Mode);

  /**
  *@}
  */
  /**
  *@}
  */
  /**
  *@}
  */

#endif /* __EPORT_DRV_H__ */

  /************************ (C) COPYRIGHT LEVETOP *****END OF FILE*************/
