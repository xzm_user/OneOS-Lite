/**
  ******************************************************************************
             Copyright(c) 2020 Levetop Semiconductor Co., Ltd.
                      All Rights Reserved
  ******************************************************************************
  * @file    clcd_drv.h
  * @author  Product application department
  * @version V1.0
  * @date    2020.07.13
  * @brief   Header file of CLCD DRV module.
  * 
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __CLCD_DRV_H__
#define __CLCD_DRV_H__

#ifdef __cplusplus
extern "C"
{
#endif

/* Includes ------------------------------------------------------------------*/
#include "iomacros.h"
#include "lt776_reg.h"

#ifdef FILE_OUT
/**@defgroup DRV 3 HAL driver
  *
  *@{
  */

/** @defgroup DRV_CLCD
  *
  *@{
  */
#endif
  /*** 宏定义 *******************************************************************/
#ifdef FILE_OUT
/** @defgroup DRV_SPI_Exported_Macros Exported Macros
  *
  * @{
  */
#endif

#define _clcd_disable_int(clcd, int_flg)           _bit_clr(clcd->LCD_IMSC, int_flg)
#define _clcd_enable_int(clcd, int_flg)            _bit_set(clcd->LCD_IMSC, int_flg)

#define _clcd_clr_int(clcd, int_flg)               _bit_set(clcd->LCD_ICR, int_flg)//_bit_clr(clcd->LCD_ICR, int_flg)
#define _clcd_get_int_status(clcd)                 _reg_read(clcd->LCD_MIS)

/*control register*/
#define _clcd_en(clcd)                             _bit_set(clcd->LCD_CONTROL, (LCDPWR | LCDEN))             /**<             */
#define _clcd_dis(clcd)                            _bit_clr(clcd->LCD_CONTROL, (LCDPWR | LCDEN))             /**<              */
 
#define _clcd_set_upbase(clcd, addr)               _reg_write(clcd->LCD_UPBASE, addr)
#define _clcd_get_upbase(clcd)                     _reg_read(clcd->LCD_UPBASE)
#define _clcd_set_lpbase(clcd,addr)                _reg_write(clcd->LCD_LPBASE, addr) 
#define _clcd_get_lpbase(clcd)                     _reg_read(clcd->LCD_LPBASE)
 /**
  *@}
  */

/*** 结构体、枚举变量定义 *****************************************************/
#ifdef FILE_OUT
/** @defgroup DRV_CLCD_Exported_Types Exported Types
  *
  * @{
  */
#endif

/**
*@brief 
*
*/

/**
*@}
*/
/*** 全局变量声明 **************************************************************/
#ifdef FILE_OUT
/** @defgroup DRV_TC_Exported_Variables Exported Variables
  *
  * @{
  */
#endif


/**
  * @}
  */

/*** 函数声明 ******************************************************************/
#ifdef FILE_OUT
/** @defgroup DRV_CLCD_Exported_Functions Exported Functions
  * @{
  */
#endif

extern void DRV_CLCD_InterruptCmd(CLCD_TypeDef *clcd, uint8_t int_flg, FunctionalStateTypeDef status);
extern void DRV_CLCD_En(CLCD_TypeDef *clcd);
extern void DRV_CLCD_Dis(CLCD_TypeDef *clcd);
extern void DRV_CLCD_Cmd(CLCD_TypeDef *clcd, FunctionalStateTypeDef NewState);
extern void DRV_CLCD_SetClkDiv(CLCD_TypeDef *clcd, uint32_t div);
extern void DRV_CLCD_SetControlRegister(CLCD_TypeDef *clcd, uint32_t config);
extern void DRV_CLCD_Deinit(CLCD_TypeDef *clcd);
extern void DRV_CLCD_PaletteConfig(CLCD_TypeDef *clcd, uint32_t *paddr, uint32_t palette_num);
extern void DRV_CLCD_GammaConfig(CLCD_TypeDef *clcd, uint32_t *paddr, uint32_t gamma_num);
extern void DRV_CLCD_SetTiming0(CLCD_TypeDef *clcd, uint8_t hbp, uint8_t hfp, uint8_t hsw, uint8_t ppl);
extern void DRV_CLCD_SetTiming1(CLCD_TypeDef *clcd, uint8_t vbp, uint8_t vfp, uint8_t vsw, uint16_t lpp);
extern void DRV_CLCD_SetTiming2(CLCD_TypeDef *clcd, uint16_t cpl, uint32_t flg);
extern void DRV_CLCD_SetTiming3(CLCD_TypeDef *clcd, uint8_t led, boolean lee);
extern void DRV_CLCD_SetMisc(CLCD_TypeDef *clcd, FunctionalStateTypeDef NewState);
extern void DRV_CLCD_UpdateBaseAddr(uint32_t addr);

/**
* @}
*/

/**
* @}
*/

/**
* @}
*/

  #ifdef __cplusplus
  }
  #endif

  #endif /* __TC_DRV_H */

