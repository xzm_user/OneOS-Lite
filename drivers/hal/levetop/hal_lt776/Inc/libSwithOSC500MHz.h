/**
  *****************************************************************************
  *           Copyright(c) 2021 Levetop Semiconductor Co. Ltd.
  *                    All Rights Reserved
  *****************************************************************************
  * @file    libSwitchOSC500MHz.h
  * @author  Product application department
  * @version V1.0
  * @date    2021.11.01
  * @brief   Header file of SwitchOSC500MHz HAL module.
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef LIBSWITCHOSC500MHZ_H_
#define LIBSWITCHOSC500MHZ_H_

#ifdef __cplusplus
extern "C"
{
#endif

/* Includes ------------------------------------------------------------------*/
//#include "hal_def.h"

typedef enum
{
	HAL_OK = 0x00,
	HAL_ERROR = 0x01,
	HAL_BUSY = 0x02,
	HAL_TIMEOUT = 0x03,

	/*CPM error code*/
	HAL_CPM_ERROR_SYSCLKDIV_OUTRANGE = 0x100, /**< ?????? error */

	/*DMA error code*/
	HAL_DMA_ERROR_TE = 0x110,              /**< Transfer error    */
	HAL_DMA_ERROR_CHANNEL_OUTRANG = 0x111, /**< DMA??????????r    */
} HAL_StatusTypeDef;

extern HAL_StatusTypeDef HAL_CPM_SwitchOSC500MHz(void);


#ifdef __cplusplus
}
#endif

#endif /* SWITCHOSC500MHZ_H_ */

/************************ (C) COPYRIGHT LEVETOP *****END OF FILE****/
