/**
  ******************************************************************************
             Copyright(c) 2020 Levetop Semiconductor Co., Ltd.
                      All Rights Reserved
  ******************************************************************************
  * @file    rtc_drv.h
  * @author  Product application department
  * @version V1.0
  * @date    2020.02.25
  * @brief   Header file of RTC module.
  *
  ******************************************************************************
*/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __RTC_DRV_H
#define __RTC_DRV_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "lt776_reg.h"

#ifdef FILE_OUT
/**@defgroup DRV 3 HAL driver
 *
 *@{
 */

/** @defgroup DRV_RTC RTC
 *
 *@{
 */
#endif
/*** 宏定义 *******************************************************************/
#ifdef FILE_OUT
/** @defgroup DRV_RTC_Exported_Macros Exported Macros
 *
 * @{
 */
#endif

/* PRT1R  */
#define _rtc_prt1r_counter_days_set(val) _reg_write(RTC->PRT1R, (uint16_t)val)

/* PRT2R  */
#define _rtc_prt2r_counter_set(h, m, s)                                                                                \
    _reg_write(RTC->PRT2R, ((h << RTC_PRT2R_HOURS_COUNTER) | (m << RTC_PRT2R_MINUTES_COUNTER) | (s)))

/* PRA1R  */
#define _rtc_pra1r_alarm_day_en               _bit_set(RTC->PRA1R, RTC_PRA1R_ALARM_DAYS_EN)
#define _rtc_pra1r_alarm_day_dis              _bit_clr(RTC->PRA1R, RTC_PRA1R_ALARM_DAYS_EN)
#define _rtc_pra1r_alarm_day_counter_set(val) _reg_modify(RTC->PRA1R, ~RTC_PRA1R_ALARM_DAYS_MASK, val)

/* PRA2R  */
#define _rtc_pra2r_alarm_hour_en  _bit_set(RTC->PRA2R, RTC_PRA2R_ALARM_HOURS_EN)
#define _rtc_pra2r_alarm_hour_dis _bit_clr(RTC->PRA2R, RTC_PRA2R_ALARM_HOURS_EN)
#define _rtc_pra2r_alarm_min_en   _bit_set(RTC->PRA2R, RTC_PRA2R_ALARM_MINUTES_EN)
#define _rtc_pra2r_alarm_min_dis  _bit_clr(RTC->PRA2R, RTC_PRA2R_ALARM_MINUTES_EN)
#define _rtc_pra2r_alarm_sec_en   _bit_set(RTC->PRA2R, RTC_PRA2R_ALARM_SECONDS_EN)
#define _rtc_pra2r_alarm_sec_dis  _bit_clr(RTC->PRA2R, RTC_PRA2R_ALARM_SECONDS_EN)
#define _rtc_pra2r_alarm_hour_counter_set(val)                                                                         \
    _reg_modify(RTC->PRA2R, ~RTC_PRA2R_ALARM_HOURS_MASK, val << RTC_PRA2R_ALARM_HOURS)
#define _rtc_pra2r_alarm_min_counter_set(val)                                                                          \
    _reg_modify(RTC->PRA2R, ~RTC_PRA2R_ALARM_MINUTES_MASK, val << RTC_PRA2R_ALARM_MINUTES)
#define _rtc_pra2r_alarm_sec_counter_set(val) _reg_modify(RTC->PRA2R, ~RTC_PRA2R_ALARM_SECONDS_MASK, val)

/* PRTCR  */
#define _rtc_prtcr_days_counter_get  ((_reg_read(RTC->PRTCR) & RTC_PRTCR_DAYS_COUNTER_MASK) >> RTC_PRTCR_DAYS_COUNTER)
#define _rtc_prtcr_hours_counter_get ((_reg_read(RTC->PRTCR) & RTC_PRTCR_HOURS_COUNTER_MASK) >> RTC_PRTCR_HOURS_COUNTER)
#define _rtc_prtcr_mins_counter_get                                                                                    \
    ((_reg_read(RTC->PRTCR) & RTC_PRTCR_MINUTES_COUNTER_MASK) >> RTC_PRTCR_MINUTES_COUNTER)
#define _rtc_prtcr_secs_counter_get                                                                                    \
    ((_reg_read(RTC->PRTCR) & RTC_PRTCR_SECONDS_COUNTER_MASK) >> RTC_PRTCR_SECONDS_COUNTER)

/* PRCSR  */
#define _rtc_prcsr_get_val              _reg_read(RTC->PRCSR)
#define _rtc_prcsr_day_pulse_ie_en      _bit_set(RTC->PRCSR, RTC_PRCSR_DAY_IEN)
#define _rtc_prcsr_day_pulse_ie_dis     _bit_clr(RTC->PRCSR, RTC_PRCSR_DAY_IEN)
#define _rtc_prcsr_hour_pulse_ie_en     _bit_set(RTC->PRCSR, RTC_PRCSR_HOUR_IEN)
#define _rtc_prcsr_hour_pulse_ie_dis    _bit_clr(RTC->PRCSR, RTC_PRCSR_HOUR_IEN)
#define _rtc_prcsr_min_pulse_ie_en      _bit_set(RTC->PRCSR, RTC_PRCSR_MINUTE_IEN)
#define _rtc_prcsr_min_pulse_ie_dis     _bit_clr(RTC->PRCSR, RTC_PRCSR_MINUTE_IEN)
#define _rtc_prcsr_sec_pulse_ie_en      _bit_set(RTC->PRCSR, RTC_PRCSR_SECOND_IEN)
#define _rtc_prcsr_sec_pulse_ie_dis     _bit_clr(RTC->PRCSR, RTC_PRCSR_SECOND_IEN)
#define _rtc_prcsr_alarm_pulse_ie_en    _bit_set(RTC->PRCSR, RTC_PRCSR_ALARM_IEN)
#define _rtc_prcsr_alarm_pulse_ie_dis   _bit_clr(RTC->PRCSR, RTC_PRCSR_ALARM_IEN)
#define _rtc_prcsr_1khz_pulse_ie_en     _bit_set(RTC->PRCSR, RTC_PRCSR_1KHZ_IEN)
#define _rtc_prcsr_1khz_pulse_ie_dis    _bit_clr(RTC->PRCSR, RTC_PRCSR_1KHZ_IEN)
#define _rtc_prcsr_32khz_pulse_ie_en    _bit_set(RTC->PRCSR, RTC_PRCSR_32KHZ_IEN)
#define _rtc_prcsr_32khz_pulse_ie_dis   _bit_clr(RTC->PRCSR, RTC_PRCSR_32KHZ_IEN)
#define _rtc_prcsr_day_pulse_ie_flag    _bit_get(RTC->PRCSR, RTC_PRCSR_DAY_INTF)
#define _rtc_prcsr_hour_pulse_ie_flag   _bit_get(RTC->PRCSR, RTC_PRCSR_HOUR_INTF)
#define _rtc_prcsr_min_pulse_ie_flag    _bit_get(RTC->PRCSR, RTC_PRCSR_MINUTE_INTF)
#define _rtc_prcsr_sec_pulse_ie_flag    _bit_get(RTC->PRCSR, RTC_PRCSR_SECOND_INTF)
#define _rtc_prcsr_alarm_pulse_ie_flag  _bit_get(RTC->PRCSR, RTC_PRCSR_ALARM_INTF)
#define _rtc_prcsr_day_pulse_clr_flag   _bit_set(RTC->PRCSR, RTC_PRCSR_DAY_INTF)
#define _rtc_prcsr_hour_pulse_clr_flag  _bit_set(RTC->PRCSR, RTC_PRCSR_HOUR_INTF)
#define _rtc_prcsr_min_pulse_clr_flag   _bit_set(RTC->PRCSR, RTC_PRCSR_MINUTE_INTF)
#define _rtc_prcsr_sec_pulse_clr_flag   _bit_set(RTC->PRCSR, RTC_PRCSR_SECOND_INTF)
#define _rtc_prcsr_alarm_pulse_clr_flag _bit_set(RTC->PRCSR, RTC_PRCSR_ALARM_INTF)
#define _rtc_prcsr_1khz_pulse_ie_flag   _bit_get(RTC->PRCSR, RTC_PRCSR_1KHZ_INTF)
#define _rtc_prcsr_32khz_pulse_ie_flag  _bit_get(RTC->PRCSR, RTC_PRCSR_32KHZ_INTF)
#define _rtc_prcsr_ie_type_low_level    _reg_modify(RTC->PRCSR, ~RTC_PRCSR_INT_TYPE_MASK, RTC_PRCSR_LOW_LEVEL_INT)
#define _rtc_prcsr_ie_type_rise_edge    _reg_modify(RTC->PRCSR, ~RTC_PRCSR_INT_TYPE_MASK, RTC_PRCSR_RISEING_EDGE_INT)
#define _rtc_prcsr_ie_type_fall_edge    _reg_modify(RTC->PRCSR, ~RTC_PRCSR_INT_TYPE_MASK, RTC_PRCSR_FALLING_EDGE_INT)
#define _rtc_prcsr_ie_type_rise_fall_edge                                                                              \
    _reg_modify(RTC->PRCSR, ~RTC_PRCSR_INT_TYPE_MASK, RTC_PRCSR_RISING_FALLING_EDGE_INT)
#define _rtc_prcsr_wclk_div(div)      _reg_modify(RTC->PRCSR, ~RTC_PRCSR_WCLK_DIV_MASK, div << 3)
#define _rtc_prcsr_rcnt_wen_en        _bit_set(RTC->PRCSR, RTC_PRCSR_RCNT_WEN)
#define _rtc_prcsr_rcnt_wen_dis       _bit_clr(RTC->PRCSR, RTC_PRCSR_RCNT_WEN)
#define _rtc_prcsr_direct_control_en  _bit_set(RTC->PRCSR, RTC_PRCSR_DIR_EN)
#define _rtc_prcsr_direct_control_dis _bit_clr(RTC->PRCSR, RTC_PRCSR_DIR_EN)

/* PRENR  */
#define _rtc_prenr_rtc_en              _bit_set(RTC->PRENR, RTC_PRENR_RTC_EN)
#define _rtc_prenr_rtc_dis             _bit_clr(RTC->PRENR, RTC_PRENR_RTC_EN)
#define _rtc_prenr_direct_control_en   _bit_set(RTC->PRENR, RTC_PRENR_RTC_EN_DIR)
#define _rtc_prenr_direct_control_dis  _bit_clr(RTC->PRENR, RTC_PRENR_RTC_EN_DIR)
#define _rtc_prenr_rtc_en_rcnt_wen_en  _bit_set(RTC->PRENR, RTC_PRENR_RTC_EN_RCNT_WEN)
#define _rtc_prenr_rtc_en_rcnt_wen_dis _bit_clr(RTC->PRENR, RTC_PRENR_RTC_EN_RCNT_WEN)
#define _rtc_prenr_cntupdate_en_get    _bit_get(RTC->PRENR, RTC_PRENR_RTC_EN_CNTUPDATE_EN)

/* PRKEYR  */
#define _rtc_prkeyr_key_set _reg_write(RTC->PRKEYR, RTC_PRKEYR_KEY)

/**
 * @}
 */

/*** 结构体、枚举变量定义 *****************************************************/
#ifdef FILE_OUT
/** @defgroup DRV_RTC_Exported_Types Exported Types
 *
 * @{
 */
#endif

/**
 * @brief RTC 时间结构体定义
 *
 */
typedef struct
{
    __IO uint16_t days;
    __IO uint8_t  hours;
    __IO uint8_t  minutes;
    __IO uint8_t  seconds;
} RTC_TimeTypeDef;

/**
 * @}
 */

/*** 全局变量声明 **************************************************************/
#ifdef FILE_OUT
/** @defgroup DRV_RTC_Exported_Variables Exported Variables
 *
 * @{
 */
#endif

extern void DRV_RTC_IRQHandler(void);
extern void DRV_RTC_SetTimeCounter(RTC_TimeTypeDef *Time);
extern void DRV_RTC_SetAlarmTimeCounter(RTC_TimeTypeDef *Time);
extern void DRV_RTC_GetTimeCounter(RTC_TimeTypeDef *Time);

/**
 * @}
 */

/*** 函数声明 ******************************************************************/
#ifdef FILE_OUT
/** @defgroup DRV_RTC_Exported_Functions Exported Functions
 * @{
 */
#endif

/**
 * @}
 */

/**
 *@}
 */

/**
 *@}
 */

#ifdef __cplusplus
}
#endif

#endif /* __RTC_DRV_H */
