#include <stdio.h>
#include <stdarg.h>
#include "ls1b_public.h"
#include "ls1b_regs.h"
#include "ls1b_pin.h"
#include "ls1b_uart.h"
#include "ls1b_clock.h"
#include "ls1b.h"

void uart_init(ls1b_uart_info_t *uart_info_p)
{
    void *uart_base = uart_info_p->UARTx;
    unsigned long baudrate_div = 0;

    reg_write_8(0,      uart_base + LS1B_UART_IER_OFFSET);
    reg_write_8(0xc3,   uart_base + LS1B_UART_FCR_OFFSET);
    baudrate_div = clk_get_apb_rate() / 16 / uart_info_p->baudrate;
    reg_write_8(0x80,   uart_base + LS1B_UART_LCR_OFFSET);
    reg_write_8((baudrate_div >> 8) & 0xff, uart_base + LS1B_UART_MSB_OFFSET);
    reg_write_8(baudrate_div & 0xff,        uart_base + LS1B_UART_LSB_OFFSET);
    reg_write_8(0x00,   uart_base + LS1B_UART_LCR_OFFSET);
    reg_write_8(0x03,   uart_base + LS1B_UART_LCR_OFFSET);

    if (TRUE == uart_info_p->rx_enable)
    {
        reg_write_8(IER_IRxE|IER_ILE , uart_base + LS1B_UART_IER_OFFSET);
    }

    return ;
}

BOOL uart_is_transmit_empty(void *uartx)
{
    void *uart_base = uartx;
    unsigned char status = reg_read_8(uart_base + LS1B_UART_LSR_OFFSET);

    if (status & (LS1B_UART_LSR_TE | LS1B_UART_LSR_TFE))
    {
        return TRUE;
    }
    else
    {
        return FALSE;
    }
}

void uart_putc(void *uartx, unsigned char ch)
{
    void *uart_base = uartx;

    while (FALSE == uart_is_transmit_empty(uartx))
        ;

    reg_write_8(ch, uart_base + LS1B_UART_DAT_OFFSET);

    return ;
}

void uart_print(void *uartx, const char *str)
{
    while ('\0' != *str)
    {
        uart_putc(uartx, *str);
        str++;
    }

    return ;
}

