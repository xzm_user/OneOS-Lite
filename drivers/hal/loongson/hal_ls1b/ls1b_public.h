#ifndef __LOONGSON_PUBLIC_H
#define __LOONGSON_PUBLIC_H

#include <stdio.h>

struct callvectors {
    int     (*open) (char *, int, int);
    int     (*close) (int);
    int     (*read) (int, void *, int);
    int     (*write) (int, void *, int);
    long long   (*lseek) (int, long long, int);
    int     (*printf) (const char *, ...);
    void    (*cacheflush) (void);
    char    *(*gets) (char *);
};
#define	myprintf (*callvec->printf)
#define	mygets   (*callvec->gets)
extern struct callvectors *callvec;

#define DIV_ROUND_UP(n, d)      (((n) + (d) - 1) / (d))

typedef enum
{
    FALSE=0, 
    TRUE=1
}BOOL;

void reg_set_one_bit(volatile unsigned int *reg, unsigned int bit);
void reg_clr_one_bit(volatile unsigned int *reg, unsigned int bit);
unsigned int reg_get_bit(volatile unsigned int *reg, unsigned int bit);
void reg_write_8(unsigned char data, volatile unsigned char *addr);
unsigned char reg_read_8(volatile unsigned char *addr);
void reg_write_32(unsigned int data, volatile unsigned int *addr);
unsigned int reg_read_32(volatile unsigned int *addr);
int ls1b_ffs(int x);
int ls1b_fls(int x);
#endif

