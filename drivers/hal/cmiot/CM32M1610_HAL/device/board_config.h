  /*
  ***********************************************************************************
  *
  * COPYRIGHT(c) 2021, China Mobile IOT
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of China Mobile IOT nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ***********************************************************************************
  */

#ifndef __BOARD_CONFIG_H__
#define __BOARD_CONFIG_H__

#include "cm1610.h"
#include "cm_gpio.h"

//broad select
#define CM1124FDdvb_1_0 0
#define CM3015_EVB_1_0  1

#define BOARD_VER  CM1124FDdvb_1_0

#if (BOARD_VER==CM1124FDdvb_1_0)
#define PRINT_PORT      UARTB
#define PRINT_BAUD      115200
#define PRINT_RX_PORT   GPIOA
#define PRINT_RX_PIN    GPIO_Pin_7
#define PRINT_TX_PORT   GPIOA
#define PRINT_TX_PIN    GPIO_Pin_5

#define TEST_PORT       UARTA
#define TEST_BAUD       115200
#define TEST_RX_PORT    GPIOD
#define TEST_RX_PIN     GPIO_Pin_4  // GPIO_28
#define TEST_TX_PORT    GPIOD
#define TEST_TX_PIN     GPIO_Pin_3  // GPIO_27

#define PWM_HZ          5000//5k
#define PWM_TIMx        TIM0
#define PWM_PORT        GPIOB
#define PWM_PIN         GPIO_Pin_0  // GPIO_8

#define LED1_PORT       GPIOC
#define LED1_PIN        GPIO_Pin_7

#define ADC6_PORT       GPIOD
#define ADC6_PIN        GPIO_Pin_0

/////////////////////////////////////////////////
/*IIC GPIO CONFIG */
#define IIC_CLK_PORT         GPIOB
#define IIC_CLK_PIN          GPIO_Pin_6
#define IIC_SDA_PORT         GPIOC
#define IIC_SDA_PIN          GPIO_Pin_3

#elif (BOARD_VER==CM3015_EVB_1_0)
#define PRINT_PORT      UARTB
#define PRINT_BAUD      115200
#define PRINT_RX_PORT   GPIOA
#define PRINT_RX_PIN    GPIO_Pin_5
#define PRINT_TX_PORT   GPIOA
#define PRINT_TX_PIN    GPIO_Pin_7

#define TEST_PORT       UARTA
#define TEST_BAUD       115200
#define TEST_RX_PORT    GPIOD
#define TEST_RX_PIN     GPIO_Pin_4  // GPIO_28
#define TEST_TX_PORT    GPIOD
#define TEST_TX_PIN     GPIO_Pin_3  // GPIO_27

#define PWM_HZ          5000//5k
#define PWM_TIMx        TIM0
#define PWM_PORT        GPIOB
#define PWM_PIN         GPIO_Pin_0  // GPIO_8

#define LED1_PORT       GPIOC
#define LED1_PIN        GPIO_Pin_7

#define ADC6_PORT       GPIOD
#define ADC6_PIN        GPIO_Pin_0

/////////////////////////////////////////////////
/*IIC GPIO CONFIG */
#define IIC_CLK_PORT         GPIOB
#define IIC_CLK_PIN          GPIO_Pin_6
#define IIC_SDA_PORT         GPIOC
#define IIC_SDA_PIN          GPIO_Pin_3
#endif

#endif
