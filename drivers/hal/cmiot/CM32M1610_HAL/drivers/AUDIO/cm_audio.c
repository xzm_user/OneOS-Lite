  /*
  ***********************************************************************************
  *
  * COPYRIGHT(c) 2021, China Mobile IOT
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of China Mobile IOT nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ***********************************************************************************
  */
#include "cm_audio.h"
#include <stdlib.h>
#include "cm_sysctrl.h"


//#define ADC_SAMPLE_16K
#define	HFP_SPEAKER_MAX_VLM_NEW                 	0x20
#define ADAC_SETTING_ANA_VOL_LIMIT (12) // 12->-18dBm
#define ADAC_SETTING_ANA_VOL_TARGET (3)
#define ADAC_SETTING_DIGITAL_VOL_LIMIT (0x80) // use for calibration

//16k
const uint8_t coef_adc_table[336] =
{
0x06,0x00,0x00,0xb4,0xff,0xff,0xa1,0xfe,0xff,0x59,0xfd,0xff,0x62,0xfe,0xff,0xa4,0x04,0x00,0xad,0x0e,0x00,0xcf,0x12,0x00,
0x60,0x03,0x00,0x3f,0xdd,0xff,0x9a,0xb6,0xff,0xa8,0xbc,0xff,0x47,0x18,0x00,0x7a,0xc8,0x00,0x04,0x92,0x01,0x1f,0x19,0x02,
0x2c,0x00,0x00,0xaa,0x00,0x00,0x45,0x00,0x00,0x2e,0x00,0x00,0xcd,0xff,0xff,0x6d,0xff,0xff,0x2e,0xff,0xff,0x3e,0xff,0xff,
0xac,0xff,0xff,0x62,0x00,0x00,0x20,0x01,0x00,0x8f,0x01,0x00,0x6a,0x01,0x00,0x99,0x00,0x00,0x51,0xff,0xff,0x08,0xfe,0xff,
0x4f,0xfd,0xff,0x99,0xfd,0xff,0xff,0xfe,0xff,0x20,0x01,0x00,0x36,0x03,0x00,0x57,0x04,0x00,0xd5,0x03,0x00,0x97,0x01,0x00,
0x3e,0xfe,0xff,0x06,0xfb,0xff,0x53,0xf9,0xff,0x26,0xfa,0xff,0x97,0xfd,0xff,0xa4,0x02,0x00,0x71,0x07,0x00,0xec,0x09,0x00,
0xa7,0x08,0x00,0x8d,0x03,0x00,0x23,0xfc,0xff,0x23,0xf5,0xff,0x8e,0xf1,0xff,0x6e,0xf3,0xff,0xd9,0xfa,0xff,0x96,0x05,0x00,
0xb4,0x0f,0x00,0xe3,0x14,0x00,0x32,0x12,0x00,0x7a,0x07,0x00,0xe3,0xf7,0xff,0x18,0xe9,0xff,0x60,0xe1,0xff,0x27,0xe5,0xff,
0xdf,0xf4,0xff,0x2d,0x0c,0x00,0xcf,0x22,0x00,0x3a,0x2f,0x00,0x28,0x2a,0x00,0xde,0x11,0x00,0xf2,0xeb,0xff,0xb5,0xc4,0xff,
0x1d,0xac,0xff,0x03,0xb1,0xff,0x18,0xdc,0xff,0x3a,0x2c,0x00,0x8b,0x95,0x00,0xbc,0x03,0x01,0x15,0x5f,0x01,0xce,0x92,0x01,
0x06,0xff,0xff,0x0d,0xfe,0xff,0xeb,0x01,0x00,0xe0,0x01,0x00,0x83,0xfd,0xff,0xe9,0xfc,0xff,0xcb,0x03,0x00,0x7a,0x04,0x00,
0x99,0xfa,0xff,0xb0,0xf9,0xff,0x7d,0x07,0x00,0xa7,0x08,0x00,0xde,0xf5,0xff,0x62,0xf4,0xff,0x7b,0x0d,0x00,0x5f,0x0f,0x00,
0x46,0xee,0xff,0xd6,0xeb,0xff,0x2e,0x17,0x00,0x63,0x1a,0x00,0xa4,0xe1,0xff,0x44,0xdd,0xff,0x37,0x28,0x00,0x8f,0x2e,0x00,
0x45,0xc9,0xff,0x1e,0xbf,0xff,0xca,0x4e,0x00,0x28,0x62,0x00,0x83,0x7f,0xff,0x0e,0x4a,0xff,0xf6,0x31,0x01,0x5f,0x99,0x03
};

//48K
const uint8_t coef_dac_table[192] =
{
0x5d,0x02,0x00,0x73,0x07,0x00,0x92,0x0f,0x00,0x44,0x18,0x00,0xbe,0x1c,0x00,0xbb,0x17,0x00,0xf3,0x06,0x00,0x56,0xee,0xff,
0x81,0xd8,0xff,0xcb,0xd2,0xff,0xcb,0xe5,0xff,0x0c,0x0e,0x00,0x0b,0x3a,0x00,0xaf,0x50,0x00,0xbc,0x3e,0x00,0x96,0x04,0x00,
0x42,0xbb,0xff,0xdc,0x8a,0xff,0x50,0x96,0xff,0xc4,0xe1,0xff,0x0a,0x4c,0x00,0x0f,0x9a,0x00,0x31,0x97,0x00,0xb8,0x37,0x00,
0xde,0xa7,0xff,0xd5,0x39,0xff,0xf3,0x37,0xff,0xa8,0xb3,0xff,0x84,0x70,0x00,0x43,0xfe,0x00,0x49,0xf9,0x00,0x23,0x4e,0x00,
0x21,0x54,0xff,0x0d,0xa6,0xfe,0x1a,0xc7,0xfe,0xe8,0xc6,0xff,0x75,0x21,0x01,0x72,0xfb,0x01,0x5c,0xa4,0x01,0x80,0x15,0x00,
0x56,0x19,0xfe,0x1d,0xf0,0xfc,0xf4,0x93,0xfd,0x2b,0x04,0x00,0xe3,0x0e,0x03,0xbc,0xd5,0x04,0x60,0xde,0x03,0x57,0x1d,0x00,
0xc7,0x50,0xfb,0xb2,0x4d,0xf8,0x23,0x77,0xf9,0xdf,0x24,0xff,0xa6,0xef,0x06,0xf9,0x7e,0x0c,0x2b,0xaf,0x0b,0x3b,0x18,0x03,
0x83,0x9b,0xf5,0x6f,0xee,0xe9,0x28,0x06,0xe8,0xe4,0x63,0xf5,0x01,0xf8,0x11,0xba,0x37,0x37,0x8d,0x12,0x5a,0x60,0x19,0x6f
};


//44.1k
const uint8_t coef_dac_table_44k[192] =
{
0x90,0xfb,0xff,0x94,0xf3,0xff,0x94,0xe7,0xff,0x58,0xdb,0xff,0x53,0xd5,0xff,0x60,0xdc,0xff,0xae,0xf3,0xff,0xff,0x16,0x00,
0xbe,0x39,0x00,0xd1,0x4a,0x00,0x9f,0x3c,0x00,0x03,0x0e,0x00,0x08,0xcf,0xff,0x3a,0x9d,0xff,0xfa,0x96,0xff,0xa8,0xca,0xff,
0x31,0x2a,0x00,0x9a,0x8b,0x00,0x97,0xb9,0x00,0xd0,0x8e,0x00,0xb1,0x0e,0x00,0xf2,0x6c,0xff,0x71,0xfb,0xfe,0xe9,0x02,0xff,
0x43,0x98,0xff,0x01,0x86,0x00,0xe8,0x5a,0x01,0x2c,0x9d,0x01,0x99,0x0c,0x01,0x5b,0xd1,0xff,0x57,0x79,0xfe,0x47,0xbc,0xfd,
0x83,0x1d,0xfe,0xe5,0x99,0xff,0xc6,0x91,0x01,0xd8,0x05,0x03,0x7e,0x13,0x03,0x8f,0x76,0x01,0xa7,0xc9,0xfe,0x97,0x56,0xfc,
0xa1,0x7e,0xfb,0xaf,0xfc,0xfc,0x61,0x63,0x00,0x96,0x27,0x04,0x86,0x48,0x06,0x89,0x53,0x05,0xc4,0x45,0x01,0x88,0xcc,0xfb,
0xa7,0xa7,0xf7,0x93,0x5d,0xf7,0xd4,0xd4,0xfb,0x4b,0x8b,0x03,0x6f,0xee,0x0a,0xa0,0xd7,0x0d,0x3c,0x9c,0x09,0x2b,0xc2,0xfe,
0xc6,0x74,0xf1,0x58,0x51,0xe8,0x56,0xde,0xe9,0xa6,0xa8,0xf9,0xe6,0x45,0x16,0xae,0x31,0x39,0x52,0xbe,0x58,0x2c,0x6f,0x6b
};



//32k#24k#16k#12k#8k
const uint8_t coef_dac_table_8k[192] =
{
0x1e,0x00,0x00,0x80,0xfe,0xff,0xf8,0xfa,0xff,0x9c,0xf4,0xff,0xf0,0xeb,0xff,0x18,0xe3,0xff,0xd0,0xdd,0xff,0x64,0xe0,0xff,
0xc7,0xed,0xff,0x69,0x05,0x00,0xd4,0x21,0x00,0x2c,0x39,0x00,0x10,0x40,0x00,0x65,0x2e,0x00,0x45,0x04,0x00,0xc5,0xcc,0xff,
0x66,0x9c,0xff,0x8b,0x8a,0xff,0x9a,0xa7,0xff,0x6f,0xf3,0xff,0xd4,0x58,0x00,0x85,0xb1,0x00,0x26,0xd3,0x00,0x7d,0xa1,0x00,
0xb4,0x1e,0x00,0xa9,0x71,0xff,0xf6,0xdc,0xfe,0x41,0xa7,0xfe,0x73,0xfc,0xfe,0x36,0xd4,0xff,0xa7,0xea,0x00,0x43,0xd2,0x01,
0x3a,0x1c,0x02,0x72,0x89,0x01,0xdc,0x2f,0x00,0x1f,0x81,0xfe,0xc0,0x29,0xfd,0x04,0xd0,0xfc,0x93,0xc9,0xfd,0xf0,0x18,0x00,
0xcc,0x70,0x02,0xb1,0x5e,0x04,0x3f,0xbd,0x04,0x57,0x1b,0x03,0x99,0xd2,0xff,0x9c,0x04,0xfc,0x6f,0x41,0xf9,0xf2,0xeb,0xf8,
0x1e,0xa8,0xfb,0x4c,0xd4,0x00,0xdb,0xaf,0x06,0x49,0xd1,0x0a,0xb9,0x0b,0x0b,0x1b,0x62,0x06,0xc7,0xad,0xfd,0x4e,0xac,0xf3,
0x43,0x59,0xec,0x49,0xb9,0xeb,0x06,0x75,0xf4,0xc1,0xbd,0x06,0x23,0xe6,0x1f,0x43,0xe2,0x3a,0x1c,0x95,0x51,0x0b,0x89,0x5e
};


const uint8_t coef_voice_table[82] =
{
0x0c,0x00,0x12,0x00,0x1b,0x00,0x24,0x00,0x29,0x00,0x28,0x00,0x1d,0x00,0x07,0x00,
0xe4,0xff,0xb9,0xff,0x8a,0xff,0x5f,0xff,0x42,0xff,0x3c,0xff,0x53,0xff,0x8c,0xff,
0xe4,0xff,0x50,0x00,0xc2,0x00,0x25,0x01,0x63,0x01,0x68,0x01,0x27,0x01,0x9d,0x00,
0xd5,0xff,0xe7,0xfe,0xf7,0xfd,0x33,0xfd,0xcb,0xfc,0xea,0xfc,0xaf,0xfd,0x27,0xff,
0x49,0x01,0xf3,0x03,0xee,0x06,0xf6,0x09,0xbd,0x0c,0xf9,0x0e,0x6b,0x10,0xeb,0x10,
0x00,0x00
};

#if 0
const uint8_t coef_eq_table[153] =
{
0x00,0x00,0x10,0xc8,0x00,0x20,0x3e,0x2e,0xc0,0x1e,0xd1,0x1f,0xc4,0xd1,0x3f,0x1b,0x2e,0xe0,0x13,0x05,0x20,0x48,0x41,0xc0,
0x33,0xba,0x1f,0xb8,0xbe,0x3f,0xba,0x40,0xe0,0xb6,0xda,0x1f,0x99,0xb5,0xc0,0xdc,0x71,0x1f,0x67,0x4a,0x3f,0x6e,0xb3,0xe0,
0xf7,0xdc,0x1f,0x07,0x3e,0xc1,0x9d,0xed,0x1e,0xf9,0xc1,0x3e,0x6c,0x35,0xe1,0x2f,0xf7,0x1f,0x41,0x43,0xc2,0x7a,0xe7,0x1d,
0xbf,0xbc,0x3d,0x57,0x21,0xe2,0x66,0x5e,0x20,0x1d,0x40,0xc4,0x79,0xe5,0x1b,0xe3,0xbf,0x3b,0x20,0xbc,0xe3,0x3f,0x7e,0x21,
0xd7,0x71,0xc8,0x9f,0x05,0x18,0x29,0x8e,0x37,0x22,0x7c,0xe6,0x50,0x01,0x22,0x0d,0x5b,0xd3,0xb4,0x8b,0x11,0xf3,0xa4,0x2c,
0xfd,0x72,0xec,0x4c,0x3e,0x20,0x42,0xf2,0xeb,0x2f,0xdd,0x07,0xbe,0x0d,0x14,0x85,0xe4,0xf7,0x0f,0x60,0x23,0x67,0x35,0x13,
0x39,0xee,0x07,0xb3,0xc0,0xe9,0x9e,0xbb,0xf7
};
#endif

const uint8_t *coef_dac_table_addr=NULL;

void Audio_EqDisable(void)
{
	HWCOR(CORE_SBC_CTRL, BIT_6);
}
void Audio_DACClkOff(void)
{
	HWORW(CORE_CLKOFF, REG_CLOCK_OFF_AUDIO_DAC);
	HWORW(CORE_CLKOFF, REG_CLOCK_OFF_VOICE_FILTER);
}

void Audio_DacStop(void)
{
	uint8_t tmp = HREAD(CORE_DAC_CTRL);
	//DEBUG_LOG(LOG_LEVEL_CORE, "UI" ,"Audio_DacInit: 0x%04X", LOG_POINT_A60A, tmp);

	//1. Disable DMA to pause audio data streaming
	HWCOR(CORE_DAC_CTRL, BIT_0);//DAC_ENABLE
	HWRITE(mem_dac_clk, 0);
	// sleep a wait//500us
	delay_us(500);

	//2. Wait Tramp for ramp down to finish
	//Tramp= (last_data_value/(left/right_down_step +1)*(1/48)) mS, if sample rate is 48K. The maximum time is 40ms if
	// down_step is '0xF' and sample rate is 48K. Use 8K sample rate the maximum time is 6*40=240ms.
	if((tmp & (BIT_0)) != 0)//DAC_ENABLE
	{
		Audio_DacWaitRampDown();
	}

	//Mute voice
	Audio_AdacPrepareSoftMute();

	//HWRITE(CORE_DAC_LVOL, 0);
	//HWRITE(CORE_DAC_RVOL, 0);
	Audio_EqDisable();

	Audio_ClosePaDelay();
}
void Audio_AdcStop(void)
{
	uint8_t temp;

	Audio_AnaStopAdcWork();

	//clear adc_fir3_scale
	temp = HREAD(CORE_TONE_CTRL);
	temp &= 0xf0;
	HWRITE(CORE_TONE_CTRL, temp);

	HWRITE(mem_cvsd_start_send, 0);
	HWRITE(mem_msbc_adc_start_send, 0);

	HWRITE(CORE_ADCD_DELAY, 0);
	HWRITE(CORE_ADCD_CTRL, 0);
}

void Audio_CvsdClkEnable(void)
{
	HWCORW(CORE_CLKOFF, REG_CLOCK_OFF_VOICE_FILTER);
	HWCORW(CORE_CLKOFF, REG_CLOCK_OFF_SBC);// Only need for 64K to 48K farrow filter init(8K -> 64K -> 48K)
	HWCORW(CORE_CLKOFF, REG_CLOCK_OFF_MRAM);
}
void Audio_CvsdClkDisable(void)
{
}

void Audio_CvsdWaitCvsdFilterClrDone(void)
{
	while((HREAD(CORE_PERF_STATUS) & BIT_7) == 0)
	{

	}
}
void Audio_CvsdCoefInit(void)
{
	uint16_t temp_16, i;

	HWOR(CORE_CVSD_CTRL, BIT_1);//CVSD_FILTER_DELAY_CHAIN_CLEAN_ENABLE

	HWRITE(CORE_COEF_CTRL, 0x40);
	for(i = 0; i < 41; i ++)
	{
		temp_16 = (coef_voice_table[i*2]|(coef_voice_table[i*2+1]<<8));
		HWRITE24BIT(CORE_COEF_WDATA, temp_16);

		HWRITE(CORE_COEF_CTRL, 0x40);
		HWRITE(CORE_COEF_CTRL, 0xC0);
	}

	HWRITE(CORE_COEF_CTRL, 0x00);

	Audio_CvsdWaitCvsdFilterClrDone();
}

void Audio_CvsdStart(void)
{
	uint8_t tmp = HREAD(CORE_CVSD_CTRL);
	tmp |= BIT_4;//CVSD_DECODE_8K_MODE_ENABLE
#ifdef FUNCTION_CVSD_OVER_SOFT_8KTO48K
	tmp &= ~(BIT_5);//CVSD_DECODE_48K_MODE_ENABLE
	tmp &= ~(BIT_2);//CVSD_64TO48K_ENABLE
#else
	tmp |= BIT_5;//CVSD_DECODE_48K_MODE_ENABLE
	tmp |= BIT_2;//CVSD_64TO48K_ENABLE
#endif
	tmp |= BIT_7;//CVSD_DMA_ENABLE
	HWRITE(CORE_CVSD_CTRL, tmp);
}
void Audio_CvsdStop(void)
{
	HWRITE(CORE_CVSD_CTRL, 0);
}
uint16_t gDacBufOffset = 0;
void Audio_CvsdInitDac(void)
{
	uint32_t dacStartAddr = HREADW(CORE_PCMOUT_48K_SADDR);
	uint16_t dacClk = 48;
	uint16_t dacBufSize = (HREADW(CORE_CVSD_BUF_LEN) + 1) * 12;//6*2=12  6 means 8K to 48K
	ADCRAMUSEDEV isMram = ((HREAD(CORE_CVSD_CTRL) & BIT_6) != 0)? USE_M0_MRAM:USE_BT_SRAM;//CVSD_SELECT_MRAM
#ifdef FUNCTION_CVSD_OVER_SOFT_8KTO48K
	dacStartAddr = reg_map_m0(dacStartAddr);
	//dacStartAddr = reg_map_m0(0x7800);
	dacBufSize = 0x2000;
	gDacBufOffset = 0;
	Filter8KTo48KInit((short *)dacStartAddr, dacBufSize/2);//uint8_t to short
#endif

#ifdef TEST_FUNCTION_USE_SPECIAL_VOICE_ENABLE
	extern void App_TestForceVoice(void);
	App_TestForceVoice();
#else
	Audio_DacInitVpCall(dacClk, isMram
		, dacStartAddr, dacBufSize);
#endif
}
void Audio_CvsdInit(void)
{
	Audio_CvsdInitProcess();
	Audio_CvsdInitDac();
}
void Audio_CvsdInitProcess(void)
{
	uint16_t cvsdInBufferAddr = HREADW(mem_param_cvsd_in_buffer_addr);
	uint16_t cvsdInBufferEndAddr = cvsdInBufferAddr + CVSD_IN_MAX_LEN;

//	DEBUG_LOG(LOG_LEVEL_CORE, "UI" ,"Audio_DacInit: 0x%04X", LOG_POINT_A630, HREAD(CORE_CVSD_CTRL));

	// Enable clock
	Audio_CvsdClkEnable();

	// Init cvsd out, pcm in buffer, and init adc
	Audio_AdcCvsdInit();

	//cvsd filter enable, it will reuse sbc resource. 1: for cvsd; 0: for sbc/msbc
	HWOR(CORE_SBC_CACHE_CFG2, BIT_7);

	// Init cvsd in buffer addr
	HWRITEW(CORE_CVSDIN_SADDR, cvsdInBufferAddr);
	HWRITEW(mem_cvsd_in_addr_ptr, cvsdInBufferAddr);

	HWRITEW(mem_cvsd_in_addr_end, cvsdInBufferEndAddr);

	if(HREAD(mem_param_cvsd_in_buffer_addr_mram_flag) != 0)
	{
		HWOR(CORE_CVSD_CTRL1, BIT_2);//DECODE_CVSD_IN_MRAM_SEL
	}
	else
	{
		HWCOR(CORE_CVSD_CTRL1, BIT_2);//DECODE_CVSD_IN_MRAM_SEL
	}

	// In cvsd, it have 8K and 48K out
	HWRITEW(CORE_PCMOUT_SADDR, HREADW(mem_param_pcm_8k_out_buffer_addr));

	// mram setting
	if(HREAD(mem_param_pcm_8k_out_buffer_addr_mram_flag) != 0)
	{
		HWOR(CORE_CVSD_CTRL1, BIT_1);//DECODE_8KPCM_OUT_MRAM_SEL
	}
	else
	{
		HWCOR(CORE_CVSD_CTRL1, BIT_1);//DECODE_8KPCM_OUT_MRAM_SEL
	}

	HWRITEW(CORE_PCMOUT_48K_SADDR, HREADW(mem_param_pcm_48k_out_buffer_addr));

	// mram setting
	if(HREAD(mem_param_pcm_48k_out_buffer_addr_mram_flag) != 0)
	{
		HWOR(CORE_CVSD_CTRL, BIT_6);//CVSD_SELECT_MRAM
	}
	else
	{
		HWCOR(CORE_CVSD_CTRL, BIT_6);//CVSD_SELECT_MRAM
	}

	HWRITEW(CORE_CVSD_BUF_LEN, CVSD_IN_MAX_LEN - 1);

	// clear and init cvsd filter
	Audio_CvsdCoefInit();

 	// cvsd encode and decode will work with the length
 	HWRITEW(CORE_CVSD_GRP_LEN, HREADW(mem_sco_rx_packet_len));

	// Only need for 64K to 48K farrow filter init(8K -> 64K -> 48K), after init, not need any more
	HWORW(CORE_CLKOFF, REG_CLOCK_OFF_SBC);

	Audio_CvsdStart();
}
void Audio_AdcCvsdInit(void)
{
	uint16_t adcStartAddr, adcBufferSize, cvsdOutBufferAddr;
	BOOL adcMramFlag, cvsdOutMramFlag;
	adcStartAddr = HREADW(mem_param_pcm_in_buffer_addr);
	adcBufferSize = CVSD_IN_MAX_LEN * 2;
	adcMramFlag = HREAD(mem_param_pcm_in_buffer_addr_mram_flag);

	//DEBUG_LOG(LOG_LEVEL_CORE, "UI" ,"Audio_DacInit: 0x%04X", LOG_POINT_A631, adcStartAddr);

	//
#ifdef FUNCTION_OAL_MODE
	if(Oal_CheckIsFunctionEnable())
	{
		Oal_Start(OALADCBUFFBCMALL);
	}
	else
#endif
	{
		Audio_AdcInit(adcStartAddr, adcBufferSize, adcMramFlag);
	}

	// PCM In Setting
	HWRITEW(CORE_PCMIN_SADDR, adcStartAddr);
	HWRITEW(mem_pcm_in_addr_end, (adcStartAddr + adcBufferSize - 1));

	if(adcMramFlag)
	{
		HWOR(CORE_CVSD_CTRL1, BIT_0);//ENCODE_8KPCM_IN_MRAM_SEL
	}
	else
	{
		HWCOR(CORE_CVSD_CTRL1, BIT_0);//ENCODE_8KPCM_IN_MRAM_SEL
	}

	cvsdOutBufferAddr = HREADW(mem_param_cvsd_out_buffer_addr);

	HWRITEW(CORE_CVSDOUT_SADDR, cvsdOutBufferAddr);
	HWRITEW(mem_cvsd_out_addr_ptr, cvsdOutBufferAddr);
	HWRITEW(mem_cvsd_out_addr_end, cvsdOutBufferAddr + CVSD_IN_MAX_LEN);
	cvsdOutMramFlag = HREAD(mem_param_cvsd_out_buffer_addr_mram_flag);

	if(cvsdOutMramFlag)
	{
		HWOR(CORE_CVSD_CTRL1, BIT_3);//ENCODE_CVSD_OUT_MRAM_SEL
	}
	else
	{
		HWCOR(CORE_CVSD_CTRL1, BIT_3);//ENCODE_CVSD_OUT_MRAM_SEL
	}

	HWRITEW(CORE_CVSD_BUF_LEN, CVSD_IN_MAX_LEN  - 1);
#ifndef FUNCTION_OAL_MODE
	NrAec_Init();
#endif
}
void NrAec_Init(void)
{
#ifdef FUNCTION_CONTROL_AECNR_ENABLE
	nsProcess_init();       //ns init
	gpAECStructVariable->gechoMode =4;
	gpAECStructVariable->gAecMobilePtr = AECProcess_init(8000,64,gpAECStructVariable->gechoMode);    //AEC init
#endif
}
void Audio_AdcInit(uint16_t adcStartAddr, uint16_t adcBufferSize, BOOL adcMramFlag)
{
	uint8_t temp;
	uint16_t adcEndAddr = (adcStartAddr + adcBufferSize - 1);
	Audio_AnaStartAdcWork();
	//Step1: init adc
	Audio_AdcFilterInit();

	HWRITEW(mem_adc_start_addr, adcStartAddr);
	HWRITEW(mem_adc_buf_size, adcBufferSize);

	//Step2: init adc buffer info
	HWRITEW(CORE_ADCD_SADDR, adcStartAddr);

	HWRITEW(CORE_ADCD_EADDR, adcEndAddr);

	// ADC SCAL setting
	HWRITE(CORE_ADC_FILTER_CTRL, 0x28);
	//HWRITE(CORE_ADC_FILTER_CTRL, 0x38);
	temp = HREAD(CORE_TONE_CTRL);
	temp &= 0xf0;
	temp |= 0x0e;
	//temp |= 0x0f;
	HWRITE(CORE_TONE_CTRL, temp);

	Audio_AdcStart8K(adcMramFlag);
}

void Audio_AdcDmaEnable(void)
{
	HWOR(CORE_ADCD_CTRL, BIT_7);
}
BOOL Audio_CheckAdcDmaEnable(void)
{
	return (HREAD(CORE_ADCD_CTRL) & BIT_7) != 0;
}
void Audio_AdcStart8K(BOOL adcMramFlag)
{
	HWRITE(CORE_ADCD_DELAY, 0x80);
	HWRITE(CORE_ADCD_CTRL, 0);

	if(adcMramFlag)
	{
		HWOR(CORE_ADCD_CTRL, BIT_6);
	}
	else
	{
		HWCOR(CORE_ADCD_CTRL, BIT_6);
	}
#ifdef ADC_SAMPLE_16K
	HWOR(CORE_ADCD_CTRL, BIT_4);
#endif
	Audio_AdcDmaEnable();

	// Start voice filter clock
	HWCORW(CORE_CLKOFF, REG_CLOCK_OFF_VOICE_FILTER);
}
void Audio_AdcFilterInit(void)
{
	uint8_t i;
	uint32_t temp_32;
	HWRITE(CORE_COEF_CTRL, 0x10);
	for(i = 0; i < 112; i ++)
	{
		temp_32 = (coef_adc_table[i*3]|(coef_adc_table[i*3+1]<<8)|coef_adc_table[i*3+2]<<16);
		HWRITE24BIT(CORE_COEF_WDATA, temp_32);

		HWRITE(CORE_COEF_CTRL, 0x30);
		HWRITE(CORE_COEF_CTRL, 0x10);
	}
	for(i = 0; i < 224; i ++)
	{
		HWRITE24BIT(CORE_COEF_WDATA, 0);

		HWRITE(CORE_COEF_CTRL, 0x30);
		HWRITE(CORE_COEF_CTRL, 0x10);
	}
	HWRITE(CORE_COEF_CTRL, 0x00);
}
void Audio_SbcBusyWait(void)
{
	while(((HREAD(CORE_MISC_STATUS)) & BIT_4) != 0);
}
void Audio_SbcClkOff(void)
{
	HWORW(CORE_CLKOFF, REG_CLOCK_OFF_SBC);
}
void Audio_SbcStop(void)
{
	uint8_t temp;
	HWRITE(CORE_SBC_CTRL2, 0);
	HWRITEW(CORE_SBC_SWP, 0);
	HWRITE(CORE_SBC_CLR, 0xff);
	delay_us(1);
	HWRITE(CORE_SBC_CLR, 0);
	Audio_SbcBusyWait();

	temp = HREAD(CORE_SBC_CTRL);
	temp &= 0x80;
	HWRITE(CORE_SBC_CTRL, temp);

	temp = HREAD(CORE_TONE_CTRL);
	temp &= 0x8f;
	HWRITE(CORE_TONE_CTRL, temp);

	Audio_SbcClkOff();
}
void Audio_mSbcStop(void)
{
	Audio_SbcStop();
}
void Audio_DacAnaClkEnable(void)
{
	HWORW(CORE_DAC_SEL2, BIT_10);//DAC_ANA_CLK_EN
}
void Audio_DacAnaClkDisable(void)
{
	HWCORW(CORE_DAC_SEL2, BIT_10);//DAC_ANA_CLK_EN
}
void Audio_DacCloseL(void)
{
	HWCOR(CORE_DAC_SEL2, BIT_2);//core_dac_sel2_clk_en_l
}
void Audio_DacSetL(uint8_t din_sel)
{
	uint8_t temp;
	if(din_sel == DIN_SEL_L_CLOSE)
	{
		Audio_DacCloseL();
	}
	else
	{
		temp = HREAD(CORE_DAC_SEL2);
		temp |= BIT_2;//core_dac_sel2_clk_en_l
		temp &= 0xfc;
		temp |= din_sel;
		HWRITE(CORE_DAC_SEL2, temp);
	}
}
void Audio_DacCloseR(void)
{
	HWCOR(CORE_DAC_SEL2, BIT_6);//core_dac_sel2_clk_en_r
}
void Audio_DacSetR(uint8_t din_sel)
{
	uint8_t temp;
	if(din_sel == DIN_SEL_R_CLOSE)
	{
		Audio_DacCloseR();
	}
	else
	{
		temp = HREAD(CORE_DAC_SEL2);
		temp |= BIT_6;//core_dac_sel2_clk_en_r
		temp &= 0xcf;
		temp |= din_sel;
		HWRITE(CORE_DAC_SEL2, temp);
	}
}
void Audio_DacStero(void)
{
	HWCOR(CORE_DAC_SEL, BIT_0);
}
void Audio_DacMono(void)
{
	HWOR(CORE_DAC_SEL, BIT_0);
}
void Audio_DacMonoStero(uint8_t mono_flag)
{
	if(mono_flag == 0)
	{
		Audio_DacStero();
	}
	else
	{
		Audio_DacMono();
	}
}
void Audio_DacFilter(void)
{
	HWOR(CORE_DAC_CTRL, BIT_4);//DAC_INIT_FILTER
}
void Audio_DacFilterDisable(void)
{
	HWCOR(CORE_DAC_CTRL, BIT_4);//DAC_INIT_FILTER
}
void Audio_DacSdm(void)
{
	HWOR(CORE_DAC_CTRL, BIT_5);//DAC_SDM
}
void Audio_DacSdmDisable(void)
{
	HWCOR(CORE_DAC_CTRL, BIT_5);//DAC_SDM
}
void Audio_DlychainDacInit(void)
{
	uint8_t i;
	HWRITE24BIT(CORE_COEF_WDATA, 0);
	HWCOR(CORE_COEF_CTRL, BIT_0);
	HWOR(CORE_COEF_CTRL, BIT_0);
	for(i = 0; i < 32; i ++)
	{
		HWOR(CORE_COEF_CTRL, BIT_1);
		delay_us(5);//5US
		HWCOR(CORE_COEF_CTRL, BIT_1);
		delay_us(5);//5US
	}
	HWCOR(CORE_COEF_CTRL, BIT_0);
}
uint32_t Audio_GetCoefStartAddr(uint16_t offset)
{
	return HREAD24BIT(mem_coef_base_addr) + HREAD24BIT(mem_storage_start_addr) + offset;
}

void Audio_LoadDac8kCoef(void)
{
    coef_dac_table_addr=coef_dac_table_8k;
}
void Audio_LoadDac16kCoef(void)
{
	Audio_LoadDac8kCoef();
}
void Audio_LoadDac48kCoef(void)
{
    coef_dac_table_addr=coef_dac_table;
}
void Audio_LoadDac44kCoef(void)
{
    coef_dac_table_addr=coef_dac_table_44k;
}
void Audio_DacAnalogSel48k(void)
{
	HWOR(RF_RX_IB_LNA, BIT_0);
}
void Audio_DacAnalogSel44k(void)
{
	HWCOR(RF_RX_IB_LNA, BIT_0);
}
void Audio_DacSamplefreqSel8k(void)
{
	Audio_LoadDac8kCoef();
	Audio_DacAnalogSel48k();
}
void Audio_DacSamplefreqSel16k(void)
{
	Audio_LoadDac16kCoef();
	Audio_DacAnalogSel48k();
}
void Audio_DacSamplefreqSel48k(void)
{
	Audio_LoadDac48kCoef();
	Audio_DacAnalogSel48k();
}
void Audio_DacSamplefreqSel44k(void)
{
	Audio_LoadDac44kCoef();
	Audio_DacAnalogSel44k();
}
uint8_t Audio_DacClkCheck(uint8_t dac_clk)
{
	uint8_t temp;
	switch(dac_clk)
	{
		case 8:
			Audio_DacSamplefreqSel8k();
			temp = 8;
			break;
		case 16:
			Audio_DacSamplefreqSel16k();
			temp = 4;
			break;
		case 48:
			Audio_DacSamplefreqSel48k();
			temp = 0;
			break;
		default:
			Audio_DacSamplefreqSel44k();
			temp = 0;
			break;
	}

	return temp;
}

void Audio_DacCoefInit(void)
{
	uint32_t temp;
	uint8_t i;
	HWCOR(CORE_COEF_CTRL, BIT_2);
	HWOR(CORE_COEF_CTRL, BIT_2);
    if(coef_dac_table_addr==NULL)
    {
        coef_dac_table_addr=coef_dac_table;
    }
	for(i = 0; i < 64; i ++)
	{
        temp = (coef_dac_table_addr[i*3]|(coef_dac_table_addr[i*3+1]<<8)|(coef_dac_table_addr[i*3+2]<<16));
		temp = temp >> 4;
		HWRITE24BIT(CORE_COEF_WDATA, temp);

		HWOR(CORE_COEF_CTRL, BIT_3);
		delay_us(5);
		HWCOR(CORE_COEF_CTRL, BIT_3);
		delay_us(5);
	}
	HWCOR(CORE_COEF_CTRL, BIT_2);
}

void Audio_DacSetSampleRate(uint8_t dac_clk)
{
	uint8_t temp_0, temp_1;
	Audio_DacAnaClkDisable();
	Audio_DacFilterDisable();
	Audio_DacSdmDisable();

	// init coef
	Audio_DlychainDacInit();

	// clock select and coef init
	temp_0 = Audio_DacClkCheck(dac_clk);

	// set clk
	temp_1 = HREAD(CORE_DAC_SEL);
	temp_1 &= 0xf3;
	temp_1 |= temp_0;
	HWRITE(CORE_DAC_SEL, temp_1);

	//init coef
	delay_us(20);
	Audio_DacCoefInit();

	Audio_DacFilter();
	Audio_DacSdm();
	delay_us(100);

	Audio_DacAnaClkEnable();
}

void Audio_DacVolAdjust(void)
{
	uint16_t vol = 0;
	uint16_t volNow = HREAD(CORE_DAC_LVOL);
	vol = HREAD(mem_hf_vlm_speaker)*4;

	if(volNow != vol)
	{
		if(volNow < vol)
		{
			volNow++;
			HWRITE(CORE_DAC_LVOL, volNow);
			HWRITE(CORE_DAC_RVOL, volNow);
		}
		else
		{
			volNow--;
			HWRITE(CORE_DAC_LVOL, volNow);
			HWRITE(CORE_DAC_RVOL, volNow);
		}
	}
}
void Audio_DacInit(uint8_t mono_flag, uint8_t l_din_sel, uint8_t r_din_sel, uint8_t dac_clk, uint8_t dac_mram_flag
	, uint16_t dac_start_addr, uint16_t dac_buf_size)
{
	BOOL isNeedStartPa = FALSE;
	uint8_t tmp = HREAD(CORE_DAC_CTRL);
	//DEBUG_LOG(LOG_LEVEL_CORE, "UI" ,"Audio_DacInit: 0x%04X", LOG_POINT_A623, tmp);

	//Check Dac open or not.
	if((tmp & (BIT_0)) != 0)//DAC_ENABLE
	{
		//DEBUG_LOG_2(LOG_LEVEL_CORE, "UI" ,"Audio_DacInit: 0x%04X", LOG_POINT_A624, mono_flag, dac_clk);
		return;
	}
	//DEBUG_LOG_2(LOG_LEVEL_CORE, "UI" ,"Audio_DacInit: 0x%04X", LOG_POINT_A600, mono_flag, dac_clk);
	HWRITE(mem_dac_clk,dac_clk);
	//TODO: Check need enable or not
	if(Audio_DacCheckDaAdacOpaEnable())
	{
		if(!Audio_AdacCheckLastDacClkSame(dac_clk))
		{
			Audio_AdacStopPAWithPop();
			// Need delay a while
			delay_ms(5);
			isNeedStartPa = TRUE;
		}
	}
	else
	{
		isNeedStartPa = TRUE;
	}
	if(isNeedStartPa)
	{
		Audio_AdacStartPAWithPop(dac_clk);
		// Need delay a while
		delay_ms(5);
	}
	//When open pa the ana vol is limit
	Audio_AdacPrepareReadyWork();

	//Avoid last finish flag
	Audio_DacClearRampDown();

	Audio_DacSetL(l_din_sel);
	Audio_DacSetR(r_din_sel);
	Audio_DacMonoStero(mono_flag);

	// Should not change immediate, it will make pop
	//Audio_DacVolAdjust();
	if(dac_mram_flag)
	{
		HWOR(CORE_DAC_CTRL, BIT_1);//DAC_MRAM_SEL
	}
	else
	{
		HWCOR(CORE_DAC_CTRL, BIT_1);//DAC_MRAM_SEL
	}

	HWCOR(CORE_DAC_CTRL, BIT_2);//DAC_CONTINUOUS_MODE
	HWCOR(CORE_DAC_CTRL, BIT_3);//DAC_MUTU

	HWRITEW(CORE_DAC_SADDR, dac_start_addr);
	HWRITEW(CORE_DAC_WPTR, dac_start_addr);
	HWRITEW(CORE_DAC_RPTR, dac_start_addr);

	HWRITEW(CORE_DAC_LEN, dac_buf_size - 1);

	// dac start
	HWOR(CORE_DAC_CTRL, BIT_0);//DAC_ENABLE

	Audio_RelasePaDelay();

	// For reduce power
	//if(Bt_CheckA2DPStart())
	//{
	//	HWRITE(0x8a01, 0xe2);
	//}
	//else
	{
		HWRITE(0x8a01, 0xf2);
	}

	// Check adc and eq enable or not
	if((HREAD(mem_eq_flag) == 0)
		//&& !Bt_CheckHfpStart()
		&& !Audio_CheckAdcDmaEnable())
	{
#ifdef FUNCTION_OAL_MODE
		if(!Oal_CheckIsFunctionEnable())
#endif
		{
			HWORW(CORE_CLKOFF, REG_CLOCK_OFF_VOICE_FILTER);
		}
	}
}

uint8_t Audio_CheckDacMRAMSelect(void)
{
	return ((HREAD(CORE_DAC_CTRL) & BIT_1) != 0);//DAC_MRAM_SEL
}
void Audio_DacInitMono(uint8_t dac_clk, uint8_t dac_mram_flag
	, uint16_t dac_start_addr, uint16_t dac_buf_size)
{
	Audio_DacInit(1, DIN_SEL_L_LEFT_CHANNEL, DIN_SEL_R_LEFT_CHANNEL, dac_clk, dac_mram_flag
		, dac_start_addr, dac_buf_size);
}
void Audio_DacInitStero(uint8_t dac_clk, uint8_t dac_mram_flag
	, uint16_t dac_start_addr, uint16_t dac_buf_size)
{
	Audio_DacInit(0, DIN_SEL_L_LEFT_CHANNEL, DIN_SEL_R_RIGHT_CHANNEL, dac_clk, dac_mram_flag
		, dac_start_addr, dac_buf_size);
}
void Audio_DacInitLeftMonoStero(uint8_t dac_clk, uint8_t dac_mram_flag
	, uint16_t dac_start_addr, uint16_t dac_buf_size)
{
	Audio_DacInit(1, DIN_SEL_L_LEFT_CHANNEL, DIN_SEL_R_CLOSE, dac_clk, dac_mram_flag
		, dac_start_addr, dac_buf_size);
}
void Audio_DacInitRightMonoStero(uint8_t dac_clk, uint8_t dac_mram_flag
	, uint16_t dac_start_addr, uint16_t dac_buf_size)
{
	Audio_DacInit(1, DIN_SEL_L_CLOSE, DIN_SEL_R_LEFT_CHANNEL, dac_clk, dac_mram_flag
		, dac_start_addr, dac_buf_size);
}
void Audio_DacInitMedia(uint8_t dac_clk, ADCRAMUSEDEV dac_mram_flag
	, uint16_t dac_start_addr, uint16_t dac_buf_size)
{
	switch(HREAD(mem_audio_output_setting))
	{
		case AUDIO_OUTPUT_TWS_SINGLE_EAR_LEFT:
			Audio_DacInitLeftMonoStero(dac_clk, dac_mram_flag
				, dac_start_addr, dac_buf_size);
			break;
		case AUDIO_OUTPUT_TWS_SINGLE_EAR_RIGHT:
			Audio_DacInitRightMonoStero(dac_clk, dac_mram_flag
				, dac_start_addr, dac_buf_size);
			break;
		default:
			Audio_DacInitStero(dac_clk, dac_mram_flag
				, dac_start_addr, dac_buf_size);
			break;
	}
}


void Audio_DacInitVpCall(uint8_t dac_clk, ADCRAMUSEDEV dac_mram_flag
	, uint16_t dac_start_addr, uint16_t dac_buf_size)
{
	switch(HREAD(mem_audio_output_setting))
	{
		case AUDIO_OUTPUT_TWS_SINGLE_EAR_LEFT:
			Audio_DacInitLeftMonoStero(dac_clk, dac_mram_flag
				, dac_start_addr, dac_buf_size);
			break;
		case AUDIO_OUTPUT_TWS_SINGLE_EAR_RIGHT:
			Audio_DacInitRightMonoStero(dac_clk, dac_mram_flag
				, dac_start_addr, dac_buf_size);
			break;
		default:
			Audio_DacInitMono(dac_clk, dac_mram_flag
				, dac_start_addr, dac_buf_size);
			break;
	}
}
BOOL Audio_CheckDacEmpty(void)
{
	return (HREADW(CORE_DAC_WPTR) == HREADW(CORE_DAC_RPTR));
}

void Audio_TishiAddressInit(uint16_t saddr, uint16_t len)
{
	HWRITEW(CORE_TISHI_SADDR, saddr);
	HWRITEW(CORE_TISHI_LEN, len - 1);
	HWRITEW(CORE_TISHI_WPTR, saddr);
	HWRITEW(CORE_TISHI_RPTR, saddr);
}

void Audio_TishiEnable(void)
{
	// use mram
	// M = 2, N = 2
	// play_sound = main_sound/M + tishi_sound/N
	// tishi_l_en, tishi_r_en
	HWRITEW(CORE_TISHI_CTRL, 0xe7);
}

void Audio_TishiDisable(void)
{
	HWRITEW(CORE_TISHI_SADDR, 0);
	HWRITEW(CORE_TISHI_LEN, 0);
	HWRITEW(CORE_TISHI_CTRL, 0);
}

uint8_t Audio_CheckTishiMRAMSelect(void)
{
	return ((HREAD(CORE_TISHI_CTRL) & BIT_5) != 0);//tishi_mram_sel, 1: mram, 0: not mram
}


void Audio_Continue_mode(FunctionalState Audio_Mod)
{
	if(Audio_Mod)
	HWOR(CORE_DAC_CTRL, BIT_2);
	else
	HWCOR(CORE_DAC_CTRL, BIT_2);
}


void Audio_AdacOpaCmnSel(uint8_t vol)//RG_ADAC_OPACMN_SEL
{
	uint8_t tmp = HREADW(CORE_AADC_2);
	tmp &= 0xF0;
	tmp |=vol;
	HWRITEW(CORE_AADC_2, tmp);
}
void Audio_AdacHPPAOutputStageQuiescentCurrentControl(DevAudioHPPAOutputStageQuiescentCurrent cur)
{
	uint8_t tmp = HREADW(CORE_AADC_2);
	tmp &= 0x8f;
	tmp |=(cur<<4);
	HWRITEW(CORE_AADC_2, tmp);
}
void Audio_HPPACascodeVoltageControl(DevAudioHPPACascodeVoltage cas)
{
	uint8_t tmp = HREADW(CORE_AADC_2);
	tmp &= 0x7f;
	tmp |=((cas&0x01)<<7);
	HWRITEW(CORE_AADC_2, tmp);
	tmp=HREADW(CORE_AADC_3);
	tmp &= 0xfc;
	tmp |=((cas&0x06)>>1);
	HWRITEW(CORE_AADC_3, tmp);
}

void Audio_AdacRefCtrl(uint8_t val)
{
	uint8_t tmp = HREAD(CORE_AADC_7);
	tmp &= 0x0F;
	HWRITE(CORE_AADC_7, tmp | (val << 4));
}

//"Audio DAC feedback cap control0: 8.8pF; 1: 2x8.8pF"
/*void Audio_AdacFeedbackCapControl(uint8_t val)
{
	uint8_t tmp = HREAD(CORE_AADC_3);
	tmp |= ((val & 0x01) << 5);
	HWRITE(CORE_AADC_7, tmp | (val << 4));
}*/

void Audio_AdacBiasSel(DevAudioDacBiasControl bias)//RG_ADAC_BIAS_SEL
{
	uint8_t tmp = HREAD(CORE_AADC_0);
	tmp &= 0xF0;
	tmp |= bias ;
	HWRITE(CORE_AADC_0, tmp );
}
void Audio_AdacOpacmpSel(uint8_t opacmp)//RG_ADAC_OPACMP_SEL
{
	uint8_t tmp = HREAD(CORE_AADC_0);
	tmp &= 0x0F;
	tmp |= (opacmp << 4);
	HWRITE(CORE_AADC_0, tmp );
}

void  Audio_AdacOpaBiasSel(DevAudioHPPAbiasCurrentControl hpcur)//RG_ADAC_OPABIAS_SEL
{
	uint8_t tmp = HREAD(CORE_AADC_1);
	tmp &= 0xCF;
	tmp |= (hpcur<<4);
	HWRITE(CORE_AADC_1, tmp );
}
void  Audio_AdacOpamcmpSel(DevAudioHPPACompensationCapControl hpcap)//RG_ADAC_OPAMCAP_SEL
{
	uint8_t tmp = HREAD(CORE_AADC_1);
	tmp &= 0x3F;
	tmp |= (hpcap<<6);
	HWRITE(CORE_AADC_1, tmp );
}

void Audio_AdacVCMIOPA_SEL(DevDACComModeVolBufOutputStageCur cur)
{
	uint8_t tmp = HREAD(CORE_AADC_3);
	tmp &=0xf3;
	tmp |=(cur<<2);
	HWRITE(CORE_AADC_3, tmp );
}
void Audio_AdacComVoltageBufferModeSet(DevAudioVoltageBufferBypassMode mode)
{
	uint8_t tmp = HREAD(CORE_AADC_3);
	tmp &=0xef;
	tmp |=(mode<<4);
	HWRITE(CORE_AADC_3, tmp);
}
void Audio_AdacFeedbackCapControl(DevAudioDACFeedbackCapControl feed)
{
	uint8_t tmp = HREAD(CORE_AADC_3);
	tmp &=0xdf;
	tmp |=(feed<<5);
	HWRITE(CORE_AADC_3, tmp );
}
void Audio_DacLDOOutputControl(uint8_t value)
{
	uint8_t tmp = HREAD(CORE_AADC_5);
	tmp &=0xd8;
	tmp |=(value&0x07);
	HWRITE(CORE_AADC_5, tmp );
}
void Audio_AdacDacCommonModeVoltageControl(DevAudioDACCommonModeVoltageControl val)
{
	uint8_t tmp = HREAD(CORE_AADC_5);
	tmp &=0xc7;
	tmp |=((val&0x03)<<3);
	HWRITE(CORE_AADC_5, tmp );
}
void Audio_AdacLdoAvddByPassEnable(void)
{
	uint8_t tmp = HREAD(CORE_AADC_5);
	tmp |=(1<<6);
	HWRITE(CORE_AADC_5, tmp );
}
void  Audio_AdacLdoHpvddByPassEnable(void)
{
	uint8_t tmp = HREAD(CORE_AADC_6);
	tmp |=(1<<5);
	HWRITE(CORE_AADC_6, tmp );
}
int16_t gDacDcCaliTargetLeft;
int16_t gDacDcCaliTargetRight;

uint8_t gDacAnaVolTargetLeft;
uint8_t gDacAnaVolTargetRight;
void Audio_AdacPorDefaultInit(void)
{
	//CORE_AADC_0
	Audio_AdacBiasSel(DAC_BIAS_3_0uA);
	Audio_AdacOpacmpSel(OPACMP_VDD200mV_RG_ADAC_OPABIAS_SEL1);
	//CORE_AADC_1
	Audio_AdacOpaBiasSel(HPPA_BIAS_CURRENT_8uA);
	Audio_AdacOpamcmpSel(HPPA_COMPENSATION_CAP_1040fF);
	//CORE_AADC_2
	Audio_AdacOpaCmnSel(HPPA_NMOS_CASCODE_VOLTAGE_200mV_OPABIAS1);
	Audio_AdacHPPAOutputStageQuiescentCurrentControl(RG_ADAC_OPA3STA_SEL2);
	Audio_HPPACascodeVoltageControl(RG_ADAC_OPABIAS_SEL1);

	// This will change max output vol
	/*Audio DAC output full-scale control
		1100: 1.2V (VDD>=1.7V)
		1101: 1.6V (VDD>=2.1V)
		1110: 2.0V (VDD>=2.5V)
		1111: 2.4V (VDD>=2.9V)	*/
	//CORE_AADC_7
	Audio_AdacRefCtrl(AUDIO_DAC_OUT_FULL_SCAL_CONTROL_1_6V);

	// When open, shoud use low ana vol
	//TODO: Current bottom nosie is 10-11uV, no need do this work
	//CORE_AADC_3
	Audio_AdacVCMIOPA_SEL(OUTPUT_STAGE_CUR_80uA);
	Audio_AdacSetAnaVolLeft(ADAC_SETTING_ANA_VOL_TARGET);
	Audio_AdacComVoltageBufferModeSet(USE_GOLBAL_COMMON_MODE_VOL_BUFFER);
	Audio_AdacFeedbackCapControl(Feedback_Cap_2x8_8pF);
	//CORE_AADC_4
	Audio_AdacSetAnaVolRight(ADAC_SETTING_ANA_VOL_TARGET);
	//CORE_AADC_5
	Audio_DacLDOOutputControl(ADAC_LDO_HPVDD_VCTRL_VALUE);
	Audio_AdacDacCommonModeVoltageControl(DAC_COM_MODE_VOL0_9VDD1_8);
	Audio_AdacLdoAvddByPassEnable();
	//CORE_AADC_6
	Audio_AdacLdoHpvddByPassEnable();

//	HWRITE(CORE_AADC_0, 0x12);//1121e
//	HWRITE(CORE_AADC_1, 0xf2);
//	HWRITE(CORE_AADC_2, 0x21);
//	HWRITE(CORE_AADC_3, 0xf3);
//	HWRITE(CORE_AADC_4, 0x18);
//	HWRITE(CORE_AADC_5, 0x50);
//	HWRITE(CORE_AADC_6, 0x20);
//	HWRITE(CORE_AADC_7, 0xd0);

    HWRITE(CORE_AADC_0, 0x1f);//1121f
    HWRITE(CORE_AADC_1, 0xe2);
    HWRITE(CORE_AADC_2, 0x21);
//    HWRITE(CORE_AADC_3, 0xe3);
    HWRITE(CORE_AADC_3, 0x23);
    HWRITE(CORE_AADC_4, 0x18);
    HWRITE(CORE_AADC_5, 0x50);
    HWRITE(CORE_AADC_6, 0x38);
    HWRITE(CORE_AADC_7, 0xd0);

	//Audio_AdacSetAnaVolLeft(6);
	//DC cali setting
	if(( (HREAD(CORE_AADC_4)>>3)&0x1f)>=3&&( (HREAD(CORE_AADC_4)>>3)&0x1f)<6)//0db
	{
		gDacDcCaliTargetLeft = HREADW(mem_efuse_dc_offset_l);//ff90 -112 0dBm
		if(abs(gDacDcCaliTargetLeft)>0xf0)
		{
			gDacDcCaliTargetLeft=0;
		}
		gDacDcCaliTargetRight = HREADW(mem_efuse_dc_offset_r);
		if(abs(gDacDcCaliTargetRight)>0xf0)
		{
			gDacDcCaliTargetRight=0;
		}
	}
	else if(( (HREAD(CORE_AADC_4)>>3)&0x1f)>=6)//-6db
	{
		gDacDcCaliTargetLeft = HREADW(mem_efuse_dc_offset_l_f6);
		if(abs(gDacDcCaliTargetLeft)>0xf0)
		{
			gDacDcCaliTargetLeft=0;
		}
		gDacDcCaliTargetRight = HREADW(mem_efuse_dc_offset_r_f6);
		if(abs(gDacDcCaliTargetRight)>0xf0)
		{
			gDacDcCaliTargetRight=0;
		}
	}
	else if(( (HREAD(CORE_AADC_4)>>3)&0x1f)==0)//6db
	{
		gDacDcCaliTargetLeft = HREADW(mem_efuse_dc_offset_l);//ff90 -112 0dBm
		gDacDcCaliTargetLeft*=2;
		if(abs(gDacDcCaliTargetLeft)>0x1e0)
		{
			gDacDcCaliTargetLeft=0;
		}
		gDacDcCaliTargetRight = HREADW(mem_efuse_dc_offset_r);
		gDacDcCaliTargetRight*=2;
		if(abs(gDacDcCaliTargetRight)>0x1e0)
		{
			gDacDcCaliTargetRight=0;
		}
	}
	HWRITEW(CORE_DAC_DC_CALI_L, gDacDcCaliTargetLeft);
	HWRITEW(CORE_DAC_DC_CALI_R, gDacDcCaliTargetRight);
}

void Audio_DacSetRampDownEnL(void)
{
	HWOR(CORE_RAMP_CTRL, 1<< left_down_en);//left_down_en
}
void Audio_DacSetRampDownEnR(void)
{
	HWOR(CORE_RAMP_CTRL, 1<< right_down_en);//right_down_en
}
void Audio_DacSetRampDownEn(void)
{
	switch(HREAD(mem_audio_output_setting))
	{
		case AUDIO_OUTPUT_TWS_SINGLE_EAR_LEFT:
			Audio_DacSetRampDownEnL();
			break;
		case AUDIO_OUTPUT_TWS_SINGLE_EAR_RIGHT:
			Audio_DacSetRampDownEnR();
			break;
		default:
			Audio_DacSetRampDownEnL();
			Audio_DacSetRampDownEnR();
			break;
	}
}

void Audio_DacSetDaAdacIntL(void)
{
	HWOR(CORE_AADC_9, 1<<DA_EN_ADAC_INT_L);
}
void Audio_DacSetDaAdacIntR(void)
{
	HWOR(CORE_AADC_9, 1<<DA_EN_ADAC_INT_R);
}
void Audio_DacSetDaAdacInt(void)
{
	switch(HREAD(mem_audio_output_setting))
	{
		case AUDIO_OUTPUT_TWS_SINGLE_EAR_LEFT:
			Audio_DacSetDaAdacIntL();
			break;
		case AUDIO_OUTPUT_TWS_SINGLE_EAR_RIGHT:
			Audio_DacSetDaAdacIntR();
			break;
		default:
			Audio_DacSetDaAdacIntL();
			Audio_DacSetDaAdacIntR();
			break;
	}
}
void Audio_DacDisableDaAdacInt(void)
{
	uint8_t tmp = HREAD(CORE_AADC_9);
	tmp &= ~(1<<DA_EN_ADAC_INT_L);
	tmp &= ~(1<<DA_EN_ADAC_INT_R);
	HWRITE(CORE_AADC_9, tmp);
}

void Audio_DacSetDaAdacOpaL(void)
{
	HWOR(CORE_AADC_9, 1<<DA_EN_ADAC_OPAL);
}
void Audio_DacSetDaAdacOpaR(void)
{
	HWOR(CORE_AADC_9, 1<<DA_EN_ADAC_OPAR);
}
void Audio_DacSetDaAdacOpa(void)
{
	switch(HREAD(mem_audio_output_setting))
	{
		case AUDIO_OUTPUT_TWS_SINGLE_EAR_LEFT:
			Audio_DacSetDaAdacOpaL();
			break;
		case AUDIO_OUTPUT_TWS_SINGLE_EAR_RIGHT:
			Audio_DacSetDaAdacOpaR();
			break;
		default:
			Audio_DacSetDaAdacOpaL();
			Audio_DacSetDaAdacOpaR();
			break;
	}
}
uint8_t Audio_DacCheckDaAdacOpaEnable(void)
{
	uint8_t tmp = HREAD(CORE_AADC_9);

	return (tmp & (1<<DA_EN_ADAC_OPAL)) | (tmp & (1<<DA_EN_ADAC_OPAR));
}
void Audio_DacDisableDaAdacOpa(void)
{
	uint8_t tmp = HREAD(CORE_AADC_9);
	tmp &= ~(1<<DA_EN_ADAC_OPAL);
	tmp &= ~(1<<DA_EN_ADAC_OPAR);
	HWRITE(CORE_AADC_9, tmp);
}

void Audio_DacSetDaAdacPathL(void)
{
	HWOR(CORE_AADC_9, 1<<DA_EN_ADAC_PATHL);
}
void Audio_DacSetDaAdacPathR(void)
{
	HWOR(CORE_AADC_9, 1<<DA_EN_ADAC_PATHR);
}
void Audio_DacSetDaAdacPath(void)
{
	switch(HREAD(mem_audio_output_setting))
	{
		case AUDIO_OUTPUT_TWS_SINGLE_EAR_LEFT:
			Audio_DacSetDaAdacPathL();
			break;
		case AUDIO_OUTPUT_TWS_SINGLE_EAR_RIGHT:
			Audio_DacSetDaAdacPathR();
			break;
		default:
			Audio_DacSetDaAdacPathL();
			Audio_DacSetDaAdacPathR();
			break;
	}
}
void Audio_DacDisableDaAdacPath(void)
{
	uint8_t tmp = HREAD(CORE_AADC_9);
	tmp &= ~(1<<DA_EN_ADAC_PATHL);
	tmp &= ~(1<<DA_EN_ADAC_PATHR);
	HWRITE(CORE_AADC_9, tmp);
}

void Audio_DacSetDaAdacDepopSwL(void)
{
	HWOR(CORE_AADC_A, 1<<DA_EN_ADAC_DEPOP_SW_L);
}
void Audio_DacSetDaAdacDepopSwR(void)
{
	HWOR(CORE_AADC_A, 1<<DA_EN_ADAC_DEPOP_SW_R);
}
void Audio_DacSetDaAdacDepopSw(void)
{
	switch(HREAD(mem_audio_output_setting))
	{
		case AUDIO_OUTPUT_TWS_SINGLE_EAR_LEFT:
			Audio_DacSetDaAdacDepopSwL();
			break;
		case AUDIO_OUTPUT_TWS_SINGLE_EAR_RIGHT:
			Audio_DacSetDaAdacDepopSwR();
			break;
		default:
			Audio_DacSetDaAdacDepopSwL();
			Audio_DacSetDaAdacDepopSwR();
			break;
	}
}
void Audio_DacDisableDaAdacDepopSw(void)
{
	uint8_t tmp = HREAD(CORE_AADC_A);
	tmp &= ~(1<<DA_EN_ADAC_DEPOP_SW_L);
	tmp &= ~(1<<DA_EN_ADAC_DEPOP_SW_R);
	HWRITE(CORE_AADC_A, tmp);
}


void Audio_DacSetDaAdacOpaDummyToMainL(void)
{
	uint8_t tmp = HREAD(CORE_AADC_A);
	tmp &= ~(1<<DA_EN_ADAC_OPA_DUMMY_L);
	tmp |= (1<<DA_EN_ADAC_OPA_LOOP_L);
	HWRITE(CORE_AADC_A, tmp);
}
void Audio_DacSetDaAdacOpaDummyToMainR(void)
{
	uint8_t tmp = HREAD(CORE_AADC_A);
	tmp &= ~(1<<DA_EN_ADAC_OPA_DUMMY_R);
	tmp |= (1<<DA_EN_ADAC_OPA_LOOP_R);
	HWRITE(CORE_AADC_A, tmp);
}
void Audio_DacSetDaAdacOpaDummyToMain(void)
{
	switch(HREAD(mem_audio_output_setting))
	{
		case AUDIO_OUTPUT_TWS_SINGLE_EAR_LEFT:
			Audio_DacSetDaAdacOpaDummyToMainL();
			break;
		case AUDIO_OUTPUT_TWS_SINGLE_EAR_RIGHT:
			Audio_DacSetDaAdacOpaDummyToMainR();
			break;
		default:
			Audio_DacSetDaAdacOpaDummyToMainL();
			Audio_DacSetDaAdacOpaDummyToMainR();
			break;
	}
}

void Audio_DacSetDaAdacOpaMainToDummyL(void)
{
	uint8_t tmp = HREAD(CORE_AADC_A);
	tmp &= ~(1<<DA_EN_ADAC_OPA_LOOP_L);
	tmp |= (1<<DA_EN_ADAC_OPA_DUMMY_L);
	HWRITE(CORE_AADC_A, tmp);
}
void Audio_DacSetDaAdacOpaMainToDummyR(void)
{
	uint8_t tmp = HREAD(CORE_AADC_A);
	tmp &= ~(1<<DA_EN_ADAC_OPA_LOOP_R);
	tmp |= (1<<DA_EN_ADAC_OPA_DUMMY_R);
	HWRITE(CORE_AADC_A, tmp);
}
void Audio_DacSetDaAdacOpaMainToDummy(void)
{
	switch(HREAD(mem_audio_output_setting))
	{
		case AUDIO_OUTPUT_TWS_SINGLE_EAR_LEFT:
			Audio_DacSetDaAdacOpaMainToDummyL();
			break;
		case AUDIO_OUTPUT_TWS_SINGLE_EAR_RIGHT:
			Audio_DacSetDaAdacOpaMainToDummyR();
			break;
		default:
			Audio_DacSetDaAdacOpaMainToDummyL();
			Audio_DacSetDaAdacOpaMainToDummyR();
			break;
	}
}
void Audio_DacSetDaAdacRstIntL(void)
{
	HWOR(CORE_AADC_A, 1<<DA_ADAC_RST_INT_L);//DA_ADAC_RST_INT_L
}
void Audio_DacSetDaAdacRstIntR(void)
{
	HWOR(CORE_AADC_A, 1<<DA_ADAC_RST_INT_R);//DA_ADAC_RST_INT_R
}
void Audio_DacSetDaAdacRstInt(void)
{
	switch(HREAD(mem_audio_output_setting))
	{
		case AUDIO_OUTPUT_TWS_SINGLE_EAR_LEFT:
			Audio_DacSetDaAdacRstIntL();
			break;
		case AUDIO_OUTPUT_TWS_SINGLE_EAR_RIGHT:
			Audio_DacSetDaAdacRstIntR();
			break;
		default:
			Audio_DacSetDaAdacRstIntL();
			Audio_DacSetDaAdacRstIntR();
			break;
	}
}
void Audio_DacSetDaAdacRstIntDisable(void)
{
	uint8_t tmp = HREAD(CORE_AADC_A);
	tmp &= (1<<DA_ADAC_RST_INT_L);
	tmp &= (1<<DA_ADAC_RST_INT_R);
	HWRITE(CORE_AADC_A, tmp);
}
void Audio_DacSetDaEnAdacOpaDummyL(void)
{
	HWOR(CORE_AADC_A, 1<<DA_EN_ADAC_OPA_DUMMY_L);//DA_EN_ADAC_OPA_DUMMY_L
}
void Audio_DacSetDaEnAdacOpaDummyR(void)
{
	HWOR(CORE_AADC_A, 1<<DA_EN_ADAC_OPA_DUMMY_R);//DA_EN_ADAC_OPA_DUMMY_L
}
void Audio_DacSetDaEnAdacOpaDummy(void)
{
	switch(HREAD(mem_audio_output_setting))
	{
		case AUDIO_OUTPUT_TWS_SINGLE_EAR_LEFT:
			Audio_DacSetDaAdacRstIntL();
			Audio_DacSetDaAdacRstIntR();
			break;
		case AUDIO_OUTPUT_TWS_SINGLE_EAR_RIGHT:
			Audio_DacSetDaAdacRstIntR();
			break;
		default:
			Audio_DacSetDaAdacRstIntL();
			Audio_DacSetDaAdacRstIntR();
			break;
	}
}
void Audio_DacDisableDaAdacOpaDummy(void)
{
	uint8_t tmp = HREAD(CORE_AADC_A);
	tmp &= (1<<DA_EN_ADAC_OPA_DUMMY_L);
	tmp &= (1<<DA_EN_ADAC_OPA_DUMMY_R);
	HWRITE(CORE_AADC_A, tmp);
}
void Audio_LpmWrite2AdcLow(uint32_t val)
{
	HWRITEL(mem_lpm_write_temp_adc_low, val);
	HWRITEL(CORE_LPM_REG, val);
	HWRITESINGLE(CORE_LPM_WR2, LPMREG_SEL_ADC_LOW);
	delay_ms(5);
}
void Audio_LpmWrite2AdcHigh(uint32_t val)
{
	HWRITEL(mem_lpm_write_temp_adc_high, val);
	HWRITEL(CORE_LPM_REG, val);
	HWRITESINGLE(CORE_LPM_WR2, LPMREG_SEL_ADC_HIGH);
	delay_ms(5);
}
void Audio_DaAaddaBgEn(void)
{
	uint32_t tmp = HREADL(mem_lpm_write_temp_adc_low);
	// If enable, rtn
	if(tmp & (1 << da_aadda_bg_en)) // da_aadda_bg_en
	{
		return;
	}
	tmp |= (1 << da_aadda_bg_en); // da_aadda_bg_en
	Audio_LpmWrite2AdcLow(tmp);

	//DA_AADDA_BG_EN_FC = 1 FOR >=5US
	//DA_AADDA_BG_EN_FC = 0
	tmp |= BIT_31;//da_aadda_bg_en_fc
	Audio_LpmWrite2AdcLow(tmp);
	 // for >=5uS
	//whileDelay(100);
	delay_us(5);
	tmp &= ~(BIT_31);//da_aadda_bg_en_fc
	Audio_LpmWrite2AdcLow(tmp);
}

void Audio_DaAaddaBgDisable(void)
{
	uint32_t tmp = HREADL(mem_lpm_write_temp_adc_low);
	tmp &= ~(1 << da_aadda_bg_en); // da_aadda_bg_en
	Audio_LpmWrite2AdcLow(tmp);
}
void Audio_DacSetVolBeforeCalibration(void)
{
	Audio_AdacAdjustDigitalVolToCali();
}

void Audio_DacInitRampDown(void)
{
	//Set left_down_dest(8048)/right_down_dest(804a) to 0x0000 (ramp down end value)
	HWRITEW(CORE_LEFT_DOWN_DEST, 0);
	HWRITEW(CORE_RIGHT_DOWN_DEST, 0);

	//Set left_down_step(806f)/right_down_step(8070) to 0xf // larger value faster ramp down speed.
	// 0xf too fast, will make pop.
	HWRITE(CORE_LEFT_STEP, 0x01);
	HWRITE(CORE_RIGHT_STEP, 0x01);

	Audio_DacSetRampDownEn();
}

uint8_t gLastStartDacClk;
// For mute POP, before PA enable, Must:
// 1. Enable clock
// 2. Fix zero out
// 3. Enable filter clk
// 4. Enable sdm clk
// 5. Enable ana clk
void Audio_DacInitForPop(uint8_t dac_clk)
{
	//1. Turn on DAC digital clock
	//RG_MISC (896E) = 0x94 for 48K/16K/8K or 0xC4 for 44.1K
	if(dac_clk != 44)
	{
		HWRITE(RG_MISC, rg_misc_8_16_48K);
		HWRITE(RF_AFC_CAP, rg_clkpll_8_16_48K);
	}
	else
	{
		HWRITE(RG_MISC, rg_misc_44d1K);
		HWRITE(RF_AFC_CAP, rg_clkpll_44d1K);
	}

	gLastStartDacClk = dac_clk;

	// enable clk
	HWCORW(CORE_CLKOFF, REG_CLOCK_OFF_AUDIO_DAC);
	Audio_DacAnaClkDisable();

	// Left right fix zero
	HWRITE(CORE_DAC_SEL2, 0x77);


	Audio_DacSetSampleRate(dac_clk);
}
void Audio_AdacStartPAWithPop(uint8_t dac_clk)
{
	uint8_t tmp;
//	DEBUG_LOG(LOG_LEVEL_CORE, "UI" ,"Audio_DacInit: 0x%04X", LOG_POINT_A601, dac_clk);

	//2. DAC init
	// Here need enable ana clock
	Audio_DacInitForPop(dac_clk);

	//3. Set digital volume to the value used when calibration
	// Must set the vol while it set in dc cali
	Audio_DacSetVolBeforeCalibration();

	//4. Enable digital ramp down for fading out music needed for pause function
	Audio_DacInitRampDown();

	//5. Turn on audio bandgap (required by audio ADC & DAC)
	Audio_DaAaddaBgEn();

	Audio_DacSetDaAdacRstInt();
	Audio_DacSetDaEnAdacOpaDummy();

	//6. Turn on DAC LDOs
	//DA_EN_ADAC_BIAS = 1
	HWOR(CORE_AADC_9, 1<<DA_EN_ADAC_BIAS);

	//DA_ADAC_LDO_HPVDD_EN = 1
	//DA_ADAC_LDO_AVDD_EN = 1
	tmp = HREAD(CORE_AADC_8);
	tmp |= (1 << DA_ADAC_LDO_AVDD_EN);
	tmp |= (1 << DA_ADAC_LDO_HPVDD_EN);
	HWRITE(CORE_AADC_8, tmp);

	//
	Audio_DacSetDaAdacRstIntDisable();
	delay_us(10);


	//7. Turn on DAC reference generator
	//DA_EN_ADAC_REF = 1
	HWOR(CORE_AADC_9, 1<<DA_EN_ADAC_REF);

	//8. Turn on DAC core
	Audio_DacSetDaAdacInt();
	delay_us(10);

	//9. Turn on HP PA
	Audio_DacSetDaAdacOpa();
	delay_us(10);

	Audio_DacSetDaAdacPath();
	delay_ms(5);

	Audio_DacSetDaAdacDepopSw();
	delay_ms(5);

	Audio_DacSetDaAdacOpaDummyToMain();
}

void Audio_DacWaitRampDown(void)
{
	uint16_t waitLimit = 0;
//	DEBUG_LOG(LOG_LEVEL_CORE, "UI" ,"Audio_DacInit: 0x%04X", LOG_POINT_A60B, HREAD(CORE_DAC_STATUS));
	// Wait ramp down finish
	//Tramp= (last_data_value/(left/right_down_step +1)*(1/48)) mS
	//,  if sample rate is 48K. The maximum time is 40ms if down_step is '0xF' and sample rate is 48K.
	//Use 8K sample rate the maximum time is 6*40=240ms.
	if(HREAD(mem_hf_vlm_speaker) <= (HFP_SPEAKER_MAX_VLM_NEW / 2))
	{
		//For Iphone
		HWRITE(CORE_LEFT_STEP, 0x02);
		HWRITE(CORE_RIGHT_STEP, 0x02);
	}
	while((HREAD(CORE_DAC_STATUS) & BIT_4) == 0)
	{
		delay_us(1);
		if(waitLimit ++ > 2000)
		{
#ifdef FUNCTION_WATCH_DOG
			WDT_Kick();
#endif
			uint8_t curRampStep = HREAD(CORE_LEFT_STEP);
			if(curRampStep < 0x0f)
			{
				curRampStep += 1;

				HWRITE(CORE_LEFT_STEP, curRampStep);
				HWRITE(CORE_RIGHT_STEP, curRampStep);
			}
			else
			{
				if((HREAD(CORE_DAC_STATUS) & 0x0f) == 0)
				{
					//DEBUG_LOG(LOG_LEVEL_CORE, "UI" ,"Audio_DacInit: 0x%04X", LOG_POINT_A60D, HREAD(CORE_DAC_STATUS));
					break;
				}
			}

			waitLimit  = 0;
		}
	}
	// Restore the step value
	HWRITE(CORE_LEFT_STEP, 0x01);
	HWRITE(CORE_RIGHT_STEP, 0x01);
	//DEBUG_LOG(LOG_LEVEL_CORE, "UI" ,"Audio_DacInit: 0x%04X", LOG_POINT_A60F, HREAD(CORE_DAC_STATUS));

	Audio_DacClearRampDown();
}

void Audio_DacClearRampDown(void)
{
	 // write 8138, bit[4], ��10�� to clear down_finish_flag
	HWOR(CORE_RAMP_CTRL, BIT_4);
	delay_us(1);
	HWCOR(CORE_RAMP_CTRL, BIT_4);
}
void Audio_AdacStopPAWithPop(void)
{
	//DEBUG_LOG(LOG_LEVEL_CORE, "UI" ,"Audio_DacInit: 0x%04X", LOG_POINT_A60C, HREAD(CORE_AADC_9));

	if(!Audio_DacCheckDaAdacOpaEnable())
	{
		return;
	}
	//3. Set digital volume to the value used when calibration
	// Must set the vol while it set in dc cali
	Audio_AdacPreparePAClose();
	// Need delay a while
	delay_ms(5);

	Audio_DacSetDaAdacOpaMainToDummy();
	delay_ms(5);

	Audio_DacDisableDaAdacDepopSw();
	Audio_DacDisableDaAdacPath();


	//4. Turn off HP PA
	Audio_DacDisableDaAdacOpa();
	//5. Turn off other DAC-related blocks
	//DA_EN_ADAC_INT_L (R) = 0
	Audio_DacDisableDaAdacInt();
	//DA_EN_ADAC_REF = 0
	HWCOR(CORE_AADC_9, (1<<DA_EN_ADAC_REF));
	//DA_ADAC_LDO_HPVDD_EN = 0
	HWCOR(CORE_AADC_8, (1<<DA_ADAC_LDO_HPVDD_EN));
	//DA_ADAC_LDO_AVDD_EN = 0
	HWCOR(CORE_AADC_8, (1<<DA_ADAC_LDO_AVDD_EN));
	//DA_EN_ADAC_BIAS = 0
	HWCOR(CORE_AADC_9, (1<<DA_EN_ADAC_BIAS));


	Audio_DacSetDaAdacRstInt();
	Audio_DacDisableDaAdacOpaDummy();

	//6. Turn off audio bandgap if needed (required by audio ADC & DAC)
	//da_aadda_bg_en = 0
#ifdef FUNCTION_OAL_MODE
	if(!Oal_CheckIsFunctionEnable())
#endif
	{
		Audio_DaAaddaBgDisable();
	}

	HWRITEW(CORE_DAC_SEL2, 0);
	HWRITEW(CORE_DAC_CTRL, 0);

	// disable clk
	HWORW(CORE_CLKOFF, REG_CLOCK_OFF_AUDIO_DAC);
#ifdef FUNCTION_OAL_MODE
	if(!Oal_CheckIsFunctionEnable())
#endif
	{
		HWORW(CORE_CLKOFF, REG_CLOCK_OFF_VOICE_FILTER);
	}
}

//RG_ADAC_VOLL[1:0]	2'b11
//RG_ADAC_VOLL[4:2]	3'b000
//"Audio HP PA left-channel analog volume control6dB-2dB*RG_ADAC_VOLL"
void Audio_AdacSetAnaVolLeft(uint8_t value)
{
	uint8_t tmp = HREAD(CORE_AADC_3);
	tmp &=0x3f;
	tmp |=((value&0x03)<<6);
	HWRITE(CORE_AADC_3, tmp );
	tmp=HREAD(CORE_AADC_4);
	tmp &=0xfc;
	tmp |=((value&0x0c)>>2);
	HWRITE(CORE_AADC_4, tmp );
}/*
void Audio_AdacSetAnaVolLeft(uint8_t val)
{
	uint16_t tmp = HREADW(CORE_AADC_3);
	tmp &= 0xF83F;
	tmp |= ((val & 0x03) << 6);
	tmp |= (((val & 0x1c)  >> 2) << 8);
	HWRITEW(CORE_AADC_3, tmp);
}*/
uint8_t Audio_AdacGetAnaVolLeft(void)
{
	uint16_t tmp = HREADW(CORE_AADC_3);
	tmp = tmp >> 6;
	return (tmp & 0x1f);
}
//RG_ADAC_VOLR              	5'b00011
//"Audio HP PA right-channel analog volume control6dB-2dB*RG_ADAC_VOLL"
void Audio_AdacSetAnaVolRight(uint8_t val)
{
	uint16_t tmp = HREAD(CORE_AADC_4);
	tmp &= 0x07;
	tmp |= ((val & 0x1f) << 3);
	HWRITE(CORE_AADC_4, tmp);
}
uint8_t Audio_AdacGetAnaVolRight(void)
{
	uint16_t tmp = HREAD(CORE_AADC_4);
	tmp = tmp >> 3;
	return (tmp & 0x1f);
}


uint8_t Audio_AdacCheckAnaVolAlreadyNormal(void)
{
	uint8_t tmp = 0;
	tmp = Audio_AdacGetAnaVolLeft();
	return (tmp == ADAC_SETTING_ANA_VOL_TARGET);
}
void Audio_AdacAdjustAnaVolToNormal(void)
{
	uint8_t tmp = 0;
	tmp = Audio_AdacGetAnaVolLeft();
	//DEBUG_LOG(LOG_LEVEL_CORE, "UI" ,"Audio_DacInit: 0x%04X", LOG_POINT_A609, tmp);
	while(tmp > ADAC_SETTING_ANA_VOL_TARGET)
	{
		tmp--;
		Audio_AdacSetAnaVolLeft(tmp);
		delay_ms(1);
	}
}
uint8_t Audio_AdacCheckAnaVolAlreadyLimit(void)
{
	uint8_t tmp = 0;
	tmp = Audio_AdacGetAnaVolLeft();
	return (tmp == ADAC_SETTING_ANA_VOL_LIMIT);
}
void Audio_AdacAdjustAnaVolToLimit(void)
{
	uint8_t tmp = 0;
	tmp = Audio_AdacGetAnaVolLeft();
	//DEBUG_LOG(LOG_LEVEL_CORE, "UI" ,"Audio_DacInit: 0x%04X", LOG_POINT_A608, tmp);
	while(tmp < ADAC_SETTING_ANA_VOL_LIMIT)
	{
		tmp++;
		Audio_AdacSetAnaVolLeft(tmp);
		delay_ms(1);
	}
}

uint16_t Audio_AdacToolGetAdjustInterval(int16_t rang)
{
	uint16_t tmpNoSignVal = rang;
	uint16_t tmpInterval = 0;
	if(rang < 0)
	{
		tmpNoSignVal = -rang;
	}
	tmpInterval = tmpNoSignVal / 30;
	if(tmpInterval == 0)
	{
		tmpInterval = tmpNoSignVal / 20;
		if(tmpInterval == 0)
		{
			tmpInterval = tmpNoSignVal / 10;
		}
	}

	if(tmpInterval == 0)
	{
		tmpInterval = 1;
	}

	return tmpInterval;
}
int16_t Audio_AdacToolGetSrcToDstVal(int16_t src, int16_t dst, uint16_t interval)
{
	if(src != dst)
	{
		if(src< dst)
		{
			src += interval;
			if(src > dst)
			{
				src = dst;
			}
		}
		else
		{
			src -= interval;
			if(src < dst)
			{
				src = dst;
			}
		}
	}

	return src;
}
void Audio_AdacAdjustDCCaliToNormal(void)
{
	int16_t tmp = HREADW(CORE_DAC_DC_CALI_L);
	int16_t tmpTarget = gDacDcCaliTargetLeft;
	uint16_t tmpInterval = Audio_AdacToolGetAdjustInterval(tmpTarget);
	//DEBUG_LOG(LOG_LEVEL_CORE, "UI" ,"Audio_DacInit: 0x%04X", LOG_POINT_A607, tmp);
	while(tmp != tmpTarget)
	{
		tmp = Audio_AdacToolGetSrcToDstVal(tmp, tmpTarget, tmpInterval);
		HWRITEW(CORE_DAC_DC_CALI_L, tmp);
		delay_ms(1);
	}
}

void Audio_AdacAdjustDCCaliToZero(void)
{
	int16_t tmp = HREADW(CORE_DAC_DC_CALI_L);
	int16_t tmpTarget = 0;
	uint16_t tmpInterval = Audio_AdacToolGetAdjustInterval(tmp);
	//DEBUG_LOG(LOG_LEVEL_CORE, "UI" ,"Audio_DacInit: 0x%04X", LOG_POINT_A606, tmp);
	while(tmp != tmpTarget)
	{
		tmp = Audio_AdacToolGetSrcToDstVal(tmp, tmpTarget, tmpInterval);
		HWRITEW(CORE_DAC_DC_CALI_L, tmp);
		delay_ms(1);
	}
}

void Audio_AdacAdjustDigitalVolToCali(void)
{
	uint16_t tmp = HREAD(CORE_DAC_LVOL);
	uint16_t tmpTarget = ADAC_SETTING_DIGITAL_VOL_LIMIT;
	uint16_t tmpInterval = Audio_AdacToolGetAdjustInterval(tmpTarget);
	//DEBUG_LOG(LOG_LEVEL_CORE, "UI" ,"Audio_DacInit: 0x%04X", LOG_POINT_A605, tmp);
	while(tmp != tmpTarget)
	{
		tmp = Audio_AdacToolGetSrcToDstVal(tmp, tmpTarget, tmpInterval);
		HWRITE(CORE_DAC_LVOL, tmp);
		delay_ms(1);
	}
}

void Audio_AdacAdjustDigitalVolToZero(void)
{
	uint16_t tmp = HREAD(CORE_DAC_LVOL);
	uint16_t tmpTarget = 0;
	uint16_t tmpInterval = Audio_AdacToolGetAdjustInterval(tmp);
	//DEBUG_LOG(LOG_LEVEL_CORE, "UI" ,"Audio_DacInit: 0x%04X", LOG_POINT_A604, tmp);
	while(tmp != tmpTarget)
	{
		tmp = Audio_AdacToolGetSrcToDstVal(tmp, tmpTarget, tmpInterval);
		HWRITE(CORE_DAC_LVOL, tmp);
		delay_ms(1);
	}
}

// When we need start work, shoul change ana vol to ready state, dc cali need be zero(becouse is will make voice work error)
void Audio_AdacPrepareReadyWork(void)
{
	//DEBUG_LOG(LOG_LEVEL_CORE, "UI" ,"Audio_DacInit: 0x%04X", LOG_POINT_A602, HREAD(CORE_AADC_9));
	//TODO: Current bottom nosie is 10-11uV, no need do this work
	/*
	if(!Audio_AdacCheckAnaVolAlreadyNormal())
	{
		// Step 1: prepare dc cali and digital vol
		Audio_AdacAdjustDCCaliToNormal();
		Audio_AdacAdjustDigitalVolToCali();

		// Step 2: change ana vol to normal
		Audio_AdacAdjustAnaVolToNormal();
	}
	else
	{
		DEBUG_LOG(LOG_LEVEL_CORE, "UI" ,"Audio_DacInit: 0x%04X", LOG_POINT_A610, HREAD(CORE_AADC_9));
	}
	*/
	// Step 3: change dc and vol to zero, becouse dc cali will make voice error
	Audio_AdacAdjustDCCaliToZero();
	Audio_AdacAdjustDigitalVolToZero();
}
void Audio_AdacPreparePAClose(void)
{
	//DEBUG_LOG(LOG_LEVEL_CORE, "UI" ,"Audio_DacInit: 0x%04X", LOG_POINT_A60E, HREAD(CORE_AADC_9));

	// Step 1: prepare dc cali and digital vol
	Audio_AdacAdjustDCCaliToNormal();
	Audio_AdacAdjustDigitalVolToCali();

	//TODO: Current bottom nosie is 10-11uV, no need do this work
	// Step 2: change ana vol to normal
	//Audio_AdacAdjustAnaVolToLimit();
}
void Audio_AdacPrepareSoftMute(void)
{
	//DEBUG_LOG(LOG_LEVEL_CORE, "UI" ,"Audio_DacInit: 0x%04X", LOG_POINT_A603, HREAD(CORE_AADC_9));
	//TODO: Current bottom nosie is 10-11uV, no need do this work
	/*
	// Step 1: prepare dc cali and digital vol
	Audio_AdacAdjustDCCaliToNormal();
	Audio_AdacAdjustDigitalVolToCali();

	// Step 2: change ana vol to normal
	Audio_AdacAdjustAnaVolToLimit();
	*/
}

uint8_t Audio_AdacCheckLastDacClkSame(uint8_t dac_clk)
{
	return gLastStartDacClk == dac_clk;
}



void Audio_CBClosePaDelay(int paras)
{
	//DEBUG_LOG(LOG_LEVEL_CORE, "UI" ,"Audio_DacInit: 0x%04X", LOG_POINT_A622, HREAD(CORE_AADC_9));

	Audio_AdacStopPAWithPop();
}

void Audio_ClosePaDelay(void)
{
	//DEBUG_LOG(LOG_LEVEL_CORE, "UI" ,"Audio_DacInit: 0x%04X", LOG_POINT_A620, HREAD(CORE_AADC_9));
	//SYS_SetTimer(&gClosePaDelayTimer, CLOSE_PA_DELAY,
	//	TIMER_SINGLE, Audio_CBClosePaDelay);
}

void Audio_RelasePaDelay(void)
{
	//DEBUG_LOG(LOG_LEVEL_CORE, "UI" ,"Audio_DacInit: 0x%04X", LOG_POINT_A621, HREAD(CORE_AADC_9));
	//SYS_ReleaseTimer(&gClosePaDelayTimer);
}

void Audio_AvdStopSbcDac(void)
{
	HWRITE(mem_audio_allow, 0);
	Audio_SbcStop();
	Audio_DacStop();
}

void Audio_AnaAdcLowControlDisable(void)
{
	uint32_t tmp = HREADL(mem_lpm_write_temp_adc_low);
	tmp &= ~(1<<da_aadc_en);
	tmp &= ~(1<<da_aadc_en_biasgen);
	tmp &= ~(1<<da_aadc_en_constgm);
	tmp &= ~(1<<da_aadc_en_reg);

	Audio_LpmWrite2AdcLow(tmp);
}

void Audio_AnaAdcLowControlEn(void)
{
	uint32_t tmp = HREADL(mem_lpm_write_temp_adc_low);
	tmp |= (1<<da_aadc_en);
	tmp |= (1<<da_aadc_en_biasgen);
	tmp |= (1<<da_aadc_en_constgm);
	tmp |= (1<<da_aadc_en_reg);
	//tmp &= ~(1<<rg_mic_diff_en);//Disable mic diff
	tmp |= (1<<rg_mic_diff_en);//Enable mic diff

	Audio_LpmWrite2AdcLow(tmp);
}

void Audio_AnaAdcHighControlDisable(void)
{
	uint32_t tmp = HREADL(mem_lpm_write_temp_adc_high);
	tmp &= ~(1<<da_mic_pga_en);
	tmp &= ~(1<<da_mic_bias_en);

	Audio_LpmWrite2AdcHigh(tmp);
}
void Audio_AnaAdcHighControlEn(void)
{
	uint32_t tmp = HREADL(mem_lpm_write_temp_adc_high);
	tmp |= (1<<da_mic_pga_en);
	tmp |= (1<<da_mic_bias_en);

	Audio_LpmWrite2AdcHigh(tmp);
}


void Audio_AnaStartAdcWork(void)
{
	Audio_DaAaddaBgEn();
	Audio_AnaAdcLowControlEn();
	Audio_AnaAdcHighControlEn();
}


void Audio_AnaStopAdcWork(void)
{
	//Audio_DaAaddaBgDisable();  DAC close
	Audio_AnaAdcLowControlDisable();
	Audio_AnaAdcHighControlDisable();
}

BOOL Audio_CheckDacEnable(void)
{
	return ((HREAD(CORE_DAC_CTRL) & BIT_0) != 0); //DAC_ENABLE
}

void Audio_VolumeSwitch(uint8_t volume)
{
	if(volume > 0x80)
		return;
	HWRITE(CORE_DAC_LVOL, volume);//����������С
	HWRITE(CORE_DAC_RVOL, volume);

}


void Audio_VoiceFilterCmd(FunctionalState NewState)
{
	if(NewState)
		HWCORW(CORE_CLKOFF, REG_CLOCK_OFF_VOICE_FILTER);
	else
		HWORW(CORE_CLKOFF, REG_CLOCK_OFF_VOICE_FILTER);

}

uint16_t Audio_GetDacRptr(void)
{
	return HREADW(CORE_DAC_RPTR);
}

uint16_t Audio_GetDacWptr(void)
{
	return  HREADW(CORE_DAC_WPTR);
}

void Audio_UpdateDacWptr(void)
{
	HWRITEW(CORE_DAC_WPTR, (Filter8KTo48KGetWptr() * 2) + HREADW(CORE_DAC_SADDR));
}





