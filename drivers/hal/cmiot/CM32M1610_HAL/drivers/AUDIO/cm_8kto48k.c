  /*
  ***********************************************************************************
  *
  * COPYRIGHT(c) 2021, China Mobile IOT
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of China Mobile IOT nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ***********************************************************************************
  */
#include "cm_8kto48k.h"
#include "cm_uart.h"

#define FILTER_ROW 6
#define FILTER_COL 6

short* gpFilter8kTo48KOut;
uint16_t gFilter8kTo48KOutSize;
uint16_t gFilter8kTo48KOutIndex;
uint8_t gFilter8kTo48KDataIndex;
short gFilter8kTo48KLData[FILTER_COL]={0};
short gFilter8kTo48KRData[FILTER_COL]={0};
int32_t g48KSampleDiff;
const short filter8kTo48KParams[FILTER_ROW][FILTER_COL]=
{-9,-281,1420,4058,824,-236,
-26,-278,2088,3815,347,-171,
-59,-191,2761,3362,10,-108,
-108,10,3362,2761,-191,-59,
-171,347,3815,2088,-278,-26,
-236,824,4058,1420,-281,-9};

void Filter8KTo48KInit(short * dataStartAddr, uint16_t size)
{
	uint8_t i;
	gpFilter8kTo48KOut = dataStartAddr;
	gFilter8kTo48KOutSize = size;
	gFilter8kTo48KOutIndex = 0;
	gFilter8kTo48KDataIndex = 0;

	for(i = 0; i < FILTER_COL; i ++)
	{
		gFilter8kTo48KLData[i] = 0;
		gFilter8kTo48KRData[i] = 0;
	}
	g48KSampleDiff = 0;
}

int16_t gTestData[2];
uint8_t gTestData1[4];
void Filter8KTo48KAddValue(int16_t interp_out)
{

	gpFilter8kTo48KOut[gFilter8kTo48KOutIndex++] = interp_out;
	if(gFilter8kTo48KOutIndex >= gFilter8kTo48KOutSize)
	{
		gFilter8kTo48KOutIndex = 0;
	}

         if(g48KSampleDiff != 0)
         {
		if(g48KSampleDiff > 0)
		{
			g48KSampleDiff --;
			if(gFilter8kTo48KOutIndex != 0)
			{
				gFilter8kTo48KOutIndex --;
			}
			else
			{
				gFilter8kTo48KOutIndex = gFilter8kTo48KOutSize-1;
			}
		}
		else
		{
			g48KSampleDiff ++;
		         gpFilter8kTo48KOut[gFilter8kTo48KOutIndex++] = interp_out;
			if(gFilter8kTo48KOutIndex >= gFilter8kTo48KOutSize)
			{
				gFilter8kTo48KOutIndex = 0;
			}
		}
         }
}
void Filter8KTo48KWork_Mono(short data)
{
	int sum ;
	gFilter8kTo48KLData[gFilter8kTo48KDataIndex] = data;
	for(int row =0; row < FILTER_ROW; row++)
	{
		sum = 0;
		for (int col=0,ind=gFilter8kTo48KDataIndex; col < FILTER_COL; col++)
		{
			sum += filter8kTo48KParams[row][col]*gFilter8kTo48KLData[ind];
			if(ind == 0)
			{
				ind = FILTER_COL-1;
			}
			else
			{
				ind--;
			}
		}

		gpFilter8kTo48KOut[gFilter8kTo48KOutIndex++] = ((sum>>14)*3);
        if(gFilter8kTo48KOutIndex >= gFilter8kTo48KOutSize)
		{
			gFilter8kTo48KOutIndex = 0;
		}
	}

	if ((++gFilter8kTo48KDataIndex) == FILTER_COL)
	{
		gFilter8kTo48KDataIndex = 0;
	}
}

void Filter8KTo48KWork_Stereo(short L_data,short R_data)
{
	int L_sum,R_sum;
	gFilter8kTo48KLData[gFilter8kTo48KDataIndex] = L_data;
	gFilter8kTo48KRData[gFilter8kTo48KDataIndex] = R_data;
	for(int row =0; row < FILTER_ROW; row++)
	{
		L_sum = 0;
		R_sum = 0;
		for (int col=0,ind=gFilter8kTo48KDataIndex; col < FILTER_COL; col++)
		{
			L_sum += filter8kTo48KParams[row][col]*gFilter8kTo48KLData[ind];
			R_sum += filter8kTo48KParams[row][col]*gFilter8kTo48KRData[ind];
			if(ind == 0)
			{
				ind = FILTER_COL-1;
			}
			else
			{
				ind--;
			}
		}

		gpFilter8kTo48KOut[gFilter8kTo48KOutIndex++] = ((L_sum>>14)*3);
        if(gFilter8kTo48KOutIndex >= gFilter8kTo48KOutSize)
		{
			gFilter8kTo48KOutIndex = 0;
		}
		gpFilter8kTo48KOut[gFilter8kTo48KOutIndex++] = ((R_sum>>14)*3);
        if(gFilter8kTo48KOutIndex >= gFilter8kTo48KOutSize)
		{
			gFilter8kTo48KOutIndex = 0;
		}
	}

	if ((++gFilter8kTo48KDataIndex) == FILTER_COL)
	{
		gFilter8kTo48KDataIndex = 0;
	}
}

uint16_t Filter8KTo48KGetWptr()
{
	return gFilter8kTo48KOutIndex;
}

