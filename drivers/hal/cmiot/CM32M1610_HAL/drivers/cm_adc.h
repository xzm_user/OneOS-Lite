  /*
  ***********************************************************************************
  *
  * COPYRIGHT(c) 2021, China Mobile IOT
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of China Mobile IOT nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ***********************************************************************************
  */

#ifndef __CM_ADC_H_
#define __CM_ADC_H_

#include "cm_drv_common.h"

#define FILT_NUM   20
#define REMOVE_NUM 6
#define WAIT_TIME  100

/* ADC Channel select */
typedef enum
{
    ADC_CHANNEL_0 = 0,    // GPIOC, GPIO_Pin_2
    ADC_CHANNEL_1,        // GPIOC, GPIO_Pin_3
    ADC_CHANNEL_2,        // GPIOC, GPIO_Pin_4
    ADC_CHANNEL_3,        // GPIOC, GPIO_Pin_5
    ADC_CHANNEL_4,        // GPIOC, GPIO_Pin_6
    ADC_CHANNEL_5,        // GPIOC, GPIO_Pin_7
    ADC_CHANNEL_6,        // GPIOD, GPIO_Pin_0
    ADC_CHANNEL_7,        // GPIOD, GPIO_Pin_1
}ADC_ChxTypeDef;

/* ADC Mode select */
typedef enum
{
    ADC_GPIO = 0,
    ADC_DIFF,
    ADC_HVIN,
    ADC_VINLPM,
}ADC_ModeTypeDef;

//ADC reference voltage
#ifdef __ADC_RANGE_24__
#define GPIO_LOW_VOLTAGE_REF		1000   //GPIO reference low voltage    1 V
#define GPIO_HIGH_VOLTAGE_REF		2000  //GPIO reference high voltage    2 V
#else
#define GPIO_LOW_VOLTAGE_REF		500    //GPIO reference low voltage   0.5V
#define GPIO_HIGH_VOLTAGE_REF		1000   //GPIO reference high voltage   1 V
#endif

#define HVIN_LOW_VOLTAGE_REF		3000   //hvin reference low voltage    3 V
#define HVIN_HIGH_VOLTAGE_REF		5000   //hvin reference high voltage   5 V
#define VINLPM_LOW_VOLTAGE_REF      2000   //VINLPM reference low voltage  2 V
#define VINLPM_HIHG_VOLTAGE_REF     3000   //VINLPM reference high voltage 3 V

/* ADC Struct Define*/
typedef struct _ADC_InitTypeDef
{
    ADC_ChxTypeDef  ADC_Channel;
    ADC_ModeTypeDef ADC_Mode;
} ADC_InitTypeDef;

/**
 * @brief Initialize adc module
 * @param ADC_InitStruct : piont to ADC_InitTypeDef struct
 * @retval none
 */

void ADC_Init(ADC_InitTypeDef *ADC_InitStruct);

/**
 * @brief  Get voltage
 * @param  none
 * @retval -1:fail;  other:adc voltage (mV);
 */
int ADC_GetVoltage(void);

/**
 * @brief  Read ADC register value
 * @param  none
 * @retval adc register value;
 */
uint16_t ADC_GetResult(void);

#endif

