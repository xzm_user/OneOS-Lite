  /*
  ***********************************************************************************
  *
  * COPYRIGHT(c) 2021, China Mobile IOT
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of China Mobile IOT nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ***********************************************************************************
  */

#include "cm_uart.h"
#include "cm_sysctrl.h"
#define uart_DMA_buf_len	512
uint8_t uart_TxIT[2] = {0};

uint8_t uartA_TX_buf[uart_DMA_buf_len] = {0};
uint8_t uartA_RX_buf[uart_DMA_buf_len] = {0};
uint8_t uartB_TX_buf[uart_DMA_buf_len] = {0};
uint8_t uartB_RX_buf[uart_DMA_buf_len] = {0};

uint32_t baudtestnum = 0;

typedef struct
{
    uint16_t Baudrate;
    uint16_t RxSadr;
    uint16_t RxEadr;
    uint16_t RxRptr;
    uint16_t TxSadr;
    uint16_t TxEadr;
    uint16_t TxWptr;
}UartxRegDef;

typedef struct
{
    UartxRegDef rbu;
    uint8_t cbu;
}UartxRegControlBackup;

#define UARTC_BIT_ENABLE (1<<0)
#define BAUD_USE_SETTING (1<<7)
#define UART_PER_NUM 2
UartxRegControlBackup regBeck[UART_PER_NUM];

void USART_Init(USART_TypeDef USARTx, USART_InitTypeDef* USART_InitStruct)
{
    uint8_t CtrValue = 0;
    void *Ptr = NULL;
    uint16_t UartxCtrlAdr = 0;
    UartxRegDef *UartAdr = NULL;

     /*check parameter*/
    _ASSERT(USART_InitStruct != NULL);
    _ASSERT(IS_USARTAB(USARTx));
    _ASSERT(IS_UARTE_BAUDRATE(USART_InitStruct->USART_BaudRate));
    _ASSERT(IS_USART_WORD_LENGTH(USART_InitStruct->USART_WordLength));
    _ASSERT(IS_USART_STOPBITS(USART_InitStruct->USART_StopBits));
    _ASSERT(IS_USART_PARITY(USART_InitStruct->USART_Parity));
    _ASSERT(IS_USART_MODE(USART_InitStruct->USART_Mode));
    _ASSERT(IS_USART_HARDWARE_FLOW_CONTROL(USART_InitStruct->USART_HardwareFlowControl));

    uint16_t baud = 0;
    if(HREAD(mem_xtal_flag) == 0)
    {
        baud =SYSCTRL_GetRcClk()/2/USART_InitStruct->USART_BaudRate;//calculate reg num for baudrate

    }else
    {
        if (HREAD(CORE_UART_CLKSEL)& 1)
            baud = 48000000/USART_InitStruct->USART_BaudRate;
        else
            baud = 24000000/USART_InitStruct->USART_BaudRate;
    }
    baud = baud|0x8000;			//use MRAM
    USART_DeInit(USARTx);

    SYSCTRL_AHBPeriphClockCmd(SYSCTRL_AHBPeriph_UART, ENABLE);
    if (USARTx == UARTA) {
    /*init tx ring buffer backup*/
    Ptr = uartA_TX_buf;
    regBeck[USARTx].rbu.TxSadr = (uint32_t)Ptr;
    regBeck[USARTx].rbu.TxEadr = (uint32_t)Ptr + (sizeof(uartA_TX_buf)-1);
        if (ram_check(Ptr)>0)//init different BaudRate for different ram
        {
            regBeck[USARTx].rbu.Baudrate = baud;
        }else
        {
            regBeck[USARTx].rbu.Baudrate = baud & ~(UArt_Baud_Ram_Sele);
        }
    /*init rx ring buffer backup*/
    Ptr = uartA_RX_buf;
    regBeck[USARTx].rbu.RxSadr = (uint32_t)Ptr;
    regBeck[USARTx].rbu.RxEadr = (uint32_t)Ptr + (sizeof(uartA_RX_buf)-1);
    }
    else {
        /*init tx ring buffer backup*/
        Ptr = uartB_TX_buf;
        regBeck[USARTx].rbu.TxSadr = (uint32_t)Ptr;
        regBeck[USARTx].rbu.TxEadr = (uint32_t)Ptr + (sizeof(uartB_TX_buf)-1);
        if (ram_check(Ptr)>0)
        {
            regBeck[USARTx].rbu.Baudrate = baud;
        }else
        {
            regBeck[USARTx].rbu.Baudrate = baud & ~(UArt_Baud_Ram_Sele);
        }
    /*init rx ring buffer backup*/
        Ptr = uartB_RX_buf;
        regBeck[USARTx].rbu.RxSadr = (uint32_t)Ptr;
        regBeck[USARTx].rbu.RxEadr = (uint32_t)Ptr + (sizeof(uartB_RX_buf)-1);
    }

    CtrValue =  USART_InitStruct->USART_Mode|\
                            USART_InitStruct->USART_HardwareFlowControl|\
                            USART_InitStruct->USART_Parity|\
                            USART_InitStruct->USART_StopBits|\
                            USART_InitStruct->USART_WordLength|\
                            BAUD_USE_SETTING|UARTC_BIT_ENABLE;

    regBeck[USARTx].cbu = CtrValue;

    if(USARTx == UARTA) {
        UartxCtrlAdr = CORE_UART_CTRL;
        UartAdr = (UartxRegDef *)(reg_map(CORE_UART_BAUD));
    }
    else {
        UartxCtrlAdr = CORE_UARTB_CTRL;
        UartAdr = (UartxRegDef *)(reg_map(CORE_UARTB_BAUD));
    }

    HWCOR(UartxCtrlAdr, 1);

    /*init all reg by backup*/
    HWRITEW(((uint32_t)(&UartAdr->Baudrate)), regBeck[USARTx].rbu.Baudrate);
    HWRITEW(((uint32_t)(&UartAdr->TxSadr)), (uint32_t)regBeck[USARTx].rbu.TxSadr);
    HWRITEW(((uint32_t)(&UartAdr->TxEadr)), (uint32_t)regBeck[USARTx].rbu.TxEadr);
    HWRITEW(((uint32_t)(&UartAdr->TxWptr)), (uint32_t)regBeck[USARTx].rbu.TxSadr);
    HWRITEW(((uint32_t)(&UartAdr->RxSadr)), (uint32_t)regBeck[USARTx].rbu.RxSadr);
    HWRITEW(((uint32_t)(&UartAdr->RxEadr)), (uint32_t)regBeck[USARTx].rbu.RxEadr);
    HWRITEW(((uint32_t)(&UartAdr->RxRptr)), (uint32_t)regBeck[USARTx].rbu.RxSadr);
    HWRITE(UartxCtrlAdr, regBeck[USARTx].cbu);
}

void USART_DeInit(USART_TypeDef USARTx)
{
    _ASSERT(IS_USARTAB(USARTx));
    if(USARTx == UARTA) {
        HWOR(reg_map(CORE_UART_CTRL), (1<<0));
        HWOR(reg_map(CORE_UART_CTRL), (0<<0));
    }else {
        HWOR(reg_map(CORE_UARTB_CTRL), (1<<0));
        HWOR(reg_map(CORE_UARTB_CTRL), (0<<0));
    }
}

void USART_SendData(USART_TypeDef USARTx, uint8_t Data)
{
    UartxRegDef * UartAdr = NULL;
    uint16_t  WPtr = 0;
    uint32_t  TxITEMS=0;
    _ASSERT(IS_USARTAB(USARTx));

    if(USARTx == UARTA) {
        UartAdr = (UartxRegDef *)(reg_map(CORE_UART_BAUD));
        TxITEMS = CORE_UART_TX_ITEMS;
    }else {
        UartAdr = (UartxRegDef *)(reg_map(CORE_UARTB_BAUD));
        TxITEMS = CORE_UARTB_TX_ITEMS;
    }
    _ASSERT((&UartAdr->TxSadr  != NULL));
    WPtr = HREADW((uint32_t)(&UartAdr->TxWptr));

    while(HREAD(TxITEMS) > 0);

    HWRITE(WPtr|M0_MEMORY_BASE,Data);
    RB_UPDATE_PTR(WPtr, HREADW((uint32_t)(&UartAdr->TxSadr)), HREADW((uint32_t)(&UartAdr->TxEadr)));

    HWRITEW((uint32_t)(&UartAdr->TxWptr), (uint32_t)(WPtr));

    if(uart_TxIT[USARTx] == ENABLE)
    {
        uint8_t tx_IT;
        if(USARTx == UARTA)
        {
            tx_IT= HREAD(CORE_UART_CTRL);
            tx_IT |= BIT_5;
            HWRITE(reg_map(CORE_UART_CTRL),tx_IT);
        }
        else
        {
            tx_IT= HREAD(CORE_UARTB_CTRL);
            tx_IT |= BIT_5;
            HWRITE(reg_map(CORE_UARTB_CTRL),tx_IT);
        }
    }
    else
    {
        while(HREADW(reg_map(TxITEMS)));
    }
}

uint8_t USART_ReceiveData(USART_TypeDef USARTx)
{
    UartxRegDef *UartAdr = NULL;
    uint16_t  RPtr = 0;
    uint8_t  RdData = 0;
    _ASSERT(IS_USARTAB(USARTx));

    if(USARTx == UARTA) {
        UartAdr = (UartxRegDef *)(reg_map(CORE_UART_BAUD));
    }else {
        UartAdr = (UartxRegDef *)(reg_map(CORE_UARTB_BAUD));
    }
    RPtr = HREADW((uint32_t)(&UartAdr->RxRptr));
    RdData = HREADW(RPtr|M0_MEMORY_BASE);
    RB_UPDATE_PTR(RPtr, HREADW((uint32_t)(&UartAdr->RxSadr)), HREADW((uint32_t)(&UartAdr->RxEadr)));
    HWRITEW((uint32_t)(&UartAdr->RxRptr), RPtr);
    return RdData;
}

uint16_t USART_ReadBuff(USART_TypeDef USARTx, uint8_t* RxBuff, uint16_t RxSize)
{
    uint16_t RxLen = 0;
        uint16_t RPtr = 0;
        uint16_t RdataLen = 0;
       uint32_t RxITEMS = 0;
    UartxRegDef *UartAdr = NULL;

    _ASSERT(IS_USARTAB(USARTx));
    _ASSERT(RxBuff != NULL);

    if(USARTx == UARTA) {
        UartAdr = (UartxRegDef *)(reg_map(CORE_UART_BAUD));
        RxITEMS = reg_map(CORE_UART_RX_ITEMS);
    }else {
        UartAdr = (UartxRegDef *)(reg_map(CORE_UARTB_BAUD));
        RxITEMS = reg_map(CORE_UARTB_RX_ITEMS);
    }
    RxLen = HREADW(reg_map(RxITEMS));
    if (RxSize!=0) {
        if (RxLen < RxSize) return 0; else RxLen = RxSize;
    }
     if (0 == RxLen) {
                return 0;
        } else {
            RPtr = HREADW((uint32_t)(&UartAdr->RxRptr));
            for(RdataLen = 0; RdataLen<RxLen; RdataLen++ )
             {
                RxBuff[RdataLen] = HREAD(RPtr|M0_MEMORY_BASE);
                RB_UPDATE_PTR(RPtr, HREADW((uint32_t)(&UartAdr->RxSadr)), HREADW((uint32_t)(&UartAdr->RxEadr)));
             }
        }
    HWRITEW((uint32_t)(&UartAdr->RxRptr), (RPtr));
    return RdataLen;
}

uint16_t USART_SendBuff(USART_TypeDef USARTx, uint8_t* TxBuff, uint16_t TxLen)
{
    uint16_t  WPtr = 0;
    uint16_t  SDataLen = 0;
    uint32_t  TxITEMS=0;
    uint16_t i;
    UartxRegDef *UartAdr = NULL;

    _ASSERT(IS_USARTAB(USARTx));
    _ASSERT(TxBuff != 0);
    _ASSERT(TxLen > 0);

    if(USARTx == UARTA) {
        UartAdr = (UartxRegDef *)(reg_map(CORE_UART_BAUD));
        TxITEMS = CORE_UART_TX_ITEMS;
    }else {
        UartAdr = (UartxRegDef *)(reg_map(CORE_UARTB_BAUD));
        TxITEMS = CORE_UARTB_TX_ITEMS;
    }
       WPtr = HREADW((uint32_t)(&UartAdr->TxWptr));
       for ( i=0; i<TxLen; i++)  {

        HWRITE(WPtr|M0_MEMORY_BASE,TxBuff[i]);
        RB_UPDATE_PTR(WPtr, HREADW((uint32_t)(&UartAdr->TxSadr)),  HREADW((uint32_t)(&UartAdr->TxEadr)));
        SDataLen++;
    }

    HWRITEW((uint32_t)(&UartAdr->TxWptr),  WPtr);
    if(uart_TxIT[USARTx] == ENABLE)
    {
        uint8_t tx_IT;
        if(USARTx == UARTA)
        {
            tx_IT= HREAD(CORE_UART_CTRL);
            tx_IT |= BIT_5;
            HWRITE(reg_map(CORE_UART_CTRL),tx_IT);
        }
        else
        {
            tx_IT= HREAD(CORE_UARTB_CTRL);
            tx_IT |= BIT_5;
            HWRITE(reg_map(CORE_UARTB_CTRL),tx_IT);
        }
    }
    else
    {
        while(HREADW(reg_map(TxITEMS)));
    }
    return SDataLen;
}

Boolean USART_IsRXFIFONotEmpty(USART_TypeDef USARTx)
{
    if(USARTx == UARTA)
    {
         return (Boolean)HREADW(reg_map(CORE_UART_RX_ITEMS));
    }
    else
    {
         return (Boolean)HREADW(reg_map(CORE_UARTB_RX_ITEMS));
    }
}

uint16_t USART_GetRxCount(USART_TypeDef USARTx)
{
    _ASSERT(IS_USARTAB(USARTx));
    if(USARTx == UARTA) {
        return HREADW(reg_map(CORE_UART_RX_ITEMS));
    }else {
        return HREADW(reg_map(CORE_UARTB_RX_ITEMS));
    }
}

void USART_SetRxITNum(USART_TypeDef USARTx, uint8_t Num)
{
    _ASSERT(IS_USARTAB(USARTx));
    if(USARTx == UARTA)
    {
        HWRITE(reg_map(CORE_UART_RXINTER),Num);
    }
    else
    {
        HWRITE(reg_map(CORE_UARTB_RXINTER),Num);
    }
}

void USART_SetRxTimeout(USART_TypeDef USARTx, uint16_t time)
{
    _ASSERT(IS_USARTAB(USARTx));
    if(USARTx == UARTA)
    {
        HWRITEW(reg_map(CORE_UART_TIMEOUT),time);
    }
    else
    {
        HWRITEW(reg_map(CORE_UARTB_TIMEOUT),time);
    }
}

void USART_TxITConfig(USART_TypeDef USARTx, FunctionalState NewState)
{
    _ASSERT(IS_USARTAB(USARTx));
    uart_TxIT[USARTx] = NewState;
}

void USART_ClearTxITPendingBit(USART_TypeDef USARTx)
{
    _ASSERT(IS_USARTAB(USARTx));
    uint8_t tx_IT;
    if(USARTx == UARTA)
    {
        tx_IT= HREAD(CORE_UART_CTRL);
        tx_IT &= ~BIT_5;
        HWRITE(reg_map(CORE_UART_CTRL),tx_IT);
    }
    else
    {
        tx_IT= HREAD(CORE_UARTB_CTRL);
        tx_IT &= ~BIT_5;
        HWRITE(reg_map(CORE_UARTB_CTRL),tx_IT);
    }
}

Boolean UART_IsTxIRQ(USART_TypeDef USARTx)
{
    _ASSERT(IS_USARTAB(USARTx));
    if(USARTx == UARTA)
    {
        if ((HREAD(CORE_UART_CTRL) & BIT_5) && (HREADW(reg_map(CORE_UART_TX_ITEMS))==0))
        {
           return TRUE;
        }
        else
        {
            return FALSE;
        }
    }
    else
    {
        if ((HREAD(CORE_UARTB_CTRL) & BIT_5) && (HREADW(reg_map(CORE_UARTB_TX_ITEMS))==0))
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }
}

