  /*
  ***********************************************************************************
  *
  * COPYRIGHT(c) 2021, China Mobile IOT
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of China Mobile IOT nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ***********************************************************************************
  */

#include "cm_gpio.h"

void GPIO_Config(GPIO_TypeDef GPIOx, uint8_t GPIO_Pin, GPIO_FunTypeDef function)
{
    _ASSERT(ISGPIOGROUP(GPIOx));

    int i=0;
    for (i = 0; i < GPIO_PIN_NUM; i++)
    {
        if (GPIO_Pin & 1 << i)
        {
            HWRITE(CORE_GPIO_CONF+GPIOx*GPIO_PIN_NUM+i,function);
        }
    }

}

void GPIO_Init(GPIO_TypeDef GPIOx, GPIO_InitTypeDef *GPIO_InitStruct)
{
    _ASSERT(ISGPIOGROUP(GPIOx));
    _ASSERT(IS_GPIO_PIN(GPIO_InitStruct->GPIO_Pin));
    _ASSERT(IS_GPIO_MODE(GPIO_InitStruct->GPIO_Mode));

    int i=0;
    switch (GPIO_InitStruct->GPIO_Mode)
    {
    case GPIO_Mode_IN_FLOATING:
        for (i = 0; i < GPIO_PIN_NUM; i++)
        {
            if (GPIO_InitStruct->GPIO_Pin & 1 << i)
            {
                HWRITE(CORE_GPIO_CONF+GPIOx*GPIO_PIN_NUM+i,0x00);
            }
        }
        break;
    case GPIO_Mode_IPU:
        for (i = 0; i < GPIO_PIN_NUM; i++)
        {
            if (GPIO_InitStruct->GPIO_Pin & 1 << i)
            {
                HWRITE(CORE_GPIO_CONF+GPIOx*GPIO_PIN_NUM+i,0x40);
            }
        }
        break;
    case GPIO_Mode_IPD:
        for (i = 0; i < GPIO_PIN_NUM; i++)
        {
            if (GPIO_InitStruct->GPIO_Pin & 1 << i)
            {
                HWRITE(CORE_GPIO_CONF+GPIOx*GPIO_PIN_NUM+i,0x80);
            }
        }
        break;
    case GPIO_Mode_AIN:
        for (i = 0; i < GPIO_PIN_NUM; i++)
        {
            if (GPIO_InitStruct->GPIO_Pin & 1 << i)
            {
                HWRITE(CORE_GPIO_CONF+GPIOx*GPIO_PIN_NUM+i,0xc0);
            }
        }
        break;
    case GPIO_Mode_Out_PP:
        for (i = 0; i < GPIO_PIN_NUM; i++)
        {
            if (GPIO_InitStruct->GPIO_Pin & 1 << i)
            {
                HWRITE(CORE_GPIO_CONF+GPIOx*GPIO_PIN_NUM+i,0x3e);
            }
        }
        break;
    default:
        break;
    }
}

void GPIO_PullUpCmd(GPIO_TypeDef GPIOx, uint8_t GPIO_Pin, FunctionalState NewState)
{
    _ASSERT(ISGPIOGROUP(GPIOx));

    int i=0;
    for (i = 0; i < GPIO_PIN_NUM; i++)
    {
        if (GPIO_Pin & 1 << i)
        {
            if (NewState == ENABLE)
            {
                HWRITE(CORE_GPIO_CONF+GPIOx*GPIO_PIN_NUM+i,GPIO_MODE_PULLUP);
            }
            else if (NewState == DISABLE)
            {
                HWRITE(CORE_GPIO_CONF+GPIOx*GPIO_PIN_NUM+i,GPIO_MODE_INPUT);
            }
        }
    }
}

void GPIO_PullDownCmd(GPIO_TypeDef GPIOx, uint8_t GPIO_Pin, FunctionalState NewState)
{
    _ASSERT(ISGPIOGROUP(GPIOx));

    int i=0;
    for (i = 0; i < GPIO_PIN_NUM; i++)
    {
        if (GPIO_Pin & 1 << i)
        {
            if (NewState == ENABLE)
            {
                HWRITE(CORE_GPIO_CONF+GPIOx*GPIO_PIN_NUM+i,GPIO_MODE_PULLDOWN);
            }
            else if (NewState == DISABLE)
            {
                HWRITE(CORE_GPIO_CONF+GPIOx*GPIO_PIN_NUM+i,GPIO_MODE_INPUT);
            }
        }
    }
}

uint8_t GPIO_ReadData(GPIO_TypeDef GPIOx)
{
    _ASSERT(ISGPIOGROUP(GPIOx));
    return (HREAD(CORE_GPIO_IN+GPIOx));
}

uint8_t GPIO_ReadDataBit(GPIO_TypeDef GPIOx, uint8_t GPIO_Pin)
{
    _ASSERT(ISGPIOGROUP(GPIOx));
    _ASSERT(IS_GET_GPIO_PIN(GPIO_Pin));

    if(GPIO_ReadData(GPIOx) & (GPIO_Pin) )
    {
        return 0x01;
    }
    else
    {
        return 0x00;
    }
}

void GPIO_ResetBits(GPIO_TypeDef GPIOx, uint8_t GPIO_Pin)
{
    _ASSERT(ISGPIOGROUP(GPIOx));
    _ASSERT(IS_GET_GPIO_PIN(GPIO_Pin));

    int i=0;
    for (i = 0; i < GPIO_PIN_NUM; i++)
    {
        if (GPIO_Pin & 1 << i)
        {
            HWRITE(CORE_GPIO_CONF + GPIOx*GPIO_PIN_NUM+i,GPIO_MODE_OUTPUT_LOW);
        }
    }
}

void GPIO_SetBits(GPIO_TypeDef GPIOx, uint8_t GPIO_Pin)
{
    _ASSERT(ISGPIOGROUP(GPIOx));
    _ASSERT(IS_GET_GPIO_PIN(GPIO_Pin));

    int i=0;
    for (i = 0; i < GPIO_PIN_NUM; i++)
    {
        if (GPIO_Pin & 1 << i)
        {
            HWRITE(CORE_GPIO_CONF + GPIOx*GPIO_PIN_NUM+i,GPIO_MODE_OUTPUT_HIGH);
        }
    }
}

void GPIO_Write(GPIO_TypeDef GPIOx, uint8_t GPIO_Pin)
{
    _ASSERT(ISGPIOGROUP(GPIOx));

    int i=0;

    for(i = 0; i < GPIO_PIN_NUM; ++i)
    {
        if(GPIO_Pin & 1 << i)
        {
            GPIO_SetBits(GPIOx,1 << i);
        }
        else
        {
            GPIO_ResetBits(GPIOx,1 << i);
        }
    }
}

void GPIO_WriteBit(GPIO_TypeDef GPIOx, uint8_t GPIO_Pin, BitAction BitVal)
{
    _ASSERT(ISGPIOGROUP(GPIOx));
    _ASSERT(IS_GET_GPIO_PIN(GPIO_Pin));

    if (BitVal == Bit_SET)
    {
        GPIO_SetBits(GPIOx, GPIO_Pin);
    }
    else if (BitVal == Bit_RESET)
    {
        GPIO_ResetBits(GPIOx, GPIO_Pin);
    }
}

