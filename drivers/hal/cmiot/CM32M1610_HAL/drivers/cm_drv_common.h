    /*
  ***********************************************************************************
  *
  * COPYRIGHT(c) 2021, China Mobile IOT
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of China Mobile IOT nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ***********************************************************************************
  */

#ifndef __CM_DRV_COMMON_H_
#define __CM_DRV_COMMON_H_

#include <string.h>
#include "cm1610.h"
#include "cm_def.h"
#include "cm_type.h"

#ifdef __cplusplus
extern "C" {
#endif

void HW_REG_24BIT(uint32_t reg, uint32_t data);

/**
  *@brief Writing hardware register.
  *@param reg register.
  *@param word value.
  *@return None.
  */
void HW_REG_16BIT(uint32_t reg, uint16_t word);

/**
  *@brief Reading hardware register.
  *@param reg register.
  *@return The register 16-bit value.
  */
uint16_t HR_REG_16BIT(uint32_t reg);

/**
  *@brief Reading hardware register.
  *@param reg register.
  *@return The register 24_bit value.
  */
uint32_t HR_REG_24BIT(uint32_t reg);

#define M0_LPM_REG mem_m0_lpm_flag

#define NONE_LPM_FLAG 0x00
#define KEY_LPM_FLAG 0x01
#define VP_LPM_FLAG 0x02
#define LED_LPM_FLAG 0x04
#define LINK_LPM_FLAG 0x10
#define CHARGER_LPM_FLAG 0x20
#define M0_LPM_FLAG 0x40
#define HIBER_LPM_FLAG 0x80
#define IGNORE_LPM_FLAG (BT_POWERON_LINK_PERFORMANCE_LPM_FLAG|LINK_LPM_FLAG|HIBER_LPM_FLAG)
#define PWM_LPM_FLAG 0x100
#define TWS_SYNC_PWR_OFF_LPM_FLAG 0x200
#define IPHONE_INSTORAGE_LPM_FLAG 0x400
#define OAL_LPM_FLAG	0x800
#define BT_POWERON_LINK_PERFORMANCE_LPM_FLAG	0x1000
#define APP_FIRST_POWERON_LPM_FLAG	0x2000

void error_handle(void);
void xmemcpy(uint8_t* dest,const uint8_t* src, uint16_t len);

void Lpm_LockLpm(uint16_t lpmNum);
void Lpm_unLockLpm(uint16_t lpmNum);
BOOL Lpm_CheckLpmFlag(void);
void whileDelay(int delayValue);
void whileDelayshort(int delayValue);

BOOL xramcmp(volatile uint8_t *op1,volatile uint8_t *op2, int len);
void xramcpy(volatile uint8_t *des,volatile uint8_t *src,int len);
uint32_t math_abs(int32_t value);
#ifdef __cplusplus
}
#endif

#endif

