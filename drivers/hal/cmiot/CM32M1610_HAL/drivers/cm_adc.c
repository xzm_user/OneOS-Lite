  /*
  ***********************************************************************************
  *
  * COPYRIGHT(c) 2021, China Mobile IOT
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of China Mobile IOT nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ***********************************************************************************
  */
#include "cm1610.h"
#include "cm_adc.h"
#include "cm_drv_common.h"
#include "cm_timer.h"
#include "cm_sysctrl.h"

static int calculate_voltage(int result, int vol_high_ref, int vol_low_ref, int vol_high, int vol_low);

void ADC_Init(ADC_InitTypeDef *ADC_InitStruct)
{
    //set adc channel
    uint8_t adc_ch,adc_en,rfen;
    adc_ch=HREAD(RF_ADC_CH);
    adc_ch &= 0xf8;
    adc_ch |=	ADC_InitStruct->ADC_Channel;
    HWRITE(RF_ADC_CH, adc_ch);

    //set adc mode
    HWRITE(RF_ADC_MODE , 0x8f|(ADC_InitStruct->ADC_Mode)<<4);

    // adc enable
    adc_en = HREAD(CORE_SBC_CTRL);
    adc_en |= 0x80;
    HWRITE(CORE_SBC_CTRL,adc_en);

    rfen=HREAD(RFEN_ADC);
    rfen |= 0x7c;
    HWRITE(RFEN_ADC,rfen);

    #ifdef __ADC_RANGE_24__
    HWRITE(RF_ADC_GC, 0xf9);
    #else
    HWRITE(RF_ADC_GC, 0xaa);
    #endif
    delay_us(WAIT_TIME);
}

static int calculate_voltage(int adc_value, int vol_high_ref,int vol_low_ref , int vol_high, int vol_low)
{
    int rega,regb,regc;
    rega = adc_value - vol_low_ref;
    regb = vol_high_ref - vol_low_ref;
    regc = vol_high - vol_low;
    return ((rega*regc)/regb + vol_low);
}

int ADC_GetVoltage(void)
{
    uint8_t mode;
    uint16_t voltage_low  = 0;
    uint16_t voltage_high = 0;
    uint16_t reg_voltage_low = 0;
    uint16_t ref_voltage_high = 0;
    uint16_t result=0;
    mode = ( HREAD(RF_ADC_MODE) & 0x70) >> 4;

    switch (mode)
    {
        case ADC_GPIO:
            #ifdef __ADC_RANGE_24__
            reg_voltage_low = HREADW(mem_1v_24_adc_io_data) ;
            ref_voltage_high = HREADW(mem_2v_24_adc_io_data );
            voltage_low = GPIO_LOW_VOLTAGE_REF;
            voltage_high =GPIO_HIGH_VOLTAGE_REF;
            #else
            reg_voltage_low = HREADW(mem_0_5_adc_io_data);
            ref_voltage_high = HREADW(mem_1v_adc_io_data);
            voltage_low = GPIO_LOW_VOLTAGE_REF;
            voltage_high = GPIO_HIGH_VOLTAGE_REF;
            #endif
            break;
        case ADC_HVIN:
            reg_voltage_low = HREADW(mem_3v_adc_hvin_data);
            ref_voltage_high = HREADW(mem_5v_adc_hvin_data);
            voltage_low = HVIN_LOW_VOLTAGE_REF;
            voltage_high = HVIN_HIGH_VOLTAGE_REF;
            break;
        case ADC_VINLPM:
            reg_voltage_low = HREADW(mem_3v_adc_hvin_data);
            ref_voltage_high = HREADW(mem_5v_adc_hvin_data);
            voltage_low = VINLPM_LOW_VOLTAGE_REF;
            voltage_high = VINLPM_HIHG_VOLTAGE_REF;
            break;
        default:
            return -1;
    }

    uint16_t result_buf[FILT_NUM]={0};
    for(uint8_t i=0; i<FILT_NUM; i++)
    {
        result =  ADC_GetResult();
         result_buf[i] = calculate_voltage(result, ref_voltage_high, reg_voltage_low, voltage_high, voltage_low);;
    }
    result = 0;
    for (uint8_t i=0; i<FILT_NUM; i++)
    {
         for(uint8_t j=0; j<i; j++)
        {
             if(result_buf[i] < result_buf[j])
            {
                uint16_t temp = result_buf[i];
                result_buf[i] = result_buf[j];
                result_buf[j] = temp;
            }
        }
    }
    for (uint8_t i=REMOVE_NUM; i<FILT_NUM - REMOVE_NUM; i++)
    {
        result+=result_buf[i];
    }
    result = result/(FILT_NUM - 2 * REMOVE_NUM);
    return result;
}

uint16_t ADC_GetResult(void)
{
    uint16_t adc_value;
    adc_value = HREADW(CORE_SUMDATA);
    return adc_value;
}

