  /*
  ***********************************************************************************
  *
  * COPYRIGHT(c) 2021, China Mobile IOT
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of China Mobile IOT nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ***********************************************************************************
  */

#include "cm_timer.h"
#include "cm_sysctrl.h"

#define BIT_MODULE_ENABLE   0x20
#define BIT_TIMER_MODE      0x40
#define BIT_AUTO_RELOAD     0x80

void TIM_DeInit(TIM_NumTypeDef TIMx)
{
    HWRITE(CORE_PWM_CTRL(TIMx),0);
    HWCOR(CORE_PWM_CLKEN, 1<<TIMx);
}

void TIM_Init(TIM_InitTypeDef* TIM_init_struct)
{
    uint8_t pwm_ctrl = 0;
    HWOR(CORE_PWM_CLKEN, 1<<TIM_init_struct->TIMx);
    pwm_ctrl |= BIT_TIMER_MODE;
    pwm_ctrl |= BIT_AUTO_RELOAD;
    pwm_ctrl |= TIM_init_struct->module_clk_div;

    HWRITEW(reg_map(CORE_PWM_PCNT(TIM_init_struct->TIMx)), TIM_init_struct->period);
    HWRITE(reg_map(CORE_PWM_CTRL(TIM_init_struct->TIMx)), pwm_ctrl);

}

void TIM_SetPeriod(TIM_NumTypeDef TIMx, uint16_t Period)
{
    HWRITEW(reg_map(CORE_PWM_PCNT(TIMx)),Period);
}

void TIM_Cmd(TIM_NumTypeDef TIMx, FunctionalState NewState)
{
    uint8_t pwm_ctrl;

    if (NewState == ENABLE)
    {
         HWOR(CORE_PWM_CTRL(TIMx), BIT_MODULE_ENABLE);
    }
    else
    {
         HWCOR(CORE_PWM_CTRL(TIMx), BIT_MODULE_ENABLE);
         pwm_ctrl = HREAD(CORE_PWM_CTRL(TIMx));
         HWOR(CORE_PWM_CTRL(TIMx), BIT_TIMER_MODE);
         HWRITE(CORE_PWM_CTRL(TIMx),pwm_ctrl);
    }
}

void TIM_PwmInit(PWM_InitTypeDef* Pwm_init_struct)
{
    uint8_t pwm_ctrl = 0;
    HWOR(CORE_PWM_CLKEN, 1<<Pwm_init_struct->TIMx);
    pwm_ctrl |= Pwm_init_struct->StartLevel;
    pwm_ctrl |= Pwm_init_struct->Sync;
    pwm_ctrl |= Pwm_init_struct->module_clk_div;

    HWRITEW(reg_map(CORE_PWM_PCNT(Pwm_init_struct->TIMx)), Pwm_init_struct->HighLevelPeriod);
    HWRITEW(reg_map(CORE_PWM_NCNT(Pwm_init_struct->TIMx)), Pwm_init_struct->LowLevelPeriod);
    HWRITE(reg_map(CORE_PWM_CTRL(Pwm_init_struct->TIMx)), pwm_ctrl);
}

uint32_t  TIM_getModuleClk(void)
{
    uint8_t clksource = HREAD(CORE_CLKSEL) & 0x03;
    if (clksource == 0)
        return 24*MHz;

    else if (clksource == 1)
        return 48*MHz;
    else
        return SYSCTRL_GetRcClk();
}

