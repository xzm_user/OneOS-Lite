  /*
  ***********************************************************************************
  *
  * COPYRIGHT(c) 2021, China Mobile IOT
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of China Mobile IOT nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ***********************************************************************************
  */
#ifndef __CM_LPM_H__
#define __CM_LPM_H__

#include "cm1610.h"
// lpm_write

typedef enum
{
LPM_CTRL,
LPM_CTRL2,
LPM_GPIOLOW,
LPM_GPIOHITH,
LPM_SLEEP_COUNTER,
LPM_BUCK_CFG,
LPM_CHARGE_CTRL1,
LPM_XTAL_CTRL,
LPM_ENTER_SCAN_MODE,
LPM_CHARGER_LOW_HIGH_WAKEUP_REG,
LPM_VOICE_DETECT_REG,
LPM_GBG_CTRL_REG,
LPM_ADC_LOW,
LPM_ADC_HIGH
} LPM_TypeDef;

typedef enum
{
LPM_MCU_STATE_RUNNING,
LPM_MCU_STATE_HIBERNATE,
LPM_MCU_STATE_LMP,
LPM_MCU_STATE_STOP
} LPM_STATE;

/**
  * @brief	Read the value in the LPM register, 32 bits at a time
  * @param	CORE_LPM_CTRL or CORE_LPM_XTALCNT or CORE_LPM_BUCK_BYPASS or CORE_GPIO_WAKEUP_LOW or CORE_GPIO_WAKEUP_HIGH
  * @retval 	The value in the register
  */
uint32_t LPM_Read(uint16_t addr);

/**
  * @brief	The value written into the LPM register, 32 bits at a time
  * @param	LPM_TypeDef and val
  * @retval 	none
  */
void LPM_Write(LPM_TypeDef type, uint32_t val);

/**
  * @brief  Drop down all unused GPIO
  * @param  None
  * @retval None
  */
void LPM_GpioUnusedPd(void);

/**
  * @brief  lpm_sleep config
  * @param  time: Wake up time(T=time*31.25us)      0: = 0xffffffff : 37h
  * @retval None
  */
void LPM_Sleep(uint32_t time);

/*
 * @brief:  lpm_sleep config
 * @param:  GPIO_Wakeup_LOW: gpio0-31  low  level wakeup enable
 * @param:  GPIO_Wakeup_HIGH:gpio0-31  high  level wakeup enable
 * @return: TRUE:  Chip Sleep success
 *          FALSE: Chip Sleep fail
 */
Boolean LPM_ChipSleep(uint32_t time, uint32_t GPIO_Wakeup_LOW, uint32_t GPIO_Wakeup_HIGH);
Boolean HIBERNATE_ChipSleep(uint32_t time, uint32_t GPIO_Wakeup_LOW, uint32_t GPIO_Wakeup_HIGH);

#endif
