  /*
  ***********************************************************************************
  *
  * COPYRIGHT(c) 2021, China Mobile IOT
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of China Mobile IOT nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ***********************************************************************************
  */

/* Include ------------------------------------------------------------------*/
#include "usb_core.h"
#include "usb_bsp.h"
#include "usb_main.h"
#include "usb_dcd_int.h"
#include "cm_sysctrl.h"

/* Private typedef ----------------------------------------------------------*/
/* Private define -----------------------------------------------------------*/
/* Private macro ------------------------------------------------------------*/
/* Private variables --------------------------------------------------------*/
/* Ptivate function prototypes ----------------------------------------------*/

/******************************************************************************
* Function Name  :
* Description    :
* Input          :
* Output         :
* Return         :
*******************************************************************

/ *******************************************************************************
* @brief  USB_OTG_WritePacket : Writes a packet into the Tx FIFO associated
*         with the EP
* @param  pdev : Selected device
* @param  src : source pointer
* @param  ch_ep_num : end point number
* @param  bytes : No. of bytes
* @retval USB_OTG_STS : status
*/

volatile uint8_t  *usb_rxptr;
volatile uint16_t rx_DataLength;

USB_OTG_STS USB_OTG_WritePacket(USB_OTG_CORE_HANDLE *pdev,
                                uint8_t             *src,
                                uint8_t             ch_ep_num,
                                uint16_t            len)
{
    USB_OTG_STS status = USB_OTG_OK;

    uint8_t *ptr = src;
    if (ch_ep_num == 0)
    {

        USBHWRITE((uint8_t *)&usb_txbuf0[0], len - 1);
        memcpy((void *)(((uint8_t *)usb_txbuf0) + 1), ptr, len);
        USBHWRITEW(core_usb_tx_saddr0, (int)(void *)&usb_txbuf0);
    }
    else if (ch_ep_num == 1)
    {

        USBHWRITE((uint8_t *)&usb_txbuf1[0], len - 1);
        memcpy((void *)(((uint8_t *)usb_txbuf1) + 1), ptr, len);
        USBHWRITEW(core_usb_tx_saddr1, (int)(void *)&usb_txbuf1);
    }
    else if (ch_ep_num == 2)
    {
        USBHWRITE((uint8_t *)&usb_txbuf2[0], len - 1);
        memcpy((void *)(((uint8_t *)usb_txbuf2) + 1), ptr, len);
        USBHWRITEW(core_usb_tx_saddr2, (int)(void *)&usb_txbuf2);
    }
    else
    {

    }
    USBHWRITE(core_usb_trig, 1 << ch_ep_num);

#ifdef USB_USER_MRAM
    for (int i = 0; i < 1000; i++)
    {
        if (!(USBHREAD(core_usb_txbusy) & (1 << ch_ep_num)))
            break;
    }
#endif
    return status;
}

/**
* @brief  USB_OTG_ReadPacket : Reads a packet from the Rx FIFO
* @param  pdev : Selected device
* @param  dest : Destination Pointer
* @param  bytes : No. of bytes
* @retval None
*/

extern uint32_t gTestCntWork;

void USB_OTG_ReadPacket(USB_OTG_CORE_HANDLE *pdev,
                        uint8_t *dest,
                        uint8_t ch_ep_num,
                        uint16_t len)
{
    uint16_t i = 0;
    uint16_t count8b = len ;
    uint8_t  *data_buff = (uint8_t *)dest;
    if (len <= 0)
    {
        return;
    }
    for (i = 0; i < count8b; i++, data_buff++)
    {
        *data_buff = usb_getbyte();
    }
    rx_DataLength = 0;
}

uint16_t glen;
void USBD_Get_Packet(USB_OTG_CORE_HANDLE *pdev, uint8_t *Ptr)
{
    uint16_t RxHead;
    uint8_t epnum;
    RxHead = usb_getword();
    glen = RxHead & 0xfff;
    epnum = (uint8_t)(RxHead >> 12);

    USB_OTG_ReadPacket(pdev, Ptr, epnum, glen);
}

extern  uint32_t DCD_HandleOutEP_ISR(USB_OTG_CORE_HANDLE *pdev, uint8_t ep_intr);

void USB_RX_CONTINUE(USB_OTG_CORE_HANDLE *pdev)
{
    uint16_t rx_ep;//,rx_ep1,test_len;
    //uint8_t j[10];
    volatile uint16_t cnt, i;
    rx_DataLength = 0;
    //MyPrintf("rx continue\n ");
    for (i = 0; i < 1000; i++)
    {
        if (USBHREAD(core_usb_status) & USB_STATUS_RXREADY)
        {

            USBHWRITE(core_usb_status, USB_STATUS_RXREADY);
            //MyPrintf("rx_ptr =%2x\n",usb_rxptr);
        }
    }
    rx_ep = usb_getword() ;
    //MyPrintf("rx =%02x\n",rx_ep);
    rx_DataLength = rx_ep & 0xfff;
    //     MyPrintf("len =%x\n",rx_DataLength);
    if (rx_DataLength == 0)
    {
        return;
    }
    if (rx_ep >> 12 == 0)
    {
        if (rx_DataLength != 8)
        {
            for (uint16_t i = 0; i < rx_DataLength; i++)
            {
                //j[i]= usb_getbyte();
                usb_getbyte();
                //  USBHWRITEW(core_usb_rxptr, usb_rxptr);
            }

        }
        else
        {
            //  MyPrintf("rx1 continue\n");
            USB_OTG_ReadPacket(pdev,
                               pdev->dev.out_ep[0].xfer_buff + pdev->dev.out_ep[0].xfer_count,
                               0,
                               rx_DataLength);
            USBD_DCD_INT_fops->SetupStage(pdev);
        }

    }
    else
    {
        //MyPrintf("rx continue1\n ");
        DCD_HandleOutEP_ISR(pdev, rx_ep >> 12);
        //USBHWRITEW(core_usb_rxptr, usb_rxptr);
    }
    //           }

}

/**
* @brief  USB_OTG_SelectCore
*         Initialize core registers address.
* @param  pdev : Selected device
* @param  coreID : USB OTG Core ID
* @retval USB_OTG_STS : status
*/
USB_OTG_STS USB_OTG_SelectCore(USB_OTG_CORE_HANDLE *pdev,
                               USB_OTG_CORE_ID_TypeDef coreID)
{
    uint32_t baseAddress = 0;
    USB_OTG_STS status = USB_OTG_OK;
    baseAddress = baseAddress;
#ifdef  USB_OTG_DMA_ENABLE
    pdev->cfg.dma_enable       = 1;
#else
    pdev->cfg.dma_enable       = 0;
#endif

    /* at startup the core is in FS mode */
    pdev->cfg.speed            = USB_OTG_SPEED_FULL;
    pdev->cfg.mps              = USB_OTG_FS_MAX_PACKET_SIZE ;

    /* initialize device cfg following its address */
    if (coreID == USB_OTG_FS_CORE_ID)
    {
        baseAddress                = USB_OTG_FS_BASE_ADDR;
        pdev->cfg.coreID           = USB_OTG_FS_CORE_ID;
        pdev->cfg.dev_endpoints    = 4 ;
        pdev->cfg.TotalFifoSize    = 64; /* in 8-bits */
        pdev->cfg.phy_itface       = USB_OTG_EMBEDDED_PHY;

#ifdef USB_OTG_FS_SOF_OUTPUT_ENABLED
        pdev->cfg.Sof_output       = 1;
#endif

#ifdef USB_OTG_FS_LOW_PWR_MGMT_SUPPORT
        pdev->cfg.low_power        = 1;
#endif
    }
    else if (coreID == USB_OTG_HS_CORE_ID)
    {
        baseAddress                = USB_OTG_FS_BASE_ADDR;
        pdev->cfg.coreID           = USB_OTG_HS_CORE_ID;
        pdev->cfg.host_channels    = 8 ;
        pdev->cfg.dev_endpoints    = 4 ;
        pdev->cfg.TotalFifoSize    = 512;/* in 8-bits */

#ifdef USB_OTG_ULPI_PHY_ENABLED
        pdev->cfg.phy_itface       = USB_OTG_ULPI_PHY;
#else
#ifdef USB_OTG_EMBEDDED_PHY_ENABLED
        pdev->cfg.phy_itface       = USB_OTG_EMBEDDED_PHY;
#endif
#endif

#ifdef USB_OTG_HS_INTERNAL_DMA_ENABLED
        pdev->cfg.dma_enable       = 1;
#endif

#ifdef USB_OTG_HS_SOF_OUTPUT_ENABLED
        pdev->cfg.Sof_output       = 1;
#endif

#ifdef USB_OTG_HS_LOW_PWR_MGMT_SUPPORT
        pdev->cfg.low_power        = 1;
#endif
    }

    /* Common USB Registers */
    pdev ->regs.CTRLREGS  = (USB_OTG_CTRLREGS *)core_usb_config;
    pdev ->regs. Hmode    = (USB_OTG_HMODE *)core_usb_hmode;
    pdev ->regs.xferctl   = (USB_OTG_XFERCTL *)core_usb_trig;
    pdev ->regs.STATUSEGS = (USB_OTG_STATUSREGS *)core_usb_status_stall;
    return status;
}

/**
* @brief  USB_OTG_CoreInit
*         Initializes the USB_OTG controller registers and prepares the core
*         device mode or host mode operation.
* @param  pdev : Selected device
* @retval USB_OTG_STS : status
*/
USB_OTG_STS USB_OTG_CoreInit(USB_OTG_CORE_HANDLE *pdev)
{
    USB_OTG_STS status = USB_OTG_OK;
    return status;
}

/**
* @brief  USB_OTG_SetCurrentMode : Set ID line
* @param  pdev : Selected device
* @param  mode :  (Host/device)only device
* @retval USB_OTG_STS : status
*/
USB_OTG_STS USB_OTG_SetCurrentMode(USB_OTG_CORE_HANDLE *pdev, uint8_t mode)
{
    USB_OTG_STS status = USB_OTG_OK;

    if (mode == HOST_MODE)
    {
    }
    else if (mode == DEVICE_MODE)
    {
#ifdef USE_DEVICE_MODE
        pdev->dev.out_ep[0].xfer_buff = pdev->dev.setup_packet;
        pdev->dev.out_ep[0].xfer_len = 8;
#endif
    }
    return status;
}

/**
* @brief  USB_OTG_EPActivate : Activates an EP
* @param  pdev : Selected device
* @retval USB_OTG_STS : status
*/
USB_OTG_STS USB_OTG_EPActivate(USB_OTG_CORE_HANDLE *pdev, USB_OTG_EP *ep)
{
    USB_OTG_STS status = USB_OTG_OK;
    USB_OTG_IRQ_MASK1_TypeDef intr_rxtxe;

    /* Read DEPCTLn register */
    if (ep->is_in == 1)
    {
        intr_rxtxe.d8 = 1 << ep->num;
        USB_OTG_MODIFY_REG8(&pdev->regs.CTRLREGS->IRQ_MASK, intr_rxtxe.d8, 0);
    }
    else
    {
        intr_rxtxe.d8 = 1 << ep->num;
        USB_OTG_MODIFY_REG8(&pdev->regs.CTRLREGS ->IRQ_MASK, intr_rxtxe.d8, 0);
    }
    return status;
}

/**
* @brief  USB_OTG_EPDeactivate : Deactivates an EP
* @param  pdev : Selected device
* @retval USB_OTG_STS : status
*/
USB_OTG_STS USB_OTG_EPDeactivate(USB_OTG_CORE_HANDLE *pdev, USB_OTG_EP *ep)
{
    USB_OTG_STS status = USB_OTG_OK;
    USB_OTG_IRQ_MASK1_TypeDef intr_rxtxe;

    /* Read DEPCTLn register */
    if (ep->is_in == 1)
    {
        intr_rxtxe.d8 = 1 << ep->num;
        USB_OTG_MODIFY_REG8(&pdev->regs.CTRLREGS ->IRQ_MASK, 0, intr_rxtxe.d8);
    }
    else
    {
        intr_rxtxe.d8 = 1 << ep->num;
        USB_OTG_MODIFY_REG8(&pdev->regs.CTRLREGS ->IRQ_MASK, 0, intr_rxtxe.d8);
    }
    return status;
}

/**
* @brief  USB_OTG_GetMode : Get current mode
* @param  pdev : Selected device
* @retval current mode
*/
uint8_t USB_OTG_GetMode(USB_OTG_CORE_HANDLE *pdev)
{
    return DEVICE_MODE;
}

/**
* @brief  USB_OTG_IsDeviceMode : Check if it is device mode
* @param  pdev : Selected device
* @retval num_in_ep
*/
uint8_t USB_OTG_IsDeviceMode(USB_OTG_CORE_HANDLE *pdev)
{
    return (USB_OTG_GetMode(pdev) != HOST_MODE);
}

/**
* @brief  USB_OTG_EPStartXfer : Handle the setup for data xfer for an EP and
*         starts the xfer
* @param  pdev : Selected device
* @retval USB_OTG_STS : status
*/

USB_OTG_STS USB_OTG_EPStartXfer(USB_OTG_CORE_HANDLE *pdev, USB_OTG_EP *ep)
{
    USB_OTG_STS status = USB_OTG_OK;
    uint8_t  rx_count;
    /* IN endpoint */

    if (ep->is_in == 1)
    {
        if ((pdev->cfg.dma_enable == 0) || ((USB_OTG_DEV_DMA_EP_NUM & 0x07) != ep->num))
        {
            ep->rem_data_len = ep->xfer_len - ep->xfer_count;
            if (ep->rem_data_len == ep->maxpacket)
            {
                USB_OTG_WritePacket(pdev,
                                    ep->xfer_buff + ep->xfer_count,
                                    ep->num,
                                    ep->maxpacket);
                ep->xfer_count += ep->maxpacket;
                ep->xfer_count = ep->xfer_len;
                ep->rem_data_len = 0;
                pdev->dev.zero_replay_flag = 1 << ep->num;

            }
            /* Zero Length Packet? */
            else if (ep->rem_data_len == 0)
            {
                USB_OTG_WritePacket(pdev,
                                    ep->xfer_buff + ep->xfer_count,
                                    ep->num,
                                    0);
                ep->xfer_count = ep->xfer_len;
                ep->rem_data_len = 0;
            }
            else
            {
                if (ep->rem_data_len > ep->maxpacket)
                {
                    USB_OTG_WritePacket(pdev,
                                        ep->xfer_buff + ep->xfer_count,
                                        ep->num,
                                        ep->maxpacket);
                    ep->xfer_count += ep->maxpacket;
                    if (ep->xfer_len >= ep->xfer_count)
                    {
                        ep->rem_data_len = ep->xfer_len - ep->xfer_count;
                    }
                    else
                    {
                        ep->rem_data_len = 0;
                        ep->xfer_count = ep->xfer_len;
                    }
                }
                else
                {
                    USB_OTG_WritePacket(pdev,
                                        ep->xfer_buff + ep->xfer_count,
                                        ep->num,
                                        ep->rem_data_len);
                    ep->xfer_count = ep->xfer_len;
                    ep->rem_data_len = 0;
                }
            }
        }
    }
    else
    {
        /* OUT endpoint */
        rx_count = rx_DataLength;
        USB_OTG_ReadPacket(pdev, ep->xfer_buff + ep->xfer_count, ep->num, rx_count);
        ep->xfer_count += rx_count;
        if (ep->xfer_len <= ep->xfer_count)
        {
            ep->rem_data_len = ep->xfer_count - ep->xfer_len;
        }
        else
        {
            ep->rem_data_len = 0;
            ep->xfer_len = ep->xfer_count;
        }
    }
    return status;
}

/**
* @brief  USB_OTG_EP0StartXfer : Handle the setup for a data xfer for EP0 and
*         starts the xfer
* @param  pdev : Selected device
* @retval USB_OTG_STS : status
*/
USB_OTG_STS USB_OTG_EP0StartXfer(USB_OTG_CORE_HANDLE *pdev, USB_OTG_EP *ep)
{
    USB_OTG_STS  status = USB_OTG_OK;

    /* IN endpoint */
    if (ep->is_in == 1)
    {
        ep->rem_data_len = ep->xfer_len - ep->xfer_count;
        /* Zero Length Packet? */
        if (ep->rem_data_len == 0)
        {
            //  send zero packet
            USB_OTG_EPReply_Zerolen(pdev, ep);
            ep->xfer_count = ep->xfer_len;
            ep->rem_data_len = 0;
        }
        else
        {
            if (ep->rem_data_len > ep->maxpacket)
            {
                USB_OTG_WritePacket(pdev,
                                    ep->xfer_buff + ep->xfer_count,
                                    0,
                                    ep->maxpacket);
                ep->xfer_count += ep->maxpacket;
                ep->rem_data_len = ep->xfer_len - ep->xfer_count;
            }
            else
            {
                USB_OTG_WritePacket(pdev,
                                    ep->xfer_buff + ep->xfer_count,
                                    0,
                                    ep->rem_data_len);
                ep->xfer_count = ep->xfer_len;
                ep->rem_data_len = 0;
                /****prepare receive outpacket *****/

            }
        }
        if (pdev->cfg.dma_enable == 0)
        {
            /* Enable the Tx FIFO Empty Interrupt for this EP */
            if (ep->xfer_len > 0)
            {
            }
        }
    }
    else
    {
        /* OUT endpoint */
    }
    return status;
}

/**
* @brief  USB_OTG_TRIG : Handle start xfer and set tx trig
* @param  pdev : Selected device
* @retval USB_OTG_STS : status
*/

USB_OTG_STS  USB_OTG_TRIG(USB_OTG_CORE_HANDLE *pdev, USB_OTG_EP *ep)
{
    USB_OTG_STS status = USB_OTG_OK;
    return status;
}

/**
* @brief  USB_OTG_EPSetStall : Set the EP STALL
* @param  pdev : Selected device
* @retval USB_OTG_STS : status
*/
USB_OTG_STS USB_OTG_EPSetStall(USB_OTG_CORE_HANDLE *pdev, USB_OTG_EP *ep)
{
    USB_OTG_STS status = USB_OTG_OK;
    if (ep->num == 0)
        USBHWRITE(&pdev->regs.xferctl ->STALL, 0x01);

    USB_OTG_EPReply_Zerolen(pdev, ep);
    return status;
}

/**
* @brief  USB_OTG_EPSetStall : ack zero  length packet
* @param  pdev : Selected device
* @retval USB_OTG_STS : status
*/

USB_OTG_STS USB_OTG_EPReply_Zerolen(USB_OTG_CORE_HANDLE *pdev, USB_OTG_EP *ep)
{
    USB_OTG_STS status = USB_OTG_OK;
    USBHWRITE(core_usb_trig, 0x10 << (ep ->num));
    return status;
}

/**
* @brief  USB_OTG_RemoteWakeup : active remote wakeup signalling
* @param  None
* @retval : None
*/
void USB_OTG_ActiveRemoteWakeup(USB_OTG_CORE_HANDLE *pdev)
{
    USB_OTG_CTL_TypeDef power;
    /* Note: If CLK has been stopped,it will need be restarted before
     * this write can occur.
     */
    power.d8 = USBHREAD(&pdev->regs.CTRLREGS ->USB_CTL);
    power.b.resume = 1;
    power.b.wakeup_enable = 1;
    USBHWRITE(&pdev->regs.CTRLREGS ->USB_CTL, power.d8);
    /* The software should leave then this bit set for approximately 10ms
     * (minimum of 2ms, a maximum of 15ms) before resetting it to 0.
     */
    delay_ms(2);
    power.b.resume = 0;
    power.b.wakeup_enable = 0;
    USBHWRITE(&pdev->regs.CTRLREGS ->USB_CTL, power.d8);
}

