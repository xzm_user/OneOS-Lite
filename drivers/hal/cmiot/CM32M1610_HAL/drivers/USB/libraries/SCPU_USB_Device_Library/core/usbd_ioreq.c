  /*
  ***********************************************************************************
  *
  * COPYRIGHT(c) 2021, China Mobile IOT
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of China Mobile IOT nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ***********************************************************************************
  */

/* Include ------------------------------------------------------------------*/
#include "usbd_ioreq.h"
/* Private typedef ----------------------------------------------------------*/
/* Private define -----------------------------------------------------------*/
/* Private macro ------------------------------------------------------------*/
/* Private variables --------------------------------------------------------*/
/* Ptivate function prototypes ----------------------------------------------*/

/******************************************************************************
* Function Name  :
* Description    :
* Input          :
* Output         :
* Return         :
******************************************************************************/
/** @addtogroup CM_USB_OTG_DEVICE_LIBRARY
  * @{
  */

/** @defgroup USBD_IOREQ
  * @brief control I/O requests module
  * @{
  */

/** @defgroup USBD_IOREQ_Private_TypesDefinitions
  * @{
  */

/** @defgroup USBD_IOREQ_Private_Defines
  * @{
  */

/** @defgroup USBD_IOREQ_Private_Macros
  * @{
  */

/** @defgroup USBD_IOREQ_Private_Variables
  * @{
  */

/** @defgroup USBD_IOREQ_Private_FunctionPrototypes
  * @{
  */

/** @defgroup USBD_IOREQ_Private_Functions
  * @{
  */

/**
* @brief  USBD_CtlSendData
*         send data on the ctl pipe
* @param  pdev: device instance
* @param  buff: pointer to data buffer
* @param  len: length of data to be sent
* @retval status
*/
USBD_Status  USBD_CtlSendData(USB_OTG_CORE_HANDLE  *pdev,
                              uint8_t *pbuf,
                              uint16_t len)
{
    USBD_Status ret = USBD_OK;

    pdev->dev.in_ep[0].total_data_len = len;
    pdev->dev.in_ep[0].rem_data_len   = len;
    pdev->dev.device_state = USB_OTG_EP0_DATA_IN;

    DCD_EP_Tx(pdev, 0, pbuf, len);

    return ret;
}

/**
* @brief  USBD_CtlContinueSendData
*         continue sending data on the ctl pipe
* @param  pdev: device instance
* @param  buff: pointer to data buffer
* @param  len: length of data to be sent
* @retval status
*/
USBD_Status  USBD_CtlContinueSendData(USB_OTG_CORE_HANDLE  *pdev,
                                      uint8_t *pbuf,
                                      uint16_t len)
{
    USBD_Status ret = USBD_OK;

    DCD_EP_Tx(pdev, 0, pbuf, len);

    return ret;
}

/**
* @brief  USBD_CtlPrepareRx
*         receive data on the ctl pipe
* @param  pdev: USB OTG device instance
* @param  buff: pointer to data buffer
* @param  len: length of data to be received
* @retval status
*/
USBD_Status  USBD_CtlPrepareRx(USB_OTG_CORE_HANDLE  *pdev,
                               uint8_t *pbuf,
                               uint16_t len)
{
    USBD_Status ret = USBD_OK;

    pdev->dev.out_ep[0].total_data_len = len;
    pdev->dev.out_ep[0].rem_data_len   = len;
    pdev->dev.device_state = USB_OTG_EP0_DATA_OUT;

    DCD_EP_PrepareRx(pdev,
                     0,
                     pbuf,
                     len);
    return ret;
}

/**
* @brief  USBD_CtlContinueRx
*         continue receive data on the ctl pipe
* @param  pdev: USB OTG device instance
* @param  buff: pointer to data buffer
* @param  len: length of data to be received
* @retval status
*/
USBD_Status  USBD_CtlContinueRx(USB_OTG_CORE_HANDLE  *pdev,
                                uint8_t *pbuf,
                                uint16_t len)
{
    USBD_Status ret = USBD_OK;

    DCD_EP_PrepareRx(pdev,
                     0,
                     pbuf,
                     len);
    return ret;
}
/**
* @brief  USBD_CtlSendStatus
*         send zero lzngth packet on the ctl pipe
* @param  pdev: USB OTG device instance
* @retval status
*/
USBD_Status  USBD_CtlSendStatus(USB_OTG_CORE_HANDLE  *pdev)
{
    USBD_Status ret = USBD_OK;
    pdev->dev.device_state = USB_OTG_EP0_STATUS_IN;
    DCD_EP_Tx(pdev,
              0,
              NULL,
              0);

    return ret;
}

/**
* @brief  USBD_CtlReceiveStatus
*         receive zero lzngth packet on the ctl pipe
* @param  pdev: USB OTG device instance
* @retval status
*/
USBD_Status  USBD_CtlReceiveStatus(USB_OTG_CORE_HANDLE  *pdev)
{
    USBD_Status ret = USBD_OK;
    pdev->dev.device_state = USB_OTG_EP0_STATUS_OUT;
    DCD_EP_PrepareRx(pdev,
                     0,
                     NULL,
                     0);

    return ret;
}

/**
* @brief  USBD_GetRxCount
*         returns the received data length
* @param  pdev: USB OTG device instance
*         epnum: endpoint index
* @retval Rx Data blength
*/
uint16_t  USBD_GetRxCount(USB_OTG_CORE_HANDLE  *pdev, uint8_t epnum)
{
    return pdev->dev.out_ep[epnum].xfer_count;
}

