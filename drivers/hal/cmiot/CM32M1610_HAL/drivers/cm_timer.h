  /*
  ***********************************************************************************
  *
  * COPYRIGHT(c) 2021, China Mobile IOT
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of China Mobile IOT nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ***********************************************************************************
  */

#ifndef __CM_TIMER_H_
#define __CM_TIMER_H_
#include "cm1610.h"
#define MHz 1000000

/**
  * @brief  timer number Structure definition
  */
typedef enum
{
    TIM0 = 0,
    TIM1,
    TIM2,
    TIM3,
    TIM4,
    TIM5,
    TIM6,
    TIM7
}TIM_NumTypeDef;
#define TIM_NumTypeDef(TIMx)	(TIMx==TIM0||\
                                                                TIMx==TIM1||\
                                                                TIMx==TIM2||\
                                                                TIMx==TIM3||\
                                                                TIMx==TIM4||\
                                                                TIMx==TIM5||\
                                                                TIMx==TIM6||\
                                                                TIMx==TIM7)

#define FREQUENCY_DIVISION_0		0x0
#define FREQUENCY_DIVISION_1		0x1
#define FREQUENCY_DIVISION_2		0x2
#define FREQUENCY_DIVISION_3		0x3
#define FREQUENCY_DIVISION_4		0x4
#define FREQUENCY_DIVISION_5		0x5
#define FREQUENCY_DIVISION_6		0x6
#define FREQUENCY_DIVISION_7		0x7
#define IS_FREQUENCY_DIVISION(FREQUENCY)		((FREQUENCY == FREQUENCY_DIVISION_0)||\
                                                                                                (FREQUENCY == FREQUENCY_DIVISION_1) ||\
                                                                                                (FREQUENCY == FREQUENCY_DIVISION_2)	||\
                                                                                                (FREQUENCY == FREQUENCY_DIVISION_3) ||\
                                                                                                (FREQUENCY == FREQUENCY_DIVISION_4)	||\
                                                                                                (FREQUENCY == FREQUENCY_DIVISION_5) ||\
                                                                                                (FREQUENCY == FREQUENCY_DIVISION_6)	||\
                                                                                                (FREQUENCY == FREQUENCY_DIVISION_7))

/**
  * @brief set the initial pwm level
  */
typedef enum
{
    OutputLow  = 0,
    OutputHigh = 0x10
} START_TypeDef;
#define IS_PWM_START(LEVEL)		(LEVEL == OutputLow || LEVEL == OutputHigh)

/**
  *@brief Sync mode .for difference and synchronize pwm output
  */
typedef enum
{
    Sync_default = 0,
    Sync_enable  = 0x08
} PWM_SyncTypedef;
#define IS_PWM_SYNC(SYNC)		(SYNC == Sync_default || SYNC == Sync_enable)

/**
  * @brief  PWM Init Structure definition
  */
typedef struct
{
    TIM_NumTypeDef TIMx;         //TIM0-7 must correspondence to  pin0-7 by onetoone relation
    uint16_t LowLevelPeriod;
    uint16_t HighLevelPeriod;    // PWM frequency = TIM_ModuleClk()/[ 2^(module_clk_div)(HighLevelPeriod + LowLevelPeriod + 2)]    Hz
    START_TypeDef StartLevel;
    PWM_SyncTypedef Sync;
    uint8_t module_clk_div;      // value must be 0-7
} PWM_InitTypeDef;

/**
  * @brief  timer Init Structure definition
  */
typedef struct
{
    TIM_NumTypeDef TIMx;
    uint16_t period;
    uint8_t module_clk_div;    // TIM time = [2^(module_clk_div) * (period+1)] /TIM_ModuleClk()  second
}TIM_InitTypeDef;

/**
 * @brief  DeInit TIM and PWM
 * @param   TIMx : the timer number,TIM0-TIM7
 * @retval none
 */
void TIM_DeInit(TIM_NumTypeDef TIMx);

/**
 * @brief   enable or disable timer
 * @param   TIMx : the timer number,TIM0-TIM7
 * @param   NewState :Fixed FunctionalState enumeration type
 * @retval  none
 */
void TIM_Cmd(TIM_NumTypeDef TIMx, FunctionalState NewState);

/**
 * @brief   Init TIM with timer mode
 * @param   TIM_Init_Struct : TIM initializes the structure
 * @retval  none
 */
void TIM_Init(TIM_InitTypeDef* TIM_init_struct);

/**
 * @brief  Configure timer Period
 * @param  TIMx : the timer number,TIM0-TIM7
 * @param  Period :the timer Period(Reload value)
 * @retval none
 */
void TIM_SetPeriod(TIM_NumTypeDef TIMx, uint16_t Period);

/**
 * @brief   Init tim with pwm mode
 * @param   TIM_Init_Struct : TIM initializes the structure
 * @retval  none
 */
void TIM_PwmInit(PWM_InitTypeDef* Pwm_init_struct);

/**
 * @brief   Get the master frequency of TIM
 * @param   none
 * @retval  the master frequency of TIM (Hz)
 */
uint32_t  TIM_getModuleClk(void);

#endif
