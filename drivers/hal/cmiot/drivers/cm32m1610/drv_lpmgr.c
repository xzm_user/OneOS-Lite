/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_lpmgr.c
 *
 * @brief       This file implements low power manager for cm32.
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */
#include <os_types.h>
#include <lpmgr.h>
#include <cm_lpm.h>
#include <cm_ipc.h>

void cm32_sleep_lpm(void)
{
    switch (HREAD(IPC_MCU_STATE))    // Read BT core state
    {
    case IPC_MCU_STATE_RUNNING:
        // always first init bt
        // IPC_HandleRxPacket(); //Accept packages from bt core
        Lpm_unLockLpm(M0_LPM_FLAG);
        break;
    case IPC_MCU_STATE_LMP:    // enter lower power mode
        if (IPC_IsTxBuffEmpty() && Lpm_CheckLpmFlag())
        {
            LPM_GpioUnusedPd();               // Drop useless IO
            LPM_ChipSleep(1600 * 5, 0, 0);    // Set wake condition  5s
        }
        else
        {
            HWRITE(IPC_MCU_STATE, IPC_MCU_STATE_RUNNING);
        }
        break;
    case IPC_MCU_STATE_HIBERNATE:    // enter super low power mode
        OS_ENTER_CRITICAL();
        HWRITE(IPC_MCU_STATE, IPC_MCU_STATE_STOP);
        break;
    case IPC_MCU_STATE_STOP:    // stopped
        break;
    }
}

void cm32_sleep_hibernate(void)
{
    LPM_GpioUnusedPd();    // hibernate deep sleep 5s
    HIBERNATE_ChipSleep(0, BIT_10, 0);
}

static os_err_t lpm_sleep(lpmgr_sleep_mode_e mode)
{
    switch (mode)
    {
    case SYS_SLEEP_MODE_NONE:
    case SYS_SLEEP_MODE_IDLE:
        break;
    case SYS_SLEEP_MODE_LIGHT:
        cm32_sleep_lpm();
        break;
    case SYS_SLEEP_MODE_DEEP:
        cm32_sleep_hibernate();
        break;

    default:
        OS_ASSERT(0);
    }

    return OS_EOK;
}

int drv_lpmgr_hw_init(void)
{
    os_lpmgr_init(lpm_sleep);
    return 0;
}

OS_PREV_INIT(drv_lpmgr_hw_init, OS_INIT_SUBLEVEL_MIDDLE);
