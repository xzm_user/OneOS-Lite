/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_gpio.h
 *
 * @brief       This file provides struct/macro declaration and functions declaration for cm32 gpio driver.
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#ifndef __DRV_GPIO_H__
#define __DRV_GPIO_H__

#include <cm_gpio.h>

#define GPIO_PORTA
#define GPIO_PORTB
#define GPIO_PORTC
#define GPIO_PORTD

#define GPIO_PIN_PER_PORT (8)
#define GPIO_PORT_MAX     (4)
#define GPIO_PIN_MAX      (GPIO_PORT_MAX * GPIO_PIN_PER_PORT)

#define PORT_INDEX(pin)     (pin / GPIO_PIN_PER_PORT)
#define PIN_INDEX(pin)      (pin % GPIO_PIN_PER_PORT)
#define GET_PIN(PORTx, PIN) (((GPIO##PORTx - GPIOA) * GPIO_PIN_PER_PORT) + PIN)

#define PIN_INDEX_INI(gpio, pin)                                                                                       \
    {                                                                                                                  \
        GPIO##gpio, pin, 0                                                                                             \
    }

int os_hw_pin_init(void);

#endif /* __DRV_GPIO_H__ */
