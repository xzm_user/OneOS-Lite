/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_usart.c
 *
 * @brief       This file implements usart driver for cm32
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include <drv_cfg.h>
#include <device.h>
#include <string.h>

#include "drv_usart.h"

static os_list_node_t cm32_uart_list = OS_LIST_INIT(cm32_uart_list);

/* uart driver */
typedef struct cm32_uart
{
    struct os_serial_device serial_dev;
    struct cm32_usart_info *info;
    soft_dma_t              sdma;

    uint8_t  *rx_buff;
    os_size_t rx_index;
    os_size_t rx_size;

    const uint8_t *tx_buff;
    os_size_t      tx_index;
    os_size_t      tx_size;

    os_list_node_t list;
} cm32_uart_t;

static void uart_isr(struct os_serial_device *serial)
{
    OS_ASSERT(serial != OS_NULL);
    cm32_uart_t *uart = os_container_of(serial, cm32_uart_t, serial_dev);

    OS_ASSERT(uart != OS_NULL);

    if (UART_IsTxIRQ(uart->info->uart))
    {
        USART_ClearTxITPendingBit(uart->info->uart);
        if (uart->tx_size)
        {
            if (uart->tx_index < uart->tx_size)
            {
                USART_SendData(uart->info->uart, uart->tx_buff[uart->tx_index++]);
            }
            else
            {
                USART_TxITConfig(uart->info->uart, DISABLE);
                os_hw_serial_isr_txdone(serial);
            }
        }
    }

    if (USART_IsRXFIFONotEmpty(uart->info->uart))
    {
        uart->rx_buff[uart->rx_index++] = USART_ReceiveData(uart->info->uart);

        if (uart->rx_index == (uart->rx_size / 2))
        {
            soft_dma_half_irq(&uart->sdma);
        }

        if (uart->rx_index == uart->rx_size)
        {
            uart->rx_index = 0;
            soft_dma_full_irq(&uart->sdma);
        }
    }
}

void uart_irq(USART_TypeDef huart)
{
    struct cm32_uart *uart;

    os_list_for_each_entry(uart, &cm32_uart_list, struct cm32_uart, list)
    {
        if (uart->info->uart == huart)
        {
            uart_isr(&uart->serial_dev);
            break;
        }
    }
}

void UART_IRQHandler(void)
{
    uart_irq(UARTA);
}

void UARTB_IRQHandler(void)
{
    uart_irq(UARTB);
}

static os_uint32_t cm32_uart_sdma_int_get_index(soft_dma_t *dma)
{
    cm32_uart_t *uart = os_container_of(dma, cm32_uart_t, sdma);

    return uart->rx_index;
}

static os_err_t cm32_uart_sdma_int_start(soft_dma_t *dma, void *buff, os_uint32_t size)
{
    cm32_uart_t *uart = os_container_of(dma, cm32_uart_t, sdma);

    uart->rx_buff  = buff;
    uart->rx_index = 0;
    uart->rx_size  = size;

    USART_SetRxITNum(uart->info->uart, 1);
    USART_SetRxTimeout(uart->info->uart, 0xff);

    return OS_EOK;
}

static os_uint32_t cm32_uart_sdma_int_stop(soft_dma_t *dma)
{
    cm32_uart_t *uart = os_container_of(dma, cm32_uart_t, sdma);

    USART_SetRxITNum(uart->info->uart, 0);
    USART_SetRxTimeout(uart->info->uart, 0x0);

    return uart->rx_index;
}
#if 0 
static os_uint32_t cm32_uart_sdma_dma_get_index(soft_dma_t *dma)
{
    cm32_uart_t *uart = os_container_of(dma, cm32_uart_t, sdma);

    //to do development

    return 0;
}

static os_err_t cm32_uart_sdma_dma_init(soft_dma_t *dma)
{
    //to do development

    return OS_EOK;
}

static os_err_t cm32_uart_sdma_dma_start(soft_dma_t *dma, void *buff, os_uint32_t size)
{
    cm32_uart_t *uart = os_container_of(dma, cm32_uart_t, sdma);

    //to do development

    return OS_EOK;
}

static os_uint32_t cm32_uart_sdma_dma_stop(soft_dma_t *dma)
{
    cm32_uart_t *uart = os_container_of(dma, cm32_uart_t, sdma);

    //to do development

    return cm32_uart_sdma_dma_get_index(dma);
}
#endif
/* sdma callback */
static void cm32_uart_sdma_callback(soft_dma_t *dma)
{
    cm32_uart_t *uart = os_container_of(dma, cm32_uart_t, sdma);

    os_hw_serial_isr_rxdone((struct os_serial_device *)uart);
}

static void cm32_uart_sdma_init(struct cm32_uart *uart, dma_ring_t *ring)
{
    soft_dma_t *dma = &uart->sdma;

    soft_dma_stop(dma);

    memset(&dma->hard_info, 0, sizeof(dma->hard_info));

    dma->hard_info.mode         = HARD_DMA_MODE_NORMAL;
    dma->hard_info.max_size     = 64 * 1024;
    dma->hard_info.data_timeout = uart_calc_byte_timeout_us(uart->serial_dev.config.baud_rate);

    /**/
    if (uart->info->dma_support == 0)
    {
        dma->hard_info.flag = HARD_DMA_FLAG_HALF_IRQ | HARD_DMA_FLAG_FULL_IRQ;
        dma->hard_info.mode = HARD_DMA_MODE_NORMAL;

        dma->ops.get_index = cm32_uart_sdma_int_get_index;
        dma->ops.dma_init  = OS_NULL;
        dma->ops.dma_start = cm32_uart_sdma_int_start;
        dma->ops.dma_stop  = cm32_uart_sdma_int_stop;
    }
    else
    {
#if 0
        if (/*hard dma support circular mode*/)
            dma->hard_info.mode     = HARD_DMA_MODE_CIRCULAR;
        else
            dma->hard_info.mode     = HARD_DMA_MODE_NORMAL;

        dma->hard_info.flag        |= HARD_DMA_FLAG_HALF_IRQ | HARD_DMA_FLAG_FULL_IRQ;
        dma->ops.get_index          = cm32_uart_sdma_dma_get_index;
        dma->ops.dma_init           = cm32_uart_sdma_dma_init;
        dma->ops.dma_start          = cm32_uart_sdma_dma_start;
        dma->ops.dma_stop           = cm32_uart_sdma_dma_stop;
#endif
    }

    dma->cbs.dma_half_callback    = cm32_uart_sdma_callback;
    dma->cbs.dma_full_callback    = cm32_uart_sdma_callback;
    dma->cbs.dma_timeout_callback = cm32_uart_sdma_callback;

    soft_dma_init(dma);
    soft_dma_start(dma, ring);
    soft_dma_irq_enable(&uart->sdma, OS_TRUE);
}

static os_err_t cm32_uart_init(struct os_serial_device *serial, struct serial_configure *cfg)
{
    USART_InitTypeDef USART_InitStruct;

    OS_ASSERT(serial != OS_NULL);
    cm32_uart_t *uart = os_container_of(serial, cm32_uart_t, serial_dev);

    OS_ASSERT(uart != OS_NULL);

    GPIO_Config(uart->info->gpio_port, uart->info->gpio_pin_tx, uart->info->gpio_pin_mode_tx);
    GPIO_Config(uart->info->gpio_port, uart->info->gpio_pin_rx, uart->info->gpio_pin_mode_rx);

    USART_InitStruct.USART_BaudRate            = cfg->baud_rate;
    USART_InitStruct.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
    USART_InitStruct.USART_WordLength          = USART_WordLength_8b;
    USART_InitStruct.USART_Mode                = USART_Mode_duplex;

    switch (cfg->stop_bits)
    {
    case STOP_BITS_1:
        USART_InitStruct.USART_StopBits = USART_StopBits_1;
        break;
    case STOP_BITS_2:
        USART_InitStruct.USART_StopBits = USART_StopBits_2;
        break;
    default:
        return OS_EINVAL;
    }

    switch (cfg->parity)
    {
    case PARITY_ODD:
        USART_InitStruct.USART_Parity = USART_Parity_Odd;
        break;
    case PARITY_EVEN:
    case PARITY_NONE:
        USART_InitStruct.USART_Parity = USART_Parity_Even;
        break;
    default:
        return OS_EINVAL;
    }

    USART_Init(uart->info->uart, &USART_InitStruct);

    NVIC_EnableIRQ(uart->info->irqno);
    NVIC_SetPriority(uart->info->irqno, 0);

    cm32_uart_sdma_init(uart, &serial->rx_fifo->ring);

    return OS_EOK;
}

static os_err_t cm32_uart_deinit(struct os_serial_device *serial)
{
    OS_ASSERT(serial != OS_NULL);

    cm32_uart_t *uart = os_container_of(serial, cm32_uart_t, serial_dev);

    NVIC_DisableIRQ(uart->info->irqno);

    soft_dma_stop(&uart->sdma);

    USART_DeInit(uart->info->uart);

    return OS_EOK;
}

static int cm32_uart_start_send(struct os_serial_device *serial, const os_uint8_t *buff, os_size_t size)
{
    OS_ASSERT(serial != OS_NULL);

    cm32_uart_t *uart = os_container_of(serial, cm32_uart_t, serial_dev);

    uart->tx_buff  = buff;
    uart->tx_size  = size;
    uart->tx_index = 0;

    USART_TxITConfig(uart->info->uart, ENABLE);

    USART_SendData(uart->info->uart, uart->tx_buff[uart->tx_index++]);

    return size;
}

static int cm32_uart_poll_send(struct os_serial_device *serial, const os_uint8_t *buff, os_size_t size)
{
    int       i;
    os_base_t level;

    OS_ASSERT(serial != OS_NULL);
    cm32_uart_t *uart = os_container_of(serial, cm32_uart_t, serial_dev);

    OS_ASSERT(uart != OS_NULL);

    for (i = 0; i < size; i++)
    {
        level = os_irq_lock();

        USART_SendData(uart->info->uart, buff[i]);

        os_irq_unlock(level);
    }

    return size;
}

static const struct os_uart_ops cm32_uart_ops = {
    .init       = cm32_uart_init,
    .deinit     = cm32_uart_deinit,
    .start_send = cm32_uart_start_send,
    .poll_send  = cm32_uart_poll_send,
};

static int cm32_usart_probe(const os_driver_info_t *drv, const os_device_info_t *dev)
{
    os_base_t               level;
    struct cm32_uart       *usart;
    struct serial_configure config = OS_SERIAL_CONFIG_DEFAULT;

    usart = os_calloc(1, sizeof(struct cm32_uart));
    OS_ASSERT(usart);

    usart->info                     = (struct cm32_usart_info *)dev->info;
    struct os_serial_device *serial = &usart->serial_dev;

    level = os_irq_lock();
    os_list_add_tail(&cm32_uart_list, &usart->list);
    os_irq_unlock(level);

    serial->ops    = &cm32_uart_ops;
    serial->config = config;

    /* register uart device */
    os_hw_serial_register(serial, dev->name, usart);

    return 0;
}

OS_DRIVER_INFO cm32_usart_driver = {
    .name  = "Usart_Type",
    .probe = cm32_usart_probe,
};

OS_DRIVER_DEFINE(cm32_usart_driver, PREV, OS_INIT_SUBLEVEL_HIGH);
