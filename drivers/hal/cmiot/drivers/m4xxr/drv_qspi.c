/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_qspi.c
 *
 * @brief       This file implements QSPI driver for cm32.
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include <os_memory.h>
#include <string.h>
#include "board.h"
#include <sfbus.h>
#include <bus/bus.h>
#include "drv_qspi.h"

struct cm32_qspi
{
    struct os_sfbus sfbus;

    struct cm32_qspi_info *info;

    os_list_node_t list;
};

static os_list_node_t cm32_qspi_list = OS_LIST_INIT(cm32_qspi_list);

os_uint8_t line_mode_get(os_uint8_t data_lines)
{
    switch (data_lines)
    {
    case 0:
        return 0;
    case 1:
        return 0;
    case 2:
        return 1;
    case 4:
        return 2;
    }

    return 0;
}

static int cm32_qspi_configure(struct os_sfbus *sfbus, struct os_spi_configuration *configuration)
{
    struct cm32_qspi *qspi = os_container_of(sfbus, struct cm32_qspi, sfbus);

    RCC_EnableAHBPeriphClk(RCC_AHB_PERIPH_QSPI, ENABLE);

    if (qspi->info->gpio_remap != 0)
    {
        GPIO_ConfigPinRemap(qspi->info->gpio_remap, ENABLE);
    }

    return OS_EOK;
}

static void cm32_qspi_xfer_command(struct os_sfbus *sfbus, struct os_xspi_message *xmessage)
{
    uint32_t    recv_size;
    os_uint8_t *data;

    OS_ASSERT(xmessage != OS_NULL);
    struct cm32_qspi *qspi = os_container_of(sfbus, struct cm32_qspi, sfbus);

    QSPI_InitType    QSPI_InitStructure;
    QSPI_CommandType QSPI_CmdStructure;

    QSPI_DeInit();

    QSPI_InitStruct(&QSPI_InitStructure);

    QSPI_GPIOConfig(line_mode_get(xmessage_data_lines(xmessage)),
                    qspi->info->nss_port,
                    QSPI_IO_OUT_HIGH,
                    QSPI_IO_OUT_HIGH);

    if (xmessage_data_lines(xmessage) == 0 || xmessage_data_lines(xmessage) == 1)
    {
        QSPI_InitStructure.SSTE = DISABLE;
    }
    else if (xmessage_data_lines(xmessage) == 2 || xmessage_data_lines(xmessage) == 4)
    {
        if (xmessage_address_size(xmessage) == 1)
        {
            QSPI_InitStructure.Enhance.AddrLen = QSPI_ADDR_8_BITS;
        }
        else if (xmessage_address_size(xmessage) == 2)
        {
            QSPI_InitStructure.Enhance.AddrLen = QSPI_ADDR_16_BITS;
        }
        else if (xmessage_address_size(xmessage) == 3)
        {
            QSPI_InitStructure.Enhance.AddrLen = QSPI_ADDR_24_BITS;
        }
        else if (xmessage_address_size(xmessage) == 4)
        {
            QSPI_InitStructure.Enhance.AddrLen = QSPI_ADDR_32_BITS;
        }

        if (xmessage_data_dir(xmessage) == OS_XSPI_DATA_DIR_DEVICE_TO_HOST)
        {
            recv_size = xmessage_data_size(xmessage);

            QSPI_InitStructure.Enhance.WaitCycles = QSPI_WAIT_8_CYCLES;
            QSPI_InitStructure.RxDataNr           = (recv_size > 0) ? recv_size - 1 : recv_size;

            __QSPI_SET_RXFIFO_ITHR(QSPI_RXFIFO_ITHR_32);
        }
    }
#if 0
    else if (xmessage_data_lines(xmessage) == QSPI_LINE_2_LINES_XIP ||
             xmessage_data_lines(xmessage) == QSPI_LINE_4_LINES_XIP)
    {
        QSPI_InitStructure.Enhance.AddrLen      = QSPI_ADDR_24_BITS;
        QSPI_InitStructure.Enhance.WaitCycles   = QSPI_WAIT_8_CYCLES;
        QSPI_InitStructure.Enhance.XIP_CT_EN    = ENABLE;
        QSPI_InitStructure.Enhance.XIP_INST_EN  = ENABLE;
        QSPI_InitStructure.Enhance.XIP_DFS_HC   = DISABLE;
        QSPI_InitStructure.Enhance.TransferType = QSPI_TRANS_BOTH_STANDARD;
        QSPI_InitStructure.Enhance.ITOC         = 0x6B;
        QSPI_InitStructure.Enhance.WTOC         = 0x6B;
        QSPI_InitStructure.Enhance.MD_BIT_EN    = DISABLE;
        QSPI_InitStructure.Enhance.XIP_MBL      = QSPI_XIP_MBL_8_BITS;
    }
#endif

    QSPI_InitStructure.LineMode = line_mode_get(xmessage_data_lines(xmessage));
    QSPI_InitStructure.ClkDiv   = 80;

    if (xmessage_data_lines(xmessage) == 0 || xmessage_data_lines(xmessage) == 1)
    {
        QSPI_InitStructure.TransferMode = QSPI_TRANSFER_TX_AND_RX;
    }
    else
    {
        if (xmessage_data_dir(xmessage) == OS_XSPI_DATA_DIR_HOST_TO_DEVICE)
        {
            QSPI_InitStructure.TransferMode = QSPI_TRANSFER_TX_ONLY;
        }
        else if (xmessage_data_dir(xmessage) == OS_XSPI_DATA_DIR_DEVICE_TO_HOST)
        {
            QSPI_InitStructure.TransferMode = QSPI_TRANSFER_RX_ONLY;
        }
    }

    QSPI_Init(&QSPI_InitStructure);

#if 0
    if (xmessage_data_lines(xmessage) == QSPI_LINE_2_LINES_XIP ||
        xmessage_data_lines(xmessage) == QSPI_LINE_4_LINES_XIP)
    {
        QSPI_XIP_Enable(ENABLE);
    }
    else
    {
        QSPI_XIP_Enable(DISABLE);
    }
#endif

    QSPI_Enable(ENABLE);

#if 0
    if (xmessage_data_lines(xmessage) == QSPI_LINE_2_LINES_XIP ||
        xmessage_data_lines(xmessage) == QSPI_LINE_4_LINES_XIP)
    {
        __QSPI_XIP_ENABLE();
    }
    else
    {
        __QSPI_XIP_DISABLE();
    }
#endif

    if (xmessage_data_lines(xmessage) == 0 || xmessage_data_lines(xmessage) == 1)
    {
        if (xmessage_instruction(xmessage) == 0x9f || xmessage_instruction(xmessage) == 0x20 ||
            xmessage_instruction(xmessage) == 0x02)
        {
            data = os_calloc(1, xmessage_data_size(xmessage) + 4);
            if (data == OS_NULL)
            {
                os_kprintf("out of memory!\r\n");
                return;
            }

            data[0] = (uint8_t)(xmessage_instruction(xmessage));
            data[1] = (uint8_t)((xmessage_address(xmessage) & 0x00FF0000) >> 16);
            data[2] = (uint8_t)((xmessage_address(xmessage) & 0x0000FF00) >> 8);
            data[3] = (uint8_t)(xmessage_address(xmessage) & 0x000000FF);

            memcpy(&data[4], xmessage_data(xmessage), xmessage_data_size(xmessage));

            QSPI_ClrRxFIFO();

            QSPI_Transmit(data, xmessage_data_size(xmessage) + 4);

            os_free(data);
        }
        else if (xmessage_instruction(xmessage) == 0x05)
        {
            uint8_t reg_cmd[2] = {xmessage_instruction(xmessage), 0};
            QSPI_ClrRxFIFO();
            QSPI_Transmit(reg_cmd, 2);
        }
        else
        {
            QSPI_ClrRxFIFO();
            QSPI_Transmit(&xmessage_instruction(xmessage), 1);
        }
    }
    else
    {
        QSPI_CmdStructure.Instruction = (uint32_t)xmessage_instruction(xmessage);
        QSPI_CmdStructure.AddressLow  = xmessage_address(xmessage);

        QSPI_Command(&QSPI_CmdStructure);
    }
}

static int cm32_qspi_transfer(struct os_sfbus *sfbus, struct os_xspi_message *xmessage)
{
    uint32_t id = 0;

    cm32_qspi_xfer_command(sfbus, xmessage);

    if (xmessage_data_lines(xmessage) == 0)
    {
        return 0;
    }

    if (xmessage_data_dir(xmessage) == OS_XSPI_DATA_DIR_HOST_TO_DEVICE)
    {
        QSPI_Transmit((uint8_t *)xmessage_data(xmessage), xmessage_data_size(xmessage));
    }
    else if (xmessage_data_dir(xmessage) == OS_XSPI_DATA_DIR_DEVICE_TO_HOST)
    {
        if (xmessage_instruction(xmessage) == 0x9f)
        {
            QSPI_Receive((uint8_t *)&id, 4);
            id = id >> 8;

            xmessage->data[0] = id & 0xFF;
            xmessage->data[1] = (id >> 8) & 0xFF;
            xmessage->data[2] = (id >> 16) & 0xFF;
        }
        else
        {
            QSPI_Receive((uint8_t *)xmessage_data(xmessage), xmessage_data_size(xmessage));
        }
    }

    return 0;
}

static const struct os_sfbus_ops cm32_qspi_ops = {
    .configure = cm32_qspi_configure,
    .transfer  = cm32_qspi_transfer,
};

static int cm32_qspi_bus_probe(const os_driver_info_t *drv, const os_device_info_t *dev)
{
    struct cm32_qspi *qspi = os_calloc(1, sizeof(struct cm32_qspi));

    OS_ASSERT(qspi != OS_NULL);

    qspi->info = (struct cm32_qspi_info *)dev->info;

    qspi->sfbus.support_1_line = OS_TRUE;
    qspi->sfbus.support_2_line = OS_TRUE;
    qspi->sfbus.support_4_line = OS_TRUE;

    return os_sfbus_xspi_register(&qspi->sfbus, dev->name, &cm32_qspi_ops);
}

OS_DRIVER_INFO cm32_qspi_driver = {
    .name  = "QSPI_HandleTypeDef",
    .probe = cm32_qspi_bus_probe,
};

OS_DRIVER_DEFINE(cm32_qspi_driver, PREV, OS_INIT_SUBLEVEL_LOW);
