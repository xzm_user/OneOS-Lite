/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_flash_common.c
 *
 * @brief       This file provides flash read/write/erase functions for apm32.
 *
 * @revision
 * Date         Author          Notes
 * 2022-01-11   OneOS Team      First Version
 ***********************************************************************************************************************
 */
#include <os_memory.h>
#include "dlog.h"
#include "board.h"

#define DRV_EXT_LVL DBG_EXT_INFO
#define DRV_EXT_TAG "drv.flash"
#include <drv_log.h>

#include "drv_flash.h"
#include "apm32f10x_fmc.h"

static uint32_t GetPage(uint32_t addr)
{
    uint32_t page = 0;
    FMC_ClearStatusFlag((FMC_FLAG_T)(FMC_FLAG_OC | FMC_FLAG_PE | FMC_FLAG_WPE));

    page = OS_ALIGN_DOWN(addr, APM32_FLASH_PAGE_SIZE);
    return page;
}

int apm32_flash_read(os_uint32_t addr, os_uint8_t *buf, size_t size)
{
    size_t i;

    if ((addr + size) > APM32_FLASH_END_ADDRESS)
    {
        LOG_E(DRV_EXT_TAG, "read outrange flash size! addr is (0x%p)", (void *)(addr + size));
        return OS_EINVAL;
    }

    for (i = 0; i < size; i++, buf++, addr++)
    {
        *buf = *(os_uint8_t *)addr;
    }

    return size;
}

int apm32_flash_write(os_uint32_t addr, const os_uint8_t *buf, size_t size)
{
    size_t      i, j;
    os_err_t    result     = 0;
    os_uint32_t write_data = 0, temp_data = 0;

    if ((addr + size) > APM32_FLASH_END_ADDRESS)
    {
        LOG_E(DRV_EXT_TAG, "ERROR: write outrange flash size! addr is (0x%p)", (void *)(addr + size));
        return OS_EINVAL;
    }

    if (addr % 4 != 0)
    {
        LOG_E(DRV_EXT_TAG, "write addr must be 4-byte alignment");
        return OS_EINVAL;
    }

    FMC_Unlock();

    FMC_ClearStatusFlag((FMC_FLAG_T)(FMC_FLAG_OC | FMC_FLAG_PE | FMC_FLAG_WPE));

    if (size < 1)
    {
        return OS_ERROR;
    }

    for (i = 0; i < size;)
    {
        if ((size - i) < 4)
        {
            for (j = 0; (size - i) > 0; i++, j++)
            {
                temp_data  = *buf;
                write_data = (write_data) | (temp_data << 8 * j);
                buf++;
            }
        }
        else
        {
            for (j = 0; j < 4; j++, i++)
            {
                temp_data  = *buf;
                write_data = (write_data) | (temp_data << 8 * j);
                buf++;
            }
        }

        /* write data */
        if (FMC_ProgramWord(addr, write_data) == FMC_STATUS_COMPLETE)
        {
            /* Check the written value */
            if (*(uint32_t *)addr != write_data)
            {
                LOG_E(DRV_EXT_TAG, "ERROR: write data != read data");
                result = OS_ERROR;
                goto __exit;
            }
        }
        else
        {
            result = OS_ERROR;
            goto __exit;
        }

        temp_data  = 0;
        write_data = 0;

        addr += 4;
    }

__exit:
    FMC_Lock();
    if (result != 0)
    {
        return result;
    }

    return size;
}

int apm32_flash_erase(os_uint32_t addr, size_t size)
{
    os_err_t    result = OS_EOK;
    os_uint32_t page_addr;
    os_uint32_t page_num;

    if ((addr + size) > APM32_FLASH_END_ADDRESS)
    {
        LOG_E(DRV_EXT_TAG, "ERROR: erase outrange flash size! addr is (0x%p)", (void *)(addr + size));
        return OS_EINVAL;
    }

    FMC_Unlock();

    page_addr = GetPage(addr);
    page_num  = (size + APM32_FLASH_PAGE_SIZE - 1) / APM32_FLASH_PAGE_SIZE;

    for (os_uint32_t i = 0; i < page_num; i++)
    {
        if (FMC_ErasePage(page_addr) != FMC_STATUS_COMPLETE)
        {
            result = OS_ERROR;
            goto __exit;
        }
        page_addr += APM32_FLASH_PAGE_SIZE;
    }

__exit:
    FMC_Lock();

    if (result != OS_EOK)
    {
        return result;
    }

    LOG_D(DRV_EXT_TAG, "erase done: addr (0x%p), size %d", (void *)addr, size);
    return size;
}

#include "fal_drv_flash.c"
