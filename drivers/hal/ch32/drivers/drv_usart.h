/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_usart.h
 *
 * @brief        This file provides functions declaration for usart driver.
 *
 * @revision
 * Date         Author          Notes
 * 2022-01-10   OneOS Team      First Version
 ***********************************************************************************************************************
 */
#ifndef __DRV_USART_H__
#define __DRV_USART_H__
#include <os_types.h>

#include "ch32v30x.h"

typedef struct ch32_usart_info
{
    GPIO_TypeDef    *tx_pin_port;
    GPIO_InitTypeDef tx_pin_info;

    GPIO_TypeDef    *rx_pin_port;
    GPIO_InitTypeDef rx_pin_info;

    void (*gpio_PeriphClock)(uint32_t Periph, FunctionalState NewState);
    uint32_t gpio_Periph;
    void (*usart_PeriphClock)(uint32_t Periph, FunctionalState NewState);
    uint32_t usart_Periph;

    USART_TypeDef *husart;
    uint16_t       usart_it;

    USART_InitTypeDef usart_def_cfg;
    NVIC_InitTypeDef  usart_nvic_cfg;
} ch32_usart_info_t;

typedef struct ch32_usart
{
    struct os_serial_device serial;
    struct ch32_usart_info *info;

    soft_dma_t  sdma;
    os_uint32_t sdma_hard_size;

    os_uint8_t *rx_buff;
    os_uint32_t rx_index;
    os_uint32_t rx_size;

    const os_uint8_t *tx_buff;
    os_uint32_t       tx_count;
    os_uint32_t       tx_size;

    os_list_node_t list;
} ch32_usart_t;

#endif /* __DRV_USART_H__ */
