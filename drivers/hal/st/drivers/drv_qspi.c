/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_qspi.c
 *
 * @brief       This file implements QSPI driver for stm32.
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include <os_memory.h>
#include <string.h>
#include "board.h"
#include <sfbus.h>
#include <bus/bus.h>
#include "drv_qspi.h"

struct stm32_qspi
{
    struct os_sfbus sfbus;

    QSPI_HandleTypeDef *QSPI_Handler;

    os_list_node_t list;
};

static os_list_node_t stm32_qspi_list = OS_LIST_INIT(stm32_qspi_list);

static int stm32_qspi_configure(struct os_sfbus *sfbus, struct os_spi_configuration *configuration)
{
    int i = 1;

    struct stm32_qspi *qspi = os_container_of(sfbus, struct stm32_qspi, sfbus);

    QSPI_HandleTypeDef *QSPI_Handler = qspi->QSPI_Handler;

    while (configuration->max_hz < HAL_RCC_GetHCLKFreq() / (i + 1))
    {
        if (++i == 255)
        {
            os_kprintf("QSPI init failed, QSPI frequency(%d) is too low.", configuration->max_hz);
            return OS_ERROR;
        }
    }

    /* 80/(1+i) */
    QSPI_Handler->Init.ClockPrescaler = i;

    if (!(configuration->mode & OS_SPI_CPOL))
    {
        /* QSPI MODE0 */
        QSPI_Handler->Init.ClockMode = QSPI_CLOCK_MODE_0;
    }
    else
    {
        /* QSPI MODE3 */
        QSPI_Handler->Init.ClockMode = QSPI_CLOCK_MODE_3;
    }

    QSPI_Handler->Init.FlashSize = POSITION_VAL(1024 * 1024 * 1024) - 1;

    HAL_QSPI_Init(QSPI_Handler);

    return OS_EOK;
}

static void stm32_qspi_xfer_command(QSPI_HandleTypeDef *QSPI_Handler, struct os_xspi_message *xmessage)
{
    OS_ASSERT(xmessage != OS_NULL);
    OS_ASSERT(QSPI_Handler != OS_NULL);

    QSPI_CommandTypeDef Cmdhandler;

    /* set QSPI cmd struct */
    Cmdhandler.Instruction = xmessage_instruction(xmessage);
    Cmdhandler.Address     = xmessage_address(xmessage);

    /* 1 instruction */
    if (xmessage_instruction_lines(xmessage) == 0)
    {
        Cmdhandler.InstructionMode = QSPI_INSTRUCTION_NONE;
    }
    else if (xmessage_instruction_lines(xmessage) == 1)
    {
        Cmdhandler.InstructionMode = QSPI_INSTRUCTION_1_LINE;
    }
    else if (xmessage_instruction_lines(xmessage) == 2)
    {
        Cmdhandler.InstructionMode = QSPI_INSTRUCTION_2_LINES;
    }
    else if (xmessage_instruction_lines(xmessage) == 4)
    {
        Cmdhandler.InstructionMode = QSPI_INSTRUCTION_4_LINES;
    }

    /* 2.1 address */
    if (xmessage_address_lines(xmessage) == 0)
    {
        Cmdhandler.AddressMode = QSPI_ADDRESS_NONE;
    }
    else if (xmessage_address_lines(xmessage) == 1)
    {
        Cmdhandler.AddressMode = QSPI_ADDRESS_1_LINE;
    }
    else if (xmessage_address_lines(xmessage) == 2)
    {
        Cmdhandler.AddressMode = QSPI_ADDRESS_2_LINES;
    }
    else if (xmessage_address_lines(xmessage) == 4)
    {
        Cmdhandler.AddressMode = QSPI_ADDRESS_4_LINES;
    }

    /* 2.2 address size */
    if (xmessage_address_size(xmessage) == 1)
    {
        Cmdhandler.AddressSize = QSPI_ADDRESS_8_BITS;
    }
    else if (xmessage_address_size(xmessage) == 2)
    {
        Cmdhandler.AddressSize = QSPI_ADDRESS_16_BITS;
    }
    else if (xmessage_address_size(xmessage) == 3)
    {
        Cmdhandler.AddressSize = QSPI_ADDRESS_24_BITS;
    }
    else if (xmessage_address_size(xmessage) == 4)
    {
        Cmdhandler.AddressSize = QSPI_ADDRESS_32_BITS;
    }

    /* 3 alternate */
    if (xmessage_alternate_lines(xmessage) == 0)
    {
        Cmdhandler.AlternateByteMode = QSPI_ALTERNATE_BYTES_NONE;
    }
    else if (xmessage_alternate_lines(xmessage) == 1)
    {
        Cmdhandler.AlternateByteMode = QSPI_ALTERNATE_BYTES_1_LINE;
    }
    else if (xmessage_alternate_lines(xmessage) == 2)
    {
        Cmdhandler.AlternateByteMode = QSPI_ALTERNATE_BYTES_2_LINES;
    }
    else if (xmessage_alternate_lines(xmessage) == 4)
    {
        Cmdhandler.AlternateByteMode = QSPI_ALTERNATE_BYTES_4_LINES;
    }

    Cmdhandler.AlternateBytes     = xmessage_alternate(xmessage);
    Cmdhandler.AlternateBytesSize = xmessage_alternate_size(xmessage);

    /* 4 dummy */
    Cmdhandler.DummyCycles = xmessage_dummy_cycles(xmessage);

    /* 5 data */
    if (xmessage_data_lines(xmessage) == 0)
    {
        Cmdhandler.DataMode = QSPI_DATA_NONE;
    }
    else if (xmessage_data_lines(xmessage) == 1)
    {
        Cmdhandler.DataMode = QSPI_DATA_1_LINE;
    }
    else if (xmessage_data_lines(xmessage) == 2)
    {
        Cmdhandler.DataMode = QSPI_DATA_2_LINES;
    }
    else if (xmessage_data_lines(xmessage) == 4)
    {
        Cmdhandler.DataMode = QSPI_DATA_4_LINES;
    }

    Cmdhandler.SIOOMode         = QSPI_SIOO_INST_EVERY_CMD;
    Cmdhandler.DdrMode          = QSPI_DDR_MODE_DISABLE;
    Cmdhandler.DdrHoldHalfCycle = QSPI_DDR_HHC_ANALOG_DELAY;
    Cmdhandler.NbData           = xmessage_data_size(xmessage);

    HAL_QSPI_Command(QSPI_Handler, &Cmdhandler, 5000);
}

static int stm32_qspi_transfer(struct os_sfbus *sfbus, struct os_xspi_message *xmessage)
{
    struct stm32_qspi *qspi = os_container_of(sfbus, struct stm32_qspi, sfbus);

    QSPI_HandleTypeDef *QSPI_Handler = qspi->QSPI_Handler;

    HAL_StatusTypeDef ret = HAL_ERROR;

    stm32_qspi_xfer_command(QSPI_Handler, xmessage);

    if (xmessage_data_lines(xmessage) == 0)
    {
        QSPI_Handler->State = HAL_QSPI_STATE_READY;
        return 0;
    }

    if (xmessage_data_dir(xmessage) == OS_XSPI_DATA_DIR_HOST_TO_DEVICE)
    {
        ret = HAL_QSPI_Transmit(QSPI_Handler, (uint8_t *)xmessage_data(xmessage), 5000);
    }
    else if (xmessage_data_dir(xmessage) == OS_XSPI_DATA_DIR_DEVICE_TO_HOST)
    {
        ret = HAL_QSPI_Receive(QSPI_Handler, (uint8_t *)xmessage_data(xmessage), 5000);
    }

    QSPI_Handler->State = HAL_QSPI_STATE_READY;

    if (ret == HAL_OK)
    {
        return 0;
    }
    else
    {
        return -1;
    }
}

static const struct os_sfbus_ops stm32_qspi_ops = {
    .configure = stm32_qspi_configure,
    .transfer  = stm32_qspi_transfer,
};

static int stm32_qspi_bus_probe(const os_driver_info_t *drv, const os_device_info_t *dev)
{
    struct stm32_qspi *qspi = os_calloc(1, sizeof(struct stm32_qspi));

    OS_ASSERT(qspi != OS_NULL);

    qspi->QSPI_Handler = (QSPI_HandleTypeDef *)dev->info;

    qspi->sfbus.support_1_line = OS_TRUE;
    qspi->sfbus.support_2_line = OS_TRUE;
    qspi->sfbus.support_4_line = OS_TRUE;

    return os_sfbus_xspi_register(&qspi->sfbus, dev->name, &stm32_qspi_ops);
}

OS_DRIVER_INFO stm32_qspi_driver = {
    .name  = "QSPI_HandleTypeDef",
    .probe = stm32_qspi_bus_probe,
};

OS_DRIVER_DEFINE(stm32_qspi_driver, PREV, OS_INIT_SUBLEVEL_LOW);
