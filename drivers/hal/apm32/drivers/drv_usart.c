/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_usart.c
 *
 * @brief       This file implements usart driver for apm32
 *
 * @revision
 * Date         Author          Notes
 * 2022-01-10   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include <os_memory.h>
#include <bus/bus.h>
#include <dma/dma.h>
#include <board.h>
#include <string.h>
#include <os_list.h>

#define DRV_EXT_LVL DBG_EXT_INFO
#define DRV_EXT_TAG "drv.usart"
#include <drv_log.h>

#include "drv_usart.h"

typedef struct apm32_usart
{
    struct os_serial_device  serial;
    struct apm32_usart_info *info;

    soft_dma_t  sdma;
    os_uint32_t sdma_hard_size;

    os_uint8_t *rx_buff;
    os_uint32_t rx_index;
    os_uint32_t rx_size;

    const os_uint8_t *tx_buff;
    os_uint32_t       tx_count;
    os_uint32_t       tx_size;

    os_list_node_t list;
} apm32_usart_t;

static os_list_node_t apm32_usart_list = OS_LIST_INIT(apm32_usart_list);

static void _usart_hard_init(apm32_usart_info_t *info)
{
    info->gpio_PeriphClock(info->gpio_Periph);
    info->usart_PeriphClock(info->usart_Periph);

    GPIO_Config(info->tx_pin_port, &info->tx_pin_info);
    GPIO_Config(info->rx_pin_port, &info->rx_pin_info);

    USART_Config(info->husart, &info->usart_def_cfg);

#ifdef SERIES_APM32F4XX
    GPIO_ConfigPinAF(info->rx_pin_port, info->tx_gpioPinSource, info->tx_gpioAf);
    GPIO_ConfigPinAF(info->rx_pin_port, info->rx_gpioPinSource, info->rx_gpioAf);
#endif

    USART_Enable(info->husart);
}

static void _usart_irq_callback(USART_T *husart, uint16_t index, enum usart_irq_type type)
{
    struct apm32_usart *usart = OS_NULL;

    os_list_for_each_entry(usart, &apm32_usart_list, struct apm32_usart, list)
    {
        if (usart->info->husart == husart)
        {
            break;
        }
    }

    if (usart == OS_NULL)
    {
        return;
    }

    if (type == USART_IRQ_RX)
    {
        usart->rx_buff[usart->rx_index] = (os_uint8_t)USART_RxData(usart->info->husart);
        usart->rx_index++;

        if (usart->rx_index == (usart->rx_size / 2))
        {
            soft_dma_half_irq(&usart->sdma);
        }

        if (usart->rx_index == usart->rx_size)
        {
            soft_dma_full_irq(&usart->sdma);
        }
    }
}

void USART_IRQHandler(USART_T *husart, uint16_t index)
{
    if ((USART_ReadStatusFlag(husart, USART_FLAG_RXBNE) != RESET) &&
        (USART_ReadIntFlag(husart, USART_INT_RXBNE) != RESET))
    {
        _usart_irq_callback(husart, index, USART_IRQ_RX);

        USART_ClearStatusFlag(husart, USART_FLAG_RXBNE);
        USART_ClearIntFlag(husart, USART_INT_RXBNE);
    }

    if (USART_ReadStatusFlag(husart, USART_FLAG_CTS) != RESET)
    {
        USART_ClearStatusFlag(husart, USART_FLAG_CTS);
    }

    if (USART_ReadStatusFlag(husart, USART_FLAG_LBD) != RESET)
    {
        USART_ClearStatusFlag(husart, USART_FLAG_LBD);
    }

    if (USART_ReadStatusFlag(husart, USART_FLAG_OVRE) != RESET)
    {
        USART_RxData(husart);

        USART_ClearStatusFlag(husart, USART_FLAG_OVRE);
    }
}

/* interrupt rx mode */
static os_uint32_t _sdma_int_get_index(soft_dma_t *dma)
{
    apm32_usart_t *usart = os_container_of(dma, apm32_usart_t, sdma);

    return usart->rx_index;
}

static os_err_t _sdma_int_start(soft_dma_t *dma, void *buff, os_uint32_t size)
{
    apm32_usart_t *usart = os_container_of(dma, apm32_usart_t, sdma);

    usart->rx_buff  = buff;
    usart->rx_index = 0;
    usart->rx_size  = size;

    return OS_EOK;
}

static os_uint32_t _sdma_int_stop(soft_dma_t *dma)
{
    apm32_usart_t *usart = os_container_of(dma, apm32_usart_t, sdma);

    return usart->rx_index;
}

/* sdma callback */
static void _usart_sdma_callback(soft_dma_t *dma)
{
    apm32_usart_t *usart = os_container_of(dma, apm32_usart_t, sdma);

    os_hw_serial_isr_rxdone((struct os_serial_device *)usart);
}

static void _usart_sdma_init(struct apm32_usart *usart, dma_ring_t *ring)
{
    soft_dma_t *dma = &usart->sdma;

    soft_dma_stop(dma);

    dma->hard_info.mode     = HARD_DMA_MODE_NORMAL;
    dma->hard_info.max_size = 64 * 1024;

    dma->hard_info.flag         = HARD_DMA_FLAG_HALF_IRQ | HARD_DMA_FLAG_FULL_IRQ;    // | HARD_DMA_FLAG_TIMEOUT_IRQ;
    dma->hard_info.data_timeout = uart_calc_byte_timeout_us(usart->serial.config.baud_rate);

    dma->ops.get_index = _sdma_int_get_index;
    dma->ops.dma_init  = OS_NULL;
    dma->ops.dma_start = _sdma_int_start;
    dma->ops.dma_stop  = _sdma_int_stop;

    dma->cbs.dma_half_callback    = _usart_sdma_callback;
    dma->cbs.dma_full_callback    = _usart_sdma_callback;
    dma->cbs.dma_timeout_callback = _usart_sdma_callback;

    soft_dma_init(dma);
    soft_dma_start(dma, ring);
    soft_dma_irq_enable(dma, OS_TRUE);
}

static os_err_t _usart_init(struct os_serial_device *serial, struct serial_configure *cfg)
{
    USART_Config_T usart_cfg = {0};

    struct apm32_usart *usart = (struct apm32_usart *)serial;

    usart_cfg.mode         = USART_MODE_TX_RX;
    usart_cfg.hardwareFlow = USART_HARDWARE_FLOW_NONE;
    usart_cfg.baudRate     = cfg->baud_rate;

    switch (cfg->stop_bits)
    {
    case STOP_BITS_1:
        usart_cfg.stopBits = USART_STOP_BIT_1;
        break;
    case STOP_BITS_2:
        usart_cfg.stopBits = USART_STOP_BIT_2;
        break;
    default:
        usart_cfg.stopBits = USART_STOP_BIT_1;
        break;
    }
    switch (cfg->parity)
    {
    case PARITY_NONE:
        usart_cfg.parity = USART_PARITY_NONE;
        break;
    case PARITY_ODD:
        usart_cfg.parity = USART_PARITY_ODD;
        break;
    case PARITY_EVEN:
        usart_cfg.parity = USART_PARITY_EVEN;
        break;
    }

    switch (cfg->data_bits)
    {
    case DATA_BITS_8:
        usart_cfg.wordLength = USART_WORD_LEN_8B;
        break;
    default:
        LOG_E(DRV_EXT_TAG, "wordLength not support!");
        break;
    }

    usart->info->usart_PeriphClock(usart->info->usart_Periph);

    USART_Config(usart->info->husart, &usart_cfg);
    USART_Enable(usart->info->husart);

    NVIC_EnableIRQRequest(usart->info->irq_type, usart->info->irq_PrePri, usart->info->irq_SubPri);

    USART_EnableInterrupt(usart->info->husart, USART_INT_RXBNE);

    _usart_sdma_init(usart, &usart->serial.rx_fifo->ring);

    return OS_EOK;
}

static os_err_t _usart_deinit(struct os_serial_device *serial)
{
    struct apm32_usart *usart = (struct apm32_usart *)serial;

    USART_Reset(usart->info->husart);

    return OS_EOK;
}

/* clang-format off */
static int _usart_poll_send(struct os_serial_device *serial, const os_uint8_t *buff, os_size_t size)
{
    int i = 0;

    struct apm32_usart *usart = (struct apm32_usart *)serial;

    for (i = 0; i < size; i++)
    {
        while (USART_ReadStatusFlag(usart->info->husart, USART_FLAG_TXC) == RESET);

        USART_TxData(usart->info->husart, *(buff + i));
    }

    return size;
}
/* clang-format on */

static const struct os_uart_ops _usart_ops = {
    .init   = _usart_init,
    .deinit = _usart_deinit,

    .start_send = OS_NULL,
    .poll_send  = _usart_poll_send,
};

static void _usart_parse_config(struct apm32_usart *usart)
{
    struct os_serial_device *serial = &usart->serial;

    serial->config.baud_rate = usart->info->usart_def_cfg.baudRate;

    switch (usart->info->usart_def_cfg.stopBits)
    {
    case USART_STOP_BIT_1:
        serial->config.stop_bits = STOP_BITS_1;
        break;
    case USART_STOP_BIT_2:
        serial->config.stop_bits = STOP_BITS_2;
        break;
    default:
        LOG_E(DRV_EXT_TAG, "stop bit not support!");
        break;
    }
    switch (usart->info->usart_def_cfg.parity)
    {
    case USART_PARITY_NONE:
        serial->config.parity = PARITY_NONE;
        break;
    case USART_PARITY_ODD:
        serial->config.parity = PARITY_ODD;
        break;
    case USART_PARITY_EVEN:
        serial->config.parity = PARITY_EVEN;
        break;
    }

    switch (usart->info->usart_def_cfg.wordLength)
    {
    case USART_WORD_LEN_8B:
        serial->config.data_bits = DATA_BITS_8;
        break;
    default:
        LOG_E(DRV_EXT_TAG, "wordLength not support!");
        break;
    }
}

static os_err_t _usart_probe(const os_driver_info_t *drv, const os_device_info_t *dev)
{
    os_base_t               level  = 0;
    struct serial_configure config = OS_SERIAL_CONFIG_DEFAULT;

    struct apm32_usart *usart = os_calloc(1, sizeof(struct apm32_usart));
    if (usart == OS_NULL)
    {
        return OS_EOK;
    }

    usart->info          = (struct apm32_usart_info *)dev->info;
    usart->serial.ops    = &_usart_ops;
    usart->serial.config = config;

#ifndef OS_USING_CONSOLE
    _usart_hard_init(usart->info);
#endif

    _usart_parse_config(usart);

    level = os_irq_lock();
    os_list_add_tail(&apm32_usart_list, &usart->list);
    os_irq_unlock(level);

    if (os_hw_serial_register(&usart->serial, dev->name, NULL) != OS_EOK)
    {
        LOG_E(DRV_EXT_TAG, "os_hw_serial_register error!");
    }

    return OS_EOK;
}

OS_DRIVER_INFO _usart_driver = {
    .name  = "USART_HandleTypeDef",
    .probe = _usart_probe,
};

OS_DRIVER_DEFINE(_usart_driver, PREV, OS_INIT_SUBLEVEL_HIGH);

#ifdef OS_USING_CONSOLE
static USART_T *console_uart = 0;
/* clang-format off */
void __os_hw_console_output(char *str)
{
    if (console_uart == 0)
        return;

    while (*str)
    {
        USART_TxData(console_uart, *str);
        while (!USART_ReadStatusFlag(console_uart, USART_FLAG_TXC));
        str++;
    }
}
/* clang-format on */

static os_err_t _usart_early_probe(const os_driver_info_t *drv, const os_device_info_t *dev)
{
    apm32_usart_info_t *info = (apm32_usart_info_t *)dev->info;

    _usart_hard_init(info);

    if (!strcmp(dev->name, OS_CONSOLE_DEVICE_NAME))
    {
        console_uart = (USART_T *)info->husart;
    }
    return OS_EOK;
}

OS_DRIVER_INFO _usart_early_driver = {
    .name  = "USART_HandleTypeDef",
    .probe = _usart_early_probe,
};

OS_DRIVER_DEFINE(_usart_early_driver, CORE, OS_INIT_SUBLEVEL_LOW);
#endif
