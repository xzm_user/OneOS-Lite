/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_flash_common.c
 *
 * @brief       This file provides flash read/write/erase functions for apm32.
 *
 * @revision
 * Date         Author          Notes
 * 2022-01-11   OneOS Team      First Version
 ***********************************************************************************************************************
 */
#include <os_memory.h>
#include "dlog.h"
#include "board.h"

#define DRV_EXT_LVL DBG_EXT_INFO
#define DRV_EXT_TAG "drv.flash"
#include <drv_log.h>

#include "drv_flash.h"
#include "apm32f4xx_fmc.h"

#ifndef APM32_FLASH_BLOCK_SIZE
#define APM32_FLASH_BLOCK_SIZE (16 * 1024)
#endif

#define APM32_FLASH_PAGE_SIZE (8)

struct flash_sector_info
{
    os_uint32_t addr;
    os_uint32_t size;
};

#ifdef OPTCR1_BYTE2_ADDRESS /*if define Bank 1 and Bank 2*/
static struct flash_sector_info flash_sector_table[] = {
    /* Base address of the Flash sectors Bank 2 */
    {0x08000000, 16 * 1024},
    {0x08004000, 16 * 1024},
    {0x08008000, 16 * 1024},
    {0x0800C000, 16 * 1024},
    {0x08010000, 64 * 1024},
    {0x08020000, 128 * 1024},
    {0x08040000, 128 * 1024},
    {0x08060000, 128 * 1024},
    {0x08080000, 128 * 1024},
    {0x080A0000, 128 * 1024},
    {0x080C0000, 128 * 1024},
    {0x080E0000, 128 * 1024},
    /* B{ase address of the Flash sectors Bank 2 */
    {0x08100000, 16 * 1024},
    {0x08104000, 16 * 1024},
    {0x08108000, 16 * 1024},
    {0x0810C000, 16 * 1024},
    {0x08110000, 64 * 1024},
    {0x08120000, 128 * 1024},
    {0x08140000, 128 * 1024},
    {0x08160000, 128 * 1024},
    {0x08180000, 128 * 1024},
    {0x081A0000, 128 * 1024},
    {0x081C0000, 128 * 1024},
    {0x081E0000, 128 * 1024},
};
#else
/* clang-format off */
static struct flash_sector_info flash_sector_table[] = 
{
    {0x08000000, 16 * 1024},
    {0x08004000, 16 * 1024},
    {0x08008000, 16 * 1024},
    {0x0800C000, 16 * 1024},
    {0x08010000, 64 * 1024},
    {0x08020000, 128 * 1024},
    {0x08040000, 128 * 1024},
    {0x08060000, 128 * 1024},
    {0x08080000, 128 * 1024},
    {0x080A0000, 128 * 1024},
    {0x080C0000, 128 * 1024},
    {0x080E0000, 128 * 1024},
    {0x08100000, 128 * 1024},
    {0x08120000, 128 * 1024},
    {0x08140000, 128 * 1024},
    {0x08160000, 128 * 1024},
    {0x08180000, 128 * 1024},
    {0x081A0000, 128 * 1024},
    {0x081C0000, 128 * 1024},
    {0x081E0000, 128 * 1024},
    {0x08200000, 128 * 1024},
    {0x08220000, 128 * 1024},
    {0x08240000, 128 * 1024},
    {0x08260000, 128 * 1024},
};
/* clang-format on */
#endif

static os_ssize_t _get_sector_info(os_uint32_t *FirstSector, os_uint32_t *EndSector, os_uint32_t addr, size_t size)
{
    os_uint32_t i       = 0;
    os_uint32_t address = addr;

    for (i = 0; i < (sizeof(flash_sector_table) / sizeof(flash_sector_table[0]) - 1); i++)
    {
        if ((address >= flash_sector_table[i].addr) && (address < flash_sector_table[i + 1].addr))
        {
            if (address != flash_sector_table[i].addr)
            {
                LOG_E(DRV_EXT_TAG,
                      "err addr: (0x%p) addr is in sector base(0x%p)",
                      (void *)addr,
                      (void *)flash_sector_table[i].addr);
                return OS_ERROR;
            }

            *FirstSector = i;
            break;
        }
    }

    address = address + size;
    for (i = 0; i < (sizeof(flash_sector_table) / sizeof(flash_sector_table[0]) - 1); i++)
    {
        if ((address >= flash_sector_table[i].addr) && (address < flash_sector_table[i + 1].addr))
        {
            if (address != flash_sector_table[i].addr)
            {
                LOG_E(DRV_EXT_TAG,
                      "err size: maybe base (0x%p) size(0x%x)",
                      (void *)flash_sector_table[(*FirstSector)].addr,
                      flash_sector_table[i + 1].addr - flash_sector_table[(*FirstSector)].addr);

                return OS_ERROR;
            }

            *EndSector = i;
            break;
        }
    }

    return OS_EOK;
}

int apm32_flash_read(os_uint32_t addr, os_uint8_t *buf, size_t size)
{
    size_t i;

    if ((addr + size) > APM32_FLASH_END_ADDRESS)
    {
        LOG_E(DRV_EXT_TAG, "read outrange flash size! addr is (0x%p)", (void *)(addr + size));
        return OS_EINVAL;
    }

    for (i = 0; i < size; i++, buf++, addr++)
    {
        *buf = *(os_uint8_t *)addr;
    }

    return size;
}

int apm32_flash_write(os_uint32_t addr, const os_uint8_t *buf, size_t size)
{
    size_t      i, j;
    os_err_t    result     = 0;
    os_uint32_t write_data = 0, temp_data = 0;

    if ((addr + size) > APM32_FLASH_END_ADDRESS)
    {
        LOG_E(DRV_EXT_TAG, "ERROR: write outrange flash size! addr is (0x%p)", (void *)(addr + size));
        return OS_EINVAL;
    }

    if (addr % 4 != 0)
    {
        LOG_E(DRV_EXT_TAG, "write addr must be 4-byte alignment");
        return OS_EINVAL;
    }

    FMC_Unlock();

    if (size < 1)
    {
        return OS_ERROR;
    }

    for (i = 0; i < size;)
    {
        if ((size - i) < 4)
        {
            for (j = 0; (size - i) > 0; i++, j++)
            {
                temp_data  = *buf;
                write_data = (write_data) | (temp_data << 8 * j);
                buf++;
            }
        }
        else
        {
            for (j = 0; j < 4; j++, i++)
            {
                temp_data  = *buf;
                write_data = (write_data) | (temp_data << 8 * j);
                buf++;
            }
        }

        /* write data */
        if (FMC_ProgramWord(addr, write_data) == FMC_COMPLETE)
        {
            /* Check the written value */
            if (*(uint32_t *)addr != write_data)
            {
                LOG_E(DRV_EXT_TAG, "ERROR: write data != read data");
                result = OS_ERROR;
                goto __exit;
            }
        }
        else
        {
            result = OS_ERROR;
            goto __exit;
        }

        temp_data  = 0;
        write_data = 0;

        addr += 4;
    }

__exit:
    FMC_Lock();
    if (result != 0)
    {
        return result;
    }

    return size;
}

int apm32_flash_erase(os_uint32_t addr, size_t size)
{
    os_err_t    result      = OS_EOK;
    os_uint32_t FirstSector = 0, EndSector = 0;
    os_uint32_t i = 0;

    if ((addr + size) > APM32_FLASH_END_ADDRESS)
    {
        LOG_E(DRV_EXT_TAG, "ERROR: erase outrange flash size! addr is (0x%p)", (void *)(addr + size));
        return OS_EINVAL;
    }

    FMC_Unlock();

    if (_get_sector_info(&FirstSector, &EndSector, addr, size) != OS_EOK)
    {
        return OS_EINVAL;
    }

    for (i = FirstSector; i < EndSector; i++)
    {
        if (FMC_EraseSector((FMC_SECTOR_T)(i * 0x08), FMC_VOLTAGE_3) != FMC_COMPLETE)
        {
            result = OS_ERROR;
            goto __exit;
        }
    }

__exit:
    FMC_Lock();

    if (result != OS_EOK)
    {
        return result;
    }

    LOG_D(DRV_EXT_TAG, "erase done: addr (0x%p), size %d", (void *)addr, size);
    return size;
}

#include "fal_drv_flash.c"
