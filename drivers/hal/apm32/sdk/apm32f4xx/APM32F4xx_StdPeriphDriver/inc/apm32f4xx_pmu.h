/*!
 * @file       apm32f4xx_pmu.h
 *
 * @brief      This file contains all the functions prototypes for the PMU firmware library.
 *
 * @version    V1.0.0
 *
 * @date       2021-09-08
 *
 */

#ifndef __APM32F4XX_PMU_H
#define __APM32F4XX_PMU_H

#ifdef __cplusplus
  extern "C" {
#endif

#include "apm32f4xx.h"

/** @addtogroup Peripherals_Library Standard Peripheral Library
  @{
*/

/** @addtogroup PMU_Driver PMU Driver
  @{
*/

/** @addtogroup PMU_Enumerations Enumerations
  @{
*/

/**
 * @brief PMU PVD detection level
 */
typedef enum
{
    PMU_PVD_LEVEL_0,
    PMU_PVD_LEVEL_1,
    PMU_PVD_LEVEL_2,
    PMU_PVD_LEVEL_3,
    PMU_PVD_LEVEL_4,
    PMU_PVD_LEVEL_5,
    PMU_PVD_LEVEL_6,
    PMU_PVD_LEVEL_7
} PMU_PVD_LEVEL_T;

/**
 * @brief PMU Regulator state in STOP mode
 */
typedef enum
{
    PMU_MAIN_REGULATOR,
    PMU_LOWPOWER_REGULATOR
} PMU_REGULATOR_T;

/**
 * @brief PMU STOP mode entry
 */
typedef enum
{
    PMU_STOP_ENTRY_WFI     = 0x01,
    PMU_STOP_ENTRY_WFE     = 0x02
} PMU_STOP_ENTRY_T;

/**
 * @brief PMU Regulator Voltage Scale
 */
typedef enum
{
    PMU_REGULATOR_VOLTAGE_SCALE1 = 0x03,
    PMU_REGULATOR_VOLTAGE_SCALE2 = 0x02,
    PMU_REGULATOR_VOLTAGE_SCALE3 = 0x01,
} PMU_REGULATOR_VOLTAGE_SCALE_T;

/**
 * @brief PMU Flag
 */
typedef enum
{
    PMU_FLAG_WUEFLG,
    PMU_FLAG_SBFLG,
    PMU_FLAG_PVDOFLG,
    PMU_FLAG_BKPRFLG,
    PMU_FLAG_VOSRFLG
} PMU_FLAG_T;

/**@} end of group PMU_Enumerations*/

/** @addtogroup PMU_Fuctions Fuctions
  @{
*/

/** PMU Reset */
void PMU_Reset(void);

/** Configuration and Operation modes */
void PMU_EnableBackupAccess(void);
void PMU_DisableBackupAccess(void);
void PMU_EnablePVD(void);
void PMU_DisablePVD(void);
void PMU_ConfigPVDLevel(PMU_PVD_LEVEL_T level);
void PMU_EnableWakeUpPin(void);
void PMU_DisableWakeUpPin(void);
void PMU_EnableBackupRegulator(void);
void PMU_DisableBackupRegulator(void);
void PMU_ConfigMainRegulatorMode(PMU_REGULATOR_VOLTAGE_SCALE_T scale);
void PMU_EnableFlashPowerDown(void);
void PMU_DisableFlashPowerDown(void);
void PMU_EnterSTOPMode(PMU_REGULATOR_T regulator, PMU_STOP_ENTRY_T entry);
void PMU_EnterSTANDBYMode(void);

/** flags */
uint8_t PMU_ReadStatusFlag(PMU_FLAG_T flag);
void PMU_ClearStatusFlag(PMU_FLAG_T flag);

/**@} end of group PMU_Fuctions*/
/**@} end of group PMU_Driver */
/**@} end of group Peripherals_Library*/

#ifdef __cplusplus
}
#endif

#endif /* __APM32F4XX_PMU_H */
