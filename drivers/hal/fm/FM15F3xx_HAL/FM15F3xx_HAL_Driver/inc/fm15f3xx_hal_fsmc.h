/**
 ******************************************************************************
 * @file    fm15f3xx_ll_fsmc.h
 * @author  WYL
 * @version V1.0.0
 * @date    2020-03-17
 * @brief
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2020 FudanMicroelectronics.
 * All rights reserved.</center></h2>
 *
 ******************************************************************************
 */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __FM15F3XX_HAL_FSMC_H
#define __FM15F3XX_HAL_FSMC_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "fm15f3xx_hal_def.h"
#include "fm15f3xx_ll_fsmc.h"


/** @addtogroup FM15F3XX_HAL_Driver
 * @{
 */


/** @addtogroup FSMC
 * @{
 */


/* Exported types ------------------------------------------------------------*/


/** @defgroup FSMC_Exported_Types FSMC Exported Types
 * @{
 */


/**
 * @brief FSMC Timing Config structures definition
 */


/**
 * @brief FSMC NORSRAM Init structures definition
 */
typedef struct {
    uint32_t AccessMode;                    /*!< Select MMAP or EXTM Mode                                                    */

    uint32_t MemoryWidth;                   /*!< Specifies the external memory device width.                                 */

    uint32_t NWaitEn;                       /*!< Enables or disables the burst access mode for Flash memory,
                                                 valid only with synchronous burst Flash memories.                           */
    uint32_t NWaitPol;                      /*!< Specifies the wait signal polarity, valid only when accessing
                                                 the Flash memory in burst mode.                                             */
    uint32_t ClkEn;                         /*!< FSMC CLK output control                                                     */

    uint32_t ClkDiv;                        /*!< FSMC CLK output control                                                     */
} FSMC_NORSRAMInitTypeDef;


/**
 * @brief FSMC LCD Init structures definition
 */
typedef struct {
    uint32_t AccessMode;                    /*!< Select MMAP or EXTM Mode                                                    */

    uint32_t LcdWidth;                      /*!< Specifies the external LCD device width.                                    */

    uint32_t LcdType;                       /*!< LCD interface: 0:6800;1:8080                                                */

    uint32_t EnOrRdPol;                     /*!< 6800 interface En signal Polarity or 8080 interface Rd signal Polarity      */

    uint32_t RwOrWrPol;                     /*!< 6800 interface Rw signal Polarity or 8080 interface Wr signal Polarity      */
} FSMC_LCDInitTypeDef;

typedef struct {
    uint32_t AddressSetupTime;              /*!< Defines the number of sys_clk cycles to configure
                                                the duration of the address setup time.
                                                This parameter can be a value between Min_Data = 1 and Max_Data = 15.
                                                @note This parameter is not used with synchronous NOR Flash memories.      */

    uint32_t AddressHoldTime;               /*!< Defines the number of sys_clk cycles to configure
                                                 the duration of the address hold time.
                                                 This parameter can be a value between Min_Data = 0 and Max_Data = 15.
                                                 @note This parameter is not used with synchronous NOR Flash memories.      */

    uint32_t DataSetupTime;                 /*!< Defines the number of sys_clk cycles to configure
                                                 the duration of the data setup time.
                                                 This parameter can be a value between Min_Data = 1 and Max_Data = 255.
                                                 @note This parameter is used for SRAMs, ROMs and asynchronous multiplexed
                                                 NOR Flash memories.                                                        */
    uint32_t DataHoldTime;                  /*!< Defines the number of sys_clk cycles to configure
                                                 the duration of the data hold time.
                                                 This parameter can be a value between Min_Data = 0 and Max_Data = 15.
                                                 @note This parameter is used for SRAMs, ROMs and asynchronous multiplexed
                                                 NOR Flash memories.                                                        */
    uint32_t BusReadyTime;                  /*!< Defines the number of sys_clk cycles to configure
                                                 the duration of the bus ready.
                                                 This parameter can be a value between Min_Data = 0 and Max_Data = 15.
                                                 @note This parameter is only used for multiplexed NOR Flash memories.      */
} FSMC_TimingConfigTypeDef;


/**
 * @brief  HAL FSMC state machine: FSMC states definition (bitfields)
 */
/* States of FSMC global scope */
#define HAL_FSMC_STATE_RESET    0x00000000U /*!< FSMC not yet initialized or disabled */
#define HAL_FSMC_STATE_READY    0x00000001U /*!< FSMC peripheral ready for use        */
#define HAL_FSMC_STATE_BUSY     0x00000002U /*!< FSMC is busy                         */
#define HAL_FSMC_STATE_ERROR    0x00000004U /*!< ERRPR occurrence                     */
#define HAL_FSMC_STATE_TIMEOUT  0x00000008U /*!< TimeOut occurrence                   */


/**
 * @brief  fsmc handle Structure definition
 */
typedef struct {
    FSMC_TypeDef *Instance;                 /*!< Register base address    */

    FSMC_NORSRAMInitTypeDef NORSRAMInit;    /*!< FSMC required parameters */

    FSMC_LCDInitTypeDef LCDInit;            /*!< FSMC required parameters */

    __IO uint32_t State;                    /*!< FSMC communication state */
} FSMC_HandleTypeDef;


/**
 * @}
 */


/*************************************************************CTRL REG*************************************************/


/** @defgroup FSMC_Enable
 * @{
 */
#define FSMC_DISABLE    LL_FSMC_DISABLE
#define FSMC_ENABLE     LL_FSMC_ENABLE


/**
 * @}
 */


/** @defgroup FSMC_Mode
 * @{
 */
#define FSMC_NORSRAM_MODE   LL_FSMC_NORSRAM_MODE
#define FSMC_LCD_MODE       LL_FSMC_LCD_MODE


/**
 * @}
 */


/** @defgroup FSMC_Width
 * @{
 */
#define FSMC_WIDTH_4    LL_FSMC_WIDTH_4
#define FSMC_WIDTH_8    LL_FSMC_WIDTH_8
#define FSMC_WIDTH_16   LL_FSMC_WIDTH_16


/**
 * @}
 */


/** @defgroup FSMC_Wait
 * @{
 */
#define FSMC_NWAIT_DISABLE  LL_FSMC_NWAIT_DISABLE
#define FSMC_NWAIT_ENABLE   LL_FSMC_NWAIT_ENABLE


/**
 * @}
 */


/** @defgroup FSMC_WaitlPol
 * @{
 */
#define FSMC_NWAITPOL_LOW   LL_FSMC_NWAITPOL_LOW
#define FSMC_NWAITPOL_HIGH  LL_FSMC_NWAITPOL_HIGH


/**
 * @}
 */


/** @defgroup FSMC_ClkEn
 * @{
 */
#define FSMC_CLK_DISABLE    LL_FSMC_CLK_DISABLE
#define FSMC_CLK_ENABLE     LL_FSMC_CLK_ENABLE


/**
 * @}
 */


/** @defgroup FSMC_ClkEn
 * @{
 */
#define FSMC_CLKDIV_1   (0x00U)
#define FSMC_CLKDIV_2   (0x01 << FSMC_SYNCT_CLKDIV_Pos)
#define FSMC_CLKDIV_3   (0x02 << FSMC_SYNCT_CLKDIV_Pos)
#define FSMC_CLKDIV_4   (0x03 << FSMC_SYNCT_CLKDIV_Pos)
#define FSMC_CLKDIV_5   (0x04 << FSMC_SYNCT_CLKDIV_Pos)
#define FSMC_CLKDIV_6   (0x05 << FSMC_SYNCT_CLKDIV_Pos)
#define FSMC_CLKDIV_7   (0x06 << FSMC_SYNCT_CLKDIV_Pos)
#define FSMC_CLKDIV_8   (0x07 << FSMC_SYNCT_CLKDIV_Pos)
#define FSMC_CLKDIV_9   (0x08 << FSMC_SYNCT_CLKDIV_Pos)
#define FSMC_CLKDIV_10  (0x09 << FSMC_SYNCT_CLKDIV_Pos)
#define FSMC_CLKDIV_11  (0x0A << FSMC_SYNCT_CLKDIV_Pos)
#define FSMC_CLKDIV_12  (0x0B << FSMC_SYNCT_CLKDIV_Pos)
#define FSMC_CLKDIV_13  (0x0C << FSMC_SYNCT_CLKDIV_Pos)
#define FSMC_CLKDIV_14  (0x0D << FSMC_SYNCT_CLKDIV_Pos)
#define FSMC_CLKDIV_15  (0x0E << FSMC_SYNCT_CLKDIV_Pos)
#define FSMC_CLKDIV_16  (0x0F << FSMC_SYNCT_CLKDIV_Pos)


/**
 * @}
 */


/** @defgroup FSMC_Lcd Type
 * @{
 */
#define FSMC_LCDTYPE_6800   LL_FSMC_LCDTYPE_6800
#define FSMC_LCDTYPE_8080   LL_FSMC_LCDTYPE_8080


/**
 * @}
 */


/** @defgroup FSMC_ENorRDPol
 * @{
 */
#define FSMC_EnOrRd_LOW     LL_FSMC_ENorRD_LOW
#define FSMC_EnOrRd_HIGH    LL_FSMC_ENorRD_HIGH


/**
 * @}
 */


/** @defgroup LL_FSMC_RWorWRPol
 * @{
 */
#define FSMC_RwOrWr_LOW     LL_FSMC_RWorWR_LOW
#define FSMC_RwOrWr_HIGH    LL_FSMC_RWorWR_HIGH


/**
 * @}
 */
/******************************************************EXTM********************************************************/


/** @defgroup LL_FSMC_AccessMode
 * @{
 */
#define FSMC_ACCESS_MMAP        LL_FSMC_ACCESS_MMAP
#define FSMC_ACCESS_EXTM        LL_FSMC_ACCESS_EXTM
#define FSMC_ACCESS_EXTMFIFO    LL_FSMC_ACCESS_EXTMFIFO
#define FSMC_ACCESS_GPIO        LL_FSMC_ACCESS_GPIO


/**
 * @}
 */

/******************************************************INTE********************************************************/


/** @defgroup LL_FSMC_INTEnable
 * @{
 */
#define FSMC_INT_EORD       LL_FSMC_INT_EORD
#define FSMC_INT_EOWR       LL_FSMC_INT_EOWR
#define FSMC_INT_ERR        LL_FSMC_INT_ERR
#define FSMC_INT_WMRD       LL_FSMC_INT_WMRD
#define FSMC_INT_RDFIFOOV   LL_FSMC_INT_RDFIFOOV
#define FSMC_INT_WMWR       LL_FSMC_INT_WMWR
#define FSMC_INT_WRFIFOOV   LL_FSMC_INT_WRFIFOOV
#define FSMC_DMA_WMTX       LL_FSMC_DMA_WMTX
#define FSMC_DMA_WMRX       LL_FSMC_DMA_WMRX
#define FSMC_INT_ALL        (0x0FF)
#define FSMC_DMA_ALL        (0x200)


/**
 * @}
 */


/** @defgroup FSMC_LOCK_MASK
 * @{
 */
#define FSMC_LOCK_MASK (0x80000000U)


/**
 * @}
 */


/** @defgroup LL_FSMC_FLAG
 * @{
 */
#define FSMC_FLAG_DONE      LL_FSMC_FLAG_DONE
#define FSMC_FLAG_RSTDONE   LL_FSMC_FLAG_RSTDONE
#define FSMC_FLAG_ERR       LL_FSMC_FLAG_ERR
#define FSMC_FLAG_RSTFIFO   LL_FSMC_FLAG_RSTFIFO

#define FSMC_FLAG_RDINT         LL_FSMC_FLAG_RDINT
#define FSMC_FLAG_WRINT         LL_FSMC_FLAG_WRINT
#define FSMC_FLAG_ERRINT        LL_FSMC_FLAG_ERRINT
#define FSMC_FLAG_RDFIFOINT     LL_FSMC_FLAG_RDFIFOINT
#define FSMC_FLAG_RDFIFOOVINT   LL_FSMC_FLAG_RDFIFOOVINT
#define FSMC_FLAG_WRFIFOINT     LL_FSMC_FLAG_WRFIFOINT
#define FSMC_FLAG_WRFIFOOVINT   LL_FSMC_FLAG_WRFIFOOVINT
#define FSMC_FLAG_RDFIFOCNT     LL_FSMC_FLAG_RDFIFOCNT
#define FSMC_FLAG_RDFIFOEMPTY   LL_FSMC_FLAG_RDFIFOEMPTY
#define FSMC_FLAG_RDFIFOFULL    LL_FSMC_FLAG_RDFIFOFULL
#define FSMC_FLAG_WRFIFOCNT     LL_FSMC_FLAG_WRFIFOCNT
#define FSMC_FLAG_WRFIFOEMPTY   LL_FSMC_FLAG_WRFIFOEMPTY
#define FSMC_FLAG_WRFIFOFULL    LL_FSMC_FLAG_WRFIFOFULL


/**
 * @}
 */


/* Exported macro ------------------------------------------------------------*/


/** @defgroup FSMC_Exported_Macros FSMC Exported Macros
 * @{
 */


/**
 * @brief  LOCK FSMC REG
 * @param  __REG__ FSMC handle
 * @retval None
 */
#define __HAL_FSMC_LOCK_REG( __REG__ ) (__REG__) |= FSMC_LOCK_MASK


/**
 * @brief  UNLOCK FSMC REG
 * @param  __REG__ FSMC handle
 * @retval None
 */
#define __HAL_FSMC_UNLOCK_REG( __REG__ ) (__REG__) &= ~FSMC_LOCK_MASK


/**
 * @brief  Enable the FSMC peripheral.
 * @param  __HANDLE__ FSMC handle
 * @retval None
 */
#define __HAL_FSMC_ENABLE( __HANDLE__ ) (__HANDLE__)->Instance->CTRL |= FSMC_CRTL_EN


/**
 * @brief  Disable the FSMC peripheral.
 * @param  __HANDLE__ FSMC handle
 * @retval None
 */
#define __HAL_FSMC_DISABLE( __HANDLE__ ) (__HANDLE__)->Instance->CTRL &= ~FSMC_CRTL_EN


/**
 * @brief  Enable the FSMC INT.
 * @param  __HANDLE__ FSMC handle
 * @retval None
 */
#define __HAL_FSMC_ENABLE_INT( __HANDLE__, __INT__ ) (__HANDLE__)->Instance->INTE |= (__INT__)


/**
 * @brief  Disable the FSMC INT.
 * @param  __HANDLE__ FSMC handle
 * @param  __INT__ FSMC INT
 * @retval None
 */
#define __HAL_FSMC_DISABLE_INT( __HANDLE__, __INT__ ) (__HANDLE__)->Instance->INTE &= ~(__INT__)


/**
 * @brief  Is EXTM Mode.
 * @param  __HANDLE__ FSMC handle
 * @param  __MODE__ Mode
 * @retval None
 */
#define __HAL_FSMC_IS_EXTMMODE( __HANDLE__, __MODE__ ) ( ( (__HANDLE__)->Instance->EXTM & (__MODE__) ) == (__MODE__) )


/**
 * @brief  Is Flag Active.
 * @param  __HANDLE__ FSMC handle
 * @param  __FLAG__ Flag
 * @retval None
 */
#define __HAL_FSMC_IS_ACTIVESTATUS( __HANDLE__, __FLAG__ ) ( ( (__HANDLE__)->Instance->STAT & (__FLAG__) ) == (__FLAG__) )


/**
 * @brief  Clear Flag.
 * @param  __HANDLE__ FSMC handle
 * @param  __FLAG__ Flag
 * @retval None
 */
#define __HAL_FSMC_CLEAR_STATUS( __HANDLE__, __FLAG__ ) ( (__HANDLE__)->Instance->STAT |= (__FLAG__) )


/**
 * @brief  Is INT Flag Active.
 * @param  __HANDLE__ FSMC handle
 * @param  __FLAG__ Flag
 * @retval None
 */
#define __HAL_FSMC_IS_ACTIVEINTSTATUS( __HANDLE__, __FLAG__ ) ( ( (__HANDLE__)->Instance->INTF & (__FLAG__) ) == (__FLAG__) )


/**
 * @brief  Clear INT Flag.
 * @param  __HANDLE__ FSMC handle
 * @param  __FLAG__ Flag
 * @retval None
 */
#define __HAL_FSMC_CLEAR_INTSTATUS( __HANDLE__, __FLAG__ ) ( (__HANDLE__)->Instance->INTF |= (__FLAG__) )


/**
 * @brief  Set write fifo for the FSMC EXTM Mode.
 * @param  __HANDLE__ FSMC handle
 * @param  __WRWM__ write fifo watermark:0~4
 * @retval None
 */
#define __HAL_FSMC_SET_WRFIFOWATERMARK( __HANDLE__, __WRWM__ ) (__HANDLE__)->Instance->FCFG &= ~FSMC_FCFG_WFLEVEL_Msk; \
    (__HANDLE__)->Instance->FCFG                                                            |= (__WRWM__ << FSMC_FCFG_WFLEVEL_Pos)


/**
 * @brief  Set read fifo for the FSMC EXTM Mode.
 * @param  __HANDLE__ FSMC handle
 * @param  __RDWM__ read fifo watermark:0~4
 * @retval None
 */
#define __HAL_FSMC_SET_RDFIFOWATERMARK( __HANDLE__, __RDWM__ ) (__HANDLE__)->Instance->FCFG &= ~FSMC_FCFG_RFLEVEL_Msk; \
    (__HANDLE__)->Instance->FCFG                                                            |= (__RDWM__ << FSMC_FCFG_RFLEVEL_Pos)


/**
 * @brief  Set read or write Number for the FSMC EXTM Mode.
 * @param  __HANDLE__ FSMC handle
 * @param  __NUM__ read or write Number:0~255
 * @retval None
 */
#define __HAL_FSMC_SET_RDWRNUM( __HANDLE__, __NUM__ ) (__HANDLE__)->Instance->WNUM = (__NUM__)


/**
 * @brief  Set address for the FSMC EXTM Mode.
 * @param  __HANDLE__ FSMC handle
 * @param  __ADD__ read or write address
 * @retval None
 */
#define __HAL_FSMC_SET_ADDRESS( __HANDLE__, __ADD__ ) (__HANDLE__)->Instance->FADDR = (__ADD__)


/**
 * @brief  write data for the FSMC EXTM Mode.
 * @param  __HANDLE__ FSMC handle
 * @param  __DATA__ write data
 * @retval None
 */
#define __HAL_FSMC_WRITE_DATA( __HANDLE__, __DATA__ ) (__HANDLE__)->Instance->FDATA = (__DATA__)


/**
 * @brief  start write/read data for the FSMC EXTM Mode.
 * @param  __HANDLE__ FSMC handle
 * @retval None
 */
#define __HAL_FSMC_START_WRITE( __HANDLE__ ) ( (__HANDLE__)->Instance->STAT |= (FSMC_STAT_RW));\
    ( (__HANDLE__)->Instance->STAT |= (FSMC_STAT_DONE))

#define __HAL_FSMC_START_READ( __HANDLE__ ) ( (__HANDLE__)->Instance->STAT &= ~FSMC_STAT_RW);\
    ( (__HANDLE__)->Instance->STAT |= FSMC_STAT_DONE)

/**
 * @brief  read data for the FSMC EXTM Mode.
 * @param  __HANDLE__ FSMC handle
 * @param  __DATA__ read data
 * @retval None
 */
#define __HAL_FSMC_READ_DATA( __HANDLE__, __DATA__ ) (__DATA__) = (__HANDLE__)->Instance->FDATA


/**
 * @} FSMC_Exported_Macros
 */

/* Exported functions --------------------------------------------------------*/


/** @addtogroup FSMC_Exported_Functions
 * @{
 */

/* Initialization/de-initialization functions ***********************************/
HAL_StatusTypeDef HAL_FSMC_NORSRAMInit(FSMC_HandleTypeDef* hfsmc);


HAL_StatusTypeDef HAL_FSMC_LCDInit(FSMC_HandleTypeDef* hfsmc);


void HAL_FSMC_MspInit(FSMC_HandleTypeDef* hfsmc);


void HAL_FSMC_WriteTimingConfig(FSMC_HandleTypeDef *hfsmc, FSMC_TimingConfigTypeDef *FSMC_TimingtStruct);


void HAL_FSMC_ReadTimingConfig(FSMC_HandleTypeDef *hfsmc, FSMC_TimingConfigTypeDef *FSMC_TimingtStruct);


/* Peripheral Control functions ***********************************************/
HAL_StatusTypeDef HAL_FSMC_START(FSMC_HandleTypeDef *hfsmc);


HAL_StatusTypeDef HAL_FSMC_Stop(FSMC_HandleTypeDef *hfsmc);


HAL_StatusTypeDef HAL_FSMC_EXTM_IT(FSMC_HandleTypeDef *hfsmc, uint32_t IT);


HAL_StatusTypeDef HAL_FSMC_EXTMFIFO_IT(FSMC_HandleTypeDef *hfsmc, uint32_t IT, uint8_t RDFIFOWaterMark, uint8_t WRFIFOWaterMark);


HAL_StatusTypeDef HAL_FSMC_Stop_IT(FSMC_HandleTypeDef *hfsmc);


void HAL_FSMC_IRQHandler(FSMC_HandleTypeDef* hfsmc);


void HAL_FSMC_RdWrCpltCallback(FSMC_HandleTypeDef* hfsmc);


void HAL_FSMC_RdWrFifoCallback(FSMC_HandleTypeDef* hfsmc);


void HAL_FSMC_RdWrFifoOvCallback(FSMC_HandleTypeDef* hfsmc);


void HAL_FSMC_ErrorCallback(FSMC_HandleTypeDef* hfsmc);


HAL_StatusTypeDef HAL_FSMC_EXTM_Write(FSMC_HandleTypeDef *hfsmc, uint32_t Address, uint32_t* Buffer, uint8_t Num, uint32_t Timeout);


HAL_StatusTypeDef HAL_FSMC_EXTM_Read(FSMC_HandleTypeDef *hfsmc, uint32_t Address, uint32_t* Buffer, uint8_t Num, uint32_t Timeout);


HAL_StatusTypeDef HAL_FSMC_EXTMFIFO_Write(FSMC_HandleTypeDef *hfsmc, uint32_t Address, uint32_t* Buffer, uint8_t Num, uint32_t Timeout);


HAL_StatusTypeDef HAL_FSMC_EXTMFIFO_Read(FSMC_HandleTypeDef *hfsmc, uint32_t Address, uint32_t* Buffer, uint8_t Num, uint32_t Timeout);


/* Peripheral State functions *************************************************/
uint32_t HAL_FSMC_GetState(FSMC_HandleTypeDef* hfsmc);


/**
 * @}FSMC_Exported_Functions
 */

/* Private macros ------------------------------------------------------------*/


/** @defgroup FSMC_Private_Macros FSMC Private Macros
 * @{
 */
#define IS_FSMC_INSTANCE( INSTANCE )        ( (INSTANCE) == FSMC)
#define IS_FSMC_MEMORYWIDTH( WIDTH )        ( ( (WIDTH) == FSMC_WIDTH_8) || \
                                              ( (WIDTH) == FSMC_WIDTH_16) )
#define IS_FSMC_LCDWIDTH( WIDTH )           ( ( (WIDTH) == FSMC_WIDTH_4) || \
                                              ( (WIDTH) == FSMC_WIDTH_8) || \
                                              ( (WIDTH) == FSMC_WIDTH_16) )
#define IS_FSMC_NWAITEN( EN )               ( ( (EN) == FSMC_NWAIT_DISABLE) || \
                                              ( (EN) == FSMC_NWAIT_ENABLE) )
#define IS_FSMC_NWAITPOL( POL )             ( ( (POL) == FSMC_NWAITPOL_LOW) || \
                                              ( (POL) == FSMC_NWAITPOL_HIGH) )
#define IS_FSMC_CLKEN( EN )                 ( ( (EN) == FSMC_CLK_DISABLE) || \
                                              ( (EN) == FSMC_CLK_ENABLE) )
#define IS_FSMC_CLKDIV( DIV )               ( (DIV) <= 0xF)
#define IS_FSMC_ACCESSMODE( MODE )          ( ( (MODE) == FSMC_ACCESS_MMAP) || \
                                              ( (MODE) == FSMC_ACCESS_EXTM) || \
                                              ( (MODE) == FSMC_ACCESS_GPIO) )
#define IS_FSMC_LCDTYPE( TYPE )             ( ( (TYPE) == FSMC_LCDTYPE_6800) || \
                                              ( (TYPE) == FSMC_LCDTYPE_8080) )
#define IS_FSMC_ENORRDPOL( POL )            ( ( (POL) == FSMC_EnOrRd_LOW) || \
                                              ( (POL) == FSMC_EnOrRd_HIGH) )
#define IS_FSMC_RWORWRPOL( POL )            ( ( (POL) == FSMC_RwOrWr_LOW) || \
                                              ( (POL) == FSMC_RwOrWr_HIGH) )
#define IS_FSMC_ADDRESSSETUPTIME( CYCLE )   ( (CYCLE) <= 0xF)
#define IS_FSMC_ADDRESSHOLDTIME( CYCLE )    ( (CYCLE) <= 0xF)
#define IS_FSMC_DATASETUPTIME( CYCLE )      ( (CYCLE) <= 0xFF)
#define IS_FSMC_DATAHOLDTIME( CYCLE )       ( (CYCLE) <= 0xF)
#define IS_FSMC_BUSREADYTIME( CYCLE )       ( (CYCLE) <= 0xF)


/**
 * @}FSMC_Private_Macros
 */


/**
 * @}FSMC_HAL
 */


/**
 * @}FM15F3XX_HAL_Driver
 */


#ifdef __cplusplus
}
#endif

#endif

