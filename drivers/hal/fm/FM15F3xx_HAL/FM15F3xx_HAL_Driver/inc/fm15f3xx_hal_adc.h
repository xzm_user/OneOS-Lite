/**
 ******************************************************************************
 * @file    fm15f3xx_hal_adc.h
 * @author  WYL
 * @version V1.0.0
 * @date    2020-03-17
 * @brief
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2020 FudanMicroelectronics.
 * All rights reserved.</center></h2>
 *
 ******************************************************************************
 */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __FM15F3xx_HAL_ADC_H
#define __FM15F3xx_HAL_ADC_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "fm15f3xx_hal_def.h"
#include "fm15f3xx_ll_adc.h"


/** @addtogroup FM15F3xx_HAL_Driver
 * @{
 */


/** @addtogroup ADC
 * @{
 */

/* Exported types ------------------------------------------------------------*/


/** @defgroup ADC_Exported_Types ADC Exported Types
 * @{
 */


/**
 * @brief  Structure definition of ADC and initialization
 */
typedef struct {
    uint32_t WorkClkSel;                /* Specifies refrence workclock select. cmu_mdlclkb reg.*/

    uint32_t WorkClkDiv;                /* Specifies refrence workclock select. cmu_mdlclkb reg.*/

    uint32_t ClkSel;                    /* Specifies refrence adcclock select. adc_cfg reg.*/

    uint32_t AutoSleepEn;               /* Enable or disable autosleep function. adc_cfg reg.*/

    uint32_t VcomEn;                    /* Enable or disable difference bias function. adc_cfg reg.*/

    uint32_t SleepWaitTime;             /* Specifies sleep wait time set. adc_cfg reg */

    uint32_t WorkMode;                  /* Specifies work mode:free trigger,polling. adc_work reg.*/

    uint32_t ConvertMode;               /* Specifies convert mode:one shot, continue. adc_work reg.*/

    uint32_t PollingNum;                /* Specifies polling number set. adc_work reg.*/

    uint32_t SampleTime;                /* Specifies sample time set. adc_time reg.*/

    uint32_t ReadyTime;                 /* Specifies ready time set. adc_time reg.*/

//  uint32_t ConvertTime;                 /* Specifies convert time. adc_time reg.*/
//  uint32_t SampleHoldTime;              /* Specifies sample hold time. adc_time reg.*/
} ADC_InitTypeDef;


/**
 * @brief  ADC Channel Init structures definition and initialization
 */
typedef struct {
    uint32_t Channel;                   /* Specifies ADC channel 0\1\2\3 select.*/

    uint32_t Input;                     /* Specifies input source select. adc_chx_cfg reg.*/

    uint32_t TrigSrc;                   /* Specifies trigger source select. adc_chx_cfg reg.*/

    uint32_t DiffEn;                    /* Enable or disable difference function. adc_chx_cfg reg.*/

    uint32_t TrigInvEn;                 /* Enable or disable the trigger inverter function. adc_chx_cfg reg.*/

    uint32_t TrigEdge;                  /* Specifies high or rising or falling or double edge trig select. adc_chx_cfg reg.*/

    uint32_t TrigDelay;                 /* Specifies trigger delay. adc_chx_cfg reg*/
} ADC_ChannelConfigTypeDef;


/**
 * @brief  ADC Data Processing structures definition and initialization
 */
typedef struct {
    uint32_t Channel;                   /* Specifies ADC channel 0\1\2\3 select.*/

    uint32_t OffsetEn;                  /* Enable or disable offset function. adc_chx_drs reg*/

    uint32_t OffsetValue;               /* Specifies offset value. adc_chx_drs reg*/

    uint32_t AverageEn;                 /* Enable or disable average function.*/

    uint32_t AverageNum;                /* Specifies average number set. adc_chx_drs reg.*/

    uint32_t CompareEn;                 /* Enable or disable convert compare function. adc_chx_drs reg.*/

    uint32_t CompareMode;               /* Specifies convert compare mode select. adc_chx_drs reg.*/

    uint32_t CompareValue1;             /* Specifies convert compare value set. adc_chx_drs reg.*/

    uint32_t CompareValue2;             /* Specifies convert compare value set. adc_chx_drs reg.*/
} ADC_DataProcessConfigTypeDef;


/**
 * @brief  ADC VREFH VALUE (mV)
 *@note default VREFH = VDD 3300mV
 */
extern uint32_t ADC_VREFH_VALUE;


/**
 * @brief  HAL ADC state machine: ADC states definition (bitfields)
 */
/* States of ADC global scope */
#define HAL_ADC_STATE_RESET         0x00000000U         /*!< ADC not yet initialized or disabled */
#define HAL_ADC_STATE_READY         0x00000001U         /*!< ADC peripheral ready for use */
#define HAL_ADC_STATE_BUSY_INTERNAL 0x00000002U         /*!< ADC is busy to internal process (initialization, calibration) */
#define HAL_ADC_STATE_TIMEOUT       0x00000004U         /*!< TimeOut occurrence */

/* States of ADC errors */
#define HAL_ADC_STATE_ERROR_INTERNAL    0x00000010U     /*!< Internal error occurrence */
#define HAL_ADC_STATE_ERROR_CONFIG      0x00000020U     /*!< Configuration error occurrence */
#define HAL_ADC_STATE_ERROR_DMA         0x00000040U     /*!< DMA error occurrence */
#define HAL_ADC_STATE_ERROR_CONFLICT    0x00000080U     /*!< Confilct error occurrence */

/* States of ADC group  */
#define HAL_ADC_STATE_BUSY  0x00000100U                 /*!< A conversion on group regular is ongoing or can occur (either by continuous mode,
                                                           external trigger, low power auto power-on (if feature available), multimode ADC master control (if feature available)) */
#define HAL_ADC_STATE_EOC   0x00000200U                 /*!< Conversion data available on group regular */


/**
 * @brief  ADC handle Structure definition
 */
typedef struct {
    ADC_TypeDef *Instance;                              /*!< Register base address */

    ADC_InitTypeDef Init;                               /*!< ADC required parameters */

    __IO uint32_t State;                                /*!< ADC communication state */

    __IO uint32_t ErrorCode;                            /*!< ADC Error code */
} ADC_HandleTypeDef;


/**
 * @}
 */

/* Exported constants --------------------------------------------------------*/


/** @defgroup ADC_Exported_Constants ADC Exported Constants
 * @{
 */


/** @defgroup ADC_Error_Code ADC Error Code
 * @{
 */
#define HAL_ADC_ERROR_NONE      0x00U                       /*!< No error                                              */
#define HAL_ADC_ERROR_INTERNAL  0x01U                       /*!< ADC IP internal error: if problem of clocking,
                                                                 enable/disable, erroneous state                       */
#define HAL_ADC_ERROR_OVR       0x02U                       /*!< Overrun error                                         */
#define HAL_ADC_ERROR_DMA       0x04U                       /*!< DMA transfer error                                    */


/**
 * @}
 */


/** @defgroup ADC_ClockPrescaler  ADC Clock Source
 * @{
 */
#define ADC_WORKSEL_4M  0x00000000U                         /* 4Mhz */
#define ADC_WORKSEL_16M (0x1U << CMU_MDLCLKB_ADCSEL_Pos)    /* 16Mhz */
#define ADC_WORKSEL_OSC (0x2U << CMU_MDLCLKB_ADCSEL_Pos)    /* Osc */
#define ADC_WORKSEL_PLL (0x3U << CMU_MDLCLKB_ADCSEL_Pos)    /* PLL */


/**
 * @}
 */


/** @defgroup ADC_ClockPrescaler  ADC Clock Prescaler
 * @{
 */
#define ADC_WORKCLK_DIV1    0x00000000U
#define ADC_WORKCLK_DIV2    (0x1U << CMU_MDLCLKB_ADCDIV_Pos)
#define ADC_WORKCLK_DIV4    (0x2U << CMU_MDLCLKB_ADCDIV_Pos)
#define ADC_WORKCLK_DIV8    (0x3U << CMU_MDLCLKB_ADCDIV_Pos)


/**
 * @}
 */


/** @defgroup ADC CLK select
 * @{
 */
#define ADC_CLKSEL_WORKCLKDIV1  (0x0U << ADC_CFG_CLKSEL_Pos)
#define ADC_CLKSEL_WORKCLKDIV2  (0x1U << ADC_CFG_CLKSEL_Pos)
#define ADC_CLKSEL_FORCE0       (0x2U << ADC_CFG_CLKSEL_Pos)
#define ADC_CLKSEL_FORCE1       (0x3U << ADC_CFG_CLKSEL_Pos)
#define ADC_CLKSEL_BUSCLKDIV2   (0x4U << ADC_CFG_CLKSEL_Pos)
#define ADC_CLKSEL_BUSCLKDIV4   (0x5U << ADC_CFG_CLKSEL_Pos)
#define ADC_CLKSEL_BUSCLKDIV6   (0x6U << ADC_CFG_CLKSEL_Pos)
#define ADC_CLKSEL_BUSCLKDIV8   (0x7U << ADC_CFG_CLKSEL_Pos)


/**
 * @}
 */


/** @defgroup ADC select channel
 * @{
 */
#define ADC_CHANNEL_0   (0x0U)
#define ADC_CHANNEL_1   (0x1U)
#define ADC_CHANNEL_2   (0x2U)
#define ADC_CHANNEL_3   (0x3U)


/**
 * @}
 */


/** @defgroup ADC AutoSleep enable auto sleep mode
 * @{
 */
#define ADC_AUTOSLEEP_DISABLE   LL_ADC_AUTOSLEEP_DISABLE
#define ADC_AUTOSLEEP_ENABLE    LL_ADC_AUTOSLEEP_ENABLE


/**
 * @}
 */


/** @defgroup ADC VcomEn enable
 * @{
 */
#define ADC_VCOM_DISABLE    LL_ADC_VCOM_DISABLE
#define ADC_VCOM_ENABLE     LL_ADC_VCOM_ENABLE


/**
 * @}
 */


/** @defgroup ADC work mode
 * @{
 */
#define ADC_WORKMODE_FREETRIG   (0x00000000U)
#define ADC_WORKMODE_POLLING    (ADC_WORK_MODE)


/**
 * @}
 */


/** @defgroup ADC convert mode
 * @{
 */
#define ADC_CONVMODE_CONTINUE   (0x00000000U)                       /* continue convert */
#define ADC_CONVMODE_ONESHOT    (ADC_WORK_ONESHOT)                  /* oneshot mode  */


/**
 * @}
 */


/** @defgroup  ADC External Trigger Source
 * @{
 */
#define ADC_TRIGSRC_TRGO_IN         (0x0U)
#define ADC_TRIGSRC_CMP             (0x1U << ADC_CHxCFG_TRGSC_Pos)
#define ADC_TRIGSRC_STIMER0         (0x2U << ADC_CHxCFG_TRGSC_Pos)
#define ADC_TRIGSRC_STIMER1         (0x3U << ADC_CHxCFG_TRGSC_Pos)
#define ADC_TRIGSRC_STIMER2         (0x4U << ADC_CHxCFG_TRGSC_Pos)
#define ADC_TRIGSRC_STIMER3         (0x5U << ADC_CHxCFG_TRGSC_Pos)
#define ADC_TRIGSRC_STIMER4         (0x6U << ADC_CHxCFG_TRGSC_Pos)
#define ADC_TRIGSRC_STIMER5         (0x7U << ADC_CHxCFG_TRGSC_Pos)
#define ADC_TRIGSRC_LTIMER          (0x8U << ADC_CHxCFG_TRGSC_Pos)
#define ADC_TRIGSRC_STIMERTGS_CH6   (0x9U << ADC_CHxCFG_TRGSC_Pos)
#define ADC_TRIGSRC_STIMERTGS_CH7   (0xAU << ADC_CHxCFG_TRGSC_Pos)
#define ADC_TRIGSRC_RTC_ALARM       (0xBU << ADC_CHxCFG_TRGSC_Pos)
#define ADC_TRIGSRC_TAMPER_ALARM    (0xCU << ADC_CHxCFG_TRGSC_Pos)
#define ADC_TRIGSRC_RTC_1S          (0xDU << ADC_CHxCFG_TRGSC_Pos)
#define ADC_TRIGSRC_SOFTWARE        (0xEU << ADC_CHxCFG_TRGSC_Pos)
#define ADC_TRIGSRC_RFU             (0xFU << ADC_CHxCFG_TRGSC_Pos)


/**
 * @}
 */


/** @defgroup  ADC External Trigger Edge
 * @{
 */
#define ADC_EXTTRIGCONVEDGE_NONE        (0x0U)
#define ADC_EXTTRIGCONVEDGE_RISING      (0x1U << ADC_CHxCFG_TRGEDGE_Pos)
#define ADC_EXTTRIGCONVEDGE_FALLING     (0x2U << ADC_CHxCFG_TRGEDGE_Pos)
#define ADC_EXTTRIGCONVEDGE_BOTHEDGE    (0x3U << ADC_CHxCFG_TRGEDGE_Pos)


/**
 * @}
 */


/** @defgroup  ADC Flags Definition
 * @{
 */
#define ADC_FLAG_DONEERR    (ADC_SRCL_DONEERR)
#define ADC_FLAG_READY      (ADC_SRCL_READY)
#define ADC_FLAG_EOC0       (ADC_SRCL_CH0CONVCO)
#define ADC_FLAG_EOC1       (ADC_SRCL_CH1CONVCO)
#define ADC_FLAG_EOC2       (ADC_SRCL_CH2CONVCO)
#define ADC_FLAG_EOC3       (ADC_SRCL_CH3CONVCO)
#define ADC_FLAG_CH0OR      (ADC_SRCL_CH0OVRUN)
#define ADC_FLAG_CH1OR      (ADC_SRCL_CH1OVRUN)
#define ADC_FLAG_CH2OR      (ADC_SRCL_CH2OVRUN)
#define ADC_FLAG_CH3OR      (ADC_SRCL_CH3OVRUN)
#define ADC_FLAG_ALL        (0x3F3F0F)


/**
 * @}
 */


/** @defgroup  ADC INSRC
 * @{
 */
#define ADC_INSRC_DP0           (0x00U)
#define ADC_INSRC_DP1           (0x01U << ADC_CHxCFG_IN_Pos)
#define ADC_INSRC_DP2           (0x02U << ADC_CHxCFG_IN_Pos)
#define ADC_INSRC_ADC3          (0x03U << ADC_CHxCFG_IN_Pos)
#define ADC_INSRC_ADC4          (0x04U << ADC_CHxCFG_IN_Pos)
#define ADC_INSRC_ADC5          (0x05U << ADC_CHxCFG_IN_Pos)
#define ADC_INSRC_ADC6          (0x06U << ADC_CHxCFG_IN_Pos)
#define ADC_INSRC_ADC7          (0x07U << ADC_CHxCFG_IN_Pos)
#define ADC_INSRC_ADC8          (0x08U << ADC_CHxCFG_IN_Pos)
#define ADC_INSRC_ADC9          (0x09U << ADC_CHxCFG_IN_Pos)
#define ADC_INSRC_ADC10         (0x0AU << ADC_CHxCFG_IN_Pos)
#define ADC_INSRC_ADC11         (0x0BU << ADC_CHxCFG_IN_Pos)
#define ADC_INSRC_ADC12         (0x0CU << ADC_CHxCFG_IN_Pos)
#define ADC_INSRC_ADC13         (0x0DU << ADC_CHxCFG_IN_Pos)
#define ADC_INSRC_ADC14         (0x0EU << ADC_CHxCFG_IN_Pos)
#define ADC_INSRC_VINCHARGE     (0x0FU << ADC_CHxCFG_IN_Pos)
#define ADC_INSRC_VOUTCHARGE    (0x10U << ADC_CHxCFG_IN_Pos)
#define ADC_INSRC_12BITDAC      (0x12U << ADC_CHxCFG_IN_Pos)
#define ADC_INSRC_TEMPERATURE   (0x13U << ADC_CHxCFG_IN_Pos)
#define ADC_INSRC_VREF12        (0x14U << ADC_CHxCFG_IN_Pos)
#define ADC_INSRC_VDDBKP        (0x16U << ADC_CHxCFG_IN_Pos)
#define ADC_INSRC_6BITDAC       (0x18U << ADC_CHxCFG_IN_Pos)


/**
 * @}
 */


/** @defgroup ADC ChxDiff
 * @{
 */
#define ADC_CHxDIFF_DISABLE LL_ADC_CHxDIFF_DISABLE
#define ADC_CHxDIFF_ENABLE  LL_ADC_CHxDIFF_ENABLE


/**
 * @}
 */


/** @defgroup ADC ChxTrigInvEn
 * @{
 */
#define ADC_CHxTRIGINV_DISABLE  LL_ADC_CHxTRIGINV_DISABLE
#define ADC_CHxTRIGINV_ENABLE   LL_ADC_CHxTRIGINV_ENABLE


/**
 * @}
 */


/** @defgroup ADC ChxOffsetEn
 * @{
 */
#define ADC_CHxOFFSET_DISABLE   LL_ADC_CHxOFFSET_DISABLE
#define ADC_CHxOFFSET_ENABLE    LL_ADC_CHxOFFSET_ENABLE


/**
 * @}
 */


/** @defgroup ADC Chx AverageEn
 * @{
 */
#define ADC_CHxAVERAGE_DISABLE  LL_ADC_CHxAVERAGE_DISABLE
#define ADC_CHxAVERAGE_ENABLE   LL_ADC_CHxAVERAGE_ENABLE


/**
 * @}
 */


/** @defgroup ADC Chx CompareEn
 * @{
 */
#define ADC_CHxCOMPARE_DISABLE  LL_ADC_CHxCOMPARE_DISABLE
#define ADC_CHxCOMPARE_ENABLE   LL_ADC_CHxCOMPARE_ENABLE


/**
 * @}
 */


/** @defgroup  ADC AVERAGE NUM
 * @{
 */
#define ADC_AVGNUM_2    (0x0U << ADC_CHxDRS_AVGNUM_Pos)
#define ADC_AVGNUM_4    (0x1U << ADC_CHxDRS_AVGNUM_Pos)
#define ADC_AVGNUM_6    (0x2U << ADC_CHxDRS_AVGNUM_Pos)
#define ADC_AVGNUM_8    (0x3U << ADC_CHxDRS_AVGNUM_Pos)


/**
 * @}
 */


/** @defgroup  ADC COMPARE MODE
 * @{
 */
#define ADC_CMP_LARGER_CV2      (0x0U << ADC_CHxDRS_CMPM_Pos)
#define ADC_CMP_LESS_CV1        (0x1U << ADC_CHxDRS_CMPM_Pos)
#define ADC_CMP_BETWEEN_CV1CV2  (0x2U << ADC_CHxDRS_CMPM_Pos)
#define ADC_CMP_OUTSIDE_CV1CV2  (0x3U << ADC_CHxDRS_CMPM_Pos)


/**
 * @}
 */


/**
 * @}
 */

/* Exported macro ------------------------------------------------------------*/


/** @defgroup ADC_Exported_Macros ADC Exported Macros
 * @{
 */


/**
 * @brief  Enable the ADC peripheral.
 * @param  __HANDLE__ ADC handle
 * @retval None
 */
#define __HAL_ADC_ENABLE( __HANDLE__ ) (__HANDLE__)->Instance->CTRL |= ADC_CTRL_ADCRSTN; \
    (__HANDLE__)->Instance->CTRL                                    |= ADC_CTRL_ADCEN


/**
 * @brief  Disable the ADC peripheral.
 * @param  __HANDLE__ ADC handle
 * @retval None
 */
#define __HAL_ADC_DISABLE( __HANDLE__ ) ( (__HANDLE__)->Instance->CTRL &= ~(ADC_CTRL_ADCEN) )


/**
 * @brief  Enable the ADC end of conversion interrupt.
 * @param  __HANDLE__ specifies the ADC Handle.
 * @param  __CHANNEL__ ADC channel.
 * @retval None
 */
#define __HAL_ADC_ENABLE_IT( __HANDLE__, __CHANNEL__ ) ( (__HANDLE__)->Instance->INTEN) &= ~(1 << (__CHANNEL__ + 4) ); \
    ( (__HANDLE__)->Instance->INTEN)                                                    |= (1 << __CHANNEL__)


/**
 * @brief  Disable the ADC end of conversion interrupt.
 * @param  __HANDLE__ specifies the ADC Handle.
 * @param  __CHANNEL__ ADC channel.
 * @retval None
 */
#define __HAL_ADC_DISABLE_IT( __HANDLE__, __CHANNEL__ ) ( ( (__HANDLE__)->Instance->INTEN) &= ~(1 << __CHANNEL__) )


/**
 * @brief  Enable the ADC end of conversion DMA.
 * @param  __HANDLE__ specifies the ADC Handle.
 * @param  __CHANNEL__ ADC channel.
 * @retval None
 */
#define __HAL_ADC_ENABLE_DMA( __HANDLE__, __CHANNEL__ ) ( (__HANDLE__)->Instance->INTEN) |= ( (1 << (__CHANNEL__ + 4) ) | (1 << __CHANNEL__) )


/**
 * @brief  Disable the ADC end of conversion DMA.
 * @param  __HANDLE__ specifies the ADC Handle.
 * @param  __CHANNEL__ ADC channel.
 * @retval None
 */
#define __HAL_ADC_DISABLE_DMA( __HANDLE__, __CHANNEL__ ) ( ( (__HANDLE__)->Instance->INTEN) &= ~(1 << __CHANNEL__) )


/**
 * @brief  Clear the ADC's pending flags.
 * @param  __HANDLE__ specifies the ADC Handle.
 * @param  __FLAG__ ADC flag.
 * @retval None
 */
#define __HAL_ADC_CLEAR_FLAG( __HANDLE__, __FLAG__ ) ( ( (__HANDLE__)->Instance->SRCL) |= (__FLAG__) )


/**
 * @brief  Get the selected ADC's flag status.
 * @param  __HANDLE__ specifies the ADC Handle.
 * @param  __FLAG__ ADC flag.
 * @retval None
 */
#define __HAL_ADC_GET_FLAG( __HANDLE__, __FLAG__ ) ( ( ( (__HANDLE__)->Instance->STATUSRCD) & (__FLAG__) ) == (__FLAG__) )


/**
 * @brief  Enable the ADC Channel of conversion.
 * @param  __HANDLE__ specifies the ADC Handle.
 * @param  __CHANNEL__ ADC channel.
 * @retval None
 */
#define __HAL_ADC_ENABLE_MULTICHANNEL( __HANDLE__, __CHANNEL__ ) ( (__HANDLE__)->Instance->CHANNEL[__CHANNEL__].CFG) |= (1)


/**
 * @brief  Enable only one Channel of conversion.
 * @param  __HANDLE__ specifies the ADC Handle.
 * @param  __CHANNEL__ ADC channel.
 * @retval None
 */
#define __HAL_ADC_ENABLE_CHANNEL( __HANDLE__, __CHANNEL__ ) ( (__HANDLE__)->Instance->CHANNEL[0].CFG)   &= ~(1); \
    ( (__HANDLE__)->Instance->CHANNEL[1].CFG)                                                           &= ~(1); \
    ( (__HANDLE__)->Instance->CHANNEL[2].CFG)                                                           &= ~(1); \
    ( (__HANDLE__)->Instance->CHANNEL[3].CFG)                                                           &= ~(1); \
    __HAL_ADC_ENABLE_MULTICHANNEL( __HANDLE__, __CHANNEL__ )


/**
 * @brief  Disable the ADC Channel of conversion.
 * @param  __HANDLE__ specifies the ADC Handle.
 * @param  __CHANNEL__ ADC channel.
 * @retval None
 */
#define __HAL_ADC_DISABLE_CHANNEL( __HANDLE__, __CHANNEL__ ) ( (__HANDLE__)->Instance->CHANNEL[__CHANNEL__].CFG) &= ~(1)


/**
 * @brief  Verification the ADC's  in one shot mode.
 * @param  __HANDLE__ specifies the ADC Handle.
 * @retval None
 */
#define __HAL_ADC_IS_ONESHOT( __HANDLE__ ) ( ( ( (__HANDLE__)->Instance->WORK) & (ADC_WORK_ONESHOT) ) == (ADC_WORK_ONESHOT) )


/**
 * @brief  Verification the software trig.
 * @param  __HANDLE__ specifies the ADC Handle.
 * @param  __CHANNEL__ ADC channel.
 * @retval None
 */
#define __HAL_ADC_IS_SOFTWARETRIG( __HANDLE__, __CHANNEL__ ) ( ( ( (__HANDLE__)->Instance->CHANNEL[__CHANNEL__].CFG) & (ADC_CHxCFG_EN | ADC_TRIGSRC_SOFTWARE) ) == (ADC_CHxCFG_EN | ADC_TRIGSRC_SOFTWARE) )


/**
 * @brief  ENABLE VREF 1.2V
 * @retval None
 */
#define __HAL_ADC_ENABLE_VREF12() MODIFY_REG( VREF->CTRL, VREF_CTRL_SCMODE_Msk | VREF_CTRL_VREFEN_Msk | VREF_CTRL_LDOEN_Msk, (1 << VREF_CTRL_SCMODE_Pos) | VREF_CTRL_VREFEN | VREF_CTRL_LDOEN )


/**
 * @brief  DISABLE VREF 1.2V
 * @retval None
 */
#define __HAL_ADC_DISABLE_VREF12() CLEAR_BIT( VREF->CTRL, VREF_CTRL_VREFEN | VREF_CTRL_LDOEN )


/**
 * @brief  Get the VREF12 BG Flag.
 * @retval None
 */
#define __HAL_ADC_VREF12_BG_FLAG() READ_BIT( VREF->BG_FLAG, VREF_BG_FLAG )


/**
 * @brief  Set the ADC's work clock.
 * @param  WORKCKL parameter.
 * @param  CLKDIV parameter.
 * @retval None
 */
#define __HAL_ADC_SET_WORKCLK( WORKCKL, CLKDIV ) MODIFY_REG( CMU->MDLCLKB, CMU_MDLCLKB_ADCSEL | CMU_MDLCLKB_ADCDIV, WORKCKL | CLKDIV )


/**
 * @}
 */


/* Exported functions --------------------------------------------------------*/


/** @addtogroup ADC_Exported_Functions
 * @{
 */
/* Initialization/de-initialization functions ***********************************/
HAL_StatusTypeDef HAL_ADC_Init(ADC_HandleTypeDef* hadc);


HAL_StatusTypeDef HAL_ADC_DeInit(ADC_HandleTypeDef *hadc);


void HAL_ADC_MspInit(ADC_HandleTypeDef* hadc);


void HAL_ADC_MspDeInit(ADC_HandleTypeDef* hadc);


/* I/O operation functions ******************************************************/
HAL_StatusTypeDef HAL_ADC_Start(ADC_HandleTypeDef* hadc, uint32_t Channel);


HAL_StatusTypeDef HAL_ADC_Stop(ADC_HandleTypeDef* hadc);


HAL_StatusTypeDef HAL_ADC_PollForConversion(ADC_HandleTypeDef* hadc, uint32_t Channel, uint32_t Timeout);


HAL_StatusTypeDef HAL_ADC_Start_IT(ADC_HandleTypeDef* hadc, uint32_t Channel);


HAL_StatusTypeDef HAL_ADC_Stop_IT(ADC_HandleTypeDef* hadc, uint32_t Channel);


void HAL_ADC_IRQHandler(ADC_HandleTypeDef* hadc, uint32_t Channel);


HAL_StatusTypeDef HAL_ADC_Start_DMA(ADC_HandleTypeDef* hadc, uint32_t Channel);


HAL_StatusTypeDef HAL_ADC_Stop_DMA(ADC_HandleTypeDef* hadc, uint32_t Channel);


uint32_t HAL_ADC_GetValue(ADC_HandleTypeDef* hadc, uint32_t Channel);


void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef* hadc, uint32_t channel);


/* Peripheral Control functions *************************************************/
HAL_StatusTypeDef HAL_ADC_ChannelConfig(ADC_HandleTypeDef* hadc, ADC_ChannelConfigTypeDef* ChannelConfig);


HAL_StatusTypeDef HAL_ADC_DataProcrssConfig(ADC_HandleTypeDef* hadc, ADC_DataProcessConfigTypeDef* ProcessConfig);


/* Peripheral State functions ***************************************************/
uint32_t HAL_ADC_GetState(ADC_HandleTypeDef* hadc);


uint32_t HAL_ADC_GetError(ADC_HandleTypeDef *hadc);


HAL_StatusTypeDef HAL_ADC_Polling_GetValue(ADC_HandleTypeDef* hadc, uint32_t* pData);


/**
 * @}
 */
/* Private types -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private constants ---------------------------------------------------------*/


/** @defgroup ADC_Private_Constants ADC Private Constants
 * @{
 */
/* Delay for ADC stabilization time.                                        */
/* Maximum delay is 1us .                                                   */
/* Unit: us                                                                 */
#define ADC_STAB_DELAY_US   3U
#define ADC_STAB_DELAY_MS   50U
/* Delay for temperature sensor stabilization time.                         */
/* Maximum delay is 10us.                                                   */
/* Unit: us                                                                 */
#define ADC_TEMPSENSOR_DELAY_US 10U
/* Delay for temperature sensor stabilization time.                         */
/* Maximum delay is 10ms.                                                   */
/* Unit: ms                                                                 */
#define ADC_VREF12_DELAY_MS 5U
/* VREF 1.2 Value.                                                          */
/* Unit: mV                                                                 */
#define ADC_VREF12_VALUE 1200U


/**
 * @}
 */

/* Private macro ------------------------------------------------------------*/


/** @defgroup ADC_Private_Macros ADC Private Macros
 * @{
 */


/* Macro reserved for internal HAL driver usage, not intended to be used in
   code of final user */


/**
 * @brief Verification of ADC state: enabled or disabled
 * @param __HANDLE__ ADC handle
 * @retval SET (ADC enabled) or RESET (ADC disabled)
 */
#define ADC_IS_ENABLE( __HANDLE__ )                                              \
    ( ( ( ( (__HANDLE__)->Instance->CTRL & ADC_CTRL_ADCEN) == ADC_CTRL_ADCEN)            \
        ) ? SET : RESET)


/**
 * @brief Simultaneously clears and sets specific bits of the handle State
 * @note: ADC_STATE_CLR_SET() macro is merely aliased to generic macro MODIFY_REG(),
 *        the first parameter is the ADC handle State, the second parameter is the
 *        bit field to clear, the third and last parameter is the bit field to set.
 * @retval None
 */
#define ADC_STATE_CLR_SET MODIFY_REG


/**
 * @brief Clear ADC error code (set it to error code: "no error")
 * @param __HANDLE__ ADC handle
 * @retval None
 */
#define ADC_CLEAR_ERRORCODE( __HANDLE__ ) ( (__HANDLE__)->ErrorCode = HAL_ADC_ERROR_NONE)

#define IS_ADC_ALL_INSTANCE( INSTANCE )         ( (INSTANCE) == ADC0)
#define IS_ADC_WORKCLK( WORKCLK )               ( ( (WORKCLK) == ADC_WORKSEL_4M) || \
                                                  ( (WORKCLK) == ADC_WORKSEL_16M) || \
                                                  ( (WORKCLK) == ADC_WORKSEL_OSC) || \
                                                  ( (WORKCLK) == ADC_WORKSEL_PLL) )
#define IS_ADC_WORKCLKPRES( WORKCLOCKPRES )     ( ( (WORKCLOCKPRES) == ADC_WORKCLK_DIV1) || \
                                                  ( (WORKCLOCKPRES) == ADC_WORKCLK_DIV2) || \
                                                  ( (WORKCLOCKPRES) == ADC_WORKCLK_DIV4) || \
                                                  ( (WORKCLOCKPRES) == ADC_WORKCLK_DIV8) )
#define IS_ADC_AUTOSLEEP_EN( EN )               ( ( (EN) == ADC_AUTOSLEEP_ENABLE) || \
                                                  ( (EN) == ADC_AUTOSLEEP_DISABLE) )
#define IS_ADC_VCOM_EN( EN )                    ( ( (EN) == ADC_VCOM_ENABLE) || \
                                                  ( (EN) == ADC_VCOM_DISABLE) )
#define IS_ADC_CLK( CLK )                       ( ( (CLK) == ADC_CLKSEL_WORKCLKDIV1) || \
                                                  ( (CLK) == ADC_CLKSEL_WORKCLKDIV2) || \
                                                  ( (CLK) == ADC_CLKSEL_FORCE0) || \
                                                  ( (CLK) == ADC_CLKSEL_FORCE1) || \
                                                  ( (CLK) == ADC_CLKSEL_BUSCLKDIV2) || \
                                                  ( (CLK) == ADC_CLKSEL_BUSCLKDIV4) || \
                                                  ( (CLK) == ADC_CLKSEL_BUSCLKDIV6) || \
                                                  ( (CLK) == ADC_CLKSEL_BUSCLKDIV8) )
#define IS_ADC_CHANNEL( CH )                    ( ( (CH) == ADC_CHANNEL_0) || \
                                                  ( (CH) == ADC_CHANNEL_1) || \
                                                  ( (CH) == ADC_CHANNEL_2) || \
                                                  ( (CH) == ADC_CHANNEL_3) )
#define IS_ADC_WORKMODE( MODE )                 ( ( (MODE) == ADC_WORKMODE_FREETRIG) || \
                                                  ( (MODE) == ADC_WORKMODE_POLLING) )
#define IS_ADC_CONVMODE( MODE )                 ( ( (MODE) == ADC_CONVMODE_CONTINUE) || \
                                                  ( (MODE) == ADC_CONVMODE_ONESHOT) )
#define IS_ADC_DIFF_EN( EN )                    ( ( (EN) == ADC_CHxDIFF_ENABLE) || \
                                                  ( (EN) == ADC_CHxDIFF_DISABLE) )
#define IS_ADC_TRIGINV_EN( EN )                 ( ( (EN) == ADC_CHxTRIGINV_ENABLE) || \
                                                  ( (EN) == ADC_CHxTRIGINV_DISABLE) )
#define IS_ADC_TRIG_EDGE( EDGE )                ( ( (EDGE) == ADC_EXTTRIGCONVEDGE_NONE) || \
                                                  ( (EDGE) == ADC_EXTTRIGCONVEDGE_RISING) || \
                                                  ( (EDGE) == ADC_EXTTRIGCONVEDGE_FALLING) || \
                                                  ( (EDGE) == ADC_EXTTRIGCONVEDGE_BOTHEDGE) )
#define IS_ADC_TRIG( TRIG )                     ( ( (TRIG) == ADC_TRIGSRC_TRGO_IN) || \
                                                  ( (TRIG) == ADC_TRIGSRC_CMP) || \
                                                  ( (TRIG) == ADC_TRIGSRC_STIMER0) || \
                                                  ( (TRIG) == ADC_TRIGSRC_STIMER1) || \
                                                  ( (TRIG) == ADC_TRIGSRC_STIMER2) || \
                                                  ( (TRIG) == ADC_TRIGSRC_STIMER3) || \
                                                  ( (TRIG) == ADC_TRIGSRC_STIMER4) || \
                                                  ( (TRIG) == ADC_TRIGSRC_STIMER5) || \
                                                  ( (TRIG) == ADC_TRIGSRC_LTIMER) || \
                                                  ( (TRIG) == ADC_TRIGSRC_STIMERTGS_CH6) || \
                                                  ( (TRIG) == ADC_TRIGSRC_STIMERTGS_CH7) || \
                                                  ( (TRIG) == ADC_TRIGSRC_RTC_ALARM) || \
                                                  ( (TRIG) == ADC_TRIGSRC_TAMPER_ALARM) || \
                                                  ( (TRIG) == ADC_TRIGSRC_RTC_1S) || \
                                                  ( (TRIG) == ADC_TRIGSRC_SOFTWARE) || \
                                                  ( (TRIG) == ADC_TRIGSRC_RFU) )
#define IS_ADC_SLEEPWAIT_CYCLE( THRESHOLD )     ( (THRESHOLD) <= 0xFFFU)
#define IS_ADC_SAMPLE_CYCLE( THRESHOLD )        ( (THRESHOLD) <= 0x1000U)
#define IS_ADC_POLLINGNUM( THRESHOLD )          ( (THRESHOLD) < 0x10U)
#define IS_ADC_READY_CYCLE( THRESHOLD )         ( (THRESHOLD) <= 0x7FFU)
#define IS_ADC_CONVERT_CYCLE( THRESHOLD )       ( (THRESHOLD) <= 0x10U)
#define IS_ADC_SAMPLEHOLD_CYCLE( THRESHOLD )    ( (THRESHOLD) <= 0x10U)
#define IS_ADC_INPUT( IN )                      ( ( (IN) == ADC_INSRC_DP0) || \
                                                  ( (IN) == ADC_INSRC_DP1) || \
                                                  ( (IN) == ADC_INSRC_DP2) || \
                                                  ( (IN) == ADC_INSRC_ADC3) || \
                                                  ( (IN) == ADC_INSRC_ADC4) || \
                                                  ( (IN) == ADC_INSRC_ADC5) || \
                                                  ( (IN) == ADC_INSRC_ADC6) || \
                                                  ( (IN) == ADC_INSRC_ADC7) || \
                                                  ( (IN) == ADC_INSRC_ADC8) || \
                                                  ( (IN) == ADC_INSRC_ADC9) || \
                                                  ( (IN) == ADC_INSRC_ADC10) || \
                                                  ( (IN) == ADC_INSRC_ADC11) || \
                                                  ( (IN) == ADC_INSRC_ADC12) || \
                                                  ( (IN) == ADC_INSRC_ADC13) || \
                                                  ( (IN) == ADC_INSRC_ADC14) || \
                                                  ( (IN) == ADC_INSRC_VINCHARGE) || \
                                                  ( (IN) == ADC_INSRC_VOUTCHARGE) || \
                                                  ( (IN) == ADC_INSRC_12BITDAC) || \
                                                  ( (IN) == ADC_INSRC_TEMPERATURE) || \
                                                  ( (IN) == ADC_INSRC_VREF12) || \
                                                  ( (IN) == ADC_INSRC_VDDBKP) || \
                                                  ( (IN) == ADC_INSRC_6BITDAC) )
#define IS_ADC_TRGDLY_CYCLE( THRESHOLD )        ( (THRESHOLD) <= 0xFFFFU)
#define IS_ADC_OFFSET_EN( EN )                  ( ( (EN) == ADC_CHxOFFSET_ENABLE) || \
                                                  ( (EN) == ADC_CHxOFFSET_DISABLE) )
#define IS_ADC_AVG_EN( EN )                     ( ( (EN) == ADC_CHxAVERAGE_ENABLE) || \
                                                  ( (EN) == ADC_CHxAVERAGE_DISABLE) )
#define IS_ADC_AVG_NUM( NUM )                   ( ( (NUM) == ADC_AVGNUM_2) || \
                                                  ( (NUM) == ADC_AVGNUM_4) || \
                                                  ( (NUM) == ADC_AVGNUM_6) || \
                                                  ( (NUM) == ADC_AVGNUM_8) )
#define IS_ADC_CMP_EN( EN )                     ( ( (EN) == ADC_CHxCOMPARE_ENABLE) || \
                                                  ( (EN) == ADC_CHxCOMPARE_DISABLE) )
#define IS_ADC_CMP_MODE( MODE )                 ( ( (MODE) == ADC_CMP_LARGER_CV2) || \
                                                  ( (MODE) == ADC_CMP_LESS_CV1) || \
                                                  ( (MODE) == ADC_CMP_BETWEEN_CV1CV2) || \
                                                  ( (MODE) == ADC_CMP_OUTSIDE_CV1CV2) )


/**
 * @}
 */

/* Private functions ---------------------------------------------------------*/


/** @defgroup ADC_Private_Functions ADC Private Functions
 * @{
 */


/**
 * @brief  Set the ADC's trigger delay time.
 * @param  delay time parameter.
 * @retval None
 */
#define ADC_CHANNEL_TRGDLY( _TIME_ ) ( (_TIME_) << (ADC_CHxCFG_TRGDLY_Pos) )


/**
 * @brief  Set the ADC's sleeep time.
 * @param  Sleep time parameter.
 * @retval None
 */
#define ADC_SLEEP_TIME( _TIME_ ) ( (_TIME_) << (ADC_CFG_TSLEEP_Pos) )


/**
 * @brief  Set the ADC's sample time.
 * @param  Sample time parameter.
 * @retval None
 */
#define ADC_SAMPLE_TIME( _TIME_ ) ( (_TIME_) << (ADC_TIME_SAMPLE_Pos) )


/**
 * @brief  Set the ADC's ready time.
 * @param  ready time parameter.
 * @retval None
 */
#define ADC_READY_TIME( _TIME_ ) ( (_TIME_) << (ADC_TIME_READY_Pos) )


/**
 * @brief  Set the ADC's convert time.
 * @param  convert time parameter.
 * @retval None
 */
#define ADC_CONVERT_TIME( _TIME_ ) ( (_TIME_) << (ADC_TIME_CONVERT_Pos) )


/**
 * @brief  Set the ADC's sample hold time.
 * @param  Sample hold time parameter.
 * @retval None
 */
#define ADC_SAMPLEHOLD_TIME( _TIME_ ) ( (uint32_t) (_TIME_) << (ADC_TIME_SAMPLHOLD_Pos) )


/**
 * @brief  Set the ADC's polling number.
 * @param  polling number parameter.
 * @retval None
 */
#define ADC_POLLING_NUM( NUM ) ( (NUM) << (ADC_WORK_POLLINGNUM_Pos) )


/**
 * @}
 */


/**
 * @}
 */


/**
 * @}
 */

#ifdef __cplusplus
}
#endif

#endif /*__FM15F3xx_ADC_H */


/************************ (C) COPYRIGHT FudanMicroelectronics *****END OF FILE****/
