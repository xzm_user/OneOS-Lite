/**
 ******************************************************************************
 * @file    fm15f3xx_hal_cmp.h
 * @author  WYL
 * @brief   Header file of CMP HAL module.
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2020 FudanMicroelectronics.
 * All rights reserved.</center></h2>
 *
 ******************************************************************************
 */


/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __FM15F3xx_HAL_CMP_H
#define __FM15F3xx_HAL_CMP_H

#ifdef __cplusplus
extern "C" {
#endif


/* Includes ------------------------------------------------------------------*/
#include "fm15f3xx_hal_def.h"
#include "fm15f3xx_ll_cmp.h"


/** @addtogroup FM15F3xx_HAL_Driver
 * @{
 */


/** @addtogroup CMP
 * @{
 */

/* Exported types ------------------------------------------------------------*/


/** @defgroup CMP_Exported_Types CMP Exported Types
 * @{
 */


/**
 * @brief HAL State structures definition
 */
typedef enum {
    HAL_CMP_STATE_RESET     = 0x00U,    /*!< CMP not yet initialized or disabled  */
    HAL_CMP_STATE_READY     = 0x01U,    /*!< CMP initialized and ready for use    */
    HAL_CMP_STATE_BUSY      = 0x02U,    /*!< CMP internal processing is ongoing   */
    HAL_CMP_STATE_TIMEOUT   = 0x03U,    /*!< CMP timeout state                    */
    HAL_CMP_STATE_ERROR     = 0x04U     /*!< CMP error state                      */
} HAL_CMP_StateTypeDef;


/**
 * @brief HAL CMP InitTypeDef structures definition
 */
typedef struct {
    uint32_t SpeedMode;                 /*!< Specifies the speed mode. */

    uint32_t HysterMode;                /*!< Specifies the Hyster Level. */

    uint32_t ReadyTime;                 /*!< Specifies the Ready time cycle before CMP EN. */

    uint32_t InputP;                    /*!< Specifies the Positive Input. */

    uint32_t InputN;                    /*!< Specifies the Negative Input. */

    uint32_t WorkMode;                  /*!< Specifies the Work mode. */

    uint32_t TrigSource;                /*!< Specifies the Tigger Source. */

    uint32_t TrigInvEn;                 /*!< Specifies the Tigger Source normal or inverse.*/

    uint32_t TrigEdge;                  /*!< Specifies the Tigger edge. */

    uint32_t WindowsEn;                 /*!< Specifies the Window Source. */

    uint32_t Filter;                    /*!< Specifies the filter or not.*/

    uint32_t OutputInvEn;               /*!< Specifies the Output normal or inverse. */
} CMP_InitTypeDef;


/**
 * @brief HAL 6Bit DAC InitTypeDef structures definition
 */
typedef struct {
    uint32_t VrefSel;                   /*!< Specifies the SixBit DAC vref select. */

    uint32_t Value;                     /*!< Specifies the SixBit DAC output value. */
} CMP_6BitDACInitTypeDef;


/**
 * @brief CMP handle Structure definition
 */
typedef struct {
    CMP_TypeDef *Instance;              /*!< Register base address             */

    CMP_InitTypeDef Init;               /*!< CMP Init structures               */

    __IO HAL_CMP_StateTypeDef State;    /*!< CMP communication state           */

//  DMA_HandleTypeDef           *DMA_Handle;  /*!< Pointer DMA handler              */

    __IO uint32_t ErrorCode;            /*!< CMP Error code                    */
} CMP_HandleTypeDef;

/* Exported constants --------------------------------------------------------*/


/** @defgroup CMP_Exported_Constants CMP Exported Constants
 * @{
 */


/** @defgroup CMP_Error_Code CMP Error Code
 * @{
 */
#define  HAL_CMP_ERROR_NONE 0x00U                                               /*!< No error                          */
#define  HAL_CMP_ERROR_DMA  0x04U                                               /*!< DMA error                         */


/**
 * @}
 */


/** @defgroup CMP Power Mode
 * @{
 */
#define CMP_SPEED_LOW   LL_CMP_SPEED_LOW                                        /* normal mode   */
#define CMP_SPEED_HIGH  LL_CMP_SPEED_HIGH                                       /* high power mode  */


/**
 * @}
 */


/** @defgroup config hyster level
 * @{
 */
#define CMP_HYSTER_5mV  LL_CMP_HYSTER_LEVEL0                                    /*!< config hyster level0 */
#define CMP_HYSTER_10mV LL_CMP_HYSTER_LEVEL1                                    /*!< config hyster level1 */
#define CMP_HYSTER_20mV LL_CMP_HYSTER_LEVEL2                                    /*!< config hyster level2 */
#define CMP_HYSTER_30mV LL_CMP_HYSTER_LEVEL3                                    /*!< config hyster level3 */


/**
 * @}
 */


/** @defgroup  select input source
 * @{
 */
#define CMP_INPUT_CMP_IN0   (0x00000000U)                                       /*!< config as CMP_IN0 */
#define CMP_INPUT_CMP_IN1   (0x00000001U)                                       /*!< config as CMP_IN1 */
#define CMP_INPUT_CMP_IN2   (0x00000002U)                                       /*!< config as CMP_IN2 */
#define CMP_INPUT_CMP_IN3   (0x00000003U)                                       /*!< config as CMP_IN3 */
#define CMP_INPUT_12BITDAC  (0x00000004U)                                       /*!< config as CMP_IN4/12 bit DAC */
#define CMP_INPUT_VREF12    (0x00000006U)                                       /*!< config as bandgap */
#define CMP_INPUT_6BITDAC   (0x00000007U)                                       /*!< config as 6 bit DAC */


/**
 * @}
 */


/** @defgroup 6bit DAC VREF refrence select
 * @{
 */
#define CMP_6BITDACVREF_1P2V    LL_CMP_6BITDAC_VREF1P2                          /* DAC Vrefrence select Vref12 */
#define CMP_6BITDACVREF_VDDA    LL_CMP_6BITDAC_VREFVDD                          /* DAC Vrefrence select VDDA */


/**
 * @}
 */


/** @defgroup TRIG INPUT  config normal or inverse
 * @{
 */
#define CMP_TRIGINPUT_NORMAL    LL_CMP_TRIGINPUT_NORMAL                         /*!< config trigsource as normal   */
#define CMP_TRIGINPUT_INVERT    LL_CMP_TRIGINPUT_INVERT                         /*!< config trigsource as inverse  */


/**
 * @}
 */


/** @defgroup Work MODE  select
 * @{
 */
#define CMP_WORKMODE_REALTIME   LL_CMP_TRIGINPUT_NORMAL                         /*!< config trigsource as normal   */
#define CMP_WORKMODE_TRIG       LL_CMP_TRIGINPUT_INVERT                         /*!< config trigsource as inverse  */


/**
 * @}
 */


/** @defgroup config trigger edge
 * @{
 */
#define CMP_TRIGEDGE_HIGH       LL_CMP_TRIGEDGE_HIGH                            /*!< config trigedge as high level */
#define CMP_TRIGEDGE_RISING     LL_CMP_TRIGEDGE_RISING                          /*!< config trigedge as rising     */
#define CMP_TRIGEDGE_FALLING    LL_CMP_TRIGEDGE_FALLING                         /*!< config trigedge as low level  */
#define CMP_TRIGEDGE_BOTHEDGE   LL_CMP_TRIGEDGE_BOTHEDGE                        /*!< config trigedge as falling or rising */


/**
 * @}
 */


/** @defgroup  select different source
 * @{
 */
#define CMP_TRIGSRC_CMP_INx         LL_CMP_TRIGSRC_CMP_INx                      /*!< config trig source as CMP_INx        */
#define CMP_TRIGSRC_CMP_RCD         LL_CMP_TRIGSRC_CMP_RCD                      /*!< config trig source as CMP_RCD        */
#define CMP_TRIGSRC_STIMER0         LL_CMP_TRIGSRC_STIMER0                      /*!< config trig source as STIMER0        */
#define CMP_TRIGSRC_STIMER1         LL_CMP_TRIGSRC_STIMER1                      /*!< config trig source as STIMER1        */
#define CMP_TRIGSRC_STIMER2         LL_CMP_TRIGSRC_STIMER2                      /*!< config trig source as STIMER2        */
#define CMP_TRIGSRC_STIMER3         LL_CMP_TRIGSRC_STIMER3                      /*!< config trig source as STIMER3        */
#define CMP_TRIGSRC_STIMER4         LL_CMP_TRIGSRC_STIMER4                      /*!< config trig source as STIMER4        */
#define CMP_TRIGSRC_STIMER5         LL_CMP_TRIGSRC_STIMER5                      /*!< config trig source as STIMER5        */
#define CMP_TRIGSRC_LPTIMER0        LL_CMP_TRIGSRC_LPTIMER0                     /*!< config trig source as LPTIMER0       */
#define CMP_TRIGSRC_RTCALARM_IT     LL_CMP_TRIGSRC_RTCALARM_IT                  /*!< config trig source as RTCALARM_IT    */
#define CMP_TRIGSRC_TAMPERALARM_IT  LL_CMP_TRIGSRC_TAMPERALARM_IT               /*!< config trig source as TAMPERALARM_IT */
#define CMP_TRIGSRC_RTC1S_IT        LL_CMP_TRIGSRC_RTC1S_IT                     /*!< config trig source as RTC1S_IT       */
#define CMP_TRIGSRC_SOFTWARE        LL_CMP_TRIGSRC_SOFTWARE                     /*!< config trig source as SOFTWARE       */


/**
 * @}
 */


/** @defgroup set filter cycle
 * @{
 */
#define CMP_FILTER_DISABLE  LL_CMP_FILTER_DISABLE                               /* not filt  */
#define CMP_FILTER_1CYCLE   (LL_CMP_FILTERCNT_1CLK | LL_CMP_FILTER_ENABLE)      /* 1 cycle filter  */
#define CMP_FILTER_2CYCLE   (LL_CMP_FILTERCNT_2CLK | LL_CMP_FILTER_ENABLE)      /* 2 cycle filter  */
#define CMP_FILTER_3CYCLE   (LL_CMP_FILTERCNT_3CLK | LL_CMP_FILTER_ENABLE)      /* 3 cycle filter  */
#define CMP_FILTER_4CYCLE   (LL_CMP_FILTERCNT_4CLK | LL_CMP_FILTER_ENABLE)      /* 4 cycle filter  */
#define CMP_FILTER_5CYCLE   (LL_CMP_FILTERCNT_5CLK | LL_CMP_FILTER_ENABLE)      /* 5 cycle filter  */
#define CMP_FILTER_6CYCLE   (LL_CMP_FILTERCNT_6CLK | LL_CMP_FILTER_ENABLE)      /* 6 cycle filter  */
#define CMP_FILTER_7CYCLE   (LL_CMP_FILTERCNT_7CLK | LL_CMP_FILTER_ENABLE)      /* 7 cycle filter  */
#define CMP_FILTER_8CYCLE   (LL_CMP_FILTERCNT_8CLK | LL_CMP_FILTER_ENABLE)      /* 8 cycle filter  */
#define CMP_FILTER_9CYCLE   (LL_CMP_FILTERCNT_9CLK | LL_CMP_FILTER_ENABLE)      /* 9 cycle filter  */
#define CMP_FILTER_10CYCLE  (LL_CMP_FILTERCNT_10CLK | LL_CMP_FILTER_ENABLE)     /* 10 cycle filter  */
#define CMP_FILTER_11CYCLE  (LL_CMP_FILTERCNT_11CLK | LL_CMP_FILTER_ENABLE)     /* 11 cycle filter  */
#define CMP_FILTER_12CYCLE  (LL_CMP_FILTERCNT_12CLK | LL_CMP_FILTER_ENABLE)     /* 12 cycle filter  */
#define CMP_FILTER_13CYCLE  (LL_CMP_FILTERCNT_13CLK | LL_CMP_FILTER_ENABLE)     /* 13 cycle filter  */
#define CMP_FILTER_14CYCLE  (LL_CMP_FILTERCNT_14CLK | LL_CMP_FILTER_ENABLE)     /* 14 cycle filter  */
#define CMP_FILTER_15CYCLE  (LL_CMP_FILTERCNT_15CLK | LL_CMP_FILTER_ENABLE)     /* 15 cycle filter  */
#define CMP_FILTER_16CYCLE  (LL_CMP_FILTERCNT_16CLK | LL_CMP_FILTER_ENABLE)     /* 16 cycle filter  */


/**
 * @}
 */


/** @defgroup CMP_LL_WINSOURCE  config disable or enable
 * @{
 */
#define CMP_WINDOW_DISABLE  LL_CMP_WINDOW_DISABLE                               /*!< CMP window disable  */
#define CMP_WINDOW_STIMER0  (LL_CMP_WINSOURCE_STIMER0 | LL_CMP_WINDOW_ENABLE)   /*!< config window sourse as STIMER0  */
#define CMP_WINDOW_STIMER1  (LL_CMP_WINSOURCE_STIMER1 | LL_CMP_WINDOW_ENABLE)   /*!< config window sourse as STIMER1  */
#define CMP_WINDOW_STIMER2  (LL_CMP_WINSOURCE_STIMER2 | LL_CMP_WINDOW_ENABLE)   /*!< config window sourse as STIMER2  */
#define CMP_WINDOW_STIMER4  (LL_CMP_WINSOURCE_STIMER4 | LL_CMP_WINDOW_ENABLE)   /*!< config window sourse as STIMER4 */


/**
 * @}
 */


/** @defgroup OUTPUT config normal or inverse
 * @{
 */
#define CMP_OUTPUT_NORMAL   LL_CMP_OUTCTRL_NORMAL                               /*!< config output as normal   */
#define CMP_OUTPUT_INVERT   LL_CMP_OUTCTRL_INVERT                               /*!< config output as inverse  */


/**
 * @}
 */


/** @defgroup  selet different mode to config CMP result to produce record and IT
 * @{
 */
#define CMP_ITMODE_HIGH     LL_CMP_RCDMODE_HIGH                                 /*!< CMP result is high to produce record      */
#define CMP_ITMODE_RISING   LL_CMP_RCDMODE_RISING                               /*!< CMP result is rising to produce record    */
#define CMP_ITMODE_FALLING  LL_CMP_RCDMODE_FALLING                              /*!< CMP result is falling to produce record   */
#define CMP_ITMODE_BOTHEDGE LL_CMP_RCDMODE_BOTHEDGE                             /*!< CMP result is both edge to produce record */


/**
 * @}
 */


/** @defgroup Result mode
 * @{
 */
#define CMP_RESULT_DIGITAL  (0x0U)
#define CMP_RESULT_ANALOG   (0x1U)


/**
 * @}
 */


/** @defgroup record flag
 * @{
 */
#define CMP_FLAG_RCD LL_CMP_RCDCLEAR_PULSE


/**
 * @}
 */


/**
 * @}
 */

/* Exported macro ------------------------------------------------------------*/


/** @defgroup CMP_Exported_Macros CMP Exported Macros
 * @{
 */


/** @brief Reset CMP handle state
 * @param  __HANDLE__ specifies the CMP handle.
 * @retval None
 */
#define __HAL_CMP_RESET_HANDLE_STATE( __HANDLE__ ) ( (__HANDLE__)->State = HAL_CMP_STATE_RESET)


/** @brief Enable the CMP
 * @param  __HANDLE__ specifies the CMP handle.
 * @retval None
 */
#define __HAL_CMP_ENABLE( __HANDLE__ ) ( (__HANDLE__)->Instance->CTRL |= CMP_CTRL_CMPEN)


/** @brief Disable the CMP
 * @param  __HANDLE__ specifies the CMP
 * @retval None
 */
#define __HAL_CMP_DISABLE( __HANDLE__ ) ( (__HANDLE__)->Instance->CTRL &= ~CMP_CTRL_CMPEN)


/** @brief Is Enable the CMP
 * @param  __HANDLE__ specifies the CMP handle.
 * @retval None
 */
#define __HAL_CMP_IS_ENABLE( __HANDLE__ ) ( ( (__HANDLE__)->Instance->CTRL & CMP_CTRL_CMPEN) == (CMP_CTRL_CMPEN) )


/** @brief Software trigger the CMP
 * @param  __HANDLE__ specifies the CMP handle
 * @retval None
 */
#define __HAL_CMP_SOFTWARETRIG( __HANDLE__ ) SET_BIT( (__HANDLE__)->Instance->SWTRIG, CMP_SWTRIG_CTRL )


/** @brief Is Software trigger
 * @param  __HANDLE__ specifies the CMP handle
 * @retval None
 */
#define __HAL_CMP_IS_SOFTWARETRIG( __HANDLE__ ) ( ( (__HANDLE__)->Instance->TRIG & CMP_TRIGSRC_SOFTWARE) == (CMP_TRIGSRC_SOFTWARE) )


/** @brief Enable the CMP interrupt
 * @param  __HANDLE__ specifies the CMP handle
 * @param  __ITMODE__ specifies the CMP interrupt mode.
 * @retval None
 */
#define __HAL_CMP_ENABLE_IT( __HANDLE__, __ITMODE__ ) ( (__HANDLE__)->Instance->INTEN) = (CMP_INTEN_INTDMAEN | __ITMODE__)


/** @brief Disable the CMP interrupt
 * @param  __HANDLE__ specifies the CMP handle
 * @retval None
 */
#define __HAL_CMP_DISABLE_IT( __HANDLE__ ) ( ( (__HANDLE__)->Instance->INTEN) &= ~(CMP_INTEN_INTDMAEN) )


/** @brief  Clear the CMP's record flag.
 * @param  __HANDLE__ specifies the CMP handle.
 * @param  __FLAG__ specifies the flag to clear.
 * @retval None
 */
#define __HAL_CMP_CLEAR_RCDFLAG( __HANDLE__, __FLAG__ ) ( ( (__HANDLE__)->Instance->RCDCL) |= (__FLAG__) )


/**
 * @brief  ENABLE VREF 1.2V
 * @retval None
 */
#define __HAL_CMP_ENABLE_VREF12() MODIFY_REG( VREF->CTRL, VREF_CTRL_VREFEN_Msk | VREF_CTRL_LDOEN_Msk, VREF_CTRL_VREFEN | VREF_CTRL_LDOEN )


/**
 * @brief  DISABLE VREF 1.2V
 * @retval None
 */
#define __HAL_CMP_DISABLE_VREF12() CLEAR_BIT( VREF->CTRL, VREF_CTRL_VREFEN | VREF_CTRL_LDOEN )


/**
 * @}
 */

/* Exported functions --------------------------------------------------------*/


/** @addtogroup CMP_Exported_Functions
 * @{
 */


/** @addtogroup CMP_Exported_Functions_Group1
 * @{
 */
/* Initialization/de-initialization functions *********************************/
HAL_StatusTypeDef HAL_CMP_Init(CMP_HandleTypeDef* hcmp);


HAL_StatusTypeDef HAL_CMP_DeInit(CMP_HandleTypeDef* hcmp);


void HAL_CMP_MspInit(CMP_HandleTypeDef* hcmp);


void HAL_CMP_MspDeInit(CMP_HandleTypeDef* hcmp);


/**
 * @}
 */


/** @addtogroup CMP_Exported_Functions_Group2
 * @{
 */
/* I/O operation functions ****************************************************/
HAL_StatusTypeDef HAL_CMP_Start(CMP_HandleTypeDef* hcmp);


HAL_StatusTypeDef HAL_CMP_Stop(CMP_HandleTypeDef* hcmp);


HAL_StatusTypeDef HAL_CMP_Start_IT(CMP_HandleTypeDef *hcmp, uint32_t ITmode);


HAL_StatusTypeDef HAL_CMP_Stop_IT(CMP_HandleTypeDef *hcmp);


/**
 * @}
 */


/** @addtogroup CMP_Exported_Functions_Group3
 * @{
 */
/* Peripheral Control functions ***********************************************/
uint8_t HAL_CMP_GetResult(CMP_HandleTypeDef *hcmp, uint8_t ResultMode);


void HAL_CMP_6BitDacInit(CMP_6BitDACInitTypeDef* DAC_InitStruct);


void HAL_CMP_6BitDacDeInit(void);


/**
 * @}
 */


/** @addtogroup CMP_Exported_Functions_Group4
 * @{
 */
/* Peripheral State functions *************************************************/
HAL_CMP_StateTypeDef HAL_CMP_GetState(CMP_HandleTypeDef *hcmp);


void HAL_CMP_IRQHandler(CMP_HandleTypeDef *hcmp);


uint32_t HAL_CMP_GetError(CMP_HandleTypeDef *hcmp);


void HAL_CMP_CpltCallback(CMP_HandleTypeDef *hcmp);


/**
 * @}
 */


/**
 * @}
 */
/* Private types -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private constants ---------------------------------------------------------*/


/** @defgroup CMP_Private_Constants CMP Private Constants
 * @{
 */


/**
 * @}
 */

/* Private macros ------------------------------------------------------------*/


/** @defgroup CMP_Private_Macros CMP Private Macros
 * @{
 */

#define IS_CMP_INSTANCE( INSTANCE )     ( (INSTANCE) == CMP0)
#define IS_CMP_SPEEDMODE( MODE )        ( ( (MODE) == CMP_SPEED_LOW) || \
                                          ( (MODE) == CMP_SPEED_HIGH) )
#define IS_CMP_HYSTERMODE( MODE )       ( ( (MODE) == CMP_HYSTER_5mV) || \
                                          ( (MODE) == CMP_HYSTER_10mV) || \
                                          ( (MODE) == CMP_HYSTER_20mV) || \
                                          ( (MODE) == CMP_HYSTER_30mV) )
#define IS_CMP_READYTIME( CYCLE )       ( (CYCLE) <= 0xFFFU)
#define IS_CMP_INPUT( INPUT )           ( ( (INPUT) == CMP_INPUT_CMP_IN0) || \
                                          ( (INPUT) == CMP_INPUT_CMP_IN1) || \
                                          ( (INPUT) == CMP_INPUT_CMP_IN2) || \
                                          ( (INPUT) == CMP_INPUT_CMP_IN3) || \
                                          ( (INPUT) == CMP_INPUT_12BITDAC) || \
                                          ( (INPUT) == CMP_INPUT_VREF12) || \
                                          ( (INPUT) == CMP_INPUT_6BITDAC) )
#define IS_CMP_6BITDAC( VREF )          ( ( (VREF) == CMP_6BITDACVREF_1P2V) || \
                                          ( (VREF) == CMP_6BITDACVREF_VDDA) )
#define IS_CMP_6BITDACVALUE( VALUE )    ( (VALUE) <= 0x3FU)
#define IS_CMP_WORKMODE( MODE )         ( ( (MODE) == CMP_WORKMODE_REALTIME) || \
                                          ( (MODE) == CMP_WORKMODE_TRIG) )
#define IS_CMP_TRIGSOURCE( TRIGGER )    ( ( (TRIGGER) == CMP_TRIGSRC_CMP_INx) || \
                                          ( (TRIGGER) == CMP_TRIGSRC_CMP_RCD) || \
                                          ( (TRIGGER) == CMP_TRIGSRC_STIMER0) || \
                                          ( (TRIGGER) == CMP_TRIGSRC_STIMER1) || \
                                          ( (TRIGGER) == CMP_TRIGSRC_STIMER2) || \
                                          ( (TRIGGER) == CMP_TRIGSRC_STIMER3) || \
                                          ( (TRIGGER) == CMP_TRIGSRC_STIMER4) || \
                                          ( (TRIGGER) == CMP_TRIGSRC_STIMER5) || \
                                          ( (TRIGGER) == CMP_TRIGSRC_LPTIMER0) || \
                                          ( (TRIGGER) == CMP_TRIGSRC_RTCALARM_IT) || \
                                          ( (TRIGGER) == CMP_TRIGSRC_TAMPERALARM_IT) || \
                                          ( (TRIGGER) == CMP_TRIGSRC_RTC1S_IT) || \
                                          ( (TRIGGER) == CMP_TRIGSRC_SOFTWARE) )
#define IS_CMP_TRIGINV( EN )            ( ( (EN) == CMP_TRIGINPUT_NORMAL) || \
                                          ( (EN) == CMP_TRIGINPUT_INVERT) )
#define IS_CMP_TRIGEDGE( MODE )         ( ( (MODE) == CMP_TRIGEDGE_HIGH) || \
                                          ( (MODE) == CMP_TRIGEDGE_RISING) || \
                                          ( (MODE) == CMP_TRIGEDGE_FALLING) || \
                                          ( (MODE) == CMP_TRIGEDGE_BOTHEDGE) )
#define IS_CMP_WINDOWS( SRC )           ( ( (SRC) == CMP_WINDOW_DISABLE) || \
                                          ( (SRC) == CMP_WINDOW_STIMER0) || \
                                          ( (SRC) == CMP_WINDOW_STIMER1) || \
                                          ( (SRC) == CMP_WINDOW_STIMER2) || \
                                          ( (SRC) == CMP_WINDOW_STIMER4) )
#define IS_CMP_FILTER( CYCLE )          ( ( (CYCLE) == CMP_FILTER_DISABLE) || \
                                          ( (CYCLE) == CMP_FILTER_1CYCLE) || \
                                          ( (CYCLE) == CMP_FILTER_2CYCLE) || \
                                          ( (CYCLE) == CMP_FILTER_3CYCLE) || \
                                          ( (CYCLE) == CMP_FILTER_4CYCLE) || \
                                          ( (CYCLE) == CMP_FILTER_5CYCLE) || \
                                          ( (CYCLE) == CMP_FILTER_6CYCLE) || \
                                          ( (CYCLE) == CMP_FILTER_7CYCLE) || \
                                          ( (CYCLE) == CMP_FILTER_8CYCLE) || \
                                          ( (CYCLE) == CMP_FILTER_9CYCLE) || \
                                          ( (CYCLE) == CMP_FILTER_10CYCLE) || \
                                          ( (CYCLE) == CMP_FILTER_11CYCLE) || \
                                          ( (CYCLE) == CMP_FILTER_12CYCLE) || \
                                          ( (CYCLE) == CMP_FILTER_13CYCLE) || \
                                          ( (CYCLE) == CMP_FILTER_14CYCLE) || \
                                          ( (CYCLE) == CMP_FILTER_15CYCLE) || \
                                          ( (CYCLE) == CMP_FILTER_16CYCLE) )
#define IS_CMP_OUTPUTINV( INV )         ( ( (INV) == CMP_OUTPUT_NORMAL) || \
                                          ( (INV) == CMP_OUTPUT_INVERT) )


/**
 * @}
 */

/* Private functions ---------------------------------------------------------*/


/** @defgroup CMP_Private_Functions CMP Private Functions
 * @{
 */


/**
 * @}
 */


/**
 * @}
 */


/**
 * @}
 */

#ifdef __cplusplus
}
#endif

#endif /*__FM15F3xx_HAL_CMP_H */

/************************ (C) COPYRIGHT FudanMicroelectronics *****END OF FILE****/
