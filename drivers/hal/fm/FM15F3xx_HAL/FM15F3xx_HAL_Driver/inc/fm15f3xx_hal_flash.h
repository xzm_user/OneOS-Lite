/**
 ******************************************************************************
 * @file    fm15f3xx_hal_flash.h
 * @author  weifan
 * @brief   Header file of FLASH HAL module.
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2020 FudanMicroelectronics.
 * All rights reserved.</center></h2>
 *
 ******************************************************************************
 */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __FM15F3xx_HAL_FLASH_H
#define __FM15F3xx_HAL_FLASH_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "fm15f3xx_hal_def.h"
#include "fm15f3xx_ll_flash.h"


/** @addtogroup FM15F3xx_HAL_Driver
 * @{
 */


/** @addtogroup FLASH
 * @{
 */

/* Exported types ------------------------------------------------------------*/


/** @defgroup FLASH_Exported_Types FLASH Exported Types
 * @{
 */


/**
 * @brief  FLASH Program type definition
 */
typedef enum {
    FLASH_TYPEPROGRAM_WORD = 0U,    /* Program 1 word at a specified address */
    FLASH_TYPEPROGRAM_4WORD,        /* Program 4 word at a specified address */
    FLASH_TYPEPROGRAM_32WORD,       /* Program 32 word at a specified address */
    FLASH_TYPEPROGRAM_SECTOR        /* Program 1 sector at a specified address */
} FLASH_ProgramTypeDef;


/**
 * @brief  FLASH Erase structure definition
 */
typedef struct {
    uint32_t InitSector;            /*!< Initial FLASH sector to erase
                                         This parameter must be less than @ref FLASH_SECTOR_TOTAL */

    uint32_t NumberSectors;         /*!< Number of sectors to be erased.
                                         This parameter must be a value between 1 and (@ref FLASH_SECTOR_TOTAL - value of Initial sector)*/
} FLASH_EraseParamTypeDef;


/**
 * @}
 */

/* Exported functions --------------------------------------------------------*/


/** @addtogroup FLASH_Exported_Functions
 * @{
 */


/** @addtogroup FLASH_Exported_Functions_Group1
 * @{
 */
/* Program operation functions  ***********************************************/
FLASH_Status HAL_FLASH_Modify(uint32_t Address, uint8_t *pData, uint32_t Len, uint32_t *SectorError);


FLASH_Status HAL_FLASH_Program(FLASH_ProgramTypeDef TypeProgram, uint32_t Address, uint32_t *pData);


FLASH_Status HAL_FLASH_Erase(FLASH_EraseParamTypeDef *pEraseInit, uint32_t *SectorError);


/**
 * @}
 */


/**
 * @}
 */

/* Private macros ------------------------------------------------------------*/


/** @defgroup FLASH_Private_Macros FLASH Private Macros
 * @{
 */
#define FLASH_MAX( a, b )   ( (a) > (b) ? (a) : (b) )
#define FLASH_MIN( a, b )   ( (a) < (b) ? (a) : (b) )

#define FLASH_WORD_SIZE( n ) (4 * (n) )

#define FLASH_SECTOR_TOTAL      320
#define FLASH_SECTOR_SIZE       (1 << 11)       // 2KB per sector
#define FLASH_32WORDS_IN_SECTOR (FLASH_SECTOR_SIZE >> 7)
#define FLASH_SECTORS_IN_BLOCK  16              // 16 sectors per block
#define FLASH_BLOCK_SIZE        (FLASH_SECTORS_IN_BLOCK * FLASH_SECTOR_SIZE)
#define FLASH_SECTOR_TO_ADDRESS( SECTOR )       (FLASH_SECTOR_SIZE * (SECTOR) )
#define FLASH_ADDRESS_TO_SECTOR( ADDRESS )      ( (ADDRESS) >> 11)
#define FLASH_ALIGN_ADDRESS( ADDRESS, SIZE )    ( (ADDRESS) &~( (SIZE) -1) )


/** @defgroup FLASH_IS_FLASH_Definitions FLASH Private macros to check input parameters
 * @{
 */
#define IS_FLASH_SECTOR( SECTOR )               ( (SECTOR) < FLASH_SECTOR_TOTAL)
#define IS_FLASH_NUMBER_SECTORS( NBSECTORS )    ( ( (NBSECTORS) != 0) && ( (NBSECTORS) <= FLASH_SECTOR_TOTAL) )
#define IS_FLASH_TYPEPROGRAM( VALUE )           ( ( (VALUE) == FLASH_TYPEPROGRAM_WORD) || \
                                                  ( (VALUE) == FLASH_TYPEPROGRAM_4WORD) || \
                                                  ( (VALUE) == FLASH_TYPEPROGRAM_32WORD) ) || \
    ( (VALUE) == FLASH_TYPEPROGRAM_SECTOR)
#define IS_FLASH_ADDRESS( ADDRESS )             ( ( (ADDRESS) <= FLASH_END) || ( (ADDRESS >= NVR0_USER) && (ADDRESS <= NVR_END) ) )


/**
 * @}
 */


/**
 * @}
 */
/* Private functions ---------------------------------------------------------*/


/**
 * @}
 */


/**
 * @}
 */

#ifdef __cplusplus
}
#endif

#endif /* __FM15F3xx_HAL_FLASH_H */

/************************ (C) COPYRIGHT FudanMicroelectronics *****END OF FILE****/
