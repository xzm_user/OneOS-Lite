/**
 ******************************************************************************
 * @file    fm15f3xx_ll_pmu.h
 * @author  SRG
 * @version V1.0.0
 * @date    2020-03-17
 * @brief
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2020 FudanMicroelectronics.
 * All rights reserved.</center></h2>
 *
 ******************************************************************************
 */
#ifndef FM15F3XX_LL_PMU_H
#define FM15F3XX_LL_PMU_H
#ifdef __cplusplus
extern "C" {
#endif

#include "fm15f3xx.h"
#include "fm15f3xx_ll_cortex.h"

typedef struct {
    uint32_t    PADHoldEn;          /*@ref PMU_LL_PAD_HOLD*/
    uint32_t    LDO16CFG;           /*@ref PMU_LL_STBY_LDO16*/
} LL_STBY_CFG_TypeDef;

typedef struct {
    uint32_t    LDO12En;            /*@ref PMU_LL_EC_PSTOP_LDO12*/
    uint32_t    LDO12CFG;           /*@ref PMU_LL_EC_PSTOP_LDO12CFG*/
    uint32_t    LDO16En;            /*@ref PMU_LL_EC_PSTOP_LDO16*/
} LL_STOP_CFG_TypeDef;

typedef struct {
    uint32_t    PADSource;          /*@ref PMU_LL_WKUP*/
    uint32_t    INTFSource;         /*@ref PMU_LL_INTF*/
    uint32_t    PADEdgeSel;         /*@ref PMU_LL_EC_WKUP*/
    uint32_t    INTFEdgeSel;        /*@ref PMU_LL_EC_WKUP*/
} LL_LLWU_TypeDef;

typedef struct {
    uint32_t    ModuleASource;      /*@ref PMU_LL_MODULE_WKUP_A*/
    uint32_t    ModuleBSource;      /*@ref PMU_LL_MODULE_WKUP_B*/
} LL_WEC_TypeDef;
typedef struct {
    uint32_t LP_mode;               /*@ref PMU_LL_EC_LP_MODE*/

    LL_LLWU_TypeDef LLWU;
    LL_WEC_TypeDef  WEC;

    LL_STBY_CFG_TypeDef STBY;
    LL_STOP_CFG_TypeDef STOP;
} LL_PMU_InitTypeDef;


/** @defgroup PMU_LL_EC_LP_MODE
 * @{
 */
#define LL_PMU_LP_POWERDOWN (0x00)
#define LL_PMU_LP_STANDBY   (0x01)
#define LL_PMU_LP_STOP      (0x02)
#define LL_PMU_LP_WAIT      (0x03)


/**
 * @}
 */


/** @defgroup PMU_LL_EC_PSTOP_LDO12
 * @{
 */
#define LL_PSTOP_LDO12ENLP_NORMAL   (0x00000000U)                       /*!< Enable ldo12 enter  normalmode  when the CPU enters Stop mode */
#define LL_PSTOP_LDO12ENLP_LOWPOWER (PMU_PSTOP_LDO12ENLP)               /*!< Enable ldo12 enter  lowpower mode  when the CPU enters Stop mode */


/**
 * @}
 */


/** @defgroup PMU_LL_EC_PSTOP_LDO12CFG
 * @{
 */
#define LL_PMU_LDO12CFG_1V0 (0x00000000U)                               /*!< level 0 1.0V*/
#define LL_PMU_LDO12CFG_1V1 (0x1U << PMU_PSTOP_LDO12CFG_Pos)            /*!< level 1 1.1V*/
#define LL_PMU_LDO12CFG_1V2 (0x2U << PMU_PSTOP_LDO12CFG_Pos)            /*!< level 2 1.2V*/
#define LL_PMU_LDO12CFG_1V3 (0x3U << PMU_PSTOP_LDO12CFG_Pos)            /*!< level 3 1.3V*/


/**
 * @}
 */


/** @defgroup PMU_LL_EC_PIDLE_BGPERCFG
 * @{
 */
#define LL_PMU_BGPERIODCFG_0    (0x00000000U)                           /*!< level 0 */
#define LL_PMU_BGPERIODCFG_1    (0x1U << PMU_PIDLE_BGPERCFG_Pos)        /*!< level 1 */
#define LL_PMU_BGPERIODCFG_2    (0x2U << PMU_PIDLE_BGPERCFG_Pos)        /*!< level 2 */
#define LL_PMU_BGPERIODCFG_3    (0x3U << PMU_PIDLE_BGPERCFG_Pos)        /*!< level 3 */


/**
 * @}
 */


/** @defgroup PMU_LL_EC_PSTOP_LDO16
 * @{
 */
#define LL_PSTOP_LDO16ENLP_NORMAL   (0x00000000U)                       /*!< Enable ldo16 enter normal mode  when the CPU enters Stop mode */
#define LL_PSTOP_LDO16ENLP_LOWPOWER (PMU_PSTOP_LDO16ENLP)               /*!< Enable ldo16 enter lowpower mode  when the CPU enters Stop mode */


/**
 * @}
 */


/**
 * @}
 */


/** @defgroup PMU_LL_WKUP PIN SELECT
 * @{
 */
#define LL_LLWU_WKUP_PD0    (0x00000001U)                               /*!< PD0 */
#define LL_LLWU_WKUP_PD1    (0x00000002U)                               /*!< PD1 */
#define LL_LLWU_WKUP_PD2    (0x00000004U)                               /*!< PD2 */
#define LL_LLWU_WKUP_PD3    (0x00000008U)                               /*!< PD3 */
#define LL_LLWU_WKUP_PD4    (0x00000010U)                               /*!< PD4 */
#define LL_LLWU_WKUP_PD5    (0x00000020U)                               /*!< PD5 */
#define LL_LLWU_WKUP_PD6    (0x00000040U)                               /*!< PD6 */
#define LL_LLWU_WKUP_PD7    (0x00000080U)                               /*!< PD7 */
#define LL_LLWU_WKUP_PC4    (0x00000100U)                               /*!< PC4 */
#define LL_LLWU_WKUP_PC5    (0x00000200U)                               /*!< PC5 */
#define LL_LLWU_WKUP_PC6    (0x00000400U)                               /*!< PC6 */
#define LL_LLWU_WKUP_PC7    (0x00000800U)                               /*!< PC7 */
#define LL_LLWU_WKUP_PF0    (0x00001000U)                               /*!< PF0 */
#define LL_LLWU_WKUP_PF1    (0x00002000U)                               /*!< PF1 */
#define LL_LLWU_WKUP_PF2    (0x00004000U)                               /*!< PF2 */
#define LL_LLWU_WKUP_PF3    (0x00008000U)                               /*!< PF3 */
#define LL_LLWU_WKUP_PF4    (0x00010000U)                               /*!< PF4 */
#define LL_LLWU_PAD_ALL     (0x0001FFFFU)                               /*!< ALL PAD */


/**
 * @}
 */


/** @defgroup PMU_LL_EC_WKUP
 * @{
 */
#define LL_LLWU_WKUP_CLOSED (0x00000000U)                               /*!< Wakeup closed */
#define LL_LLWU_WKUP_RISE   (0x00000001U)                               /*!< Wakeup rising edge */
#define LL_LLWU_WKUP_FALL   (0x00000002U)                               /*!< Wakeup falling edge */
#define LL_LLWU_WKUP_BOTH   (0x00000003U)                               /*!< Wakeup both edge */


/**
 * @}
 */


/** @defgroup PMU_LL_INTF SELECT
 * @{
 */
#define LL_LLWU_WKUP_LPTM           (0x00000001U)                       /*!< v2m_pdm_lptmr_wakeup */
#define LL_LLWU_WKUP_CMPOUT         (0x00000002U)                       /*!< a2v_cmp_out */
#define LL_LLWU_WKUP_USBDP          (0x00000004U)                       /*!< a2v_phy_rx_dp */
#define LL_LLWU_WKUP_USBDM          (0x00000008U)                       /*!< a2v_phy_rx_dm */
#define LL_LLWU_WKUP_VBUSDET        (0x00000010U)                       /*!< p2v_vbus_det */
#define LL_LLWU_WKUP_RTCALARM       (0x00000020U)                       /*!< b2v_rtc_alarm */
#define LL_LLWU_WKUP_RTCTAMPER      (0x00000040U)                       /*!< b2v_rtc_tamper */
#define LL_LLWU_WKUP_RTC1S          (0x00000080U)                       /*!< b2v_rtc_1s */
#define LL_LLWU_WKUP_CHG90PER       (0x00000100U)                       /*!< a2v_chg_90per */
#define LL_LLWU_WKUP_CHGIEOC        (0x00000200U)                       /*!< a2v_chg_ieoc_flg */
#define LL_LLWU_WKUP_CHGPWOK        (0x00000400U)                       /*!< a2v_chg_pwok */
#define LL_LLWU_WKUP_CHG_MON_CHG    (0x00000800U)                       /*!< a2v_chg_vbat_mon_chg */
#define LL_LLWU_WKUP_CHG_MON_RCH    (0x00001000U)                       /*!< a2v_chg_vbat_mon_rch */
#define LL_LLWU_WKUP_JTAG_SELN      (0x00004000U)                       /*!< p2v_jtag_sel_n */
#define LL_LLWU_WKUP_STBY_PAD_EVENT (0x00008000U)                       /*!< m2v_pm_standby_pad_event */
#define LL_LLWU_INTF_ALL            (0x0000FFFFU)                       /*!< all intf event */


/**
 * @}
 */


/** @defgroup PMU_LL_PAD_HOLD
 * @{
 */
#define LL_PMU_PAD_HOLD_DISABLE (0x00)
#define LL_PMU_PAD_HOLD_ENABLE  (0x01)


/**
 * @}
 */


/** @defgroup PMU_LL_STBY_LDO16
 * @{
 */
#define LL_PMU_STBY_LDO16_LOWPOWER  (0x00)
#define LL_PMU_STBY_LDO16_CLOSE     (0x01)


/**
 * @}
 */


/** @defgroup PMU_LL_PADFILTER
 * @{
 */
#define LL_PMU_LLWU_PADFILTER_CH0   (0x01)
#define LL_PMU_LLWU_PADFILTER_CH1   (0x02)
#define LL_PMU_LLWU_PADFILTER_CH2   (0x04)
#define LL_PMU_LLWU_PADFILTER_CH3   (0x08)

#define LL_PMU_LLWU_PADFILTER_CH0SN (0)
#define LL_PMU_LLWU_PADFILTER_CH1SN (1)
#define LL_PMU_LLWU_PADFILTER_CH2SN (2)
#define LL_PMU_LLWU_PADFILTER_CH3SN (3)


/**
 * @}
 */


/** @defgroup PMU_LL_MODULE_WKUP_A
 * @{
 */
#define LL_WEC_WKUPA_LPTIMER_INT    (0x00000001U)
#define LL_WEC_WKUPA_STIMER4_INT    (0x00000004U)
#define LL_WEC_WKUPA_STIMER5_INT    (0x00000008U)
#define LL_WEC_WKUPA_STIMER0_INT    (0x00000010U)
#define LL_WEC_WKUPA_STIMER1_INT    (0x00000020U)
#define LL_WEC_WKUPA_STIMER2_INT    (0x00000040U)
#define LL_WEC_WKUPA_STIMER3_INT    (0x00000080U)
#define LL_WEC_WKUPA_LLWU           (0x00000100U)
#define LL_WEC_WKUPA_UART0_INT      (0x00010000U)
#define LL_WEC_WKUPA_UART1_INT      (0x00020000U)
#define LL_WEC_WKUPA_UART2_INT      (0x00040000U)
#define LL_WEC_WKUPA_UART3_INT      (0x00080000U)
#define LL_WEC_WKUPA_CT0_INT        (0x01000000U)
#define LL_WEC_WKUPA_CT1_INT        (0x02000000U)
#define LL_WEC_WKUPA_ALM_INT        (0x10000000U)
#define LL_WEC_WKUPA_TAMPER_ALM_INT (0x20000000U)
#define LL_WEC_WKUPA_RTC_ALM_INT    (0x40000000U)
#define LL_WEC_WKUPA_RTC_1S_INT     (0x80000000U)


/**
 * @}
 */


/** @defgroup PMU_LL_MODULE_WKUP_B
 * @{
 */
#define LL_WEC_WKUPB_GPIOA  (0x00000002U)
#define LL_WEC_WKUPB_GPIOB  (0x00000004U)
#define LL_WEC_WKUPB_GPIOC  (0x00000008U)
#define LL_WEC_WKUPB_GPIOD  (0x00000010U)
#define LL_WEC_WKUPB_GPIOE  (0x00000020U)
#define LL_WEC_WKUPB_GPIOF  (0x00000040U)


/**
 * @}
 */


/** @defgroup PMU_LL_MODULE_WKUP_C
 * @{
 */
#define LL_WEC_WKUPC_LLWU   (0x00000001U)
#define LL_WEC_WKUPC_CMP    (0x00000002U)
#define LL_WEC_WKUPC_ADC    (0x00000004U)
#define LL_WEC_WKUPC_DMA    (0x00000008U)


/**
 * @}
 */


/**
 * @}
 */
#define LL_Reserved_PF4 (0x00002000U)                 /*for internal use only*/


/**
 * @}
 */


/** @defgroup PMU_LL_EF_PWRCFG power configuration
 * @{
 */


/**
 * @brief  Enable the LDO12 enter lpmode in Stop Mode
 * @rmtoll PSTOP    LDO12ENLP       LL_PSTOP_EnableLDO12LP
 * @retval None
 */
__STATIC_INLINE void LL_PSTOP_EnableLDO12LP(void)
{
    SET_BIT(PMU->PSTOP, PMU_PSTOP_LDO12ENLP);
}


/**
 * @brief  Disable the LDO12 enter lpmode in Stop Mode
 * @rmtoll PSTOP    LDO12ENLP       LL_PSTOP_DisableLDO12LP
 * @retval None
 */
__STATIC_INLINE void LL_PSTOP_DisableLDO12LP(void)
{
    CLEAR_BIT(PMU->PSTOP, PMU_PSTOP_LDO12ENLP);
}


/**
 * @brief  Enable the LDO16 enter lpmode in Stop Mode
 * @rmtoll PSTOP    LDO16ENLP       LL_PSTOP_EnableLDO16LP
 * @retval None
 */
__STATIC_INLINE void LL_PSTOP_EnableLDO16LP(void)
{
    SET_BIT(PMU->PSTOP, PMU_PSTOP_LDO16ENLP);
}


/**
 * @brief  Disable the LDO16 enter lpmode in Stop Mode
 * @rmtoll PSTOP    LDO16ENLP       LL_PSTOP_DisableLDO16LP
 * @retval None
 */
__STATIC_INLINE void LL_PSTOP_DisableLDO16LP(void)
{
    CLEAR_BIT(PMU->PSTOP, PMU_PSTOP_LDO16ENLP);
}


/**
 * @brief  Configure the voltage of ldo12 in stop mode
 * @rmtoll PSTOP    LDO12CFG       LL_PSTOP_LDO12CFG
 * @param  PVDLevel This parameter can be one of the following values:
 *         @arg @ref LL_PMU_LDO12CFG_1V0
 *         @arg @ref LL_PMU_LDO12CFG_1V1
 *         @arg @ref LL_PMU_LDO12CFG_1V2
 *         @arg @ref LL_PMU_LDO12CFG_1V3
 * @retval None
 */
__STATIC_INLINE void LL_PSTOP_SetLDO12(uint32_t PVDLevel)
{
    MODIFY_REG(PMU->PSTOP, PMU_PSTOP_LDO12CFG, PVDLevel);
}


/**
 * @brief  Get the voltage of ldo12 in stop mode
 * @rmtoll PSTOP    LDO12CFG       LL_PSTOP_GetLDO12
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_PMU_LDO12CFG_1V0
 *         @arg @ref LL_PMU_LDO12CFG_1V1
 *         @arg @ref LL_PMU_LDO12CFG_1V2
 *         @arg @ref LL_PMU_LDO12CFG_1V3
 */
__STATIC_INLINE uint32_t LL_PSTOP_GetLDO12(void)
{
    return ((uint32_t)(READ_BIT(PMU->PSTOP, PMU_PSTOP_LDO12CFG)));
}


/**
 * @brief  Enable the LDO16  in Standby Mode
 * @rmtoll PSTBY    LDO16EN       LL_PSTBY_EnableLDO16
 * @retval None
 */
__STATIC_INLINE void LL_PSTBY_EnableLDO16(void)
{
    CLEAR_BIT(PMU->PSTBY, PMU_PSTBY_LDO16EN);
}


/**
 * @brief  Disable the LDO16  in Standby Mode
 * @rmtoll PSTBY    LDO16EN       LL_PSTBY_DisableLDO16
 * @retval None
 */
__STATIC_INLINE void LL_PSTBY_DisableLDO16(void)
{
    SET_BIT(PMU->PSTBY, PMU_PSTBY_LDO16EN);
}


/**
 * @}
 */


/**
 * @brief  Set the need count of 4MCLK when digit 1.2V power switch ready
 * @rmtoll PSWRDY    CNT4MDIG       LL_PSW_DIG_SetRdyCnt4M
 * @param  count This parameter can be one of the following values:
 *         0~0xF
 * @retval None
 */
__STATIC_INLINE void LL_PSW_DIG_SetRdyCnt4M(uint32_t count)
{
    MODIFY_REG(PMU->PSWRDY, PMU_PSWRDY_CNT4MDIG, count);
}


/**
 * @brief  Set the need count of 4MCLK when ram1/2 power switch ready
 * @rmtoll PSWRDY    CNT4MRAM       LL_PSW_RAM_SetRdyCnt4M
 * @param  count This parameter can be one of the following values:
 *         0~0xF
 * @retval None
 */
__STATIC_INLINE void LL_PSW_RAM_SetRdyCnt4M(uint32_t count)
{
    MODIFY_REG(PMU->PSWRDY, PMU_PSWRDY_CNT4MRAM, count << PMU_PSWRDY_CNT4MRAM_Pos);
}


/**
 * @brief  Set the need count of 4MCLK when flash power switch ready
 * @rmtoll PSWRDY    CNT4MFLASH       LL_PSW_Flash_SetRdfCnt4M
 * @param  count This parameter can be one of the following values:
 *         0~0xF
 * @retval None
 */
__STATIC_INLINE void LL_PSW_Flash_SetRdfCnt4M(uint32_t count)
{
    MODIFY_REG(PMU->PSWRDY, PMU_PSWRDY_CNT4MFLASH, count << PMU_PSWRDY_CNT4MFLASH_Pos);
}


/**
 * @brief  Set the need count of 4MCLK when USB power switch ready
 * @rmtoll PSWRDY    CNT4MUSB       LL_PSW_USB_SetRdyCnt4M
 * @param  count This parameter can be one of the following values:
 *         0~0xF
 * @retval None
 */
__STATIC_INLINE void LL_PSW_USB_SetRdyCnt4M(uint32_t count)
{
    MODIFY_REG(PMU->PSWRDY, PMU_PSWRDY_CNT4MUSB, count << PMU_PSWRDY_CNT4MUSB_Pos);
}


/**
 * @brief  open the xxx power switch
 * @rmtoll PSWEN    ENxxx       LL_PSW_OpenModule
 * @param  pswch This parameter can be a combination of the following values:
 *         @arg @ref PMU_PSWEN_ENDIG
 *         @arg @ref PMU_PSWEN_ENFLASH
 *         @arg @ref PMU_PSWEN_ENRAMA
 *         @arg @ref PMU_PSWEN_ENRAMB
 *         @arg @ref PMU_PSWEN_ENUSB
 * @retval None
 */
__STATIC_INLINE void LL_PSW_OpenModule(uint32_t pswch)
{
    SET_BIT(PMU->PSWEN, pswch);
}


/**
 * @brief  Close the xxx power switch
 * @rmtoll PSWEN    xxx       LL_PSW_CloseModule
 * @param  pswch This parameter can be a combination of the following values:
 *         @arg @ref PMU_PSWEN_ENDIG
 *         @arg @ref PMU_PSWEN_ENFLASH
 *         @arg @ref PMU_PSWEN_ENRAMA
 *         @arg @ref PMU_PSWEN_ENRAMB
 *         @arg @ref PMU_PSWEN_ENUSB
 * @retval None
 */
__STATIC_INLINE void LL_PSW_CloseModule(uint32_t pswch)
{
    CLEAR_BIT(PMU->PSWEN, pswch);
}


/**
 * @}
 */


/** @defgroup PMU_LL_EF_RTC rtc isolation
 * @{
 */


/**
 * @brief  Enable the rtc isolation
 * @rmtoll RTCISO    OPEN       LL_EnableRtcIso
 * @retval None
 */
__STATIC_INLINE void LL_ISORTC_Enable(void)
{
    SET_BIT(PMU->RTCISO, PMU_RTCISO_OPEN);
}


/**
 * @brief  Disable the rtc isolation
 * @rmtoll RTCISO    OPEN       LL_DisableRtcIso
 * @retval None
 */
__STATIC_INLINE void LL_ISORTC_Disable(void)
{
    CLEAR_BIT(PMU->RTCISO, PMU_RTCISO_OPEN);
}


/** @defgroup PMU_LL_EF_LLWU Low leak wakeup
 * @{
 */


/**
 * @brief  Select the channel of filter in llwu and enable channel
 * @rmtoll FILTS,PADEN        FILTxSEL       LL_LLWU_ConfigPadFilterx
 * @param  fliterSN This parameter can be one of the following values:
    0-0x03
 * @param  channel Between Min_Data = 0 and Max_Data = 0x1F
 * @retval None
 */
__STATIC_INLINE void LL_LLWU_ConfigPadFilter(uint32_t fliterSN, uint32_t channel)
{
    SET_BIT(PMU->PADEN, (0x1U) << channel);
    MODIFY_REG(PMU->FILTS, (0x1FU) << (fliterSN * (8U)), channel);
}


/**
 * @brief  Enable the filter0 of llwu
 * @rmtoll FILTE    LLWUFE       LL_LLWU_EnableFilterx
 * @param  fliterch This parameter can be a combination of the following values:
 *         @arg @ref LL_PMU_LLWU_PADFILTER_CH0
 *         @arg @ref LL_PMU_LLWU_PADFILTER_CH1
 *         @arg @ref LL_PMU_LLWU_PADFILTER_CH2
 *         @arg @ref LL_PMU_LLWU_PADFILTER_CH3
 * @retval None
 */
__STATIC_INLINE void LL_LLWU_EnablePadFilter(uint32_t fliterch)
{
    SET_BIT(PMU->FILTE, fliterch);
}


/**
 * @brief  Disable the filter0 of llwu
 * @rmtoll FILTE    LLWUFE       LL_LLWU_DisableFilterx
 * @param  fliterch This parameter can be a combination of the following values:
 *         @arg @ref LL_PMU_LLWU_PADFILTER_CH0
 *         @arg @ref LL_PMU_LLWU_PADFILTER_CH1
 *         @arg @ref LL_PMU_LLWU_PADFILTER_CH2
 *         @arg @ref LL_PMU_LLWU_PADFILTER_CH3
 * @retval None
 */
__STATIC_INLINE void LL_LLWU_DisablePadFilter(uint32_t fliterch)
{
    CLEAR_BIT(PMU->FILTE, fliterch);
}


/**
 * @brief  Enable the channel of intf
 * @rmtoll INTFE        LLWUIFE       LL_LLWU_EnableIntf
 * @param  INFT This parameter can be a combination of the following values:
 *         @arg @ref LL_LLWU_WKUP_LPTM
 *         @arg @ref LL_LLWU_WKUP_CMPOUT
 *         @arg @ref LL_LLWU_WKUP_USBDP
 *         @arg @ref LL_LLWU_WKUP_USBDM
 *         @arg @ref LL_LLWU_WKUP_VBUSDET
 *         @arg @ref LL_LLWU_WKUP_RTCALARM
 *         @arg @ref LL_LLWU_WKUP_RTCTAMPER
 *         @arg @ref LL_LLWU_WKUP_RTC1S
 *         @arg @ref LL_LLWU_WKUP_CHG90PER
 *         @arg @ref LL_LLWU_WKUP_CHGIEOC
 *         @arg @ref LL_LLWU_WKUP_CHGPWOK
 *         @arg @ref LL_LLWU_WKUP_CHG_MON_CHG
 *         @arg @ref LL_LLWU_WKUP_CHG_MON_RCH
 *         @arg @ref LL_LLWU_WKUP_JTAG_SELN
 *         @arg @ref LL_LLWU_WKUP_STBY_PAD_EVENT (only used in standby mode)
 *         @arg @ref LL_LLWU_INTF_ALL
 * @retval None
 */
__STATIC_INLINE void LL_LLWU_EnableIntf(uint32_t channel)
{
    SET_BIT(PMU->INTFE, channel);
}


/**
 * @brief  Disable the channel of intf
 * @rmtoll INTFE        LLWUIFE       LL_LLWU_DisableIntf
 * @param  INFT This parameter can be a combination of the following values:
 *         @arg @ref LL_LLWU_WKUP_LPTM
 *         @arg @ref LL_LLWU_WKUP_CMPOUT
 *         @arg @ref LL_LLWU_WKUP_USBDP
 *         @arg @ref LL_LLWU_WKUP_USBDM
 *         @arg @ref LL_LLWU_WKUP_VBUSDET
 *         @arg @ref LL_LLWU_WKUP_RTCALARM
 *         @arg @ref LL_LLWU_WKUP_RTCTAMPER
 *         @arg @ref LL_LLWU_WKUP_RTC1S
 *         @arg @ref LL_LLWU_WKUP_CHG90PER
 *         @arg @ref LL_LLWU_WKUP_CHGIEOC
 *         @arg @ref LL_LLWU_WKUP_CHGPWOK
 *         @arg @ref LL_LLWU_WKUP_CHG_MON_CHG
 *         @arg @ref LL_LLWU_WKUP_CHG_MON_RCH
 *         @arg @ref LL_LLWU_WKUP_JTAG_SELN
 *         @arg @ref LL_LLWU_WKUP_STBY_PAD_EVENT (only used in standby mode)
 *         @arg @ref LL_LLWU_INTF_ALL
 * @retval None
 */
__STATIC_INLINE void LL_LLWU_DisableIntf(uint32_t channel)
{
    CLEAR_BIT(PMU->INTFE, channel);
}


/**
 * @brief  Enable the channel of pad
 * @rmtoll PADEN        LLWUPAD       LL_LLWU_EnablePad
 * @param  pad This parameter can be a combination of the following values:
 *         @arg @ref LL_LLWU_WKUP_PD0
 *         @arg @ref LL_LLWU_WKUP_PD1
 *         @arg @ref LL_LLWU_WKUP_PD2
 *         @arg @ref LL_LLWU_WKUP_PD3
 *         @arg @ref LL_LLWU_WKUP_PD4
 *         @arg @ref LL_LLWU_WKUP_PD5
 *         @arg @ref LL_LLWU_WKUP_PD6
 *         @arg @ref LL_LLWU_WKUP_PD7
 *         @arg @ref LL_LLWU_WKUP_PC4
 *         @arg @ref LL_LLWU_WKUP_PC5
 *         @arg @ref LL_LLWU_WKUP_PC6
 *         @arg @ref LL_LLWU_WKUP_PC7
 *         @arg @ref LL_LLWU_WKUP_PF0
 *         @arg @ref LL_LLWU_WKUP_PF1
 *         @arg @ref LL_LLWU_WKUP_PF2
 *         @arg @ref LL_LLWU_WKUP_PF3
 *         @arg @ref LL_LLWU_WKUP_PF4
 *         @arg @ref LL_LLWU_PAD_ALL
 * @retval None
 */
__STATIC_INLINE void LL_LLWU_EnablePad(uint32_t channel)
{
    if (LL_LLWU_WKUP_PF4 & channel)
        SET_BIT(PMU->INTFE, LL_Reserved_PF4);

    channel &= (~LL_LLWU_WKUP_PF4);
    SET_BIT(PMU->PADEN, channel);
}


/**
 * @brief  Disable the channel of pad
 * @rmtoll PADEN        LLWUPAD       LL_LLWU_DisablePad
 * @param  channel This parameter can be a combination of the following values:
 *         @arg @ref LL_LLWU_WKUP_PD0
 *         @arg @ref LL_LLWU_WKUP_PD1
 *         @arg @ref LL_LLWU_WKUP_PD2
 *         @arg @ref LL_LLWU_WKUP_PD3
 *         @arg @ref LL_LLWU_WKUP_PD4
 *         @arg @ref LL_LLWU_WKUP_PD5
 *         @arg @ref LL_LLWU_WKUP_PD6
 *         @arg @ref LL_LLWU_WKUP_PD7
 *         @arg @ref LL_LLWU_WKUP_PC4
 *         @arg @ref LL_LLWU_WKUP_PC5
 *         @arg @ref LL_LLWU_WKUP_PC6
 *         @arg @ref LL_LLWU_WKUP_PC7
 *         @arg @ref LL_LLWU_WKUP_PF0
 *         @arg @ref LL_LLWU_WKUP_PF1
 *         @arg @ref LL_LLWU_WKUP_PF2
 *         @arg @ref LL_LLWU_WKUP_PF3
 *         @arg @ref LL_LLWU_WKUP_PF4
 *         @arg @ref LL_LLWU_PAD_ALL
 * @retval None
 */
__STATIC_INLINE void LL_LLWU_DisablePad(uint32_t channel)
{
    if (LL_LLWU_WKUP_PF4 & channel)
        CLEAR_BIT(PMU->INTFE, LL_Reserved_PF4);

    channel &= (~LL_LLWU_WKUP_PF4);
    CLEAR_BIT(PMU->PADEN, channel);
}


/**
 * @brief  Configure the wakeup of pad in llwu
 * @rmtoll PCFGA,PCFGB    ---       LL_LLWU_ConfigWakeupPad
 * @param  pad This parameter can be a combination of the following values:
 *         @arg @ref LL_LLWU_WKUP_PD0
 *         @arg @ref LL_LLWU_WKUP_PD1
 *         @arg @ref LL_LLWU_WKUP_PD2
 *         @arg @ref LL_LLWU_WKUP_PD3
 *         @arg @ref LL_LLWU_WKUP_PD4
 *         @arg @ref LL_LLWU_WKUP_PD5
 *         @arg @ref LL_LLWU_WKUP_PD6
 *         @arg @ref LL_LLWU_WKUP_PD7
 *         @arg @ref LL_LLWU_WKUP_PC4
 *         @arg @ref LL_LLWU_WKUP_PC5
 *         @arg @ref LL_LLWU_WKUP_PC6
 *         @arg @ref LL_LLWU_WKUP_PC7
 *         @arg @ref LL_LLWU_WKUP_PF0
 *         @arg @ref LL_LLWU_WKUP_PF1
 *         @arg @ref LL_LLWU_WKUP_PF2
 *         @arg @ref LL_LLWU_WKUP_PF3
 *         @arg @ref LL_LLWU_WKUP_PF4
 *         @arg @ref LL_LLWU_PAD_ALL
 * @param  wakeup This parameter can be one of the following values:
 *         @arg @ref LL_LLWU_WKUP_CLOSED
 *         @arg @ref LL_LLWU_WKUP_FALL
 *         @arg @ref LL_LLWU_WKUP_RISE
 *         @arg @ref LL_LLWU_WKUP_BOTH
 * @retval None
 */
__STATIC_INLINE void LL_LLWU_ConfigWakeupPad(uint32_t pad, uint32_t wakeup)
{
    uint32_t    bits = pad;
    uint32_t    setmask = 0, clearmask = 0;
    for (uint32_t i = 0; i < 16; i++) {
        if (bits & 0x01) {
            setmask     |= (wakeup << (i * 2));
            clearmask   |= (0x03U << (i * 2));
        }
        bits >>= 1;
    }
    CLEAR_BIT(PMU->PCFGA, clearmask);
    MODIFY_REG(PMU->PCFGA, clearmask, setmask);
    if (LL_LLWU_WKUP_PF4 & pad) {
        CLEAR_BIT(PMU->INTFCFGA, (0x3U) << (0x0D * 2));
        MODIFY_REG(PMU->INTFCFGA, (0x3U) << (0x0D * 2), wakeup << (0x0D * 2));
    }
}


/**
 * @brief  Configure the wakeup of intf in llwu
 * @rmtoll INTFCFGA           LL_LLWU_ConfigWakeupIntf
 * @param  INFT This parameter can be a combination of the following values:
 *         @arg @ref LL_LLWU_WKUP_LPTM
 *         @arg @ref LL_LLWU_WKUP_CMPOUT
 *         @arg @ref LL_LLWU_WKUP_USBDP
 *         @arg @ref LL_LLWU_WKUP_USBDM
 *         @arg @ref LL_LLWU_WKUP_VBUSDET
 *         @arg @ref LL_LLWU_WKUP_RTCALARM
 *         @arg @ref LL_LLWU_WKUP_RTCTAMPER
 *         @arg @ref LL_LLWU_WKUP_RTC1S
 *         @arg @ref LL_LLWU_WKUP_CHG90PER
 *         @arg @ref LL_LLWU_WKUP_CHGIEOC
 *         @arg @ref LL_LLWU_WKUP_CHGPWOK
 *         @arg @ref LL_LLWU_WKUP_CHG_MON_CHG
 *         @arg @ref LL_LLWU_WKUP_CHG_MON_RCH
 *         @arg @ref LL_LLWU_WKUP_JTAG_SELN
 *         @arg @ref LL_LLWU_WKUP_STBY_PAD_EVENT (only used in standby mode)
 *         @arg @ref LL_LLWU_INTF_ALL
 * @param  wakeup This parameter can be one of the following values:
 *         @arg @ref LL_LLWU_WKUP_CLOSED
 *         @arg @ref LL_LLWU_WKUP_FALL
 *         @arg @ref LL_LLWU_WKUP_RISE
 *         @arg @ref LL_LLWU_WKUP_BOTH
 * @retval None
 */
__STATIC_INLINE void LL_LLWU_ConfigWakeupIntf(uint32_t intf, uint32_t wakeup)
{
    uint32_t    bits = intf;
    uint32_t    setmask = 0, clearmask = 0;
    uint32_t    i;
    for (i = 0; i < 16; i++) {
        if (bits & 0x01) {
            setmask     |= (wakeup << (i * 2));
            clearmask   |= (0x03U << (i * 2));
        }
        bits >>= 1;
    }
    CLEAR_BIT(PMU->INTFCFGA, clearmask);
    MODIFY_REG(PMU->INTFCFGA, clearmask, setmask);
}


/**
 * @brief  Configure the wakeup of filter in llwu
 * @rmtoll FILTCFGA    ---       LL_LLWU_ConfigWakeupPadFilter
 * @param  filter This parameter can be one of the following values:
    0-0x03
 * @param  wakeup This parameter can be one of the following values:
 *         @arg @ref LL_LLWU_WKUP_CLOSED
 *         @arg @ref LL_LLWU_WKUP_FALL
 *         @arg @ref LL_LLWU_WKUP_RISE
 *         @arg @ref LL_LLWU_WKUP_BOTH
 * @retval None
 */
__STATIC_INLINE void LL_LLWU_ConfigWakeupPadFilter(uint32_t filter, uint32_t wakeup)
{
    MODIFY_REG(PMU->FILTCFGA, (0x3U) << (filter << 1), wakeup);
}


/**
 * @brief  Clear the record of wakeup in llwu
 * @rmtoll RCDCLR        CLR       LL_LLWU_WakeupAllRecordClear
 * @param  None
 * @retval None
 */
__STATIC_INLINE void LL_LLWU_ClearAllRecordWakeup()
{
    SET_BIT(PMU->RCDCLR, PMU_RCDCLR_WAKEUP);
}


/**
 * @brief  Hold the  analog and pad signal
 * @note
 * @rmtoll PHLDR        HOLDENLP       LL_PMU_PadHoldEn
 * @param  None
 * @retval None
 */
__STATIC_INLINE void LL_PMU_EnablePadHold(void)
{
    SET_BIT(PMU->HOLDENLP, PMU_HOLDENLP_PADHOLDEN);
}


/** @defgroup PMU_LL_EF_PM power manage
 * @{
 */


/**
 * @brief  Release the hold of analog and pad signal
 * @note hardware automaticly set 0
 * @rmtoll PHLDR        PHOLDRLS       LL_PMU_ReleasePadHold
 * @param  None
 * @retval None
 */
__STATIC_INLINE void LL_PMU_ReleasePadHold(void)
{
    SET_BIT(PMU->PHLDR, PMU_PHLDR_PHOLDRLS);
}


/**
 * @brief  Get the status of pad hold
 * @rmtoll PHLDS        PADHOLD       LL_PMU_GetPadHoldStatus
 * @retval 0,1
 */
__STATIC_INLINE uint32_t LL_PMU_GetStatus_PadHold(void)
{
    return ((uint32_t)(READ_BIT(PMU->PHLDS, PMU_PHLDS_PADHOLD)));
}


/**
 * @brief  Get the status of analog hold
 * @rmtoll PHLDS        ANAHOLD       LL_PMU_GetAnaHoldStatus
 * @retval 0,1
 */
__STATIC_INLINE uint32_t LL_PMU_GetStatus_AnaHold(void)
{
    return ((uint32_t)(READ_BIT(PMU->PHLDS, PMU_PHLDS_ANAHOLD) >> PMU_PHLDS_ANAHOLD_Pos));
}


/**
 * @brief  Set the ready counter of LDO
 * @rmtoll LDORDY               LL_PMU_SetLDOReadyCnt
 * @retval None
 */
__STATIC_INLINE void LL_PMU_SetLDOReadyCnt(void)
{
    WRITE_REG(PMU->LDORDY, 0x01020202);
}


/** @defgroup LL_PM_LPM_SLEEP
 * @{
 */
#define LL_PMU_SLP_WAIT 0x00000000U                                 /*!< wait */
#define LL_PMU_SLP_STOP 0x00000001U                                 /*!< stop */


/**
 * @}
 */


/** @defgroup LL_PM_LPM_DEEPSLEEP
 * @{
 */
#define LL_PMU_DPSLP_STDBY      0x00000000U                         /*!< standby   */
#define LL_PMU_DPSLP_PWRDOWN    0x00000002U                         /*!< deepsleep */


/**
 * @}
 */


/**
 * @brief  Set the low power mode for sleep
 * @rmtoll LPM SLEEP           LL_PMU_SetSleepMode
 * @param  SlpMode This parameter can be one of the following values:
 *         @arg @ref LL_PMU_SLP_WAIT
 *         @arg @ref LL_PMU_SLP_STOP
 * @retval None
 */
__STATIC_INLINE void LL_PMU_SetSleepMode(uint32_t SlpMode)
{
    MODIFY_REG(PMU->LPM, PMU_LPM_SLEEP, SlpMode);
}


/**
 * @brief  Set the low power mode for deepsleep
 * @rmtoll LPM DEEPSLEEP       LL_PMU_SetDeepsleepMode
 * @param  DpSlpMode This parameter can be one of the following values:
 *         @arg @ref LL_PMU_DPSLP_STDBY
 *         @arg @ref LL_PMU_DPSLP_PWRDOWN
 * @retval None
 */
__STATIC_INLINE void LL_PMU_SetDeepSleepMode(uint32_t DpSlpMode)
{
    MODIFY_REG(PMU->LPM, PMU_LPM_DEEPSLEEP, DpSlpMode);
}


/**
 * @brief  Clear the PM interrupt
 * @rmtoll INTCLR PMU_INTCLR_PM       LL_PMU_ClearFlagPM_IT
 * @param  None
 * @retval None
 */
__STATIC_INLINE void LL_PMU_ClearFlagPM_IT(void)
{
    WRITE_REG(PMU->INTCLR, PMU_INTCLR_PM);
}


/*************************************************************CHGERCFG REG*************************************************/


/** @defgroup LL_CHARGE_Mode
 * @{
 */
#define LL_CHARGE_HARDMODEDIS   0x0U
#define LL_CHARGE_HARDMODEEN    PMU_CHGERCFG_HDEN


/**
 * @}
 */


/** @defgroup LL_CHARGE_LevelShift
 * @{
 */
#define LL_CHARGE_LEVELSHIFTCLOSE   0x0U
#define LL_CHARGE_LEVELSHIFTOPEN    PMU_CHGERCFG_LSEN


/**
 * @}
 */

/* Private functions ---------------------------------------------------------*/


/** @defgroup CHARGE_LL_Functions CHARGE Functions
 * @{
 */


/** @defgroup CHARGE_LL_Configuration Configuration
 * @{
 */


/**
 * @brief  Set CHARGE Mode
 * @param  This parameter can be one of the following values:
 *         @arg @ref LL_CHARGE_HARDMODEEN
 *         @arg @ref LL_CHARGE_HARDMODEDIS
 * @retval None
 */
__STATIC_INLINE void LL_CHARGE_SetMode(uint32_t mode)
{
    MODIFY_REG(PMU->CHGERCFG, PMU_CHGERCFG_HDEN, mode);
}


/**
 * @brief  OPEN OR Close CHARGE levelshift
 * @param  This parameter can be one of the following values:
 *         @arg @ref LL_CHARGE_LEVELSHIFTOPEN
 *         @arg @ref LL_CHARGE_LEVELSHIFTCLOSE
 * @retval None
 */
__STATIC_INLINE void LL_CHARGE_SetLevelShiftEn(uint32_t en)
{
    MODIFY_REG(PMU->CHGERCFG, PMU_CHGERCFG_LSEN, en);
}


/**
 * @brief  Set CHARGE filter for lpo clk
 * @param  This parameter can be one of the following values:
 *         @arg @ref cfg: 0x0-0x3
 * @retval None
 */
__STATIC_INLINE void LL_CHARGE_SetFilterMode(uint32_t cfg)
{
    MODIFY_REG(PMU->CHGERCFG, PMU_CHGERCFG_FILTCFG, cfg);
}


/**
 * @}
 */

/******************************************************PMU_CHARGER_STATUS*******************************************************/


/**
 * @brief  get Charge 90% Flag
 * @retval None
 */
__STATIC_INLINE uint32_t LL_CHARGE_IsActiveFlag_Charge90(void)
{
    return (READ_BIT(PMU->CHGERSTS, PMU_CHGERSTS_CSR90PER) == PMU_CHGERSTS_CSR90PER);
}


/**
 * @brief  get Charge 100% Flag
 * @retval None
 */
__STATIC_INLINE uint32_t LL_CHARGE_IsActiveFlag_Eoc(void)
{
    return (READ_BIT(PMU->CHGERSTS, PMU_CHGERSTS_CSRIECOFLG) == PMU_CHGERSTS_CSRIECOFLG);
}


/**
 * @brief get llwu pad wakeup record
 * @param  pad This parameter can be a combination of the following values:
 *         @arg @ref LL_LLWU_WKUP_PD0
 *         @arg @ref LL_LLWU_WKUP_PD1
 *         @arg @ref LL_LLWU_WKUP_PD2
 *         @arg @ref LL_LLWU_WKUP_PD3
 *         @arg @ref LL_LLWU_WKUP_PD4
 *         @arg @ref LL_LLWU_WKUP_PD5
 *         @arg @ref LL_LLWU_WKUP_PD6
 *         @arg @ref LL_LLWU_WKUP_PD7
 *         @arg @ref LL_LLWU_WKUP_PC4
 *         @arg @ref LL_LLWU_WKUP_PC5
 *         @arg @ref LL_LLWU_WKUP_PC6
 *         @arg @ref LL_LLWU_WKUP_PC7
 *         @arg @ref LL_LLWU_WKUP_PF0
 *         @arg @ref LL_LLWU_WKUP_PF1
 *         @arg @ref LL_LLWU_WKUP_PF2
 *         @arg @ref LL_LLWU_WKUP_PF3
 *         @arg @ref LL_LLWU_WKUP_PF4
 * @retval 0 or !0
 */
__STATIC_INLINE uint32_t LL_LLWU_GetRecord_Pad(uint32_t channel)
{
    uint32_t pad = channel, temp = 0;
    for (uint32_t i = 0; i < 16; i++) {
        if (pad & 0x01)
            temp |= (0x01 << i);
        pad >>= 1;
    }
    temp = READ_BIT(PMU->RCDPAD, temp);
    if (LL_LLWU_WKUP_PF4 & channel) {
        if (READ_BIT(PMU->RCDINTF, LL_Reserved_PF4))
            temp |= LL_LLWU_WKUP_PF4;
    }
    return (temp);
}


/**
 * @}
 */


/**
 * @brief get llwu intf wakeup record
 * @param  pad This parameter can be a combination of the following values:
 *         @arg @ref LL_LLWU_WKUP_LPTM
 *         @arg @ref LL_LLWU_WKUP_CMPOUT
 *         @arg @ref LL_LLWU_WKUP_USBDP
 *         @arg @ref LL_LLWU_WKUP_USBDM
 *         @arg @ref LL_LLWU_WKUP_VBUSDET
 *         @arg @ref LL_LLWU_WKUP_RTCALARM
 *         @arg @ref LL_LLWU_WKUP_RTCTAMPER
 *         @arg @ref LL_LLWU_WKUP_RTC1S
 *         @arg @ref LL_LLWU_WKUP_CHG90PER
 *         @arg @ref LL_LLWU_WKUP_CHGIEOC
 *         @arg @ref LL_LLWU_WKUP_CHGPWOK
 *         @arg @ref LL_LLWU_WKUP_CHG_MON_CHG
 *         @arg @ref LL_LLWU_WKUP_CHG_MON_RCH
 *         @arg @ref LL_LLWU_WKUP_JTAG_SELN
 *         @arg @ref LL_LLWU_WKUP_STBY_PAD_EVENT (only used in standby mode)
 * @retval 0 or !0
 */
__STATIC_INLINE uint32_t LL_LLWU_GetRecord_Intf(uint32_t channel)
{
    uint32_t pad = channel, temp = 0;
    for (uint32_t i = 0; i < 16; i++) {
        if (pad & 0x01)
            temp |= (0x01 << i);
        pad >>= 1;
    }
    return (READ_BIT(PMU->RCDINTF, temp));
}


/**
 * @}
 */


/**
 * @brief get llwu pad filter wakeup record
 * @param  pad This parameter can be a combination of the following values:
 *         @arg @ref LL_PMU_LLWU_PADFILTER_CH0
 *         @arg @ref LL_PMU_LLWU_PADFILTER_CH1
 *         @arg @ref LL_PMU_LLWU_PADFILTER_CH2
 *         @arg @ref LL_PMU_LLWU_PADFILTER_CH3
 * @retval 0 or !0
 */
__STATIC_INLINE uint32_t LL_LLWU_GetRecord_PadFilter(uint32_t channel)
{
    return (READ_BIT(PMU->RCDFILT, (1 << channel)));
}


/**
 * @}
 */


/**
 * @brief  LLWU Channel CLK Config
 * @rmtoll CFG          LL_LLWU_ChannelCfg
 * @param None
 * @retval None
 */
__STATIC_INLINE void LL_LLWU_Enable(void)
{
    MODIFY_REG(PMU->CFG, PMU_LLWU_CHRST | PMU_LLWU_CHCLK, (~PMU_LLWU_CHRST) | PMU_LLWU_CHCLK);
}


/**
 * @}
 */


/**
 * @brief write backup reg
 * @param  data:Write data buff
 * @param  len:data len(<8)
 * @retval None
 */
__STATIC_INLINE void LL_PMU_WriteBackupReg(uint32_t *data, uint32_t len)
{
    if (len > 8)
        len = 8;
    for (uint32_t i = 0; i < len; i++)
        WRITE_REG(PMU->REG[i], data[i]);
}


/**
 * @}
 */


/**
 * @brief write backup reg[n]
 * @param  data:Write data
 * @param  n:reg index(<8)
 * @retval None
 */
__STATIC_INLINE void LL_PMU_WriteBackupRegIndex(uint32_t data, uint32_t n)
{
    if (n > 7)
        n = 7;
    WRITE_REG(PMU->REG[n], data);
}


/**
 * @}
 */


/**
 * @brief Read backup reg
 * @param  data:Receive data buff
 * @param  len:data len(<8)
 * @retval None
 */
__STATIC_INLINE void LL_PMU_ReadBackupReg(uint32_t *data, uint32_t len)
{
    if (len > 8)
        len = 8;
    for (uint32_t i = 0; i < len; i++)
        data[i] = READ_REG(PMU->REG[i]);
}


/**
 * @}
 */


/**
 * @brief Read backup reg[n]
 * @param  n:reg index(<8)
 * @retval reg[n]
 */
__STATIC_INLINE uint32_t LL_PMU_ReadBackupRegIndex(uint32_t n)
{
    if (n > 7)
        n = 7;
    return (READ_REG(PMU->REG[n]));
}


/**
 * @}
 */


/**
 * @brief  Enable the channel of module wakeup A
 * @rmtoll WKUPA          LL_WEC_ModuleWakeupAEn
 * @param  channel This parameter can be a combination of the following values:
 *         @arg @ref LL_WEC_WKUPA_LPTIMER_INT
 *         @arg @ref LL_WEC_WKUPA_STIMER4_INT
 *         @arg @ref LL_WEC_WKUPA_STIMER5_INT
 *         @arg @ref LL_WEC_WKUPA_STIMER0_INT
 *         @arg @ref LL_WEC_WKUPA_STIMER1_INT
 *         @arg @ref LL_WEC_WKUPA_STIMER2_INT
 *         @arg @ref LL_WEC_WKUPA_STIMER3_INT
 *         @arg @ref LL_WEC_WKUPA_LLWU
 *         @arg @ref LL_WEC_WKUPA_UART0_INT
 *         @arg @ref LL_WEC_WKUPA_UART1_INT
 *         @arg @ref LL_WEC_WKUPA_UART2_INT
 *         @arg @ref LL_WEC_WKUPA_UART3_INT
 *         @arg @ref LL_WEC_WKUPA_CT0_INT
 *         @arg @ref LL_WEC_WKUPA_CT1_INT
 *         @arg @ref LL_WEC_WKUPA_ALM_INT
 *         @arg @ref LL_WEC_WKUPA_TAMPER_ALM_INT
 *         @arg @ref LL_WEC_WKUPA_RTC_ALM_INT
 *         @arg @ref LL_WEC_WKUPA_RTC_1S_INT
 * @retval None
 */
__STATIC_INLINE void LL_WEC_EnableWakeupA(uint32_t channel)
{
    SET_BIT(PMU->WKUPA, channel);
}


__STATIC_INLINE void LL_WEC_DisableWakeupA(uint32_t channel)
{
    CLEAR_BIT(PMU->WKUPA, channel);
}


__STATIC_INLINE void LL_WEC_DisableAllWakeupA(void)
{
    CLEAR_BIT(PMU->WKUPA, 0xFFFFFFFF);
}


/**
 * @}
 */


/**
 * @brief  Enable the channel of module wakeup B
 * @rmtoll WKUPB          LL_WEC_ModuleWakeupBEn
 * @param  channel This parameter can be a combination of the following values:
 *         @arg @ref LL_WEC_WKUPB_GPIOA
 *         @arg @ref LL_WEC_WKUPB_GPIOB
 *         @arg @ref LL_WEC_WKUPB_GPIOC
 *         @arg @ref LL_WEC_WKUPB_GPIOD
 *         @arg @ref LL_WEC_WKUPB_GPIOE
 *         @arg @ref LL_WEC_WKUPB_GPIOF
 * @retval None
 */
__STATIC_INLINE void LL_WEC_EnableWakeupB(uint32_t channel)
{
    SET_BIT(PMU->WKUPB, channel);
}


__STATIC_INLINE void LL_WEC_DisableWakeupB(uint32_t channel)
{
    CLEAR_BIT(PMU->WKUPB, channel);
}


__STATIC_INLINE void LL_WEC_DisableAllWakeupB(void)
{
    CLEAR_BIT(PMU->WKUPB, 0xFFU);
}


/**
 * @}
 */


/**
 * @brief  Clear all wakeup record
 * @rmtoll LWKUPRCDCLR          LL_WEC_WakeupRecordClear
 * @retval None
 */
__STATIC_INLINE void LL_WEC_ClearAllRecordWakeup(void)
{
    SET_BIT(PMU->LWKUPRCDCLR, PMU_RCDCLR_WAKEUP);
}


/**
 * @}
 */


/**
 * @brief  get wakeup record A
 * @rmtoll LWKUPRCDA          LL_WEC_GetWakeupRecordA
 * @param This parameter can be a combination of the following values:
 *         @arg @ref LL_WEC_WKUPA_LPTIMER_INT
 *         @arg @ref LL_WEC_WKUPA_STIMER4_INT
 *         @arg @ref LL_WEC_WKUPA_STIMER5_INT
 *         @arg @ref LL_WEC_WKUPA_STIMER0_INT
 *         @arg @ref LL_WEC_WKUPA_STIMER1_INT
 *         @arg @ref LL_WEC_WKUPA_STIMER2_INT
 *         @arg @ref LL_WEC_WKUPA_STIMER3_INT
 *         @arg @ref LL_WEC_WKUPA_LLWU
 *         @arg @ref LL_WEC_WKUPA_UART0_INT
 *         @arg @ref LL_WEC_WKUPA_UART1_INT
 *         @arg @ref LL_WEC_WKUPA_UART2_INT
 *         @arg @ref LL_WEC_WKUPA_UART3_INT
 *         @arg @ref LL_WEC_WKUPA_CT0_INT
 *         @arg @ref LL_WEC_WKUPA_CT1_INT
 *         @arg @ref LL_WEC_WKUPA_ALM_INT
 *         @arg @ref LL_WEC_WKUPA_TAMPER_ALM_INT
 *         @arg @ref LL_WEC_WKUPA_RTC_ALM_INT
 *         @arg @ref LL_WEC_WKUPA_RTC_1S_INT
 * @retval 0 or !0
 */
__STATIC_INLINE uint32_t LL_WEC_GetRecord_WakeupA(uint32_t channel)
{
    return (READ_BIT(PMU->LWKUPRCDA, channel));
}


/**
 * @}
 */


/**
 * @brief  get wakeup record B
 * @rmtoll LWKUPRCDB          LL_WEC_GetWakeupRecordB
 * @param This parameter can be a combination of the following values:
 *         @arg @ref LL_WEC_WKUPB_GPIOA
 *         @arg @ref LL_WEC_WKUPB_GPIOB
 *         @arg @ref LL_WEC_WKUPB_GPIOC
 *         @arg @ref LL_WEC_WKUPB_GPIOD
 *         @arg @ref LL_WEC_WKUPB_GPIOE
 *         @arg @ref LL_WEC_WKUPB_GPIOF
 * @retval 0 or !0
 */
__STATIC_INLINE uint32_t LL_WEC_GetRecord_WakeupB(uint32_t channel)
{
    return (READ_BIT(PMU->LWKUPRCDB, channel));
}


/**
 * @}
 */


/**
 * @brief  get wakeup record C
 * @rmtoll LWKUPRCDC          LL_WEC_GetWakeupRecordC
 * @param This parameter can be a combination of the following values:
 *         @arg @ref LL_WEC_WKUPC_LLWU
 *         @arg @ref LL_WEC_WKUPC_CMP
 *         @arg @ref LL_WEC_WKUPC_ADC
 *         @arg @ref LL_WEC_WKUPC_DMA
 * @retval 0 or !0
 */
__STATIC_INLINE uint32_t LL_WEC_GetRecord_WakeupC(uint32_t channel)
{
    return (READ_BIT(PMU->LWKUPRCDC, channel));
}


/**
 * @}
 */


/**
 * @brief  get PM Int
 * @rmtoll INT          LL_PMU_GetPMInt
 * @param None
 * @retval 0 or !0
 */
__STATIC_INLINE uint32_t LL_PMU_IsActiveFlagPM_IT(void)
{
    return (READ_BIT(PMU->INT, PMU_INT_PM) == PMU_INT_PM);
}


/**
 * @}
 */


/**
 * @brief  enable iload
 * @rmtoll          LL_PMU_EnableiLoad
 * @param None
 * @retval None
 */
__STATIC_INLINE void LL_PMU_EnableiLoad(void)
{
    WRITE_REG(PMU->LDO12ILSWCFG, 0x03);     //ldo12 iload
    WRITE_REG(PMU->LDO16ILSWCFG, 0x01);     //ldo16 iload
}


/**
 * @}
 */


/**
 * @brief  enable iload
 * @rmtoll          LL_PMU_EnableiLoad
 * @param None
 * @retval None
 */
__STATIC_INLINE void LL_PMU_DisableiLoad(void)
{
    WRITE_REG(PMU->LDO12ILSWCFG, 0x00);     //ldo12 iload
    WRITE_REG(PMU->LDO16ILSWCFG, 0x00);     //ldo16 iload
}


/**
 * @}
 */


/**
 * @brief  enable iload hardware auto control
 * @rmtoll          LL_PMU_EnableiLoadAutoCTL
 * @param None
 * @retval None
 */
__STATIC_INLINE void LL_PMU_EnableiLoadAutoCTL(void)
{
    WRITE_REG(PMU->LDO12PDCFG1, 0x006403F1);
}


/**
 * @}
 */


/**
 * @brief  disable iload hardware auto control
 * @rmtoll          LL_PMU_DisableiLoadAutoCTL
 * @param None
 * @retval None
 */
__STATIC_INLINE void LL_PMU_DisableiLoadAutoCTL(void)
{
    WRITE_REG(PMU->LDO12PDCFG1, 0x00640000);
}


/**
 * @}
 */

void LL_PMU_Init(LL_PMU_InitTypeDef *PMU_InitStruct);


void LL_PMU_EnterLowpowerMode(void);


#ifdef __cplusplus
}
#endif
#endif

/************************ (C) COPYRIGHT FudanMicroelectronics *****END OF FILE****/

