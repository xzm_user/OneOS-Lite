/**
 ******************************************************************************
 * @file    fm15f3xx_ll_cmp.h
 * @author  SRG
 * @version V1.0.0
 * @date    2020-03-17
 * @brief
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2020 FudanMicroelectronics.
 * All rights reserved.</center></h2>
 *
 ******************************************************************************
 */

#ifndef FM15F3XX_LL_CMP_H
#define FM15F3XX_LL_CMP_H
#ifdef __cplusplus
extern "C" {
#endif

#include "fm15f3xx.h"


/** @defgroup CMP_LL_INIT CMP Exported Init structure
 * @{
 */
typedef struct {
    uint32_t SpeedMode;         /*!< Specifies the speed mode.
                                     This parameter can be a value of @ref CMP_LL_SPEEDMODE
                                     This feature can be modified afterwards using unitary function @ref LL_CMP_SetSpeedMode(). */
    uint32_t HysterLevel;       /*!< Specifies the Hyster Level
                                     This parameter can be a value of @ref CMP_LL_HYSTERLEVEL
                                     This feature can be modified afterwards using unitary function @ref LL_CMP_SetHysterLevel(). */
    uint32_t PositiveInput;     /*!< Specifies the Positive Input
                                     This parameter can be a value of @ref CMP_LL_POSITIVE
                                     This feature can be modified afterwards using unitary function @ref LL_CMP_SetInputSourse(). */
    uint32_t NegativeInput;     /*!< Specifies the Negative Input
                                     This parameter can be a value of @ref CMP_LL_NEGATIVE
                                     This feature can be modified afterwards using unitary function @ref LL_CMP_SetInputSourse(). */
    uint32_t SixBitDACEn;       /*!< Specifies the SixBit DAC enable
                                     This parameter can be a value of @ref CMP_LL_6DACEN
                                     This feature can be modified afterwards using unitary function @ref LL_CMP_SixBitDACEnable(). */
    uint32_t SixBitDACVref;     /*!< Specifies the SixBit DAC vref select
                                     This parameter can be a value of @ref CMP_LL_DAC_VREFSOURCE
                                     This feature can be modified afterwards using unitary function @ref LL_CMP_SetSixBitDACVref(). */
    uint32_t TrigMode;          /*!< Specifies the Tigger mode
                                     This parameter can be a value of @ref CMP_LL_TRIGCFG_MODE
                                     This feature can be modified afterwards using unitary function @ref LL_CMP_SetTriggerMode(). */
    uint32_t TrigSourceDir;     /*!< Specifies the Tigger Source direction
                                     This parameter can be a value of @ref CMP_LL_TRIGINPUTDIR
                                     This feature can be modified afterwards using unitary function @ref LL_CMP_SetTriggerSourceDir(). */
    uint32_t TrigSource;        /*!< Specifies the Tigger Source
                                     This parameter can be a value of @ref CMP_LL_TRIGSOURSE
                                     This feature can be modified afterwards using unitary function @ref LL_CMP_SetTriggerSource(). */
    uint32_t TrigEdge;          /*!< Specifies the Tigger edge
                                     This parameter can be a value of @ref CMP_LL_TRIGEDGE
                                     This feature can be modified afterwards using unitary function @ref LL_CMP_SetTriggerEdge(). */
    uint32_t WindowEn;          /*!< Specifies the window or not
                                     This parameter can be a value of @ref CMP_LL_WINDOW
                                     This feature can be modified afterwards using unitary function @ref LL_CMP_WindowEnable() or LL_CMP_WindowDisable(). */
    uint32_t WindowSource;      /*!< Specifies the Window Source
                                     This parameter can be a value of @ref CMP_LL_WINSOURCE
                                     This feature can be modified afterwards using unitary function @ref LL_CMP_SetWindowSource(). */
    uint32_t FilterEn;          /*!< Specifies the filter or not
                                     This parameter can be a value of @ref CMP_LL_FILTER
                                     This feature can be modified afterwards using unitary function @ref LL_CMP_FilterEnable() or LL_CMP_FilterDisable(). */
    uint32_t FilterCnt;         /*!< Specifies the filter count
                                     This parameter can be a value of @ref CMP_LL_FILTERCNT
                                     This feature can be modified afterwards using unitary function @ref LL_CMP_SetFilterCnt()*/
    uint32_t DMAorINT;          /*!< Specifies the DMA or INT
                                     This parameter can be a value of @ref CMP_LL_RCDSEL
                                     This feature can be modified afterwards using unitary function @ref LL_CMP_RecordIntEnable() and LL_CMP_SetINTOrDMAReq */
    uint32_t RecordTrigMode;    /*!< Specifies the record trigger mode
                                     This parameter can be a value of @ref CMP_LL_RCDMODE
                                     This feature can be modified afterwards using unitary function @ref LL_CMP_SetRcdTrigMode */
} LL_CMP_InitTypeDef;


/**
 * @}
 */


/** @defgroup CMP_LL_CMPEN  disable or enable CMP module
 * @{
 */
#define LL_CMP_DISABLE  (0x00000000U)                                       /*!< Select disable CMP module */
#define LL_CMP_ENABLE   CMP_CTRL_CMPEN                                      /*!< Select enable CMP module*/


/**
 * @}
 */


/** @defgroup CMP_LL_SPEEDMODE  select high or low speed mode
 * @{
 */
#define LL_CMP_SPEED_LOW    (0x00000000U)                                   /*!< Select CMP low speed mode    */
#define LL_CMP_SPEED_HIGH   CMP_CTRL_HPEN                                   /*!< Select CMP high speed mode   */


/**
 * @}
 */


/** @defgroup CMP_LL_HYSTERLEVEL config hyster level from level0 to level3
 * @{
 */
#define LL_CMP_HYSTER_LEVEL0    (0x00000000U << CMP_CTRL_HYSTER_Pos)        /*!< config hyster level0 */
#define LL_CMP_HYSTER_LEVEL1    (0x00000001U << CMP_CTRL_HYSTER_Pos)        /*!< config hyster level1 */
#define LL_CMP_HYSTER_LEVEL2    (0x00000002U << CMP_CTRL_HYSTER_Pos)        /*!< config hyster level2 */
#define LL_CMP_HYSTER_LEVEL3    (0x00000003U << CMP_CTRL_HYSTER_Pos)        /*!< config hyster level3 */


/**
 * @}
 */


/** @defgroup CMP_LL_POSITIVE select positive channel source
 * @{
 */
#define LL_CMP_POSITIVE_CMP_IN0     (0x00000000U << CMP_INSEL_INPSEL_Pos)   /*!< config positive channel as CMP_IN0 */
#define LL_CMP_POSITIVE_CMP_IN1     (0x00000001U << CMP_INSEL_INPSEL_Pos)   /*!< config positive channel as CMP_IN1 */
#define LL_CMP_POSITIVE_CMP_IN2     (0x00000002U << CMP_INSEL_INPSEL_Pos)   /*!< config positive channel as CMP_IN2 */
#define LL_CMP_POSITIVE_CMP_IN3     (0x00000003U << CMP_INSEL_INPSEL_Pos)   /*!< config positive channel as CMP_IN3 */
#define LL_CMP_POSITIVE_12BITDAC    (0x00000004U << CMP_INSEL_INPSEL_Pos)   /*!< config positive channel as CMP_IN4/12 bit DAC */
#define LL_CMP_POSITIVE_VREF12      (0x00000006U << CMP_INSEL_INPSEL_Pos)   /*!< config positive channel as bandgap */
#define LL_CMP_POSITIVE_6BITDAC     (0x00000007U << CMP_INSEL_INPSEL_Pos)   /*!< config positive channel as 6 bit DAC */


/**
 * @}
 */


/** @defgroup CMP_LL_NEGATIVE  select negative channel source
 * @{
 */
#define LL_CMP_NEGATIVE_CMP_IN0     (0x00000000U << CMP_INSEL_INNSEL_Pos)   /*!< config negative channel as CMP_IN0 */
#define LL_CMP_NEGATIVE_CMP_IN1     (0x00000001U << CMP_INSEL_INNSEL_Pos)   /*!< config negative channel as CMP_IN1 */
#define LL_CMP_NEGATIVE_CMP_IN2     (0x00000002U << CMP_INSEL_INNSEL_Pos)   /*!< config negative channel as CMP_IN2 */
#define LL_CMP_NEGATIVE_CMP_IN3     (0x00000003U << CMP_INSEL_INNSEL_Pos)   /*!< config negative channel as CMP_IN3 */
#define LL_CMP_NEGATIVE_12BITDAC    (0x00000004U << CMP_INSEL_INNSEL_Pos)   /*!< config negative channel as CMP_IN4/12 bit DAC */
#define LL_CMP_NEGATIVE_VREF12      (0x00000006U << CMP_INSEL_INNSEL_Pos)   /*!< config negative channel as bandgap */
#define LL_CMP_NEGATIVE_6BITDAC     (0x00000007U << CMP_INSEL_INNSEL_Pos)   /*!< config negative channel as 6 bit DAC */


/**
 * @}
 */


/** @defgroup CMP_LL_6DACEN   disable or enable SixBit DAC
 * @{
 */
#define LL_CMP_6BITDAC_DISABLE  (0x00000000U)                               /*!< disable SixBit DAC */
#define LL_CMP_6BITDAC_ENABLE   CMP_DAC_DACEN                               /*!< ensable SixBit DAC */


/**
 * @}
 */


/** @defgroup CMP_LL_DAC_VREFSOURCE  vref select vref1.2V or vdd
 * @{
 */
#define LL_CMP_6BITDAC_VREF1P2  (0x00000000U)                               /*!< vin1 vref1.2V */
#define LL_CMP_6BITDAC_VREFVDD  CMP_DAC_VREFSEL                             /*!< vin2 vdd      */


/**
 * @}
 */


/** @defgroup CMP_LL_TRIGCFG_MODE  select real time mode or trigger mode
 * @{
 */
#define LL_CMP_TRIGMODE_REALTIME    (0x00000000U)                           /*!< select real time mode */
#define LL_CMP_TRIGMODE_TRIGGER     CMP_TRIGCFG_MODE                        /*!<select trigger mode    */


/**
 * @}
 */


/** @defgroup CMP_LL_TRIGINPUTDIR  config normal or inverse
 * @{
 */
#define LL_CMP_TRIGINPUT_NORMAL (0x00000000U)                               /*!< config trigsource as normal   */
#define LL_CMP_TRIGINPUT_INVERT CMP_TRIGCFG_INV                             /*!< config trigsource as inverse  */


/**
 * @}
 */


/** @defgroup CMP_LL_TRIGEDGE  config normal or inverse
 * @{
 */
#define LL_CMP_TRIGEDGE_HIGH        (0x00000000U << CMP_TRIGCFG_EDGE_Pos)   /*!< config trigedge as high level */
#define LL_CMP_TRIGEDGE_RISING      (0x00000001U << CMP_TRIGCFG_EDGE_Pos)   /*!< config trigedge as rising   */
#define LL_CMP_TRIGEDGE_FALLING     (0x00000002U << CMP_TRIGCFG_EDGE_Pos)   /*!< config trigedge as falling  */
#define LL_CMP_TRIGEDGE_BOTHEDGE    (0x00000003U << CMP_TRIGCFG_EDGE_Pos)   /*!< config trigedge as falling or  rising */


/**
 * @}
 */


/** @defgroup CMP_LL_TRIGSOURCE  select different source
 * @{
 */
#define LL_CMP_TRIGSRC_CMP_INx          (0x00000000U << CMP_TRIGCFG_SC_Pos) /*!< config trig source as CMP_INx        */
#define LL_CMP_TRIGSRC_CMP_RCD          (0x00000001U << CMP_TRIGCFG_SC_Pos) /*!< config trig source as CMP_RCD        */
#define LL_CMP_TRIGSRC_STIMER0          (0x00000002U << CMP_TRIGCFG_SC_Pos) /*!< config trig source as STIMER0        */
#define LL_CMP_TRIGSRC_STIMER1          (0x00000003U << CMP_TRIGCFG_SC_Pos) /*!< config trig source as STIMER1        */
#define LL_CMP_TRIGSRC_STIMER2          (0x00000004U << CMP_TRIGCFG_SC_Pos) /*!< config trig source as STIMER2        */
#define LL_CMP_TRIGSRC_STIMER3          (0x00000005U << CMP_TRIGCFG_SC_Pos) /*!< config trig source as STIMER3        */
#define LL_CMP_TRIGSRC_STIMER4          (0x00000006U << CMP_TRIGCFG_SC_Pos) /*!< config trig source as STIMER4        */
#define LL_CMP_TRIGSRC_STIMER5          (0x00000007U << CMP_TRIGCFG_SC_Pos) /*!< config trig source as STIMER5        */
#define LL_CMP_TRIGSRC_LPTIMER0         (0x00000008U << CMP_TRIGCFG_SC_Pos) /*!< config trig source as LPTIMER0       */
#define LL_CMP_TRIGSRC_RTCALARM_IT      (0x0000000BU << CMP_TRIGCFG_SC_Pos) /*!< config trig source as RTCALARM_IT    */
#define LL_CMP_TRIGSRC_TAMPERALARM_IT   (0x0000000CU << CMP_TRIGCFG_SC_Pos) /*!< config trig source as TAMPERALARM_IT */
#define LL_CMP_TRIGSRC_RTC1S_IT         (0x0000000DU << CMP_TRIGCFG_SC_Pos) /*!< config trig source as RTC1S_IT       */
#define LL_CMP_TRIGSRC_SOFTWARE         (0x0000000EU << CMP_TRIGCFG_SC_Pos) /*!< config trig source as SOFTWARE       */


/**
 * @}
 */


/** @defgroup CMP_LL_SWTRIG  config normal or inverse
 * @{
 */
#define LL_CMP_SWTRIG_NO    (0x00000000U)                                   /*!< no trigger pulse   */
#define LL_CMP_SWTRIG_PULSE CMP_SWTRIG_CTRL                                 /*!< produce a trigger pulse  */


/**
 * @}
 */


/** @defgroup CMP_LL_OUTCTRL output select normal or inverse
 * @{
 */
#define LL_CMP_OUTCTRL_NORMAL   (0x00000000U)                               /*!< CMP output normal   */
#define LL_CMP_OUTCTRL_INVERT   CMP_OUTCTRL_INV                             /*!< CMP output inverse  */


/**
 * @}
 */


/** @defgroup CMP_LL_WINDOW  config disable or enable
 * @{
 */
#define LL_CMP_WINDOW_DISABLE   (0x00000000U)                               /*!< CMP window disable  */
#define LL_CMP_WINDOW_ENABLE    CMP_OUTCTRL_WINEN                           /*!< CMP window enable   */


/**
 * @}
 */


/** @defgroup CMP_LL_WINSOURCE  config disable or enable
 * @{
 */
#define LL_CMP_WINSOURCE_STIMER0    (0x00000000U << CMP_OUTCTRL_WINSC_Pos)  /*!< config window sourse as STIMER0  */
#define LL_CMP_WINSOURCE_STIMER1    (0x00000001U << CMP_OUTCTRL_WINSC_Pos)  /*!< config window sourse as STIMER1  */
#define LL_CMP_WINSOURCE_STIMER2    (0x00000002U << CMP_OUTCTRL_WINSC_Pos)  /*!< config window sourse as STIMER2  */
#define LL_CMP_WINSOURCE_STIMER4    (0x00000003U << CMP_OUTCTRL_WINSC_Pos)  /*!< config window sourse as STIMER4 */


/**
 * @}
 */


/** @defgroup CMP_LL_FILTER  config disable or enable
 * @{
 */
#define LL_CMP_FILTER_DISABLE   (0x00000000U)                               /*!< CMP filter disable  */
#define LL_CMP_FILTER_ENABLE    CMP_OUTCTRL_FILTEN                          /*!< CMP filter enable   */


/**
 * @}
 */


/** @defgroup CMP_LL_FILTERCNT  config disable or enable
 * @{
 */
#define LL_CMP_FILTERCNT_1CLK   (0x00000000U << CMP_OUTCTRL_FILTCNT_Pos)    /*!< config filter cnt as 1 bus clk  */
#define LL_CMP_FILTERCNT_2CLK   (0x00000001U << CMP_OUTCTRL_FILTCNT_Pos)    /*!< config filter cnt as 2 bus clk  */
#define LL_CMP_FILTERCNT_3CLK   (0x00000002U << CMP_OUTCTRL_FILTCNT_Pos)    /*!< config filter cnt as 3 bus clk  */
#define LL_CMP_FILTERCNT_4CLK   (0x00000003U << CMP_OUTCTRL_FILTCNT_Pos)    /*!< config filter cnt as 4 bus clk  */
#define LL_CMP_FILTERCNT_5CLK   (0x00000004U << CMP_OUTCTRL_FILTCNT_Pos)    /*!< config filter cnt as 5 bus clk  */
#define LL_CMP_FILTERCNT_6CLK   (0x00000005U << CMP_OUTCTRL_FILTCNT_Pos)    /*!< config filter cnt as 6 bus clk  */
#define LL_CMP_FILTERCNT_7CLK   (0x00000006U << CMP_OUTCTRL_FILTCNT_Pos)    /*!< config filter cnt as 7 bus clk  */
#define LL_CMP_FILTERCNT_8CLK   (0x00000007U << CMP_OUTCTRL_FILTCNT_Pos)    /*!< config filter cnt as 8 bus clk  */
#define LL_CMP_FILTERCNT_9CLK   (0x00000008U << CMP_OUTCTRL_FILTCNT_Pos)    /*!< config filter cnt as 9 bus clk  */
#define LL_CMP_FILTERCNT_10CLK  (0x00000009U << CMP_OUTCTRL_FILTCNT_Pos)    /*!< config filter cnt as 10 bus clk  */
#define LL_CMP_FILTERCNT_11CLK  (0x0000000AU << CMP_OUTCTRL_FILTCNT_Pos)    /*!< config filter cnt as 11 bus clk  */
#define LL_CMP_FILTERCNT_12CLK  (0x0000000BU << CMP_OUTCTRL_FILTCNT_Pos)    /*!< config filter cnt as 12 bus clk  */
#define LL_CMP_FILTERCNT_13CLK  (0x0000000CU << CMP_OUTCTRL_FILTCNT_Pos)    /*!< config filter cnt as 13 bus clk  */
#define LL_CMP_FILTERCNT_14CLK  (0x0000000DU << CMP_OUTCTRL_FILTCNT_Pos)    /*!< config filter cnt as 14 bus clk  */
#define LL_CMP_FILTERCNT_15CLK  (0x0000000EU << CMP_OUTCTRL_FILTCNT_Pos)    /*!< config filter cnt as 15 bus clk  */
#define LL_CMP_FILTERCNT_16CLK  (0x0000000FU << CMP_OUTCTRL_FILTCNT_Pos)    /*!< config filter cnt as 16 bus clk  */


/**
 * @}
 */


/** @defgroup CMP_LL_RCDINT enable cmp result output event produce int and record or a DMA request
 * @{
 */
#define LL_CMP_RCDINT_DISABLE   (0x00000000U)                               /*!< CMP record INT or DMA request disable  */
#define LL_CMP_RCDINT_ENABLE    CMP_INTEN_INTDMAEN                          /*!< CMP record INT request enable   */
#define LL_CMP_RCDDMA_ENABLE    (CMP_INTEN_INTDMAEN | CMP_INTEN_DMASEL)     /*!< CMP record DMA request enable   */


/**
 * @}
 */


/** @defgroup CMP_LL_RCDSEL  select cmp result output event produce int and record or a DMA request
 * @{
 */
#define LL_CMP_REQSEL_INT   CMP_INTEN_INTDMAEN                              /*!< record produce INT   */
#define LL_CMP_REQSEL_DMA   (CMP_INTEN_INTDMAEN | CMP_INTEN_DMASEL)         /*!< record produce DMA   */
#define LL_CMP_REQSEL_NONE  0x0U


/**
 * @}
 */


/** @defgroup CMP_LL_RCDMODE  selet different mode to config CMP result to produce record
 * @{
 */
#define LL_CMP_RCDMODE_HIGH     (0x00000000U << CMP_INTEN_MODE_Pos) /*!< CMP result is high to produce record      */
#define LL_CMP_RCDMODE_RISING   (0x00000001U << CMP_INTEN_MODE_Pos) /*!< CMP result is rising to produce record    */
#define LL_CMP_RCDMODE_FALLING  (0x00000002U << CMP_INTEN_MODE_Pos) /*!< CMP result is falling to produce record   */
#define LL_CMP_RCDMODE_BOTHEDGE (0x00000003U << CMP_INTEN_MODE_Pos) /*!< CMP result is both edge to produce record */


/**
 * @}
 */


/** @defgroup CMP_LL_RCDCLEAR   write 1 to clear record
 * @{
 */
#define LL_CMP_RCDCLEAR_NO      (0x00000000U)                       /*!< no write 1    */
#define LL_CMP_RCDCLEAR_PULSE   CMP_RCDCLR_CLEAR                    /*!< write 1 to clear   */


/**
 * @}
 */


/* Exported functions --------------------------------------------------------*/


/**
 * @brief  CMP  Enable
 * @rmtoll CTRL     CMPEN          LL_CMP_Enable
 * @param  CMPx CMP Instance
 * @retval None
 */
__STATIC_INLINE void LL_CMP_Enable(CMP_TypeDef *CMPx)
{
    SET_BIT(CMPx->CTRL, CMP_CTRL_CMPEN_Msk);
}


/**
 * @brief  CMP  disable
 * @rmtoll CTRL     CMPEN       LL_CMP_Disable
 * @param  CMPx CMP Instance
 * @retval None
 */
__STATIC_INLINE void LL_CMP_Disable(CMP_TypeDef *CMPx)
{
    CLEAR_BIT(CMPx->CTRL, CMP_CTRL_CMPEN_Msk);
}


/**
 * @brief  Indicate if CMP is enabled
 * @rmtoll CTRL     CMPEN       LL_CMP_IsEnable
 * @param  CMPx CMP Instance
 * @retval State of bit (1 0r 0)
 */
__STATIC_INLINE uint32_t LL_CMP_IsEnable(CMP_TypeDef *CMPx)
{
    return ((uint32_t)(READ_BIT(CMPx->CTRL, CMP_CTRL_CMPEN_Msk)));
}


/**
 * @brief  CMP  config speed mode
 * @rmtoll CTRL     CMP_CTRL_HPEN        LL_CMP_SetSpeedMode
 * @param  CMPx CMP Instance
 * @param  SpeedMode This parameter can be one of the following values:
 *         @arg @ref LL_CMP_SPEED_LOW
 *         @arg @ref LL_CMP_SPEED_HIGH
 * @retval None
 */
__STATIC_INLINE void LL_CMP_SetSpeedMode(CMP_TypeDef *CMPx, uint32_t SpeedMode)
{
    MODIFY_REG(CMPx->CTRL, CMP_CTRL_HPEN_Msk, SpeedMode);
}


/**
 * @brief  Return SpeedMode states of CMP
 * @rmtoll CTRL     CMP_CTRL_HPEN        LL_CMP_GetSpeedMode
 * @param  CMPx CMP Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_CMP_SPEED_LOW
 *         @arg @ref LL_CMP_SPEED_HIGH

 */
__STATIC_INLINE uint32_t LL_CMP_GetSpeedMode(CMP_TypeDef *CMPx)
{
    return ((uint32_t)(READ_BIT(CMPx->CTRL, CMP_CTRL_HPEN_Msk)));
}


/**
 * @brief  CMP  config HysterLevel
 * @rmtoll CTRL     CMP_CTRL_HYSTER       LL_CMP_SetHysterLevel
 * @param  CMPx CMP Instance
 * @param  HysterLevel This parameter can be one of the following values:
 *         @arg @ref LL_CMP_HYSTER_LEVEL0
 *         @arg @ref LL_CMP_HYSTER_LEVEL1
 *         @arg @ref LL_CMP_HYSTER_LEVEL2
 *         @arg @ref LL_CMP_HYSTER_LEVEL3
 * @retval None
 */
__STATIC_INLINE void LL_CMP_SetHysterLevel(CMP_TypeDef *CMPx, uint32_t HysterLevel)
{
    MODIFY_REG(CMPx->CTRL, CMP_CTRL_HYSTER_Msk, HysterLevel);
}


/**
 * @brief  Return HysterLevel config of CMP
 * @rmtoll CTRL     CMP_CTRL_HYSTER       LL_CMP_GetHysterLevel
 * @param  CMPx CMP Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_CMP_HYSTER_LEVEL0
 *         @arg @ref LL_CMP_HYSTER_LEVEL1
 *         @arg @ref LL_CMP_HYSTER_LEVEL2
 *         @arg @ref LL_CMP_HYSTER_LEVEL3
 */
__STATIC_INLINE uint32_t LL_CMP_GetHysterLevel(CMP_TypeDef *CMPx)
{
    return ((uint32_t)(READ_BIT(CMPx->CTRL, CMP_CTRL_HYSTER_Msk)));
}


/**
 * @brief  CMP  config positive input and negative input
 * @rmtoll IN       CMP_INSEL_INPSEL       LL_CMP_ConfigInputSrc
 * @param  CMPx CMP Instance
 * @param  PositiveInput This parameter can be one of the following values:
 *         @arg @ref LL_CMP_POSITIVE_CMP_IN0
 *         @arg @ref LL_CMP_POSITIVE_CMP_IN1
 *         @arg @ref LL_CMP_POSITIVE_CMP_IN2
 *         @arg @ref LL_CMP_POSITIVE_CMP_IN3
 *         @arg @ref LL_CMP_POSITIVE_12BitDAC
 *         @arg @ref LL_CMP_POSITIVE_VREF12
 *         @arg @ref LL_CMP_POSITIVE_6BitDAC
 * @param  NegativeInput This parameter can be one of the following values:
 *         @arg @ref LL_CMP_NEGATIVE_CMP_IN0
 *         @arg @ref LL_CMP_NEGATIVE_CMP_IN1
 *         @arg @ref LL_CMP_NEGATIVE_CMP_IN2
 *         @arg @ref LL_CMP_NEGATIVE_CMP_IN3
 *         @arg @ref LL_CMP_NEGATIVE_12BitDAC
 *         @arg @ref LL_CMP_NEGATIVE_VREF12
 *         @arg @ref LL_CMP_NEGATIVE_6BitDAC
 * @retval None
 */
__STATIC_INLINE void LL_CMP_ConfigInputSrc(CMP_TypeDef *CMPx, uint32_t PositiveInput, uint32_t NegativeInput)
{
    MODIFY_REG(CMPx->IN, CMP_INSEL_INPSEL_Msk | CMP_INSEL_INNSEL_Msk, PositiveInput | NegativeInput);
}


/**
 * @brief  CMP  return the positive input
 * @rmtoll IN       CMP_INSEL_INPSEL       LL_CMP_GetPositiveInputSrc
 * @param  CMPx CMP Instance
 * @retval returnd value can be one of the following values:
 *         @arg @ref LL_CMP_POSITIVE_CMP_IN0
 *         @arg @ref LL_CMP_POSITIVE_CMP_IN1
 *         @arg @ref LL_CMP_POSITIVE_CMP_IN2
 *         @arg @ref LL_CMP_POSITIVE_CMP_IN3
 *         @arg @ref LL_CMP_POSITIVE_12BitDAC
 *         @arg @ref LL_CMP_POSITIVE_VREF12
 *         @arg @ref LL_CMP_POSITIVE_6BitDAC
 */
__STATIC_INLINE uint32_t LL_CMP_GetPositiveInputSrc(CMP_TypeDef *CMPx)
{
    return ((uint32_t)(READ_BIT(CMPx->IN, CMP_INSEL_INPSEL_Msk)));
}


/**
 * @brief  CMP  return  the negative input
 * @rmtoll IN       CMP_INSEL_INNSEL       LL_CMP_GetNegativeInputSrc
 * @param  CMPx CMP Instance
 * @retval returnd value can be one of the following values:
 *         @arg @ref LL_CMP_NEGATIVE_CMP_IN0
 *         @arg @ref LL_CMP_NEGATIVE_CMP_IN1
 *         @arg @ref LL_CMP_NEGATIVE_CMP_IN2
 *         @arg @ref LL_CMP_NEGATIVE_CMP_IN3
 *         @arg @ref LL_CMP_NEGATIVE_12BITDAC
 *         @arg @ref LL_CMP_NEGATIVE_VREF12
 *         @arg @ref LL_CMP_NEGATIVE_6BITDAC
 */
__STATIC_INLINE uint32_t LL_CMP_GetNegativeInputSrc(CMP_TypeDef *CMPx)
{
    return ((uint32_t)(READ_BIT(CMPx->IN, CMP_INSEL_INNSEL_Msk)));
}


/**
 * @brief  CMP SixBit DAC Enable
 * @rmtoll DAC      CMP_DAC_DACEN      LL_CMP_Enable6BitDAC
 * @param  CMPx CMP Instance
 * @retval None
 */
__STATIC_INLINE void LL_CMP_Enable6BitDAC(CMP_TypeDef *CMPx)
{
    SET_BIT(CMPx->DAC, CMP_DAC_DACEN_Msk);
}


/**
 * @brief  CMP SixBit DAC disable
 * @rmtoll DAC     CMP_DAC_DACEN       LL_CMP_Disable6BitDAC
 * @param  CMPx CMP Instance
 * @retval None
 */
__STATIC_INLINE void LL_CMP_Disable6BitDAC(CMP_TypeDef *CMPx)
{
    CLEAR_BIT(CMPx->DAC, CMP_DAC_DACEN_Msk);
}


/**
 * @brief  Indicate if SixBit DACis enabled
 * @rmtoll CTRL     CMP_DAC_DACEN       LL_CMP_IsEnableSixBitDAC
 * @param  CMPx CMP Instance
 * @retval State of bit (1 0r 0)
 */
__STATIC_INLINE uint32_t LL_CMP_IsEnable6BitDAC(CMP_TypeDef *CMPx)
{
    return ((uint32_t)(READ_BIT(CMPx->DAC, CMP_DAC_DACEN_Msk)));
}


/**
 * @brief  CMP SixBit DAC select Vref
 * @rmtoll DAC      CMP_DAC_VREFSEL      LL_CMP_Set6BitDACVref
 * @param  CMPx CMP Instance
 * @param  SixBitDACVrefSource This parameter can be one of the following values:
 *         @arg @ref LL_CMP_6BITDAC_VREF1P2
 *         @arg @ref LL_CMP_6BITDAC_VREFVDD
 * @retval None
 */
__STATIC_INLINE void LL_CMP_Set6BitDACVref(CMP_TypeDef *CMPx, uint32_t SixBitDACVrefSource)
{
    MODIFY_REG(CMPx->DAC, CMP_DAC_VREFSEL_Msk, SixBitDACVrefSource);
}


/**
 * @brief  Indicate if the Vref source 6Bit DAC selectd
 * @rmtoll CTRL     CMP_DAC_VREFSEL       LL_CMP_Get6BitDACVref
 * @param  CMPx CMP Instance
 * @retval return value can be one of the following values:
 *         @arg @ref LL_CMP_6BITDAC_VREF1P2
 *         @arg @ref LL_CMP_6BITDAC_VREFVDD
 */
__STATIC_INLINE uint32_t LL_CMP_Get6BitDACVref(CMP_TypeDef *CMPx)
{
    return ((uint32_t)(READ_BIT(CMPx->DAC, CMP_DAC_VREFSEL)));
}


/**
 * @brief  CMP SixBit DAC set Data
 * @rmtoll DACDATA      CMP_DACDATA_DATA      LL_CMP_Set6BitDACData
 * @param  CMPx CMP Instance
 * @param  SixBitDACData This parameter can be one of the following values:
 *         @arg @ref the value from 0 to 63
 * @retval None
 */
__STATIC_INLINE void LL_CMP_Set6BitDACData(CMP_TypeDef *CMPx, uint32_t SixBitDACData)
{
    MODIFY_REG(CMPx->DACDATA, CMP_DACDATA_DATA_Msk, SixBitDACData);
}


/**
 * @brief  Indicate if the Data of SixBit DAC set
 * @rmtoll DACDATA     CMP_DACDATA_DATA       LL_CMP_Get6BitDACData
 * @param  CMPx CMP Instance
 * @retval return value can be one of the following values:
 *         @arg @ref the value from 0 to 63
 */
__STATIC_INLINE uint32_t LL_CMP_Get6BitDACData(CMP_TypeDef *CMPx)
{
    return ((uint32_t)(READ_BIT(CMPx->DACDATA, CMP_DACDATA_DATA_Msk)));
}


/**
 * @brief  CMP set trigger mode
 * @rmtoll TRIG     CMP_TRIGCFG_MODE        LL_CMP_SetTriggerMode
 * @param  CMPx CMP Instance
 * @param  TriggerMode This parameter can be one of the following values:
 *         @arg @ref LL_CMP_TRIGMODE_REALTIME
 *         @arg @ref LL_CMP_TRIGMODE_TRIGGER
 * @retval None
 */
__STATIC_INLINE void LL_CMP_SetTrigMode(CMP_TypeDef *CMPx, uint32_t TriggerMode)
{
    MODIFY_REG(CMPx->TRIG, CMP_TRIGCFG_MODE_Msk, TriggerMode);
}


/**
 * @brief  Return the trigger mode is set
 * @rmtoll TRIG     CMP_TRIGCFG_MODE       LL_CMP_GetTriggerMode
 * @param  CMPx CMP Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_CMP_TRIGMODE_REALTIME
 *         @arg @ref LL_CMP_TRIGMODE_TRIGGER
 */
__STATIC_INLINE uint32_t LL_CMP_GetTrigMode(CMP_TypeDef *CMPx)
{
    return ((uint32_t)(READ_BIT(CMPx->TRIG, CMP_TRIGCFG_MODE_Msk)));
}


/**
 * @brief  CMP set the Polarity of trigger source input is normal or inverse
 * @rmtoll TRIG     CMP_TRIGCFG_INV        LL_CMP_SetTrigSrcInv
 * @param  CMPx CMP Instance
 * @param  TriggerMode This parameter can be one of the following values:
 *         @arg @ref LL_CMP_TRIGINPUT_NORMAL
 *         @arg @ref LL_CMP_TRIGINPUT_INVERT
 * @retval None
 */
__STATIC_INLINE void LL_CMP_SetTrigSrcInv(CMP_TypeDef *CMPx, uint32_t TriggerSourceDir)
{
    MODIFY_REG(CMPx->TRIG, CMP_TRIGCFG_INV_Msk, TriggerSourceDir);
}


/**
 * @brief  Return the trigger Source Polarity is set
 * @rmtoll TRIG     CMP_TRIGCFG_INV       LL_CMP_GetTrigSrcInv
 * @param  CMPx CMP Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_CMP_TRIGINPUT_NORMAL
 *         @arg @ref LL_CMP_TRIGINPUT_INVERT
 */
__STATIC_INLINE uint32_t LL_CMP_GetTrigSrcInv(CMP_TypeDef *CMPx)
{
    return ((uint32_t)(READ_BIT(CMPx->TRIG, CMP_TRIGCFG_INV_Msk)));
}


/**
 * @brief  CMP set  trigger edge
 * @rmtoll TRIG     CMP_TRIGCFG_EDGE        LL_CMP_SetTriggerEdge
 * @param  CMPx CMP Instance
 * @param  TriggerEdge This parameter can be one of the following values:
 *         @arg @ref LL_CMP_TRIGEDGE_HIGH
 *         @arg @ref LL_CMP_TRIGEDGE_RISING
 *         @arg @ref LL_CMP_TRIGEDGE_FALLING
 *         @arg @ref LL_CMP_TRIGEDGE_BOTHEDGE
 * @retval None
 */
__STATIC_INLINE void LL_CMP_SetTrigEdge(CMP_TypeDef *CMPx, uint32_t TriggerEdge)
{
    MODIFY_REG(CMPx->TRIG, CMP_TRIGCFG_EDGE_Msk, TriggerEdge);
}


/**
 * @brief  Return the trigger edge is set
 * @rmtoll TRIG     CMP_TRIGCFG_EDGE       LL_CMP_GetTriggerEdge
 * @param  CMPx CMP Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_CMP_TRIGEDGE_HIGH
 *         @arg @ref LL_CMP_TRIGEDGE_RISING
 *         @arg @ref LL_CMP_TRIGEDGE_FALLING
 *         @arg @ref LL_CMP_TRIGEDGE_BOTHEDGE
 */
__STATIC_INLINE uint32_t LL_CMP_GetTrigEdge(CMP_TypeDef *CMPx)
{
    return ((uint32_t)(READ_BIT(CMPx->TRIG, CMP_TRIGCFG_EDGE_Msk)));
}


/**
 * @brief  CMP select  trigger source
 * @rmtoll TRIG     CMP_TRIGCFG_SC        LL_CMP_SetTriggerSource
 * @param  CMPx CMP Instance
 * @param  TriggerSource This parameter can be one of the following values:
 *         @arg @ref LL_CMP_TRIGSRC_CMP_INx
 *         @arg @ref LL_CMP_TRIGSRC_CMP_RCD
 *         @arg @ref LL_CMP_TRIGSRC_STIMER0
 *         @arg @ref LL_CMP_TRIGSRC_STIMER1
 *         @arg @ref LL_CMP_TRIGSRC_STIMER2
 *         @arg @ref LL_CMP_TRIGSRC_STIMER3
 *         @arg @ref LL_CMP_TRIGSRC_STIMER4
 *         @arg @ref LL_CMP_TRIGSRC_STIMER5
 *         @arg @ref LL_CMP_TRIGSRC_LPTIMER0
 *         @arg @ref LL_CMP_TRIGSRC_RTCALARM_IT
 *         @arg @ref LL_CMP_TRIGSRC_TAMPERALARM_IT
 *         @arg @ref LL_CMP_TRIGSRC_RTC1S_IT
 *         @arg @ref LL_CMP_TRIGSRC_SOFTWARE
 */
__STATIC_INLINE void LL_CMP_SetTrigSrc(CMP_TypeDef *CMPx, uint32_t TriggerSource)
{
    MODIFY_REG(CMPx->TRIG, CMP_TRIGCFG_SC_Msk, TriggerSource);
}


/**
 * @brief  Return the trigger source is select
 * @rmtoll TRIG     CMP_TRIGCFG_SC       LL_CMP_GetTriggerSource
 * @param  CMPx CMP Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_CMP_TRIGSRC_CMP_INx
 *         @arg @ref LL_CMP_TRIGSRC_CMP_RCD
 *         @arg @ref LL_CMP_TRIGSRC_STIMER0
 *         @arg @ref LL_CMP_TRIGSRC_STIMER1
 *         @arg @ref LL_CMP_TRIGSRC_STIMER2
 *         @arg @ref LL_CMP_TRIGSRC_STIMER3
 *         @arg @ref LL_CMP_TRIGSRC_STIMER4
 *         @arg @ref LL_CMP_TRIGSRC_STIMER5
 *         @arg @ref LL_CMP_TRIGSRC_LPTIMER0
 *         @arg @ref LL_CMP_TRIGSRC_RTCALARM_IT
 *         @arg @ref LL_CMP_TRIGSRC_TAMPERALARM_IT
 *         @arg @ref LL_CMP_TRIGSRC_RTC1S_IT
 *         @arg @ref LL_CMP_TRIGSRC_SOFTWARE
 */
__STATIC_INLINE uint32_t LL_CMP_GetTrigSrc(CMP_TypeDef *CMPx)
{
    return ((uint32_t)(READ_BIT(CMPx->TRIG, CMP_TRIGCFG_SC_Msk)));
}


/**
 * @brief  CMP set software trigger pulse
 * @rmtoll SWTRIG     CMP_SWTRIG_CTRL        LL_CMP_SoftTrig
 * @param  CMPx CMP Instance
 * @retval None
 */
__STATIC_INLINE void LL_CMP_SoftTrig(CMP_TypeDef *CMPx)
{
    SET_BIT(CMPx->SWTRIG, CMP_SWTRIG_CTRL);
}


/**
 * @brief  CMP set the Polarity of CMP analog out is normal or inverse
 * @rmtoll OUT     CMP_OUTCTRL_INV        LL_CMP_SetAnaOutInv
 * @param  CMPx CMP Instance
 * @param  TriggerMode This parameter can be one of the following values:
 *         @arg @ref LL_CMP_OUTCTRL_NORMAL
 *         @arg @ref LL_CMP_OUTCTRL_INVERT
 * @retval None
 */
__STATIC_INLINE void LL_CMP_SetAnaOutInv(CMP_TypeDef *CMPx, uint32_t AnaCMPOutDir)
{
    MODIFY_REG(CMPx->OUT, CMP_TRIGCFG_INV_Msk, AnaCMPOutDir);
}


/**
 * @brief  Return the Polarity of CMP analog out is normal or inverse
 * @rmtoll OUT     CMP_OUTCTRL_INV       LL_CMP_GetAnaOutInv
 * @param  CMPx CMP Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_CMP_OUTCTRL_NORMAL
 *         @arg @ref LL_CMP_OUTCTRL_INVERT
 */
__STATIC_INLINE uint32_t LL_CMP_GetAnaOutInv(CMP_TypeDef *CMPx)
{
    return ((uint32_t)(READ_BIT(CMPx->OUT, CMP_OUTCTRL_INV_Msk)));
}


/**
 * @brief  CMP window is Enable
 * @rmtoll OUT     CMP_OUTCTRL_WINEN     LL_CMP_WindowEnable
 * @param  CMPx CMP Instance
 * @retval None
 */
__STATIC_INLINE void LL_CMP_EnableWindow(CMP_TypeDef *CMPx)
{
    SET_BIT(CMPx->OUT, CMP_OUTCTRL_WINEN_Msk);
}


/**
 * @brief  CMP window is disable
 * @rmtoll OUT     CMP_OUTCTRL_WINEN       LL_CMP_WindowDisable
 * @param  CMPx CMP Instance
 * @retval None
 */
__STATIC_INLINE void LL_CMP_DisableWindow(CMP_TypeDef *CMPx)
{
    CLEAR_BIT(CMPx->OUT, CMP_OUTCTRL_WINEN_Msk);
}


/**
 * @brief  Indicate if CMP window is enabled
 * @rmtoll OUT     CMP_OUTCTRL_WINEN       LL_CMP_IsWindowEnabled
 * @param  CMPx CMP Instance
 * @retval Returned value can be one of the following values:
 *       @arg @ref LL_CMP_WINDOW_DISABLE
 *         @arg @ref LL_CMP_WINDOW_ENABLE
 */
__STATIC_INLINE uint32_t LL_CMP_IsEnableWindow(CMP_TypeDef *CMPx)
{
    return ((uint32_t)(READ_BIT(CMPx->OUT, CMP_OUTCTRL_WINEN_Msk)));
}


/**
 * @brief  CMP set the window source
 * @rmtoll OUT     CMP_OUTCTRL_WINSC        LL_CMP_SetWindowSource
 * @param  CMPx CMP Instance
 * @param  WindowSource This parameter can be one of the following values:
 *         @arg @ref LL_CMP_WINSOURCE_STIMER0
 *         @arg @ref LL_CMP_WINSOURCE_STIMER1
 *         @arg @ref LL_CMP_WINSOURCE_STIMER2
 *         @arg @ref LL_CMP_WINSOURCE_STIMER4
 * @retval None
 */
__STATIC_INLINE void LL_CMP_SetWindowSrc(CMP_TypeDef *CMPx, uint32_t WindowSource)
{
    MODIFY_REG(CMPx->OUT, CMP_OUTCTRL_WINSC_Msk, WindowSource);
}


/**
 * @brief  Return the window sourse of CMP
 * @rmtoll OUT     CMP_OUTCTRL_INV       LL_CMP_GetWindowSource
 * @param  CMPx CMP Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_CMP_WINSOURCE_STIMER0
 *         @arg @ref LL_CMP_WINSOURCE_STIMER1
 *         @arg @ref LL_CMP_WINSOURCE_STIMER2
 *         @arg @ref LL_CMP_WINSOURCE_STIMER4
 */
__STATIC_INLINE uint32_t LL_CMP_GetWindowSrc(CMP_TypeDef *CMPx)
{
    return ((uint32_t)(READ_BIT(CMPx->OUT, CMP_OUTCTRL_WINSC_Msk)));
}


/**
 * @brief  CMP filter is Enable
 * @rmtoll OUT     CMP_OUTCTRL_FILTEN     LL_CMP_FilterEnable
 * @param  CMPx CMP Instance
 * @retval None
 */
__STATIC_INLINE void LL_CMP_EnableFilter(CMP_TypeDef *CMPx)
{
    SET_BIT(CMPx->OUT, CMP_OUTCTRL_FILTEN_Msk);
}


/**
 * @brief  CMP filter is disable
 * @rmtoll OUT     CMP_OUTCTRL_FILTEN       LL_CMP_FilterDisable
 * @param  CMPx CMP Instance
 * @retval None
 */
__STATIC_INLINE void LL_CMP_DisableFilter(CMP_TypeDef *CMPx)
{
    CLEAR_BIT(CMPx->OUT, CMP_OUTCTRL_FILTEN_Msk);
}


/**
 * @brief  Indicate if CMP filter is enabled
 * @rmtoll OUT     CMP_OUTCTRL_FILTEN       LL_CMP_IsFilterEnabled
 * @param  CMPx CMP Instance
 * @retval Returned value can be one of the following values:
 *       @arg @ref LL_CMP_FILTER_DISABLE
 *         @arg @ref LL_CMP_FILTER_ENABLE
 */
__STATIC_INLINE uint32_t LL_CMP_IsEnableFilter(CMP_TypeDef *CMPx)
{
    return ((uint32_t)(READ_BIT(CMPx->OUT, CMP_OUTCTRL_FILTEN_Msk)));
}


/**
 * @brief  CMP set the filter count
 * @rmtoll OUT     CMP_OUTCTRL_FILTCNT        LL_CMP_SetFilterCnt
 * @param  CMPx CMP Instance
 * @param  FilterCNT This parameter can be one of the following values:
 *         @arg @ref LL_CMP_FILTERCNT_1CLK
 *         @arg @ref LL_CMP_FILTERCNT_2CLK
 *         @arg @ref LL_CMP_FILTERCNT_3CLK
 *         @arg @ref LL_CMP_FILTERCNT_4CLK
 *         @arg @ref LL_CMP_FILTERCNT_5CLK
 *         @arg @ref LL_CMP_FILTERCNT_6CLK
 *         @arg @ref LL_CMP_FILTERCNT_7CLK
 *         @arg @ref LL_CMP_FILTERCNT_8CLK
 *         @arg @ref LL_CMP_FILTERCNT_9CLK
 *         @arg @ref LL_CMP_FILTERCNT_10CLK
 *         @arg @ref LL_CMP_FILTERCNT_11CLK
 *         @arg @ref LL_CMP_FILTERCNT_12CLK
 *         @arg @ref LL_CMP_FILTERCNT_13CLK
 *         @arg @ref LL_CMP_FILTERCNT_14CLK
 *         @arg @ref LL_CMP_FILTERCNT_15CLK
 *         @arg @ref LL_CMP_FILTERCNT_16CLK
 * @retval None
 */
__STATIC_INLINE void LL_CMP_SetFilterCnt(CMP_TypeDef *CMPx, uint32_t FilterCNT)
{
    MODIFY_REG(CMPx->OUT, CMP_OUTCTRL_FILTCNT_Msk, FilterCNT);
}


/**
 * @brief  Return the filter count of CMP
 * @rmtoll OUT     CMP_OUTCTRL_FILTCNT       LL_CMP_GetFilterCnt
 * @param  CMPx CMP Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_CMP_FILTERCNT_1CLK
 *         @arg @ref LL_CMP_FILTERCNT_2CLK
 *         @arg @ref LL_CMP_FILTERCNT_3CLK
 *         @arg @ref LL_CMP_FILTERCNT_4CLK
 *         @arg @ref LL_CMP_FILTERCNT_5CLK
 *         @arg @ref LL_CMP_FILTERCNT_6CLK
 *         @arg @ref LL_CMP_FILTERCNT_7CLK
 *         @arg @ref LL_CMP_FILTERCNT_8CLK
 *         @arg @ref LL_CMP_FILTERCNT_9CLK
 *         @arg @ref LL_CMP_FILTERCNT_10CLK
 *         @arg @ref LL_CMP_FILTERCNT_11CLK
 *         @arg @ref LL_CMP_FILTERCNT_12CLK
 *         @arg @ref LL_CMP_FILTERCNT_13CLK
 *         @arg @ref LL_CMP_FILTERCNT_14CLK
 *         @arg @ref LL_CMP_FILTERCNT_15CLK
 *         @arg @ref LL_CMP_FILTERCNT_16CLK
 */
__STATIC_INLINE uint32_t LL_CMP_GetFilterCnt(CMP_TypeDef *CMPx)
{
    return ((uint32_t)(READ_BIT(CMPx->OUT, CMP_OUTCTRL_FILTCNT_Msk)));
}


/**
 * @brief  Return the digital CMP out signal
 * @rmtoll STATUS     CMP_STATUS_RESULT       LL_CMP_GetStatusDigOut
 * @param  CMPx CMP Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref state of bit (1 0r 0)
 */
__STATIC_INLINE uint32_t LL_CMP_GetStatus_DigOut(CMP_TypeDef *CMPx)
{
    return ((uint32_t)(READ_BIT(CMPx->STATUS, CMP_STATUS_RESULT_Msk)));
}


/**
 * @brief  Return the digital CMP out logical value
 * @rmtoll STATUS     CMP_STATUS_RESULT       LL_CMP_IsHigh_DigOut
 * @param  CMPx CMP Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref state of bit (1 0r 0)
 */
__STATIC_INLINE uint32_t LL_CMP_IsHigh_DigOut(CMP_TypeDef *CMPx)
{
    return ((uint32_t)(READ_BIT(CMPx->STATUS, CMP_STATUS_RESULT_Msk) == CMP_STATUS_RESULT_Msk));
}


/**
 * @brief  Return  the state of CMP analog out
 * @rmtoll STATUS     CMP_STATUS_ANAOUT       LL_CMP_GetStatusAnaOut
 * @param  CMPx CMP Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref state of bit (1 0r 0)
 */
__STATIC_INLINE uint32_t LL_CMP_GetStatus_AnaOut(CMP_TypeDef *CMPx)
{
    return ((uint32_t)(READ_BIT(CMPx->STATUS, CMP_STATUS_ANAOUT_Msk)));
}


/**
 * @brief  Return  the state of CMP analog out logical value
 * @rmtoll STATUS     CMP_STATUS_ANAOUT       LL_CMP_IsHigh_AnaOut
 * @param  CMPx CMP Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref state of bit (1 0r 0)
 */
__STATIC_INLINE uint32_t LL_CMP_IsHigh_AnaOut(CMP_TypeDef *CMPx)
{
    return ((uint32_t)(READ_BIT(CMPx->STATUS, CMP_STATUS_ANAOUT_Msk) == CMP_STATUS_ANAOUT_Msk));
}


/**
 * @brief  Return  the Ready Flag of CMP analog out
 * @rmtoll STATUS     CMP_STATUS_ANARDY       LL_CMP_IsReadyFlagAnaOut
 * @param  CMPx CMP Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref state of bit (1 0r 0)
 */
__STATIC_INLINE uint32_t LL_CMP_IsReady_AnaOut(CMP_TypeDef *CMPx)
{
    return ((uint32_t)(READ_BIT(CMPx->STATUS, CMP_STATUS_ANARDY_Msk) == CMP_STATUS_ANARDY_Msk));
}


/**
 * @brief  CMP set the INT or DMA request if record produce
 * @rmtoll INTEN     CMP_INTEN_DMASEL        LL_CMP_SetINTorDMAEn
 * @param  CMPx CMP Instance
 * @param  INTOrDMAReq This parameter can be one of the following values:
 *         @arg @ref LL_CMP_REQSEL_INT
 *         @arg @ref LL_CMP_REQSEL_DMA
 *         @arg @ref LL_CMP_REQSEL_NONE
 * @retval None
 */
__STATIC_INLINE void LL_CMP_SetINTorDMAEn(CMP_TypeDef *CMPx, uint32_t INTOrDMAReq)
{
    MODIFY_REG(CMPx->INTEN, CMP_INTEN_INTDMAEN_Msk | CMP_INTEN_DMASEL_Msk, INTOrDMAReq);
}


/**
 * @brief  MP Record INT and DMA request is disable
 * @rmtoll INTEN     CMP_INTEN_INTDMAEN     LL_CMP_DisableINTorDMA
 * @param  CMPx CMP Instance
 * @retval None
 */
__STATIC_INLINE void LL_CMP_DisableINTorDMA(CMP_TypeDef *CMPx)
{
    CLEAR_BIT(CMPx->INTEN, CMP_INTEN_INTDMAEN_Msk | CMP_INTEN_DMASEL_Msk);
}


/**
 * @brief  Return the setting  if record produce
 * @rmtoll INTEN     CMP_INTEN_DMASEL       LL_CMP_GetINTorDMAEn
 * @param  CMPx CMP Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_CMP_REQSEL_INT
 *         @arg @ref LL_CMP_REQSEL_DMA
 *         @arg @ref LL_CMP_REQSEL_NONE
 */
__STATIC_INLINE uint32_t LL_CMP_GetINTorDMAEn(CMP_TypeDef *CMPx)
{
    return ((uint32_t)(READ_BIT(CMPx->INTEN, CMP_INTEN_INTDMAEN_Msk | CMP_INTEN_DMASEL_Msk)));
}


/**
 * @brief  CMP set the record  trigger Mode
 * @rmtoll INTEN     CMP_INTEN_MODE        LL_CMP_SetRcdTrigMode
 * @param  CMPx CMP Instance
 * @param  RcdTrigMode This parameter can be one of the following values:
 *         @arg @ref LL_CMP_RCDMODE_HIGH
 *         @arg @ref LL_CMP_RCDMODE_RISING
 *         @arg @ref LL_CMP_RCDMODE_FALLING
 *         @arg @ref LL_CMP_RCDMODE_BOTHEDGE
 * @retval None
 */
__STATIC_INLINE void LL_CMP_SetTrigRecordMode(CMP_TypeDef *CMPx, uint32_t RcdTrigMode)
{
    MODIFY_REG(CMPx->INTEN, CMP_INTEN_MODE_Msk, RcdTrigMode);
}


/**
 * @brief  Return trigger Mode of record
 * @rmtoll INTEN     CMP_INTEN_MODE       LL_CMP_GetRcdTrigMode
 * @param  CMPx CMP Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_CMP_RCDMODE_HIGH
 *         @arg @ref LL_CMP_RCDMODE_RISING
 *         @arg @ref LL_CMP_RCDMODE_FALLING
 *         @arg @ref LL_CMP_RCDMODE_BOTHEDGE
 */
__STATIC_INLINE uint32_t LL_CMP_GetTrigRecordMode(CMP_TypeDef *CMPx)
{
    return ((uint32_t)(READ_BIT(CMPx->INTEN, CMP_INTEN_MODE_Msk)));
}


/**
 * @brief  Return the CMP record
 * @rmtoll RCD     CMP_RCD_RCD       LL_CMP_IsActiveRecord
 * @param  CMPx CMP Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref Record of bit (1 0r 0)
 */
__STATIC_INLINE uint32_t LL_CMP_IsActiveRecord(CMP_TypeDef *CMPx)
{
    return ((uint32_t)(READ_BIT(CMPx->RCD, CMP_RCD_RCD_Msk) == CMP_RCD_RCD_Msk));
}


/**
 * @brief  Return the CMP INT Record
 * @rmtoll RCD     CMP_RCD_RCD       LL_CMP_IsActiveRecord_IT
 * @param  CMPx CMP Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref Record of bit (1 0r 0)
 */
__STATIC_INLINE uint32_t LL_CMP_IsActiveRecord_IT(CMP_TypeDef *CMPx)
{
    return ((uint32_t)(READ_BIT(CMPx->RCD, CMP_RCD_INT_Msk) == CMP_INT_RCD));
}


/**
 * @brief  clear the CMP Record
 * @rmtoll RCDCL     CMP_RCDCLR_CLEAR        LL_CMP_ClearAllRecord
 * @param  CMPx CMP Instance
 * @retval None
 */
__STATIC_INLINE void LL_CMP_ClearAllRecord(CMP_TypeDef *CMPx)
{
    SET_BIT(CMPx->RCDCL, CMP_RCDCLR_CLEAR);
}


void LL_CMP_Init(CMP_TypeDef *CMPx, LL_CMP_InitTypeDef *CMP_InitStruct);


void LL_SPI_DeInit(CMP_TypeDef *CMPx);


#ifdef __cplusplus
}
#endif
#endif
