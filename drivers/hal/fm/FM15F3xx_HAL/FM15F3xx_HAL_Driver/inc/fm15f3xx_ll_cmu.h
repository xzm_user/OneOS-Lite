/**
  ******************************************************************************
  * @file    fm15f3xx_ll_cmu.h
  * @author  ZJH
  * @version V1.0.0
  * @date    2020-03-17
  * @brief
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 FudanMicroelectronics.
  * All rights reserved.</center></h2>
  *
  ******************************************************************************
  */
#ifndef FM15F3XX_LL_CMU_H
#define FM15F3XX_LL_CMU_H

#ifdef __cplusplus
extern "C" {
#endif

#include "fm15f3xx.h"


typedef struct {
    uint32_t OscType;           /*!< @ref CMU_Oscillator_Type*/
    uint32_t OscValueHz;        /*!< External Osc Value Hz*/
    uint32_t ExpectSysClkHz;    /*!< Expect System Clk Hz*/
    uint32_t ExpectBusClkHz;    /*!< Expect Bus Clk Hz*/
} LL_CMU2_InitTypeDef;

typedef struct {
    uint32_t PLLState;          /*!< @ref CMU_PLL_Config*/
    uint32_t PLLSource;         /*!< @ref CMU_PLL_Clock_Source*/
    uint32_t PLLFreqHz;         /*!< Expect PLL Clk Hz*/
} LL_PLL_InitTypeDef;
typedef struct {
    uint32_t OscType;           /*!< @ref CMU_Oscillator_Type*/
    uint32_t XtalHz;            /*!< External Osc Value Hz*/
    LL_PLL_InitTypeDef PLL;     /*!< PLL structure parameters*/
    uint32_t MainClkSrc;        /*!< @ref CMU_Main_Clock_Source*/
    uint32_t MainClkDiv;        /*!< @ref CMU_LL_MAINCLK_DIV*/
    uint32_t BusClkDiv;         /*!< @ref CMU_LL_EC_REUSE3DIV*/
} LL_CMU_InitTypeDef;

typedef struct {
    uint8_t Div;
    uint8_t Cfg;  //[3:0]sysdiv [6:4]mainclkdiv
} CMU_SysClk_CFG;

#define IRC4M_VALUE          (4000000)
#define IRC16M_VALUE         (16000000)

/** @defgroup CMU_Oscillator_Type
  * @{
  */
#define LL_CMU_OSCTYPE_IRC4M          (0x01U)      /*irc4M*/
#define LL_CMU_OSCTYPE_IRC16M         (0x02U)      /*irc16M*/
#define LL_CMU_OSCTYPE_XTAL           (0x04U)      /*xtal*/
/**
  * @}
  */

/** @defgroup CMU_PLL_Config PLL Config
  * @{
  */
#define LL_CMU_PLL_OFF                ((uint8_t)0x00)
#define LL_CMU_PLL_ON                 ((uint8_t)0x01)
/**
  * @}
  */

/** @defgroup CMU_PLL_Clock_Source PLL Clock Source
  * @{
  */
#define LL_CMU_PLLCLK_XTAL            0x00000000U                    /*!< xtal */
#define LL_CMU_PLLCLK_IRC16M          (CMU_CFGCLKS1_PLLCLKINS)         /*!< irc16m */
/**
  * @}
  */

/** @defgroup CMU_Main_Clock_Source
  * @{
  */
#define LL_CMU_MCGMAINCLK_IRC4M       (0x1U)      /*!< irc4m   */
#define LL_CMU_MCGMAINCLK_IRC16M      (0x2U)      /*!< irc16m  */
#define LL_CMU_MCGMAINCLK_PLLCLK      (0x3U)      /*!< pll_clk */
#define LL_CMU_MCGMAINCLK_OSCCLK      (0x4U)      /*!< osc_clk */
#define LL_CMU_MCGMAINCLK_USB48MCLK   (0x5U)      /*!< usb48m_clk */
#define LL_CMU_MCGMAINCLK_GPIOCLK     (0x7U)      /*!< gpio_clk*/
/**
  * @}
  */


/** @defgroup CMU_LL_EC_MCG_IRCCLK
  * @{
  */
#define LL_CMU_MCGIRCCLK_IRC4M        0x00000000U                   /*!< IRC4M  */
#define LL_CMU_MCGIRCCLK_IRC16M       (CMU_MCGCLKA_IRCSEL)          /*!< IRC16M */
/**
  * @}
  */

/** @defgroup CMU_LL_MAINCLK_ORG_SRC
  * @{
  */
#define LL_CMU_ORGMAINCLK_IRCCLK      (0x0U)      /*!< irc_clk  */
#define LL_CMU_ORGMAINCLK_IRC4M       (0x1U)      /*!< irc4m    */
#define LL_CMU_ORGMAINCLK_IRC16M      (0x2U)      /*!< irc16m   */
#define LL_CMU_ORGMAINCLK_PLLCLK      (0x3U)      /*!< pll_clk  */
#define LL_CMU_ORGMAINCLK_OSCCLK      (0x4U)      /*!< osc_clk  */
#define LL_CMU_ORGMAINCLK_DFSCLK      (0x5U)      /*!< dfs_clk  */
#define LL_CMU_ORGMAINCLK_PLLDIV      (0x6U)      /*!< pll_div  */
#define LL_CMU_ORGMAINCLK_GPIOCLK     (0x7U)      /*!< gpio_clk  */
/**
  * @}
  */

/** @defgroup  CT Source
  * @{
  */
#define LL_CMU_CTCLK_SYSCLK           (0x0U)      /*!< sysclk   */
#define LL_CMU_CTCLK_IOCLK            (0x1U)      /*!< ioclk    */
#define LL_CMU_CTCLK_IRC16M           (0x2U)      /*!< irc16m   */
#define LL_CMU_CTCLK_OSCCLK           (0x3U)      /*!< oscclk   */
/**
  * @}
  */

/** @defgroup  UART Source
  * @{
  */
#define LL_CMU_UARTCLK_SYSCLK         (0x0U)       /*!< sysclk   */
#define LL_CMU_UARTCLK_IOCLK          (0x1U)       /*!< ioclk    */
#define LL_CMU_UARTCLK_IRC16M         (0x2U)       /*!< irc16m    */
#define LL_CMU_UARTCLK_OSCCLK         (0x3U)       /*!< oscclk    */
/**
  * @}
  */



/** @defgroup CMU_LL_USBFS_CLK_SEL
  * @{
  */
#define LL_CMU_USBFSCLK_PLL           (0x0U)     /*!< pll_clk  */
#define LL_CMU_USBFSCLK_USB48M        (0x1U)     /*!< usb48m_clk   */
#define LL_CMU_USBFSCLK_PLLDIV        (0x2U)     /*!< pll_div_clk   */
#define LL_CMU_USBFSCLK_SYSTEST       (0x3U)     /*!< sys_test_clk  */
/**
  * @}
  */

/** @defgroup SHIELD Source
  * @{
  */
#define LL_CMU_SHIELDCLK_IRC4M        (0x0U)      /*!< IRC4M  */
#define LL_CMU_SHIELDCLK_LPO1K        (0x1U)      /*!< LPO1K  */
/**
  * @}
  */
/** @defgroup ALWAYSON bus clk Source
  * @{
  */
#define LL_CMU_AONBUSCLK_BUS          (0x0U)      /*!< bus_clk  */
#define LL_CMU_AONBUSCLK_BUSDIV2      (0x1U)      /*!< bus_clk/2  */
#define LL_CMU_AONBUSCLK_BUSDIV3      (0x2U)      /*!< bus_clk/3  */
#define LL_CMU_AONBUSCLK_BUSDIV4      (0x3U)      /*!< bus_clk/4  */
/**
  * @}
  */

/** @defgroup RTC MAIN BUS Source
  * @{
  */
#define LL_CMU_RTCMAINBUSCLK_BUS      (0x0U)      /*!< bus_clk  */
#define LL_CMU_RTCMAINBUSCLK_BUSDIV2  (0x1U)      /*!< bus_clk/2  */
#define LL_CMU_RTCMAINBUSCLK_BUSDIV3  (0x2U)      /*!< bus_clk/3  */
#define LL_CMU_RTCMAINBUSCLK_BUSDIV4  (0x3U)      /*!< bus_clk/4  */
/**
  * @}
  */

/** @defgroup WDT Source
  * @{
  */
#define LL_CMU_WDTCLK_LPO1K           (0x0U)      /*!< LPO1K  */
#define LL_CMU_WDTCLK_BUSCLK          (0x1U)      /*!< BUSCLK  */
/**
  * @}
  */

/** @defgroup CMU_CLK ADC Source
  * @{
  */
#define LL_CMU_ADCCLK_IRC4M           (0x0U)      /*!< IRC4M  */
#define LL_CMU_ADCCLK_IRC16M          (0x1U)      /*!< IRC16M */
#define LL_CMU_ADCCLK_OSCCLK          (0x2U)      /*!< OSC    */
#define LL_CMU_ADCCLK_PLLDIV          (0x3U)      /*!< PLLDIV */
/**
  * @}
  */

/** @defgroup CMU_CLK CLK OUT Source
  * @{
  */
#define LL_CMU_COUTCLK_SYSTEST        (0x0U)     /*!< systest    */
#define LL_CMU_COUTCLK_IRC4M          (0x1U)     /*!< IRC4M    */
#define LL_CMU_COUTCLK_IRC16M         (0x2U)     /*!< IRC16M   */
#define LL_CMU_COUTCLK_PLLCLK         (0x3U)     /*!< PLL      */
#define LL_CMU_COUTCLK_OSCCLK         (0x4U)     /*!< OSC      */
#define LL_CMU_COUTCLK_PLLDIV8        (0x5U)     /*!< PLL/8    */
#define LL_CMU_COUTCLK_PLLDIV         (0x6U)     /*!< PLLDIV   */
#define LL_CMU_COUTCLK_GPIOCLK        (0x7U)     /*!< GPIO     */
#define LL_CMU_COUTCLK_TRNGANA        (0xAU)     /*!< TRNG ANA   */
#define LL_CMU_COUTCLK_USB48M         (0xBU)     /*!< USB48M   */
#define LL_CMU_COUTCLK_SYSCLK         (0xCU)     /*!< SYS CLK    */
#define LL_CMU_COUTCLK_SECCLK         (0xDU)     /*!< SEC CLK    */
#define LL_CMU_COUTCLK_OSC32K         (0xFU)     /*!< OSC 32K    */
/**
  * @}
  */

/** @defgroup MCG_IO_CLK Source
  * @{
  */
#define LL_CMU_MCGIOCLK_IRCCLK        (0x0U)      /*!< IRC      */
#define LL_CMU_MCGIOCLK_IRC4M         (0x1U)      /*!< IRC4M    */
#define LL_CMU_MCGIOCLK_IRC16M        (0x2U)      /*!< IRC16M   */
#define LL_CMU_MCGIOCLK_PLLCLK        (0x3U)      /*!< PLL      */
#define LL_CMU_MCGIOCLK_OSCCLK        (0x4U)      /*!< OSC      */
#define LL_CMU_MCGIOCLK_USB48M        (0x5U)      /*!< USB48M   */
#define LL_CMU_MCGIOCLK_PLLDIV        (0x6U)      /*!< PLLDIV   */
#define LL_CMU_MCGIOCLK_GPIOCLK       (0x7U)      /*!< GPIO     */
/**
  * @}
  */

/** @defgroup ST_CLK_SOURCE
  * @{
  */
#define LL_CMU_STCLK_SYSCLK           (0x0U)      /*!< sys_clk */
#define LL_CMU_STCLK_PLLCLK           (0x1U)      /*!< pll_clk */
#define LL_CMU_STCLK_OSCCLK           (0x2U)      /*!< osc_clk */
#define LL_CMU_STCLK_IRC4M            (0x3U)      /*!< irc4m */
#define LL_CMU_STCLK_IRC16M           (0x4U)      /*!< irc16m */
#define LL_CMU_STCLK_GPIOCLK          (0x5U)      /*!< gpio_clk */
#define LL_CMU_STCLK_PLLDIV           (0x6U)      /*!< pll_div */
#define LL_CMU_STCLK_USB48M           (0x7U)      /*!< usb48m_clk */
/**
  * @}
  */
/** @defgroup CMU_CLK USBPHY Source
  * @{
  */
#define LL_CMU_USBPHYCLK_OSCCLK       (0x0U)      /*!< osc_clk */
#define LL_CMU_USBPHYCLK_PLLCLK       (0x1U)      /*!< pll_clk */
#define LL_CMU_USBPHYCLK_GPIOCLK      (0x2U)      /*!< gpio_clk */
#define LL_CMU_USBPHYCLK_USB48M       (0x3U)      /*!< usb48m */
/**
  * @}
  */

/** @defgroup CMU_CLK SYSTICKREF Source
  * @{
  */
#define LL_CMU_SYSTICKREFCLK_IRC4M    (0x0U)      /*!< irc4m */
#define LL_CMU_SYSTICKREFCLK_IRC16M   (0x1U)      /*!< irc16m */
#define LL_CMU_SYSTICKREFCLK_OSCCLK   (0x2U)      /*!< osc clk */
#define LL_CMU_SYSTICKREFCLK_GPIOCLK  (0x3U)      /*!< gpio clk */
/**
  * @}
  */

/** @defgroup CMU_CLK PLL EXT Source
  * @{
  */
#define LL_CMU_PLLEXTCLK_CLOSE        (0x0U)      /*!< close */
#define LL_CMU_PLLEXTCLK_GPIOCLK      (0x1U)      /*!< gpioclk */
#define LL_CMU_PLLEXTCLK_USB12M       (0x2U)      /*!< usb12m */
#define LL_CMU_PLLEXTCLK_USB48M       (0x3U)      /*!< usb48m */
/**
  * @}
  */

/** @defgroup CMU_LL_GATE_MODULE_CLKS
  * @{
  */
#define LL_CMU_MDCLK_SHIELD           CMU_GMDLCLK_SHIELD      /*!<shield_clks */
#define LL_CMU_MDCLK_WDT              CMU_GMDLCLK_WDT         /*!<wdt_clks */
#define LL_CMU_MDCLK_ADC              CMU_GMDLCLK_ADC         /*!<adc_clks */
#define LL_CMU_MDCLK_TRNG             CMU_GMDLCLK_TRNG        /*!<trng_clks */
#define LL_CMU_MDCLK_CT               CMU_GMDLCLK_CT          /*!<ct_clks */
#define LL_CMU_MDCLK_UART             CMU_GMDLCLK_UART        /*!<uart_clks */
#define LL_CMU_MDCLK_USBFS            CMU_GMDLCLK_USBFS       /*!<usb_fs_clks */
#define LL_CMU_MDCLK_COUT0            CMU_GMDLCLK_TST0        /*!<test0_clks */
#define LL_CMU_MDCLK_COUT1            CMU_GMDLCLK_TST1        /*!<test1_clks */
#define LL_CMU_MDCLK_COUT2            CMU_GMDLCLK_TST2        /*!<test2_clks */
#define LL_CMU_MDCLK_STIMER0          CMU_GMDLCLK_STM0        /*!<stimer0_clks */
#define LL_CMU_MDCLK_STIMER1          CMU_GMDLCLK_STM1        /*!<stimer1_clks */
#define LL_CMU_MDCLK_STIMER2          CMU_GMDLCLK_STM2        /*!<stimer2_clks */
#define LL_CMU_MDCLK_STIMER3          CMU_GMDLCLK_STM3        /*!<stimer3_clks */
#define LL_CMU_MDCLK_STIMER4          CMU_GMDLCLK_STM4        /*!<stimer4_clks */
#define LL_CMU_MDCLK_STIMER5          CMU_GMDLCLK_STM5        /*!<stimer5_clks */
#define LL_CMU_MDCLK_USBPHY           CMU_GMDLCLK_USBPHY      /*!<usb_phy_clks */
#define LL_CMU_MDCLK_SYSTKRF          CMU_GMDLCLK_SYSTKRF     /*!<sys_tick_ref_clks */
#define LL_CMU_MDCLK_SYSDIV           CMU_GMDLCLK_SYSDIV      /*!<sys_div_clks */
#define LL_CMU_MDCLK_SYSTST           CMU_GMDLCLK_SYSTST      /*!<sys_test_clks */
#define LL_CMU_MDCLK_PLLEXT           CMU_GMDLCLK_PLLEXT      /*!<pll_ext_clks */
/**
  * @}
  */

/** @defgroup CMU_LL_CLK_IDLE_SRC
  * @{
  */
#define LL_CMU_CLKIDLESRC_USB48M      CMU_CLKIDLE_ENFLL      /*!<shield_clks */
#define LL_CMU_CLKIDLESRC_PLLCLK      CMU_CLKIDLE_ENPLL      /*!<wdt_clks */
#define LL_CMU_CLKIDLESRC_OSCCLK      CMU_CLKIDLE_ENOSC      /*!<adc_clks */
#define LL_CMU_CLKIDLESRC_IRC16M      CMU_CLKIDLE_EN16M      /*!<trng_clks */
/**
  * @}
  */

/** @defgroup CMU_LL_CLK_STOP_SRC
  * @{
  */
#define LL_CMU_CLKSTOPSRC_USB48M      CMU_CLKSTOP_ENFLL      /*!<shield_clks */
#define LL_CMU_CLKSTOPSRC_PLLCLK      CMU_CLKSTOP_ENPLL      /*!<wdt_clks */
#define LL_CMU_CLKSTOPSRC_OSCCLK      CMU_CLKSTOP_ENOSC      /*!<adc_clks */
#define LL_CMU_CLKSTOPSRC_IRC16M      CMU_CLKSTOP_EN16M      /*!<trng_clks */
/**
  * @}
  */

/** @defgroup CMU_LL_CLK_LP_SRC
  * @{
  */
#define LL_CMU_CLKLPSRC_LPO1K         CMU_CLKLP_PDLPOEN         /*!<shield_clks */
#define LL_CMU_CLKLPSRC_STBYIRC4M     CMU_CLKLP_STBYIRC4MEN     /*!<wdt_clks */
#define LL_CMU_CLKLPSRC_STOPIRC4M     CMU_CLKLP_STOPIRC4MEN     /*!<adc_clks */
/**
  * @}
  */

/** @defgroup SEC CLK Config
  * @{
  */
#define LL_CMU_SECCLK_SYNSYSCLK       0x00000000U           /*!< Security Clk Synchronize with Sys clk*/
#define LL_CMU_SECCLK_RNDSYSCLK       (CMU_MCLKS_DFSSCY)    /*!< Security Clk Randomization based on Sys clk  */
/**
  * @}
  */

/** @defgroup CMU_LL_MAINCLK_DIV
  * @{
  */
#define LL_CMU_MAINCLK_DIV001         (0x00U)
#define LL_CMU_MAINCLK_DIV002         (0x01U)
#define LL_CMU_MAINCLK_DIV003         (0x02U)
#define LL_CMU_MAINCLK_DIV004         (0x03U)
#define LL_CMU_MAINCLK_DIV005         (0x04U)
#define LL_CMU_MAINCLK_DIV006         (0x05U)
#define LL_CMU_MAINCLK_DIV007         (0x06U)
#define LL_CMU_MAINCLK_DIV008         (0x07U)
#define LL_CMU_MAINCLK_DIV009         (0x08U)
#define LL_CMU_MAINCLK_DIV010         (0x09U)
#define LL_CMU_MAINCLK_DIV011         (0x0AU)
#define LL_CMU_MAINCLK_DIV012         (0x0BU)
#define LL_CMU_MAINCLK_DIV013         (0x0CU)
#define LL_CMU_MAINCLK_DIV014         (0x0DU)
#define LL_CMU_MAINCLK_DIV015         (0x0EU)
#define LL_CMU_MAINCLK_DIV016         (0x0FU)
#define LL_CMU_MAINCLK_DIV018         (0x10U)
#define LL_CMU_MAINCLK_DIV020         (0x11U)
#define LL_CMU_MAINCLK_DIV022         (0x12U)
#define LL_CMU_MAINCLK_DIV024         (0x13U)
#define LL_CMU_MAINCLK_DIV026         (0x14U)
#define LL_CMU_MAINCLK_DIV028         (0x15U)
#define LL_CMU_MAINCLK_DIV030         (0x16U)
#define LL_CMU_MAINCLK_DIV032         (0x17U)
#define LL_CMU_MAINCLK_DIV036         (0x18U)
#define LL_CMU_MAINCLK_DIV040         (0x19U)
#define LL_CMU_MAINCLK_DIV044         (0x1AU)
#define LL_CMU_MAINCLK_DIV048         (0x1BU)
#define LL_CMU_MAINCLK_DIV052         (0x1CU)
#define LL_CMU_MAINCLK_DIV056         (0x1DU)
#define LL_CMU_MAINCLK_DIV060         (0x1EU)
#define LL_CMU_MAINCLK_DIV064         (0x1FU)
#define LL_CMU_MAINCLK_DIV072         (0x20U)
#define LL_CMU_MAINCLK_DIV080         (0x21U)
#define LL_CMU_MAINCLK_DIV088         (0x22U)
#define LL_CMU_MAINCLK_DIV096         (0x23U)
#define LL_CMU_MAINCLK_DIV104         (0x24U)
#define LL_CMU_MAINCLK_DIV112         (0x25U)
#define LL_CMU_MAINCLK_DIV120         (0x26U)
#define LL_CMU_MAINCLK_DIV128         (0x27U)
/**
  * @}
  */


/** @defgroup CMU_LL_EC_MCG_IODIV
  * @{
  */
#define LL_CMU_MCGIOCLK_DIV1          (0x1U)      /*!< divider 1  */
#define LL_CMU_MCGIOCLK_DIV2          (0x2U)      /*!< divider 2  */
#define LL_CMU_MCGIOCLK_DIV3          (0x3U)      /*!< divider 3  */
#define LL_CMU_MCGIOCLK_DIV4          (0x4U)      /*!< divider 4  */
#define LL_CMU_MCGIOCLK_DIV5          (0x5U)      /*!< divider 5  */
#define LL_CMU_MCGIOCLK_DIV6          (0x6U)      /*!< divider 6  */
#define LL_CMU_MCGIOCLK_DIV7          (0x7U)      /*!< divider 7  */
#define LL_CMU_MCGIOCLK_DIV8          (0x8U)      /*!< divider 8  */
/**
  * @}
  */


/** @defgroup CMU_LL_EC_PLLEXTDIV
  * @{
  */
#define LL_CMU_PLLEXTCLK_DIV1         (0x0U)      /*!< divider 1  */
#define LL_CMU_PLLEXTCLK_DIV2         (0x1U)      /*!< divider 2  */
#define LL_CMU_PLLEXTCLK_DIV3         (0x2U)      /*!< divider 3  */
#define LL_CMU_PLLEXTCLK_DIV4         (0x3U)      /*!< divider 4  */
/**
  * @}
  */

/** @defgroup CMU_LL_EC_MCG_PLLDIV
  * @{
  */
#define LL_CMU_PLL_DIV1P5             (0x02U)      /*!< divider 1.5  */
#define LL_CMU_PLL_DIV02              (0x03U)      /*!< divider 2  */
#define LL_CMU_PLL_DIV2P5             (0x04U)      /*!< divider 2.5  */
#define LL_CMU_PLL_DIV03              (0x05U)      /*!< divider 3  */
#define LL_CMU_PLL_DIV3P5             (0x06U)      /*!< divider 3.5 */
#define LL_CMU_PLL_DIV04              (0x07U)      /*!< divider 4 */
#define LL_CMU_PLL_DIV4P5             (0x08U)      /*!< divider 4.5 */
#define LL_CMU_PLL_DIV05              (0x09U)      /*!< divider 5 */
#define LL_CMU_PLL_DIV5P5             (0x0AU)      /*!< divider 5.5 */
#define LL_CMU_PLL_DIV06              (0x0BU)      /*!< divider 6 */
#define LL_CMU_PLL_DIV6P5             (0x0CU)      /*!< divider 6.5 */
#define LL_CMU_PLL_DIV07              (0x0DU)      /*!< divider 7 */
#define LL_CMU_PLL_DIV7P5             (0x0EU)      /*!< divider 7.5 */
#define LL_CMU_PLL_DIV08              (0x0FU)      /*!< divider 8 */
#define LL_CMU_PLL_DIV8P5             (0x10U)      /*!< divider 8.5 */
#define LL_CMU_PLL_DIV09              (0x11U)      /*!< divider 9 */
#define LL_CMU_PLL_DIV9P5             (0x12U)      /*!< divider 9.5 */
#define LL_CMU_PLL_DIV10              (0x13U)      /*!< divider 10 */
#define LL_CMU_PLL_DIV10P5            (0x14U)      /*!< divider 10.5 */
#define LL_CMU_PLL_DIV11              (0x15U)      /*!< divider 11 */
#define LL_CMU_PLL_DIV11P5            (0x16U)      /*!< divider 11.5 */
#define LL_CMU_PLL_DIV12              (0x17U)      /*!< divider 12 */
#define LL_CMU_PLL_DIV12P5            (0x18U)      /*!< divider 12.5 */
#define LL_CMU_PLL_DIV13              (0x19U)      /*!< divider 13 */
#define LL_CMU_PLL_DIV13P5            (0x1AU)      /*!< divider 13.5 */
#define LL_CMU_PLL_DIV14              (0x1BU)      /*!< divider 14 */
#define LL_CMU_PLL_DIV14P5            (0x1CU)      /*!< divider 14.5 */
#define LL_CMU_PLL_DIV15              (0x1DU)      /*!< divider 15 */
#define LL_CMU_PLL_DIV15P5            (0x1EU)      /*!< divider 15.5 */
#define LL_CMU_PLL_DIV16              (0x1FU)      /*!< divider 16 */
/**
  * @}
  */

/** @defgroup CMU_LL_EC_SHIELDDIV
  * @{
  */
#define LL_CMU_SHIELDCLK_DIV1         (0x0U)      /*!< divider 1  */
#define LL_CMU_SHIELDCLK_DIV2         (0x1U)      /*!< divider 2  */
#define LL_CMU_SHIELDCLK_DIV4         (0x2U)      /*!< divider 4  */
#define LL_CMU_SHIELDCLK_DIV8         (0x3U)      /*!< divider 8  */
#define LL_CMU_SHIELDCLK_DIV16        (0x4U)      /*!< divider 16  */
#define LL_CMU_SHIELDCLK_DIV32        (0x5U)      /*!< divider 32  */
#define LL_CMU_SHIELDCLK_DIV64        (0x6U)      /*!< divider 64  */
#define LL_CMU_SHIELDCLK_DIV128       (0x7U)      /*!< divider 128  */
/**
  * @}
  */

/** @defgroup CMU_LL_EC_REUSE1DIV
  * @{
  */
#define LL_CMU_REUSEDIV1_DIV1         (0x0U)      /*!< divider 1  */
#define LL_CMU_REUSEDIV1_DIV2         (0x1U)      /*!< divider 2  */
#define LL_CMU_REUSEDIV1_DIV4         (0x2U)      /*!< divider 4  */
#define LL_CMU_REUSEDIV1_DIV8         (0x3U)      /*!< divider 8  */
/**
  * @}
  */

/** @defgroup CMU_LL_EC_REUSE2DIV
  * @{
  */
#define LL_CMU_REUSEDIV2_DIV2         (0x0U)      /*!< divider 2  */
#define LL_CMU_REUSEDIV2_DIV4         (0x1U)      /*!< divider 4  */
#define LL_CMU_REUSEDIV2_DIV8         (0x2U)      /*!< divider 8  */
#define LL_CMU_REUSEDIV2_DIV16        (0x3U)      /*!< divider 16  */
#define LL_CMU_REUSEDIV2_DIV32        (0x4U)      /*!< divider 32  */
#define LL_CMU_REUSEDIV2_DIV64        (0x5U)      /*!< divider 64  */
#define LL_CMU_REUSEDIV2_DIV128       (0x6U)      /*!< divider 128  */
#define LL_CMU_REUSEDIV2_DIV256       (0x7U)      /*!< divider 256  */
/**
  * @}
  */

/** @defgroup CMU_LL_EC_REUSE3DIV
  * @{
  */
#define LL_CMU_REUSEDIV3_DIV1         (0x0U)      /*!< divider 1  */
#define LL_CMU_REUSEDIV3_DIV2         (0x1U)      /*!< divider 2  */
#define LL_CMU_REUSEDIV3_DIV3         (0x2U)      /*!< divider 3  */
#define LL_CMU_REUSEDIV3_DIV4         (0x3U)      /*!< divider 4  */
#define LL_CMU_REUSEDIV3_DIV5         (0x4U)      /*!< divider 5  */
#define LL_CMU_REUSEDIV3_DIV6         (0x5U)      /*!< divider 6  */
#define LL_CMU_REUSEDIV3_DIV7         (0x6U)      /*!< divider 7  */
#define LL_CMU_REUSEDIV3_DIV8         (0x7U)      /*!< divider 8  */
#define LL_CMU_REUSEDIV3_DIV9         (0x8U)      /*!< divider 9  */
#define LL_CMU_REUSEDIV3_DIV10        (0x9U)      /*!< divider 10  */
#define LL_CMU_REUSEDIV3_DIV11        (0xAU)      /*!< divider 11  */
#define LL_CMU_REUSEDIV3_DIV12        (0xBU)      /*!< divider 12  */
#define LL_CMU_REUSEDIV3_DIV13        (0xCU)      /*!< divider 13  */
#define LL_CMU_REUSEDIV3_DIV14        (0xDU)      /*!< divider 14  */
#define LL_CMU_REUSEDIV3_DIV15        (0xEU)      /*!< divider 15  */
#define LL_CMU_REUSEDIV3_DIV16        (0xFU)      /*!< divider 16  */
/**
  * @}
  */


/**
 * @brief  Enable the POR in power down Mode
 * @rmtoll CLKLP    PDPOREN       LL_CMU_PD_EnablePOR
 * @retval None
 */
__STATIC_INLINE void LL_CMU_PD_EnablePOR(void)
{
    SET_BIT(CMU->CLKLP, CMU_CLKLP_PDPOREN);
}
/**
 * @brief  Disable the POR in power down Mode
 * @rmtoll CLKLP    PDPOREN       LL_CMU_PD_DisablePOR
 * @retval None
 */
__STATIC_INLINE void LL_CMU_PD_DisablePOR(void)
{
    CLEAR_BIT(CMU->CLKLP, CMU_CLKLP_PDPOREN);
}

/**
 * @brief  Enable the lpo in power down Mode
 * @rmtoll CLKLP    PDLPOEN       LL_CMU_PD_EnableLPO
 * @retval None
 */
__STATIC_INLINE void LL_CMU_PD_EnableLPO(void)
{
    SET_BIT(CMU->CLKLP, CMU_CLKLP_PDLPOEN);
}
/**
 * @brief  Disable the lpo in power down Mode
 * @rmtoll CLKLP    PDLPOEN       LL_CMU_PD_DisableLPO
 * @retval None
 */
__STATIC_INLINE void LL_CMU_PD_DisableLPO(void)
{
    CLEAR_BIT(CMU->CLKLP, CMU_CLKLP_PDLPOEN);
}

/**
 * @brief  Enable the irc4m in standby Mode
 * @rmtoll CLKLP    STBYIRC4MEN       LL_CMU_STBY_EnableIRC4M
 * @retval None
 */
__STATIC_INLINE void LL_CMU_STBY_EnableIRC4M(void)
{
    SET_BIT(CMU->CLKLP, CMU_CLKLP_STBYIRC4MEN);
}
/**
 * @brief  Disable the irc4m in standby Mode
 * @rmtoll CLKLP    STBYIRC4MEN       LL_CMU_STBY_DisableIRC4M
 * @retval None
 */
__STATIC_INLINE void LL_CMU_STBY_DisableIRC4M(void)
{
    CLEAR_BIT(CMU->CLKLP, CMU_CLKLP_STBYIRC4MEN);
}

/**
  * @brief  Enable the irc4m in stop Mode
  * @rmtoll CLKLP    STOPIRC4MEN       LL_CMU_STOP_EnableIRC4M
  * @retval None
  */
__STATIC_INLINE void LL_CMU_STOP_EnableIRC4M(void)
{
    SET_BIT(CMU->CLKLP, CMU_CLKLP_STOPIRC4MEN);
}
/**
  * @brief  Disable the irc4m in stop Mode
  * @rmtoll CLKLP    STOPIRC4MEN       LL_CMU_STOP_DisableIRC4M
  * @retval None
  */
__STATIC_INLINE void LL_CMU_STOP_DisableIRC4M(void)
{
    CLEAR_BIT(CMU->CLKLP, CMU_CLKLP_STOPIRC4MEN);
}

/**
  * @brief  Enable the irc4m in idle Mode
  * @rmtoll CLKLP    IDLEIRC4MEN       LL_CMU_IDLE_EnableIRC4M
  * @retval None
  */
__STATIC_INLINE void LL_CMU_IDLE_EnableIRC4M(void)
{
    SET_BIT(CMU->CLKLP, CMU_CLKLP_IDLEIRC4MEN);
}
/**
  * @brief  Disable the irc4m in idle Mode
  * @rmtoll CLKLP    IDLEIRC4MEN       LL_CMU_IDLE_DisableIRC4M
  * @retval None
  */
__STATIC_INLINE void LL_CMU_IDLE_DisableIRC4M(void)
{
    CLEAR_BIT(CMU->CLKLP, CMU_CLKLP_IDLEIRC4MEN);
}

// /**
// * @brief  Select the clkin of usb
// * @rmtoll CLKLP    USBCLKINS       LL_CMU_SetUSBClkSource
// * @param  Clkin This parameter can be one of the following values:
// *         @arg @ref LL_CMU_USBCLK_0
// *         @arg @ref LL_CMU_USBCLK_1
// * @retval None
// */
// __STATIC_INLINE void LL_CMU_SetUSBClkSource(uint32_t ClkSrc)
// {
// MODIFY_REG(CMU->CLKLP, CMU_CLKLP_USBCLKINS, ClkSrc);
// }
// /**
// * @brief  Get the clkin of usb
// * @rmtoll CLKLP    USBCLKINS       LL_CMU_GetUSBClkSource
// * @retval Returned value can be one of the following values:
// *         @arg @ref LL_CMU_USBCLK_0
// *         @arg @ref LL_CMU_USBCLK_1
// */
// __STATIC_INLINE uint32_t LL_CMU_GetUSBClkSource(void)
// {
// return (uint32_t)(READ_BIT(CMU->CLKLP, CMU_CLKLP_USBCLKINS));
// }
/**
  * @brief  Enable the irc32k in stop mode
  * @rmtoll CLKSTOP    ENIRC32K       LL_CMU_STOP_EnableIRC32K
  * @retval None
  */
__STATIC_INLINE void LL_CMU_STOP_EnableIRC32K(void)
{
    SET_BIT(CMU->CLKSTOP, CMU_CLKSTOP_EN32K);
}
/**
  * @brief  Disable the irc32k in stop mode
  * @rmtoll CLKSTOP    ENIRC32K       LL_CMU_STOP_DisableIRC32K
  * @retval None
  */
__STATIC_INLINE void LL_CMU_STOP_DisableIRC32K(void)
{
    CLEAR_BIT(CMU->CLKSTOP, CMU_CLKSTOP_EN32K);
}

/**
  * @brief  Enable the dfs in stop mode
  * @rmtoll CLKSTOP    ENDFS       LL_CMU_STOP_EnableDFS
  * @retval None
  */
__STATIC_INLINE void LL_CMU_STOP_EnableDFS(void)
{
    SET_BIT(CMU->CLKSTOP, CMU_CLKSTOP_ENDFS);
}
/**
  * @brief  Disable the dfs in stop mode
  * @rmtoll CLKSTOP    ENDFS       LL_CMU_STOP_DisableDFS
  * @retval None
  */
__STATIC_INLINE void LL_CMU_STOP_DisableDFS(void)
{
    CLEAR_BIT(CMU->CLKSTOP, CMU_CLKSTOP_ENDFS);
}

/**
  * @brief  Enable the fll in stop mode
  * @rmtoll CLKSTOP    ENFLL       LL_CMU_STOP_EnableFLL
  * @retval None
  */
__STATIC_INLINE void LL_CMU_STOP_EnableFLL(void)
{
    SET_BIT(CMU->CLKSTOP, CMU_CLKSTOP_ENFLL);
}
/**
  * @brief  Disable the fll in stop mode
  * @rmtoll CLKSTOP    ENFLL       LL_CMU_STOP_DisableFLL
  * @retval None
  */
__STATIC_INLINE void LL_CMU_STOP_DisableFLL(void)
{
    CLEAR_BIT(CMU->CLKSTOP, CMU_CLKSTOP_ENFLL);
}

/**
  * @brief  Enable the pll in stop mode
  * @rmtoll CLKSTOP    ENPLL       LL_CMU_STOP_EnablePLL
  * @retval None
  */
__STATIC_INLINE void LL_CMU_STOP_EnablePLL(void)
{
    SET_BIT(CMU->CLKSTOP, CMU_CLKSTOP_ENPLL);
}
/**
  * @brief  Disable the pll in stop mode
  * @rmtoll CLKSTOP    ENPLL       LL_CMU_STOP_DisablePLL
  * @retval None
  */
__STATIC_INLINE void LL_CMU_STOP_DisablePLL(void)
{
    CLEAR_BIT(CMU->CLKSTOP, CMU_CLKSTOP_ENPLL);
}

/**
  * @brief  Enable the osc in stop mode
  * @rmtoll CLKSTOP    ENOSC       LL_CMU_STOP_EnableOSC
  * @retval None
  */
__STATIC_INLINE void LL_CMU_STOP_EnableOSC(void)
{
    SET_BIT(CMU->CLKSTOP, CMU_CLKSTOP_ENOSC);
}
/**
  * @brief  Disable the osc in stop mode
  * @rmtoll CLKSTOP    ENOSC       LL_CMU_STOP_DisableOSC
  * @retval None
  */
__STATIC_INLINE void LL_CMU_STOP_DisableOSC(void)
{
    CLEAR_BIT(CMU->CLKSTOP, CMU_CLKSTOP_ENOSC);
}

/**
  * @brief  Enable the irc16m in stop mode
  * @rmtoll CLKSTOP    ENIRC16M       LL_CMU_STOP_EnableIRC16M
  * @retval None
  */
__STATIC_INLINE void LL_CMU_STOP_EnableIRC16M(void)
{
    SET_BIT(CMU->CLKSTOP, CMU_CLKSTOP_EN16M);
}
/**
  * @brief  Disable the irc16m in stop mode
  * @rmtoll CLKSTOP    ENIRC16M       LL_CMU_STOP_DisableIRC16M
  * @retval None
  */
__STATIC_INLINE void LL_CMU_STOP_DisableIRC16M(void)
{
    CLEAR_BIT(CMU->CLKSTOP, CMU_CLKSTOP_EN16M);
}
/**
  * @brief  Enable the irc32k in run/wait mode
  * @rmtoll CLKIDLE    ENIRC32K       LL_CMU_IDLE_EnableIRC32K
  * @retval None
  */
__STATIC_INLINE void LL_CMU_IDLE_EnableIRC32K(void)
{
    SET_BIT(CMU->CLKIDLE, CMU_CLKIDLE_EN32K);
}
/**
  * @brief  Disable the irc32k in run/wait mode
  * @rmtoll CLKIDLE    ENIRC32K       LL_CMU_IDLE_DisableIRC32K
  * @retval None
  */
__STATIC_INLINE void LL_CMU_IDLE_DisableIRC32K(void)
{
    CLEAR_BIT(CMU->CLKIDLE, CMU_CLKIDLE_EN32K);
}

/**
  * @brief  Enable the dfs in run/wait mode
  * @rmtoll CLKIDLE    ENDFS       LL_CMU_IDLE_EnableDFS
  * @retval None
  */
__STATIC_INLINE void LL_CMU_IDLE_EnableDFS(void)
{
    SET_BIT(CMU->CLKIDLE, CMU_CLKIDLE_ENDFS);
}
/**
  * @brief  Disable the dfs in run/wait mode
  * @rmtoll CLKIDLE    ENDFS       LL_CMU_IDLE_DisableDFS
  * @retval None
  */
__STATIC_INLINE void LL_CMU_IDLE_DisableDFS(void)
{
    CLEAR_BIT(CMU->CLKIDLE, CMU_CLKIDLE_ENDFS);
}

/**
  * @brief  Enable the fll in run/wait mode
  * @rmtoll CLKIDLE    ENFLL       LL_CMU_IDLE_EnableFLL
  * @retval None
  */
__STATIC_INLINE void LL_CMU_IDLE_EnableFLL(void)
{
    SET_BIT(CMU->CLKIDLE, CMU_CLKIDLE_ENFLL);
}
/**
  * @brief  Disable the fll in run/wait mode
  * @rmtoll CLKIDLE    ENFLL       LL_CMU_IDLE_DisableFLL
  * @retval None
  */
__STATIC_INLINE void LL_CMU_IDLE_DisableFLL(void)
{
    CLEAR_BIT(CMU->CLKIDLE, CMU_CLKIDLE_ENFLL);
}

/**
  * @brief  Enable the pll in run/wait mode
  * @rmtoll CLKIDLE    ENPLL       LL_CMU_IDLE_EnablePLL
  * @retval None
  */
__STATIC_INLINE void LL_CMU_IDLE_EnablePLL(void)
{
    SET_BIT(CMU->CLKIDLE, CMU_CLKIDLE_ENPLL);
}
/**
  * @brief  Disable the pll in run/wait mode
  * @rmtoll CLKIDLE    ENPLL       LL_CMU_IDLE_DisablePLL
  * @retval None
  */
__STATIC_INLINE void LL_CMU_IDLE_DisablePLL(void)
{
    CLEAR_BIT(CMU->CLKIDLE, CMU_CLKIDLE_ENPLL);
}

/**
  * @brief  Check if the pll clock is Disable in run/wait mode
  * @rmtoll CLKIDLE    ENPLL       LL_CMU_IDLE_IsDisable_PLL
  * @retval 0 or !0
  */
__STATIC_INLINE uint32_t LL_CMU_IDLE_IsEnable_PLL(void)
{
    return READ_BIT(CMU->CLKIDLE, CMU_CLKIDLE_ENPLL);
}

/**
  * @brief  Enable the osc in run/wait mode
  * @rmtoll CLKIDLE    ENOSC       LL_CMU_IDLE_EnableOSC
  * @retval None
  */
__STATIC_INLINE void LL_CMU_IDLE_EnableOSC(void)
{
    SET_BIT(CMU->CLKIDLE, CMU_CLKIDLE_ENOSC);
}
/**
  * @brief  Disable the osc in run/wait mode
  * @rmtoll CLKIDLE    ENOSC       LL_CMU_IDLE_DisableOSC
  * @retval None
  */
__STATIC_INLINE void LL_CMU_IDLE_DisableOSC(void)
{
    CLEAR_BIT(CMU->CLKIDLE, CMU_CLKIDLE_ENOSC);
}

/**
  * @brief  Check if the OSC clock is Disable in run/wait mode
  * @rmtoll CLKIDLE    ENOSC       LL_CMU_IDLE_IsDisable_OSC
  * @retval 0 or !0
  */
__STATIC_INLINE uint32_t LL_CMU_IDLE_IsEnable_OSC(void)
{
    return READ_BIT(CMU->CLKIDLE, CMU_CLKIDLE_ENPLL);
}

/**
  * @brief  Enable the irc16m in run/wait mode
  * @rmtoll CLKIDLE    ENIRC16M       LL_CMU_IDLE_EnableIRC16M
  * @retval None
  */
__STATIC_INLINE void LL_CMU_IDLE_EnableIRC16M(void)
{
    SET_BIT(CMU->CLKIDLE, CMU_CLKIDLE_EN16M);
}
/**
  * @brief  Disable the irc16m in run/wait mode
  * @rmtoll CLKIDLE    ENIRC16M       LL_CMU_IDLE_DisableIRC16M
  * @retval None
  */
__STATIC_INLINE void LL_CMU_IDLE_DisableIRC16M(void)
{
    CLEAR_BIT(CMU->CLKIDLE, CMU_CLKIDLE_EN16M);
}


// /**
// * @brief  Configure IRC16M VEN
// * @rmtoll CLKS0    CFG16MVEN       LL_CMU_IRC16MVENConfig
// * @param  cfg This parameter can be one of the following values:
// *         0~0x03
// * @retval None
// */
// __STATIC_INLINE void LL_CMU_IRC16MVENConfig(uint32_t cfg)
// {
// MODIFY_REG(CMU->CFG_CLKS0, CMU_CFGCLKS0_16MMVEN, cfg);
// }


// /**
// * @brief  select current signal of XTAL
// * @rmtoll CLKS0    XTALISEL       LL_CMU_XtalIsel
// * @param  cfg This parameter can be one of the following values:
// *         0~0x07
// * @retval None
// */
// __STATIC_INLINE void LL_CMU_XtalIsel(uint32_t cfg)
// {
// MODIFY_REG(CMU->CFG_CLKS0, CMU_CFGCLKS0_XTALISEL, cfg);
// }


/**
  * @brief  Bypass xtal
  * @rmtoll CLKS0    XTALBYPS   LL_CMU_EnableXtalBypass
  * @param  None
  * @retval None
  */
__STATIC_INLINE void LL_CMU_EnableXtalBypass(void)
{
    SET_BIT(CMU->CFG_CLKS0, CMU_CFGCLKS0_XTALBYPS);
}

/**
  * @brief  Unbypass xtal
  * @rmtoll CLKS0    XTALBYPS   LL_CMU_DisableXtalBypass
  * @param  None
  * @retval None
  */
__STATIC_INLINE void LL_CMU_DisableXtalBypass(void)
{
    CLEAR_BIT(CMU->CFG_CLKS0, CMU_CFGCLKS0_XTALBYPS);
}
/**
  * @brief  Enable the pll
  * @rmtoll CLKS1    PLLEN,PLLENPWR,PLLPD       LL_CMU_EnablePLL
  * @retval None
  */
__STATIC_INLINE void LL_CMU_EnablePLL(void)
{
    SET_BIT(CMU->CFG_CLKS1, CMU_CFGCLKS1_PLLEN | CMU_CFGCLKS1_PLLENPWR);
    CLEAR_BIT(CMU->CFG_CLKS1, CMU_CFGCLKS1_PLLPD | CMU_CFGCLKS1_PLLRSTDIV);
}

/**
  * @brief  Disable the pll
  * @rmtoll CLKS1    PLLEN,PLLENPWR,PLLPD       LL_CMU_DisablePLL
  * @retval None
  */
__STATIC_INLINE void LL_CMU_DisablePLL(void)
{
    CLEAR_BIT(CMU->CFG_CLKS1, CMU_CFGCLKS1_PLLEN);
}

/**
  * @brief  Select the clock source of pll
  * @rmtoll CLKS1    PLLCLKINS       LL_CMU_SetPLLClkSrc
  * @param  Clkin This parameter can be one of the following values:
  *         @arg @ref LL_CMU_PLLCLK_XTAL
  *         @arg @ref LL_CMU_PLLCLK_IRC16M
  * @retval None
  */
__STATIC_INLINE void LL_CMU_SetPLLClkSrc(uint32_t Clkin)
{
    MODIFY_REG(CMU->CFG_CLKS1, CMU_CFGCLKS1_PLLCLKINS, Clkin);
}
/**
  * @brief  Get the clkin of pll
  * @rmtoll CLKS1    PLLCLKINS       LL_CMU_GetPLLClkSource
  * @retval Returned value can be one of the following values:
  *         @arg @ref LL_CMU_PLLCLK_XTAL
  *         @arg @ref LL_CMU_PLLCLK_IRC16M
  */
__STATIC_INLINE uint32_t LL_CMU_GetPLLClkSrc(void)
{
    return (uint32_t)(READ_BIT(CMU->CFG_CLKS1, CMU_CFGCLKS1_PLLCLKINS));
}

/**
  * @brief  Configure the prectrl and fbctrl of pll
  * @note   f = (16+fbctrl)/(2*(1+prectrl))
  * @rmtoll CLKS1    PLLPRECTRL,PLLFBCTRL       LL_CMU_PLLConfig
  * @param  pre This parameter can be one of the following values:
            0-0x7
  * @param  fb This parameter can be one of the following values:
            0-0x1F
  * @retval None
  */
__STATIC_INLINE void LL_CMU_ConfigPLL(uint32_t pre, uint32_t fb)
{
    CLEAR_BIT(CMU->CFG_CLKS1, CMU_CFGCLKS1_PLLCFG);//
    MODIFY_REG(CMU->CFG_CLKS1, CMU_CFGCLKS1_PLLPRECFG | CMU_CFGCLKS1_PLLFBCFG, (pre << CMU_CFGCLKS1_PLLPRECFG_Pos) | (fb << CMU_CFGCLKS1_PLLFBCFG_Pos));
}
/**
  * @brief  get the fbctrl of pll
  * @note   f = (16+fbctrl)/(2*(1+prectrl))
  * @rmtoll CLKS1    PLLFBCTRL       LL_CMU_GetPLLFB
  * @param None
  * @retval 0-0x1F
  */
__STATIC_INLINE uint32_t LL_CMU_GetPLLFB(void)
{
    return (uint32_t)(READ_BIT(CMU->CFG_CLKS1, CMU_CFGCLKS1_PLLFBCFG) >> CMU_CFGCLKS1_PLLFBCFG_Pos);
}

/**
  * @brief  get the prectrl of pll
  * @note   f = (16+fbctrl)/(2*(1+prectrl))
  * @rmtoll CLKS1    PLLPRECTRL       LL_CMU_GetPLLPRE
  * @param  None
  * @retval 0-0x7
  */
__STATIC_INLINE uint32_t LL_CMU_GetPLLPRE(void)
{
    return (uint32_t)(READ_BIT(CMU->CFG_CLKS1, CMU_CFGCLKS1_PLLPRECFG) >> CMU_CFGCLKS1_PLLPRECFG_Pos);
}

#define LL_CMU_GET_SYSCLKDIV()  ((0x01U<<LL_CMU_GetMCGMainClkPrescaler())*(LL_CMU_GetSysClkPrescaler()+1))

/**
  * @brief  Check if the pll is locked
  * @rmtoll PLLSTA               LL_CMU_PLLIsLock
  * @retval 0 or !0
  */
__STATIC_INLINE uint32_t LL_CMU_IsLock_PLL(void)
{
    return (READ_BIT(CMU->PLLSTA, CMU_PLLSTA_PLLLOCK) == (CMU_PLLSTA_PLLLOCK));
}
/**
  * @brief  Set the ready count of the irc4m clock
  * @rmtoll IRC4MRDY   CNT       LL_CMU_SetIRC4MReadyCNT
  * @retval None
  */
__STATIC_INLINE void LL_CMU_SetIRC4MReadyCnt(uint16_t cnt)
{
    WRITE_REG(CMU->IRC4MRDY, cnt);
}

/**
  * @brief  Set the ready count of the irc16m clock
  * @rmtoll IRC16MRDY   CNT       LL_CMU_SetIRC16MReadyCNT
  * @retval None
  */
__STATIC_INLINE void LL_CMU_SetIRC16MReadyCnt(uint16_t cnt)
{
    WRITE_REG(CMU->IRC16MRDY, cnt);
}
/**
  * @brief  Set the ready count of the pll clock
  * @rmtoll PLLRDY      CNT       LL_CMU_SetPLLReadyCnt
  * @retval None
  */
__STATIC_INLINE void LL_CMU_SetPLLReadyCnt(uint16_t cnt)
{
    WRITE_REG(CMU->PLLRDY, cnt);
}

/**
  * @brief  Set the ready count of the usb48m clock
  * @rmtoll USB48MRDY      CNT       LL_CMU_SetUSB48MReadyCnt
  * @retval None
  */
__STATIC_INLINE void LL_CMU_SetUSB48MReadyCnt(uint16_t cnt)
{
    WRITE_REG(CMU->USB48MRDY, cnt);
}


/**
  * @brief  Check if the GPIO clock is ready
  * @rmtoll CLKVLDF    GPIO     LL_CMU_GPIOClkIsReady
  * @retval 0 or !0
  */
__STATIC_INLINE uint32_t LL_CMU_IsReady_GPIOClk(void)
{
    return (READ_BIT(CMU->CLKVLDF, CMU_CLKVLDF_GPIO) == (CMU_CLKVLDF_GPIO));
}


/**
  * @brief  Check if the USB48M clock is ready
  * @rmtoll CLKVLDF    USB48M       LL_MCU_USB48MIsReady
  * @retval 0 or !0
  */
__STATIC_INLINE uint32_t LL_CMU_IsReady_USB48M(void)
{
    return (READ_BIT(CMU->CLKVLDF, CMU_CLKVLDF_USB48M) == (CMU_CLKVLDF_USB48M));
}

/**
  * @brief  Check if the pll clock is ready
  * @rmtoll CLKVLDF    PLL       LL_CMU_PLLIsReady
  * @retval 0 or !0
  */
__STATIC_INLINE uint32_t LL_CMU_IsReady_PLL(void)
{
    return (READ_BIT(CMU->CLKVLDF, CMU_CLKVLDF_PLL) == (CMU_CLKVLDF_PLL));
}

/**
  * @brief  Check if the osc clock is ready
  * @rmtoll CLKVLDF    OSC       LL_CMU_OSCIsReady
  * @retval 0 or !0
  */
__STATIC_INLINE uint32_t LL_CMU_IsReady_OSC(void)
{
    return (READ_BIT(CMU->CLKVLDF, CMU_CLKVLDF_OSC) == (CMU_CLKVLDF_OSC));
}

/**
  * @brief  Check if the IRC16M clock is ready
  * @rmtoll CLKVLDF    IRC16M       LL_CMU_IRC16MIsReady
  * @retval 0 or !0
  */
__STATIC_INLINE uint32_t LL_CMU_IsReady_IRC16M(void)
{
    return (READ_BIT(CMU->CLKVLDF, CMU_CLKVLDF_IRC16M) == (CMU_CLKVLDF_IRC16M));
}

/**
  * @brief  Configure the MCG_IRC_CLK source
  * @rmtoll MCGCLKA    IRCSEL       LL_CMU_SetMCGIrcClkSource
  * @param  Source This parameter can be one of the following values:
  *         @arg @ref LL_CMU_MCGIRCCLK_IRC4M
  *         @arg @ref LL_CMU_MCGIRCCLK_IRC16M
  * @retval None
  */
__STATIC_INLINE void LL_CMU_SetMCGIrcClkSrc(uint32_t Source)
{
    MODIFY_REG(CMU->MCGCLKA, CMU_MCGCLKA_IRCSEL, Source);
}
/**
  * @brief  Get the MCG_IRC_CLK source
  * @rmtoll MCGCLKA    IRCSEL       LL_CMU_GetMCGIrcClkSource
  * @retval Returned value can be one of the following values:
  *         @arg @ref LL_CMU_MCGIRCCLK_IRC4M
  *         @arg @ref LL_CMU_MCGIRCCLK_IRC16M
  */
__STATIC_INLINE uint32_t LL_CMU_GetMCGIrcClkSrc(void)
{
    return (uint32_t)(READ_BIT(CMU->MCGCLKA, CMU_MCGCLKA_IRCSEL));
}

/**
  * @brief  Set the MCG_IRC_CLK prescaler
  * @rmtoll MCGCLKA    IRCDIV       LL_CMU_SetMCGIrcClkPrescaler
  * @param  Prescaler This parameter can be one of the following values:
  *         @arg @ref LL_CMU_REUSEDIV1_DIV1
  *         @arg @ref LL_CMU_REUSEDIV1_DIV2
  *         @arg @ref LL_CMU_REUSEDIV1_DIV4
  *         @arg @ref LL_CMU_REUSEDIV1_DIV8
  * @retval None
  */
__STATIC_INLINE void LL_CMU_SetMCGIrcClkPrescaler(uint32_t Prescaler)
{
    MODIFY_REG(CMU->MCGCLKA, CMU_MCGCLKA_IRCDIV, Prescaler << CMU_MCGCLKA_IRCDIV_Pos);
}

/**
  * @brief  Get the MCG_IRC_CLK prescaler
  * @rmtoll MCGCLKA    IRCDIV       LL_CMU_GetMCGIrcClkPrescaler
  * @retval Returned value can be one of the following values:
  *         @arg @ref LL_CMU_REUSEDIV1_DIV1
  *         @arg @ref LL_CMU_REUSEDIV1_DIV2
  *         @arg @ref LL_CMU_REUSEDIV1_DIV4
  *         @arg @ref LL_CMU_REUSEDIV1_DIV8
  */
__STATIC_INLINE uint32_t LL_CMU_GetMCGIrcClkPrescaler(void)
{
    return (uint32_t)((READ_BIT(CMU->MCGCLKA, CMU_MCGCLKA_IRCDIV)) >> CMU_MCGCLKA_IRCDIV_Pos);
}


/**
  * @brief  Set the MCG_IO_CLK Source
  * @rmtoll MCGCLKB    SELIOCLKS       LL_CMU_SetMCGIOClkSource
  * @param  Prescaler This parameter can be one of the following values:
  *         @arg @ref LL_CMU_MCGIOCLK_IRCCLK
  *         @arg @ref LL_CMU_MCGIOCLK_IRC4M
  *         @arg @ref LL_CMU_MCGIOCLK_IRC16M
  *         @arg @ref LL_CMU_MCGIOCLK_PLLCLK
  *         @arg @ref LL_CMU_MCGIOCLK_OSCCLK
  *         @arg @ref LL_CMU_MCGIOCLK_USB48M
  *         @arg @ref LL_CMU_MCGIOCLK_PLLDIV
  *         @arg @ref LL_CMU_MCGIOCLK_GPIOCLK
  * @retval None
  */
__STATIC_INLINE void LL_CMU_SetMCGIOClkSrc(uint32_t Source)
{
    MODIFY_REG(CMU->MCGCLKB, CMU_MCGCLKB_SELIOCLKS, Source << CMU_MCGCLKB_SELIOCLKS_Pos);
}

/**
  * @brief  Set the MCG_IO_CLK prescaler
  * @rmtoll MCGCLKB    DIVIOCLKS       LL_CMU_SetMCGIOClkPrescaler
  * @param  Prescaler This parameter can be one of the following values:
  *         @arg @ref LL_CMU_MCGIOCLK_DIV1
  *         @arg @ref LL_CMU_MCGIOCLK_DIV2
  *         @arg @ref LL_CMU_MCGIOCLK_DIV3
  *         @arg @ref LL_CMU_MCGIOCLK_DIV4
  *         @arg @ref LL_CMU_MCGIOCLK_DIV5
  *         @arg @ref LL_CMU_MCGIOCLK_DIV6
  *         @arg @ref LL_CMU_MCGIOCLK_DIV7
  *         @arg @ref LL_CMU_MCGIOCLK_DIV8
  * @retval None
  */
__STATIC_INLINE void LL_CMU_SetMCGIOClkPrescaler(uint32_t Prescaler)
{
    MODIFY_REG(CMU->MCGCLKB, CMU_MCGCLKB_DIVIOCLKS, Prescaler << CMU_MCGCLKB_DIVIOCLKS_Pos);
}

/**
  * @brief  Configure the MCG_MAIN_CLK source
  * @rmtoll MCGCLKA    MCLKSEL       LL_CMU_SetMCGMainClkSource
  * @param  Source This parameter can be one of the following values:
  *         @arg @ref LL_CMU_MCGMAINCLK_IRC4M
  *         @arg @ref LL_CMU_MCGMAINCLK_IRC16M
  *         @arg @ref LL_CMU_MCGMAINCLK_PLLCLK
  *         @arg @ref LL_CMU_MCGMAINCLK_OSCCLK
  *         @arg @ref LL_CMU_MCGMAINCLK_USB48MCLK
  *         @arg @ref LL_CMU_MCGMAINCLK_GPIOCLK
  * @retval None
  */
__STATIC_INLINE void LL_CMU_SetMCGMainClkSrc(uint32_t Source)
{
    MODIFY_REG(CMU->MCGCLKA, CMU_MCGCLKA_MCLKSEL, Source << CMU_MCGCLKA_MCLKSEL_Pos);
}
/**
  * @brief  Get the MCG_MAIN_CLK source
  * @rmtoll MCGCLKA    MCLKSEL       LL_MCU_GetMCGMainClkSource
  * @retval Returned value can be one of the following values:
  *         @arg @ref LL_CMU_MAINCLK_IRC4M
  *         @arg @ref LL_CMU_MAINCLK_IRC16M
  *         @arg @ref LL_CMU_MAINCLK_PLLCLK
  *         @arg @ref LL_CMU_MAINCLK_OSCCLK
  *         @arg @ref LL_CMU_MAINCLK_GPIOCLK
  */
__STATIC_INLINE uint32_t LL_CMU_GetMCGMainClkSrc(void)
{
    return (uint32_t)((READ_BIT(CMU->MCGCLKA, CMU_MCGCLKA_MCLKSEL)) >> CMU_MCGCLKA_MCLKSEL_Pos);
}

/**
  * @brief  Set the MCG_MAIN_CLK prescaler
  * @rmtoll MCGCLKA    MCLKDIV       LL_CMU_SetMCGMainClkPrescaler
  * @param  Prescaler This parameter can be one of the following values:
  *         @arg @ref LL_CMU_REUSEDIV1_DIV1
  *         @arg @ref LL_CMU_REUSEDIV1_DIV2
  *         @arg @ref LL_CMU_REUSEDIV1_DIV4
  *         @arg @ref LL_CMU_REUSEDIV1_DIV8
  * @retval None
  */
__STATIC_INLINE void LL_CMU_SetMCGMainClkPrescaler(uint32_t Prescaler)
{
    MODIFY_REG(CMU->MCGCLKA, CMU_MCGCLKA_MCLKDIV, Prescaler << CMU_MCGCLKA_MCLKDIV_Pos);
}
/**
  * @brief  Get the MCG_MAIN_CLK prescaler
  * @rmtoll MCGCLKA    MCLKDIV       LL_CMU_GetMCGMainClkPrescaler
  * @retval Returned value can be one of the following values:
  *         @arg @ref LL_CMU_REUSEDIV1_DIV1
  *         @arg @ref LL_CMU_REUSEDIV1_DIV2
  *         @arg @ref LL_CMU_REUSEDIV1_DIV4
  *         @arg @ref LL_CMU_REUSEDIV1_DIV8
  */
__STATIC_INLINE uint32_t LL_CMU_GetMCGMainClkPrescaler(void)
{
    return (uint32_t)((READ_BIT(CMU->MCGCLKA, CMU_MCGCLKA_MCLKDIV)) >> CMU_MCGCLKA_MCLKDIV_Pos);
}

/**
  * @brief  Set the mcg_pll_div_clk prescaler
  * @rmtoll MCGCLKA    PLLDIV       LL_CMU_SetPLLClkPrescaler
  * @param  Prescaler This parameter can be one of the following values:
  *         @arg @ref LL_CMU_PLL_DIV1P5
  *         @arg @ref LL_CMU_PLL_DIV02
  *         @arg @ref LL_CMU_PLL_DIV2P5
  *         @arg @ref LL_CMU_PLL_DIV03
  *         @arg @ref LL_CMU_PLL_DIV3P5
  *         @arg @ref LL_CMU_PLL_DIV04
  *         @arg @ref LL_CMU_PLL_DIV4P5
  *         @arg @ref LL_CMU_PLL_DIV05
  *         @arg @ref LL_CMU_PLL_DIV5P5
  *         @arg @ref LL_CMU_PLL_DIV06
  *         @arg @ref LL_CMU_PLL_DIV6P5
  *         @arg @ref LL_CMU_PLL_DIV07
  *         @arg @ref LL_CMU_PLL_DIV7P5
  *         @arg @ref LL_CMU_PLL_DIV08
  *         @arg @ref LL_CMU_PLL_DIV8P5
  *         @arg @ref LL_CMU_PLL_DIV09
  *         @arg @ref LL_CMU_PLL_DIV9P5
  *         @arg @ref LL_CMU_PLL_DIV10
  *         @arg @ref LL_CMU_PLL_DIV10P5
  *         @arg @ref LL_CMU_PLL_DIV11
  *         @arg @ref LL_CMU_PLL_DIV11P5
  *         @arg @ref LL_CMU_PLL_DIV12
  *         @arg @ref LL_CMU_PLL_DIV12P5
  *         @arg @ref LL_CMU_PLL_DIV13
  *         @arg @ref LL_CMU_PLL_DIV13P5
  *         @arg @ref LL_CMU_PLL_DIV14
  *         @arg @ref LL_CMU_PLL_DIV14P5
  *         @arg @ref LL_CMU_PLL_DIV15
  *         @arg @ref LL_CMU_PLL_DIV15P5
  *         @arg @ref LL_CMU_PLL_DIV16
  * @retval None
  */
__STATIC_INLINE void LL_CMU_SetPLLClkPrescaler(uint32_t Prescaler)
{
    MODIFY_REG(CMU->MCGCLKA, CMU_MCGCLKA_PLLDIV, Prescaler << CMU_MCGCLKA_PLLDIV_Pos);
}

/**
  * @brief  Config the bus clk divider
  * @rmtoll MCLKS    BUSCLKDIV       LL_CMU_SetBusClkPrescaler
  * @param  Prescaler This parameter can be one of the following values:
  *         @arg @ref 0x0~0xF,divider=prescaler+1
  *         @arg @ref LL_CMU_REUSEDIV3_DIV1
  *         @arg @ref LL_CMU_REUSEDIV3_DIV2
  *         @arg @ref LL_CMU_REUSEDIV3_DIV3
  *         @arg @ref LL_CMU_REUSEDIV3_DIV4
  *         @arg @ref LL_CMU_REUSEDIV3_DIV5
  *         @arg @ref LL_CMU_REUSEDIV3_DIV6
  *         @arg @ref LL_CMU_REUSEDIV3_DIV7
  *         @arg @ref LL_CMU_REUSEDIV3_DIV8
  *         @arg @ref LL_CMU_REUSEDIV3_DIV9
  *         @arg @ref LL_CMU_REUSEDIV3_DIV10
  *         @arg @ref LL_CMU_REUSEDIV3_DIV11
  *         @arg @ref LL_CMU_REUSEDIV3_DIV12
  *         @arg @ref LL_CMU_REUSEDIV3_DIV13
  *         @arg @ref LL_CMU_REUSEDIV3_DIV14
  *         @arg @ref LL_CMU_REUSEDIV3_DIV15
  *         @arg @ref LL_CMU_REUSEDIV3_DIV16
  * @retval None
  */
__STATIC_INLINE void LL_CMU_SetBusClkPrescaler(uint32_t Prescaler)
{
    MODIFY_REG(CMU->MCLKS, CMU_MCLKS_BUSDIV, Prescaler << CMU_MCLKS_BUSDIV_Pos);
}

/**
  * @brief  get the bus clk divider
  * @rmtoll MCLKS    BUSCLKDIV       LL_CMU_GetBusClkPrescaler
  * @retval Returned value can be one of the following values:
  *         @arg @ref LL_CMU_REUSEDIV3_DIV1
  *         @arg @ref LL_CMU_REUSEDIV3_DIV2
  *         @arg @ref LL_CMU_REUSEDIV3_DIV3
  *         @arg @ref LL_CMU_REUSEDIV3_DIV4
  *         @arg @ref LL_CMU_REUSEDIV3_DIV5
  *         @arg @ref LL_CMU_REUSEDIV3_DIV6
  *         @arg @ref LL_CMU_REUSEDIV3_DIV7
  *         @arg @ref LL_CMU_REUSEDIV3_DIV8
  *         @arg @ref LL_CMU_REUSEDIV3_DIV9
  *         @arg @ref LL_CMU_REUSEDIV3_DIV10
  *         @arg @ref LL_CMU_REUSEDIV3_DIV11
  *         @arg @ref LL_CMU_REUSEDIV3_DIV12
  *         @arg @ref LL_CMU_REUSEDIV3_DIV13
  *         @arg @ref LL_CMU_REUSEDIV3_DIV14
  *         @arg @ref LL_CMU_REUSEDIV3_DIV15
  *         @arg @ref LL_CMU_REUSEDIV3_DIV16
  */
__STATIC_INLINE uint32_t LL_CMU_GetBusClkPrescaler(void)
{
    return (uint32_t)((READ_BIT(CMU->MCLKS, CMU_MCLKS_BUSDIV)) >> CMU_MCLKS_BUSDIV_Pos);
}

/**
  * @brief  Configure the security clk source
  * @rmtoll MCLKS    SYSCLKDIV       LL_CMU_SetSecuClkSrc
  * @param  Prescaler This parameter can be one of the following values:
  *         @arg @ref LL_CMU_SECCLK_SYNSYSCLK
  *         @arg @ref LL_CMU_SECCLK_RNDSYSCLK
  * @retval None
  */
__STATIC_INLINE void LL_CMU_SetSecuClkSrc(uint32_t Source)
{
    MODIFY_REG(CMU->MCLKS, CMU_MCLKS_DFSSCY, Source);
}

/**
  * @brief  Config the sys clk divider
  * @rmtoll MCLKS    SYSCLKDIV       LL_CMU_SetSysClkPrescaler
  * @param  Prescaler This parameter can be one of the following values:
  *         @arg @ref 0x0~0xF,divider=prescaler+1
  *         @arg @ref LL_CMU_REUSEDIV3_DIV1
  *         @arg @ref LL_CMU_REUSEDIV3_DIV2
  *         @arg @ref LL_CMU_REUSEDIV3_DIV3
  *         @arg @ref LL_CMU_REUSEDIV3_DIV4
  *         @arg @ref LL_CMU_REUSEDIV3_DIV5
  *         @arg @ref LL_CMU_REUSEDIV3_DIV6
  *         @arg @ref LL_CMU_REUSEDIV3_DIV7
  *         @arg @ref LL_CMU_REUSEDIV3_DIV8
  *         @arg @ref LL_CMU_REUSEDIV3_DIV9
  *         @arg @ref LL_CMU_REUSEDIV3_DIV10
  *         @arg @ref LL_CMU_REUSEDIV3_DIV11
  *         @arg @ref LL_CMU_REUSEDIV3_DIV12
  *         @arg @ref LL_CMU_REUSEDIV3_DIV13
  *         @arg @ref LL_CMU_REUSEDIV3_DIV14
  *         @arg @ref LL_CMU_REUSEDIV3_DIV15
  *         @arg @ref LL_CMU_REUSEDIV3_DIV16
  * @retval None
  */
__STATIC_INLINE void LL_CMU_SetSysClkPrescaler(uint32_t Prescaler)
{
    MODIFY_REG(CMU->MCLKS, CMU_MCLKS_SYSDIV, Prescaler << CMU_MCLKS_SYSDIV_Pos);
}

/**
  * @brief  Config the sys clk divider
  * @rmtoll MCLKS    SYSCLKDIV       LL_CMU_GetSysClkPrescaler
  * @param  None
  * @retval Prescaler This parameter can be one of the following values:
  *         @arg @ref 0x0~0xF,divider=prescaler+1
  *         @arg @ref LL_CMU_REUSEDIV3_DIV1
  *         @arg @ref LL_CMU_REUSEDIV3_DIV2
  *         @arg @ref LL_CMU_REUSEDIV3_DIV3
  *         @arg @ref LL_CMU_REUSEDIV3_DIV4
  *         @arg @ref LL_CMU_REUSEDIV3_DIV5
  *         @arg @ref LL_CMU_REUSEDIV3_DIV6
  *         @arg @ref LL_CMU_REUSEDIV3_DIV7
  *         @arg @ref LL_CMU_REUSEDIV3_DIV8
  *         @arg @ref LL_CMU_REUSEDIV3_DIV9
  *         @arg @ref LL_CMU_REUSEDIV3_DIV10
  *         @arg @ref LL_CMU_REUSEDIV3_DIV11
  *         @arg @ref LL_CMU_REUSEDIV3_DIV12
  *         @arg @ref LL_CMU_REUSEDIV3_DIV13
  *         @arg @ref LL_CMU_REUSEDIV3_DIV14
  *         @arg @ref LL_CMU_REUSEDIV3_DIV15
  *         @arg @ref LL_CMU_REUSEDIV3_DIV16
  */
__STATIC_INLINE uint32_t LL_CMU_GetSysClkPrescaler(void)
{
    return (uint32_t)(READ_BIT(CMU->MCLKS, CMU_MCLKS_SYSDIV) >> CMU_MCLKS_SYSDIV_Pos);
}

/**
  * @brief  Set the MDL_TRNGCLK prescaler
  * @rmtoll MDLCLKA_TRNGDIV       LL_CMU_SetTRngClkPrescaler
  * @param  Prescaler This parameter can be one of the following values:
  *         @arg @ref LL_CMU_REUSEDIV2_DIV2
  *         @arg @ref LL_CMU_REUSEDIV2_DIV4
  *         @arg @ref LL_CMU_REUSEDIV2_DIV8
  *         @arg @ref LL_CMU_REUSEDIV2_DIV16
  *         @arg @ref LL_CMU_REUSEDIV2_DIV32
  *         @arg @ref LL_CMU_REUSEDIV2_DIV64
  *         @arg @ref LL_CMU_REUSEDIV2_DIV128
  *         @arg @ref LL_CMU_REUSEDIV2_DIV256
  * @retval None
  */
__STATIC_INLINE void LL_CMU_SetTRngClkPrescaler(uint32_t Prescaler)
{
    MODIFY_REG(CMU->MDLCLKA, CMU_MDLCLKA_TRNGDIV, Prescaler << CMU_MDLCLKA_TRNGDIV_Pos);
}

/**
  * @brief  Configure the MDL_CtCLK source
  * @rmtoll MDLCLKA_CTSEL      LL_CMU_SetCtClkSrc
  * @param  Source This parameter can be one of the following values:
  *         @arg @ref LL_CMU_CTCLK_SYSCLK
  *         @arg @ref LL_CMU_CTCLK_IOCLK
  *         @arg @ref LL_CMU_CTCLK_IRC16M
  *         @arg @ref LL_CMU_CTCLK_OSCCLK
  * @retval None
  */
__STATIC_INLINE void LL_CMU_SetCtClkSrc(uint32_t Source)
{
    MODIFY_REG(CMU->MDLCLKA, CMU_MDLCLKA_CTSEL, (Source << CMU_MDLCLKA_CTSEL_Pos));
}
/**
  * @brief  Get the MDL_CT7816 CLK source
  * @rmtoll MDLCLKA_CTSEL      LL_CMU_GetCtClkSrc
  * @retval Returned value can be one of the following values:
  *         @arg @ref LL_CMU_CTCLK_SYSCLK
  *         @arg @ref LL_CMU_CTCLK_IOCLK
  *         @arg @ref LL_CMU_CTCLK_IRC16M
  *         @arg @ref LL_CMU_CTCLK_OSCCLK
  */
__STATIC_INLINE uint32_t LL_CMU_GetCtClkSrc(void)
{
    return (uint32_t)((READ_BIT(CMU->MDLCLKA, CMU_MDLCLKA_CTSEL) >> CMU_MDLCLKA_CTSEL_Pos));
}

/**
  * @brief  Set the MDL_CT7816 CLK prescaler
  * @rmtoll MDLCLKA_CTDIV       LL_CMU_SetCtClkPrescaler
  * @param  Prescaler This parameter can be one of the following values:
  *         @arg @ref LL_CMU_REUSEDIV1_DIV1
  *         @arg @ref LL_CMU_REUSEDIV1_DIV2
  *         @arg @ref LL_CMU_REUSEDIV1_DIV4
  *         @arg @ref LL_CMU_REUSEDIV1_DIV8
  * @retval None
  */
__STATIC_INLINE void LL_CMU_SetCtClkPrescaler(uint32_t Prescaler)
{
    MODIFY_REG(CMU->MDLCLKA, CMU_MDLCLKA_CTDIV, Prescaler << CMU_MDLCLKA_CTDIV_Pos);
}
/**
  * @brief  Get the MDL_CT7816 CLK prescaler
  * @rmtoll MDLCLKA_CTDIV       LL_CMU_GetCtClkPrescaler
  * @retval Returned value can be one of the following values:
  *         @arg @ref LL_CMU_REUSEDIV1_DIV1
  *         @arg @ref LL_CMU_REUSEDIV1_DIV2
  *         @arg @ref LL_CMU_REUSEDIV1_DIV4
  *         @arg @ref LL_CMU_REUSEDIV1_DIV8
  */
__STATIC_INLINE uint32_t LL_CMU_GetCtClkPrescaler(void)
{
    return (uint32_t)((READ_BIT(CMU->MDLCLKA, CMU_MDLCLKA_CTDIV)) >> CMU_MDLCLKA_CTDIV_Pos);
}

/**
  * @brief  Config CT Clk
  * @param  Clk Source This parameter can be one of the following values:
  *         @arg @ref LL_CMU_CTCLK_SYSCLK
  *         @arg @ref LL_CMU_CTCLK_IOCLK
  *         @arg @ref LL_CMU_CTCLK_IRC16M
  *         @arg @ref LL_CMU_CTCLK_OSCCLK
  * @param  Clk Divider This parameter can be one of the following values:
  *         @arg @ref LL_CMU_REUSEDIV1_DIV1
  *         @arg @ref LL_CMU_REUSEDIV1_DIV2
  *         @arg @ref LL_CMU_REUSEDIV1_DIV4
  *         @arg @ref LL_CMU_REUSEDIV1_DIV8
  * @retval None
  */
__STATIC_INLINE void LL_CMU_ConfigCtClk(uint32_t ClkSrc, uint32_t ClkDiv)
{
    MODIFY_REG(CMU->MDLCLKA, CMU_MDLCLKA_CTDIV | CMU_MDLCLKA_CTSEL, (ClkSrc << CMU_MDLCLKA_CTSEL_Pos) | (ClkDiv << CMU_MDLCLKA_CTDIV_Pos));
}

/**
  * @brief  Configure the MDL_UARTCLK source
  * @rmtoll MDLCLKA_UARTSEL      LL_CMU_SetUartClkSrc
  * @param  Source This parameter can be one of the following values:
  *         @arg @ref LL_CMU_UARTCLK_SYSCLK
  *         @arg @ref LL_CMU_UARTCLK_IOCLK
  *         @arg @ref LL_CMU_UARTCLK_IRC16M
  *         @arg @ref LL_CMU_UARTCLK_OSCCLK
  * @retval None
  */
__STATIC_INLINE void LL_CMU_SetUARTClkSrc(uint32_t Source)
{
    MODIFY_REG(CMU->MDLCLKA, CMU_MDLCLKA_UARTSEL, (Source << CMU_MDLCLKA_UARTSEL_Pos));
}
/**
  * @brief  Get the MDL_UARTCLK source
  * @rmtoll MDLCLKA_UARTSEL      LL_CMU_GetUartClkSrc
  * @retval Returned value can be one of the following values:
  *         @arg @ref LL_CMU_UARTCLK_SYSCLK
  *         @arg @ref LL_CMU_UARTCLK_IOCLK
  *         @arg @ref LL_CMU_UARTCLK_IRC16M
  *         @arg @ref LL_CMU_UARTCLK_OSCCLK
  */
__STATIC_INLINE uint32_t LL_CMU_GetUARTClkSrc(void)
{
    return (uint32_t)((READ_BIT(CMU->MDLCLKA, CMU_MDLCLKA_UARTSEL) >> CMU_MDLCLKA_UARTSEL_Pos));
}

/**
  * @brief  Set the MDL_UARTCLK prescaler
  * @rmtoll MDLCLKA_UARTDIV       LL_CMU_SetUartClkPrescaler
  * @param  Prescaler This parameter can be one of the following values:
  *         @arg @ref LL_CMU_REUSEDIV1_DIV1
  *         @arg @ref LL_CMU_REUSEDIV1_DIV2
  *         @arg @ref LL_CMU_REUSEDIV1_DIV4
  *         @arg @ref LL_CMU_REUSEDIV1_DIV8
  * @retval None
  */
__STATIC_INLINE void LL_CMU_SetUARTClkPrescaler(uint32_t Prescaler)
{
    MODIFY_REG(CMU->MDLCLKA, CMU_MDLCLKA_UARTDIV, Prescaler << CMU_MDLCLKA_UARTDIV_Pos);
}
/**
  * @brief  Get the MDL_UARTCLK prescaler
  * @rmtoll MDLCLKA_UARTDIV       LL_CMU_GetUartClkPrescaler
  * @retval Returned value can be one of the following values:
  *         @arg @ref LL_CMU_REUSEDIV1_DIV1
  *         @arg @ref LL_CMU_REUSEDIV1_DIV2
  *         @arg @ref LL_CMU_REUSEDIV1_DIV4
  *         @arg @ref LL_CMU_REUSEDIV1_DIV8
  */
__STATIC_INLINE uint32_t LL_CMU_GetUARTClkPrescaler(void)
{
    return (uint32_t)((READ_BIT(CMU->MDLCLKA, CMU_MDLCLKA_UARTDIV)) >> CMU_MDLCLKA_UARTDIV_Pos);
}
/**
  * @brief  Config Uart Clk
  * @rmtoll MDLCLKA CMU_MDLCLKA_UARTSEL,CMU_MDLCLKA_UARTDIV LL_CMU_UARTClkConfig
  * @param  Clk Source This parameter can be one of the following values:
  *         @arg @ref LL_CMU_UARTCLK_SYSCLK
  *         @arg @ref LL_CMU_UARTCLK_IOCLK
  *         @arg @ref LL_CMU_UARTCLK_IRC16M
  *         @arg @ref LL_CMU_UARTCLK_OSCCLK
  * @param  Clk Divider This parameter can be one of the following values:
  *         @arg @ref LL_CMU_REUSEDIV1_DIV1
  *         @arg @ref LL_CMU_REUSEDIV1_DIV2
  *         @arg @ref LL_CMU_REUSEDIV1_DIV4
  *         @arg @ref LL_CMU_REUSEDIV1_DIV8
  * @retval None
  */
__STATIC_INLINE void LL_CMU_ConfigUARTClk(uint32_t ClkSrc, uint32_t ClkDiv)
{
    MODIFY_REG(CMU->MDLCLKA, CMU_MDLCLKA_UARTDIV | CMU_MDLCLKA_UARTSEL, (ClkSrc << CMU_MDLCLKA_UARTSEL_Pos) | (ClkDiv << CMU_MDLCLKA_UARTDIV_Pos));
}


/**
  * @brief  Config USBFS Clk Source
  * @rmtoll MDLCLKA CMU_MDLCLKA_USBFSSEL LL_CMU_SetUSBFSClkSource
  * @param  ClkSource
  * @param  Clk Source This parameter can be one of the following values:
  *         @arg @ref LL_CMU_USBFSCLK_PLL
  *         @arg @ref LL_CMU_USBFSCLK_USB48M
  *         @arg @ref LL_CMU_USBFSCLK_PLLDIV
  *         @arg @ref LL_CMU_USBFSCLK_SYSTEST
  * @retval None
  */
__STATIC_INLINE void LL_CMU_SetUSBFSClkSrc(uint32_t ClkSrc)
{
    MODIFY_REG(CMU->MDLCLKA, CMU_MDLCLKA_USBFSSEL, ClkSrc << CMU_MDLCLKA_USBFSSEL_Pos);
}

/**
  * @brief  Config Shield Clk
  * @rmtoll MDLCLKA CMU_MDLCLKA_UARTSEL,CMU_MDLCLKA_UARTDIV LL_CMU_ShieldClkConfig
  * @param  Clk Source This parameter can be one of the following values:
  *         @arg @ref LL_CMU_SHIELDCLK_IRC4M
  *         @arg @ref LL_CMU_SHIELDCLK_LPO1K
  * @param  Clk Divider This parameter can be one of the following values:
  *         @arg @ref LL_CMU_SHIELDCLK_DIV1
  *         @arg @ref LL_CMU_SHIELDCLK_DIV2
  *         @arg @ref LL_CMU_SHIELDCLK_DIV4
  *         @arg @ref LL_CMU_SHIELDCLK_DIV8
  *         @arg @ref LL_CMU_SHIELDCLK_DIV16
  *         @arg @ref LL_CMU_SHIELDCLK_DIV32
  *         @arg @ref LL_CMU_SHIELDCLK_DIV64
  *         @arg @ref LL_CMU_SHIELDCLK_DIV128
  * @retval None
  */
__STATIC_INLINE void LL_CMU_ConfigShieldClk(uint32_t ClkSrc, uint32_t ClkDiv)
{
    MODIFY_REG(CMU->MDLCLKB, CMU_MDLCLKB_SLDDIV | CMU_MDLCLKB_SLDSEL, (ClkSrc << CMU_MDLCLKB_SLDSEL_Pos) | (ClkDiv << CMU_MDLCLKB_SLDDIV_Pos));
}

/**
  * @brief  Configure the ALWAYSON bus source
  * @param  Source This parameter can be one of the following values:
  *         @arg @ref LL_CMU_AONBUSCLK_BUS
  *         @arg @ref LL_CMU_AONBUSCLK_BUSDIV2
  *         @arg @ref LL_CMU_AONBUSCLK_BUSDIV3
  *         @arg @ref LL_CMU_AONBUSCLK_BUSDIV4
  * @retval None
  */
__STATIC_INLINE void LL_CMU_SetAonBusClkSrc(uint32_t Source)
{
    MODIFY_REG(CMU->MDLCLKB, CMU_MDLCLKB_AONBUSCLK, (Source << CMU_MDLCLKB_AONBUSCLK_Pos));
}

/**
  * @brief  Configure the RTC MIAN Bus CLK Prescaler
  * @param  Source This parameter can be one of the following values:
  *         @arg @ref LL_CMU_RTCMAINBUSCLK_BUS
  *         @arg @ref LL_CMU_RTCMAINBUSCLK_BUSDIV2
  *         @arg @ref LL_CMU_RTCMAINBUSCLK_BUSDIV3
  *         @arg @ref LL_CMU_RTCMAINBUSCLK_BUSDIV4
  * @retval None
  */
__STATIC_INLINE void LL_CMU_SetRtcMainBusClkPrescaler(uint32_t Source)
{
    MODIFY_REG(CMU->MDLCLKB, CMU_MDLCLKB_DIVRTCMBCLK, Source << CMU_MDLCLKB_DIVRTCMBCLK_Pos);
}

/**
  * @brief  Configure the WDT CLK source
  * @param  Source This parameter can be one of the following values:
  *         @arg @ref LL_CMU_WDTCLK_BUSCLK
  *         @arg @ref LL_CMU_WDTCLK_LPO1K
  * @retval None
  */
__STATIC_INLINE void LL_CMU_SetWDTClkSrc(uint32_t Source)
{
    MODIFY_REG(CMU->MDLCLKB, CMU_MDLCLKB_WDTSEL, Source << CMU_MDLCLKB_WDTSEL_Pos);
}

/**
  * @brief  Config ADC Clk
  * @rmtoll MDLCLKA CMU_MDLCLKA_UARTSEL,CMU_MDLCLKA_UARTDIV LL_CMU_ADCClkConfig
  * @param  Clk Source This parameter can be one of the following values:
  *         @arg @ref LL_CMU_ADCCLK_IRC4M
  *         @arg @ref LL_CMU_ADCCLK_IRC16M
  *         @arg @ref LL_CMU_ADCCLK_OSCCLK
  *         @arg @ref LL_CMU_ADCCLK_PLLDIV
  * @param  Clk Divider This parameter can be one of the following values:
  *         @arg @ref LL_CMU_REUSEDIV1_DIV1
  *         @arg @ref LL_CMU_REUSEDIV1_DIV2
  *         @arg @ref LL_CMU_REUSEDIV1_DIV4
  *         @arg @ref LL_CMU_REUSEDIV1_DIV8
  * @retval None
  */
__STATIC_INLINE void LL_CMU_ConfigADCClk(uint32_t ClkSrc, uint32_t ClkDiv)
{
    MODIFY_REG(CMU->MDLCLKB, CMU_MDLCLKB_ADCDIV | CMU_MDLCLKB_ADCSEL, (ClkSrc << CMU_MDLCLKB_ADCSEL_Pos) | (ClkDiv << CMU_MDLCLKB_ADCDIV_Pos));
}

/**
  * @brief  Configure the CLKOUT0 source
  * @param  Source This parameter can be one of the following values:
  *         @arg @ref LL_CMU_COUTCLK_SYSTEST
  *         @arg @ref LL_CMU_COUTCLK_IRC4M
  *         @arg @ref LL_CMU_COUTCLK_IRC16M
  *         @arg @ref LL_CMU_COUTCLK_PLLCLK
  *         @arg @ref LL_CMU_COUTCLK_OSCCLK
  *         @arg @ref LL_CMU_COUTCLK_PLLDIV8
  *         @arg @ref LL_CMU_COUTCLK_PLLDIV
  *         @arg @ref LL_CMU_COUTCLK_GPIOCLK
  *         @arg @ref LL_CMU_COUTCLK_TRNGANA
  *         @arg @ref LL_CMU_COUTCLK_USB48M
  *         @arg @ref LL_CMU_COUTCLK_SYSCLK
  *         @arg @ref LL_CMU_COUTCLK_SECCLK
  *         @arg @ref LL_CMU_COUTCLK_OSC32K
  * @retval None
  */
__STATIC_INLINE void LL_CMU_SetCout0ClkSrc(uint32_t Source)
{
    MODIFY_REG(CMU->MDLCLKB, CMU_MDLCLKB_TST0SEL, Source << CMU_MDLCLKB_TST0SEL_Pos);
}

/**
  * @brief  Configure the CLKOUT1 source
  * @param  Source This parameter can be one of the following values:
  *         @arg @ref LL_CMU_COUTCLK_SYSTEST
  *         @arg @ref LL_CMU_COUTCLK_IRC4M
  *         @arg @ref LL_CMU_COUTCLK_IRC16M
  *         @arg @ref LL_CMU_COUTCLK_PLLCLK
  *         @arg @ref LL_CMU_COUTCLK_OSCCLK
  *         @arg @ref LL_CMU_COUTCLK_PLLDIV8
  *         @arg @ref LL_CMU_COUTCLK_PLLDIV
  *         @arg @ref LL_CMU_COUTCLK_GPIOCLK
  *         @arg @ref LL_CMU_COUTCLK_TRNGANA
  *         @arg @ref LL_CMU_COUTCLK_USB48M
  *         @arg @ref LL_CMU_COUTCLK_SYSCLK
  *         @arg @ref LL_CMU_COUTCLK_SECCLK
  *         @arg @ref LL_CMU_COUTCLK_OSC32K
  * @retval None
  */
__STATIC_INLINE void LL_CMU_SetCout1ClkSrc(uint32_t Source)
{
    MODIFY_REG(CMU->MDLCLKB, CMU_MDLCLKB_TST1SEL, Source << CMU_MDLCLKB_TST1SEL_Pos);
}

/**
  * @brief  Configure the CLKOUT2 source
  * @param  Source This parameter can be one of the following values:
  *         @arg @ref LL_CMU_COUTCLK_SYSTEST
  *         @arg @ref LL_CMU_COUTCLK_IRC4M
  *         @arg @ref LL_CMU_COUTCLK_IRC16M
  *         @arg @ref LL_CMU_COUTCLK_PLLCLK
  *         @arg @ref LL_CMU_COUTCLK_OSCCLK
  *         @arg @ref LL_CMU_COUTCLK_PLLDIV8
  *         @arg @ref LL_CMU_COUTCLK_PLLDIV
  *         @arg @ref LL_CMU_COUTCLK_GPIOCLK
  *         @arg @ref LL_CMU_COUTCLK_TRNGANA
  *         @arg @ref LL_CMU_COUTCLK_USB48M
  *         @arg @ref LL_CMU_COUTCLK_SYSCLK
  *         @arg @ref LL_CMU_COUTCLK_SECCLK
  *         @arg @ref LL_CMU_COUTCLK_OSC32K
  * @retval None
  */
__STATIC_INLINE void LL_CMU_SetCout2ClkSrc(uint32_t Source)
{
    MODIFY_REG(CMU->MDLCLKB, CMU_MDLCLKB_TST2SEL, Source << CMU_MDLCLKB_TST2SEL_Pos);
}

/**
  * @brief  Configure the BKP TEST CLK Prescaler
  * @param  Source This parameter can be one of the following values:
  *         @arg @ref LL_CMU_REUSEDIV2_DIV2
  *         @arg @ref LL_CMU_REUSEDIV2_DIV4
  *         @arg @ref LL_CMU_REUSEDIV2_DIV8
  *         @arg @ref LL_CMU_REUSEDIV2_DIV16
  *         @arg @ref LL_CMU_REUSEDIV2_DIV32
  *         @arg @ref LL_CMU_REUSEDIV2_DIV64
  *         @arg @ref LL_CMU_REUSEDIV2_DIV128
  *         @arg @ref LL_CMU_REUSEDIV2_DIV256
  * @retval None
  */
__STATIC_INLINE void LL_CMU_SetBkpTestClkPrescaler(uint32_t Prescaler)
{
    MODIFY_REG(CMU->MDLCLKB, CMU_MDLCLKB_DIVBKPTSTCLK, Prescaler << CMU_MDLCLKB_DIVBKPTSTCLK_Pos);
}

/**
  * @brief  Configure the Stimer source
  * @rmtoll MDLCLKC    STIMER n SEL       LL_CMU_SetStimerClkSource
  * @param  STn_Type STn(0~5)
  * @param  Source This parameter can be one of the following values:
  *         @arg @ref LL_CMU_STCLK_SYSCLK
  *         @arg @ref LL_CMU_STCLK_PLLCLK
  *         @arg @ref LL_CMU_STCLK_OSCCLK
  *         @arg @ref LL_CMU_STCLK_IRC4M
  *         @arg @ref LL_CMU_STCLK_IRC16M
  *         @arg @ref LL_CMU_STCLK_GPIOCLK
  *         @arg @ref LL_CMU_STCLK_PLLDIV
  *         @arg @ref LL_CMU_STCLK_USB48M
  * @retval None
  */
__STATIC_INLINE void LL_CMU_SetStimerClkSrc(uint8_t STn, uint32_t Source)
{
    MODIFY_REG(CMU->MDLCLKC, CMU_MDLCLKC_STMCLKSEL(STn), Source << CMU_MDLCLKC_STMCLKSEL_Pos(STn));
}

/**
  * @brief  Get the Stimer source
  * @rmtoll MDLCLKC    STIMER n       LL_CMU_GetStimerClkSrc
  * @retval Returned value can be one of the following values:
  * @param  STn_Type STn(0~5)
  * @param  Source This parameter can be one of the following values:
  *         @arg @ref LL_CMU_STCLK_SYSCLK
  *         @arg @ref LL_CMU_STCLK_PLLCLK
  *         @arg @ref LL_CMU_STCLK_OSCCLK
  *         @arg @ref LL_CMU_STCLK_IRC4M
  *         @arg @ref LL_CMU_STCLK_IRC16M
  *         @arg @ref LL_CMU_STCLK_GPIOCLK
  *         @arg @ref LL_CMU_STCLK_PLLDIV
  *         @arg @ref LL_CMU_STCLK_USB48M
  */
__STATIC_INLINE uint32_t LL_CMU_GetStimerClkSrc(uint8_t STn)
{
    return (uint32_t)(READ_BIT(CMU->MDLCLKC, CMU_MDLCLKC_STMCLKSEL(STn)) >> CMU_MDLCLKC_STMCLKSEL_Pos(STn));
}

/**
  * @brief  Config USBPHY Clk
  * @rmtoll MDLCLKA CMU_MDLCLKA_UARTSEL,CMU_MDLCLKA_UARTDIV LL_CMU_USBPHYClkConfig
  * @param  Clk Source This parameter can be one of the following values:
  *         @arg @ref LL_CMU_USBPHYCLK_OSCCLK
  *         @arg @ref LL_CMU_USBPHYCLK_PLLCLK
  *         @arg @ref LL_CMU_USBPHYCLK_GPIOCLK
  *         @arg @ref LL_CMU_USBPHYCLK_USB48M
  * @param  Clk Divider This parameter can be one of the following values:
  *         @arg @ref LL_CMU_REUSEDIV3_DIV1
  *         @arg @ref LL_CMU_REUSEDIV3_DIV2
  *         @arg @ref LL_CMU_REUSEDIV3_DIV3
  *         @arg @ref LL_CMU_REUSEDIV3_DIV4
  *         @arg @ref LL_CMU_REUSEDIV3_DIV5
  *         @arg @ref LL_CMU_REUSEDIV3_DIV6
  *         @arg @ref LL_CMU_REUSEDIV3_DIV7
  *         @arg @ref LL_CMU_REUSEDIV3_DIV8
  *         @arg @ref LL_CMU_REUSEDIV3_DIV9
  *         @arg @ref LL_CMU_REUSEDIV3_DIV10
  *         @arg @ref LL_CMU_REUSEDIV3_DIV11
  *         @arg @ref LL_CMU_REUSEDIV3_DIV12
  *         @arg @ref LL_CMU_REUSEDIV3_DIV13
  *         @arg @ref LL_CMU_REUSEDIV3_DIV14
  *         @arg @ref LL_CMU_REUSEDIV3_DIV15
  *         @arg @ref LL_CMU_REUSEDIV3_DIV16

  * @retval None
  */
__STATIC_INLINE void LL_CMU_ConfigUSBPhyClk(uint32_t ClkSrc, uint32_t ClkDiv)
{
    MODIFY_REG(CMU->MDLCLKC, CMU_MDLCLKC_USBPHYCLKSEL | CMU_MDLCLKC_USBPHYCLKDIV, (ClkSrc << CMU_MDLCLKC_USBPHYCLKSEL_Pos) | (ClkDiv << CMU_MDLCLKC_USBPHYCLKDIV_Pos));
}

/**
  * @brief  Set the systick ref clk config
  * @rmtoll LL_CMU_SysTickRefClkConfig
  * @param  Clk Source This parameter can be one of the following values:
  *         @arg @ref LL_CMU_SYSTICKREFCLK_IRC4M
  *         @arg @ref LL_CMU_SYSTICKREFCLK_IRC16M
  *         @arg @ref LL_CMU_SYSTICKREFCLK_OSCCLK
  *         @arg @ref LL_CMU_SYSTICKREFCLK_GPIOCLK
  * @param  Prescaler This parameter can be one of the following values:
  *         @arg @ref LL_CMU_REUSEDIV2_DIV2
  *         @arg @ref LL_CMU_REUSEDIV2_DIV4
  *         @arg @ref LL_CMU_REUSEDIV2_DIV8
  *         @arg @ref LL_CMU_REUSEDIV2_DIV16
  *         @arg @ref LL_CMU_REUSEDIV2_DIV32
  *         @arg @ref LL_CMU_REUSEDIV2_DIV64
  *         @arg @ref LL_CMU_REUSEDIV2_DIV128
  *         @arg @ref LL_CMU_REUSEDIV2_DIV256
  * @retval None
  */
__STATIC_INLINE void LL_CMU_ConfigSysTickRefClk(uint32_t ClkSrc, uint32_t ClkDiv)
{
    MODIFY_REG(CMU->MDLCLKD, CMU_MDLCLKC_SELSYSTKRFCLK | CMU_MDLCLKC_DIVSYSTKRFCLK, (ClkSrc << CMU_MDLCLKC_SELSYSTKRFCLK_Pos) | (ClkDiv << CMU_MDLCLKC_DIVSYSTKRFCLK_Pos));
}

/**
  * @brief  Config SYS TEST Clk Prescaler
  * @rmtoll LL_CMU_SetSysTestClkPrescaler
  * @param  Clk Source This parameter can be one of the following values:
  *         @arg @ref LL_CMU_REUSEDIV3_DIV1
  *         @arg @ref LL_CMU_REUSEDIV3_DIV2
  *         @arg @ref LL_CMU_REUSEDIV3_DIV3
  *         @arg @ref LL_CMU_REUSEDIV3_DIV4
  *         @arg @ref LL_CMU_REUSEDIV3_DIV5
  *         @arg @ref LL_CMU_REUSEDIV3_DIV6
  *         @arg @ref LL_CMU_REUSEDIV3_DIV7
  *         @arg @ref LL_CMU_REUSEDIV3_DIV8
  *         @arg @ref LL_CMU_REUSEDIV3_DIV9
  *         @arg @ref LL_CMU_REUSEDIV3_DIV10
  *         @arg @ref LL_CMU_REUSEDIV3_DIV11
  *         @arg @ref LL_CMU_REUSEDIV3_DIV12
  *         @arg @ref LL_CMU_REUSEDIV3_DIV13
  *         @arg @ref LL_CMU_REUSEDIV3_DIV14
  *         @arg @ref LL_CMU_REUSEDIV3_DIV15
  *         @arg @ref LL_CMU_REUSEDIV3_DIV16

  * @retval None
  */
__STATIC_INLINE void LL_CMU_SetSysTestClkPrescaler(uint32_t ClkDiv)
{
    MODIFY_REG(CMU->MDLCLKD, CMU_MDLCLKC_SYSTESTCLK, (ClkDiv) << CMU_MDLCLKC_SYSTESTCLK_Pos);
}

/**
  * @brief  Config PLLEXT Clk
  * @rmtoll LL_CMU_PLLEXTClkConfig
  * @param  Clk Source This parameter can be one of the following values:
  *         @arg @ref LL_CMU_PLLEXTCLK_CLOSE
  *         @arg @ref LL_CMU_PLLEXTCLK_GPIOCLK
  *         @arg @ref LL_CMU_PLLEXTCLK_USB12M
  *         @arg @ref LL_CMU_PLLEXTCLK_USB48M
  * @param  Clk Divider This parameter can be one of the following values:
  *         @arg @ref LL_CMU_PLLEXTCLK_DIV1
  *         @arg @ref LL_CMU_PLLEXTCLK_DIV2
  *         @arg @ref LL_CMU_PLLEXTCLK_DIV3
  *         @arg @ref LL_CMU_PLLEXTCLK_DIV4
  * @retval None
  */
__STATIC_INLINE void LL_CMU_ConfigPLLExtClk(uint32_t ClkSrc, uint32_t ClkDiv)
{
    MODIFY_REG(CMU->MDLCLKD, CMU_MDLCLKC_SELPLLECLK | CMU_MDLCLKC_DIVPLLECLK, (ClkSrc << CMU_MDLCLKC_SELPLLECLK_Pos) | (ClkDiv << CMU_MDLCLKC_DIVPLLECLK_Pos));
}

/**
  * @brief  Enable Clk Source in lower power mode
  * @rmtoll LL_CMU_LP_EnableClkSrc
  * @param  ClkGate Release of module clk This parameter can be one of the following values:
  *         @arg @ref LL_CMU_CLKLPSRC_LPO1K
  *         @arg @ref LL_CMU_CLKLPSRC_STBYIRC4M
  *         @arg @ref LL_CMU_CLKLPSRC_STOPIRC4M
  * @retval None
  */
__STATIC_INLINE void LL_CMU_LP_EnableClkSrc(uint32_t ClkSrc)
{
    SET_BIT(CMU->CLKLP, ClkSrc);
}

/**
  * @brief  Diable Clk Source in lower power mode
  * @rmtoll LL_CMU_LP_DisableClkSrc
  * @param  ClkGate Release of module clk This parameter can be one of the following values:
  *         @arg @ref LL_CMU_CLKLPSRC_LPO1K
  *         @arg @ref LL_CMU_CLKLPSRC_STBYIRC4M
  *         @arg @ref LL_CMU_CLKLPSRC_STOPIRC4M
  * @retval None
  */
__STATIC_INLINE void LL_CMU_LP_DisableClkSrc(uint32_t ClkSrc)
{
    CLEAR_BIT(CMU->CLKLP, ClkSrc);
}

/**
  * @brief  Enable Clk Source in Stop mode
  * @rmtoll LL_CMU_STOP_EnableClkSrc
  * @param  ClkGate Release of module clk This parameter can be one of the following values:
  *         @arg @ref LL_CMU_CLKSTOPSRC_USB48M
  *         @arg @ref LL_CMU_CLKSTOPSRC_PLLCLK
  *         @arg @ref LL_CMU_CLKSTOPSRC_OSCCLK
  *         @arg @ref LL_CMU_CLKSTOPSRC_IRC16M
  * @retval None
  */
__STATIC_INLINE void LL_CMU_STOP_EnableClkSrc(uint32_t ClkSrc)
{
    SET_BIT(CMU->CLKSTOP, ClkSrc);
}

/**
  * @brief  Diable Clk Source in Stop mode
  * @rmtoll LL_CMU_STOP_DisableClkSource
  * @param  ClkGate Release of module clk This parameter can be one of the following values:
  *         @arg @ref LL_CMU_CLKSTOPSRC_USB48M
  *         @arg @ref LL_CMU_CLKSTOPSRC_PLLCLK
  *         @arg @ref LL_CMU_CLKSTOPSRC_OSCCLK
  *         @arg @ref LL_CMU_CLKSTOPSRC_IRC16M
  * @retval None
  */
__STATIC_INLINE void LL_CMU_STOP_DisableClkSrc(uint32_t ClkSrc)
{
    CLEAR_BIT(CMU->CLKSTOP, ClkSrc);
}

/**
  * @brief  Enable Root Clk Source in Idle mode
  * @rmtoll LL_CMU_IDLE_EnableClkSrc
  * @param  ClkGate Release of module clk This parameter can be one of the following values:
  *         @arg @ref LL_CMU_CLKIDLESRC_USB48M
  *         @arg @ref LL_CMU_CLKIDLESRC_PLLCLK
  *         @arg @ref LL_CMU_CLKIDLESRC_OSCCLK
  *         @arg @ref LL_CMU_CLKIDLESRC_IRC16M
  * @retval None
  */
__STATIC_INLINE void LL_CMU_IDLE_EnableRootClkSrc(uint32_t ClkSrc)
{
    SET_BIT(CMU->CLKIDLE, ClkSrc);
}

/**
  * @brief  Diable root Clk Source in Idle mode
  * @rmtoll LL_CMU_IDLE_DisableClkSrc
  * @param  ClkGate Release of module clk This parameter can be one of the following values:
  *         @arg @ref LL_CMU_CLKIDLESRC_USB48M
  *         @arg @ref LL_CMU_CLKIDLESRC_PLLCLK
  *         @arg @ref LL_CMU_CLKIDLESRC_OSCCLK
  *         @arg @ref LL_CMU_CLKIDLESRC_IRC16M
  * @retval None
  */
__STATIC_INLINE void LL_CMU_IDLE_DisableRootClkSrc(uint32_t ClkSrc)
{
    CLEAR_BIT(CMU->CLKIDLE, ClkSrc);
}

/**
  * @brief  Open module Clk gate in ilde mode
  * @rmtoll LL_CMU_IDLE_OpenModuleClk
  * @param  ClkGate Release of module clk This parameter can be one of the following values:
  *         @arg @ref LL_CMU_MDCLK_SHIELD
  *         @arg @ref LL_CMU_MDCLK_WDT
  *         @arg @ref LL_CMU_MDCLK_ADC
  *         @arg @ref LL_CMU_MDCLK_TRNG
  *         @arg @ref LL_CMU_MDCLK_CT
  *         @arg @ref LL_CMU_MDCLK_UART
  *         @arg @ref LL_CMU_MDCLK_USBFS
  *         @arg @ref LL_CMU_MDCLK_COUT0
  *         @arg @ref LL_CMU_MDCLK_COUT1
  *         @arg @ref LL_CMU_MDCLK_COUT2
  *         @arg @ref LL_CMU_MDCLK_STIMER0
  *         @arg @ref LL_CMU_MDCLK_STIMER1
  *         @arg @ref LL_CMU_MDCLK_STIMER2
  *         @arg @ref LL_CMU_MDCLK_STIMER3
  *         @arg @ref LL_CMU_MDCLK_STIMER4
  *         @arg @ref LL_CMU_MDCLK_STIMER5
  *         @arg @ref LL_CMU_MDCLK_USBPHY
  *         @arg @ref LL_CMU_MDCLK_SYSTKRF
  *         @arg @ref LL_CMU_MDCLK_SYSDIV
  *         @arg @ref LL_CMU_MDCLK_SYSTST
  *         @arg @ref LL_CMU_MDCLK_PLLEXT
  * @retval None
  */
__STATIC_INLINE void LL_CMU_IDLE_OpenModuleClk(uint32_t ClkGate)
{
    SET_BIT(CMU->GMDLCLK, ClkGate);
}

/**
  * @brief  Close module Clk gate in ilde mode
  * @rmtoll LL_CMU_IDLE_CloseModuleClk
  * @param  ClkGate Release of module clk This parameter can be one of the following values:
  *         @arg @ref LL_CMU_MDCLK_SHIELD
  *         @arg @ref LL_CMU_MDCLK_WDT
  *         @arg @ref LL_CMU_MDCLK_ADC
  *         @arg @ref LL_CMU_MDCLK_TRNG
  *         @arg @ref LL_CMU_MDCLK_CT
  *         @arg @ref LL_CMU_MDCLK_UART
  *         @arg @ref LL_CMU_MDCLK_USBFS
  *         @arg @ref LL_CMU_MDCLK_COUT0
  *         @arg @ref LL_CMU_MDCLK_COUT1
  *         @arg @ref LL_CMU_MDCLK_COUT2
  *         @arg @ref LL_CMU_MDCLK_STIMER0
  *         @arg @ref LL_CMU_MDCLK_STIMER1
  *         @arg @ref LL_CMU_MDCLK_STIMER2
  *         @arg @ref LL_CMU_MDCLK_STIMER3
  *         @arg @ref LL_CMU_MDCLK_STIMER4
  *         @arg @ref LL_CMU_MDCLK_STIMER5
  *         @arg @ref LL_CMU_MDCLK_USBPHY
  *         @arg @ref LL_CMU_MDCLK_SYSTKRF
  *         @arg @ref LL_CMU_MDCLK_SYSDIV
  *         @arg @ref LL_CMU_MDCLK_SYSTST
  *         @arg @ref LL_CMU_MDCLK_PLLEXT
  * @retval None
  */
__STATIC_INLINE void LL_CMU_IDLE_CloseModuleClk(uint32_t ClkSrc)
{
    CLEAR_BIT(CMU->GMDLCLK, ClkSrc);
}

/**
  * @brief  Close All module Clk gate in ilde mode
  * @rmtoll LL_CMU_IDLE_CloseAllModuleClk
  * @retval None
  */
__STATIC_INLINE void LL_CMU_IDLE_CloseAllModuleClk(void)
{
    CLEAR_BIT(CMU->GMDLCLK, 0x0DFFFA75UL);
}


/**
  * @brief  Open module Clk gate in stop mode
  * @rmtoll LL_CMU_STOP_OpenModuleClk
  * @param  ClkGate Release of module clk This parameter can be one of the following values:
  *         @arg @ref LL_CMU_MDCLK_SHIELD
  *         @arg @ref LL_CMU_MDCLK_WDT
  *         @arg @ref LL_CMU_MDCLK_ADC
  *         @arg @ref LL_CMU_MDCLK_TRNG
  *         @arg @ref LL_CMU_MDCLK_CT
  *         @arg @ref LL_CMU_MDCLK_UART
  *         @arg @ref LL_CMU_MDCLK_USBFS
  *         @arg @ref LL_CMU_MDCLK_STIMER0
  *         @arg @ref LL_CMU_MDCLK_STIMER1
  *         @arg @ref LL_CMU_MDCLK_STIMER2
  *         @arg @ref LL_CMU_MDCLK_STIMER3
  *         @arg @ref LL_CMU_MDCLK_STIMER4
  *         @arg @ref LL_CMU_MDCLK_STIMER5
  *         @arg @ref LL_CMU_MDCLK_USBPHY
  *         @arg @ref LL_CMU_MDCLK_SYSTKRF
  *         @arg @ref LL_CMU_MDCLK_SYSDIV
  *         @arg @ref LL_CMU_MDCLK_SYSTST
  *         @arg @ref LL_CMU_MDCLK_PLLEXT
  * @retval None
  */
__STATIC_INLINE void LL_CMU_STOP_OpenModuleClk(uint32_t ClkGate)
{
    CLEAR_BIT(CMU->PMGMDLCLK, ClkGate);
}

/**
  * @brief  Close module Clk gate in stop mode
  * @rmtoll LL_CMU_STOP_CloseModuleClk
  * @param  ClkGate Release of module clk This parameter can be one of the following values:
  *         @arg @ref LL_CMU_MDCLK_SHIELD
  *         @arg @ref LL_CMU_MDCLK_WDT
  *         @arg @ref LL_CMU_MDCLK_ADC
  *         @arg @ref LL_CMU_MDCLK_TRNG
  *         @arg @ref LL_CMU_MDCLK_CT
  *         @arg @ref LL_CMU_MDCLK_UART
  *         @arg @ref LL_CMU_MDCLK_USBFS
  *         @arg @ref LL_CMU_MDCLK_STIMER0
  *         @arg @ref LL_CMU_MDCLK_STIMER1
  *         @arg @ref LL_CMU_MDCLK_STIMER2
  *         @arg @ref LL_CMU_MDCLK_STIMER3
  *         @arg @ref LL_CMU_MDCLK_STIMER4
  *         @arg @ref LL_CMU_MDCLK_STIMER5
  *         @arg @ref LL_CMU_MDCLK_USBPHY
  *         @arg @ref LL_CMU_MDCLK_SYSTKRF
  *         @arg @ref LL_CMU_MDCLK_SYSDIV
  *         @arg @ref LL_CMU_MDCLK_SYSTST
  *         @arg @ref LL_CMU_MDCLK_PLLEXT
  * @retval None
  */
__STATIC_INLINE void LL_CMU_STOP_CloseModuleClk(uint32_t ClkSrc)
{
    SET_BIT(CMU->PMGMDLCLK, ClkSrc);
}

/**
  * @brief  Close All module Clk gate in stop mode
  * @rmtoll LL_CMU_STOP_CloseAllModuleClk
  * @retval None
  */
__STATIC_INLINE void LL_CMU_STOP_CloseAllModuleClk(void)
{
    SET_BIT(CMU->PMGMDLCLK, 0x0FFFFFF9);
}

/**
  * @brief  Open module COUT Clk gate in stop mode
  * @rmtoll LL_CMU_STOP_OpenCoutClk
  * @param  ClkGate Release of module clk This parameter can be one of the following values:
  *         @arg @ref LL_CMU_MDCLK_COUT0
  *         @arg @ref LL_CMU_MDCLK_COUT1
  *         @arg @ref LL_CMU_MDCLK_COUT2
  * @retval None
  */
__STATIC_INLINE void LL_CMU_STOP_OpenCoutClk(uint32_t ClkGate)
{
    SET_BIT(CMU->PMGMDLCLK, ClkGate);
}

/**
  * @brief  Close module COUT Clk gate in stop mode
  * @rmtoll LL_CMU_STOP_CloseCoutClk
  * @param  ClkGate Release of module clk This parameter can be one of the following values:
  *         @arg @ref LL_CMU_MDCLK_COUT0
  *         @arg @ref LL_CMU_MDCLK_COUT1
  *         @arg @ref LL_CMU_MDCLK_COUT2
  * @retval None
  */
__STATIC_INLINE void LL_CMU_STOP_CloseCoutClk(uint32_t ClkSrc)
{
    CLEAR_BIT(CMU->PMGMDLCLK, ClkSrc);
}

ErrorStatus LL_CMU_SysClkInit(LL_CMU_InitTypeDef *CMU_InitStruct);
ErrorStatus LL_CMU_SysClkInit2(LL_CMU2_InitTypeDef *CMU_InitStruct);
uint32_t LL_CMU_SystemCoreClockUpdate(void);
ErrorStatus LL_CMU_StartPLL(LL_CMU_InitTypeDef *CMU_InitStruct);
ErrorStatus LL_CMU_SysClkToPLL(uint32_t pll_freq_hz, uint32_t main_clk_div, uint32_t bus_clk_div);
ErrorStatus LL_CMU_SysClkRestoreToPLL(uint32_t sysclkfreq);
uint32_t LL_CMU_GetOscClock(void);
uint32_t LL_CMU_GetSysClock(void);
uint32_t LL_CMU_GetBusClock(void);


#ifdef __cplusplus
}
#endif
#endif

/************************ (C) COPYRIGHT FudanMicroelectronics *****END OF FILE****/
