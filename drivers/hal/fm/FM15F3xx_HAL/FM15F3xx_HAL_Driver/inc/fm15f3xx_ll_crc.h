/**
 ******************************************************************************
 * @file    fm15f3xx_ll_crc.h
 * @author  ZJH
 * @version V1.0.0
 * @date    2020-03-17
 * @brief
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2020 FudanMicroelectronics.
 * All rights reserved.</center></h2>
 *
 ******************************************************************************
 */

#ifndef FM15F3XX_LL_CRC_H
#define FM15F3XX_LL_CRC_H
#ifdef __cplusplus
extern "C" {
#endif

#include "fm15f3xx.h"

#if defined (CRC)

/* Private macro ------------------------------------------------------------*/


/**
 * @brief  CRC Init structures definition
 */
typedef struct {
    uint32_t    Init;       /* xor with the head of data */
    uint8_t     RefIn;      /* true or false ,if input data need to reverse */
    uint8_t     RefOut;     /* true or false ,if output data need to reverse */
    uint32_t    XorOut;     /* xor with it the last step */
} LL_CRC_InitTypeDef;

/* Exported constants --------------------------------------------------------*/


/** @defgroup CRC_Config
 * @{
 */


/**
 * @brief  Configure  CRC RUN
 */
#define LL_CRC_START    CRC_CFG_RUN
#define LL_CRC_STOP     (0x00U)


/**
 * @brief  Configure  DATA WIDTH
 */
#define LL_CRC_DATAWIDTH_8  (0x00U)
#define LL_CRC_DATAWIDTH_32 CRC_CFG_DATAWIDTH


/**
 * @brief  Configure  DATA ENDIAN
 */
#define LL_CRC_REVERSE_NO           (0x00U)
#define LL_CRC_REVERSE_BYTE         (CRC_CFG_ENDIAN_BYTE)
#define LL_CRC_REVERSE_BITinBYTE    (CRC_CFG_ENDIAN_BIT)
#define LL_CRC_REVERSE_BITinWORD    (CRC_CFG_ENDIAN_BIT | CRC_CFG_ENDIAN_BYTE)


/**
 * @}
 */


/**
 * @brief  Start CRC
 * @param  CRC Instance
 * @retval
 */
__STATIC_INLINE void LL_CRC_Start(CRC_TypeDef *CRCx)
{
    SET_BIT(CRCx->CFG, CRC_CFG_RUN);
}


/**
 * @brief  Stop CRC
 * @param  CRC Instance
 * @retval
 */
__STATIC_INLINE void LL_CRC_Stop(CRC_TypeDef *CRCx)
{
    CLEAR_BIT(CRCx->CFG, CRC_CFG_RUN);
}


/**
 * @brief  Set CRC input data width
 * @param  CRC Instance
 * @param  This parameter can be one of the following values:
            @arg @ref LL_CRC_DATAWIDTH_8
            @arg @ref LL_CRC_DATAWIDTH_32
 * @retval
 */
__STATIC_INLINE void LL_CRC_SetInputDataWidth(CRC_TypeDef *CRCx, uint32_t CFG)
{
    MODIFY_REG(CRCx->CFG, CRC_CFG_DATAWIDTH, CFG);
}


/**
 * @brief  Set CRC input data Reverse
 * @param  CRC Instance
 * @param  This parameter can be one of the following values:
            @arg @ref LL_CRC_REVERSE_NO
            @arg @ref LL_CRC_REVERSE_BYTE
            @arg @ref LL_CRC_REVERSE_BITinBYTE
            @arg @ref LL_CRC_REVERSE_BITinWORD
 * @retval
 */
__STATIC_INLINE void LL_CRC_SetInputReverse(CRC_TypeDef *CRCx, uint32_t CFG)
{
    MODIFY_REG(CRCx->CFG, CRC_CFG_ENDIAN_BIT | CRC_CFG_ENDIAN_BYTE, CFG);
}


void LL_CRC_DeInit(void);


uint16_t LL_CRC16_Calculation(LL_CRC_InitTypeDef* CRC_InitStrcuct, uint8_t *pData, uint32_t DataLength);


#endif /* defined (CRC) */

#ifdef __cplusplus
}
#endif

#endif

/************************ (C) COPYRIGHT FudanMicroelectronics *****END OF FILE****/
