/**
 ******************************************************************************
 * @file    fm15f3xx_hal_crc.h
 * @author  WYL
 * @brief   Header file of CRC HAL module.
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2020 FudanMicroelectronics.
 * All rights reserved.</center></h2>
 *
 ******************************************************************************
 */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __FM15F3xx_HAL_CRC_H
#define __FM15F3xx_HAL_CRC_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "fm15f3xx_hal_def.h"
#include "fm15f3xx_ll_crc.h"


/** @addtogroup FM15F3xx_HAL_Driver
 * @{
 */


/** @defgroup CRC CRC
 * @brief CRC HAL module driver
 * @{
 */

/* Exported types ------------------------------------------------------------*/


/** @defgroup CRC_Exported_Types CRC Exported Types
 * @{
 */


/** @defgroup CRC_Exported_Types_Group1 CRC State Structure definition
 * @{
 */
typedef enum {
    HAL_CRC_STATE_RESET     = 0x00U,    /*!< CRC not yet initialized or disabled */
    HAL_CRC_STATE_READY     = 0x01U,    /*!< CRC initialized and ready for use   */
    HAL_CRC_STATE_BUSY      = 0x02U,    /*!< CRC internal process is ongoing     */
    HAL_CRC_STATE_TIMEOUT   = 0x03U,    /*!< CRC timeout state                   */
    HAL_CRC_STATE_ERROR     = 0x04U     /*!< CRC error state                     */
} HAL_CRC_StateTypeDef;


/**
 * @}
 */


/** @defgroup CRC_Exported_Types_Group1 CRC Structure definition
 * @{
 */
#define CRC_InitTypeDef LL_CRC_InitTypeDef


/**
 * @}
 */


/** @defgroup CRC_Exported_Types_Group2 CRC Handle Structure definition
 * @{
 */
typedef struct {
    CRC_TypeDef *Instance;                  /*!< Register base address   */

    CRC_InitTypeDef Init;                   /*!< CRC required parameters */

    __IO HAL_CRC_StateTypeDef State;        /*!< CRC communication state */
} CRC_HandleTypeDef;


/**
 * @}
 */


/**
 * @}
 */

/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/


/** @defgroup CRC_Exported_Macros CRC Exported Macros
 * @{
 */


/** @brief Resets CRC handle state
 * @param  __HANDLE__ CRC handle
 * @retval None
 */
#define __HAL_CRC_RESET_HANDLE_STATE( __HANDLE__ ) ( (__HANDLE__)->State = HAL_CRC_STATE_RESET)


/**
 * @}
 */

/* Exported functions --------------------------------------------------------*/


/** @defgroup CRC_Exported_Functions CRC Exported Functions
 * @{
 */


/** @defgroup CRC_Exported_Functions_Group1 Initialization and de-initialization functions
 * @{
 */
HAL_StatusTypeDef HAL_CRC_Init(CRC_HandleTypeDef *hcrc);


HAL_StatusTypeDef HAL_CRC_DeInit(CRC_HandleTypeDef *hcrc);


void HAL_CRC_MspInit(CRC_HandleTypeDef *hcrc);


void HAL_CRC_MspDeInit(CRC_HandleTypeDef *hcrc);


/**
 * @}
 */


/** @defgroup CRC_Exported_Functions_Group2 Peripheral Control functions
 * @{
 */
uint16_t HAL_CRC16_Calculate(CRC_HandleTypeDef *hcrc, uint8_t* pBuffer, uint32_t BufferLength);


/**
 * @}
 */


/** @defgroup CRC_Exported_Functions_Group3 Peripheral State functions
 * @{
 */
HAL_CRC_StateTypeDef HAL_CRC_GetState(CRC_HandleTypeDef *hcrc);


/**
 * @}
 */


/**
 * @}
 */
/* Private types -------------------------------------------------------------*/


/** @defgroup CRC_Private_Types CRC Private Types
 * @{
 */


/**
 * @}
 */

/* Private defines -----------------------------------------------------------*/


/** @defgroup CRC_Private_Defines CRC Private Defines
 * @{
 */


/**
 * @}
 */

/* Private variables ---------------------------------------------------------*/


/** @defgroup CRC_Private_Variables CRC Private Variables
 * @{
 */


/**
 * @}
 */

/* Private constants ---------------------------------------------------------*/


/** @defgroup CRC_Private_Constants CRC Private Constants
 * @{
 */


/**
 * @}
 */

/* Private macros ------------------------------------------------------------*/


/** @defgroup CRC_Private_Macros CRC Private Macros
 * @{
 */
#define IS_CRC_INSTANCE( INSTANCE ) ( (INSTANCE) == CRC)


/**
 * @}
 */

/* Private functions prototypes ----------------------------------------------*/


/** @defgroup CRC_Private_Functions_Prototypes CRC Private Functions Prototypes
 * @{
 */


/**
 * @}
 */

/* Private functions ---------------------------------------------------------*/


/** @defgroup CRC_Private_Functions CRC Private Functions
 * @{
 */


/**
 * @}
 */


/**
 * @}
 */


/**
 * @}
 */

#ifdef __cplusplus
}
#endif

#endif /* __FM15F3xx_HAL_CRC_H */

/************************ (C) COPYRIGHT FudanMicroelectronics *****END OF FILE****/
