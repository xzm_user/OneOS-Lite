/**
 ******************************************************************************
 * @file    fm15f3xx_system.h
 * @author  SRG
 * @version V1.0.0
 * @date    2020-03-17
 * @brief
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2020 FudanMicroelectronics.
 * All rights reserved.</center></h2>
 *
 ******************************************************************************
 */
#ifndef __FM15F3XX_SYSTEM_H_
#define __FM15F3XX_SYSTEM_H_


#ifdef __cplusplus
extern "C" {
#endif


#include "fm15f3xx.h"
/* Exported functions --------------------------------------------------------*/


/**
 * @brief  Enable code cache
 * @rmtoll CACHE_CTRL  CODE_CACHE_EN        LL_Code_Cache_Enable
 * @retval None
 */
__STATIC_INLINE void LL_Code_Cache_Enable(void)
{
    SET_BIT(CACHE->CTRL, CODE_CACHE_EN);
}


/**
 * @brief  Enable data cache
 * @rmtoll CACHE_CTRL  DATA_CACHE_EN        LL_Data_Cache_Enable
 * @retval None
 */
__STATIC_INLINE void LL_Data_Cache_Enable(void)
{
    SET_BIT(CACHE->CTRL, DATA_CACHE_EN);
}


/**
 * @brief  Enable cache
 * @rmtoll CACHE_CTRL  CODE_CACHE_EN        LL_Cache_Enable
 *         CACHE_CTRL  DATA_CACHE_EN        LL_Cache_Enable
 * @retval None
 */
__STATIC_INLINE void LL_Cache_Enable(void)
{
    SET_BIT(CACHE->CTRL, DATA_CACHE_EN | CODE_CACHE_EN);
}


/**
 * @brief  Disable code cache
 * @rmtoll CACHE_CTRL  CODE_CACHE_EN        LL_Code_Cache_Disable
 * @retval None
 */
__STATIC_INLINE void LL_Code_Cache_Disable(void)
{
    CLEAR_BIT(CACHE->CTRL, CODE_CACHE_EN);
}


/**
 * @brief  Disable data cache
 * @rmtoll CACHE_CTRL  DATA_CACHE_EN        LL_Data_Cache_Disable
 * @retval None
 */
__STATIC_INLINE void LL_Data_Cache_Disable(void)
{
    CLEAR_BIT(CACHE->CTRL, DATA_CACHE_EN);
}


/**
 * @brief  Disable cache
 * @rmtoll CACHE_CTRL  CODE_CACHE_EN        LL_Cache_Disable
 *         CACHE_CTRL  DATA_CACHE_EN        LL_Cache_Disable
 * @retval None
 */
__STATIC_INLINE void LL_Cache_Disable(void)
{
    CLEAR_BIT(CACHE->CTRL, DATA_CACHE_EN | CODE_CACHE_EN);
}


/**
 * @brief  Flush code cache
 * @rmtoll CACHE_CTRL  CODE_CACHE_FLUSH     LL_Code_Cache_Flush
 * @retval None
 */
__STATIC_INLINE void LL_Code_Cache_Flush(void)
{
    SET_BIT(CACHE->CTRL, CODE_CACHE_FLUSH);
}


/**
 * @brief  Flush data cache
 * @rmtoll CACHE_CTRL  DATA_CACHE_FLUSH     LL_Data_Cache_Flush
 * @retval None
 */
__STATIC_INLINE void LL_Data_Cache_Flush(void)
{
    SET_BIT(CACHE->CTRL, DATA_CACHE_FLUSH);
}


#ifdef __cplusplus
}
#endif
#endif
/************************ (C) COPYRIGHT FudanMicroelectronics *****END OF FILE****/

