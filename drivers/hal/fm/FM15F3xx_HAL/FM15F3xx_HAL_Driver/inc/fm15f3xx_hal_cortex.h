/**
 ******************************************************************************
 * @file    fm15f3xx_hal_cortex.h
 * @author  WYL
 * @version V1.0.0
 * @date    2020-04-14
 * @brief   Header file of CORTEX module.
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2020 FudanMicroelectronics.
 * All rights reserved.</center></h2>
 *
 ******************************************************************************
 */


/*- Define to prevent recursive inclusion -------------------------------------*/
#ifndef __FM15F3XX_HAL_CORTEX_H
#define __FM15F3XX_HAL_CORTEX_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "fm15f3xx_hal_def.h"
#include "fm15f3xx_ll_cortex.h"


/** @addtogroup FM15F3XX_HAL_Driver
 * @{
 */


/** @defgroup CORTEX_HAL CORTEX
 * @{
 */

/* Private types -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/

/* Private constants ---------------------------------------------------------*/

/* Private macros ------------------------------------------------------------*/

/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/


/** @defgroup CORTEX_HAL_Exported_Constants CORTEX Exported Constants
 * @{
 */


/** @defgroup CORTEX_HAL_EC_CLKSOURCE_HCLK SYSTICK Clock Source
 * @{
 */
#define SYSTICK_CLKSOURCE_HCLK_DIV8 0x00000000U                     /*!< AHB clock divided by 8 selected as SysTick clock source.*/
#define SYSTICK_CLKSOURCE_HCLK      SysTick_CTRL_CLKSOURCE_Msk      /*!< AHB clock selected as SysTick clock source. */


/**
 * @}
 */


/**
 * @}
 */

/* Exported macro ------------------------------------------------------------*/
/* Exported functions --------------------------------------------------------*/


/** @addtogroup CORTEX_Exported_Functions
 * @{
 */


/** @addtogroup CORTEX_Exported_Functions_Group1
 * @{
 */
/* Initialization and de-initialization functions *****************************/
void HAL_NVIC_SetPriorityGrouping(uint32_t PriorityGroup);


void HAL_NVIC_SetPriority(IRQn_Type IRQn, uint32_t PreemptPriority, uint32_t SubPriority);


void HAL_NVIC_EnableIRQ(IRQn_Type IRQn);


void HAL_NVIC_DisableIRQ(IRQn_Type IRQn);


void HAL_NVIC_SystemReset(void);


uint32_t HAL_SYSTICK_Config(uint32_t TicksNumb);


/**
 * @}
 */


/** @addtogroup CORTEX_Exported_Functions_Group2
 * @{
 */
/* Peripheral Control functions ***********************************************/
uint32_t HAL_NVIC_GetPriorityGrouping(void);


void HAL_NVIC_GetPriority(IRQn_Type IRQn, uint32_t PriorityGroup, uint32_t* pPreemptPriority, uint32_t* pSubPriority);


uint32_t HAL_NVIC_GetPendingIRQ(IRQn_Type IRQn);


void HAL_NVIC_SetPendingIRQ(IRQn_Type IRQn);


void HAL_NVIC_ClearPendingIRQ(IRQn_Type IRQn);


uint32_t HAL_NVIC_GetActive(IRQn_Type IRQn);


void HAL_SYSTICK_CLKSourceConfig(uint32_t CLKSource);


void HAL_SYSTICK_IRQHandler(void);


void HAL_SYSTICK_Callback(void);


/**
 * @}
 */


/**
 * @}
 */

/* Exported functions --------------------------------------------------------*/


/** @defgroup CORTEX_HAL_Exported_Functions CORTEX Exported Functions
 * @{
 */
#define IS_NVIC_PRIORITY_GROUP( GROUP ) ( ( (GROUP) == NVIC_PRIORITYGROUP_0) || \
                                          ( (GROUP) == NVIC_PRIORITYGROUP_1) || \
                                          ( (GROUP) == NVIC_PRIORITYGROUP_2) || \
                                          ( (GROUP) == NVIC_PRIORITYGROUP_3) || \
                                          ( (GROUP) == NVIC_PRIORITYGROUP_4) )

#define IS_NVIC_PREEMPTION_PRIORITY( PRIORITY ) ( (PRIORITY) < 0x10U)

#define IS_NVIC_SUB_PRIORITY( PRIORITY ) ( (PRIORITY) < 0x10U)

#define IS_NVIC_DEVICE_IRQ( IRQ ) ( (IRQ) >= (IRQn_Type) 0x00U)

#define IS_SYSTICK_CLK_SOURCE( SOURCE ) ( ( (SOURCE) == SYSTICK_CLKSOURCE_HCLK) || \
                                          ( (SOURCE) == SYSTICK_CLKSOURCE_HCLK_DIV8) )


/**
 * @}
 */


/**
 * @}
 */


/**
 * @}
 */

#ifdef __cplusplus
}
#endif

#endif /* __FM15F3XX_HAL_CORTEX_H */

/************************ (C) COPYRIGHT FudanMicroelectronics *****END OF FILE****/
