/**
 ******************************************************************************
 * @file    fm15f3xx_ll_lptimer.h
 * @author  SRG
 * @version V1.0.0
 * @date    2020-03-17
 * @brief
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2020 FudanMicroelectronics.
 * All rights reserved.</center></h2>
 *
 ******************************************************************************
 */

#ifndef FM15F3XX_LL_LPTIMER_H
#define FM15F3XX_LL_LPTIMER_H
#ifdef __cplusplus
extern "C" {
#endif

#include "fm15f3xx.h"

typedef struct {
    uint8_t ClockSel;

    FunctionalState PrescaleEn;

    uint8_t PrescaleCfg;

    uint8_t PadClkSel;

    FunctionalState IntEn;

    uint8_t RunMode;

    uint8_t FreeMode;

    FunctionalState InPolarEn;

    uint8_t TriggerSource;

    uint16_t Mod;
} LL_LPTIM_InitTypeDef;


#define LL_LPTIM_CLK_SEL_IRC4M  (0x0U)
#define LL_LPTIM_CLK_SEL_LPO_1K (0x1U)
#define LL_LPTIM_CLK_SEL_RTC_1S (0x2U)
#define LL_LPTIM_CLK_SEL_PADCLK (0x3U)

#define LL_LPTIM_PSC_DIV2       (0x0U)
#define LL_LPTIM_PSC_DIV4       (0x1U)
#define LL_LPTIM_PSC_DIV8       (0x2U)
#define LL_LPTIM_PSC_DIV16      (0x3U)
#define LL_LPTIM_PSC_DIV32      (0x4U)
#define LL_LPTIM_PSC_DIV64      (0x5U)
#define LL_LPTIM_PSC_DIV128     (0x6U)
#define LL_LPTIM_PSC_DIV256     (0x7U)
#define LL_LPTIM_PSC_DIV512     (0x8U)
#define LL_LPTIM_PSC_DIV1024    (0x9U)
#define LL_LPTIM_PSC_DIV2048    (0xAU)
#define LL_LPTIM_PSC_DIV4096    (0xBU)
#define LL_LPTIM_PSC_DIV8192    (0xCU)
#define LL_LPTIM_PSC_DIV16384   (0xDU)
#define LL_LPTIM_PSC_DIV32768   (0xEU)
#define LL_LPTIM_PSC_DIV65536   (0xFU)


#define LL_LPTIM_PAD_CLK_SEL_PD0    (0x0U)
#define LL_LPTIM_PAD_CLK_SEL_PD1    (0x1U)
#define LL_LPTIM_PAD_CLK_SEL_PD2    (0x2U)
#define LL_LPTIM_PAD_CLK_SEL_PD3    (0x3U)
#define LL_LPTIM_PAD_CLK_SEL_PD4    (0x4U)
#define LL_LPTIM_PAD_CLK_SEL_PD5    (0x5U)
#define LL_LPTIM_PAD_CLK_SEL_PD6    (0x6U)
#define LL_LPTIM_PAD_CLK_SEL_PD7    (0x7U)
#define LL_LPTIM_PAD_CLK_SEL_PC4    (0x8U)
#define LL_LPTIM_PAD_CLK_SEL_PC5    (0x9U)
#define LL_LPTIM_PAD_CLK_SEL_PC6    (0xAU)
#define LL_LPTIM_PAD_CLK_SEL_PC7    (0xBU)
#define LL_LPTIM_PAD_CLK_SEL_PF0    (0xCU)
#define LL_LPTIM_PAD_CLK_SEL_PF1    (0xDU)
#define LL_LPTIM_PAD_CLK_SEL_PF2    (0xEU)
#define LL_LPTIM_PAD_CLK_SEL_PF3    (0xFU)


#define LL_LPTIM_RUN_MODE_TIME  (0x0U)
#define LL_LPTIM_RUN_MODE_PULSE (0x1U)

#define LL_LPTIM_FREE_MODE_CMR  (0x0U)
#define LL_LPTIM_FREE_MODE_FREE (0x1U)

#define LL_LPTIM_TRIGSRC_PD0            0x00000000U
#define LL_LPTIM_TRIGSRC_PD1            (0x1 << LPTIMER_CFG_INSEL_Pos)
#define LL_LPTIM_TRIGSRC_PD2            (0x2 << LPTIMER_CFG_INSEL_Pos)
#define LL_LPTIM_TRIGSRC_PD3            (0x3 << LPTIMER_CFG_INSEL_Pos)
#define LL_LPTIM_TRIGSRC_PD4            (0x4 << LPTIMER_CFG_INSEL_Pos)
#define LL_LPTIM_TRIGSRC_PD5            (0x5 << LPTIMER_CFG_INSEL_Pos)
#define LL_LPTIM_TRIGSRC_PD6            (0x6 << LPTIMER_CFG_INSEL_Pos)
#define LL_LPTIM_TRIGSRC_PD7            (0x7 << LPTIMER_CFG_INSEL_Pos)
#define LL_LPTIM_TRIGSRC_PC4            (0x8 << LPTIMER_CFG_INSEL_Pos)
#define LL_LPTIM_TRIGSRC_PC5            (0x9 << LPTIMER_CFG_INSEL_Pos)
#define LL_LPTIM_TRIGSRC_PC6            (0xA << LPTIMER_CFG_INSEL_Pos)
#define LL_LPTIM_TRIGSRC_PC7            (0xB << LPTIMER_CFG_INSEL_Pos)
#define LL_LPTIM_TRIGSRC_PF0            (0xC << LPTIMER_CFG_INSEL_Pos)
#define LL_LPTIM_TRIGSRC_PF1            (0xD << LPTIMER_CFG_INSEL_Pos)
#define LL_LPTIM_TRIGSRC_PF2            (0xE << LPTIMER_CFG_INSEL_Pos)
#define LL_LPTIM_TRIGSRC_PF3            (0xF << LPTIMER_CFG_INSEL_Pos)
#define LL_LPTIM_TRIGSRC_LPTIM_WAKEUP   (0x10 << LPTIMER_CFG_INSEL_Pos)
#define LL_LPTIM_TRIGSRC_CMP            (0x11 << LPTIMER_CFG_INSEL_Pos)
#define LL_LPTIM_TRIGSRC_PHY_RXDP       (0x12 << LPTIMER_CFG_INSEL_Pos)
#define LL_LPTIM_TRIGSRC_PHY_RXDM       (0x13 << LPTIMER_CFG_INSEL_Pos)
#define LL_LPTIM_TRIGSRC_VBUS_DET       (0x14 << LPTIMER_CFG_INSEL_Pos)
#define LL_LPTIM_TRIGSRC_RTC_ALARM      (0x15 << LPTIMER_CFG_INSEL_Pos)
#define LL_LPTIM_TRIGSRC_BKP_TAMPER     (0x16 << LPTIMER_CFG_INSEL_Pos)
#define LL_LPTIM_TRIGSRC_RTC_1S         (0x17 << LPTIMER_CFG_INSEL_Pos)
#define LL_LPTIM_TRIGSRC_PMU_CHG90      (0x18 << LPTIMER_CFG_INSEL_Pos)
#define LL_LPTIM_TRIGSRC_PMU_CHGEOC     (0x19 << LPTIMER_CFG_INSEL_Pos)
#define LL_LPTIM_TRIGSRC_PF4            (0x1D << LPTIMER_CFG_INSEL_Pos)
#define LL_LPTIM_TRIGSRC_JTAG_SEL       (0x1E << LPTIMER_CFG_INSEL_Pos)
#define LL_LPTIM_TRIGSRC_STANDBY_WAKEUP (0x1F << LPTIMER_CFG_INSEL_Pos)


/**
 * @brief  Write a value in STIMER register
 * @param  __INSTANCE__ STIMER Instance
 * @param  __REG__ Register to be written
 * @param  __VALUE__ Value to be written in the register
 * @retval None
 */
#define LL_LPTIMER_WriteReg( __INSTANCE__, __REG__, __VALUE__ ) WRITE_REG( __INSTANCE__->__REG__, (__VALUE__) )


/**
 * @brief  Read a value in STIMER register
 * @param  __INSTANCE__ STIMER Instance
 * @param  __REG__ Register to be read
 * @retval Register value
 */
#define LL_LPTIMER_ReadReg( __INSTANCE__, __REG__ ) READ_REG( __INSTANCE__->__REG__ )


/**
 * @brief  Enable low power timer.
 * @rmtoll CTRL          lpt_en           LL_LPTIM_Enable
 * @param
 * @param
 * @retval None
 */
__STATIC_INLINE void LL_LPTIM_Enable(void)
{
    SET_BIT(LPTIMER0->CTRL, LPTIMER_CTRL_RUNEN);
}


/**
 * @brief  Disable low power timer.
 * @rmtoll CTRL          lpt_en           LL_LPTIM_Disable
 * @param
 * @param
 * @retval None
 */
__STATIC_INLINE void LL_LPTIM_Disable(void)
{
    CLEAR_BIT(LPTIMER0->CTRL, LPTIMER_CTRL_RUNEN);
}


/**
 * @brief  Get low power timer enable status.
 * @rmtoll CTRL          lpt_en           LL_LPTIM_IsEnabled
 * @param
 * @param
 * @retval 0 or !0
 */
__STATIC_INLINE uint32_t LL_LPTIM_IsEnabled(void)
{
    return (READ_BIT(LPTIMER0->CTRL, LPTIMER_CTRL_RUNEN) == LPTIMER_CTRL_RUNEN);
}


/**
 * @brief  Reset low power timer.
 * @note
 * @rmtoll RESET          LPt_reset           LL_LPTIM_Reset
 * @param
 * @param
 * @retval None
 */
__STATIC_INLINE void LL_LPTIM_Reset(void)
{
    CLEAR_BIT(LPTIMER0->CTRL, LPTIMER_CTRL_CRSTN);
    SET_BIT(LPTIMER0->CTRL, LPTIMER_CTRL_CRSTN);
}


/**
 * @brief  Set the clk
 * @rmtoll CLKCFG            LL_LPTIM_SetClkSource
 * @param  clk_pin This parameter can be one of the following values:
 *         @arg @ref LL_LPTIM_CLK_SEL_IRC4M
 *         @arg @ref LL_LPTIM_CLK_SEL_LPO_1K
 *         @arg @ref LL_LPTIM_CLK_SEL_RTC_1S
 *         @arg @ref LL_LPTIM_CLK_SEL_PADCLK
 * @retval None
 */
__STATIC_INLINE void LL_LPTIM_SetClkSrc(uint8_t clk)
{
    MODIFY_REG(LPTIMER0->CLKCFG, LPTIMER_CLKCTRL_CLKSEL, clk << LPTIMER_CLKCTRL_CLKSEL_Pos);
}


/**
 * @brief  Get the clk
 * @rmtoll CLKCFG            LL_LPTIM_GetClkSource
 * @param  None
 * @retval clk_pin This parameter can be one of the following values:
 *         @arg @ref LL_LPTIM_CLK_SEL_IRC4M
 *         @arg @ref LL_LPTIM_CLK_SEL_LPO_1K
 *         @arg @ref LL_LPTIM_CLK_SEL_RTC_1S
 *         @arg @ref LL_LPTIM_CLK_SEL_PADCLK
 */
__STATIC_INLINE uint8_t LL_LPTIM_GetClkSrc(void)
{
    return ((uint8_t)(READ_BIT(LPTIMER0->CLKCFG, LPTIMER_CLKCTRL_CLKSEL) >> LPTIMER_CLKCTRL_CLKSEL_Pos));
}


/**
 * @brief  Enable low power timer int.
 * @rmtoll CLKCFG      LL_LPTIM_EnableINT
 * @param
 * @param
 * @retval None
 */
__STATIC_INLINE void LL_LPTIM_EnableINT(void)
{
    SET_BIT(LPTIMER0->CLKCFG, LPTIMER_CLKCTRL_INTEN);
}


/**
 * @brief  Disable low power timer int.
 * @rmtoll CLKCFG      LL_LPTIM_DisableINT
 * @param
 * @param
 * @retval None
 */
__STATIC_INLINE void LL_LPTIM_DisableINT(void)
{
    CLEAR_BIT(LPTIMER0->CLKCFG, LPTIMER_CLKCTRL_INTEN);
}


/**
 * @brief  Get low power timer Int Enable status.
 * @rmtoll CLKCFG      LL_LPTIM_IsEnableINT
 * @param
 * @param
 * @retval 0 or !0
 */
__STATIC_INLINE uint32_t LL_LPTIM_IsEnableINT(void)
{
    return (READ_BIT(LPTIMER0->CLKCFG, LPTIMER_CLKCTRL_INTEN) == LPTIMER_CLKCTRL_INTEN);
}


/**
 * @brief  Enable low power timer prescaler.
 * @rmtoll CLKCFG      LL_LPTIM_EnablePrescaler
 * @param
 * @param
 * @retval None
 */
__STATIC_INLINE void LL_LPTIM_EnablePrescaler(void)
{
    SET_BIT(LPTIMER0->CLKCFG, LPTIMER_CLKCTRL_PRESEN);
}


/**
 * @brief  Disable low power timer prescaler.
 * @rmtoll CLKCFG      LL_LPTIM_DisablePrescaler
 * @param
 * @param
 * @retval None
 */
__STATIC_INLINE void LL_LPTIM_DisablePrescaler(void)
{
    CLEAR_BIT(LPTIMER0->CLKCFG, LPTIMER_CLKCTRL_PRESEN);
}


/**
 * @brief  Get low power timer prescaler Enable status.
 * @rmtoll CLKCFG      LL_LPTIM_IsEnablePrescaler
 * @param
 * @param
 * @retval 0 or !0
 */
__STATIC_INLINE uint32_t LL_LPTIM_IsEnablePrescaler(void)
{
    return (READ_BIT(LPTIMER0->CLKCFG, LPTIMER_CLKCTRL_PRESEN) == LPTIMER_CLKCTRL_PRESEN);
}


/**
 * @brief  Set the clk prescaler
 * @rmtoll CLKCFG            LL_LPTIM_SetPrescaler
 * @param  clk_pin This parameter can be one of the following values:
 *         @arg @ref LL_LPTIM_PSC_DIV2
 *         @arg @ref LL_LPTIM_PSC_DIV4
 *         @arg @ref LL_LPTIM_PSC_DIV8
 *         @arg @ref LL_LPTIM_PSC_DIV16
 *         @arg @ref LL_LPTIM_PSC_DIV32
 *         @arg @ref LL_LPTIM_PSC_DIV64
 *         @arg @ref LL_LPTIM_PSC_DIV128
 *         @arg @ref LL_LPTIM_PSC_DIV256
 *         @arg @ref LL_LPTIM_PSC_DIV512
 *         @arg @ref LL_LPTIM_PSC_DIV1024
 *         @arg @ref LL_LPTIM_PSC_DIV2048
 *         @arg @ref LL_LPTIM_PSC_DIV4096
 *         @arg @ref LL_LPTIM_PSC_DIV8192
 *         @arg @ref LL_LPTIM_PSC_DIV16384
 *         @arg @ref LL_LPTIM_PSC_DIV32768
 *         @arg @ref LL_LPTIM_PSC_DIV65536
 * @retval None
 */
__STATIC_INLINE void LL_LPTIM_SetPrescaler(uint8_t Prescaler)
{
    LL_LPTIM_EnablePrescaler();//Enable prescaler
    MODIFY_REG(LPTIMER0->CLKCFG, LPTIMER_CLKCTRL_PRESCFG, Prescaler << LPTIMER_CLKCTRL_PRESCFG_Pos);
}


/**
 * @brief  Get the clk prescaler
 * @rmtoll CLKCFG            LL_LPTIM_GetPrescaler
 * @param  clk_pin This parameter can be one of the following values:
 *         @arg @ref LL_LPTIM_PSC_DIV2
 *         @arg @ref LL_LPTIM_PSC_DIV4
 *         @arg @ref LL_LPTIM_PSC_DIV8
 *         @arg @ref LL_LPTIM_PSC_DIV16
 *         @arg @ref LL_LPTIM_PSC_DIV32
 *         @arg @ref LL_LPTIM_PSC_DIV64
 *         @arg @ref LL_LPTIM_PSC_DIV128
 *         @arg @ref LL_LPTIM_PSC_DIV256
 *         @arg @ref LL_LPTIM_PSC_DIV512
 *         @arg @ref LL_LPTIM_PSC_DIV1024
 *         @arg @ref LL_LPTIM_PSC_DIV2048
 *         @arg @ref LL_LPTIM_PSC_DIV4096
 *         @arg @ref LL_LPTIM_PSC_DIV8192
 *         @arg @ref LL_LPTIM_PSC_DIV16384
 *         @arg @ref LL_LPTIM_PSC_DIV32768
 *         @arg @ref LL_LPTIM_PSC_DIV65536
 * @retval None
 */
__STATIC_INLINE uint8_t LL_LPTIM_GetPrescaler(void)
{
    return ((uint8_t)(READ_BIT(LPTIMER0->CLKCFG, LPTIMER_CLKCTRL_PRESCFG) >> LPTIMER_CLKCTRL_PRESCFG_Pos));
}


/**
 * @brief  Set the clk pin
 * @rmtoll CLKCFG            LL_LPTIM_ConfigPadClkPin
 * @param  clk_pin This parameter can be one of the following values:
 *         @arg @ref LL_LPTIM_PAD_CLK_SEL_PD0
 *         @arg @ref LL_LPTIM_PAD_CLK_SEL_PD1
 *         @arg @ref LL_LPTIM_PAD_CLK_SEL_PD2
 *         @arg @ref LL_LPTIM_PAD_CLK_SEL_PD3
 *         @arg @ref LL_LPTIM_PAD_CLK_SEL_PD4
 *         @arg @ref LL_LPTIM_PAD_CLK_SEL_PD5
 *         @arg @ref LL_LPTIM_PAD_CLK_SEL_PD6
 *         @arg @ref LL_LPTIM_PAD_CLK_SEL_PD7
 *         @arg @ref LL_LPTIM_PAD_CLK_SEL_PC4
 *         @arg @ref LL_LPTIM_PAD_CLK_SEL_PC5
 *         @arg @ref LL_LPTIM_PAD_CLK_SEL_PC6
 *         @arg @ref LL_LPTIM_PAD_CLK_SEL_PC7
 *         @arg @ref LL_LPTIM_PAD_CLK_SEL_PF0
 *         @arg @ref LL_LPTIM_PAD_CLK_SEL_PF1
 *         @arg @ref LL_LPTIM_PAD_CLK_SEL_PF2
 *         @arg @ref LL_LPTIM_PAD_CLK_SEL_PF3
 * @retval None
 */

__STATIC_INLINE void LL_LPTIM_ConfigPadClkPin(uint8_t clk_pin)
{
    MODIFY_REG(LPTIMER0->CLKCFG, LPTIMER_CLKCTRL_PADCLKSEL, clk_pin << LPTIMER_CLKCTRL_PADCLKSEL_Pos);
}


/**
 * @brief  Get the clk pin
 * @rmtoll CLKCFG            LL_LPTIM_GetPadClkPin
 * @param  None
 * @retval This parameter can be one of the following values:
 *         @arg @ref LL_LPTIM_PAD_CLK_SEL_PD0
 *         @arg @ref LL_LPTIM_PAD_CLK_SEL_PD1
 *         @arg @ref LL_LPTIM_PAD_CLK_SEL_PD2
 *         @arg @ref LL_LPTIM_PAD_CLK_SEL_PD3
 *         @arg @ref LL_LPTIM_PAD_CLK_SEL_PD4
 *         @arg @ref LL_LPTIM_PAD_CLK_SEL_PD5
 *         @arg @ref LL_LPTIM_PAD_CLK_SEL_PD6
 *         @arg @ref LL_LPTIM_PAD_CLK_SEL_PD7
 *         @arg @ref LL_LPTIM_PAD_CLK_SEL_PC4
 *         @arg @ref LL_LPTIM_PAD_CLK_SEL_PC5
 *         @arg @ref LL_LPTIM_PAD_CLK_SEL_PC6
 *         @arg @ref LL_LPTIM_PAD_CLK_SEL_PC7
 *         @arg @ref LL_LPTIM_PAD_CLK_SEL_PF0
 *         @arg @ref LL_LPTIM_PAD_CLK_SEL_PF1
 *         @arg @ref LL_LPTIM_PAD_CLK_SEL_PF2
 *         @arg @ref LL_LPTIM_PAD_CLK_SEL_PF3
 */
__STATIC_INLINE uint8_t LL_LPTIM_GetPadClkPin(void)
{
    return ((uint8_t)(READ_BIT(LPTIMER0->CLKCFG, LPTIMER_CLKCTRL_PADCLKSEL) >> LPTIMER_CLKCTRL_PADCLKSEL_Pos));
}


/**
 * @brief  Set low power timer run mode
 * @rmtoll CFG               LL_LPTIM_SetRunMode
 * @param  This parameter can be one of the following values:
 *         @arg @ref LL_LPTIM_RUN_MODE_TIME
 *         @arg @ref LL_LPTIM_RUN_MODE_PULSE
 * @retval None
 */
__STATIC_INLINE void LL_LPTIM_SetRunMode(uint8_t mode)
{
    MODIFY_REG(LPTIMER0->CFG, LPTIMER_CFG_RUNM, mode);
}


/**
 * @brief  Set low power timer run mode
 * @rmtoll CFG               LL_LPTIM_GetRunMode
 * @param  None
 * @retval This parameter can be one of the following values:
 *         @arg @ref LL_LPTIM_RUN_MODE_TIME
 *         @arg @ref LL_LPTIM_RUN_MODE_PULSE
 */
__STATIC_INLINE uint8_t LL_LPTIM_GetRunMode(void)
{
    return ((uint8_t)(READ_BIT(LPTIMER0->CFG, LPTIMER_CFG_RUNM) >> LPTIMER_CFG_RUNM_Pos));
}


/**
 * @brief  Set low power timer free mode
 * @rmtoll CFG               LL_LPTIM_SetCountMode
 * @param  This parameter can be one of the following values:
 *         @arg @ref LL_LPTIM_FREE_MODE_CMR
 *         @arg @ref LL_LPTIM_FREE_MODE_FREE
 * @retval None
 */
__STATIC_INLINE void LL_LPTIM_SetCountMode(uint8_t mode)
{
    MODIFY_REG(LPTIMER0->CFG, LPTIMER_CFG_FREEM, mode << LPTIMER_CFG_FREEM_Pos);
}


/**
 * @brief  Get low power timer free mode
 * @rmtoll CFG               LL_LPTIM_SetFreeMode
 * @param  None
 * @retval This parameter can be one of the following values:
 *         @arg @ref LL_LPTIM_FREE_MODE_CMR
 *         @arg @ref LL_LPTIM_FREE_MODE_FREE
 */
__STATIC_INLINE uint8_t LL_LPTIM_IsFreeMode(void)
{
    return ((uint8_t)(READ_BIT(LPTIMER0->CFG, LPTIMER_CFG_FREEM) == LPTIMER_CFG_FREEM));
}


/**
 * @brief  Enable low power timer input polar reversal
 * @rmtoll CFG               LL_LPTIM_EnableTrigInvert
 * @param  None
 * @retval None
 */
__STATIC_INLINE void LL_LPTIM_EnableTrigInv(void)
{
    SET_BIT(LPTIMER0->CFG, LPTIMER_CFG_INPOLAR);
}


/**
 * @brief  Disable low power timer input polar
 * @rmtoll CFG               LL_LPTIM_DisableTrigInvert
 * @param  None
 * @retval None
 */
__STATIC_INLINE void LL_LPTIM_DisableTrigInv(void)
{
    CLEAR_BIT(LPTIMER0->CFG, LPTIMER_CFG_INPOLAR);
}


/**
 * @brief  Get low power timer Trigger input invert
 * @rmtoll CFG              LL_LPTIM_IsTrigInv
 * @param  None
 * @retval 0 or !0
 */
__STATIC_INLINE uint32_t LL_LPTIM_IsTrigInv(void)
{
    return (READ_BIT(LPTIMER0->CFG, LPTIMER_CFG_INPOLAR) == LPTIMER_CFG_INPOLAR);
}


/**
 * @brief  set low power timer trigger source
 * @rmtoll CFG               LL_LPTIM_SetTriggerSource
 * @param  This parameter can be one of the following values:
 *         @arg @ref LL_LPTIM_TRIGSRC_PD0
 *         @arg @ref LL_LPTIM_TRIGSRC_PD1
 *         @arg @ref LL_LPTIM_TRIGSRC_PD2
 *         @arg @ref LL_LPTIM_TRIGSRC_PD3
 *         @arg @ref LL_LPTIM_TRIGSRC_PD4
 *         @arg @ref LL_LPTIM_TRIGSRC_PD5
 *         @arg @ref LL_LPTIM_TRIGSRC_PD6
 *         @arg @ref LL_LPTIM_TRIGSRC_PD7
 *         @arg @ref LL_LPTIM_TRIGSRC_PC4
 *         @arg @ref LL_LPTIM_TRIGSRC_PC5
 *         @arg @ref LL_LPTIM_TRIGSRC_PC6
 *         @arg @ref LL_LPTIM_TRIGSRC_PC7
 *         @arg @ref LL_LPTIM_TRIGSRC_PF0
 *         @arg @ref LL_LPTIM_TRIGSRC_PF1
 *         @arg @ref LL_LPTIM_TRIGSRC_PF2
 *         @arg @ref LL_LPTIM_TRIGSRC_PF3
 *         @arg @ref LL_LPTIM_TRIGSRC_LPTIM_WAKEUP
 *         @arg @ref LL_LPTIM_TRIGSRC_CMP
 *         @arg @ref LL_LPTIM_TRIGSRC_PHY_RXDP
 *         @arg @ref LL_LPTIM_TRIGSRC_PHY_RXDM
 *         @arg @ref LL_LPTIM_TRIGSRC_VBUS_DET
 *         @arg @ref LL_LPTIM_TRIGSRC_RTC_ALARM
 *         @arg @ref LL_LPTIM_TRIGSRC_BKP_TAMPER
 *         @arg @ref LL_LPTIM_TRIGSRC_RTC_1S
 *         @arg @ref LL_LPTIM_TRIGSRC_PMU_CHG90
 *         @arg @ref LL_LPTIM_TRIGSRC_PMU_CHGEOC
 *         @arg @ref LL_LPTIM_TRIGSRC_PF4
 *         @arg @ref LL_LPTIM_TRIGSRC_JTAG_SEL
 *         @arg @ref LL_LPTIM_TRIGSRC_STANDBY_WAKEUP
 * @retval None
 */
__STATIC_INLINE void LL_LPTIM_SetTrigSrc(uint8_t trigger)
{
    MODIFY_REG(LPTIMER0->CFG, LPTIMER_CFG_INSEL, trigger << LPTIMER_CFG_INSEL_Pos);
}


/**
 * @brief  Get low power timer trigger source
 * @rmtoll CFG               LL_LPTIM_GetTriggerSource
 * @param  None
 * @retval This parameter can be one of the following values:
 *         @arg @ref LL_LPTIM_TRIGSRC_PD0
 *         @arg @ref LL_LPTIM_TRIGSRC_PD1
 *         @arg @ref LL_LPTIM_TRIGSRC_PD2
 *         @arg @ref LL_LPTIM_TRIGSRC_PD3
 *         @arg @ref LL_LPTIM_TRIGSRC_PD4
 *         @arg @ref LL_LPTIM_TRIGSRC_PD5
 *         @arg @ref LL_LPTIM_TRIGSRC_PD6
 *         @arg @ref LL_LPTIM_TRIGSRC_PD7
 *         @arg @ref LL_LPTIM_TRIGSRC_PC4
 *         @arg @ref LL_LPTIM_TRIGSRC_PC5
 *         @arg @ref LL_LPTIM_TRIGSRC_PC6
 *         @arg @ref LL_LPTIM_TRIGSRC_PC7
 *         @arg @ref LL_LPTIM_TRIGSRC_PF0
 *         @arg @ref LL_LPTIM_TRIGSRC_PF1
 *         @arg @ref LL_LPTIM_TRIGSRC_PF2
 *         @arg @ref LL_LPTIM_TRIGSRC_PF3
 *         @arg @ref LL_LPTIM_TRIGSRC_LPTIM_WAKEUP
 *         @arg @ref LL_LPTIM_TRIGSRC_CMP
 *         @arg @ref LL_LPTIM_TRIGSRC_PHY_RXDP
 *         @arg @ref LL_LPTIM_TRIGSRC_PHY_RXDM
 *         @arg @ref LL_LPTIM_TRIGSRC_VBUS_DET
 *         @arg @ref LL_LPTIM_TRIGSRC_RTC_ALARM
 *         @arg @ref LL_LPTIM_TRIGSRC_BKP_TAMPER
 *         @arg @ref LL_LPTIM_TRIGSRC_RTC_1S
 *         @arg @ref LL_LPTIM_TRIGSRC_PMU_CHG90
 *         @arg @ref LL_LPTIM_TRIGSRC_PMU_CHGEOC
 *         @arg @ref LL_LPTIM_TRIGSRC_PF4
 *         @arg @ref LL_LPTIM_TRIGSRC_JTAG_SEL
 *         @arg @ref LL_LPTIM_TRIGSRC_STANDBY_WAKEUP
 */
__STATIC_INLINE uint8_t LL_LPTIM_GetTrigSrc(void)
{
    return ((uint8_t)(READ_BIT(LPTIMER0->CFG, LPTIMER_CFG_INSEL) >> LPTIMER_CFG_INSEL_Pos));
}


/**
 * @brief  Set low power timer mod value
 * @rmtoll MOD                     LL_LPTIM_SetModValue
 * @param  MOD0Value
 * @retval None
 */
__STATIC_INLINE void LL_LPTIM_SetModValue(uint16_t ModValue)
{
    WRITE_REG(LPTIMER0->MOD, ModValue);
}


/**
 * @brief  Get low power timer mod value
 * @rmtoll MOD                     LL_LPTIM_GetModValue
 * @param  None
 * @retval MOD0Value
 */
__STATIC_INLINE uint16_t LL_LPTIM_GetModValue(void)
{
    return ((uint16_t)(READ_REG(LPTIMER0->MOD)));
}


/**
 * @brief  Get low power timer current value
 * @rmtoll CVAL                     LL_LPTIM_GetCurrentValue
 * @param  None
 * @retval current value
 */
__STATIC_INLINE uint16_t LL_LPTIM_GetCurrentValue(void)
{
    return ((uint16_t)(READ_REG(LPTIMER0->CVAL)));
}


/**
 * @brief  Clear low power timer over flag
 * @note   The hardware automatically returns this bit back to 0
 * @rmtoll FLAGCLR                     LL_LPTIM_ClearFlag_Over
 * @param  None
 * @retval None
 */
__STATIC_INLINE void LL_LPTIM_ClearFlag_Over(void)
{
    SET_BIT(LPTIMER0->FLAGCLR, LPTIMER_IFCLR_OVF);
}


__STATIC_INLINE void LL_LPTIM_ClearFlag_IT(void)
{
    SET_BIT(LPTIMER0->FLAGCLR, LPTIMER_IFCLR_OVF);
}


/**
 * @brief  Get low power timer over flag
 * @rmtoll FLAGSTATUS                     LL_LPTIM_IsActiveOverFlag
 * @param  None
 * @retval 0 or !0
 */
__STATIC_INLINE uint32_t LL_LPTIM_IsActiveFlag_Over(void)
{
    return (READ_BIT(LPTIMER0->FLAGSTATUS, LPTIMER_FLAGSTATUS_OVF) == LPTIMER_FLAGSTATUS_OVF);
}


void LL_LPTIM_Init(LL_LPTIM_InitTypeDef *LPTIM_InitStruct);


#ifdef __cplusplus
}
#endif
#endif
/************************ (C) COPYRIGHT FudanMicroelectronics *****END OF FILE****/

