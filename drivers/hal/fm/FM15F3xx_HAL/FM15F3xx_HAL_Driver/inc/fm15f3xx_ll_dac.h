/**
 ******************************************************************************
 * @file    fm15f3xx_ll_dac.h
 * @author  SRG
 * @version V1.0.0
 * @date    2020-03-17
 * @brief
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2020 FudanMicroelectronics.
 * All rights reserved.</center></h2>
 *
 ******************************************************************************
 */

#ifndef FM15F3XX_LL_DAC_H
#define FM15F3XX_LL_DAC_H
#ifdef __cplusplus
extern "C" {
#endif

#include "fm15f3xx.h"

#if defined (DAC0)


/** @defgroup DAC_LL DAC
 * @{
 */

/* Private types -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private constants ---------------------------------------------------------*/
/* Private macros ------------------------------------------------------------*/


/** @defgroup DAC_LL_EM_WRITE_READ Common Write and read registers Macros
 * @{
 */


/**
 * @brief  Write a value in DAC register
 * @param  __INSTANCE__ DAC Instance
 * @param  __REG__ Register to be written
 * @param  __VALUE__ Value to be written in the register
 * @retval None
 */
#define LL_DAC_WriteReg( __INSTANCE__, __REG__, __VALUE__ ) WRITE_REG( __INSTANCE__->__REG__, (__VALUE__) )


/**
 * @brief  Read a value in DAC register
 * @param  __INSTANCE__ DAC Instance
 * @param  __REG__ Register to be read
 * @retval Register value
 */
#define LL_DAC_ReadReg( __INSTANCE__, __REG__ ) READ_REG( __INSTANCE__->__REG__ )


/**
 * @}
 */

/* ExDACed functions --------------------------------------------------------*/


/** @defgroup DAC_LL_ExDACed_Functions DAC ExSPIed Functions
 * @{
 */


/**
 * @brief  DAC Init structures definition
 */
typedef struct {
    uint32_t DACEN;                     /* Specifies DAC module enable or disable. dac_ctrl reg.
                                           This parameter can be a value of @ref DAC_LL_DAC_EN.
                                           This feature can be modified afterwards using unitary function @ref LL_DAC_SetClockPhase().*/
    uint32_t VrefSel;                   /* Specifies refrence select. dac_cfg reg.
                                           This parameter can be a value of @ref DAC_LL_VREF_SEL.
                                           This feature can be modified afterwards using unitary function @ref LL_DAC_SetClockPolarity().*/
    uint32_t HighPowerEn;               /* Specifies high power mode.  dac_cfg reg.
                                           This parameter can be a value of @ref DAC_LL_HighPowerEn.
                                           This feature can be modified afterwards using unitary function @ref LL_DAC_SetMode().*/
    uint32_t PointerMode;               /* Specifies Pointer mode:single,up,swing,RUF.  dac_work_cfg reg.
                                           This parameter can be a value of @ref DAC_LL_PointerMode.
                                           This feature can be modified afterwards using unitary function @ref LL_DAC_SetTransferBitOrder().*/
    uint32_t ScanEn;                    /* Specifies scan mode.  dac_work_cfg reg.
                                             This parameter can be a value of @ref DAC_LL_ScanModeEn.
                                             This feature can be modified afterwards using unitary function @ref LL_DAC_SetSSNMode().*/
    uint32_t ScanMode;                  /* Specifies scan loop continiualy or only onetime. dac_work_cfg reg.
                                           This parameter can be a value of @ref DAC_LL_ScanOneShot.
                                           @note The communication clock is derived from the master clock. The slave clock does not need to be set.
                                           This feature can be modified afterwards using unitary function @ref LL_DAC_SetBaudRatePrescaler().*/
    uint32_t ScanInterval;              /* Specifies delay cycle(n sys_clk) for data out to convert in scan mode. dac_work_cfg reg.
                                             This parameter can be a value of @ref DAC_LL_ScanInterval.
                                             @note The communication clock is derived from the master clock. The slave clock does not need to be set.
                                             This feature can be modified afterwards using unitary function @ref LL_DAC_SetBaudRatePrescaler().*/
    uint32_t TrigSource;                /* Specifies Triger source select. dac_trig_cfg reg.
                                           This parameter can be a value of @ref DAC_LL_TrigSource.
                                           @note The communication clock is derived from the master clock. The slave clock does not need to be set.
                                           This feature can be modified afterwards using unitary function @ref LL_DAC_SetBaudRatePrescaler().*/
    uint32_t TrigInvEn;                 /* Enable the trigger inverter.   dac_trig_cfg reg.
                                           This parameter can be a value of @ref DAC_LL_TrigInvEn.
                                           This feature can be modified afterwards using unitary function @ref LL_DAC_SetNSSMode().*/
    uint32_t TrigEdge;                  /* Specifies rising or falling edge trig.  dac_trig_cfg reg.
                                           This parameter can be a value of @ref DAC_LL_TrigEdge.
                                           This feature can be modified afterwards using unitary function @ref LL_DAC_SetNSSMode().*/
    uint32_t TrigFilter;                /* Specifies trigger filter clock cycle; dac_trig_cfg reg.
                                           this bit will clear automatically while transfer is completed.
                                           This parameter can be a value of @ref DAC_LL_TrigFilt.
                                           This feature can be modified afterwards using unitary function @ref LL_DAC_SetNSSMode().*/
} LL_DAC_InitTypeDef;


/**SPCR1****************************
 * @}
 */


/** @defgroup DAC_LL_DAC_EN module enable
 * @{
 */
#define LL_DAC_DISABLE  0x00000000U                                 /* First clock transition is the first data capture edge(default)  */
#define LL_DAC_ENABLE   (DAC_CTRL_DACEN)                            /* Second clock transition is the first data capture edge */


/**
 * @}
 */


/** @defgroup DAC_LL_VREF_SEL refrence select
 * @{
 */
#define LL_DAC_VREFSEL_1P2V 0x00000000U                             /* DAC Vrefrence select Vref12 */
#define LL_DAC_VREFSEL_VDDA (DAC_CFG_VSEL)                          /* DAC Vrefrence select VDDA */


/**
 * @}
 */


/** @defgroup DAC_LL_HighPowerEn high power Mode
 * @{
 */
#define LL_DAC_HIGHPOWER_DISABLE    0x00000000U                     /* normal mode   */
#define LL_DAC_HIGHPOWER_ENABLE     (DAC_CFG_HPEN)                  /* high power mode  */


/**
 * @}
 */


/** @defgroup DAC_LL_PointerMode pointer mode
 * @{
 */
#define LL_DAC_POINTERMODE_FIX      0x00000000U                     /* fix mode   */
#define LL_DAC_POINTERMODE_UP       (0x1U << DAC_WORK_PTRM_Pos)     /* up mode  */
#define LL_DAC_POINTERMODE_SWING    (0x2U << DAC_WORK_PTRM_Pos)     /* swing mode  */


/**
 * @}
 */


/** @defgroup DAC_LL_ScanModeEn Scan mode
 * @{
 */
#define LL_DAC_SCAN_TRIG    0x00000000U                             /* DAC work in trig mode  */
#define LL_DAC_SCAN_DISABLE 0x00000000U                             /* DAC work in trig mode  */
#define LL_DAC_SCAN_ENABLE  (DAC_WORK_SCANME)                       /* DAC work in scan mode  */


/**
 * @}
 */


/** @defgroup DAC_LL_ScanOneShot continue/onetime
 * @{
 */
#define LL_DAC_SCANMODE_CONTINUE    0x00000000U                     /* scan loop continually run   */
#define LL_DAC_SCANMODE_ONESHOT     (DAC_WORK_SCANOS)               /* scan loop only run onetime */


/**
 * @}
 */


/** @defgroup DAC_LL_TrigSource trigger source
 * @{
 */
#define LL_DAC_TRIGSRC_TRIGIN           0x00000000U                 /* trigger source select PAD              */
#define LL_DAC_TRIGSRC_CMP              (0x1U << DAC_TRIG_TSC_Pos)  /* trigger source select CMP              */
#define LL_DAC_TRIGSRC_ST0              (0x2U << DAC_TRIG_TSC_Pos)  /* trigger source select STimer0          */
#define LL_DAC_TRIGSRC_ST1              (0x3U << DAC_TRIG_TSC_Pos)  /* trigger source select STimer1          */
#define LL_DAC_TRIGSRC_ST2              (0x4U << DAC_TRIG_TSC_Pos)  /* trigger source select STimer2          */
#define LL_DAC_TRIGSRC_ST3              (0x5U << DAC_TRIG_TSC_Pos)  /* trigger source select STimer3          */
#define LL_DAC_TRIGSRC_ST4              (0x6U << DAC_TRIG_TSC_Pos)  /* trigger source select STimer4          */
#define LL_DAC_TRIGSRC_ST5              (0x7U << DAC_TRIG_TSC_Pos)  /* trigger source select STimer5          */
#define LL_DAC_TRIGSRC_LPTIM0           (0x8U << DAC_TRIG_TSC_Pos)  /* trigger source select LPTimer0         */
#define LL_DAC_TRIGSRC_TGSCH6           (0x9U << DAC_TRIG_TSC_Pos)  /* trigger source select STimer TGS CH6   */
#define LL_DAC_TRIGSRC_TGSCH7           (0xAU << DAC_TRIG_TSC_Pos)  /* trigger source select STimer TGS CH7   */
#define LL_DAC_TRIGSRC_RTCALARM_IT      (0xBU << DAC_TRIG_TSC_Pos)  /* trigger source select RTC Alarm INT    */
#define LL_DAC_TRIGSRC_TAMPERALARM_IT   (0xCU << DAC_TRIG_TSC_Pos)  /* trigger source select Tamper Alarm INT */
#define LL_DAC_TRIGSRC_RTC1S_IT         (0xDU << DAC_TRIG_TSC_Pos)  /* trigger source select RTC Seconds INT  */
#define LL_DAC_TRIGSRC_SOFTWARE         (0xEU << DAC_TRIG_TSC_Pos)  /* trigger source select Software         */


/**
 * @}
 */


/** @defgroup DAC_LL_TrigInvEn trigger via inverter
 * @{
 */
#define LL_DAC_TRIGINV_DISABLE  0x00000000U                         /* normal trig  */
#define LL_DAC_TRIGINV_ENABLE   (DAC_TRIG_TINV)                     /* invert trig  */


/**
 * @}
 */


/** @defgroup DAC_LL_TrigEdge Level/rising or falling edge trig
 * @{
 */
#define LL_DAC_TRIGEDGE_HIGH        0x00000000U                     /* High Level trig  */
#define LL_DAC_TRIGEDGE_RISING      (0x1U << DAC_TRIG_TEDGE_Pos)    /* Rising Edge trig  */
#define LL_DAC_TRIGEDGE_FALLING     (0x2U << DAC_TRIG_TEDGE_Pos)    /* Falling Edge trig  */
#define LL_DAC_TRIGEDGE_BOTHEDGE    (0x3U << DAC_TRIG_TEDGE_Pos)    /* rising or falling edge trig  */


/**
 * @}
 */


/** @defgroup DAC_LL_TrigFilt set filter cycle
 * @{
 */
#define LL_DAC_TRIGFILTER_NOFILTER  0x00000000U                     /* not filt  */
#define LL_DAC_TRIGFILTER_1CYCLE    (0x1U << DAC_TRIG_TFILT_Pos)    /* 1 cycle filter  */
#define LL_DAC_TRIGFILTER_2CYCLE    (0x2U << DAC_TRIG_TFILT_Pos)    /* 2 cycle filter  */
#define LL_DAC_TRIGFILTER_3CYCLE    (0x3U << DAC_TRIG_TFILT_Pos)    /* 3 cycle filter  */
#define LL_DAC_TRIGFILTER_4CYCLE    (0x4U << DAC_TRIG_TFILT_Pos)    /* 4 cycle filter  */
#define LL_DAC_TRIGFILTER_5CYCLE    (0x5U << DAC_TRIG_TFILT_Pos)    /* 5 cycle filter  */
#define LL_DAC_TRIGFILTER_6CYCLE    (0x6U << DAC_TRIG_TFILT_Pos)    /* 6 cycle filter  */
#define LL_DAC_TRIGFILTER_7CYCLE    (0x7U << DAC_TRIG_TFILT_Pos)    /* 7 cycle filter  */
#define LL_DAC_TRIGFILTER_8CYCLE    (0x8U << DAC_TRIG_TFILT_Pos)    /* 8 cycle filter  */
#define LL_DAC_TRIGFILTER_9CYCLE    (0x9U << DAC_TRIG_TFILT_Pos)    /* 9 cycle filter  */
#define LL_DAC_TRIGFILTER_10CYCLE   (0xAU << DAC_TRIG_TFILT_Pos)    /* 10 cycle filter  */
#define LL_DAC_TRIGFILTER_11CYCLE   (0xBU << DAC_TRIG_TFILT_Pos)    /* 11 cycle filter  */
#define LL_DAC_TRIGFILTER_12CYCLE   (0xCU << DAC_TRIG_TFILT_Pos)    /* 12 cycle filter  */
#define LL_DAC_TRIGFILTER_13CYCLE   (0xDU << DAC_TRIG_TFILT_Pos)    /* 13 cycle filter  */
#define LL_DAC_TRIGFILTER_14CYCLE   (0xEU << DAC_TRIG_TFILT_Pos)    /* 14 cycle filter  */
#define LL_DAC_TRIGFILTER_15CYCLE   (0xFU << DAC_TRIG_TFILT_Pos)    /* 15 cycle filter  */


/**
 * @}
 */


/** @defgroup DACDataPointer DAC data pointer
 * @{
 */
#define LL_DAC_DATAPOINTER_0    0x00000000U                         /* position 0  */
#define LL_DAC_DATAPOINTER_1    (0x1U << DAC_DPTR_DPTR_Pos)         /* position 1  */
#define LL_DAC_DATAPOINTER_2    (0x2U << DAC_DPTR_DPTR_Pos)         /* position 2  */
#define LL_DAC_DATAPOINTER_3    (0x3U << DAC_DPTR_DPTR_Pos)         /* position 3  */
#define LL_DAC_DATAPOINTER_4    (0x4U << DAC_DPTR_DPTR_Pos)         /* position 4  */
#define LL_DAC_DATAPOINTER_5    (0x5U << DAC_DPTR_DPTR_Pos)         /* position 5  */
#define LL_DAC_DATAPOINTER_6    (0x6U << DAC_DPTR_DPTR_Pos)         /* position 6  */
#define LL_DAC_DATAPOINTER_7    (0x7U << DAC_DPTR_DPTR_Pos)         /* position 7  */


/**
 * @}
 */


/** @defgroup TopPointer top pointer
 * @{
 */
#define LL_DAC_TOPPOINTER_0 0x00000000U                             /* position 0  */
#define LL_DAC_TOPPOINTER_1 (0x1U << DAC_DPCFG_TOP_Pos)             /* position 1  */
#define LL_DAC_TOPPOINTER_2 (0x2U << DAC_DPCFG_TOP_Pos)             /* position 2  */
#define LL_DAC_TOPPOINTER_3 (0x3U << DAC_DPCFG_TOP_Pos)             /* position 3  */
#define LL_DAC_TOPPOINTER_4 (0x4U << DAC_DPCFG_TOP_Pos)             /* position 4  */
#define LL_DAC_TOPPOINTER_5 (0x5U << DAC_DPCFG_TOP_Pos)             /* position 5  */
#define LL_DAC_TOPPOINTER_6 (0x6U << DAC_DPCFG_TOP_Pos)             /* position 6  */
#define LL_DAC_TOPPOINTER_7 (0x7U << DAC_DPCFG_TOP_Pos)             /* position 7  */


/**
 * @}
 */


/** @defgroup WatermarkPointer Watermark pointer
 * @{
 */
#define LL_DAC_WATERMARKPOINTER_0   0x00000000U                     /* position 0  */
#define LL_DAC_WATERMARKPOINTER_1   (0x1U << DAC_DPCFG_WMK_Pos)     /* position 1  */
#define LL_DAC_WATERMARKPOINTER_2   (0x2U << DAC_DPCFG_WMK_Pos)     /* position 2  */
#define LL_DAC_WATERMARKPOINTER_3   (0x3U << DAC_DPCFG_WMK_Pos)     /* position 3  */
#define LL_DAC_WATERMARKPOINTER_4   (0x4U << DAC_DPCFG_WMK_Pos)     /* position 4  */
#define LL_DAC_WATERMARKPOINTER_5   (0x5U << DAC_DPCFG_WMK_Pos)     /* position 5  */
#define LL_DAC_WATERMARKPOINTER_6   (0x6U << DAC_DPCFG_WMK_Pos)     /* position 6  */
#define LL_DAC_WATERMARKPOINTER_7   (0x7U << DAC_DPCFG_WMK_Pos)     /* position 7  */


/**
 * @}
 */


/** @defgroup DAC_LL_DataN dataN
 * @{
 */
#define LL_DAC_DATA( _VALUE_ ) ( (_VALUE_) << DAC_DATAN_DATA_Pos)   /* data0~7   */


/**
 * @}
 */


/** @defgroup DAC_LL_Flag Flag(read only)
 * @{
 */
#define LL_DAC_FLAG_BOTTOM      (DAC_FLAG_BOTTOMF)                  /* bottom flag   */
#define LL_DAC_FLAG_TOP         (DAC_FLAG_TOPF)                     /* top flag   */
#define LL_DAC_FLAG_WATERMARK   (DAC_FLAG_WMKF)                     /* watermark flag   */


/**
 * @}
 */


/** @defgroup DAC_LL_BottomIntDmaEn bottom_int_dma_en
 * @{
 */
#define LL_DAC_INT_BOTTOM       (DAC_INTEN_BTMIDE)
#define LL_DAC_INT_TOP          (DAC_INTEN_TOPIDE)
#define LL_DAC_INT_WATERMARK    (DAC_INTEN_WMKIDE)

#define LL_DAC_DMA_BOTTOM       (DAC_INTEN_BTMIDE | DAC_INTEN_BTMDMAS)
#define LL_DAC_DMA_TOP          (DAC_INTEN_TOPIDE | DAC_INTEN_TOPDMAS)
#define LL_DAC_DMA_WATERMARK    (DAC_INTEN_WMKIDE | DAC_INTEN_WMKDMAS)


/**
 * @}
 */


/* functions --------------------------------------------------------*/


/** @defgroup DAC_LL_Functions DAC Functions
 * @{
 */


/** @defgroup DAC_LL_Configuration Configuration
 * @{
 */


/**
 * @brief  Set DACEN DAC module enable
 * @rmtoll CTRL          dac_en      LL_DAC_SetDACEN
 * @param  DAC DAC Instance
 * @retval None
 */
__STATIC_INLINE void LL_DAC_Enable(DAC_TypeDef *DACx)
{
    SET_BIT(DACx->CTRL, DAC_CTRL_DACEN);
}


/**
 * @brief  Set DACEN DAC module disable
 * @rmtoll CTRL          dac_en      LL_DAC_SetDACEN
 * @param  DAC DAC Instance
 * @retval None
 */
__STATIC_INLINE void LL_DAC_Disable(DAC_TypeDef *DACx)
{
    CLEAR_BIT(DACx->CTRL, DAC_CTRL_DACEN);
}


/**
 * @brief  Get DACEN
 * @rmtoll CTRL          dac_en          LL_DAC_IsEnable
 * @param  DAC DAC Instance
 * @retval 0 or !0
 */
__STATIC_INLINE uint32_t LL_DAC_IsEnable(DAC_TypeDef *DACx)
{
    return (READ_BIT(DACx->CTRL, DAC_CTRL_DACEN) == DAC_CTRL_DACEN);
}


/**
 * @brief  Set VrefSel select DAC refrence 1.2V or Vcc_ana
 * @rmtoll CFG          vref_sel      LL_DAC_SetVrefSource
 * @param  DAC DAC Instance
 * @param  vref_sel This parameter can be one of the following values:
 *         @arg @ref LL_DAC_VREFSEL_1P2V
 *         @arg @ref LL_DAC_VREFSEL_VDDA
 * @retval None
 */
__STATIC_INLINE void LL_DAC_SetVrefSrc(DAC_TypeDef *DACx, uint32_t vref_sel)
{
    MODIFY_REG(DACx->CFG, DAC_CFG_VSEL, vref_sel);
}


/**
 * @brief  Get VrefSel
 * @rmtoll CFG          vref_sel          LL_DAC_GetVrefSource
 * @param  DAC DAC Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_DAC_VREFSEL_1P2V
 *         @arg @ref LL_DAC_VREFSEL_VDDA
 */
__STATIC_INLINE uint32_t LL_DAC_GetVrefSrc(DAC_TypeDef *DACx)
{
    return ((uint32_t)(READ_BIT(DACx->CFG, DAC_CFG_VSEL)));
}


/**
 * @brief  Set HighPowerEn:enable DAC high power mode
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll CFG          high_power_en      LL_DAC_EnableHighPowerMode
 * @param  DAC DAC Instance
 * @retval None
 */
__STATIC_INLINE void LL_DAC_EnableHighPower(DAC_TypeDef *DACx)
{
    SET_BIT(DACx->CFG, DAC_CFG_HPEN);
}


/**
 * @brief  Set HighPowerEn:disable DAC high power mode
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll CFG          high_power_en      LL_DAC_EnableHighPowerMode
 * @param  DAC DAC Instance
 * @retval None
 */
__STATIC_INLINE void LL_DAC_DisableHighPower(DAC_TypeDef *DACx)
{
    CLEAR_BIT(DACx->CFG, DAC_CFG_HPEN);
}


/**
 * @brief  Get HighPowerEn
 * @rmtoll CFG          high_power_en          LL_DAC_IsEnableHighPower
 * @param  DAC DAC Instance
 * @retval 0 or !0
 */
__STATIC_INLINE uint32_t LL_DAC_IsEnableHighPower(DAC_TypeDef *DACx)
{
    return (READ_BIT(DACx->CFG, DAC_CFG_HPEN) == DAC_CFG_HPEN);
}


/**
 * @brief  Set PointerMode:set DAC data buffer Pointer Mode
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll WORK          pointer_mode      LL_DAC_SetPointerMode
 * @param  DAC DAC Instance
 * @param  pointer_mode This parameter can be one of the following values:
 *         @arg @ref LL_DAC_POINTERMODE_FIX
 *         @arg @ref LL_DAC_POINTERMODE_UP
 *         @arg @ref LL_DAC_POINTERMODE_SWING
 * @retval None
 */
__STATIC_INLINE void LL_DAC_SetPointerMode(DAC_TypeDef *DACx, uint32_t pointer_mode)
{
    MODIFY_REG(DACx->WORK, DAC_WORK_PTRM, pointer_mode);
}


/**
 * @brief  Get PointerMode
 * @rmtoll WORK          pointer_mode          LL_DAC_GetPointerMode
 * @param  DAC DAC Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_DAC_POINTERMODE_FIX
 *         @arg @ref LL_DAC_POINTERMODE_UP
 *         @arg @ref LL_DAC_POINTERMODE_SWING
 */
__STATIC_INLINE uint32_t LL_DAC_GetPointerMode(DAC_TypeDef *DACx)
{
    return ((uint32_t)(READ_BIT(DACx->WORK, DAC_WORK_PTRM)));
}


/**
 * @brief  Set ScanModeEn:set DAC data diliver mode (scan/trig)
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll WORK          scan_mode_en      LL_DAC_SetScanMode
 * @param  DAC DAC Instance
 * @param  scan_mode_en This parameter can be one of the following values:
 *         @arg @ref LL_DAC_SCAN_DISABLE /LL_DAC_SCAN_TRIG
 *         @arg @ref LL_DAC_SCAN_ENABLE
 * @retval None
 */
__STATIC_INLINE void LL_DAC_SetScanEn(DAC_TypeDef *DACx, uint32_t scan_en)
{
    MODIFY_REG(DACx->WORK, DAC_WORK_SCANME, scan_en);
}


/**
 * @brief  Get ScanModeEn
 * @rmtoll WORK          scan_mode_en          LL_DAC_GetScanMode
 * @param  DAC DAC Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_DAC_SCAN_DISABLE /LL_DAC_SCAN_TRIG
 *         @arg @ref LL_DAC_SCAN_ENABLE
 */
__STATIC_INLINE uint32_t LL_DAC_IsEnableScan(DAC_TypeDef *DACx)
{
    return ((uint32_t)(READ_BIT(DACx->WORK, DAC_WORK_SCANME) == DAC_WORK_SCANME));
}


/**
 * @brief  Set ScanOneShot:set scan loop contiually or oneshot(onetime)
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll WORK          scan_one_shot      LL_DAC_SetScanOneShot
 * @param  DAC DAC Instance
 * @param  scan_one_shot This parameter can be one of the following values:
 *         @arg @ref LL_DAC_SCANMODE_CONTINUE
 *         @arg @ref LL_DAC_SCANMODE_ONESHOT
 * @retval None
 */
__STATIC_INLINE void LL_DAC_SetScanMode(DAC_TypeDef *DACx, uint32_t scan_mode)
{
    MODIFY_REG(DACx->WORK, DAC_WORK_SCANOS, scan_mode);
}


/**
 * @brief  Enable SCAN Oneshot mode
 * @rmtoll WORK          DAC_WORK_SCANOS      LL_DAC_EnableOneShot
 * @param  DAC DAC Instance
 * @retval None
 */
__STATIC_INLINE void LL_DAC_EnableOneShot(DAC_TypeDef *DACx)
{
    SET_BIT(DACx->WORK, DAC_WORK_SCANOS);
}


/**
 * @brief  Disable SCAN Oneshot mode
 * @rmtoll WORK          DAC_WORK_SCANOS      LL_DAC_DisableOneShot
 * @param  DAC DAC Instance
 * @retval None
 */
__STATIC_INLINE void LL_DAC_DisableOneShot(DAC_TypeDef *DACx)
{
    CLEAR_BIT(DACx->WORK, DAC_WORK_SCANOS);
}


/**
 * @brief  Get ScanOneShot
 * @rmtoll WORK          scan_one_shot          LL_DAC_GetScanOneShot
 * @param  DAC DAC Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_DAC_SCANMODE_CONTINUE
 *         @arg @ref LL_DAC_SCANMODE_ONESHOT
 */
__STATIC_INLINE uint32_t LL_DAC_IsEnableOneShot(DAC_TypeDef *DACx)
{
    return ((uint32_t)(READ_BIT(DACx->WORK, DAC_WORK_SCANOS) == LL_DAC_SCANMODE_ONESHOT));
}


/**
 * @brief  Set ScanInterval:set data out delay cycle(n sys_clk)
 * @note   This bit should not be changed when communication is ongoing.
 *         delaycycle > DAC convert time (max 30us).
 * @rmtoll WORK               LL_DAC_SetScanIntervalValue
 * @param  DAC DAC Instance
 * @param  Value This parameter can be one of the following values:
 *         @arg @ref Value:0x0000U~0xFFFFU
 * @retval None
 */
__STATIC_INLINE void LL_DAC_SetScanIntervalValue(DAC_TypeDef *DACx, uint32_t Value)
{
    MODIFY_REG(DACx->WORK, DAC_WORK_SCANINVL, Value << DAC_WORK_SCANINVL_Pos);
}


/**
 * @brief  Get ScanInterval
 * @rmtoll WORK                    LL_DAC_GetScanIntervalValue
 * @param  DAC DAC Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref Value:0x0000U~0xFFFFU
 */
__STATIC_INLINE uint32_t LL_DAC_GetScanIntervalValue(DAC_TypeDef *DACx)
{
    return ((uint32_t)(READ_BIT(DACx->WORK, DAC_WORK_SCANINVL) >> DAC_WORK_SCANINVL_Pos));
}


/**
 * @brief  Set TrigSource:set trigger source
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll TRIG          trig_sc      LL_DAC_SetTrigSource
 * @param  DAC DAC Instance
 * @param  trig_sc This parameter can be one of the following values:
 *         @arg @ref LL_DAC_TRIGSRC_TRGIN
 *         @arg @ref LL_DAC_TRIGSRC_CMP
 *         @arg @ref LL_DAC_TRIGSRC_STIMER0
 *         @arg @ref LL_DAC_TRIGSRC_STIMER1
 *         @arg @ref LL_DAC_TRIGSRC_STIMER2
 *         @arg @ref LL_DAC_TRIGSRC_STIMER3
 *         @arg @ref LL_DAC_TRIGSRC_STIMER4
 *         @arg @ref LL_DAC_TRIGSRC_STIMER5
 *         @arg @ref LL_DAC_TRIGSRC_LPTIMER0
 *         @arg @ref LL_DAC_TRIGSRC_STIMERTGS_CH6
 *         @arg @ref LL_DAC_TRIGSRC_STIMERTGS_CH7
 *         @arg @ref LL_DAC_TRIGSRC_RTCALARM_IT
 *         @arg @ref LL_DAC_TRIGSRC_TAMPERALARM_IT
 *         @arg @ref LL_DAC_TRIGSRC_RTC1S_IT
 *         @arg @ref LL_DAC_TRIGSRC_SOFTWARE
 * @retval None
 */
__STATIC_INLINE void LL_DAC_SetTrigSrc(DAC_TypeDef *DACx, uint32_t trig_sc)
{
    MODIFY_REG(DACx->TRIG, DAC_TRIG_TSC, trig_sc);
}


/**
 * @brief  Get TrigSource
 * @rmtoll TRIG          trig_sc          LL_DAC_GetTrigSource
 * @param  DAC DAC Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_DAC_TRIGSRC_TRGIN
 *         @arg @ref LL_DAC_TRIGSRC_CMP
 *         @arg @ref LL_DAC_TRIGSRC_STIMER0
 *         @arg @ref LL_DAC_TRIGSRC_STIMER1
 *         @arg @ref LL_DAC_TRIGSRC_STIMER2
 *         @arg @ref LL_DAC_TRIGSRC_STIMER3
 *         @arg @ref LL_DAC_TRIGSRC_STIMER4
 *         @arg @ref LL_DAC_TRIGSRC_STIMER5
 *         @arg @ref LL_DAC_TRIGSRC_LPTIMER0
 *         @arg @ref LL_DAC_TRIGSRC_STIMERTGS_CH6
 *         @arg @ref LL_DAC_TRIGSRC_STIMERTGS_CH7
 *         @arg @ref LL_DAC_TRIGSRC_RTCALARM_IT
 *         @arg @ref LL_DAC_TRIGSRC_TAMPERALARM_IT
 *         @arg @ref LL_DAC_TRIGSRC_RTC1S_IT
 *         @arg @ref LL_DAC_TRIGSRC_SOFTWARE
 */
__STATIC_INLINE uint32_t LL_DAC_GetTrigSrc(DAC_TypeDef *DACx)
{
    return ((uint32_t)(READ_BIT(DACx->TRIG, DAC_TRIG_TSC)));
}


/**
 * @brief  Set TrigInvEn:enable trigger invert
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll TRIG          trig_inv      LL_DAC_EnableTrigInv
 * @param  DAC DAC Instance
 * @retval None
 */
__STATIC_INLINE void LL_DAC_EnableTrigInv(DAC_TypeDef *DACx)
{
    SET_BIT(DACx->TRIG, DAC_TRIG_TINV);
}


/**
 * @brief  Set TrigInvEn:disable trigger inverter
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll TRIG          trig_inv      LL_DAC_DisableTrigInv
 * @param  DAC DAC Instance
 * @retval None
 */
__STATIC_INLINE void LL_DAC_DisableTrigInv(DAC_TypeDef *DACx)
{
    CLEAR_BIT(DACx->TRIG, DAC_TRIG_TINV);
}


/**
 * @brief  Get TrigInvEn
 * @rmtoll TRIG          trig_inv          LL_DAC_IsEnableTrigInv
 * @param  DAC DAC Instance
 * @retval 0 or !0
 */
__STATIC_INLINE uint32_t LL_DAC_IsEnableTrigInv(DAC_TypeDef *DACx)
{
    return ((uint32_t)(READ_BIT(DACx->TRIG, DAC_TRIG_TINV) == DAC_TRIG_TINV));
}


/**
 * @brief  Set TrigEdge:set trig signal
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll TRIG          trig_edge      LL_DAC_SetTrigEdge
 * @param  DAC DAC Instance
 * @param  trig_edge This parameter can be one of the following values:
 *         @arg @ref LL_DAC_TRIGEDGE_HIGH
 *         @arg @ref LL_DAC_TRIGEDGE_RISING
 *         @arg @ref LL_DAC_TRIGEDGE_FALLING
 *         @arg @ref LL_DAC_TRIGEDGE_BOTHEDGE
 * @retval None
 */
__STATIC_INLINE void LL_DAC_SetTrigEdge(DAC_TypeDef *DACx, uint32_t trig_edge)
{
    MODIFY_REG(DACx->TRIG, DAC_TRIG_TEDGE, trig_edge);
}


/**
 * @brief  Get TrigEdge
 * @rmtoll TRIG          trig_edge          LL_DAC_GetTrigEdge
 * @param  DAC DAC Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_DAC_TRIGEDGE_HIGH
 *         @arg @ref LL_DAC_TRIGEDGE_RISING
 *         @arg @ref LL_DAC_TRIGEDGE_FALLING
 *         @arg @ref LL_DAC_TRIGEDGE_BOTHEDGE
 */
__STATIC_INLINE uint32_t LL_DAC_GetTrigEdge(DAC_TypeDef *DACx)
{
    return ((uint32_t)(READ_BIT(DACx->TRIG, DAC_TRIG_TEDGE)));
}


/**
 * @brief  Set TrigFilt:set filter cycle
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll TRIG          trig_filt      LL_DAC_SetTrigFiltCycle
 * @param  DAC DAC Instance
 * @param  trig_filt This parameter can be one of the following values:
 *         @arg @ref LL_DAC_TRIGFILTER_NOFILTER
 *         @arg @ref LL_DAC_TRIGFILTER_1CYCLE
 *         @arg @ref LL_DAC_TRIGFILTER_2CYCLE
 *         @arg @ref LL_DAC_TRIGFILTER_3CYCLE
 *         @arg @ref LL_DAC_TRIGFILTER_4CYCLE
 *         @arg @ref LL_DAC_TRIGFILTER_5CYCLE
 *         @arg @ref LL_DAC_TRIGFILTER_6CYCLE
 *         @arg @ref LL_DAC_TRIGFILTER_7CYCLE
 *         @arg @ref LL_DAC_TRIGFILTER_8CYCLE
 *         @arg @ref LL_DAC_TRIGFILTER_9CYCLE
 *         @arg @ref LL_DAC_TRIGFILTER_10CYCLE
 *         @arg @ref LL_DAC_TRIGFILTER_11CYCLE
 *         @arg @ref LL_DAC_TRIGFILTER_12CYCLE
 *         @arg @ref LL_DAC_TRIGFILTER_13CYCLE
 *         @arg @ref LL_DAC_TRIGFILTER_14CYCLE
 *         @arg @ref LL_DAC_TRIGFILTER_15CYCLE
 * @retval None
 */
__STATIC_INLINE void LL_DAC_SetTrigFilterCnt(DAC_TypeDef *DACx, uint32_t trig_filt)
{
    MODIFY_REG(DACx->TRIG, DAC_TRIG_TFILT, trig_filt);
}


/**
 * @brief  Get TrigFilt
 * @rmtoll TRIG          trig_filt          LL_DAC_GetTrigFilterCnt
 * @param  DAC DAC Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_DAC_TRIGFILTER_NOFILTER
 *         @arg @ref LL_DAC_TRIGFILTER_1CYCLE
 *         @arg @ref LL_DAC_TRIGFILTER_2CYCLE
 *         @arg @ref LL_DAC_TRIGFILTER_3CYCLE
 *         @arg @ref LL_DAC_TRIGFILTER_4CYCLE
 *         @arg @ref LL_DAC_TRIGFILTER_5CYCLE
 *         @arg @ref LL_DAC_TRIGFILTER_6CYCLE
 *         @arg @ref LL_DAC_TRIGFILTER_7CYCLE
 *         @arg @ref LL_DAC_TRIGFILTER_8CYCLE
 *         @arg @ref LL_DAC_TRIGFILTER_9CYCLE
 *         @arg @ref LL_DAC_TRIGFILTER_10CYCLE
 *         @arg @ref LL_DAC_TRIGFILTER_11CYCLE
 *         @arg @ref LL_DAC_TRIGFILTER_12CYCLE
 *         @arg @ref LL_DAC_TRIGFILTER_13CYCLE
 *         @arg @ref LL_DAC_TRIGFILTER_14CYCLE
 *         @arg @ref LL_DAC_TRIGFILTER_15CYCLE
 */
__STATIC_INLINE uint32_t LL_DAC_GetTrigFilterCnt(DAC_TypeDef *DACx)
{
    return ((uint32_t)(READ_BIT(DACx->TRIG, DAC_TRIG_TFILT)));
}


/********dac_sw_trig*******
 * @retval None
 */


/**
 * @brief  Set SwTrigCtrl:set 1 start trig dac convert if trigsource select the  LL_DAC_TrigSource_Software
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll SWTRIG          sw_trig_ctrl      LL_DAC_SoftTrig
 * @param  DAC DAC Instance
 * @retval None
 */
__STATIC_INLINE void LL_DAC_SoftTrig(DAC_TypeDef *DACx)
{
    SET_BIT(DACx->SWTRIG, DAC_SWTRIG_CTRL);
}


/********dac_data_pointer*******
 * @retval None
 */


/**
 * @brief  Set DACDataPointer:MCU can modify the DAC data pointr while DAC module disable or DAC work as trig mode.
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll DPTR          data_pointer      LL_DAC_SetDACDataPointer
 * @param  DAC DAC Instance
 * @param  DACDataPointer This parameter can be one of the following values:
 *         @arg @ref LL_DAC_DATAPOINTER_x  (x=0~7)
 * @retval None
 */
__STATIC_INLINE void LL_DAC_SetDataPointer(DAC_TypeDef *DACx, uint32_t data_pointer)
{
    MODIFY_REG(DACx->DPTR, DAC_DPTR_DPTR, data_pointer);
}


/**
 * @brief  Get DACDataPointer
 * @rmtoll DPTR          data_pointer          LL_DAC_GetDACDataPointer
 * @param  DAC DAC Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_DAC_DATAPOINTER_x  (x=0~7)
 */
__STATIC_INLINE uint32_t LL_DAC_GetDataPointer(DAC_TypeDef *DACx)
{
    return ((uint32_t)(READ_BIT(DACx->DPTR, DAC_DPTR_DPTR)));
}


/********dac_dp_cfg*******
 * @retval None
 */


/**
 * @brief  Set TopPointer:.
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll DPCFG          top_pointer      LL_DAC_SetTopPointer
 * @param  DAC DAC Instance
 * @param  top_pointer This parameter can be one of the following values:
 *         @arg @ref LL_DAC_DATAPOINTER_x  (x=0~7)
 * @retval None
 */
__STATIC_INLINE void LL_DAC_SetTopPointer(DAC_TypeDef *DACx, uint32_t top_pointer)
{
    MODIFY_REG(DACx->DPCFG, DAC_DPCFG_TOP, top_pointer);
}


/**
 * @brief  Get TopPointer
 * @rmtoll DPCFG          top_pointer          LL_DAC_GetTopPointer
 * @param  DAC DAC Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_DAC_TOPPOINTER_x  (x=0~7)
 */
__STATIC_INLINE uint32_t LL_DAC_GetTopPointer(DAC_TypeDef *DACx)
{
    return ((uint32_t)(READ_BIT(DACx->DPCFG, DAC_DPCFG_TOP)));
}


/**
 * @brief  Set WatermarkPointer:.
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll DPCFG          Watermark_Pointer      LL_DAC_SetWatermarkPointer
 * @param  DAC DAC Instance
 * @param  Watermark_Pointer This parameter can be one of the following values:
 *         @arg @ref LL_DAC_WATERMARKPOINTER_x  (x=0~7)
 * @retval None
 */
__STATIC_INLINE void LL_DAC_SetWatermarkPointer(DAC_TypeDef *DACx, uint32_t Watermark_Pointer)
{
    MODIFY_REG(DACx->DPCFG, DAC_DPCFG_WMK, Watermark_Pointer);
}


/**
 * @brief  Get WatermarkPointer
 * @rmtoll DPCFG          Watermark_Pointer          LL_DAC_GetWatermarkPointer
 * @param  DAC DAC Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_DAC_WATERMARKPOINTER_x  (x=0~7)
 */
__STATIC_INLINE uint32_t LL_DAC_GetWatermarkPointer(DAC_TypeDef *DACx)
{
    return ((uint32_t)(READ_BIT(DACx->DPCFG, DAC_DPCFG_WMK)));
}


/********dac_data0~7*******
 * @retval None
 */


/**
 * @brief  Set DataN:.
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll DATA[N]          DataN      LL_DAC_SetData
 * @param  DAC DAC Instance
 * @param  DataN This parameter can be one of the following values:
 *         @arg @ref  Value=[11:0]
 * @retval None
 */
__STATIC_INLINE void LL_DAC_SetData(DAC_TypeDef *DACx, uint8_t N, uint32_t Value)         //N:0~7
{
    MODIFY_REG(DACx->DATA[N], DAC_DATAN_DATA, Value);
}


/**
 * @brief  Get DataN
 * @rmtoll DATA[N]          DataN          LL_DAC_GetData
 * @param  DAC DAC Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref Value=[11:0]
 */
__STATIC_INLINE uint32_t LL_DAC_GetData(DAC_TypeDef *DACx, uint8_t N)
{
    return ((uint32_t)(READ_BIT(DACx->DATA[N], DAC_DATAN_DATA)));
}


/********dac_flag*******
 * @retval None
 */


/**
 * @brief  Get BottomFlag
 * @rmtoll FLAG          bottom_flag          LL_DAC_GetBottomFlag
 * @param  DAC DAC Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref 0 or !0
 */
__STATIC_INLINE uint32_t LL_DAC_IsActiveFlag_Bottom(DAC_TypeDef *DACx)
{
    return ((uint32_t)(READ_BIT(DACx->FLAG, DAC_FLAG_BOTTOMF) == DAC_FLAG_BOTTOMF));
}


/**
 * @brief  Get TopFlag
 * @rmtoll FLAG          top_flag          LL_DAC_IsActiveTopFlag
 * @param  DAC DAC Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref 0 or !0
 */
__STATIC_INLINE uint32_t LL_DAC_IsActiveFlag_Top(DAC_TypeDef *DACx)
{
    return ((uint32_t)(READ_BIT(DACx->FLAG, DAC_FLAG_TOPF) == DAC_FLAG_TOPF));
}


/**
 * @brief  Get WatermarkFlag
 * @rmtoll FLAG          watermark_flag          LL_DAC_IsActiveWatermarkFlag
 * @param  DAC DAC Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref 0 or !0
 */
__STATIC_INLINE uint32_t LL_DAC_IsActiveFlag_Watermark(DAC_TypeDef *DACx)
{
    return ((uint32_t)(READ_BIT(DACx->FLAG, DAC_FLAG_WMKF) == DAC_FLAG_WMKF));
}


/********dac_int_en*******
 * @retval None
 */


/**
 * @brief  Set BottomIntDmaEn:.
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll INTEN          bottom_int_dma_en      LL_DAC_EnableBottomINTorDMA
 * @param  DAC DAC Instance
 * @param  Cfg This parameter can be one of the following values:
 *         @arg @ref LL_DAC_DMA_BOTTOM
 *         @arg @ref LL_DAC_INT_BOTTOM
 * @retval None
 */
__STATIC_INLINE void LL_DAC_EnableBottomINTorDMA(DAC_TypeDef *DACx, uint32_t Cfg)
{
    MODIFY_REG(DACx->INTEN, DAC_INTEN_BTMIDE | DAC_INTEN_BTMDMAS, Cfg);
}


/**
 * @brief  Set BottomIntDmaEn:.
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll INTEN          bottom_int_dma_en      LL_DAC_DisableBottomINTandDMA
 * @param  DAC DAC Instance
 * @retval None
 */
__STATIC_INLINE void LL_DAC_DisableBottomINTandDMA(DAC_TypeDef *DACx)
{
    CLEAR_BIT(DACx->INTEN, DAC_INTEN_BTMIDE | DAC_INTEN_BTMDMAS);
}


/**
 * @brief  Get BottomIntDmaEn
 * @rmtoll INTEN          bottom_int_dma_en          LL_DAC_IsEnableBottomINTorDMA
 * @param  DAC DAC Instance
 * @param  Cfg This parameter can be one of the following values:
 *         @arg @ref LL_DAC_DMA_BOTTOM
 *         @arg @ref LL_DAC_INT_BOTTOM
 * @retval 0 or !0
 */
__STATIC_INLINE uint32_t LL_DAC_IsEnableBottomINTorDMA(DAC_TypeDef *DACx, uint32_t Cfg)
{
    return ((uint32_t)(READ_BIT(DACx->INTEN, DAC_INTEN_BTMIDE | DAC_INTEN_BTMDMAS) == Cfg));
}


/**
 * @brief  Set TopIntDmaEn:
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll INTEN          top_int_dma_en      LL_DAC_EnableTopINTorDMA
 * @param  DAC DAC Instance
 * @param  Cfg This parameter can be one of the following values:
 *         @arg @ref LL_DAC_DMA_TOP
 *         @arg @ref LL_DAC_INT_TOP
 * @retval None
 */
__STATIC_INLINE void LL_DAC_EnableTopINTorDMA(DAC_TypeDef *DACx, uint32_t Cfg)
{
    MODIFY_REG(DACx->INTEN, DAC_INTEN_TOPIDE | DAC_INTEN_TOPDMAS, Cfg);
}


/**
 * @brief  Set TopIntDmaEn:
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll INTEN          top_int_dma_en      LL_DAC_DisableTopINTandDMA
 * @param  DAC DAC Instance
 * @retval None
 */
__STATIC_INLINE void LL_DAC_DisableTopINTandDMA(DAC_TypeDef *DACx)
{
    CLEAR_BIT(DACx->INTEN, DAC_INTEN_TOPIDE | DAC_INTEN_TOPDMAS);
}


/**
 * @brief  Get TopIntDmaEn
 * @rmtoll INTEN          top_int_dma_en          LL_DAC_IsEnableTopINTorDMA
 * @param  DAC DAC Instance
 * @param  Cfg This parameter can be one of the following values:
 *         @arg @ref LL_DAC_DMA_TOP
 *         @arg @ref LL_DAC_INT_TOP
 * @retval 0 or !0
 */
__STATIC_INLINE uint32_t LL_DAC_IsEnableTopINTorDMA(DAC_TypeDef *DACx, uint32_t Cfg)
{
    return ((uint32_t)(READ_BIT(DACx->INTEN, DAC_INTEN_TOPIDE | DAC_INTEN_TOPDMAS) == Cfg));
}


/**
 * @brief  Set WatermarkIntDmaEn:.
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll INTEN          watermark_int_dma_en      LL_DAC_EnableWatermarkINTorDMA
 * @param  DAC DAC Instance
 * @param  Cfg This parameter can be one of the following values:
 *         @arg @ref LL_DAC_DMA_WATERMARK
 *         @arg @ref LL_DAC_INT_WATERMARK
 * @retval None
 */
__STATIC_INLINE void LL_DAC_EnableWatermarkINTorDMA(DAC_TypeDef *DACx, uint32_t Cfg)
{
    MODIFY_REG(DACx->INTEN, DAC_INTEN_WMKIDE | DAC_INTEN_WMKDMAS, Cfg);
}


/**
 * @brief  Set WatermarkIntDmaEn:.
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll INTEN          watermark_int_dma_en      LL_DAC_DisableWatermarkINTandDMA
 * @param  DAC DAC Instance
 * @retval None
 */
__STATIC_INLINE void LL_DAC_DisableWatermarkINTandDMA(DAC_TypeDef *DACx)
{
    CLEAR_BIT(DACx->INTEN, DAC_INTEN_WMKIDE | DAC_INTEN_WMKDMAS);
}


/**
 * @brief  Get WatermarkIntDmaEn
 * @rmtoll INTEN          watermark_int_dma_en          LL_DAC_IsEnableWatermarkINTorDMA
 * @param  DAC DAC Instance
 * @param  Cfg This parameter can be one of the following values:
 *         @arg @ref LL_DAC_DMA_WATERMARK
 *         @arg @ref LL_DAC_INT_WATERMARK
 * @retval 0 or !0
 */
__STATIC_INLINE uint32_t LL_DAC_IsEnableWatermarkINTorDMA(DAC_TypeDef *DACx, uint32_t Cfg)
{
    return ((uint32_t)(READ_BIT(DACx->INTEN, DAC_INTEN_WMKIDE | DAC_INTEN_WMKDMAS) == Cfg));
}


/********dac_int_clear*******
 * @retval None
 */


/**
 * @brief  Set BottomIntClr:clear bottom int & bottom flag bit.
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll IFCL          bottom_icl      LL_DAC_ClearBottomINTFlag
 * @param  DAC DAC Instance
 * @retval None
 */
__STATIC_INLINE void LL_DAC_ClearFlagBottom_IT(DAC_TypeDef *DACx)
{
    SET_BIT(DACx->IFCL, DAC_IFCL_BTMICL);
}


/**
 * @brief  Set TopIntClr:clear top int & top flag bit.
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll IFCL          top_icl      LL_DAC_ClearTopINTFlag
 * @param  DAC DAC Instance
 * @retval None
 */
__STATIC_INLINE void LL_DAC_ClearFlagTop_IT(DAC_TypeDef *DACx)
{
    SET_BIT(DACx->IFCL, DAC_IFCL_TOPICL);
}


/**
 * @brief  Set WatermarkIntClr:clear watermark int & water flag bit.
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll IFCL          watermark_icl      LL_DAC_ClearWatermarkINTFlag
 * @param  DAC DAC Instance
 * @retval None
 */
__STATIC_INLINE void LL_DAC_ClearFlagWatermark_IT(DAC_TypeDef *DACx)
{
    SET_BIT(DACx->IFCL, DAC_IFCL_WMKICL);
}


/**
 * @brief  Clear all int flag
 * @note   This bit should not be changed when communication is ongoing.
 * @rmtoll IFCL          watermark_icl      LL_DAC_ClearWatermarkINTFlag
 * @param  DAC DAC Instance
 * @retval None
 */
__STATIC_INLINE void LL_DAC_ClearFlagAll_IT(DAC_TypeDef *DACx)
{
    SET_BIT(DACx->IFCL, 0x7);
}


void LL_DAC_StructInit(LL_DAC_InitTypeDef *DAC_InitStruct);


void LL_DAC_Init(DAC_TypeDef *DACx, LL_DAC_InitTypeDef *DAC_InitStruct);


#endif /* defined (DAC0) */


#ifdef __cplusplus
}
#endif
#endif
/************************ (C) COPYRIGHT FudanMicroelectronics *****END OF FILE****/

