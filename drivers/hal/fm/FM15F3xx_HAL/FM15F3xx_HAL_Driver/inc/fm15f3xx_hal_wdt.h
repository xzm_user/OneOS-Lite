/**
 ******************************************************************************
 * @file    fm15f3xx_hal_wdt.h
 * @author  MCD Application Team
 * @brief   Header file of WDT HAL module.
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; COPYRIGHT(c) 2020 FudanMicroelectronics</center></h2>
 *
 ******************************************************************************
 */


/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __FM15F3xx_HAL_WDT_H
#define __FM15F3xx_HAL_WDT_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "fm15f3xx_hal_def.h"


/** @addtogroup FM15F3xx_HAL_Driver
 * @{
 */


/** @addtogroup WDT
 * @{
 */

/* Exported types ------------------------------------------------------------*/


/** @defgroup WDT_Exported_Types WDT Exported Types
 * @{
 */


/**
 * @brief  WDT Init structure definition
 */
typedef struct {
    uint32_t Period;        /*!< Select the Period Counter of the WDT.
                                 This parameter can be a value of @ref WDT_Period */
} WDT_InitTypeDef;


/**
 * @brief  WDT Handle Structure definition
 */
typedef struct {
    WDT_TypeDef *Instance;  /*!< Register base address    */

    WDT_InitTypeDef Init;   /*!< WDT required parameters */
} WDT_HandleTypeDef;


/**
 * @}
 */

/* Exported constants --------------------------------------------------------*/


/** @defgroup WDT_Exported_Constants WDT Exported Constants
 * @{
 */


/** @defgroup WDT_Period WDT Period
 * @{
 */
#define WDT_PERIOD_8    (0x0U << WDT_CFG_PERIOD_Pos)
#define WDT_PERIOD_12   (0x1U << WDT_CFG_PERIOD_Pos)
#define WDT_PERIOD_15   (0x2U << WDT_CFG_PERIOD_Pos)
#define WDT_PERIOD_18   (0x3U << WDT_CFG_PERIOD_Pos)
#define WDT_PERIOD_21   (0x4U << WDT_CFG_PERIOD_Pos)
#define WDT_PERIOD_24   (0x5U << WDT_CFG_PERIOD_Pos)
#define WDT_PERIOD_27   (0x6U << WDT_CFG_PERIOD_Pos)
#define WDT_PERIOD_30   (0x7U << WDT_CFG_PERIOD_Pos)


/**
 * @}
 */


/**
 * @brief  WDT WRITE ACCESS
 */
#define WDT_WRITE_ACCESS_CMD0   0xAAAAAAAAU     /*!< WDT Write Access Cmd0  */
#define WDT_WRITE_ACCESS_CMD1   0x55555555U     /*!< WDT Write Access Cmd1  */


/**
 * @}
 */


/**
 * @}
 */

/* Exported macros -----------------------------------------------------------*/


/** @defgroup WDT_Exported_Macros WDT Exported Macros
 * @{
 */


/**
 * @brief  Enable the WDT peripheral.
 * @param  __HANDLE__  WDT handle
 * @retval None
 */
#define WDT_ENABLE_WRITE( __HANDLE__ ) WRITE_REG( (__HANDLE__)->Instance->WRCTRL, WDT_WRITE_ACCESS_CMD0 ); \
    WRITE_REG( (__HANDLE__)->Instance->WRCTRL, WDT_WRITE_ACCESS_CMD1 )


/**
 * @brief  Enable the WDT peripheral.
 * @param  __HANDLE__  WDT handle
 * @retval None
 */
#define __HAL_WDT_START( __HANDLE__ ) WDT_ENABLE_WRITE( __HANDLE__ ); \
    SET_BIT( (__HANDLE__)->Instance->CFG, WDT_CFG_EN )


/**
 * @brief  Reset Counter Reset registers.
 * @param  __HANDLE__  WDT handle
 * @retval None
 */
#define __HAL_WDT_CNT_RESET( __HANDLE__ ) WDT_ENABLE_WRITE( __HANDLE__ ); \
    SET_BIT( (__HANDLE__)->Instance->CFG, WDT_CFG_CNTRST )


/**
 * @brief  Reset Counter Reset registers.
 * @param  __HANDLE__  WDT handle
 * @retval None
 */
#define __HAL_WDT_SET_PERIOD( __HANDLE__, __PERIOD__ ) WRITE_REG( (__HANDLE__)->Instance->WRCTRL, WDT_WRITE_ACCESS_CMD0 ); \
    WRITE_REG( (__HANDLE__)->Instance->WRCTRL, WDT_WRITE_ACCESS_CMD1 ); \
    MODIFY_REG( (__HANDLE__)->Instance->CFG, WDT_CFG_PERIOD, __PERIOD__ )


/**
 * @}
 */

/* Exported functions --------------------------------------------------------*/


/** @defgroup WDT_Exported_Functions  WDT Exported Functions
 * @{
 */


/** @defgroup WDT_Exported_Functions_Group1 Initialization and Start functions
 * @{
 */
/* Initialization/Start functions  ********************************************/
HAL_StatusTypeDef HAL_WDT_Init(WDT_HandleTypeDef *hiwdg);


/**
 * @}
 */


/** @defgroup WDT_Exported_Functions_Group2 IO operation functions
 * @{
 */
/* I/O operation functions ****************************************************/
HAL_StatusTypeDef HAL_WDT_Start(WDT_HandleTypeDef *hwdt);


HAL_StatusTypeDef HAL_WDT_Refresh(WDT_HandleTypeDef *hwdt);


uint32_t HAL_WDT_GetCountValue(WDT_HandleTypeDef *hwdt);


/**
 * @}
 */


/**
 * @}
 */

/* Private constants ---------------------------------------------------------*/


/** @defgroup WDT_Private_Constants WDT Private Constants
 * @{
 */


/* Private macros ------------------------------------------------------------*/


/** @defgroup WDT_Private_Macros WDT Private Macros
 * @{
 */


/**
 * @brief  Check WDT prescaler value.
 * @param  __PRESCALER__  WDT prescaler value
 * @retval None
 */
#define IS_WDT_INSTANCE( INSTANCE ) ( (INSTANCE) == WDT0)

#define IS_WDT_PERIOD( PERIOD ) ( ( (PERIOD) == WDT_PERIOD_8) || \
                                  ( (PERIOD) == WDT_PERIOD_12) || \
                                  ( (PERIOD) == WDT_PERIOD_15) || \
                                  ( (PERIOD) == WDT_PERIOD_18) || \
                                  ( (PERIOD) == WDT_PERIOD_21) || \
                                  ( (PERIOD) == WDT_PERIOD_24) || \
                                  ( (PERIOD) == WDT_PERIOD_27) || \
                                  ( (PERIOD) == WDT_PERIOD_30) )


/**
 * @}
 */


/**
 * @}
 */


#ifdef __cplusplus
}
#endif

#endif /* __FM15F3xx_HAL_WDT_H */

/************************ (C) COPYRIGHT FudanMicroelectronics *****END OF FILE****/
