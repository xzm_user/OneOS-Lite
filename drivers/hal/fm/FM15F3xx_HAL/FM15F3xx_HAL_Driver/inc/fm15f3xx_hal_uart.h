/**
 ******************************************************************************
 * @file    fm15f3xx_hal_uart.h
 * @author  WYL
 * @brief   Header file of UART HAL module.
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; COPYRIGHT(c) 2020 FudanMicroelectronics</center></h2>
 *
 ******************************************************************************
 */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __FM15F3xx_HAL_UART_H
#define __FM15F3xx_HAL_UART_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "fm15f3xx_hal_def.h"
#include "fm15f3xx_ll_uart.h"
#include "fm15f3xx_hal_dma.h"

/** @addtogroup FM15F3xx_HAL_Driver
 * @{
 */


/** @addtogroup UART
 * @{
 */

/* Exported types ------------------------------------------------------------*/


/** @defgroup UART_Exported_Types UART Exported Types
 * @{
 */


/**
 * @brief UART Init Structure definition
 */
typedef struct {
    uint32_t BaudRate;                      /*!< This field defines expected Uart communication baud rate.*/

    uint32_t WordLength;                    /*!< Specifies the number of data bits transmitted or received in a frame. */

    uint32_t StopBits;                      /*!< Specifies the number of stop bits transmitted.*/

    uint32_t TransferMode;                  /*!< Specifies whether the Receive and/or Transmit mode is enabled or disabled.*/
} UART_InitTypeDef;


/**
 * @brief HAL UART State structures definition
 * @note  HAL UART State value is a combination of 2 different substates: gState and RxState.
 */
typedef enum {
    HAL_UART_STATE_RESET        = 0x00U,    /*!< Peripheral is not yet Initialized
                                                Value is allowed for gState and RxState */
    HAL_UART_STATE_READY        = 0x20U,    /*!< Peripheral Initialized and ready for use
                                                Value is allowed for gState and RxState */
    HAL_UART_STATE_BUSY         = 0x24U,    /*!< an internal process is ongoing
                                                Value is allowed for gState only */
    HAL_UART_STATE_BUSY_TX      = 0x21U,    /*!< Data Transmission process is ongoing
                                                Value is allowed for gState only */
    HAL_UART_STATE_BUSY_RX      = 0x22U,    /*!< Data Reception process is ongoing
                                                Value is allowed for RxState only */
    HAL_UART_STATE_BUSY_TX_RX   = 0x23U,    /*!< Data Transmission and Reception process is ongoing
                                                Not to be used for neither gState nor RxState.
                                                Value is result of combination (Or) between gState and RxState values */
    HAL_UART_STATE_TIMEOUT      = 0xA0U,    /*!< Timeout state
                                                Value is allowed for gState only */
    HAL_UART_STATE_ERROR        = 0xE0U     /*!< Error
                                                Value is allowed for gState only */
} HAL_UART_StateTypeDef;


/**
 * @brief  UART handle Structure definition
 */
typedef struct {
    UART_TypeDef *Instance;                 /*!< UART registers base address        */

    UART_InitTypeDef Init;                  /*!< UART communication parameters      */

    uint8_t *pTxBuffPtr;                    /*!< Pointer to UART Tx transfer Buffer */

    uint16_t TxXferSize;                    /*!< UART Tx Transfer size              */

    __IO uint16_t TxXferCount;              /*!< UART Tx Transfer Counter           */

    uint8_t *pRxBuffPtr;                    /*!< Pointer to UART Rx transfer Buffer */

    uint16_t RxXferSize;                    /*!< UART Rx Transfer size              */

    __IO uint16_t RxXferCount;              /*!< UART Rx Transfer Counter           */

    DMA_HandleTypeDef             *hdmatx;          /*!< UART Tx DMA Handle parameters      */

    DMA_HandleTypeDef             *hdmarx;          /*!< UART Rx DMA Handle parameters      */

    __IO HAL_UART_StateTypeDef gState;      /*!< UART state information related to global Handle management
                                                 and also related to Tx operations.
                                                 This parameter can be a value of @ref HAL_UART_StateTypeDef */

    __IO HAL_UART_StateTypeDef RxState;     /*!< UART state information related to Rx operations.
                                                 This parameter can be a value of @ref HAL_UART_StateTypeDef */

    __IO uint32_t ErrorCode;                /*!< UART Error code                    */
} UART_HandleTypeDef;


/**
 * @}
 */

/* Exported constants --------------------------------------------------------*/


/** @defgroup UART_Exported_Constants UART Exported constants
 * @{
 */


/** @defgroup UART_Error_Code UART Error Code
 * @brief    UART Error Code
 * @{
 */
#define HAL_UART_ERROR_NONE 0x00000000U             /*!< No error            */
#define HAL_UART_ERROR_PE   0x00000001U             /*!< Parity error        */
#define HAL_UART_ERROR_NE   0x00000002U             /*!< Noise error         */
#define HAL_UART_ERROR_FE   0x00000004U             /*!< Frame error         */
#define HAL_UART_ERROR_ORE  0x00000008U             /*!< Overrun error       */
#define HAL_UART_ERROR_DMA  0x00000010U             /*!< DMA transfer error  */


/**
 * @}
 */


/** @defgroup UART_Word_Length UART Word Length
 * @{
 */
#define UART_WORDLENGTH_8B_NONE LL_UART_PDSEL_N_8
#define UART_WORDLENGTH_8B_EVEN LL_UART_PDSEL_E_8
#define UART_WORDLENGTH_8B_ODD  LL_UART_PDSEL_O_8
#define UART_WORDLENGTH_9B_NONE LL_UART_PDSEL_N_9


/**
 * @}
 */


/** @defgroup UART_Stop_Bits UART Number of Stop Bits
 * @{
 */
#define UART_STOPBITS_1 0x00000000U
#define UART_STOPBITS_2 ( (uint32_t) UART_CR_STOPSEL)


/**
 * @}
 */


/** @defgroup UART_Mode UART Transfer Mode
 * @{
 */
#define UART_TRANSFER_RX    ( (uint32_t) UART_CR_RXEN)
#define UART_TRANSFER_TX    ( (uint32_t) UART_CR_TXEN)
#define UART_TRANSFER_TX_RX ( (uint32_t) (UART_CR_RXEN | UART_CR_TXEN) )


/**
 * @}
 */


/** @defgroup UART_Sample_Factor
 * @{
 */
#define UART_SampleFactor_64    (0x00000000U)                   /*!<Low Bradurate factor*/
#define UART_SampleFactor_16    (0x1U << UART_SPEED_BRGH_Pos)   /*!<Normal Bradurate factor*/
#define UART_SampleFactor_4     (0x2U << UART_SPEED_BRGH_Pos)   /*!<High Bradurate factor*/


/**
 * @}
 */


/** @defgroup UART_Flags   UART FLags
 * @{
 */
#define UART_FLAG_TC        ( (uint32_t) UART_STATUS_TXDONE)
#define UART_FLAG_RXBUSY    ( (uint32_t) UART_STATUS_RXBUSY)
#define UART_FLAG_TXE       ( (uint32_t) UART_STATUS_TXFIFOE)
#define UART_FLAG_TXHE      ( (uint32_t) UART_STATUS_TXFIFOHE)
#define UART_FLAG_RXNE      ( (uint32_t) UART_STATUS_RXFIFONE)
#define UART_FLAG_RXHF      ( (uint32_t) UART_STATUS_RXFIFOHF)
#define UART_FLAG_TXOE      ( (uint32_t) UART_STATUS_TXFOERR)
#define UART_FLAG_RXOE      ( (uint32_t) UART_STATUS_RXFOERR)
#define UART_FLAG_FE        ( (uint32_t) UART_STATUS_FERR)
#define UART_FLAG_PE        ( (uint32_t) UART_STATUS_PERR)
#define UART_FLAG_TXFIFO    ( (uint32_t) UART_STATUS_TXFIFOS)
#define UART_FLAG_RXFIFO    ( (uint32_t) UART_STATUS_RXFIFOS)


/**
 * @}
 */


/** @defgroup UART_Interrupt_definition  UART Interrupt Definitions
 * @{
 */
#define UART_IT_RXNE        (uint32_t) (UART_INTCFG_RXFNEIE)
#define UART_IT_RXHF        (uint32_t) (UART_INTCFG_RXFHFIE)
#define UART_IT_TXE         (uint32_t) (UART_INTCFG_TXFEIE)
#define UART_IT_TXHE        (uint32_t) (UART_INTCFG_TXFHEIE)
#define UART_IT_TC          (uint32_t) (UART_INTCFG_TXDONEIE)
#define UART_IT_ERR         (uint32_t) (UART_INTCFG_ERRIE)
#define UART_IT_RXNE_DMA    (uint32_t) (UART_INTCFG_RXFNEDE)
#define UART_IT_TXE_DMA     (uint32_t) (UART_INTCFG_TXFEDE)


/**
 * @}
 */


/**
 * @}
 */

/* Exported macro ------------------------------------------------------------*/


/** @defgroup UART_Exported_Macros UART Exported Macros
 * @{
 */


/** @brief Reset UART handle gstate & RxState
 * @param  __HANDLE__ specifies the UART Handle.
 * @retval None
 */
#define __HAL_UART_RESET_HANDLE_STATE( __HANDLE__ ) do {                                                   \
        (__HANDLE__)->gState    = HAL_UART_STATE_RESET;      \
        (__HANDLE__)->RxState   = HAL_UART_STATE_RESET;     \
} while ( 0U )

/** @brief  Checks whether the specified UART flag is set or not.
 * @param  __HANDLE__ specifies the UART Handle.
 * @param  __FLAG__ specifies the flag to check.
 * @retval The new state of __FLAG__ (TRUE or FALSE).
 */
#define __HAL_UART_GET_RXFIFO_LEN( __HANDLE__ ) ( ( (__HANDLE__)->Instance->STATUS & UART_STATUS_RXFIFOS ) >> UART_STATUS_RXFIFOS_Pos)


/** @brief  Checks whether the specified UART flag is set or not.
 * @param  __HANDLE__ specifies the UART Handle.
 * @param  __FLAG__ specifies the flag to check.
 * @retval The new state of __FLAG__ (TRUE or FALSE).
 */
#define __HAL_UART_GET_FLAG( __HANDLE__, __FLAG__ ) ( ( (__HANDLE__)->Instance->STATUS & (__FLAG__) ) == (__FLAG__) )


/** @brief  Clears the specified UART pending flag.
 * @param  __HANDLE__ specifies the UART Handle.
 * @param  __FLAG__ specifies the flag to check.
 * @retval None
 */
#define __HAL_UART_CLEAR_FLAG( __HANDLE__, __FLAG__ ) ( (__HANDLE__)->Instance->STATUS &= ~(__FLAG__) )


/** @brief  Enable the specified UART interrupt.
 * @param  __HANDLE__ specifies the UART Handle.
 * @param  __INTERRUPT__ specifies the UART interrupt source to enable.
 * @retval None
 */
#define UART_IT_MASK 0x000000FFU
#define __HAL_UART_ENABLE_IT( __HANDLE__, __INTERRUPT__ ) ( (__HANDLE__)->Instance->INTCFG |= ( (__INTERRUPT__) &UART_IT_MASK) )

/** @brief  Disable the specified UART interrupt.
 * @param  __HANDLE__ specifies the UART Handle.
 * @param  __INTERRUPT__ specifies the UART interrupt source to disable.  * @retval None
 */
#define __HAL_UART_DISABLE_IT( __HANDLE__, __INTERRUPT__ ) ( (__HANDLE__)->Instance->INTCFG &= ~( (__INTERRUPT__) &UART_IT_MASK) )


/** @brief  Enable the specified UART DMA.
 * @param  __HANDLE__ specifies the UART Handle.
 * @param  __DMA__ specifies the UART DMA source to enable.
 * @retval None
 */
#define UART_DMA_MASK 0x000000C0U
#define __HAL_UART_ENABLE_DMA( __HANDLE__, __DMA__ ) ( (__HANDLE__)->Instance->INTCFG |= ( (__DMA__) &UART_DMA_MASK) )

/** @brief  Disable the specified UART DMA.
 * @param  __HANDLE__ specifies the UART Handle.
 * @param  __INTERRUPT__ specifies the UART DMA source to disable.
 * @retval None
 */
#define __HAL_UART_DISABLE_DMA( __HANDLE__, __DMA__ ) ( (__HANDLE__)->Instance->INTCFG &= ~( (__DMA__) &UART_DMA_MASK) )


/* Exported functions --------------------------------------------------------*/


/** @addtogroup UART_Exported_Functions
 * @{
 */


/** @addtogroup UART_Exported_Functions_Group1
 * @{
 */
/* Initialization/de-initialization functions  **********************************/
HAL_StatusTypeDef HAL_UART_Init(UART_HandleTypeDef *huart);


HAL_StatusTypeDef HAL_UART_DeInit(UART_HandleTypeDef *huart);


void HAL_UART_MspInit(UART_HandleTypeDef *huart);


void HAL_UART_MspDeInit(UART_HandleTypeDef *huart);


/**
 * @}
 */


/** @addtogroup UART_Exported_Functions_Group2
 * @{
 */
/* IO operation functions *******************************************************/
HAL_StatusTypeDef HAL_UART_Transmit(UART_HandleTypeDef *huart, uint8_t *pData, uint16_t Size, uint32_t Timeout);


HAL_StatusTypeDef HAL_UART_Receive(UART_HandleTypeDef *huart, uint8_t *pData, uint16_t Size, uint32_t Timeout);


HAL_StatusTypeDef HAL_UART_Transmit_IT(UART_HandleTypeDef *huart, uint8_t *pData, uint16_t Size);


HAL_StatusTypeDef HAL_UART_Receive_IT(UART_HandleTypeDef *huart, uint8_t *pData, uint16_t Size);


HAL_StatusTypeDef HAL_UART_Transmit_DMA(UART_HandleTypeDef *huart, uint8_t *pData, uint16_t Size);


HAL_StatusTypeDef HAL_UART_Receive_DMA(UART_HandleTypeDef *huart, uint8_t *pData, uint16_t Size);


void HAL_UART_IRQHandler(UART_HandleTypeDef *huart);



void HAL_UART_TxCpltCallback(UART_HandleTypeDef *huart);


void HAL_UART_TxHalfCpltCallback(UART_HandleTypeDef *huart);


void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart);


void HAL_UART_RxHalfCpltCallback(UART_HandleTypeDef *huart);


void HAL_UART_ErrorCallback(UART_HandleTypeDef *huart);


/**
 * @}
 */


/** @addtogroup UART_Exported_Functions_Group3
 * @{
 */
/* Peripheral State functions  **************************************************/
HAL_UART_StateTypeDef HAL_UART_GetState(UART_HandleTypeDef *huart);


uint32_t HAL_UART_GetError(UART_HandleTypeDef *huart);


/**
 * @}
 */


/**
 * @}
 */
/* Private types -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private constants ---------------------------------------------------------*/
/* Private macros ------------------------------------------------------------*/


/** @defgroup UART_Private_Macros UART Private Macros
 * @{
 */
#define IS_UART_INSTANCE( INSTANCE )    ( ( (INSTANCE) == UART0) || \
                                          ( (INSTANCE) == UART1) || \
                                          ( (INSTANCE) == UART2) )
#define IS_UART_WORD_LENGTH( LENGTH )   ( ( (LENGTH) == UART_WORDLENGTH_8B_NONE) || \
                                          ( (LENGTH) == UART_WORDLENGTH_8B_EVEN) || \
                                          ( (LENGTH) == UART_WORDLENGTH_8B_ODD) || \
                                          ( (LENGTH) == UART_WORDLENGTH_9B_NONE) )
#define IS_UART_STOPBITS( STOPBITS )    ( ( (STOPBITS) == UART_STOPBITS_1) || \
                                          ( (STOPBITS) == UART_STOPBITS_2) )
#define IS_UART_TRANSFER_MODE( MODE )   ( ( (MODE) == UART_TRANSFER_RX) || \
                                          ( (MODE) == UART_TRANSFER_TX) || \
                                          ( (MODE) == UART_TRANSFER_TX_RX) )
#define IS_UART_BAUDRATE( BAUDRATE )    ( (BAUDRATE) < 3000001U)


/**
 * @}
 */

/* Private functions ---------------------------------------------------------*/


/**
 * @}
 */


/**
 * @}
 */

#ifdef __cplusplus
}
#endif

#endif /* __FM15F3xx_HAL_UART_H */

/************************ (C) COPYRIGHT FudanMicroelectronics *****END OF FILE****/
