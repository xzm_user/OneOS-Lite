/**
 ******************************************************************************
 * @file    fm15f3xx_ll_systick.h
 * @author  SRG
 * @version V1.0.0
 * @date    2020-04-21
 * @brief
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2020 FudanMicroelectronics.
 * All rights reserved.</center></h2>
 *
 ******************************************************************************
 */
#ifndef __FM15F3XX_LL_SYSTICK_H_
#define __FM15F3XX_LL_SYSTICK_H_


#ifdef __cplusplus
extern "C" {
#endif

#include "fm15f3xx.h"

typedef struct {
    uint32_t    OscValueHz;
    uint32_t    TickClkSrcSel;                                  /*@ref SYSTICK_CLKSRC*/
    uint32_t    TickRefClkSel;                                  /*@ref CMU_CLK SYSTICKREF Source*/
    uint32_t    TickRefClkDiv;                                  /*@ref CMU_LL_EC_REUSE2DIV*/
} LL_SYSTICK_InitTypeDef;


/** @defgroup SYSTICK_CLKSRC
 * @{
 */
#define LL_SYSTICKCLK_REFCLK    (0U)                            /*systick ref clk*/
#define LL_SYSTICKCLK_SYSCLK    (SysTick_CTRL_CLKSOURCE_Msk)    /*sys clk*/


/**
 * @}
 */


ErrorStatus LL_SYSTICK_Init(LL_SYSTICK_InitTypeDef *InitStruct);


void init_systick(void);


void systick_delay_us(uint32_t us);


void systick_delay_ms(uint32_t ms);


#ifdef __cplusplus
}
#endif
#endif

/************************ (C) COPYRIGHT FudanMicroelectronics *****END OF FILE****/
