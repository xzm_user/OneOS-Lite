/**
 ******************************************************************************
 * @file    fm15f3xx_ll_fsmc.h
 * @author  WYL
 * @version V1.0.0
 * @date    2020-03-17
 * @brief
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2020 FudanMicroelectronics.
 * All rights reserved.</center></h2>
 *
 ******************************************************************************
 */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __FM15F3XX_LL_FSMC_H
#define __FM15F3XX_LL_FSMC_H

#ifdef __cplusplus
extern "C" {
#endif

#include "fm15f3xx.h"


/** @addtogroup FM15F3XX_LL_Driver
 * @{
 */

#if defined (FSMC)


/** @defgroup FSMC_LL FSMC
 * @{
 */

typedef struct {
    uint32_t AccessMode;                    /*!< Select MMAP or EXTM                                                         */

    uint32_t MemoryWidth;                   /*!< Specifies the external memory device width.
                                                 This parameter can be a value of @ref FSMC_NORSRAM_Data_Width               */

    uint32_t NWaitEn;                       /*!< Enables or disables the burst access mode for Flash memory,
                                                 valid only with synchronous burst Flash memories.
                                             */
    uint32_t NWaitPol;                      /*!< Specifies the wait signal polarity, valid only when accessing
                                                 the Flash memory in burst mode.
                                                 This parameter can be a value of @ref FSMC_Wait_Signal_Polarity             */
    uint32_t ClkEn;                         /*!< FSMC CLK output control                                                     */

    uint32_t ClkDiv;                        /*!< Defines the period of CLK clock output signal, expressed in number of
                                                 HCLK cycles. This parameter can be a value between Min_Data = 2 and Max_Data = 16.
                                                 @note This parameter is not used for asynchronous NOR Flash, SRAM or ROM
                                                 accesses.                                                                   */
} LL_FSMC_NORSRAMInitTypeDef;


typedef struct {
    uint32_t AccessMode;                    /*!< Select MMAP or EXTM                                                        */

    uint32_t LcdWidth;                      /*!< Specifies the external LCD device width.
                                                 This parameter can be a value of @ref FSMC_NORSRAM_Data_Width              */

    uint32_t LcdType;                       /*!< LCD Type: 0:6800;1:8080                                                    */

    uint32_t EnOrRdPol    ;                 /*!< 6800 EN/8080 RD                                                            */

    uint32_t RwOrWrPol     ;                /*!< 6800 Rw/8080 Wr                                                            */
} LL_FSMC_LCDInitTypeDef;

typedef struct {
    uint32_t AddressSetupTime;              /*!< Defines the number of sys_clk cycles to configure
                                                 the duration of the address setup time.
                                                 This parameter can be a value between Min_Data = 1 and Max_Data = 15.
                                                 @note This parameter is not used with synchronous NOR Flash memories.      */

    uint32_t AddressHoldTime;               /*!< Defines the number of sys_clk cycles to configure
                                                 the duration of the address hold time.
                                                 This parameter can be a value between Min_Data = 0 and Max_Data = 15.
                                                 @note This parameter is not used with synchronous NOR Flash memories.      */

    uint32_t DataSetupTime;                 /*!< Defines the number of sys_clk cycles to configure
                                                 the duration of the data setup time.
                                                 This parameter can be a value between Min_Data = 1 and Max_Data = 255.
                                                 @note This parameter is used for SRAMs, ROMs and asynchronous multiplexed
                                                 NOR Flash memories.                                                        */

    uint32_t DataHoldTime;                  /*!< Defines the number of sys_clk cycles to configure
                                                 the duration of the data hold time.
                                                 This parameter can be a value between Min_Data = 0 and Max_Data = 15.
                                                 @note This parameter is used for SRAMs, ROMs and asynchronous multiplexed
                                                 NOR Flash memories.                                                        */

    uint32_t BusReadyTime;                  /*!< Defines the number of sys_clk cycles to configure
                                                 the duration of the bus ready.
                                                 This parameter can be a value between Min_Data = 0 and Max_Data = 15.
                                                 @note This parameter is only used for multiplexed NOR Flash memories.      */
} LL_FSMC_TimingConfigTypeDef;

/*************************************************************CTRL REG*************************************************/


/** @defgroup LL_FSMC_Enable
 * @{
 */
#define LL_FSMC_DISABLE 0x00U
#define LL_FSMC_ENABLE  FSMC_CRTL_EN


/**
 * @}
 */


/** @defgroup LL_FSMC_Mode
 * @{
 */
#define LL_FSMC_NORSRAM_MODE    0x00U
#define LL_FSMC_LCD_MODE        (0x01U << FSMC_CRTL_MODE_Pos)


/**
 * @}
 */


/** @defgroup LL_FSMC_Width
 * @{
 */
#define LL_FSMC_WIDTH_4     (0x02U << FSMC_CRTL_WIDTH_Pos)
#define LL_FSMC_WIDTH_8     0x00U
#define LL_FSMC_WIDTH_16    (0x01U << FSMC_CRTL_WIDTH_Pos)


/**
 * @}
 */


/** @defgroup LL_FSMC_Width
 * @{
 */
#define LL_FSMC_NWAIT_DISABLE   0x0U
#define LL_FSMC_NWAIT_ENABLE    FSMC_CRTL_WAITEN


/**
 * @}
 */


/** @defgroup LL_FSMC_WaitlPol
 * @{
 */
#define LL_FSMC_NWAITPOL_LOW    0x0U
#define LL_FSMC_NWAITPOL_HIGH   FSMC_CRTL_WAITPOL


/**
 * @}
 */


/** @defgroup LL_FSMC_ClkEn
 * @{
 */
#define LL_FSMC_CLK_DISABLE 0x0U
#define LL_FSMC_CLK_ENABLE  FSMC_CRTL_CKKEN


/**
 * @}
 */


/** @defgroup LL_FSMC_Lcd Type
 * @{
 */
#define LL_FSMC_LCDTYPE_6800    0x0
#define LL_FSMC_LCDTYPE_8080    FSMC_CRTL_LCDTYPE


/**
 * @}
 */


/** @defgroup LL_FSMC_ENorRDPol
 * @{
 */
#define LL_FSMC_ENorRD_LOW  0x0
#define LL_FSMC_ENorRD_HIGH FSMC_CRTL_ENPOL


/**
 * @}
 */


/** @defgroup LL_FSMC_RWorWRPol
 * @{
 */
#define LL_FSMC_RWorWR_LOW  0x0
#define LL_FSMC_RWorWR_HIGH FSMC_CRTL_RWPOL


/**
 * @}
 */
/******************************************************EXTM********************************************************/


/** @defgroup LL_FSMC_AccessMode
 * @{
 */
#define LL_FSMC_ACCESS_MMAP     0x00
#define LL_FSMC_ACCESS_EXTM     FSMC_EXTM_EN
#define LL_FSMC_ACCESS_EXTMFIFO (FSMC_EXTM_EN | FSMC_EXTM_RDFIFOEN | FSMC_EXTM_WRFIFOEN)
#define LL_FSMC_ACCESS_GPIO     (FSMC_EXTM_GPIOEN | FSMC_EXTM_EN)


/**
 * @}
 */

/******************************************************INTE********************************************************/


/** @defgroup LL_FSMC_INTEnable
 * @{
 */
#define LL_FSMC_INT_EORD        FSMC_INTE_RD
#define LL_FSMC_INT_EOWR        FSMC_INTE_WR
#define LL_FSMC_INT_ERR         FSMC_INTE_ERR
#define LL_FSMC_INT_WMRD        FSMC_INTE_FIFORD
#define LL_FSMC_INT_RDFIFOOV    FSMC_INTE_FIFOROV
#define LL_FSMC_INT_WMWR        FSMC_INTE_FIFOWR
#define LL_FSMC_INT_WRFIFOOV    FSMC_INTE_FIFOWOV
#define LL_FSMC_DMA_WMTX        FSMC_INTE_DMARXE
#define LL_FSMC_DMA_WMRX        FSMC_INTE_DMATXE


/**
 * @}
 */

/******************************************************INTF********************************************************/


/** @defgroup LL_FSMC_INTEnable
 * @{
 */
#define LL_FSMC_FLAG_DONE           FSMC_STAT_DONE
#define LL_FSMC_FLAG_RSTDONE        FSMC_STAT_RSTDONE
#define LL_FSMC_FLAG_ERR            FSMC_STAT_ERR
#define LL_FSMC_FLAG_RSTFIFO        FSMC_STAT_FIFORST
#define LL_FSMC_FLAG_RDINT          FSMC_INTF_RD
#define LL_FSMC_FLAG_WRINT          FSMC_INTF_WR
#define LL_FSMC_FLAG_ERRINT         FSMC_INTF_ERR
#define LL_FSMC_FLAG_RDFIFOINT      FSMC_INTF_FIFORD
#define LL_FSMC_FLAG_RDFIFOOVINT    FSMC_INTF_FIFOROV
#define LL_FSMC_FLAG_WRFIFOINT      FSMC_INTF_FIFOWR
#define LL_FSMC_FLAG_WRFIFOOVINT    FSMC_INTF_FIFOWOV
#define LL_FSMC_FLAG_RDFIFOCNT      FSMC_FSTAT_RFIFOCNT
#define LL_FSMC_FLAG_RDFIFOEMPTY    FSMC_FSTAT_RFEMPTY
#define LL_FSMC_FLAG_RDFIFOFULL     FSMC_FSTAT_RFFULL
#define LL_FSMC_FLAG_WRFIFOCNT      FSMC_FSTAT_WFCNT
#define LL_FSMC_FLAG_WRFIFOEMPTY    FSMC_FSTAT_WFEMPTY
#define LL_FSMC_FLAG_WRFIFOFULL     FSMC_FSTAT_WFFULL


/**
 * @}
 */

/*************************************************************CTRL REG*************************************************/

/* Private functions ---------------------------------------------------------*/


/** @defgroup FMSC_LL_Functions FSMC Functions
 * @{
 */


/** @defgroup FSMC_LL_Configuration Configuration
 * @{
 */


/**
 * @brief  enable FSMC module
 * @rmtoll CTRL                LL_FSMC_Enable
 * @param  FSMC Instance
 * @retval None
 */
__STATIC_INLINE void LL_FSMC_Enable(FSMC_TypeDef *FSMCx)
{
    SET_BIT(FSMCx->CTRL, FSMC_CRTL_EN);
}


/**
 * @brief  Disable FSMC module
 * @rmtoll CTRL                LL_FSMC_Enable
 * @param  FSMC Instance
 * @retval None
 */
__STATIC_INLINE void LL_FSMC_Disable(FSMC_TypeDef *FSMCx)
{
    CLEAR_BIT(FSMCx->CTRL, FSMC_CRTL_EN);
}


/**
 * @brief  Set FSMC mode
 * @rmtoll CTRL                LL_FSMC_SetWorkMode
 * @param  FSMC Instance
 * @param  This parameter can be one of the following values:
 *         @arg @ref LL_FSMC_NORSRAM_MODE
 *         @arg @ref LL_FSMC_LCD_MODE
 * @retval None
 */
__STATIC_INLINE void LL_FSMC_SetWorkMode(FSMC_TypeDef *FSMCx, uint32_t Mode)
{
    MODIFY_REG(FSMCx->CTRL, FSMC_CRTL_MODE, Mode);
}


/**
 * @brief  Set FSMC Bus Width
 * @rmtoll CTRL                LL_FSMC_SetDataWidth
 * @param  FSMC Instance
 * @param  This parameter can be one of the following values:
 *         @arg @ref LL_FSMC_WIDTH_4
 *         @arg @ref LL_FSMC_WIDTH_8
 *         @arg @ref LL_FSMC_WIDTH_16
 * @retval None
 */
__STATIC_INLINE void LL_FSMC_SetDataWidth(FSMC_TypeDef *FSMCx, uint32_t Width)
{
    MODIFY_REG(FSMCx->CTRL, FSMC_CRTL_WIDTH, Width);
}


/**
 * @brief  Set FSMC wait enable
 * @rmtoll CTRL                LL_FSMC_SeNtWaitMode
 * @param  FSMC Instance
 * @param  This parameter can be one of the following values:
 *         @arg @ref LL_FSMC_NWAIT_DISABLE
 *         @arg @ref LL_FSMC_NWAIT_ENABLE
 * @retval None
 */
__STATIC_INLINE void LL_FSMC_SetNWaitMode(FSMC_TypeDef *FSMCx, uint32_t Wait_en)
{
    MODIFY_REG(FSMCx->CTRL, FSMC_CRTL_WAITEN, Wait_en);
}


/**
 * @brief  Set FSMC waitlPol
 * @rmtoll CTRL                LL_FSMC_SetWaitlPol
 * @param  FSMC Instance
 * @param  This parameter can be one of the following values:
 *         @arg @ref LL_FSMC_NWAITPOL_LOW
 *         @arg @ref LL_FSMC_NWAITPOL_HIGH
 * @retval None
 */
__STATIC_INLINE void LL_FSMC_SetWaitlPol(FSMC_TypeDef *FSMCx, uint32_t Pol)
{
    MODIFY_REG(FSMCx->CTRL, FSMC_CRTL_WAITPOL, Pol);
}


/**
 * @brief  Set FSMC waitlPol
 * @rmtoll CTRL                LL_FSMC_SetClkMode
 * @param  FSMC Instance
 * @param  This parameter can be one of the following values:
 *         @arg @ref LL_FSMC_NWAITPOL_LOW
 *         @arg @ref LL_FSMC_NWAITPOL_HIGH
 * @retval None
 */
__STATIC_INLINE void LL_FSMC_SetClkMode(FSMC_TypeDef *FSMCx, uint32_t Clk_en)
{
    MODIFY_REG(FSMCx->CTRL, FSMC_CRTL_CKKEN, Clk_en);
}


/**
 * @brief  Set FSMC lcd type
 * @rmtoll CTRL                LL_FSMC_SetLCDType
 * @param  FSMC Instance
 * @param  This parameter can be one of the following values:
 *         @arg @ref LL_FSMC_LCDTYPE_6800
 *         @arg @ref LL_FSMC_LCDTYPE_8080
 * @retval None
 */
__STATIC_INLINE void LL_FSMC_SetLCDType(FSMC_TypeDef *FSMCx, uint32_t LcdType)
{
    MODIFY_REG(FSMCx->CTRL, FSMC_CRTL_LCDTYPE, LcdType);
}


/**
 * @brief  Set FSMC lcd type 6800 or 8080 ENorRD pol
 * @rmtoll CTRL                LL_FSMC_SetEnOrRdPol
 * @param  FSMC Instance
 * @param  This parameter can be one of the following values:
 *         @arg @ref LL_FSMC_ENorRD_LOW
 *         @arg @ref LL_FSMC_ENorRD_HIGH
 * @retval None
 */
__STATIC_INLINE void LL_FSMC_SetEnOrRdPol(FSMC_TypeDef *FSMCx, uint32_t Pol)
{
    MODIFY_REG(FSMCx->CTRL, FSMC_CRTL_ENPOL, Pol);
}


/**
 * @brief  Set FSMC lcd type 6800 or 8080 RWoWR pol
 * @rmtoll CTRL                LL_FSMC_SetRwOrWrPol
 * @param  FSMC Instance
 * @param  This parameter can be one of the following values:
 *         @arg @ref LL_FSMC_RWorWR_LOW
 *         @arg @ref LL_FSMC_RWorWR_HIGH
 * @retval None
 */
__STATIC_INLINE void LL_FSMC_SetRwOrWrPol(FSMC_TypeDef *FSMCx, uint32_t Pol)
{
    MODIFY_REG(FSMCx->CTRL, FSMC_CRTL_RWPOL, Pol);
}


/**
 * @brief  Set FSMC CTRL Reg lock
 * @rmtoll CTRL                LL_FSMC_LockCtrlReg
 * @param  FSMC Instance
 * @retval None
 */
__STATIC_INLINE void LL_FSMC_LockCtrlReg(FSMC_TypeDef *FSMCx)
{
    SET_BIT(FSMCx->CTRL, FSMC_CRTL_LOCK);
}


/**
 * @brief  Set FSMC CTRL Reg unlock
 * @rmtoll CTRL                LL_FSMC_UnlockCtrlReg
 * @param  FSMC Instance
 * @retval None
 */
__STATIC_INLINE void LL_FSMC_UnlockCtrlReg(FSMC_TypeDef *FSMCx)
{
    CLEAR_BIT(FSMCx->CTRL, FSMC_CRTL_LOCK);
}


/**
 * @}
 */

/******************************************************ARDT********************************************************/


/**
 * @brief  Set FSMC ReadAddrSetupTime
 * @rmtoll ARDT                LL_FSMC_SetReadAddrSetupTime
 * @param  FSMC Instance
 * @param  This parameter can be one of the following values:
 *         @arg @ref _VALUE_:0x01U~0xFU
 * @retval None
 */
__STATIC_INLINE void LL_FSMC_SetReadAddrSetupTime(FSMC_TypeDef *FSMCx, uint32_t AddrSetupTime)
{
    MODIFY_REG(FSMCx->ARDT, FSMC_ARDT_RDADDSET, AddrSetupTime);
}


/**
 * @brief  Set FSMC ReadAddrHoldTime
 * @rmtoll ARDT                LL_FSMC_SetReadAddrHoldTime
 * @param  FSMC Instance
 * @param  This parameter can be one of the following values:
 *         @arg @ref _VALUE_:0x0U~0xFU
 * @retval None
 */
__STATIC_INLINE void LL_FSMC_SetReadAddrHoldTime(FSMC_TypeDef *FSMCx, uint32_t AddrHoldTime)
{
    MODIFY_REG(FSMCx->ARDT, FSMC_ARDT_RDADDHLD, AddrHoldTime << FSMC_ARDT_RDADDHLD_Pos);
}


/**
 * @brief  Set FSMC ReadAddrHoldTime
 * @rmtoll ARDT                LL_FSMC_ReadAddrHoldTime
 * @param  FSMC Instance
 * @param  This parameter can be one of the following values:
 *         @arg @ref _VALUE_:0x01U~0xFFU
 * @retval None
 */
__STATIC_INLINE void LL_FSMC_SetReadDataSetupTime(FSMC_TypeDef *FSMCx, uint32_t DataSetupTime)
{
    MODIFY_REG(FSMCx->ARDT, FSMC_ARDT_RDDATAST, DataSetupTime << FSMC_ARDT_RDDATAST_Pos);
}


/**
 * @brief  Set FSMC ReadBusReadyTime
 * @rmtoll ARDT                LL_FSMC_SetReadBusReadyTime
 * @param  FSMC Instance
 * @param  This parameter can be one of the following values:
 *         @arg @ref _VALUE_:0x0U~0xFU
 * @retval None
 */
__STATIC_INLINE void LL_FSMC_SetReadBusReadyTime(FSMC_TypeDef *FSMCx, uint32_t ReadyTime)
{
    MODIFY_REG(FSMCx->ARDT, FSMC_ARDT_RDBUSRDY, ReadyTime << FSMC_ARDT_RDBUSRDY_Pos);
}


/**
 * @brief  Set FSMC ReadDataHoldTime
 * @rmtoll ARDT                LL_FSMC_SetReadDataHoldTime
 * @param  FSMC Instance
 * @param  This parameter can be one of the following values:
 *         @arg @ref _VALUE_:0x0U~0xFU
 * @retval None
 */
__STATIC_INLINE void LL_FSMC_SetReadDataHoldTime(FSMC_TypeDef *FSMCx, uint32_t DataHoldTime)
{
    MODIFY_REG(FSMCx->ARDT, FSMC_ARDT_RDDATAHLD, DataHoldTime << FSMC_ARDT_RDDATAHLD_Pos);
}


/**
 * @brief  Set FSMC ARDT Reg lock
 * @rmtoll ARDT                LL_FSMC_LockARDTReg
 * @param  FSMC Instance
 * @retval None
 */
__STATIC_INLINE void LL_FSMC_LockARDTReg(FSMC_TypeDef *FSMCx)
{
    SET_BIT(FSMCx->ARDT, FSMC_ARDT_LOCK);
}


/**
 * @brief  Set FSMC ARDT Reg unlock
 * @rmtoll ARDT                LL_FSMC_UnlockARDTReg
 * @param  FSMC Instance
 * @retval None
 */
__STATIC_INLINE void LL_FSMC_UnlockARDTReg(FSMC_TypeDef *FSMCx)
{
    CLEAR_BIT(FSMCx->ARDT, FSMC_ARDT_LOCK);
}


/******************************************************AWDT********************************************************/


/**
 * @brief  Set FSMC WriteAddrSetupTime
 * @rmtoll AWDT                LL_FSMC_SetWriteAddrSetupTime
 * @param  FSMC Instance
 * @param  This parameter can be one of the following values:
 *         @arg @ref _VALUE_:0x01U~0xFU
 * @retval None
 */
__STATIC_INLINE void LL_FSMC_SetWriteAddrSetupTime(FSMC_TypeDef *FSMCx, uint32_t AddrSetupTime)
{
    MODIFY_REG(FSMCx->AWRT, FSMC_AWRT_WRADDSET, AddrSetupTime);
}


/**
 * @brief  Set FSMC WriteAddrHoldTime
 * @rmtoll AWDT                LL_FSMC_SetWriteAddrHoldTime
 * @param  FSMC Instance
 * @param  This parameter can be one of the following values:
 *         @arg @ref _VALUE_:0x0U~0xFU
 * @retval None
 */
__STATIC_INLINE void LL_FSMC_SetWriteAddrHoldTime(FSMC_TypeDef *FSMCx, uint32_t AddrHoldTime)
{
    MODIFY_REG(FSMCx->AWRT, FSMC_AWRT_WRADDHLD, AddrHoldTime << FSMC_AWRT_WRADDHLD_Pos);
}


/**
 * @brief  Set FSMC WriteDataSetupTime
 * @rmtoll AWDT                LL_FSMC_SetWriteDataSetupTime
 * @param  FSMC Instance
 * @param  This parameter can be one of the following values:
 *         @arg @ref _VALUE_:0x01U~0xFFU
 * @retval None
 */
__STATIC_INLINE void LL_FSMC_SetWriteDataSetupTime(FSMC_TypeDef *FSMCx, uint32_t DataSetupTime)
{
    MODIFY_REG(FSMCx->AWRT, FSMC_AWRT_WRDATAST, DataSetupTime << FSMC_AWRT_WRDATAST_Pos);
}


/**
 * @brief  Set FSMC WriteBusReadyTime
 * @rmtoll AWDT                LL_FSMC_SetWriteBusReadyTime
 * @param  FSMC Instance
 * @param  This parameter can be one of the following values:
 *         @arg @ref _VALUE_:0x0U~0xFU
 * @retval None
 */
__STATIC_INLINE void LL_FSMC_SetWriteBusReadyTime(FSMC_TypeDef *FSMCx, uint32_t ReadyTime)
{
    MODIFY_REG(FSMCx->AWRT, FSMC_AWRT_WRBUSRDY, ReadyTime << FSMC_AWRT_WRBUSRDY_Pos);
}


/**
 * @brief  Set FSMC WriteDataHoldTime
 * @rmtoll AWDT                LL_FSMC_SetWriteDataHoldTime
 * @param  FSMC Instance
 * @param  This parameter can be one of the following values:
 *         @arg @ref _VALUE_:0x0U~0xFU
 * @retval None
 */
__STATIC_INLINE void LL_FSMC_SetWriteDataHoldTime(FSMC_TypeDef *FSMCx, uint32_t DataHoldTime)
{
    MODIFY_REG(FSMCx->ARDT, FSMC_AWRT_WRDATAHLD, DataHoldTime << FSMC_AWRT_WRDATAHLD_Pos);
}


/**
 * @brief  Set FSMC AWDT Reg lock
 * @rmtoll AWDT                LL_FSMC_LockAWDTReg
 * @param  FSMC Instance
 * @retval None
 */
__STATIC_INLINE void LL_FSMC_LockAWDTReg(FSMC_TypeDef *FSMCx)
{
    SET_BIT(FSMCx->ARDT, FSMC_AWRT_LOCK);
}


/**
 * @brief  Set FSMC AWDT Reg unlock
 * @rmtoll AWDT                LL_FSMC_UnlockAWDTReg
 * @param  FSMC Instance
 * @retval None
 */
__STATIC_INLINE void LL_FSMC_UnlockAWDTReg(FSMC_TypeDef *FSMCx)
{
    CLEAR_BIT(FSMCx->ARDT, FSMC_AWRT_LOCK);
}


/******************************************************SYNCT********************************************************/


/**
 * @brief  Set FSMC Clk Div
 * @rmtoll SYNCT                LL_FSMC_ClkDiv
 * @param  FSMC Instance
 * @param  This parameter can be one of the following values:
 *         @arg @ref _VALUE_:0x0U~0xFU
 * @retval FMC_CLK=HCLK/(CLK_DIV[3:0]+1)
 */
__STATIC_INLINE void LL_FSMC_SetClkDiv(FSMC_TypeDef *FSMCx, uint32_t clkdiv)
{
    MODIFY_REG(FSMCx->SYNCT, FSMC_SYNCT_CLKDIV, clkdiv);
}


/**
 * @brief  Set FSMC SYNCT Reg lock
 * @rmtoll SYNCT                LL_FSMC_LockSYNCTReg
 * @param  FSMC Instance
 * @retval None
 */
__STATIC_INLINE void LL_FSMC_LockSYNCTReg(FSMC_TypeDef *FSMCx)
{
    SET_BIT(FSMCx->ARDT, FSMC_SYNCT_LOCK);
}


/**
 * @brief  Set FSMC SYNCT Reg unlock
 * @rmtoll SYNCT                LL_FSMC_UnlockSYNCTReg
 * @param  FSMC Instance
 * @retval None
 */
__STATIC_INLINE void LL_FSMC_UnlockSYNCTReg(FSMC_TypeDef *FSMCx)
{
    CLEAR_BIT(FSMCx->ARDT, FSMC_SYNCT_LOCK);
}


/******************************************************EXTM********************************************************/


/**
 * @brief  Set FSMC Access mode
 * @rmtoll EXTM                 LL_FSMC_SetAccessMode
 * @param  FSMC Instance
 * @param  This parameter can be one of the following values:
 *         @arg @ref LL_FSMC_ACCESS_MMAP
 *         @arg @ref LL_FSMC_ACCESS_EXTM
 *         @arg @ref LL_FSMC_ACCESS_EXTMFIFO
 *         @arg @ref LL_FSMC_ACCESS_GPIO
 * @retval None
 */
__STATIC_INLINE void LL_FSMC_SetAccessMode(FSMC_TypeDef *FSMCx, uint32_t Mode)
{
    MODIFY_REG(FSMCx->EXTM, FSMC_EXTM_EN | FSMC_EXTM_RDFIFOEN | FSMC_EXTM_WRFIFOEN | FSMC_EXTM_GPIOEN, Mode);
}


/**
 * @brief  Set FSMC RD FIFO Enable
 * @rmtoll EXTM                  LL_FSMC_EnableRdFifo
 * @param  FSMC Instance
 * @retval None
 */
__STATIC_INLINE void LL_FSMC_EnableRdFifo(FSMC_TypeDef *FSMCx)
{
    SET_BIT(FSMCx->EXTM, FSMC_EXTM_RDFIFOEN | FSMC_EXTM_EN);
}


/**
 * @brief  Set FSMC RD FIFO Disable
 * @rmtoll EXTM                   LL_FSMC_DisableRdFifo
 * @param  FSMC Instance
 * @retval None
 */
__STATIC_INLINE void LL_FSMC_DisableRdFifo(FSMC_TypeDef *FSMCx)
{
    CLEAR_BIT(FSMCx->EXTM, FSMC_EXTM_RDFIFOEN);
}


/**
 * @brief  Set FSMC WR FIFO Enable
 * @rmtoll EXTM                  LL_FSMC_EnableWrFifo
 * @param  FSMC Instance
 * @retval None
 */
__STATIC_INLINE void LL_FSMC_EnableWrFifo(FSMC_TypeDef *FSMCx)
{
    SET_BIT(FSMCx->EXTM, FSMC_EXTM_WRFIFOEN | FSMC_EXTM_EN);
}


/**
 * @brief  Set FSMC WR FIFO Enable
 * @rmtoll EXTM                    LL_FSMC_DisableWrFifo
 * @param  FSMC Instance
 * @retval None
 */
__STATIC_INLINE void LL_FSMC_DisableWrFifo(FSMC_TypeDef *FSMCx)
{
    CLEAR_BIT(FSMCx->EXTM, FSMC_EXTM_WRFIFOEN);
}


/**
 * @brief  Set FSMC GPIO mode Enable
 * @rmtoll EXTM                   LL_FSMC_EnableGPIOMode
 * @param  FSMC Instance
 * @retval None
 */
__STATIC_INLINE void LL_FSMC_EnableGPIOMode(FSMC_TypeDef *FSMCx)
{
    SET_BIT(FSMCx->EXTM, FSMC_EXTM_GPIOEN | FSMC_EXTM_EN);
}


/**
 * @brief  Set FSMC GPIO mode Enable
 * @rmtoll EXTM                   LL_FSMC_DisableGPIOMode
 * @param  FSMC Instance
 * @retval None
 */
__STATIC_INLINE void LL_FSMC_DisableGPIOMode(FSMC_TypeDef *FSMCx)
{
    CLEAR_BIT(FSMCx->EXTM, FSMC_EXTM_GPIOEN);
}


/**
 * @brief  Set FSMC EXTM Reg lock
 * @rmtoll SYNCT                LL_FSMC_LockEXTMReg
 * @param  FSMC Instance
 * @retval None
 */
__STATIC_INLINE void LL_FSMC_LockEXTMReg(FSMC_TypeDef *FSMCx)
{
    SET_BIT(FSMCx->EXTM, FSMC_EXTM_LOCK);
}


/**
 * @brief  Set FSMC EXTM Reg unlock
 * @rmtoll SYNCT                LL_FSMC_UnlockEXTMReg
 * @param  FSMC Instance
 * @retval None
 */
__STATIC_INLINE void LL_FSMC_UnlockEXTMReg(FSMC_TypeDef *FSMCx)
{
    CLEAR_BIT(FSMCx->EXTM, FSMC_EXTM_LOCK);
}


/******************************************************STAT********************************************************/


/**
 * @brief  get FSMC EXTM mode Done flag
 * @rmtoll STAT                LL_FSMC_IsDone
 * @param  FSMC Instance
 * @retval None
 */
__STATIC_INLINE uint32_t LL_FSMC_IsDone(FSMC_TypeDef *FSMCx)
{
    return (READ_BIT(FSMCx->STAT, FSMC_STAT_DONE) == FSMC_STAT_DONE);
}

/**
 * @brief  get FSMC EXTM mode Done flag
 * @rmtoll STAT                LL_FSMC_IsDone
 * @param  FSMC Instance
 * @retval None
 */
__STATIC_INLINE void LL_FSMC_ClearFlag_Done(FSMC_TypeDef *FSMCx)
{
    SET_BIT(FSMCx->STAT, FSMC_STAT_DONE);
}


/**
 * @brief  get FSMC EXTM mode RstDone flag
 * @rmtoll STAT                LL_FSMC_GetResetDoneFlag
 * @param  FSMC Instance
 * @retval None
 */
__STATIC_INLINE uint32_t LL_FSMC_IsActiveFlag_ResetDone(FSMC_TypeDef *FSMCx)
{
    return (READ_BIT(FSMCx->STAT, FSMC_STAT_RSTDONE));
}


/**
 * @brief  get FSMC EXTM mode err flag
 * @rmtoll STAT                LL_FSMC_IsActiveFlag_Err
 * @param  FSMC Instance
 * @retval None
 */
__STATIC_INLINE uint32_t LL_FSMC_IsActiveFlag_Err(FSMC_TypeDef *FSMCx)
{
    return (READ_BIT(FSMCx->STAT, FSMC_STAT_ERR));
}


/**
 * @brief  clear FSMC EXTM mode err flag
 * @rmtoll STAT                LL_FSMC_ClearFlag_Err
 * @param  FSMC Instance
 * @retval None
 */
__STATIC_INLINE void LL_FSMC_ClearFlag_Err(FSMC_TypeDef *FSMCx)
{
    SET_BIT(FSMCx->STAT, FSMC_STAT_ERR);
}


/**
 * @brief  set FSMC EXTM  read mode
 * @rmtoll STAT                LL_FSMC_SetReadMode
 * @param  FSMC Instance
 * @retval None
 */
__STATIC_INLINE void LL_FSMC_SetReadMode(FSMC_TypeDef *FSMCx)
{
    WRITE_REG(FSMCx->STAT, 0x0);
}


/**
 * @brief  start FSMC EXTM mode read
 * @rmtoll STAT                LL_FSMC_StartRead
 * @param  FSMC Instance
 * @retval None
 */
__STATIC_INLINE void LL_FSMC_StartRead(FSMC_TypeDef *FSMCx)
{
    CLEAR_BIT(FSMCx->STAT, FSMC_STAT_RW);
    SET_BIT(FSMCx->STAT, FSMC_STAT_DONE);
}


/**
 * @brief  set FSMC EXTM  write mode
 * @rmtoll STAT                LL_FSMC_SetWriteMode
 * @param  FSMC Instance
 * @retval None
 */
__STATIC_INLINE void LL_FSMC_SetWriteMode(FSMC_TypeDef *FSMCx)
{
    WRITE_REG(FSMCx->STAT, FSMC_STAT_RW);
}


/**
 * @brief  start FSMC EXTM mode write
 * @rmtoll STAT                LL_FSMC_StartWrite
 * @param  FSMC Instance
 * @retval None
 */
__STATIC_INLINE void LL_FSMC_StartWrite(FSMC_TypeDef *FSMCx)
{
    SET_BIT(FSMCx->STAT, FSMC_STAT_RW);
    SET_BIT(FSMCx->STAT, FSMC_STAT_DONE);
}


/**
 * @brief  Clear FSMC Read fifo
 * @rmtoll STAT                LL_FSMC_ClearRdFifo
 * @param  FSMC Instance
 * @retval None
 */
__STATIC_INLINE void LL_FSMC_ClearFifo(FSMC_TypeDef *FSMCx)
{
    WRITE_REG(FSMCx->STAT, FSMC_STAT_FIFORST);
}


/******************************************************INTE********************************************************/


/**
 * @brief  Set FSMC int enable
 * @rmtoll INTE                LL_FSMC_EnableINT
 * @param  FSMC Instance
 * @param  This parameter can be one of the following values:
 *         @arg @ref LL_FSMC_INT_RD
 *         @arg @ref LL_FSMC_INT_WR
 *         @arg @ref LL_FSMC_INT_ERR
 *         @arg @ref LL_FSMC_INT_RFIFO
 *         @arg @ref LL_FSMC_INT_RFIFOOV
 *         @arg @ref LL_FSMC_INT_WFIFO
 *         @arg @ref LL_FSMC_INT_WFIFOOV
 * @retval None
 */
__STATIC_INLINE void LL_FSMC_EnableINT(FSMC_TypeDef *FSMCx, uint32_t IntCfg)
{
    WRITE_REG(FSMCx->INTE, IntCfg);
}


/**
 * @brief  Set FSMC int disable
 * @rmtoll INTE                LL_FSMC_DisableINT
 * @param  FSMC Instance
 * @param  This parameter can be one of the following values:
 *         @arg @ref LL_FSMC_INT_RD
 *         @arg @ref LL_FSMC_INT_WR
 *         @arg @ref LL_FSMC_INT_ERR
 *         @arg @ref LL_FSMC_INT_RFIFO
 *         @arg @ref LL_FSMC_INT_RFIFOOV
 *         @arg @ref LL_FSMC_INT_WFIFO
 *         @arg @ref LL_FSMC_INT_WFIFOOV
 * @retval None
 */
__STATIC_INLINE void LL_FSMC_DisableINT(FSMC_TypeDef *FSMCx, uint32_t IntCfg)
{
    CLEAR_BIT(FSMCx->INTE, IntCfg);
}


/**
 * @brief  Set FSMC dma enable
 * @rmtoll INTE                 LL_FSMC_EnableDMA
 * @param  FSMC Instance
 * @param  This parameter can be one of the following values:
 *    @arg @ref LL_FSMC_DMA_TX
 *    @arg @ref LL_FSMC_DMA_RX
 * @retval None
 */
__STATIC_INLINE void LL_FSMC_EnableDMA(FSMC_TypeDef *FSMCx, uint32_t DMACfg)
{
    WRITE_REG(FSMCx->INTE, DMACfg);
}


/**
 * @brief  Set FSMC dma disable
 * @rmtoll INTE                  LL_FSMC_DisableDMA
 * @param  FSMC Instance
 * @param  This parameter can be one of the following values:
 *   @arg @ref LL_FSMC_DMA_TX
 *   @arg @ref LL_FSMC_DMA_RX
 * @retval None
 */
__STATIC_INLINE void LL_FSMC_DisableDMA(FSMC_TypeDef *FSMCx, uint32_t DMACfg)
{
    CLEAR_BIT(FSMCx->INTE, DMACfg);
}


/**
 * @brief  Set FSMC INTE Reg lock
 * @rmtoll INTE                LL_FSMC_LockINTEReg
 * @param  FSMC Instance
 * @retval None
 */
__STATIC_INLINE void LL_FSMC_LockINTEReg(FSMC_TypeDef *FSMCx)
{
    SET_BIT(FSMCx->INTE, FSMC_INTE_LOCK);
}


/**
 * @brief  Set FSMC INTE Reg lock
 * @rmtoll INTE                LL_FSMC_UnlockINTEReg
 * @param  FSMC Instance
 * @retval None
 */
__STATIC_INLINE void LL_FSMC_UnlockINTEReg(FSMC_TypeDef *FSMCx)
{
    CLEAR_BIT(FSMCx->INTE, FSMC_INTE_LOCK);
}


/******************************************************INTF********************************************************/


/**
 * @brief  read FSMC int/dma flag
 * @rmtoll INTF                LL_FSMC_GetFlag_IT
 * @param  FSMC Instance
 * @param  This parameter can be one of the following values:
 *         @arg @ref LL_FSMC_FLAG_RDINT
 *         @arg @ref LL_FSMC_FLAG_WRINT
 *         @arg @ref LL_FSMC_FLAG_ERRINT
 *         @arg @ref LL_FSMC_FLAG_RDFIFOINT
 *         @arg @ref LL_FSMC_FLAG_RDFIFOOVINT
 *         @arg @ref LL_FSMC_FLAG_WRFIFOINT
 *         @arg @ref LL_FSMC_FLAG_WRFIFOOVINT
 * @retval None
 */
__STATIC_INLINE uint32_t LL_FSMC_GetFlag_IT(FSMC_TypeDef *FSMCx, uint32_t INTorDMAFlag)
{
    return (READ_BIT(FSMCx->INTF, INTorDMAFlag));
}


/**
 * @brief  clear FSMC int/dma flag
 * @rmtoll INTF                LL_FSMC_ClearFlag_IT
 * @param  FSMC Instance
 * @param  This parameter can be one of the following values:
 *         @arg @ref LL_FSMC_FLAG_RDINT
 *         @arg @ref LL_FSMC_FLAG_WRINT
 *         @arg @ref LL_FSMC_FLAG_ERRINT
 *         @arg @ref LL_FSMC_FLAG_RDFIFOINT
 *         @arg @ref LL_FSMC_FLAG_RDFIFOOVINT
 *         @arg @ref LL_FSMC_FLAG_WRFIFOINT
 *         @arg @ref LL_FSMC_FLAG_WRFIFOOVINT
 * @retval None
 */
__STATIC_INLINE void LL_FSMC_ClearFlag_IT(FSMC_TypeDef *FSMCx, uint32_t INTorDMAFlag)
{
    CLEAR_BIT(FSMCx->INTF, INTorDMAFlag);
}


/******************************************************ADDR********************************************************/


/**
 * @brief  set FSMC EXTM Start Addr
 * @rmtoll ADDR                LL_FSMC_SetEXTMStartAddr
 * @param  FSMC Instance
 * @retval None
 */
__STATIC_INLINE void LL_FSMC_SetEXTMStartAddr(FSMC_TypeDef *FSMCx, uint32_t Addr)
{
    WRITE_REG(FSMCx->FADDR, Addr);
}


/******************************************************WNUM********************************************************/


/**
 * @brief  set FSMC EXTM Word Num
 * @rmtoll WNUM                LL_FSMC_SetEXTMWordNum
 * @param  FSMC Instance
 * @retval None
 */
__STATIC_INLINE void LL_FSMC_SetEXTMWordNum(FSMC_TypeDef *FSMCx, uint32_t WordNum)
{
    WRITE_REG(FSMCx->WNUM, WordNum);
}


/**
 * @brief  get FSMC EXTM Word Num
 * @rmtoll WNUM                LL_FSMC_SetEXTMWordNum
 * @param  FSMC Instance
 * @retval None
 */
__STATIC_INLINE uint32_t LL_FSMC_GetEXTMWordNum(FSMC_TypeDef *FSMCx)
{
    return (READ_REG(FSMCx->WNUM));
}


/******************************************************DATA*******************************************************/


/**
 * @brief  set FSMC EXTM data
 * @rmtoll FDATA                LL_FSMC_SetExtmData
 * @param  FSMC Instance
 * @retval None
 */
__STATIC_INLINE void LL_FSMC_SetEXTMData(FSMC_TypeDef *FSMCx, uint32_t Data)
{
    WRITE_REG(FSMCx->FDATA, Data);
}


/**
 * @brief  get FSMC EXTMdata
 * @rmtoll FDATA                LL_FSMC_GetExtmData
 * @param  FSMC Instance
 * @retval None
 */
__STATIC_INLINE uint32_t LL_FSMC_GetEXTMData(FSMC_TypeDef *FSMCx)
{
    return (READ_REG(FSMCx->FDATA));
}


/******************************************************FCFG*******************************************************/


/**
 * @brief  Set FSMC Read fifo level
 * @rmtoll FCFG                LL_FSMC_SetRdFifoLevel
 * @param  FSMC Instance
 * @param  This parameter can be one of the following values:
 *         @arg @ref _VALUE_:0x0U~0x7U
 * @retval null
 */
__STATIC_INLINE void LL_FSMC_SetRdFifoLevel(FSMC_TypeDef *FSMCx, uint32_t level)
{
    MODIFY_REG(FSMCx->FCFG, FSMC_FCFG_RFLEVEL_Msk, level);
}


/**
 * @brief  Set FSMC write fifo level
 * @rmtoll FCFG                LL_FSMC_SetWrFifoLevel
 * @param  FSMC Instance
 * @param  This parameter can be one of the following values:
 *         @arg @ref _VALUE_:0x0U~0x7U
 * @retval null
 */
__STATIC_INLINE void LL_FSMC_SetWrFifoLevel(FSMC_TypeDef *FSMCx, uint32_t level)
{
    MODIFY_REG(FSMCx->FCFG, FSMC_FCFG_WFLEVEL_Msk, level << FSMC_FCFG_WFLEVEL_Pos);
}


/**
 * @brief  Set FSMC FCFG Reg lock
 * @rmtoll FCFG                LL_FSMC_LockFCFGReg
 * @param  FSMC Instance
 * @retval None
 */

__STATIC_INLINE void LL_FSMC_LockFCFGReg(FSMC_TypeDef *FSMCx)
{
    SET_BIT(FSMCx->FCFG, FSMC_FCFG_LOCK);
}


/**
 * @brief  Set FSMC FCFG Reg unlock
 * @rmtoll FCFG                LL_FSMC_UnlockFCFGReg
 * @param  FSMC Instance
 * @retval None
 */
__STATIC_INLINE void LL_FSMC_UnlockFCFGReg(FSMC_TypeDef *FSMCx)
{
    CLEAR_BIT(FSMCx->FCFG, FSMC_FCFG_LOCK);
}


/******************************************************FSTAT*******************************************************/


/**
 * @brief  get FSMC rd Fifo data Length
 * @rmtoll FSTAT             LL_FSMC_GetLengthRdFifo
 * @param  FSMC Instance
 * @retval None
 */
__STATIC_INLINE uint32_t LL_FSMC_GetLengthRdFifo(FSMC_TypeDef *FSMCx)
{
    return (READ_BIT(FSMCx->FSTAT, FSMC_FSTAT_RFIFOCNT_Msk));
}


/**
 * @brief  get FSMC rd Fifo empty flag
 * @rmtoll FSTAT             LL_FSMC_IsEmpty_RdFifo
 * @param  FSMC Instance
 * @retval None
 */
__STATIC_INLINE uint32_t LL_FSMC_IsEmpty_RdFifo(FSMC_TypeDef *FSMCx)
{
    return (READ_BIT(FSMCx->FSTAT, FSMC_FSTAT_RFEMPTY_Msk) == FSMC_FSTAT_RFEMPTY);
}


/**
 * @brief  get FSMC rd Fifo full flag
 * @rmtoll FSTAT             LL_FSMC_IsFull_RdFifo
 * @param  FSMC Instance
 * @retval None
 */
__STATIC_INLINE uint32_t LL_FSMC_IsFull_RdFifo(FSMC_TypeDef *FSMCx)
{
    return (READ_BIT(FSMCx->FSTAT, FSMC_FSTAT_RFFULL_Msk) == FSMC_FSTAT_RFFULL);
}


/**
 * @brief  get FSMC wr Fifo data Length
 * @rmtoll FSTAT             LL_FSMC_GetLengthWrFifo
 * @param  FSMC Instance
 * @retval None
 */
__STATIC_INLINE uint32_t LL_FSMC_GetLengthWrFifo(FSMC_TypeDef *FSMCx)
{
    return (READ_BIT(FSMCx->FSTAT, FSMC_FSTAT_WFCNT_Msk));
}


/**
 * @brief  get FSMC wr  empty flag
 * @rmtoll FSTAT             LL_FSMC_IsEmpty_WrFifo
 * @param  FSMC Instance
 * @retval None
 */
__STATIC_INLINE uint32_t LL_FSMC_IsEmpty_WrFifo(FSMC_TypeDef *FSMCx)
{
    return (READ_BIT(FSMCx->FSTAT, FSMC_FSTAT_WFEMPTY_Msk) == FSMC_FSTAT_WFEMPTY);
}


/**
 * @brief  get FSMC wr full flag
 * @rmtoll FSTAT             LL_FSMC_IsFull_WrFifo
 * @param  FSMC Instance
 * @retval None
 */
__STATIC_INLINE uint32_t LL_FSMC_IsFull_WrFifo(FSMC_TypeDef *FSMCx)
{
    return (READ_BIT(FSMCx->FSTAT, FSMC_FSTAT_WFFULL_Msk) == FSMC_FSTAT_WFFULL);
}


/**
 * @}FSMC Functions
 */


/**
 * @} FSMC_LL
 */


/**
 * @}FM15F3XX_LL_Driver
 */
void LL_FSMC_NORSRAMInit(FSMC_TypeDef *FSMCx, LL_FSMC_NORSRAMInitTypeDef *FSMC_NORSRAMInitStruct);


void LL_FSMC_LCDInit(FSMC_TypeDef *FSMCx, LL_FSMC_LCDInitTypeDef *FSMC_LCDInitStruct);


void LL_FSMC_ReadTimingConfig(FSMC_TypeDef *FSMCx, LL_FSMC_TimingConfigTypeDef *FSMC_TimingtInitStruct);


void LL_FSMC_WriteTimingConfig(FSMC_TypeDef *FSMCx, LL_FSMC_TimingConfigTypeDef *FSMC_TimingtInitStruct);


void LL_FSMC_START(FSMC_TypeDef *FSMCx);


void LL_FSMC_STOP(FSMC_TypeDef *FSMCx);


void LL_FSMC_EXTM_Write(FSMC_TypeDef *FSMCx, uint32_t Address, uint32_t* Buffer, uint8_t Num);


void LL_FSMC_EXTM_Read(FSMC_TypeDef *FSMCx, uint32_t Address, uint32_t* Buffer, uint8_t Num);


void LL_FSMC_EXTMFIFO_Write(FSMC_TypeDef *FSMCx, uint32_t Address, uint32_t* Buffer, uint8_t Num);


void LL_FSMC_EXTMFIFO_Read(FSMC_TypeDef *FSMCx, uint32_t Address, uint32_t* Buffer, uint8_t Num);


#endif /* defined (FSMC) */

#ifdef __cplusplus
}
#endif

#endif

/************************ (C) COPYRIGHT FudanMicroelectronics *****END OF FILE****/
