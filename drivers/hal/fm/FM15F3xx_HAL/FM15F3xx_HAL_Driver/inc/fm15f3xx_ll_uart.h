/**
 ******************************************************************************
 * @file    fm15f3xx_ll_uart.h
 * @author  TYW
 * @version V1.0.0
 * @date    2020-03-17
 * @brief
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2020 FudanMicroelectronics.
 * All rights reserved.</center></h2>
 *
 ******************************************************************************
 */

/* Define to prevent recursive inclusion -------------------------------------*/

#ifndef __FM15F3XX_LL_UART_H
#define __FM15F3XX_LL_UART_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "fm15f3xx.h"


/** @addtogroup FM15F3XX_LL_Driver
 * @{
 */


/** @addtogroup UART
 * @{
 */


/** @defgroup UART_Exported_Types
 * @{
 */


/**
 * @brief LL UART Init Structure definition
 */

typedef struct {
    uint32_t BaudRate;          /*!< This field defines expected Uart communication baud rate.
                                     This feature can be modified afterwards using unitary function @ref LL_UART_SetBaudRate().*/

    uint32_t Pdsel;             /*!< Specifies the number of data bits transmitted or received in a frame.
                                     This parameter can be a value of @ref UART_LL_EC_P_DSEL.
                                     This feature can be modified afterwards using unitary function @ref LL_UART_SetParity_DataWidth().*/

    uint32_t StopBits;          /*!< Specifies the number of stop bits transmitted.
                                     This parameter can be a value of @ref UART_LL_EC_STOPBITS.
                                     This feature can be modified afterwards using unitary function @ref LL_UART_SetStopBitsLength().*/

    uint32_t TransferDirection; /*!< Specifies whether the Receive and/or Transmit mode is enabled or disabled.
                                     This parameter can be a value of @ref UART_LL_EC_DIRECTION.
                                     This feature can be modified afterwards using unitary function @ref LL_UART_SetTransferDirection().*/

    uint32_t SampleFactor;      /*!< Specifies whether UART SampleFactor(oversampling) mode is 64,16 or 8.
                                     This parameter can be a value of @ref UART_LL_EC_SAMPLEFACTOR.
                                     This feature can be modified afterwards using unitary function @ref LL_UART_SetBaudFactor().*/
} LL_UART_InitTypeDef;


/**
 * @}
 */


/** @defgroup UART_Exported_Constants
 * @{
 */


/** @defgroup UART FIFO length Macros
 * @{
 */

#define UART_TXFIFO_LENGTH  4
#define UART_RXFIFO_LENGTH  4


/**
 * @}
 */


/** @defgroup uart nBpsIndex Macros
 * @{
 */

#define BAUD1200    1200                            //1200bps
#define BAUD2400    2400                            //2400bps
#define BAUD4800    4800                            //4800bps
#define BAUD9600    9600                            //9600bps
#define BAUD19200   19200                           //19200bps
#define BAUD28800   28800                           //28800bps
#define BAUD38400   38400                           //38400bps
#define BAUD57600   57600                           //57600bps
#define BAUD115200  115200                          //115200bps
#define BAUD128000  128000                          //128000bps
#define BARD230400  230400                          //230400bps
#define BAUD256000  256000                          //256000bps


/**
 * @}
 */


/** @defgroup uart speed Macros
 * @{
 */

#define HISPEED     2
#define NMLSPEED    1
#define LOWSPEED    0


/**
 * @}
 */


/** @defgroup UART_RX_EN
 * @{
 */

#define LL_UART_RX_DISABLE  0x00000000U
#define LL_UART_RX_ENABLE   (UART_CR_RXEN)


/**
 * @}
 */


/** @defgroup UART_TX_EN
 * @{
 */

#define LL_UART_TX_DISABLE  0x00000000U
#define LL_UART_TX_ENABLE   (UART_CR_TXEN)


/**
 * @}
 */


/** @defgroup UART_Parity_DataWidth_Sel
 * @{
 */

#define LL_UART_PDSEL_N_8   0x00000000U
#define LL_UART_PDSEL_E_8   (1U << UART_CR_PDSEL_Pos)
#define LL_UART_PDSEL_O_8   (2U << UART_CR_PDSEL_Pos)
#define LL_UART_PDSEL_N_9   (3U << UART_CR_PDSEL_Pos)


/**
 * @}
 */


/** @defgroup UART_Stop_Bits
 * @{
 */

#define LL_UART_STOPSEL_1BIT    0x00000000U
#define LL_UART_STOPSEL_2BIT    (UART_CR_STOPSEL)


/**
 * @}
 */


/** @defgroup UART_Error_IE
 * @{
 */

#define LL_UART_ERRIE_DISABLE   0x00000000U
#define LL_UART_ERRIE_ENABLE    (UART_INTCFG_ERRIE)


/**
 * @}
 */


/** @defgroup uart_speed_brgh
 * @{
 */

#define LL_UART_SPEEDBRGH_LOW       0U
#define LL_UART_SPEEDBRGH_NORMAL    (1U)        //Normal Bradurate<1.5Mbps
#define LL_UART_SPEEDBRGH_HIGH      (2U)        //High Bradurate>=1.5Mbps


/**
 * @}
 */


/** @defgroup UART_Sample_Factor
 * @{
 */

#define LL_UART_SAMPLEFACTOR_64 0x00000000U                             /*!<Low Bradurate factor*/
#define LL_UART_SAMPLEFACTOR_16 (0x1U << UART_SPEED_BRGH_Pos)           /*!<Normal Bradurate factor*/
#define LL_UART_SAMPLEFACTOR_4  (0x2U << UART_SPEED_BRGH_Pos)           /*!<High Bradurate factor*/


/**
 * @}
 */


/** @defgroup UART_LL_EC_IT IT Defines
 * @brief    IT defines which can be used with LL_UART_ReadReg and  LL_UART_WriteReg functions
 * @{
 */

#define LL_UART_RXNEIE      UART_INTCFG_RXFNEIE                         /*!< Read data fifo not empty interrupt enable */
#define LL_UART_RXFHFIE     UART_INTCFG_RXFHFIE                         /*!< Read data fifo half full interrupt enable */
#define LL_UART_TXFEIE      UART_INTCFG_TXFEIE                          /*!< Transmit data fifo empty interrupt enable */
#define LL_UART_TXFHEIE     UART_INTCFG_TXFHEIE                         /*!< Transmit data fifo half empty interrupt enable */
#define LL_UART_TXDONEIE    UART_INTCFG_TXDONEIE                        /*!< Transmission complete interrupt enable */
#define LL_UART_ERRIEE      UART_INTCFG_ERRIE                           /*!< Error interrupt enable */
#define LL_UART_RXFNEDE     UART_INTCFG_RXFNEDE                         /*!< Read data fifo not empty DMA interrupt enable */
#define LL_UART_TXFEDE      UART_INTCFG_TXFEDE                          /*!< Transmit data fifo empty DMA interrupt enable */


/**
 * @}
 */


/** @defgroup UART_LL_EC_DIRECTION Communication Direction
 * @{
 */

#define LL_UART_DIRECTION_NONE  0x00000000U                                 /*!< Transmitter and Receiver are disabled */
#define LL_UART_DIRECTION_RX    UART_CR_RXEN                                /*!< Transmitter is disabled and Receiver is enabled */
#define LL_UART_DIRECTION_TX    UART_CR_TXEN                                /*!< Transmitter is enabled and Receiver is disabled */
#define LL_UART_DIRECTION_TX_RX (UART_CR_TXEN | UART_CR_RXEN)               /*!< Transmitter and Receiver are enabled */


/**
 * @}
 */


/** @defgroup UART_LL_STATE select
 * @{
 */
#define LL_UART_FLAG_TXDONE     UART_STATUS_TXDONE
#define LL_UART_FLAG_RXBUSY     UART_STATUS_RXBUSY
#define LL_UART_FLAG_TXFIFOE    UART_STATUS_TXFIFOE
#define LL_UART_FLAG_TXFIFOHE   UART_STATUS_TXFIFOHE
#define LL_UART_FLAG_RXFIFONE   UART_STATUS_RXFIFONE
#define LL_UART_FLAG_RXFIFOHF   UART_STATUS_RXFIFOHF
#define LL_UART_FLAG_TXFIFOERR  UART_STATUS_TXFOERR
#define LL_UART_FLAG_RXFIFOERR  UART_STATUS_RXFOERR
#define LL_UART_FLAG_FERR       UART_STATUS_FERR
#define LL_UART_FLAG_PERR       UART_STATUS_PERR


/**
 * @}
 */


/** @defgroup UART_LL_EC_DIRECTION Communication Direction
 * @{
 */

#define LL_UART_AUTO_MODE0  0x00000000U
#define LL_UART_AUTO_MODE1  UART_ACR_AUTOMODE


/**
 * @}
 */


/**
 * @}
 */


/** @defgroup UART_Exported_Functions
 * @{
 */


/**
 * @brief  Write a value in UART register
 * @param  __INSTANCE__ UART Instance
 * @param  __REG__ Register to be written
 * @param  __VALUE__ Value to be written in the register
 * @retval None
 */
#define LL_UART_WriteReg( __INSTANCE__, __REG__, __VALUE__ ) WRITE_REG( __INSTANCE__->__REG__, (__VALUE__) )


/**
 * @brief  Read a value in UART register
 * @param  __INSTANCE__ UART Instance
 * @param  __REG__ Register to be read
 * @retval Register value
 */
#define LL_UART_ReadReg( __INSTANCE__, __REG__ ) READ_REG( __INSTANCE__->__REG__ )


/**
 * @brief  calculate the param of register for LL_UART_SetBaudRate
 * @param  __PERIPHCLK__ PeriphClk
 * @param  __BAUDRATE__ BaudRate
 * @retval result of calculate
 */
#define __LL_UART_CalBrghH_Spbrghl( __PERIPHCLK__, __BAUDRATE__ )   ( ( (__PERIPHCLK__) / (__BAUDRATE__) ) >> 2)    /*div4*/
#define __LL_UART_CalBrghH_Spbrgl( __PERIPHCLK__, __BAUDRATE__ )    ( ( ( ( (__PERIPHCLK__) / (__BAUDRATE__) ) / 4) << UART_SPEED_SPBRGL_Pos) & UART_SPEED_SPBRGL)
#define __LL_UART_CalBrghH_Spbrgh( __PERIPHCLK__, __BAUDRATE__ )    ( ( ( ( ( (__PERIPHCLK__) / (__BAUDRATE__) ) / 4) / 256) << UART_SPEED_SPBRGH_Pos) & UART_SPEED_SPBRGH)
#define __LL_UART_CalBrghH_Bradj( __PERIPHCLK__, __BAUDRATE__ )     ( ( ( ( (__PERIPHCLK__) / (__BAUDRATE__) ) - ( (__LL_UART_CalBrghH_Spbrghl( (__PERIPHCLK__), (__BAUDRATE__) ) ) * 4) ) * 4) << UART_SPEED_BRADJ_Pos)

#define __LL_UART_CalBrghN_Spbrghl( __PERIPHCLK__, __BAUDRATE__ )   ( ( (__PERIPHCLK__) / (__BAUDRATE__) ) >> 4)    /*div16*/
#define __LL_UART_CalBrghN_Spbrgl( __PERIPHCLK__, __BAUDRATE__ )    ( ( ( ( (__PERIPHCLK__) / (__BAUDRATE__) ) / 16) << UART_SPEED_SPBRGL_Pos) & UART_SPEED_SPBRGL)
#define __LL_UART_CalBrghN_Spbrgh( __PERIPHCLK__, __BAUDRATE__ )    ( ( ( ( ( (__PERIPHCLK__) / (__BAUDRATE__) ) / 16) / 256) << UART_SPEED_SPBRGH_Pos) & UART_SPEED_SPBRGH)
#define __LL_UART_CalBrghN_Bradj( __PERIPHCLK__, __BAUDRATE__ )     ( ( ( (__PERIPHCLK__) / (__BAUDRATE__) ) - ( (__LL_UART_CalBrghN_Spbrghl( (__PERIPHCLK__), (__BAUDRATE__) ) ) * 16) ) << UART_SPEED_BRADJ_Pos)

#define __LL_UART_CalBrghL_Spbrghl( __PERIPHCLK__, __BAUDRATE__ )   ( ( (__PERIPHCLK__) / (__BAUDRATE__) ) >> 6)    /*div64*/
#define __LL_UART_CalBrghL_Spbrgl( __PERIPHCLK__, __BAUDRATE__ )    ( ( ( ( (__PERIPHCLK__) / (__BAUDRATE__) ) / 64) << UART_SPEED_SPBRGL_Pos) & UART_SPEED_SPBRGL)
#define __LL_UART_CalBrghL_Spbrgh( __PERIPHCLK__, __BAUDRATE__ )    ( ( ( ( ( (__PERIPHCLK__) / (__BAUDRATE__) ) / 64) / 256) << UART_SPEED_SPBRGH_Pos) & UART_SPEED_SPBRGH)
#define __LL_UART_CalBrghL_Bradj( __PERIPHCLK__, __BAUDRATE__ )     ( ( ( ( (__PERIPHCLK__) / (__BAUDRATE__) ) - ( (__LL_UART_CalBrghL_Spbrghl( (__PERIPHCLK__), (__BAUDRATE__) ) ) * 64) ) / 4) << UART_SPEED_BRADJ_Pos)


/**
 * @}
 */


/** @defgroup UART_Exported_Functions
 * @{
 */


/**
 * @brief  UART TX Enable
 * @rmtoll CR          TXEN            LL_UART_EnableTx
 * @param  UARTx UART Instance
 * @retval None
 */
__STATIC_INLINE void LL_UART_EnableTx(UART_TypeDef *UARTx)
{
    SET_BIT(UARTx->CR, UART_CR_TXEN);
}


/**
 * @brief  UART TX Disable
 * @rmtoll CR          TXEN            LL_UART_DisableTx
 * @param  UARTx UART Instance
 * @retval None
 */
__STATIC_INLINE void LL_UART_DisableTx(UART_TypeDef *UARTx)
{
    CLEAR_BIT(UARTx->CR, UART_CR_TXEN);
}


/**
 * @brief  UART RX Enable
 * @rmtoll CR          RXEN            LL_UART_EnableRx
 * @param  UARTx UART Instance
 * @retval None
 */
__STATIC_INLINE void LL_UART_EnableRx(UART_TypeDef *UARTx)
{
    SET_BIT(UARTx->CR, UART_CR_RXEN);
}


/**
 * @brief  UART RX Disable
 * @rmtoll CR          RXEN            LL_UART_DisableRx
 * @param  UARTx UART Instance
 * @retval None
 */
__STATIC_INLINE void LL_UART_DisableRx(UART_TypeDef *UARTx)
{
    CLEAR_BIT(UARTx->CR, UART_CR_RXEN);
}


/**
 * @brief  Configure simultaneously enabled/disabled states
 *         of Transmitter and Receiver
 * @rmtoll CR          RXEN            LL_UART_SetTransferDir
 *         CR          TXEN            LL_UART_SetTransferDir
 * @param  UARTx UART Instance
 * @param  TransferDirection This parameter can be one of the following values:
 *         @arg @ref LL_UART_DIRECTION_NONE
 *         @arg @ref LL_UART_DIRECTION_RX
 *         @arg @ref LL_UART_DIRECTION_TX
 *         @arg @ref LL_UART_DIRECTION_TX_RX
 * @retval None
 */
__STATIC_INLINE void LL_UART_SetTransferDir(UART_TypeDef *UARTx, uint32_t TransferDirection)
{
    MODIFY_REG(UARTx->CR, UART_CR_RXEN | UART_CR_TXEN, TransferDirection);
}


/**
 * @brief  Return enabled/disabled states of Transmitter and Receiver
 * @rmtoll CR          RXEN            LL_UART_GetTransferDir\n
 *         CR          TXEN            LL_UART_GetTransferDir
 * @param  UARTx UART Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_UART_DIRECTION_NONE
 *         @arg @ref LL_UART_DIRECTION_RX
 *         @arg @ref LL_UART_DIRECTION_TX
 *         @arg @ref LL_UART_DIRECTION_TX_RX
 */
__STATIC_INLINE uint32_t LL_UART_GetTransferDir(UART_TypeDef *UARTx)
{
    return ((uint32_t)(READ_BIT(UARTx->CR, UART_CR_TXEN | UART_CR_RXEN)));
}


/**
 * @brief  Configure Parity and DataWidth (enabled/disabled and parity mode if enabled).
 * @note   This function selects Odd or Even when 8bit is used, computed parity bit is inserted at the MSB position
 *         (9th or 8th bit depending on data width) and parity is checked on the received data.
 * @rmtoll CR          PDSEL            LL_UART_SetParity_DataWidth
 * @param  UARTx UART Instance
 * @param  Parity This parameter can be one of the following values:
 *         @arg @ref LL_UART_PDSEL_N_8
 *         @arg @ref LL_UART_PDSEL_E_8
 *         @arg @ref LL_UART_PDSEL_O_8
 *         @arg @ref LL_UART_PDSEL_N_9
 * @retval None
 */
__STATIC_INLINE void LL_UART_SetParity_DataWidth(UART_TypeDef *UARTx, uint32_t Parity_DataWidth)
{
    MODIFY_REG(UARTx->CR, UART_CR_PDSEL, Parity_DataWidth);
}


/**
 * @brief  Return Parity and DataWidth configuration
 * @rmtoll CR          PDSEL            LL_UART_GetParity_DataWidth
 * @param  UARTx UART Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_UART_PDSEL_N_8
 *         @arg @ref LL_UART_PDSEL_E_8
 *         @arg @ref LL_UART_PDSEL_O_8
 *         @arg @ref LL_UART_PDSEL_N_9
 */
__STATIC_INLINE uint32_t LL_UART_GetParity_DataWidth(UART_TypeDef *UARTx)
{
    return ((uint32_t)(READ_BIT(UARTx->CR, UART_CR_PDSEL)));
}


/**
 * @brief  Set the length of the stop bits
 * @rmtoll CR          STOPSEL          LL_UART_SetStopBitLength
 * @param  UARTx UART Instance
 * @param  StopBits This parameter can be one of the following values:
 *         @arg @ref LL_UART_STOPBITS_1
 *         @arg @ref LL_UART_STOPBITS_2
 * @retval None
 */
__STATIC_INLINE void LL_UART_SetLengthStopBit(UART_TypeDef *UARTx, uint32_t StopBits)
{
    MODIFY_REG(UARTx->CR, UART_CR_STOPSEL, StopBits);
}


/**
 * @brief  Retrieve the length of the stop bits
 * @rmtoll CR          STOPSEL          LL_UART_GetStopBitLength
 * @param  UARTx UART Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_UART_STOPBITS_1
 *         @arg @ref LL_UART_STOPBITS_2
 */
__STATIC_INLINE uint32_t LL_UART_GetLengthStopBit(UART_TypeDef *UARTx)
{
    return ((uint32_t)(READ_BIT(UARTx->CR, UART_CR_STOPSEL)));
}


/**
 * @brief  Set Sample factor to 8-bit, 16-bit or 64-bit mode
 * @rmtoll SPEED          BRGH         LL_UART_SetOverSampling
 * @param  UARTx UART Instance
 * @param  OverSampling This parameter can be one of the following values:
 *         @arg @ref LL_UART_SAMPLEFACTOR_64
 *         @arg @ref LL_UART_SAMPLEFACTOR_16
 *         @arg @ref LL_UART_SAMPLEFACTOR_8
 * @retval None
 */
__STATIC_INLINE void LL_UART_SetSampleFactor(UART_TypeDef *UARTx, uint32_t SampleFactor)
{
    MODIFY_REG(UARTx->SPEED, UART_SPEED_BRGH, SampleFactor);
}


/**
 * @brief  Return Sample factor
 * @rmtoll SPEED          BRGH         LL_UART_GetOverSampling
 * @param  UARTx UART Instance
 * @retval Returned value can be one of the following values:
 *         @arg @ref LL_UART_SAMPLEFACTOR_64
 *         @arg @ref LL_UART_SAMPLEFACTOR_16
 *         @arg @ref LL_UART_SAMPLEFACTOR_8
 */
__STATIC_INLINE uint32_t LL_UART_GetSampleFactor(UART_TypeDef *UARTx)
{
    return ((uint32_t)(READ_BIT(UARTx->SPEED, UART_SPEED_BRGH)));
}


/**
 * @brief  Configure Character frame format (Datawidth, Parity control, Stop Bits)
 * @note   Call of this function is equivalent to following function call sequence :
 *         - Parity mode and Data Width configuration using @ref LL_UART_SetParity_DataWidth() function
 *         - Stop bits configuration using @ref LL_UART_SetStopBitsLength() function
 * @rmtoll CR          PDSEL         LL_UART_ConfigCharacter\n
 *         CR          STOPSEL       LL_UART_ConfigCharacter
 * @param  UARTx UART Instance
 * @param  Parity and DataWidth This parameter can be one of the following values:
 *         @arg @ref LL_UART_PDSEL_N_8
 *         @arg @ref LL_UART_PDSEL_E_8
 *         @arg @ref LL_UART_PDSEL_O_8
 *         @arg @ref LL_UART_PDSEL_N_9
 * @param  StopBits This parameter can be one of the following values:
 *         @arg @ref LL_UART_STOPBITS_1
 *         @arg @ref LL_UART_STOPBITS_2
 * @retval None
 */
__STATIC_INLINE void LL_UART_ConfigCharacter(UART_TypeDef *UARTx, uint32_t Parity_DataWidth,
        uint32_t StopBits)
{
    MODIFY_REG(UARTx->CR, UART_CR_PDSEL, Parity_DataWidth);
    MODIFY_REG(UARTx->CR, UART_CR_STOPSEL, StopBits);
}


/**
 * @brief  Configure UART BRR register for achieving expected Baud Rate value.
 * @note   Compute and set UARTDIV value in BRR Register (full BRR content)
 *         according to used Peripheral Clock, Oversampling mode, and expected Baud Rate values
 * @note   Peripheral clock and Baud rate values provided as function parameters should be valid
 *         (Baud rate value != 0)
 * @rmtoll SPEED          BRGH           LL_UART_SetBaudRate\n
 * @rmtoll SPEED          SPBRG_L        LL_UART_SetBaudRate\n
 * @rmtoll SPEED          SPBRG_H        LL_UART_SetBaudRate\n
 * @rmtoll SPEED          BRADJ          LL_UART_SetBaudRate
 * @param  UARTx UART Instance
 * @param  PeriphClk Peripheral Clock
 * @param  brgh This parameter can be one of the following values:
 *         @arg @ref LL_UART_SAMPLEFACTOR_64
 *         @arg @ref LL_UART_SAMPLEFACTOR_16
 *         @arg @ref LL_UART_SAMPLEFACTOR_4
 * @param  BaudRate Baud Rate
 * @retval None
 */
__STATIC_INLINE ErrorStatus LL_UART_SetBaudRate(UART_TypeDef *UARTx, uint32_t PeriphClk, uint32_t brgh, uint32_t BaudRate)
{
    if (brgh == LL_UART_SAMPLEFACTOR_16) {
        if ((PeriphClk / 16) > BaudRate) {
            MODIFY_REG(UARTx->SPEED, UART_SPEED_SPBRGL | UART_SPEED_SPBRGH | UART_SPEED_BRADJ | UART_SPEED_BRGH, __LL_UART_CalBrghN_Spbrgl(PeriphClk, BaudRate) |
                       __LL_UART_CalBrghN_Spbrgh(PeriphClk, BaudRate) | __LL_UART_CalBrghN_Bradj(PeriphClk, BaudRate) | LL_UART_SAMPLEFACTOR_16);
            return (SUCCESS);
        } else {
            MODIFY_REG(UARTx->SPEED, UART_SPEED_SPBRGL | UART_SPEED_SPBRGH | UART_SPEED_BRADJ | UART_SPEED_BRGH,
                       (0U << UART_SPEED_SPBRGL_Pos) | (0U << UART_SPEED_SPBRGH_Pos) | (0U << UART_SPEED_BRADJ_Pos) | LL_UART_SAMPLEFACTOR_16);
            return (ERROR);
        }
    } else if (brgh == LL_UART_SAMPLEFACTOR_4) {
        if ((PeriphClk / 4) > BaudRate) {
            MODIFY_REG(UARTx->SPEED, UART_SPEED_SPBRGL | UART_SPEED_SPBRGH | UART_SPEED_BRADJ | UART_SPEED_BRGH, __LL_UART_CalBrghH_Spbrgl(PeriphClk, BaudRate) |
                       __LL_UART_CalBrghH_Spbrgh(PeriphClk, BaudRate) | __LL_UART_CalBrghH_Bradj(PeriphClk, BaudRate) | LL_UART_SAMPLEFACTOR_4);
            return (SUCCESS);
        } else {
            MODIFY_REG(UARTx->SPEED, UART_SPEED_SPBRGL | UART_SPEED_SPBRGH | UART_SPEED_BRADJ | UART_SPEED_BRGH,
                       (0U << UART_SPEED_SPBRGL_Pos) | (0U << UART_SPEED_SPBRGH_Pos) | (0U << UART_SPEED_BRADJ_Pos) | LL_UART_SAMPLEFACTOR_4);
            return (ERROR);
        }
    } else if (brgh == LL_UART_SAMPLEFACTOR_64) {
        if ((PeriphClk / 64) > BaudRate) {
            MODIFY_REG(UARTx->SPEED, UART_SPEED_SPBRGL | UART_SPEED_SPBRGH | UART_SPEED_BRADJ | UART_SPEED_BRGH, __LL_UART_CalBrghL_Spbrgl(PeriphClk, BaudRate) |
                       __LL_UART_CalBrghL_Spbrgh(PeriphClk, BaudRate) | __LL_UART_CalBrghL_Bradj(PeriphClk, BaudRate) | LL_UART_SAMPLEFACTOR_64);
            return (SUCCESS);
        } else {
            MODIFY_REG(UARTx->SPEED, UART_SPEED_SPBRGL | UART_SPEED_SPBRGH | UART_SPEED_BRADJ | UART_SPEED_BRGH,
                       (0U << UART_SPEED_SPBRGL_Pos) | (0U << UART_SPEED_SPBRGH_Pos) | (0U << UART_SPEED_BRADJ_Pos) | LL_UART_SAMPLEFACTOR_64);
            return (ERROR);
        }
    } else
        return (ERROR);
}


/**
 * @brief  Return current Baud Rate value, according to SPEED register
 *         (full BRR content), and to used Peripheral Clock values
 * @note   In case of non-initialized or invalid value stored in SPEED register, value 0 will be returned.
 * @rmtoll SPEED          BRGH           LL_UART_SetBaudRate\n
 * @rmtoll SPEED          SPBRG_L        LL_UART_SetBaudRate\n
 * @rmtoll SPEED          SPBRG_H        LL_UART_SetBaudRate\n
 * @rmtoll SPEED          BRADJ          LL_UART_SetBaudRate
 * @param  UARTx UART Instance
 * @param  PeriphClk Peripheral Clock
 * @retval Baud Rate
 */
__STATIC_INLINE uint32_t LL_UART_GetBaudRate(UART_TypeDef *UARTx, uint32_t PeriphClk)
{
    register uint32_t   speedcfg    = 0x0U;
    register uint32_t   brrresult   = 0x0U;

    speedcfg = UARTx->SPEED;

    if ((speedcfg & UART_SPEED_BRGH) == LL_UART_SAMPLEFACTOR_16) {
        if ((speedcfg & 0xFFFFU) != 0U) {
            brrresult = (PeriphClk) / ((uint16_t) speedcfg * 16 + ((speedcfg >> 16) & 0x0F));
        } else {
            brrresult = (PeriphClk) / ((uint16_t) speedcfg * 16 + ((speedcfg >> 16) & 0x0F) + 16);
        }
    } else if ((speedcfg & UART_SPEED_BRGH) == LL_UART_SAMPLEFACTOR_4) {
        if ((speedcfg & 0xFFFFU) != 0U) {
            brrresult = ((PeriphClk) * 4U) / ((uint16_t) speedcfg * 16 + ((speedcfg >> 16) & 0x0F));
        } else {
            brrresult = ((PeriphClk) * 4U) / ((uint16_t) speedcfg * 16 + ((speedcfg >> 16) & 0x0F) + 16);
        }
    } else {
        if ((speedcfg & 0xFFFFU) != 0U) {
            brrresult = (PeriphClk) / (4 * ((uint16_t) speedcfg * 16 + ((speedcfg >> 16) & 0x0F)));
        } else {
            brrresult = (PeriphClk) / (4 * ((uint16_t) speedcfg * 16 + ((speedcfg >> 16) & 0x0F) + 16));
        }
    }

    return (brrresult);
}


/**
 * @brief  Check the length of the stored data in UART TX fifo
 * @rmtoll STATUS    TXFIFOS     LL_UART_GetLengthTxFifo
 * @param  UARTx UART Instance
 * @param  flag
 * @retval value 0~4.
 */
__STATIC_INLINE uint32_t LL_UART_GetLengthTxFifo(UART_TypeDef *UARTx)
{
    return (READ_BIT(UARTx->STATUS, UART_STATUS_TXFIFOS) >> UART_STATUS_TXFIFOS_Pos);
}


/**
 * @brief  Check the length of the stored data in UART TX fifo
 * @rmtoll STATUS     RXFIFOS    LL_UART_GetLengthRxFifo
 * @param  UARTx UART Instance
 * @param  flag
 * @retval value 0~4.
 */
__STATIC_INLINE uint32_t LL_UART_GetLengthRxFifo(UART_TypeDef *UARTx)
{
    return (READ_BIT(UARTx->STATUS, UART_STATUS_RXFIFOS) >> UART_STATUS_RXFIFOS_Pos);
}


/**
 * @brief  Check the UART TX fifo is full
 * @rmtoll STATUS    TXFIFOS     LL_UART_IsFull_TxFifo
 * @param  UARTx UART Instance
 * @param  flag
 * @retval State of bit (1 or 0).
 */
__STATIC_INLINE uint32_t LL_UART_IsFull_TxFifo(UART_TypeDef *UARTx)
{
    return (LL_UART_GetLengthTxFifo(UARTx) >= UART_TXFIFO_LENGTH);
}


/**
 * @brief  Check the UART RX fifo is full
 * @rmtoll STATUS   RXFIFOS     LL_UART_IsFull_RxFifo
 * @param  UARTx UART Instance
 * @param  flag
 * @retval State of bit (1 or 0).
 */
__STATIC_INLINE uint32_t LL_UART_IsFull_RxFifo(UART_TypeDef *UARTx)
{
    return (LL_UART_GetLengthRxFifo(UARTx) >= UART_RXFIFO_LENGTH);
}


/**
 * @brief  Check if the UART Flag is set or not
 * @rmtoll STATUS               LL_UART_GetFlag_STATUS
 * @param  UARTx UART Instance
 * @param  flag
 *         @arg @ref LL_UART_FLAG_TXDONE
 *         @arg @ref LL_UART_FLAG_RXBUSY
 *         @arg @ref LL_UART_FLAG_TXFIFOE
 *         @arg @ref LL_UART_FLAG_TXFIFOHE
 *         @arg @ref LL_UART_FLAG_RXFIFONE
 *         @arg @ref LL_UART_FLAG_RXFIFOHF
 *         @arg @ref LL_UART_FLAG_TXFIFOERR
 *         @arg @ref LL_UART_FLAG_RXFIFOERR
 *         @arg @ref LL_UART_FLAG_FERR
 *         @arg @ref LL_UART_FLAG_PERR
 * @retval State of bit (1 or 0).
 */
__STATIC_INLINE uint32_t LL_UART_GetFlag(UART_TypeDef *UARTx, uint32_t flag)
{
    return (READ_BIT(UARTx->STATUS, flag) == (flag));
}


/**
 * @brief  Check if the UART Flag is set or not
 * @rmtoll STATUS               LL_UART_GetFlag_STATUS
 * @param  UARTx UART Instance
 * @param  flag
 *         @arg @ref LL_UART_FLAG_TXDONE
 *         @arg @ref LL_UART_FLAG_TXFIFOERR
 *         @arg @ref LL_UART_FLAG_RXFIFOERR
 *         @arg @ref LL_UART_FLAG_FERR
 *         @arg @ref LL_UART_FLAG_PERR
 * @retval State of bit (1 or 0).
 */
__STATIC_INLINE void LL_UART_ClearFlag(UART_TypeDef *UARTx, uint32_t flag)
{
    CLEAR_BIT(UARTx->STATUS, flag);
}


/**
 * @brief  clear all uart_status_flag.
 * @rmtoll LL_UART_ClearAllFlag
 * @param  USARTx USART Instance
 * @retval None
 */
__STATIC_INLINE void LL_UART_ClearAllFlag(UART_TypeDef *UARTx)
{
    WRITE_REG(UARTx->STATUS, 0x0U);
}


/**
 * @brief  Enable the interrupt of the UART
 * @rmtoll INT                    LL_UART_EnableINT
 * @param  UARTx UART Instance
 * @param  intsrc source This parameter can be one of the following values:
 *         @arg @ref LL_UART_RXNEIE
 *         @arg @ref LL_UART_RXFHFIE
 *         @arg @ref LL_UART_TXFEIE
 *         @arg @ref LL_UART_TXFHEIE
 *         @arg @ref LL_UART_TXDONEIE
 *         @arg @ref LL_UART_ERRIEE
 *         @arg @ref LL_UART_RXFNEDE
 *         @arg @ref LL_UART_TXFEDE
 * @retval None
 */
__STATIC_INLINE void LL_UART_EnableINT(UART_TypeDef *UARTx, uint32_t intsrc)
{
    SET_BIT(UARTx->INTCFG, intsrc);
}


/**
 * @brief  Disable the interrupt of the UART
 * @rmtoll INT                    LL_UART_DisableINT
 * @param  UARTx UART Instance
 * @param  intsrc source This parameter can be one of the following values:
 *         @arg @ref LL_UART_RXNEIE
 *         @arg @ref LL_UART_RXFHFIE
 *         @arg @ref LL_UART_TXFEIE
 *         @arg @ref LL_UART_TXFHEIE
 *         @arg @ref LL_UART_TXDONEIE
 *         @arg @ref LL_UART_ERRIEE
 *         @arg @ref LL_UART_RXFNEDE
 *         @arg @ref LL_UART_TXFEDE
 * @retval None
 */
__STATIC_INLINE void LL_UART_DisableINT(UART_TypeDef *UARTx, uint32_t intsrc)
{
    CLEAR_BIT(UARTx->INTCFG, intsrc);
}


/**
 * @brief  Disable all the interrupt of the UART
 * @rmtoll INT                    LL_UART_DisableAllINT
 * @param  UARTx UART Instance

 * @retval None
 */
__STATIC_INLINE void LL_UART_DisableAllINT(UART_TypeDef *UARTx)
{
    CLEAR_BIT(UARTx->INTCFG, 0xFFU);
}


/**
 * @brief  Check if the UART Error Interrupt is enabled or disabled.
 * @rmtoll INTCFG               LL_UART_IsEnableINT
 * @param  UARTx UART Instance
 * @param  intsrc source This parameter can be one of the following values:
 *         @arg @ref LL_UART_RXNEIE
 *         @arg @ref LL_UART_RXFHFIE
 *         @arg @ref LL_UART_TXFEIE
 *         @arg @ref LL_UART_TXFHEIE
 *         @arg @ref LL_UART_TXDONEIE
 *         @arg @ref LL_UART_ERRIEE
 *         @arg @ref LL_UART_RXFNEDE
 *         @arg @ref LL_UART_TXFEDE
 * @retval State of bit (1 or 0).
 */
__STATIC_INLINE uint32_t LL_UART_IsEnableINT(UART_TypeDef *UARTx, uint32_t intsrc)
{
    return (READ_BIT(UARTx->INTCFG, intsrc) == (intsrc));
}


/**
 * @brief  Read Receiver Data register (Receive Data value, 8 bits)
 * @rmtoll RXFIFO     RXREG         LL_UART_ReceiveData8
 * @param  UARTx UART Instance
 * @retval Value between Min_Data=0x00 and Max_Data=0xFF
 */
__STATIC_INLINE uint8_t LL_UART_ReceiveData8(UART_TypeDef *UARTx)
{
    return ((uint8_t)(READ_BIT(UARTx->RXFIFO, UART_RXREG_RXFIFO)));
}


/**
 * @brief  Read Receiver Data register (Receive Data value, 9 bits)
 * @rmtoll RXFIFO     RXREG         LL_UART_ReceiveData9
 * @rmtoll RX9D       BIT9CR        LL_UART_ReceiveData9
 * @param  UARTx UART Instance
 * @retval Value between Min_Data=0x00 and Max_Data=0x1FF
 */
__STATIC_INLINE uint16_t LL_UART_ReceiveData9(UART_TypeDef *UARTx)
{
    return ((uint16_t)(((READ_BIT(UARTx->BIT9CR, UART_BIT9CR_RX9D) << 7) & (uint16_t) 0x0100) | READ_BIT(UARTx->RXFIFO, UART_RXREG_RXFIFO)));
}


/**
 * @brief  Write in Transmitter Data Register (Transmit Data value, 8 bits)
 * @rmtoll TXFIFO     TXREG         LL_UART_TransmitData8
 * @param  UARTx UART Instance
 * @param  Value between Min_Data=0x00 and Max_Data=0xFF
 * @retval None
 */

__STATIC_INLINE void LL_UART_TransmitData8(UART_TypeDef *UARTx, uint8_t Value)
{
    UARTx->TXFIFO = Value;
}


/**
 * @brief  Write in Transmitter Data Register (Transmit Data value, 9 bits)
 * @rmtoll TXFIFO     TXREG         LL_UART_TransmitData9\n
 * @rmtoll TX9D       BIT9CR        LL_UART_TransmitData9
 * @param  UARTx UART Instance
 * @param  Value between Min_Data=0x00 and Max_Data=0x1FF
 * @retval None
 */
__STATIC_INLINE void LL_UART_TransmitData9(UART_TypeDef *UARTx, uint16_t Value)
{
    UARTx->BIT9CR   = ((uint32_t)Value << 8) & 0x00010000U;;
    UARTx->TXFIFO   = Value & 0x00FFU;
}


/**
 * @brief  Set Auto Detection Mode
 * @param  UARTx UART Instance
 * @param  intsrc source This parameter can be one of the following values:
 *         @arg @ref LL_UART_AUTO_MODE0
 *         @arg @ref LL_UART_AUTO_MODE1
 * @retval None
 */
__STATIC_INLINE void LL_UART_SetAutoDetectMode(UART_TypeDef *UARTx, uint32_t Mode)
{
    MODIFY_REG(UARTx->ACR, UART_ACR_AUTOMODE, Mode);
}


/**
 * @brief  Start Auto Detection
 * @param  UARTx UART Instance
 * @retval None
 */
__STATIC_INLINE void LL_UART_StartAutoDetect(UART_TypeDef *UARTx)
{
    SET_BIT(UARTx->ACR, UART_ACR_START);
}


/**
 * @brief  Reset Auto Detection
 * @param  UARTx UART Instance
 * @retval None
 */
__STATIC_INLINE void LL_UART_ResetAutoDetect(UART_TypeDef *UARTx)
{
    CLEAR_BIT(UARTx->CR, UART_CR_RXEN);
    SET_BIT(UARTx->CR, UART_CR_RXEN);
    SET_BIT(UARTx->ACR, UART_ACR_AUTORST);
    SET_BIT(UARTx->ACR, UART_ACR_START);
}


ErrorStatus LL_UART_DeInit(UART_TypeDef *UARTx);


ErrorStatus LL_UART_Init(UART_TypeDef *UARTx, LL_UART_InitTypeDef UART_InitStruct, uint32_t periphclk);


void LL_UART_StructInit(LL_UART_InitTypeDef *UART_InitStruct);


/**
 * @}
 */


/**
 * @}
 */


/**
 * @}
 */


#ifdef __cplusplus
}
#endif


#endif /* __FM15F3XX_LL_UART_H */

/************************ (C) COPYRIGHT Fudan Microelectronics *****END OF FILE****/

