/**
 ******************************************************************************
 * @file    fm15f3xx_hal_alarm.c
 * @author  WYL
 * @brief   ALM HAL module driver.
 *          This file provides firmware functions to manage the following
 *          functionalities of the Alarm (ALM):
 *           + Initialization and de-initialization functions
 *           + operation functions
 *
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2020 FudanMicroelectronics.
 * All rights reserved.</center></h2>
 *
 ******************************************************************************
 */


/* Includes ------------------------------------------------------------------*/
#include "fm15f3xx_hal.h"


/** @addtogroup FM15F3xx_HAL_Driver
 * @{
 */


/** @defgroup ALM ALM
 * @brief ALM HAL module driver
 * @{
 */

#ifdef ALM_MODULE_ENABLED

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/


/** @addtogroup ALM_Private_Constants ALM Private Constants
 * @{
 */


/**
 * @}
 */
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/
/* Exported functions --------------------------------------------------------*/


/** @defgroup ALM_Exported_Functions ALM Exported Functions
 * @{
 */


/** @defgroup ALM_Exported_Functions_Group1 Initialization and de-initialization functions
 *  @brief    Initialization and Configuration functions
 *
   @verbatim
   ===============================================================================
 ##### Initialization and de-initialization functions #####
   ===============================================================================
   @endverbatim
 * @{
 */


/**
 * @brief  Initializes the ALARM Monitor according to the specified parameters in the HAL_ALM_MonitorInit.
 * @param  GROUPx where x can be (A..D)
 * @param  MonitorInit pointer to a ALM_MonitorInitTypeDef structure that contains
 *         the configuration information.
 * @retval
 */
HAL_StatusTypeDef HAL_ALM_MonitorInit(ALM_MonitorGroupTypeDef GROUPx, ALM_MonitorInitTypeDef* MonitorInit)
{
    uint8_t num = 0, tmp = 0;
    /* Check the parameters */
    assert_param(IS_ALM_MONITOR_GROUP(GROUPx));
    assert_param(IS_ALM_MONITOR_RCD(MonitorInit->RecordEn));
    if (MonitorInit->RecordEn == ALM_MONITOR_RCD_ENABLE) {
        assert_param(IS_ALM_MONITOR_INPUT(MonitorInit->Input));
        assert_param(IS_ALM_MONITOR_DMAEN(MonitorInit->DmaEn));
        assert_param(IS_ALM_MONITOR_INTEN(MonitorInit->IntEn));
        assert_param(IS_ALM_MONITOR_RSTEN(MonitorInit->RstEn));
        assert_param(IS_ALM_MONITOR_POWERRESRT(MonitorInit->Power));
        assert_param(IS_ALM_MONITOR_NRST(MonitorInit->Nrst));
        assert_param(IS_ALM_MONITOR_FILTER(MonitorInit->Filter));

        tmp = (MonitorInit->RecordEn | MonitorInit->Input | MonitorInit->DmaEn | MonitorInit->IntEn | \
               MonitorInit->RstEn | MonitorInit->Power | MonitorInit->Nrst | MonitorInit->Filter);
        if (MONITOR_GROUPA == GROUPx) {
            for (num = 0; num < 10; num++) {
                if (MonitorInit->Monitor & 0x00000001) {
                    if (num < 4)
                        MODIFY_REG(ALM_MONITOR->GROUPA0, 0xFF << 8 * num, tmp << 8 * num);
                    if (num < 8)
                        MODIFY_REG(ALM_MONITOR->GROUPA1, 0xFF << 8 * (num - 4), tmp << 8 * (num - 4));
                    if (num == 8)
                        MODIFY_REG(ALM_MONITOR->GROUPA2, 0xFF, tmp);
                    if (num == 9)
                        MODIFY_REG(ALM_MONITOR->GROUPA3, 0xFF, tmp);
                }
                MonitorInit->Monitor >>= 1;
            }
        } else if (MONITOR_GROUPB == GROUPx) {
            for (num = 0; num < 4; num++) {
                if (MonitorInit->Monitor & 0x00000001) {
                    MODIFY_REG(ALM_MONITOR->GROUPB0, 0xFF << 8 * num, tmp << 8 * num);
                }
                MonitorInit->Monitor >>= 1;
            }
        } else if (MONITOR_GROUPC == GROUPx) {
            for (num = 0; num < 19; num++) {
                if (MonitorInit->Monitor & 0x00000001) {
                    if (num < 4)
                        MODIFY_REG(ALM_MONITOR->GROUPC0, 0xFF << 8 * num, tmp << 8 * num);
                    if (num < 8)
                        MODIFY_REG(ALM_MONITOR->GROUPC1, 0xFF << 8 * (num - 4), tmp << 8 * (num - 4));
                    if (num < 12)
                        MODIFY_REG(ALM_MONITOR->GROUPC2, 0xFF << 8 * (num - 8), tmp << 8 * (num - 8));
                    if (num < 14)
                        MODIFY_REG(ALM_MONITOR->GROUPC3, 0xFF << 8 * (num - 12 + 2), tmp << 8 * (num - 10));
                    if (num < 18)
                        MODIFY_REG(ALM_MONITOR->GROUPC4, 0xFF << 8 * (num - 14), tmp << 8 * (num - 14));
                    if (num == 18)
                        MODIFY_REG(ALM_MONITOR->GROUPC5, 0xFF, tmp);
                }
                MonitorInit->Monitor >>= 1;
            }
        } else if (MONITOR_GROUPD == GROUPx) {
            for (num = 0; num < 9; num++) {
                if (MonitorInit->Monitor & 0x00000001) {
                    if (num < 4)
                        MODIFY_REG(ALM_MONITOR->GROUPD0, 0xFF << 8 * num, tmp << 8 * num);
                    if (num < 8)
                        MODIFY_REG(ALM_MONITOR->GROUPD1, 0xFF << 8 * (num - 4), tmp << 8 * (num - 4));
                }
                MonitorInit->Monitor >>= 1;
            }
        } else {
            return (HAL_ERROR);
        }
    }
    return (HAL_OK);
}


/**
 * @brief  De-initializes the ALARM Monitor.
 * @retval None
 */
void HAL_ALM_MonitorDeInit(void)
{
    /* disable all Monitor */
    LL_ALM_DisableAllMonitor();
}


/**
 * @brief  Initializes the ALARM Sensors according to the specified parameters in the HAL_ALM_SensorsInit.
 * @param  SENSORSn This parameter can be combination of the following values:
 *         @arg @ref ALM_SENSORS_LIGHT_ENABLE
 *         @arg @ref ALM_SENSORS_LIGHT_DISABLE
 *         @arg @ref ALM_SENSORS_VDET_ENABLE
 *         @arg @ref ALM_SENSORS_VDET_DISABLE
 *         @arg @ref ALM_SENSORS_LDO12L_ENABLE
 *         @arg @ref ALM_SENSORS_LDO12L_DISABLE
 *         @arg @ref ALM_SENSORS_LDO16L_ENABLE
 *         @arg @ref ALM_SENSORS_LDO16L_DISABLE
 *         @arg @ref ALM_SENSORS_VDDGLITCH_ENABLE
 *         @arg @ref ALM_SENSORS_VDDGLITCH_DISABLE
 *         @arg @ref ALM_SENSORS_IRC4M_ENABLE
 *         @arg @ref ALM_SENSORS_IRC4M_DISABLE
 *         @arg @ref ALM_SENSORS_SHIELD_ENABLE
 *         @arg @ref ALM_SENSORS_SHIELD_DISABLE
 *         @arg @ref ALM_SENSORS_BASETIMER0_ENABLE
 *         @arg @ref ALM_SENSORS_BASETIMER0_DISABLE
 *         @arg @ref ALM_SENSORS_BASETIMER1_ENABLE
 *         @arg @ref ALM_SENSORS_BASETIMER1_DISABLE
 * @retval
 */
HAL_StatusTypeDef HAL_ALM_SensorsInit(ALM_SensorsTypeDef* SENSORSn)
{
    assert_param(IS_ALM_SENSORS_LIGHT(SENSORSn->Light));
    assert_param(IS_ALM_SENSORS_VDET(SENSORSn->VddDet));
    assert_param(IS_ALM_SENSORS_LDO12L(SENSORSn->Ldo12L));
    assert_param(IS_ALM_SENSORS_LDO16L(SENSORSn->Ldo16L));
    assert_param(IS_ALM_SENSORS_VDDGLITCH(SENSORSn->VddGlitch));
    assert_param(IS_ALM_SENSORS_IRC4M(SENSORSn->Irc4M));
    assert_param(IS_ALM_SENSORS_SHIELD(SENSORSn->Shield));
    assert_param(IS_ALM_SENSORS_BASETIMER0(SENSORSn->BaseTimer0));
    assert_param(IS_ALM_SENSORS_BASETIMER1(SENSORSn->BaseTimer1));
    assert_param(IS_ALM_SENSORS_STANDBY_VDET(SENSORSn->StandBy_Vdet));
    assert_param(IS_ALM_SENSORS_STANDBY_LDO12L(SENSORSn->StandBy_Ldo12L));
    assert_param(IS_ALM_SENSORS_STANDBY_LDO16L(SENSORSn->StandBy_Ldo16L));
    assert_param(IS_ALM_SENSORS_POWERDOWN_VDET(SENSORSn->PowerDown_Vdet));
    assert_param(IS_ALM_SENSORS_POWERDOWN_LDO12L(SENSORSn->PowerDown_Ldo12L));
    assert_param(IS_ALM_SENSORS_POWERDOWN_LDO16L(SENSORSn->PowerDown_Ldo16L));

    SET_BIT(ALM_SENSORS0->EN, SENSORSn->Light | SENSORSn->VddDet | SENSORSn->Ldo12L | SENSORSn->Ldo16L | SENSORSn->VddGlitch | \
            SENSORSn->Irc4M | SENSORSn->StandBy_Vdet | SENSORSn->StandBy_Ldo12L | SENSORSn->StandBy_Ldo16L | \
            SENSORSn->PowerDown_Vdet | SENSORSn->PowerDown_Ldo12L | SENSORSn->PowerDown_Ldo16L);

    SET_BIT(ALM_SENSORS1->EN, SENSORSn->Shield | SENSORSn->BaseTimer0 | SENSORSn->BaseTimer1);

    return (HAL_OK);
}


/**
 * @brief  De-initializes the ALARM Sensors.
 * @retval None
 */
void HAL_ALM_SensorsDeInit(void)
{
    /* disable all sensors */
    LL_ALM_DisableAllSensors();
}


/**
 * @brief  Initializes the ALARM Monitor according to the specified parameters in the HAL_ALM_MonitorInit.
 * @param  BTx BaseTimer where x can be (0..1)
 * @param  MonitorInit pointer to a ALM_MonitorInitTypeDef structure that contains
 *         the configuration information.
 * @retval
 */
HAL_StatusTypeDef HAL_ALM_BaseTimerInit(ALM_BaseTimerTypeDef BTx, ALM_BaseTimerInitTypeDef* BaseTimerInit)
{
    /* Check the parameters */
    assert_param(IS_ALM_BASETIMER(BTx));
    assert_param(IS_ALM_BT_WORKMODE(BaseTimerInit->WorkMode));
    assert_param(IS_ALM_BT_FREQALARM(BaseTimerInit->FreqAlarmEn));
    /* Enable BaseTimer*/
    __HAL_ALM_BT_DISABLE(BTx);
    /* Set Threshold Value */
    WRITE_REG(ALM_SENSORS1->BT[BTx].CLKAC, BaseTimerInit->RefClkCnt);
    /* Set Threshold Value */
    if (ALM_BT_FREQALARM_ENABLE == BaseTimerInit->FreqAlarmEn) {
        assert_param(IS_ALM_BT_COUNT(BaseTimerInit->Alarm.AlarmCntLower));
        assert_param(IS_ALM_BT_COUNT(BaseTimerInit->Alarm.AlarmCntUpper));
        WRITE_REG(ALM_SENSORS1->BT[BTx].CLKBCL, BaseTimerInit->Alarm.AlarmCntLower);
        WRITE_REG(ALM_SENSORS1->BT[BTx].CLKBCH, BaseTimerInit->Alarm.AlarmCntUpper);
    }
    /* Set BaseTimer */
    MODIFY_REG(ALM_SENSORS1->BT[BTx].CFG, 0x7, BaseTimerInit->WorkMode | BaseTimerInit->FreqAlarmEn);

    return (HAL_OK);
}


/**
 * @}
 */


/** @defgroup ALM_Exported_Functions_Group2 operation functions
 * @{
 */


/**
 * @brief  Reads the specified alarm record is Set or Reset.
 * @param  GROUPx where x can be (A..D)
 * @param  AlarmRecord specifies the alarm record bit to read.
 *         This parameter can be combination of the following GroupA values:
 *         @arg @ref ALM_ALARMRESETA_LIGHT
 *         @arg @ref ALM_ALARMRESETA_VDDL
 *         @arg @ref ALM_ALARMRESETA_VDDH
 *         @arg @ref ALM_ALARMRESETA_LDO12L
 *         @arg @ref ALM_ALARMRESETA_LDO16L
 *         @arg @ref ALM_ALARMRESETA_VDDGLITCH
 *         @arg @ref ALM_ALARMRESETA_IRC4M
 *         @arg @ref ALM_ALARMRESETA_BASATIMER0
 *         @arg @ref ALM_ALARMRESETA_BASATIMER1
 *         @arg @ref ALM_ALARMRESETA_SHIELD
 *         or can be combination of the following GroupB values:
 *         @arg @ref ALM_ALARMRESETB_TAMPERALARM
 *         @arg @ref ALM_ALARMRESETB_RTCALARM
 *         @arg @ref ALM_ALARMRESETB_RTC1S
 *         @arg @ref ALM_ALARMRESETB_BKPCHIPRST
 *         or can be combination of the following GroupC values:
 *         @arg @ref ALM_ALARMRESETC_FLASHPE1
 *         @arg @ref ALM_ALARMRESETC_FLASHPE2
 *         @arg @ref ALM_ALARMRESETC_FLASHPEFF
 *         @arg @ref ALM_ALARMRESETC_FLASHNEFF
 *         @arg @ref ALM_ALARMRESETC_FLASHCE
 *         @arg @ref ALM_ALARMRESETC_RAMPE
 *         @arg @ref ALM_ALARMRESETC_RAERPE
 *         @arg @ref ALM_ALARMRESETC_CACHERPE
 *         @arg @ref ALM_ALARMRESETC_PMUTOE
 *         @arg @ref ALM_ALARMRESETC_BUSTOE
 *         @arg @ref ALM_ALARMRESETC_CPULOCKUP
 *         @arg @ref ALM_ALARMRESETC_WDT
 *         @arg @ref ALM_ALARMRESETC_REGPE
 *         @arg @ref ALM_ALARMRESETC_MODECTRLRE
 *         @arg @ref ALM_ALARMRESETC_RNGE
 *         @arg @ref ALM_ALARMRESETC_BCAE
 *         @arg @ref ALM_ALARMRESETC_HASHE
 *         @arg @ref ALM_ALARMRESETC_PAEE
 *         @arg @ref ALM_ALARMRESETC_CPUSELF
 *         or can be combination of the following GroupD values:
 *         @arg @ref ALM_ALARMRESETD_SYSSELFALARM1
 *         @arg @ref ALM_ALARMRESETD_SYSSELFALARM2
 *         @arg @ref ALM_ALARMRESETD_GPIOA
 *         @arg @ref ALM_ALARMRESETD_GPIOB
 *         @arg @ref ALM_ALARMRESETD_GPIOC
 *         @arg @ref ALM_ALARMRESETD_GPIOD
 *         @arg @ref ALM_ALARMRESETD_GPIOE
 *         @arg @ref ALM_ALARMRESETD_GPIOF
 * @retval The alarm record is set or reset.
 */
uint32_t HAL_ALM_IsActiveAlarmRecord(ALM_MonitorGroupTypeDef GROUPx, uint32_t AlarmRecord)
{
    /* Check the parameters */
    assert_param(IS_ALM_MONITOR_GROUP(GROUPx));
    if (MONITOR_GROUPA == GROUPx) {
        return (LL_ALM_IsActiveAlarm_RecordA(AlarmRecord));
    } else if (MONITOR_GROUPB == GROUPx) {
        return (LL_ALM_IsActiveAlarm_RecordB(AlarmRecord));
    } else if (MONITOR_GROUPC == GROUPx) {
        return (LL_ALM_IsActiveAlarm_RecordC(AlarmRecord));
    } else if (MONITOR_GROUPD == GROUPx) {
        return (LL_ALM_IsActiveAlarm_RecordD(AlarmRecord));
    } else {
        return (RESET);
    }
}


/**
 * @brief  Clear all alarm record.
 * @retval None.
 */
void HAL_ALM_ClearAllAlarmAndRecord(void)
{
    /* Clear alarm */
    LL_ALM_ClearAllAlarm();
    /* Clear record */
    LL_ALM_ClearAllAlarmRecord();
}


/**
 * @brief  Reads the specified reset record is Set or Reset.
 * @param  GROUPx where x can be (A..E)
 * @param  AlarmRecord specifies the alarm record bit to read.
 *         This parameter can be combination of the following GroupA values:
 *         @arg @ref ALM_ALARMRESETA_LIGHT
 *         @arg @ref ALM_ALARMRESETA_VDDL
 *         @arg @ref ALM_ALARMRESETA_VDDH
 *         @arg @ref ALM_ALARMRESETA_LDO12L
 *         @arg @ref ALM_ALARMRESETA_LDO16L
 *         @arg @ref ALM_ALARMRESETA_VDDGLITCH
 *         @arg @ref ALM_ALARMRESETA_IRC4M
 *         @arg @ref ALM_ALARMRESETA_BASATIMER0
 *         @arg @ref ALM_ALARMRESETA_BASATIMER1
 *         @arg @ref ALM_ALARMRESETA_SHIELD
 *         or can be combination of the following GroupB values:
 *         @arg @ref ALM_ALARMRESETB_TAMPERALARM
 *         @arg @ref ALM_ALARMRESETB_RTCALARM
 *         @arg @ref ALM_ALARMRESETB_RTC1S
 *         @arg @ref ALM_ALARMRESETB_BKPCHIPRST
 *         or can be combination of the following GroupC values:
 *         @arg @ref ALM_ALARMRESETC_FLASHPE1
 *         @arg @ref ALM_ALARMRESETC_FLASHPE2
 *         @arg @ref ALM_ALARMRESETC_FLASHPEFF
 *         @arg @ref ALM_ALARMRESETC_FLASHNEFF
 *         @arg @ref ALM_ALARMRESETC_FLASHCE
 *         @arg @ref ALM_ALARMRESETC_RAMPE
 *         @arg @ref ALM_ALARMRESETC_RAERPE
 *         @arg @ref ALM_ALARMRESETC_CACHERPE
 *         @arg @ref ALM_ALARMRESETC_PMUTOE
 *         @arg @ref ALM_ALARMRESETC_BUSTOE
 *         @arg @ref ALM_ALARMRESETC_CPULOCKUP
 *         @arg @ref ALM_ALARMRESETC_WDT
 *         @arg @ref ALM_ALARMRESETC_REGPE
 *         @arg @ref ALM_ALARMRESETC_MODECTRLRE
 *         @arg @ref ALM_ALARMRESETC_RNGE
 *         @arg @ref ALM_ALARMRESETC_BCAE
 *         @arg @ref ALM_ALARMRESETC_HASHE
 *         @arg @ref ALM_ALARMRESETC_PAEE
 *         @arg @ref ALM_ALARMRESETC_CPUSELF
 *         or can be combination of the following GroupD values:
 *         @arg @ref ALM_ALARMRESETD_SYSSELFALARM1
 *         @arg @ref ALM_ALARMRESETD_SYSSELFALARM2
 *         @arg @ref ALM_ALARMRESETD_GPIOA
 *         @arg @ref ALM_ALARMRESETD_GPIOB
 *         @arg @ref ALM_ALARMRESETD_GPIOC
 *         @arg @ref ALM_ALARMRESETD_GPIOD
 *         @arg @ref ALM_ALARMRESETD_GPIOE
 *         @arg @ref ALM_ALARMRESETD_GPIOF
 *         or can be combination of the following GroupE values:
 *         @arg @ref ALM_RESETE_POR
 *         @arg @ref ALM_RESETE_BOR
 *         @arg @ref ALM_RESETE_LDO12L
 *         @arg @ref ALM_RESETE_LDO16L
 *         @arg @ref ALM_RESETE_NRST
 *         @arg @ref ALM_RESETE_LLWUEXIT
 *         @arg @ref ALM_RESETE_LLWUERR
 * @retval The Reset record is set or reset.
 */
uint32_t HAL_ALM_IsActiveResetRecord(ALM_MonitorGroupTypeDef GROUPx, uint32_t ResetRecord)
{
    /* Check the parameters */
    assert_param(IS_ALM_RESET_GROUP(GROUPx));
    if (MONITOR_GROUPA == GROUPx) {
        return (LL_ALM_IsActiveReset_RecordA(ResetRecord));
    } else if (MONITOR_GROUPB == GROUPx) {
        return (LL_ALM_IsActiveReset_RecordB(ResetRecord));
    } else if (MONITOR_GROUPC == GROUPx) {
        return (LL_ALM_IsActiveReset_RecordC(ResetRecord));
    } else if (MONITOR_GROUPD == GROUPx) {
        return (LL_ALM_IsActiveReset_RecordD(ResetRecord));
    } else if (MONITOR_GROUPE == GROUPx) {
        return (LL_ALM_IsActiveReset_RecordE(ResetRecord));
    } else {
        return (RESET);
    }
}


/**
 * @brief  Clear all reset record.
 *
 * @retval None.
 */
void HAL_ALM_ClearAllResetRecord(void)
{
    /* Clear rst record */
    LL_ALM_ClearAllResetRecord();
}


/**
 * @brief  Set NRST pin reset range.
 * @param  Range This parameter can be one of the following values:
 *         @arg @ref ALM_NRST_ALL
 *         @arg @ref ALM_NRST_LDO12
 * @retval None.
 */
void HAL_ALM_SetNrstRange(uint32_t Range)
{
    /* Set NRST pin */
    LL_ALM_SetNrstRange(Range);
}


/**
 * @brief  Enanble GroupE Reset.
 * @param  Range This parameter can be one of the following values:
 *         @arg @ref ALM_RESETE_POR
 *         @arg @ref ALM_RESETE_BOR
 *         @arg @ref ALM_RESETE_LDO12L
 *         @arg @ref ALM_RESETE_LDO16L
 *         @arg @ref ALM_RESETE_NRST
 *         @arg @ref ALM_RESETE_LLWUEXIT
 *         @arg @ref ALM_RESETE_LLWUERR
 * @retval None.
 */
void HAL_ALM_EnanbleGroupEReset(uint32_t Reset)
{
    /* Enable GroupE Reset */
    LL_ALM_EnableGroupEReset(Reset);
}


/**
 * @brief  Frequency Test
 * @param  BTx BaseTimer where x can be (0..1)
 * @param  RefClock This parameter can be one of the following values:
 *         @arg @ref ALM_BT_CLK_SYSTEST
 *         @arg @ref ALM_BT_CLK_IRC4M
 *         @arg @ref ALM_BT_CLK_IRC16M
 *         @arg @ref ALM_BT_CLK_PLL
 *         @arg @ref ALM_BT_CLK_OSC
 *         @arg @ref ALM_BT_CLK_PLL_DIV8
 *         @arg @ref ALM_BT_CLK_MCG_PLL_DIV
 *         @arg @ref ALM_BT_CLK_GPIO
 *         @arg @ref ALM_BT_CLK_TRNG_ANA
 *         @arg @ref ALM_BT_CLK_USB48M
 *         @arg @ref ALM_BT_CLK_SYS
 *         @arg @ref ALM_BT_CLK_SECURITY
 *         @arg @ref ALM_BT_CLK_OSC32K
 * @param  TestClock This parameter can be one of the following values:
 *         @arg @ref ALM_BT_CLK_SYSTEST
 *         @arg @ref ALM_BT_CLK_IRC4M
 *         @arg @ref ALM_BT_CLK_IRC16M
 *         @arg @ref ALM_BT_CLK_PLL
 *         @arg @ref ALM_BT_CLK_OSC
 *         @arg @ref ALM_BT_CLK_PLL_DIV8
 *         @arg @ref ALM_BT_CLK_MCG_PLL_DIV
 *         @arg @ref ALM_BT_CLK_GPIO
 *         @arg @ref ALM_BT_CLK_TRNG_ANA
 *         @arg @ref ALM_BT_CLK_USB48M
 *         @arg @ref ALM_BT_CLK_SYS
 *         @arg @ref ALM_BT_CLK_SECURITY
 *         @arg @ref ALM_BT_CLK_OSC32K
 * @retval TestClock Frequency.
 */
uint32_t HAL_ALM_FrequencyTest_Start(ALM_BaseTimerTypeDef BTx, uint32_t RefClock, uint32_t TestClock)
{
    uint32_t RefFerq = 0, Ferq = 0, cnt = 0;

    assert_param(IS_ALM_BT_CLOCK(TestClock));
    assert_param(IS_ALM_BT_CLOCK(RefClock));

    /* Enable BaseTimer and Select Clock*/
    if (ALM_BASETIMER_0 == BTx) {
        MODIFY_REG(ALM_SENSORS1->BTCLKC, 0x300FF, ALM_BTCLKCFG_GTBS_T0CLKA | ALM_BTCLKCFG_GTBS_T0CLKB | RefClock | (TestClock << ALM_BTCLKCFG_T0CLKBSEL_Pos));
    } else {
        MODIFY_REG(ALM_SENSORS1->BTCLKC, 0xCFF00, ALM_BTCLKCFG_GTBS_T1CLKA | ALM_BTCLKCFG_GTBS_T1CLKB | (RefClock << ALM_BTCLKCFG_T1CLKASEL_Pos) | (TestClock << ALM_BTCLKCFG_T1CLKBSEL_Pos));
    }

    /* Clear BaseTimer Count*/
    __HAL_ALM_BTCNT_CLEAR(BTx);

    switch (__HAL_ALM_BT_GET_CLOCK(BTx)) {
    case ALM_BT_CLK_IRC4M:
        RefFerq = IRC4M_VALUE;
        break;
    case ALM_BT_CLK_IRC16M:
        RefFerq = IRC16M_VALUE;
        break;
    case ALM_BT_CLK_SYS:
        RefFerq = HAL_CMU_GetSysClock();
        break;
    case ALM_BT_CLK_OSC:
        RefFerq = HAL_CMU_GetOscClock();
        break;
    case ALM_BT_CLK_OSC32K:
        RefFerq = 32768;
        break;
    default:
        /* Set to SYS Clock*/
        if (ALM_BASETIMER_0 == BTx) {
            MODIFY_REG(ALM_SENSORS1->BTCLKC, 0x300FF, ALM_BTCLKCFG_GTBS_T0CLKA | ALM_BTCLKCFG_GTBS_T0CLKB | ALM_BT_CLK_SYS | (TestClock << ALM_BTCLKCFG_T0CLKBSEL_Pos));
        } else {
            MODIFY_REG(ALM_SENSORS1->BTCLKC, 0xCFF00, ALM_BTCLKCFG_GTBS_T1CLKA | ALM_BTCLKCFG_GTBS_T1CLKB | (ALM_BT_CLK_SYS << ALM_BTCLKCFG_T1CLKASEL_Pos) | (TestClock << ALM_BTCLKCFG_T1CLKBSEL_Pos));
        }
        RefFerq = HAL_CMU_GetSysClock();
        break;
    }

    /* Enable BaseTimer*/
    __HAL_ALM_BT_ENABLE(BTx);

    while (LL_ALM_IsBusy_BaseTimer(BTx))
        ;
    while (!LL_ALM_IsValid_BaseTimer(BTx))
        ;

    Ferq    = LL_ALM_GetBaseTimerResult(BTx);
    cnt     = __HAL_ALM_BT_GET_REFCNT(BTx);
    Ferq    = RefFerq / cnt * (Ferq + 1);

    /* Enable BaseTimer*/
    __HAL_ALM_BT_DISABLE(BTx);

    return (Ferq);
}


/**
 * @}
 */


/**
 * @}
 */

#endif /* ALM_MODULE_ENABLED */


/**
 * @}
 */


/**
 * @}
 */

/************************ (C) COPYRIGHT FudanMicroelectronics *****END OF FILE****/
