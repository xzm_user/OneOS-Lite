/**
 ******************************************************************************
 * @file    fm15f3xx_hal_adc.c
 * @author  WYL
 * @brief   This file provides firmware functions to manage the following
 *          functionalities of the Analog to Digital Convertor (ADC) peripheral:
 *           + Initialization and de-initialization functions
 *           + IO operation functions
 *           + State and errors functions
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2020 FudanMicroelectronics.
 * All rights reserved.</center></h2>
 *
 ******************************************************************************
 */

/* Includes ------------------------------------------------------------------*/
#include "fm15f3xx_hal.h"


/** @addtogroup FM15F3xx_HAL_Driver
 * @{
 */


/** @defgroup ADC ADC
 * @brief ADC driver modules
 * @{
 */

#ifdef ADC_MODULE_ENABLED

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/


/** @addtogroup ADC_Private_Functions
 * @{
 */
static void __ADC_Config(ADC_HandleTypeDef* hadc);

static int32_t COUNT_DELAY_GET_EOC;/* Query ADC EOC Conversion flag delay time count value */
/**
 * @}
 */
/* Exported functions --------------------------------------------------------*/


/** @defgroup ADC_Exported_Functions ADC Exported Functions
 * @{
 */


/**
 * @brief  Initializes the ADCx peripheral according to the specified parameters
 *         in the ADC_InitStruct and initializes the ADC MSP.
 * @param  hadc pointer to a ADC_HandleTypeDef structure that contains
 *         the configuration information for the specified ADC.
 * @retval HAL status
 */
HAL_StatusTypeDef HAL_ADC_Init(ADC_HandleTypeDef* hadc)
{
    HAL_StatusTypeDef tmp_hal_status = HAL_OK;
    uint32_t tmp1, tmp2;
    /* Check ADC handle */
    if (hadc == NULL) {
        return (HAL_ERROR);
    }

    /* Check the parameters */
    assert_param(IS_ADC_ALL_INSTANCE(hadc->Instance));
    assert_param(IS_ADC_CLK(hadc->Init.ClkSel));

    assert_param(IS_ADC_WORKMODE(hadc->Init.WorkMode));
    assert_param(IS_ADC_CONVMODE(hadc->Init.ConvertMode));

    assert_param(IS_ADC_SAMPLE_CYCLE(hadc->Init.SampleTime));
    assert_param(IS_ADC_READY_CYCLE(hadc->Init.ReadyTime));
//  assert_param(IS_ADC_CONVERT_CYCLE(hadc->Init.ConvertTime));
//  assert_param(IS_ADC_SAMPLEHOLD_CYCLE(hadc->Init.SampleHoldTime));

    if (hadc->State == HAL_ADC_STATE_RESET) {
        /* Init the low level hardware */
        HAL_ADC_MspInit(hadc);

        /* Initialize ADC error code */
        ADC_CLEAR_ERRORCODE(hadc);
    }

    /* Configuration of ADC parameters if previous preliminary actions are      */
    /* correctly completed.                                                     */
    if (HAL_IS_BIT_CLR(hadc->State, HAL_ADC_STATE_ERROR_INTERNAL)) {
        /* Set ADC state */
        ADC_STATE_CLR_SET(hadc->State,
                          HAL_ADC_STATE_BUSY,
                          HAL_ADC_STATE_BUSY_INTERNAL);

        /* Set ADC parameters */
        __ADC_Config(hadc);

        /* Set ADC error code to none */
        ADC_CLEAR_ERRORCODE(hadc);

        /* Set the ADC state */
        ADC_STATE_CLR_SET(hadc->State,
                          HAL_ADC_STATE_BUSY_INTERNAL,
                          HAL_ADC_STATE_READY);
    } else {
        tmp_hal_status = HAL_ERROR;
    }
    
    /* Set ADC EOC Conversion flag delay time count value */
    tmp1 = HAL_CMU_GetSysClock() / 1024 / 1024;
    tmp2 = HAL_CMU_GetBusClock() / 1024 / 1024;
    COUNT_DELAY_GET_EOC += tmp1 - 16;     
    if( COUNT_DELAY_GET_EOC > 0)
    { 
        COUNT_DELAY_GET_EOC *= 24;
        COUNT_DELAY_GET_EOC /= 16;
        COUNT_DELAY_GET_EOC += ((int32_t)(HAL_CMU_GetBusClock() / 1024 / 1024) - 16) * 2; 
        COUNT_DELAY_GET_EOC +=  8 * tmp1 / tmp2;
    } 
    else
    {
        COUNT_DELAY_GET_EOC = 0;
    }  
    /* Return function status */
    return (tmp_hal_status);
}


/**
 * @brief  Deinitializes the ADCx peripheral registers to their default reset values.
 * @param  hadc pointer to a ADC_HandleTypeDef structure that contains
 *         the configuration information for the specified ADC.
 * @retval HAL status
 */
HAL_StatusTypeDef HAL_ADC_DeInit(ADC_HandleTypeDef* hadc)
{
    HAL_StatusTypeDef tmp_hal_status = HAL_OK;

    /* Check ADC handle */
    if (hadc == NULL) {
        return (HAL_ERROR);
    }

    /* Check the parameters */
    assert_param(IS_ADC_ALL_INSTANCE(hadc->Instance));

    /* Set ADC state */
    SET_BIT(hadc->State, HAL_ADC_STATE_BUSY_INTERNAL);

    /* Stop potential conversion on going, on regular and injected groups */
    /* Disable ADC peripheral */
    __HAL_ADC_DISABLE(hadc);

    /* Configuration of ADC parameters if previous preliminary actions are      */
    /* correctly completed.                                                     */
    if (HAL_IS_BIT_CLR(hadc->Instance->CTRL, ADC_CTRL_ADCEN)) {
        /* DeInit the low level hardware: RCC clock, NVIC */
        HAL_ADC_MspDeInit(hadc);
        /* Set ADC error code to none */
        ADC_CLEAR_ERRORCODE(hadc);

        /* Set ADC state */
        hadc->State = HAL_ADC_STATE_RESET;
    }

    /* Return function status */
    return (tmp_hal_status);
}


/**
 * @brief  Initializes the ADC MSP.
 * @param  hadc pointer to a ADC_HandleTypeDef structure that contains
 *         the configuration information for the specified ADC.
 * @retval None
 */
__weak void HAL_ADC_MspInit(ADC_HandleTypeDef* hadc)
{
    /* Prevent unused argument(s) compilation warning */
    UNUSED(hadc);


    /* NOTE : This function Should not be modified, when the callback is needed,
              the HAL_ADC_MspInit could be implemented in the user file
     */
}


/**
 * @brief  DeInitializes the ADC MSP.
 * @param  hadc pointer to a ADC_HandleTypeDef structure that contains
 *         the configuration information for the specified ADC.
 * @retval None
 */
__weak void HAL_ADC_MspDeInit(ADC_HandleTypeDef* hadc)
{
    /* Prevent unused argument(s) compilation warning */
    UNUSED(hadc);


    /* NOTE : This function Should not be modified, when the callback is needed,
              the HAL_ADC_MspDeInit could be implemented in the user file
     */
}


/**
 * @brief  Enables ADC and starts conversion.
 * @param  hadc pointer to a ADC_HandleTypeDef structure that contains
 *         the configuration information for the specified ADC.
 * @param  Channel
 * @retval HAL status
 */
HAL_StatusTypeDef HAL_ADC_Start(ADC_HandleTypeDef* hadc, uint32_t Channel)
{
    __IO uint32_t counter = 0U;

    /* Check the parameters */
    assert_param(IS_ADC_ALL_INSTANCE(hadc->Instance));
    assert_param(IS_ADC_CHANNEL(Channel));

    /* Enable the ADC peripheral */


    /* Check if ADC peripheral is disabled in order to enable it and wait during
       Tstab time the ADC's stabilization */
    if ((hadc->Instance->CTRL & ADC_CTRL_ADCEN) != ADC_CTRL_ADCEN) {
        /* Enable the Peripheral */
        __HAL_ADC_ENABLE(hadc);
    }

    /* Start conversion if ADC is effectively enabled */
    if (HAL_IS_BIT_SET(hadc->Instance->CTRL, ADC_CTRL_ADCEN)) {
        /* Set ADC state                                                          */
        /* - Clear state bitfield related to regular group conversion results     */
        /* - Set state bitfield related to regular group operation                */
        ADC_STATE_CLR_SET(hadc->State,
                          HAL_ADC_STATE_READY | HAL_ADC_STATE_EOC,
                          HAL_ADC_STATE_BUSY);

        /* Clear conversion flag, overrun flag and complate flag */
        /* (To ensure of no unknown state from potential previous ADC operations) */
        __HAL_ADC_CLEAR_FLAG(hadc, ADC_FLAG_ALL);

        /* Enable the ADCChannel */
        __HAL_ADC_ENABLE_CHANNEL(hadc, Channel);

        /* if instance of handle correspond to ADC0 and  no external trigger present enable software conversion*/
        if (__HAL_ADC_IS_SOFTWARETRIG(hadc, Channel)) {
            /* Enable the selected ADC software conversion */
            hadc->Instance->SWTRG = ENABLE;
        }
    }

    /* Return function status */
    return (HAL_OK);
}


/**
 * @brief  Disables ADC and stop conversion of regular channels.
 *
 * @note   Caution: This function will stop also injected channels.
 *
 * @param  hadc pointer to a ADC_HandleTypeDef structure that contains
 *         the configuration information for the specified ADC.
 *
 * @retval HAL status.
 */
HAL_StatusTypeDef HAL_ADC_Stop(ADC_HandleTypeDef* hadc)
{
    /* Check the parameters */
    assert_param(IS_ADC_ALL_INSTANCE(hadc->Instance));

    /* Stop potential conversion on going, on regular and injected groups */
    /* Disable ADC peripheral */
    __HAL_ADC_DISABLE(hadc);

    /* Check if ADC is effectively disabled */
    if (HAL_IS_BIT_CLR(hadc->Instance->CTRL, ADC_CTRL_ADCEN)) {
        /* Set ADC state */
        ADC_STATE_CLR_SET(hadc->State, HAL_ADC_STATE_BUSY, HAL_ADC_STATE_READY);
    }

    /* Return function status */
    return (HAL_OK);
}


/**
 * @brief  Poll for conversion complete
 * @param  hadc pointer to a ADC_HandleTypeDef structure that contains
 *         the configuration information for the specified ADC.
 * @param  Channel
 * @param  Timeout Timeout value in millisecond.
 * @retval HAL status
 */
HAL_StatusTypeDef HAL_ADC_PollForConversion(ADC_HandleTypeDef* hadc, uint32_t Channel, uint32_t Timeout)
{
    uint32_t tickstart = 0U;

    /* Check the parameters */
    assert_param(IS_ADC_ALL_INSTANCE(hadc->Instance));
    assert_param(IS_ADC_CHANNEL(Channel));

    /* Verification that ADC configuration is compliant with polling      */
    if (HAL_IS_BIT_SET(hadc->Instance->INTEN, ADC_INTEN_CH0CONVCO | ADC_INTEN_CH1CONVCO | ADC_INTEN_CH2CONVCO | ADC_INTEN_CH3CONVCO)) {
        /* Update ADC state machine to error */
        SET_BIT(hadc->State, HAL_ADC_STATE_ERROR_CONFIG);

        return (HAL_ERROR);
    }

    /* Get tick */
    tickstart = HAL_GetTick();

    /* Check End of conversion flag */
    while (!(__HAL_ADC_GET_FLAG(hadc, 1 << Channel))) {
        /* Check if timeout is disabled (set to infinite wait) */
        if (Timeout != HAL_MAX_DELAY) {
            if ((Timeout == 0U) || ((HAL_GetTick() - tickstart) > Timeout)) {
                /* Update ADC state machine to timeout */
                SET_BIT(hadc->State, HAL_ADC_STATE_TIMEOUT);

                return (HAL_TIMEOUT);
            }
            /* Check ADC done error */
            if (__HAL_ADC_GET_FLAG(hadc, ADC_STATUSRCD_DONEERR)) {
                /* Update ADC state machine to error internal */
                SET_BIT(hadc->State, HAL_ADC_STATE_ERROR_INTERNAL);

                return (HAL_ERROR);
            }
        }
    }
    
//    if (hadc->Init.ConvertMode != ADC_CONVMODE_ONESHOT)
//    {
//       /* Clear conversion flag */
//       __HAL_ADC_CLEAR_FLAG( hadc, 1 << Channel );
//    }

    /* Update ADC state machine */
    SET_BIT(hadc->State, HAL_ADC_STATE_EOC);

    /* Determine whether any further conversion upcoming on group  */
    /* by continuous mode on going.              */
    if (__HAL_ADC_IS_ONESHOT(hadc)) {
        /* Set ADC state */
        CLEAR_BIT(hadc->State, HAL_ADC_STATE_BUSY);
    }

    /* Return ADC state */
    return (HAL_OK);
}


/**
 * @brief  Enables the interrupt and starts ADC conversion.
 * @param  hadc pointer to a ADC_HandleTypeDef structure that contains
 *         the configuration information for the specified ADC.
 * @param  channel.
 * @retval HAL status.
 */
HAL_StatusTypeDef HAL_ADC_Start_IT(ADC_HandleTypeDef* hadc, uint32_t Channel)
{
    __IO uint32_t counter = 0U;

    /* Check the parameters */
    assert_param(IS_ADC_ALL_INSTANCE(hadc->Instance));
    assert_param(IS_ADC_CHANNEL(Channel));

    /* Enable the ADC peripheral */


    /* Check if ADC peripheral is disabled in order to enable it and wait during
       Tstab time the ADC's stabilization */
    if ((hadc->Instance->CTRL & ADC_CTRL_ADCEN) != ADC_CTRL_ADCEN) {
        /* Enable the Peripheral */
        __HAL_ADC_ENABLE(hadc);
    }

    /* Start conversion if ADC is effectively enabled */
    if (HAL_IS_BIT_SET(hadc->Instance->CTRL, ADC_CTRL_ADCEN)) {
        /* Set ADC state                                                          */
        /* - Clear state bitfield related to conversion results     */
        /* - Set state bitfield related to operation                */
        ADC_STATE_CLR_SET(hadc->State,
                          HAL_ADC_STATE_READY | HAL_ADC_STATE_EOC,
                          HAL_ADC_STATE_BUSY);

        /* Clear conversion flag and overrun flag */
        /* (To ensure of no unknown state from potential previous ADC operations) */
        __HAL_ADC_CLEAR_FLAG(hadc, ADC_FLAG_ALL);

        /* Enable end of conversion interrupt */
        __HAL_ADC_ENABLE_IT(hadc, Channel);

        /* Enable the ADCChannel */
        __HAL_ADC_ENABLE_CHANNEL(hadc, Channel);

        /* if instance of handle correspond to ADC0 and  no external trigger present enable software conversion */
        if (__HAL_ADC_IS_SOFTWARETRIG(hadc, Channel)) {
            /* Enable the selected ADC software conversion */
            hadc->Instance->SWTRG = ENABLE;
        }
    }

    /* Return function status */
    return (HAL_OK);
}


/**
 * @brief  Disables the interrupt and stop ADC conversion of channels.
 * @param  hadc pointer to a ADC_HandleTypeDef structure that contains
 *         the configuration information for the specified ADC.
 * @param  channel
 * @retval HAL status.
 */
HAL_StatusTypeDef HAL_ADC_Stop_IT(ADC_HandleTypeDef* hadc, uint32_t Channel)
{
    /* Check the parameters */
    assert_param(IS_ADC_ALL_INSTANCE(hadc->Instance));
    assert_param(IS_ADC_CHANNEL(Channel));

    /* Stop potential conversion on going, on regular and injected groups */
    /* Disable ADC peripheral */
    __HAL_ADC_DISABLE(hadc);

    /* Check if ADC is effectively disabled */
    if (HAL_IS_BIT_CLR(hadc->Instance->CTRL, ADC_CTRL_ADCEN)) {
        /* Disable ADC end of conversion interrupt */
        __HAL_ADC_DISABLE_IT(hadc, Channel);

        /* Set ADC state */
        ADC_STATE_CLR_SET(hadc->State,
                          HAL_ADC_STATE_BUSY,
                          HAL_ADC_STATE_READY);
    }

    /* Return function status */
    return (HAL_OK);
}


/**
 * @brief  Enables the DMA and starts ADC conversion.
 * @param  hadc pointer to a ADC_HandleTypeDef structure that contains
 *         the configuration information for the specified ADC.
 * @param  channel.
 * @retval HAL status.
 */
HAL_StatusTypeDef HAL_ADC_Start_DMA(ADC_HandleTypeDef* hadc, uint32_t Channel)
{
    __IO uint32_t counter = 0U;

    /* Check the parameters */
    assert_param(IS_ADC_ALL_INSTANCE(hadc->Instance));
    assert_param(IS_ADC_CHANNEL(Channel));

    /* Enable the ADC peripheral */


    /* Check if ADC peripheral is disabled in order to enable it and wait during
       Tstab time the ADC's stabilization */
    if ((hadc->Instance->CTRL & ADC_CTRL_ADCEN) != ADC_CTRL_ADCEN) {
        /* Enable the Peripheral */
        __HAL_ADC_ENABLE(hadc);
    }

    /* Start conversion if ADC is effectively enabled */
    if (HAL_IS_BIT_SET(hadc->Instance->CTRL, ADC_CTRL_ADCEN)) {
        /* Set ADC state                                                          */
        /* - Clear state bitfield related to conversion results     */
        /* - Set state bitfield related to operation                */
        ADC_STATE_CLR_SET(hadc->State,
                          HAL_ADC_STATE_READY | HAL_ADC_STATE_EOC,
                          HAL_ADC_STATE_BUSY);

        /* Clear conversion flag and overrun flag */
        /* (To ensure of no unknown state from potential previous ADC operations) */
        __HAL_ADC_CLEAR_FLAG(hadc, ADC_FLAG_ALL);

        /* Enable end of conversion DMA */
        __HAL_ADC_ENABLE_DMA(hadc, Channel);

        /* Enable the ADCChannel */
        __HAL_ADC_ENABLE_CHANNEL(hadc, Channel);

        /* if instance of handle correspond to ADC0 and  no external trigger present enable software conversion */
        if (__HAL_ADC_IS_SOFTWARETRIG(hadc, Channel)) {
            /* Enable the selected ADC software conversion */
            hadc->Instance->SWTRG = ENABLE;
        }
    }

    /* Return function status */
    return (HAL_OK);
}


/**
 * @brief  Disables the DMA and stop ADC conversion of channels.
 * @param  hadc pointer to a ADC_HandleTypeDef structure that contains
 *         the configuration information for the specified ADC.
 * @param  channel
 * @retval HAL status.
 */
HAL_StatusTypeDef HAL_ADC_Stop_DMA(ADC_HandleTypeDef* hadc, uint32_t Channel)
{
    /* Check the parameters */
    assert_param(IS_ADC_ALL_INSTANCE(hadc->Instance));
    assert_param(IS_ADC_CHANNEL(Channel));

    /* Stop potential conversion on going, on regular and injected groups */
    /* Disable ADC peripheral */
    __HAL_ADC_DISABLE(hadc);

    /* Check if ADC is effectively disabled */
    if (HAL_IS_BIT_CLR(hadc->Instance->CTRL, ADC_CTRL_ADCEN)) {
        /* Disable ADC end of conversion interrupt */
        __HAL_ADC_DISABLE_DMA(hadc, Channel);

        /* Set ADC state */
        ADC_STATE_CLR_SET(hadc->State,
                          HAL_ADC_STATE_BUSY,
                          HAL_ADC_STATE_READY);
    }

    /* Return function status */
    return (HAL_OK);
}


/**
 * @brief  Handles ADC interrupt request
 * @param  hadc pointer to a ADC_HandleTypeDef structure that contains
 *         the configuration information for the specified ADC.
 * @retval None
 */
void HAL_ADC_IRQHandler(ADC_HandleTypeDef* hadc, uint32_t Channel)
{
    uint32_t tmp1 = 0U;

    /* Check the parameters */
    assert_param(IS_ADC_CONVMODE(hadc->Init.ConvertMode));
    assert_param(IS_ADC_CHANNEL(Channel));

    tmp1 = __HAL_ADC_GET_FLAG(hadc, 1 << Channel);
    /* Check End of conversion flag */
    if (tmp1) {
        /* Update state machine on conversion status if not in error state */
        if (HAL_IS_BIT_CLR(hadc->State, HAL_ADC_STATE_ERROR_INTERNAL)) {
            /* Set ADC state */
            SET_BIT(hadc->State, HAL_ADC_STATE_EOC);
        }

        /* Determine whether any further conversion upcoming by external trigger, continuous mode on going. */
        if (__HAL_ADC_IS_ONESHOT(hadc)) {
            /* Disable ADC end of single conversion interrupt*/
            __HAL_ADC_DISABLE_IT(hadc, Channel);

            /* Set ADC state */
            CLEAR_BIT(hadc->State, HAL_ADC_STATE_BUSY);
        }
        /* Clear conversion flag */
        __HAL_ADC_CLEAR_FLAG(hadc, 1 << Channel);

        HAL_ADC_ConvCpltCallback(hadc, Channel);
    }
}


/**
 * @brief  conversion complete callback in non blocking mode
 * @param  hadc pointer to a ADC_HandleTypeDef structure that contains
 *         the configuration information for the specified ADC.
 * @retval None
 */
__weak void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef* hadc, uint32_t channel)
{
    /* Prevent unused argument(s) compilation warning */
    UNUSED(hadc);


    /* NOTE : This function Should not be modified, when the callback is needed,
              the HAL_ADC_ConvCpltCallback could be implemented in the user file
     */
}


/**
 * @brief  Gets the converted value from data register of channel.
 * @param  hadc pointer to a ADC_HandleTypeDef structure that contains
 *         the configuration information for the specified ADC.
 * @retval Converted value
 */
uint32_t HAL_ADC_GetValue(ADC_HandleTypeDef* hadc, uint32_t Channel)
{
    /* Clear conversion flag, overrun flag and complate flag */
        __HAL_ADC_CLEAR_FLAG(hadc, 1 << Channel);
    /* Return the selected ADC converted value */
    return (hadc->Instance->CHANNEL[Channel].RESULT);
}


/**
 * @brief  Gets the converted value from Polling.
 * @param  hadc pointer to a ADC_HandleTypeDef structure that contains
 *         the configuration information for the specified ADC.
 * @param  pData pointer to data buf.
 * @retval Converted value
 */
HAL_StatusTypeDef HAL_ADC_Polling_GetValue(ADC_HandleTypeDef* hadc, uint32_t* pData)
{
    int32_t i;
    uint8_t PollingNum, PollingMode, SeqCh[16]; 
    
    /* Query ADC EOC Conversion flag delay */
    if(hadc->Init.ConvertMode	== ADC_CONVMODE_ONESHOT)
    {
        for(i=0;i<COUNT_DELAY_GET_EOC;i++)   
        ; 
    }      
    /* Get Polling Number */
    PollingNum = LL_ADC_GetPollingNum(hadc->Instance);
    if (hadc->Init.ConvertMode == ADC_CONVMODE_CONTINUE) {
        PollingNum += 1;
    }   

    /* Get Channel select of Polling Sequence */
    for (i = 0; i < PollingNum; i++)
        SeqCh[i] = LL_ADC_GetPollSequence(hadc->Instance, i);

    /* Get Channel Polling Mode */
    PollingMode = SeqCh[0];
    for (i = 1; i < PollingNum; i++) {
        if (SeqCh[i] > PollingMode)
            PollingMode = SeqCh[i];
    }
    PollingMode += 1;

    /*Polling Mode */
    switch (PollingMode) {
    /* 2 Channel Polling Mode */
    case 2:
        while (!(__HAL_ADC_GET_FLAG(hadc, 0x03)))
            ;    
        /* Clear conversion flag, overrun flag and complate flag */
        __HAL_ADC_CLEAR_FLAG(hadc, ADC_FLAG_ALL);
        
        pData[1] = HAL_ADC_GetValue(hadc, SeqCh[0]);
        pData[0] = HAL_ADC_GetValue(hadc, SeqCh[1]);
        break;
    /* 3 Channel Polling Mode */
    case 3:
        while (!(__HAL_ADC_GET_FLAG(hadc, 0x07)))
            ;
        /* Clear conversion flag, overrun flag and complate flag */
        __HAL_ADC_CLEAR_FLAG(hadc, ADC_FLAG_ALL);
        
        pData[2] = HAL_ADC_GetValue(hadc, SeqCh[0]);
        pData[0] = HAL_ADC_GetValue(hadc, SeqCh[1]);
        pData[1] = HAL_ADC_GetValue(hadc, SeqCh[2]);
        break;
    /* 4 Channel Polling Mode */
    case 4:
        while (!(__HAL_ADC_GET_FLAG(hadc, 0x0F)))
            ;
        /* Clear conversion flag, overrun flag and complate flag */
        __HAL_ADC_CLEAR_FLAG(hadc, ADC_FLAG_ALL);
        
        pData[3] = HAL_ADC_GetValue(hadc, SeqCh[0]);
        pData[0] = HAL_ADC_GetValue(hadc, SeqCh[1]);
        pData[1] = HAL_ADC_GetValue(hadc, SeqCh[2]);
        pData[2] = HAL_ADC_GetValue(hadc, SeqCh[3]);        
        break;
    default:
        return (HAL_ERROR);
    }
    /* Return function status */
    return (HAL_OK);
}


/**
 * @brief  Configures for the selected ADC regular channel its corresponding
 *         rank in the sequencer and its sample time.
 * @param  hadc pointer to a ADC_HandleTypeDef structure that contains
 *         the configuration information for the specified ADC.
 * @param  sConfig ADC configuration structure.
 * @retval HAL status
 */
HAL_StatusTypeDef HAL_ADC_ChannelConfig(ADC_HandleTypeDef* hadc, ADC_ChannelConfigTypeDef* ChannelConfig)
{
    /* Check the parameters */
    assert_param(IS_ADC_CHANNEL(ChannelConfig->Channel));
    assert_param(IS_ADC_INPUT(ChannelConfig->Input));
    assert_param(IS_ADC_TRIG(ChannelConfig->TrigSrc));
    assert_param(IS_ADC_TRGDLY_CYCLE(ChannelConfig->TrigDelay));

    /* Set Channel CFG */
    hadc->Instance->CHANNEL[ChannelConfig->Channel].CFG = 0;

    if (ChannelConfig->DiffEn == ADC_CHxDIFF_ENABLE) {
        assert_param(IS_ADC_DIFF_EN(ChannelConfig->DiffEn));
        hadc->Instance->CHANNEL[ChannelConfig->Channel].CFG |= ChannelConfig->DiffEn;
    }

    if (ChannelConfig->TrigInvEn == ADC_CHxTRIGINV_ENABLE) {
        assert_param(IS_ADC_TRIGINV_EN(ChannelConfig->TrigInvEn));
        hadc->Instance->CHANNEL[ChannelConfig->Channel].CFG |= ChannelConfig->TrigInvEn;
    }

    if (ChannelConfig->TrigSrc != ADC_TRIGSRC_SOFTWARE) {
        assert_param(IS_ADC_TRIG_EDGE(ChannelConfig->TrigEdge));
        hadc->Instance->CHANNEL[ChannelConfig->Channel].CFG |= ChannelConfig->TrigEdge;
    }

    if (ChannelConfig->Input == ADC_INSRC_VREF12) {
        __HAL_ADC_ENABLE_VREF12();
    }

    hadc->Instance->CHANNEL[ChannelConfig->Channel].CFG |= ADC_CHANNEL_TRGDLY(ChannelConfig->TrigDelay) | ChannelConfig->TrigSrc | ChannelConfig->Input;

    __HAL_ADC_ENABLE_CHANNEL(hadc, ChannelConfig->Channel);

    /* Return function status */
    return (HAL_OK);
}


/**
 * @brief  Configures for the selected ADC channel's data procrss.
 * @param  hadc pointer to a ADC_HandleTypeDef structure that contains
 *         the configuration information for the specified ADC.
 * @param  sConfig ADC DataProcrss configuration structure.
 * @retval HAL status
 */
HAL_StatusTypeDef HAL_ADC_DataProcrssConfig(ADC_HandleTypeDef* hadc, ADC_DataProcessConfigTypeDef* ProcessConfig)
{
    /* Check the parameters */
    assert_param(IS_ADC_CHANNEL(ProcessConfig->Channel));

    /* Set Channel DRS */
    hadc->Instance->CHANNEL[ProcessConfig->Channel].DRS = 0;
    if (ProcessConfig->OffsetEn == ADC_CHxOFFSET_ENABLE) {
        assert_param(IS_ADC_OFFSET_EN(ProcessConfig->OffsetEn));
        hadc->Instance->CHANNEL[ProcessConfig->Channel].DRS |= ProcessConfig->OffsetEn;
        LL_ADC_SetChxOffsetVal(hadc->Instance, ProcessConfig->Channel, ProcessConfig->OffsetValue);
    }

    if (ProcessConfig->AverageEn == ADC_CHxAVERAGE_ENABLE) {
        assert_param(IS_ADC_AVG_EN(ProcessConfig->AverageEn));
        assert_param(IS_ADC_AVG_NUM(ProcessConfig->AverageNum));
        hadc->Instance->CHANNEL[ProcessConfig->Channel].DRS |= ProcessConfig->AverageEn | ProcessConfig->AverageNum;
    }

    if (ProcessConfig->CompareEn == ADC_CHxCOMPARE_ENABLE) {
        assert_param(IS_ADC_CMP_EN(ProcessConfig->CompareEn));
        assert_param(IS_ADC_CMP_MODE(ProcessConfig->CompareMode));
        hadc->Instance->CHANNEL[ProcessConfig->Channel].DRS |= ProcessConfig->CompareEn | ProcessConfig->CompareMode;
        LL_ADC_SetChxCompareValue1(hadc->Instance, ProcessConfig->Channel, ProcessConfig->CompareValue1);
        LL_ADC_SetChxCompareValue2(hadc->Instance, ProcessConfig->Channel, ProcessConfig->CompareValue2);
    }

    /* Return function status */
    return (HAL_OK);
}


/**
 * @brief  return the ADC state
 * @param  hadc pointer to a ADC_HandleTypeDef structure that contains
 *         the configuration information for the specified ADC.
 * @retval HAL state
 */
uint32_t HAL_ADC_GetState(ADC_HandleTypeDef* hadc)
{
    /* Return ADC state */
    return (hadc->State);
}


/**
 * @brief  Return the ADC error code
 * @param  hadc pointer to a ADC_HandleTypeDef structure that contains
 *         the configuration information for the specified ADC.
 * @retval ADC Error Code
 */
uint32_t HAL_ADC_GetError(ADC_HandleTypeDef *hadc)
{
    return (hadc->ErrorCode);
}


/* Private functions ---------------------------------------------------------*/


/**
 * @brief  Initializes the ADCx peripheral according to the specified parameters
 *         in the ADC_InitStruct without initializing the ADC MSP.
 * @param  hadc pointer to a ADC_HandleTypeDef structure that contains
 *         the configuration information for the specified ADC.
 * @retval None
 */
static void __ADC_Config(ADC_HandleTypeDef* hadc)
{
    __HAL_ADC_DISABLE(hadc);

    if (hadc->Init.ClkSel < ADC_CLKSEL_FORCE0) {
        assert_param(IS_ADC_WORKCLK(hadc->Init.WorkClkSel));
        assert_param(IS_ADC_WORKCLKPRES(hadc->Init.WorkClkDiv));
        __HAL_ADC_SET_WORKCLK(hadc->Init.WorkClkSel, hadc->Init.WorkClkDiv);
    }
    /* Set ADC CFG */
    hadc->Instance->CFG &= ~(ADC_CFG_CLKSEL | ADC_CFG_ASLEEP | ADC_CFG_VCOMEN | ADC_CFG_TSLEEP);

    if (hadc->Init.AutoSleepEn == ADC_AUTOSLEEP_ENABLE) {
        assert_param(IS_ADC_AUTOSLEEP_EN(hadc->Init.AutoSleepEn));
        assert_param(IS_ADC_SLEEPWAIT_CYCLE(hadc->Init.SleepWaitTime));
        hadc->Instance->CFG |= hadc->Init.AutoSleepEn | ADC_SLEEP_TIME(hadc->Init.SleepWaitTime);
    }

    if (hadc->Init.VcomEn == ADC_VCOM_ENABLE) {
        assert_param(IS_ADC_VCOM_EN(hadc->Init.VcomEn));
        hadc->Instance->CFG |= hadc->Init.VcomEn;
    }

    hadc->Instance->CFG |= hadc->Init.ClkSel;

    /* Set ADC TIME */
    hadc->Instance->TIME    = 0x0;
    hadc->Instance->TIME    |= (ADC_SAMPLE_TIME(hadc->Init.SampleTime) | ADC_READY_TIME(hadc->Init.ReadyTime) | \
                                ADC_CONVERT_TIME(10) | ADC_SAMPLEHOLD_TIME(3));

    /* Set ADC WORK */
    hadc->Instance->WORK &= ~(ADC_WORK_MODE | ADC_WORK_ONESHOT | ADC_WORK_POLLINGNUM | ADC_WORK_SAMPLETIMEEN);

    if (hadc->Init.WorkMode == ADC_WORKMODE_POLLING) {
        assert_param(IS_ADC_POLLINGNUM(hadc->Init.PollingNum));

        if (hadc->Init.ConvertMode == ADC_CONVMODE_ONESHOT)
            hadc->Instance->WORK |= ADC_POLLING_NUM(hadc->Init.PollingNum + 1);
        else
            hadc->Instance->WORK |= ADC_POLLING_NUM(hadc->Init.PollingNum);
    }

    hadc->Instance->WORK |= hadc->Init.WorkMode | hadc->Init.ConvertMode | ADC_WORK_SAMPLETIMEEN;
}


/**
 * @}
 */

#endif /* ADC_MODULE_ENABLED */


/**
 * @}
 */


/**
 * @}
 */

/************************ (C) COPYRIGHT FudanMicroelectronics *****END OF FILE****/
