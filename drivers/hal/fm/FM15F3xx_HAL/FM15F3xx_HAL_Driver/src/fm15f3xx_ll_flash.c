/**
 ******************************************************************************
 * @file    fm15f3xx_ll_flash.c
 * @author  SRG
 * @version V1.0.0
 * @date    2020-03-17
 * @brief
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2020 FudanMicroelectronics.
 * All rights reserved.</center></h2>
 *
 ******************************************************************************
 */
#include "fm15f3xx_ll_flash.h"

#if !defined  (FLASH_OP_TIMEOUT)
#define FLASH_OP_TIMEOUT ( (uint32_t) 0xFFFFFFFF) /*!< Time out */
#endif /* FLASH_OP_TIMEOUT */


/**
 * @brief  Returns the FLASH Status.
 * @param  None
 * @retval FLASH Status: The returned value can be: FLASH_BUSY, FLASH_ERROR_RDCOL, FLASH_ERROR_ACC,
 *                        FLASH_ERROR_AUTH, FLASH_ERROR_LVT, FLASH_ERROR_COMM, FLASH_UNKNOWN or FLASH_COMPLETE.
 */
FLASH_Status FLASH_GetStatus(void)
{
    FLASH_Status flashstatus = FLASH_COMPLETE;

    if ((FLASH->CMDSTAT & LL_FLASH_DONE_FINISH) == 0x00) {
        flashstatus = FLASH_BUSY;
    } else if ((FLASH->CMDSTAT & LL_FLASH_RESULT_FAIL) != (uint32_t) 0x00) {
        //if((FLASH->CMDSTAT & LL_FLASH_Error_RDCOL) != (uint32_t)0x00)
        //{
        //  flashstatus = FLASH_ERROR_RDCOL;
        //}
        //else
        if ((FLASH->CMDSTAT & LL_FLASH_ERROR_ACC) != (uint32_t) 0x00) {
            flashstatus = FLASH_ERROR_ACC;
        } else if ((FLASH->CMDSTAT & LL_FLASH_ERROR_AUTH) != (uint32_t) 0x00) {
            flashstatus = FLASH_ERROR_AUTH;
        } else if ((FLASH->CMDSTAT & LL_FLASH_ERROR_LVT) != (uint32_t) 0x00) {
            flashstatus = FLASH_ERROR_LVT;
        } else if ((FLASH->CMDSTAT & LL_FLASH_ERROR_COMM) != (uint32_t) 0x00) {
            flashstatus = FLASH_ERROR_COMM;
        } else {
            flashstatus = FLASH_UNKNOWN;
        }
    } else {
        flashstatus = FLASH_COMPLETE;
    }

    /* Return the FLASH Status */
    return (flashstatus);
}


/**
 * @brief  Waits for a FLASH operation to complete.
 * @param  None
 * @retval FLASH Status: The returned value can be: FLASH_BUSY, FLASH_ERROR_PROGRAM,
 *                       FLASH_ERROR_WRP, FLASH_ERROR_OPERATION or FLASH_COMPLETE.
 */
FLASH_Status FLASH_WaitForLastOperation(void)
{
    __IO FLASH_Status   status          = FLASH_COMPLETE;
    __IO uint32_t       TimeOutCounter  = 0;


    /* Wait for the FLASH operation to complete by polling on BUSY flag to be reset.
       Even if the FLASH operation fails, the BUSY flag will be reset and an error
       flag will be set */
    do {
        status = FLASH_GetStatus();
        TimeOutCounter++;
    } while ((status == FLASH_BUSY) && (TimeOutCounter != FLASH_OP_TIMEOUT));

    if (TimeOutCounter == FLASH_OP_TIMEOUT)
        status = FLASH_TIMEOUT;
    /* Return the operation status */
    return (status);
}


FLASH_Status LL_FLASH_Erase1Sector(uint32_t Addr)
{
    __IO FLASH_Status   status  = FLASH_COMPLETE;
    uint32_t            * bp    = (uint32_t *) Addr;
    uint32_t            i;
    volatile uint32_t   tmp;

    LL_FLASH_SetEraseVReadl(FLASH, LL_FLASH_ERASEVREADL_VERIFY);    //Check empty after erase
    LL_FLASH_SetVRDVread(FLASH, LL_FLASH_VRDVREAD_DISABLE);
    LL_FLASH_SetEraseWay(FLASH, LL_FLASH_ERASEWAY_RETRY);           //LL_FLASH_EraseWay_Retry;LL_FLASH_EraseWay_Single

    for (i = 0; i < 3; i++) {
        LL_FLASH_SetEraseRetry(FLASH, LL_FLASH_ERASERETRY(i));

        LL_FLASH_SetCMDEn(FLASH, LL_FLASH_CMD_ENABLE);
        LL_FLASH_SetCMD(FLASH, LL_FLASH_COMM_EREASCMD);
        LL_FLASH_SetCMDAddr(FLASH, LL_FLASH_CMDADDR(Addr));
        LL_FLASH_ClearFlag_Done(FLASH, LL_FLASH_DONE_FINISH);
        LL_FLASH_ClearFlag_Done(FLASH, LL_FLASH_DONE_FINISH);

        *bp = 0x0;
        __DSB();

        /* Wait till Flash operation is completed and if Time out is reached exit */
        status = FLASH_WaitForLastOperation();

        /* Flush the data cache */
        LL_Data_Cache_Flush();

        if (FLASH_COMPLETE == status)
            return (status);
    }

    status = LL_FLASH_Erase1Sector_Single(Addr);

    return (status);
}


FLASH_Status LL_FLASH_Erase1Sector_Single(uint32_t Addr)
{
    __IO FLASH_Status   status  = FLASH_COMPLETE;
    uint32_t            * bp    = (uint32_t *) Addr;
    volatile uint32_t   tmp;

    LL_FLASH_SetEraseVReadl(FLASH, LL_FLASH_ERASEVREADL_VERIFY);   //Check empty after erase
    LL_FLASH_SetVRDVread(FLASH, LL_FLASH_VRDVREAD_DISABLE);
    LL_FLASH_SetEraseRetry(FLASH, LL_FLASH_ERASERETRY(0x3));
    LL_FLASH_SetEraseWay(FLASH, LL_FLASH_ERASEWAY_SINGLE);

    LL_FLASH_SetCMDEn(FLASH, LL_FLASH_CMD_ENABLE);
    LL_FLASH_SetCMD(FLASH, LL_FLASH_COMM_EREASCMD);
    LL_FLASH_SetCMDAddr(FLASH, LL_FLASH_CMDADDR(Addr));
    LL_FLASH_ClearFlag_Done(FLASH, LL_FLASH_DONE_FINISH);
    LL_FLASH_ClearFlag_Done(FLASH, LL_FLASH_DONE_FINISH);

    *bp = 0x0;
    __DSB();

    /* Wait till Flash operation is completed and if Time out is reached exit */
    status = FLASH_WaitForLastOperation();

    /* Flush the data cache */
    LL_Data_Cache_Flush();

    return (status);
}


FLASH_Status LL_FLASH_Prog1Word(uint32_t Addr, uint32_t Data, uint8_t ReadCheckEn)
{
    __IO FLASH_Status   status  = FLASH_COMPLETE;
    uint32_t            * bp    = (uint32_t *) Addr;

    LL_FLASH_SetProgramSize(FLASH, LL_FLASH_PROGRAMSIZE_1WORD);
    LL_FLASH_SetCMDEn(FLASH, LL_FLASH_CMD_ENABLE);
    LL_FLASH_SetCMD(FLASH, LL_FLASH_COMM_PROGRAMCMD);
    LL_FLASH_SetCMDAddr(FLASH, LL_FLASH_CMDADDR(Addr));
    LL_FLASH_SetCMDPARAn(FLASH, 0, Data);
    LL_FLASH_ClearFlag_Done(FLASH, LL_FLASH_DONE_FINISH);
    LL_FLASH_ClearFlag_Done(FLASH, LL_FLASH_DONE_FINISH);

    *bp = 0x0;
    __DSB();

    /* Wait till Flash operation is completed and if Time out is reached exit */
    status = FLASH_WaitForLastOperation();

    /* Flush the data cache */
    LL_Data_Cache_Flush();

    if (status == FLASH_COMPLETE) {
        if (ReadCheckEn == LL_FLASH_READCHECK_ENABLE) {
            if ((*bp) != Data)
                status = FLASH_ERROR_NORAML_READ;
        }
    }

    return (status);
}


FLASH_Status LL_FLASH_Prog4Word(uint32_t Addr, uint32_t Data_buf[4], uint8_t ReadCheckEn)
{
    __IO FLASH_Status   status  = FLASH_COMPLETE;
    uint8_t             i       = 0;
    uint32_t            * bp    = (uint32_t *) Addr;

    LL_FLASH_SetProgramSize(FLASH, LL_FLASH_PROGRAMSIZE_4WORD);
    LL_FLASH_SetCMDEn(FLASH, LL_FLASH_CMD_ENABLE);
    LL_FLASH_SetCMD(FLASH, LL_FLASH_COMM_PROGRAMCMD);
    LL_FLASH_SetCMDAddr(FLASH, LL_FLASH_CMDADDR(Addr));
    LL_FLASH_SetCMDPARAn(FLASH, 0, Data_buf[0]);
    LL_FLASH_SetCMDPARAn(FLASH, 1, Data_buf[1]);
    LL_FLASH_SetCMDPARAn(FLASH, 2, Data_buf[2]);
    LL_FLASH_SetCMDPARAn(FLASH, 3, Data_buf[3]);
    LL_FLASH_ClearFlag_Done(FLASH, LL_FLASH_DONE_FINISH);
    LL_FLASH_ClearFlag_Done(FLASH, LL_FLASH_DONE_FINISH);
    *bp = 0x0;
    __DSB();

    /* Wait till Flash operation is completed and if Time out is reached exit */
    status = FLASH_WaitForLastOperation();

    /* Flush the data cache */
    LL_Data_Cache_Flush();

    if (status == FLASH_COMPLETE) {
        if (ReadCheckEn == LL_FLASH_READCHECK_ENABLE) {
            for (i = 0; i < 4; i++) {
                if ((*(bp + i)) != Data_buf[i]) {
                    status = FLASH_ERROR_NORAML_READ;
                    break;
                }
            }
        }
    }

    return (status);
}


FLASH_Status LL_FLASH_Prog32Word(uint32_t Addr, uint32_t Data_buf[32], uint8_t ReadCheckEn)
{
    __IO FLASH_Status   status = FLASH_COMPLETE;
    uint32_t            i;
    uint32_t            * bp = (uint32_t *) Addr;

    LL_FLASH_SetProgramSize(FLASH, LL_FLASH_PROGRAMSIZE_32WORD);
    LL_FLASH_SetCMDEn(FLASH, LL_FLASH_CMD_ENABLE);
    LL_FLASH_SetCMD(FLASH, LL_FLASH_COMM_PROGRAMCMD);
    LL_FLASH_SetCMDAddr(FLASH, LL_FLASH_CMDADDR(Addr));
    for (i = 0; i < 32; i++) {
        LL_FLASH_SetCMDPARAn(FLASH, i, Data_buf[i]);
    }
    LL_FLASH_ClearFlag_Done(FLASH, LL_FLASH_DONE_FINISH);
    LL_FLASH_ClearFlag_Done(FLASH, LL_FLASH_DONE_FINISH);
    *bp = 0x0;
    __DSB();

    /* Wait till Flash operation is completed and if Time out is reached exit */
    status = FLASH_WaitForLastOperation();

    /* Flush the data cache */
    LL_Data_Cache_Flush();

    if (status == FLASH_COMPLETE) {
        if (ReadCheckEn == LL_FLASH_READCHECK_ENABLE) {
            for (i = 0; i < 32; i++) {
                if ((*(bp + i)) != Data_buf[i]) {
                    status = FLASH_ERROR_NORAML_READ;
                    break;
                }
            }
        }
    }

    return (status);
}


FLASH_Status LL_FLASH_4WordProDataCheck(uint32_t addr, uint32_t Data_buf[4])
{
    __IO FLASH_Status   status = FLASH_COMPLETE;
    uint32_t            *tmp_bp;
    volatile uint32_t   tmp;
    tmp_bp = (uint32_t *) addr;

    LL_FLASH_SetCMDEn(FLASH, LL_FLASH_CMD_ENABLE);
    LL_FLASH_SetCMD(FLASH, LL_FLASH_COMM_PROGDATACHECKCMD);
    LL_FLASH_SetCMDAddr(FLASH, LL_FLASH_CMDADDR(addr));

    LL_FLASH_SetCMDPARAn(FLASH, 0, Data_buf[0]);
    LL_FLASH_SetCMDPARAn(FLASH, 1, Data_buf[1]);
    LL_FLASH_SetCMDPARAn(FLASH, 2, Data_buf[2]);
    LL_FLASH_SetCMDPARAn(FLASH, 3, Data_buf[3]);
    LL_FLASH_ClearFlag_Done(FLASH, LL_FLASH_DONE_FINISH);
    LL_FLASH_ClearFlag_Done(FLASH, LL_FLASH_DONE_FINISH);
    tmp = *tmp_bp;

    /* Wait till Flash operation is completed and if Time out is reached exit */
    status = FLASH_WaitForLastOperation();

    return (status);
}


FLASH_Status LL_FLASH_EnCry_4WordBlankCheck(uint32_t Addr)
{
    uint16_t            i       = 0;
    uint32_t            * bp    = (uint32_t *) Addr;
    volatile uint32_t   tmp;

    ALM_MONITOR->GROUPC0    = 0x00000000;
    ALM_MONITOR->ALMRCDCLR  = 0x03;

    LL_FLASH_SetEccCrcEn(FLASH, LL_FLASH_ECCRC_ENABLE);
    ALM_MONITOR->ALMRCDCLR = 0x03;

    for (i = 0; i < 3000; i++) {
        tmp = *bp;
    }
    if ((ALM_MONITOR->ALMRCDC & 0x08) == 0x08)
        return (CHECK_BLANK);
    else
        return (CHECK_NOT_BLANK);
}


/************************ (C) COPYRIGHT FudanMicroelectronics *****END OF FILE****/
