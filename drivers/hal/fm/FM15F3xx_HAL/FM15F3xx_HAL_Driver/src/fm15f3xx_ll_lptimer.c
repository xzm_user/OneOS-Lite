/**
 ******************************************************************************
 * @file    fm15f3xx_ll_lptimer.c
 * @author  SRG
 * @version V1.0.0
 * @date    2020-03-17
 * @brief
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2020 FudanMicroelectronics.
 * All rights reserved.</center></h2>
 *
 ******************************************************************************
 */
#include "fm15f3xx_ll_lptimer.h"


void LL_LPTIM_Init(LL_LPTIM_InitTypeDef *LPTIM_InitStruct)
{
    LL_LPTIM_SetClkSrc(LPTIM_InitStruct->ClockSel);

    if (DISABLE == LPTIM_InitStruct->IntEn)
        LL_LPTIM_DisableINT();
    else
        LL_LPTIM_EnableINT();

    if (DISABLE == LPTIM_InitStruct->PrescaleEn)
        LL_LPTIM_DisablePrescaler();
    else {
        LL_LPTIM_EnablePrescaler();
        LL_LPTIM_SetPrescaler(LPTIM_InitStruct->PrescaleCfg);
    }

    if (LL_LPTIM_CLK_SEL_PADCLK == LPTIM_InitStruct->ClockSel)
        LL_LPTIM_ConfigPadClkPin(LPTIM_InitStruct->PadClkSel);

    LL_LPTIM_SetRunMode(LPTIM_InitStruct->RunMode);
    LL_LPTIM_SetCountMode(LPTIM_InitStruct->FreeMode);

    if (DISABLE == LPTIM_InitStruct->InPolarEn)
        LL_LPTIM_DisableTrigInv();
    else
        LL_LPTIM_EnableTrigInv();

    if (LL_LPTIM_RUN_MODE_PULSE == LPTIM_InitStruct->RunMode)
        LL_LPTIM_SetTrigSrc(LPTIM_InitStruct->TriggerSource);

    LL_LPTIM_SetModValue(LPTIM_InitStruct->Mod);
}


/************************ (C) COPYRIGHT FudanMicroelectronics *****END OF FILE****/

