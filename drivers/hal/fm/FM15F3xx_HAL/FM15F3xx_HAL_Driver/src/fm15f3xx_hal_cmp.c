/**
 ******************************************************************************
 * @file    fm15f3xx_hal_cmp.c
 * @author  MCD Application Team
 * @brief   CMP HAL module driver.
 *         This file provides firmware functions to manage the following
 *         functionalities of the Comparator (CMP) peripheral:
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2020 FudanMicroelectronics.
 * All rights reserved.</center></h2>
 *
 ******************************************************************************
 */


/* Includes ------------------------------------------------------------------*/
#include "fm15f3xx_hal.h"


/** @addtogroup FM15F3xx_HAL_Driver
 * @{
 */


/** @defgroup CMP CMP
 * @brief CMP driver modules
 * @{
 */

#ifdef CMP_MODULE_ENABLED

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/


/** @addtogroup CMP_Private_Functions
 * @{
 */
/* Private function prototypes -----------------------------------------------*/
static void __CMP_Config(CMP_HandleTypeDef *hcmp);


/**
 * @}
 */

/* Exported functions --------------------------------------------------------*/


/** @defgroup CMP_Exported_Functions CMP Exported Functions
 * @{
 */


/** @defgroup CMP_Exported_Functions_Group1 Initialization and de-initialization functions
 *  @brief    Initialization and Configuration functions
 *
   @verbatim
   ==============================================================================
 ##### Initialization and de-initialization functions #####
   ==============================================================================
    [..]  This section provides functions allowing to:
      (+) Initialize and configure the CMP.
      (+) De-initialize the CMP.

   @endverbatim
 * @{
 */


/**
 * @brief  Initializes the CMP peripheral according to the specified parameters
 *         in the CMP_InitStruct.
 * @param  hcmp pointer to a CMP_HandleTypeDef structure that contains
 *         the configuration information for the specified CMP.
 * @retval HAL status
 */
HAL_StatusTypeDef HAL_CMP_Init(CMP_HandleTypeDef* hcmp)
{
    /* Check CMP handle */
    if (hcmp == NULL) {
        return (HAL_ERROR);
    }
    /* Check the parameters */
    assert_param(IS_CMP_INSTANCE(hcmp->Instance));

    if (hcmp->State == HAL_CMP_STATE_RESET) {
        /* Init the low level hardware */
        HAL_CMP_MspInit(hcmp);
    }

    /* Set CMP parameters */
    __CMP_Config(hcmp);

    /* Set CMP error code to none */
    hcmp->ErrorCode = HAL_CMP_ERROR_NONE;

    /* Initialize the CMP state*/
    hcmp->State = HAL_CMP_STATE_READY;

    /* Return function status */
    return (HAL_OK);
}


/**
 * @brief  Deinitializes the CMP peripheral registers to their default reset values.
 * @param  hcmp pointer to a CMP_HandleTypeDef structure that contains
 *         the configuration information for the specified CMP.
 * @retval HAL status
 */
HAL_StatusTypeDef HAL_CMP_DeInit(CMP_HandleTypeDef* hcmp)
{
    /* Check CMP handle */
    if (hcmp == NULL) {
        return (HAL_ERROR);
    }

    /* Check the parameters */
    assert_param(IS_CMP_INSTANCE(hcmp->Instance));

    /* DeInit the low level hardware */
    HAL_CMP_MspDeInit(hcmp);

    /* Set CMP error code to none */
    hcmp->ErrorCode = HAL_CMP_ERROR_NONE;

    /* Change CMP state */
    hcmp->State = HAL_CMP_STATE_RESET;

    /* Return function status */
    return (HAL_OK);
}


/**
 * @brief  Initializes the CMP MSP.
 * @param  hcmp pointer to a CMP_HandleTypeDef structure that contains
 *         the configuration information for the specified CMP.
 * @retval None
 */
__weak void HAL_CMP_MspInit(CMP_HandleTypeDef* hcmp)
{
    /* Prevent unused argument(s) compilation warning */
    UNUSED(hcmp);


    /* NOTE : This function Should not be modified, when the callback is needed,
              the HAL_CMP_MspInit could be implemented in the user file
     */
}


/**
 * @brief  DeInitializes the CMP MSP.
 * @param  hcmp pointer to a CMP_HandleTypeDef structure that contains
 *         the configuration information for the specified CMP.
 * @retval None
 */
__weak void HAL_CMP_MspDeInit(CMP_HandleTypeDef* hcmp)
{
    __HAL_CMP_DISABLE(hcmp);
}


/**
 * @}
 */


/** @defgroup CMP_Exported_Functions_Group2 IO operation functions
 *  @brief    IO operation functions
 *
   @verbatim
   ==============================================================================
 ##### IO operation functions #####
   ==============================================================================
   @endverbatim
 * @{
 */


/**
 * @brief  Enables CMP.
 * @param  hcmp pointer to a CMP_HandleTypeDef structure that contains
 *         the configuration information for the specified CMP.
 * @retval HAL status
 */
HAL_StatusTypeDef HAL_CMP_Start(CMP_HandleTypeDef* hcmp)
{
    if (!__HAL_CMP_IS_ENABLE(hcmp)) {
        /* Enable the Peripheral */
        __HAL_CMP_ENABLE(hcmp);
    }

    /* Change CMP state */
    hcmp->State = HAL_CMP_STATE_READY;

    /* if enable software conversion*/
    if (__HAL_CMP_IS_SOFTWARETRIG(hcmp)) {
        /* Enable the selected ADC software conversion */
        hcmp->Instance->SWTRIG = ENABLE;
    }

    /* Return function status */
    return (HAL_OK);
}


/**
 * @brief  Disables CMP.
 * @param  hcmp pointer to a CMP_HandleTypeDef structure that contains
 *         the configuration information for the specified CMP.
 * @retval HAL status
 */
HAL_StatusTypeDef HAL_CMP_Stop(CMP_HandleTypeDef* hcmp)
{
    /* Disable the Peripheral */
    __HAL_CMP_DISABLE(hcmp);

    /* Change CMP state */
    hcmp->State = HAL_CMP_STATE_READY;

    /* Return function status */
    return (HAL_OK);
}


/**
 * @brief  Enables CMP and INT.
 * @param  hcmp pointer to a CMP_HandleTypeDef structure that contains
 *         the configuration information for the specified CMP.
 * @param  Interrupt flag can be combination of the following values:
 *         @arg @ref CMP_ITMODE_HIGH
 *         @arg @ref CMP_ITMODE_RISING
 *         @arg @ref CMP_ITMODE_FALLING
 *         @arg @ref CMP_ITMODE_BOTHEDGE
 * @retval HAL status
 */
HAL_StatusTypeDef HAL_CMP_Start_IT(CMP_HandleTypeDef *hcmp, uint32_t ITmode)
{
    /* Enable IT and Config IT mode */
    __HAL_CMP_ENABLE_IT(hcmp, ITmode);

    if (!__HAL_CMP_IS_ENABLE(hcmp)) {
        /* Enable the Peripheral */
        __HAL_CMP_ENABLE(hcmp);
    }
    /* Change CMP state */
    hcmp->State = HAL_CMP_STATE_READY;

    /* Return function status */
    return (HAL_OK);
}


/**
 * @brief  Disables CMP and INT.
 * @param  hcmp pointer to a CMP_HandleTypeDef structure that contains
 *         the configuration information for the specified CMP.
 * @retval HAL status
 */
HAL_StatusTypeDef HAL_CMP_Stop_IT(CMP_HandleTypeDef *hcmp)
{
    /* Disable the Peripheral */
    __HAL_CMP_DISABLE_IT(hcmp);

    /* Disable the Peripheral */
    __HAL_CMP_DISABLE(hcmp);

    /* Change CMP state */
    hcmp->State = HAL_CMP_STATE_READY;

    /* Return function status */
    return (HAL_OK);
}


/**
 * @brief  Handles CMP interrupt request
 * @param  hcmp pointer to a CMP_HandleTypeDef structure that contains
 *         the configuration information for the specified CMP.
 * @retval None
 */
void HAL_CMP_IRQHandler(CMP_HandleTypeDef *hcmp)
{
    HAL_CMP_CpltCallback(hcmp);
    __HAL_CMP_CLEAR_RCDFLAG(hcmp, CMP_FLAG_RCD);
}


/**
 * @brief  Comparer complete callback in non blocking mode.
 * @param  hcmp pointer to a CMP_HandleTypeDef structure that contains
 *         the configuration information for the specified CMP.
 * @retval None
 */
__weak void HAL_CMP_CpltCallback(CMP_HandleTypeDef *hcmp)
{
    /* Prevent unused argument(s) compilation warning */
    UNUSED(hcmp);


    /* NOTE : This function Should not be modified, when the callback is needed,
              the HAL_CMP_CmpCpltCallback could be implemented in the user file
     */
}


/**
 * @}
 */


/** @defgroup CMP_Exported_Functions_Group3 Peripheral Control functions
 *  @brief      Peripheral Control functions
 *
   @verbatim
   ==============================================================================
 ##### Peripheral Control functions #####
   ==============================================================================
   @endverbatim
 * @{
 */


/**
 * @brief  Get the CMP output.
 * @param  hcmp pointer to a CMP_HandleTypeDef structure that contains
 *         the configuration information for the specified CMP.
 * @param  ResultMode This parameter can be one of the following values:
 *         @arg @ref CMP_RESULT_DIGITAL
 *         @arg @ref CMP_RESULT_ANALOG
 * @retval HAL status
 */
uint8_t HAL_CMP_GetResult(CMP_HandleTypeDef *hcmp, uint8_t ResultMode)
{
    uint32_t tmp;
    tmp = hcmp->Instance->STATUS;

    if (CMP_RESULT_DIGITAL == ResultMode) {
        tmp &= CMP_STATUS_RESULT;
    } else {
        tmp &= CMP_STATUS_ANAOUT;
        tmp = tmp >> CMP_STATUS_ANAOUT_Pos;
    }
    /* Return result */
    return (tmp);
}


/**
 * @brief  Initializes the CMP 6BitDAC peripheral according to the specified parameters
 *         in the DACInitStruct.
 * @param  DAC_InitStruct pointer to a CMP_6BitDACInitTypeDef structure that contains
 *         the configuration information for the specified 6BitDAC.
 */
void HAL_CMP_6BitDacInit(CMP_6BitDACInitTypeDef* DAC_InitStruct)
{
    assert_param(IS_CMP_6BITDAC(DAC_InitStruct->VrefSel));
    assert_param(IS_CMP_6BITDACVALUE(DAC_InitStruct->Value));

    WRITE_REG(CMP0->DACDATA, DAC_InitStruct->Value);
    MODIFY_REG(CMP0->DAC, CMP_DAC_VREFSEL, DAC_InitStruct->VrefSel | CMP_DAC_DACEN);
}


/**
 * @brief  Deinitializes the CMP 6BitDAC peripheral according to the specified parameters
 *         in the DACInitStruct.
 * @param  DAC_InitStruct pointer to a CMP_6BitDACInitTypeDef structure that contains
 *         the configuration information for the specified 6BitDAC.
 */
void HAL_CMP_6BitDacDeInit(void)
{
    CLEAR_BIT(CMP0->DAC, CMP_DAC_DACEN);
}


/**
 * @}
 */


/** @defgroup CMP_Exported_Functions_Group4 Peripheral State and Errors functions
 *  @brief   Peripheral State and Errors functions
 *
   @verbatim
   ==============================================================================
 ##### Peripheral State and Errors functions #####
   ==============================================================================
   @endverbatim
 * @{
 */


/**
 * @brief  return the CMP state
 * @param  hcmp pointer to a CMP_HandleTypeDef structure that contains
 *         the configuration information for the specified CMP.
 * @retval HAL state
 */
HAL_CMP_StateTypeDef HAL_CMP_GetState(CMP_HandleTypeDef *hcmp)
{
    /* Return CMP state */
    return (hcmp->State);
}


/**
 * @brief  Return the CMP error code
 * @param  hcmp pointer to a CMP_HandleTypeDef structure that contains
 *         the configuration information for the specified CMP.
 * @retval CMP Error Code
 */
uint32_t HAL_CMP_GetError(CMP_HandleTypeDef *hcmp)
{
    return (hcmp->ErrorCode);
}


/**
 * @brief  Configures the CMP.
 * @param  hcmp pointer to a CMP_HandleTypeDef structure that contains
 *         the configuration information for the specified CMP.
 * @retval HAL status
 */
static void __CMP_Config(CMP_HandleTypeDef *hcmp)
{
    /* Disable the Peripheral */
    __HAL_CMP_DISABLE(hcmp);

    /* Check the parameters */
    assert_param(IS_CMP_SPEEDMODE(hcmp->Init.SpeedMode));
    assert_param(IS_CMP_HYSTERMODE(hcmp->Init.HysterMode));
    assert_param(IS_CMP_READYTIME(hcmp->Init.ReadyTime));
    /* Config CTRL Reg */
    MODIFY_REG(hcmp->Instance->CTRL, CMP_CTRL_HPEN | CMP_CTRL_HYSTER,
               hcmp->Init.SpeedMode | hcmp->Init.HysterMode | (hcmp->Init.ReadyTime << CMP_CTRL_RDYTIME_Pos));

    /* Config IN Reg */
    assert_param(IS_CMP_INPUT(hcmp->Init.InputP));
    assert_param(IS_CMP_INPUT(hcmp->Init.InputN));
    MODIFY_REG(hcmp->Instance->IN, CMP_INSEL_INPSEL | CMP_INSEL_INNSEL, hcmp->Init.InputP | (hcmp->Init.InputN << CMP_INSEL_INNSEL_Pos));

    if (CMP_INPUT_VREF12 == hcmp->Init.InputP || CMP_INPUT_VREF12 == hcmp->Init.InputN) {
        __HAL_CMP_ENABLE_VREF12();
    }

    /* Config TRIG Reg */
    assert_param(IS_CMP_WORKMODE(hcmp->Init.WorkMode));
    assert_param(IS_CMP_TRIGSOURCE(hcmp->Init.TrigSource));
    assert_param(IS_CMP_TRIGINV(hcmp->Init.TrigInvEn));
    assert_param(IS_CMP_TRIGEDGE(hcmp->Init.TrigEdge));
    MODIFY_REG(hcmp->Instance->TRIG, CMP_TRIGCFG_MODE | CMP_TRIGCFG_INV | CMP_TRIGCFG_EDGE | CMP_TRIGCFG_SC,
               hcmp->Init.WorkMode | hcmp->Init.TrigSource | hcmp->Init.TrigInvEn | hcmp->Init.TrigEdge);

    /* Config OUT Reg */
    assert_param(IS_CMP_WINDOWS(hcmp->Init.WindowsEn));
    assert_param(IS_CMP_FILTER(hcmp->Init.Filter));
    assert_param(IS_CMP_OUTPUTINV(hcmp->Init.OutputInvEn));
    MODIFY_REG(hcmp->Instance->OUT, CMP_INTEN_INTDMAEN | CMP_INTEN_DMASEL | CMP_INTEN_MODE,
               hcmp->Init.WindowsEn | hcmp->Init.Filter | hcmp->Init.OutputInvEn);
}


/**
 * @}
 */


/**
 * @}
 */

#endif /* CMP_MODULE_ENABLED */


/**
 * @}
 */


/**
 * @}
 */

/************************ (C) COPYRIGHT FudanMicroelectronics *****END OF FILE****/
