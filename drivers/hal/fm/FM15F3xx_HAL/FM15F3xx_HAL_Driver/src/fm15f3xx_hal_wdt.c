/**
 ******************************************************************************
 * @file    fm15f3xx_hal_wdt.c
 * @author  MCD Application Team
 * @brief   WDT HAL module driver.
 *          This file provides firmware functions to manage the following
 *          functionalities of the Watchdog (WDT) peripheral:
 *           + Initialization and Start functions
 *           + IO operation functions
 *
 ******************************************************************************
 * @attention
 * <h2><center>&copy; COPYRIGHT(c) 2020 FudanMicroelectronics</center></h2>
 *
 ******************************************************************************
 */


/* Includes ------------------------------------------------------------------*/
#include "fm15f3xx_hal.h"


/** @addtogroup FM15F3xx_HAL_Driver
 * @{
 */

#ifdef WDT_MODULE_ENABLED


/** @defgroup WDT WDT
 * @brief WDT HAL module driver.
 * @{
 */

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/


/** @defgroup WDT_Private_Defines WDT Private Defines
 * @{
 */


/**
 * @}
 */

/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
/* Exported functions --------------------------------------------------------*/


/** @addtogroup WDT_Exported_Functions
 * @{
 */


/** @addtogroup WDT_Exported_Functions_Group1
 *  @brief    Initialization and Start functions.
 *
   @verbatim
   ===============================================================================
 ##### Initialization and Start functions #####
   ===============================================================================
   [..]  This section provides functions allowing to:
      (+) Initialize the WDT according to the specified parameters in the
          WDT_InitTypeDef of associated handle.
      (+) Once initialization is performed in HAL_WDT_Init function, Watchdog
          is reloaded in order to exit function with correct time base.

   @endverbatim
 * @{
 */


/**
 * @brief  Initialize the WDT according to the specified parameters in the
 *         WDT_InitTypeDef and start watchdog. Before exiting function,
 *         watchdog is refreshed in order to have correct time base.
 * @param  hiwdg  pointer to a WDT_HandleTypeDef structure that contains
 *                the configuration information for the specified WDT module.
 * @retval HAL status
 */
HAL_StatusTypeDef HAL_WDT_Init(WDT_HandleTypeDef *hwdt)
{
    /* Check the WDT handle allocation */
    if (hwdt == NULL) {
        return (HAL_ERROR);
    }

    /* Check the parameters */
    assert_param(IS_WDT_INSTANCE(hwdt->Instance));
    assert_param(IS_WDT_PERIOD(hwdt->Init.Period));

    /* Reset WDT Counter*/
    __HAL_WDT_CNT_RESET(hwdt);

    /* Write to WDT registers the Period values to work with */
    __HAL_WDT_SET_PERIOD(hwdt, hwdt->Init.Period);

    /* Return function status */
    return (HAL_OK);
}


/**
 * @}
 */


/** @addtogroup WDT_Exported_Functions_Group2
 *  @brief   IO operation functions
 *
   @verbatim
   ===============================================================================
 ##### IO operation functions #####
   ===============================================================================
   [..]  This section provides functions allowing to:
      (+) Refresh the WDT.

   @endverbatim
 * @{
 */


/**
 * @brief  Start the WDT.
 * @param  hiwdg  pointer to a WDT_HandleTypeDef structure that contains
 *                the configuration information for the specified WDT module.
 * @retval HAL status
 */
HAL_StatusTypeDef HAL_WDT_Start(WDT_HandleTypeDef *hwdt)
{
    /* Enable WDT is turned on automaticaly */
    __HAL_WDT_START(hwdt);

    /* Return function status */
    return (HAL_OK);
}


/**
 * @brief  Get Count Value.
 * @param  hiwdg  pointer to a WDT_HandleTypeDef structure that contains
 *                the configuration information for the specified WDT module.
 * @retval Count Value
 */
uint32_t HAL_WDT_GetCountValue(WDT_HandleTypeDef *hwdt)
{
    return (hwdt->Instance->CNT);
}


/**
 * @brief  Refresh the WDT.
 * @param  hiwdg  pointer to a WDT_HandleTypeDef structure that contains
 *                the configuration information for the specified WDT module.
 * @retval HAL status
 */
HAL_StatusTypeDef HAL_WDT_Refresh(WDT_HandleTypeDef *hwdt)
{
    /* Reload WDT counter with value defined in the reload register */
    __HAL_WDT_CNT_RESET(hwdt);

    /* Return function status */
    return (HAL_OK);
}


/**
 * @}
 */


/**
 * @}
 */

#endif /* HAL_WDT_MODULE_ENABLED */


/**
 * @}
 */


/**
 * @}
 */

/************************ (C) COPYRIGHT FudanMicroelectronics *****END OF FILE****/
