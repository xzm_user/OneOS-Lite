/**
 ******************************************************************************
 * @file    fm15f3xx_ll_cmp.c
 * @author  SRG
 * @version V1.0.0
 * @date    2020-03-17
 * @brief
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2020 FudanMicroelectronics.
 * All rights reserved.</center></h2>
 *
 ******************************************************************************
 */
#include "fm15f3xx_ll_cmp.h"


/* CMP registers Masks */
#define LL_CMP_CTRL_CLEAR_MASK  (CMP_CTRL_CMPEN | CMP_CTRL_HPEN | CMP_CTRL_HYSTER)
#define LL_CMP_IN_CLEAR_MASK    (CMP_INSEL_INPSEL | CMP_INSEL_INNSEL)
#define LL_CMP_DAC_CLEAR_MASK   (CMP_DAC_DACEN | CMP_DAC_VREFSEL)
#define LL_CMP_TRIG_CLEAR_MASK  (CMP_TRIGCFG_MODE | CMP_TRIGCFG_INV | CMP_TRIGCFG_EDGE | CMP_TRIGCFG_SC)
#define LL_CMP_OUT_CLEAR_MASK   (CMP_OUTCTRL_INV | CMP_OUTCTRL_WINEN | CMP_OUTCTRL_WINSC | CMP_OUTCTRL_FILTEN | CMP_OUTCTRL_FILTCNT)
#define LL_CMP_INTEN_CLEAR_MASK (CMP_INTEN_INTDMAEN | CMP_INTEN_DMASEL | CMP_INTEN_MODE)


/**
 * @brief  Set each @ref LL_CMP_InitTypeDef field to default value.
 * @param  CMP_InitStruct pointer to a @ref LL_CMP_InitTypeDef structure
 * whose fields will be set to default values.
 * @retval None
 */
void LL_CMP_StructInit(LL_CMP_InitTypeDef *CMP_InitStruct)
{
    /* Set CMP_InitStruct fields to default values */
    CMP_InitStruct->SpeedMode       = LL_CMP_SPEED_LOW;
    CMP_InitStruct->HysterLevel     = LL_CMP_HYSTER_LEVEL0;
    CMP_InitStruct->PositiveInput   = LL_CMP_POSITIVE_CMP_IN2;
    CMP_InitStruct->NegativeInput   = LL_CMP_NEGATIVE_CMP_IN3;

    CMP_InitStruct->SixBitDACEn     = LL_CMP_6BITDAC_DISABLE;
    CMP_InitStruct->SixBitDACVref   = LL_CMP_6BITDAC_VREFVDD;
    CMP_InitStruct->TrigMode        = LL_CMP_TRIGMODE_REALTIME;
    CMP_InitStruct->TrigSource      = LL_CMP_TRIGSRC_CMP_INx;
    CMP_InitStruct->TrigSourceDir   = LL_CMP_TRIGINPUT_NORMAL;
    CMP_InitStruct->TrigEdge        = LL_CMP_TRIGEDGE_HIGH;
    CMP_InitStruct->WindowEn        = LL_CMP_WINDOW_DISABLE;
    CMP_InitStruct->WindowSource    = LL_CMP_WINSOURCE_STIMER0;
    CMP_InitStruct->FilterEn        = LL_CMP_FILTER_DISABLE;
    CMP_InitStruct->FilterCnt       = LL_CMP_FILTERCNT_1CLK;
    CMP_InitStruct->DMAorINT        = LL_CMP_REQSEL_NONE;
    CMP_InitStruct->RecordTrigMode  = LL_CMP_RCDMODE_HIGH;
}


void LL_CMP_Init(CMP_TypeDef *CMPx, LL_CMP_InitTypeDef *CMP_InitStruct)
{
    LL_CMP_Disable(CMPx);

    MODIFY_REG(CMPx->CTRL,
               LL_CMP_CTRL_CLEAR_MASK,
               CMP_InitStruct->SpeedMode | CMP_InitStruct->HysterLevel);

    MODIFY_REG(CMPx->IN,
               LL_CMP_IN_CLEAR_MASK,
               CMP_InitStruct->PositiveInput | CMP_InitStruct->NegativeInput);

    MODIFY_REG(CMPx->DAC,
               LL_CMP_DAC_CLEAR_MASK,
               CMP_InitStruct->SixBitDACVref | CMP_InitStruct->SixBitDACEn);

    MODIFY_REG(CMPx->TRIG,
               LL_CMP_TRIG_CLEAR_MASK,
               CMP_InitStruct->TrigMode | CMP_InitStruct->TrigSource | CMP_InitStruct->TrigSourceDir |
               CMP_InitStruct->TrigEdge);

    MODIFY_REG(CMPx->OUT,
               LL_CMP_OUT_CLEAR_MASK,
               CMP_InitStruct->WindowEn | CMP_InitStruct->WindowSource | CMP_InitStruct->FilterEn |
               CMP_InitStruct->FilterCnt);

    MODIFY_REG(CMPx->INTEN,
               LL_CMP_INTEN_CLEAR_MASK,
               CMP_InitStruct->DMAorINT | CMP_InitStruct->RecordTrigMode);
}


void LL_CMP_DeInit(CMP_TypeDef *CMPx)
{
    LL_CMP_Disable(CMPx);
}


/************************ (C) COPYRIGHT FudanMicroelectronics *****END OF FILE****/

