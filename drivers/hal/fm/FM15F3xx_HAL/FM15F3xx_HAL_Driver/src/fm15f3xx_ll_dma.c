/**
  ******************************************************************************
  * @file    fm15f3xx_ll_dma.c
  * @author  SRG
  * @version V1.0.0
  * @date    2020-03-17
  * @brief
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 FudanMicroelectronics.
  * All rights reserved.</center></h2>
  *
  ******************************************************************************
  */
#include "fm15f3xx_ll_dma.h"


/** @addtogroup FM15F3XX_LL_Driver
  * @{
  */

#define UNUSED( X ) (void) X      /* To avoid gcc/g++ warnings */

/** @addtogroup DMA_LL
  * @{
  */
/* Exported functions --------------------------------------------------------*/
/** @addtogroup DMA_LL_Exported_Functions
  * @{
  */

/**
  * @brief  Initialize the DMA registers according to the specified parameters in LL_DMA_TransferConfig.
  * @param  Stream This parameter can be one of the following values:
  *         @arg @ref LL_DMA_CHANNEL_0
  *         @arg @ref LL_DMA_CHANNEL_1
  *         @arg @ref LL_DMA_CHANNEL_2
  *         @arg @ref LL_DMA_CHANNEL_3
  *         @arg @ref LL_DMA_CHANNEL_4
  *         @arg @ref LL_DMA_CHANNEL_5
  *         @arg @ref LL_DMA_CHANNEL_6
  *         @arg @ref LL_DMA_CHANNEL_7
  * @param  DMA_TransferStruct pointer to a @ref LL_DMA_TransferConfig structure.
  * @param  NextTcd pointer to a @ref dma_tcd_t structure.
  */
void LL_DMA_Init(uint32_t Channel, LL_DMA_TransferConfig *DMA_TransferStruct, dma_tcd_t *NextTcd)
{
    dma_tcd_t *Tcd = (dma_tcd_t *)&DMA->TCD[Channel];

    LL_DMA_ConfigTcd(Tcd, Channel, DMA_TransferStruct, NextTcd);
}


/**
  * @brief  Initialize the DMA registers according to the specified parameters in LL_DMA_TransferConfig.
  * @param  tcd pointer to a @ref dma_tcd_t structure.
  * @param  Stream This parameter can be one of the following values:
  *         @arg @ref LL_DMA_CHANNEL_0
  *         @arg @ref LL_DMA_CHANNEL_1
  *         @arg @ref LL_DMA_CHANNEL_2
  *         @arg @ref LL_DMA_CHANNEL_3
  *         @arg @ref LL_DMA_CHANNEL_4
  *         @arg @ref LL_DMA_CHANNEL_5
  *         @arg @ref LL_DMA_CHANNEL_6
  *         @arg @ref LL_DMA_CHANNEL_7
  * @param  config pointer to a @ref LL_DMA_TransferConfig structure.
  * @param  nextTcd pointer to a @ref dma_tcd_t structure.
 */
void LL_DMA_ConfigTcd(dma_tcd_t *tcd, uint32_t channel, LL_DMA_TransferConfig * config, dma_tcd_t *nextTcd)
{
    tcd->SADDR = config->SrcAddr;
    tcd->DADDR = config->DestAddr;
    tcd->ATTR = DMA_ATTR_DBURST(config->TcdAttr.DestTransferBurst)   | DMA_ATTR_DSIZE(config->TcdAttr.DestTransferWidth) \
                | DMA_ATTR_SBURST(config->TcdAttr.SrcTransferBurst)  | DMA_ATTR_SSIZE(config->TcdAttr.SrcTransferWidth) \
                | DMA_ATTR_SMOD(config->TcdAttr.Smod)     | DMA_ATTR_DMOD(config->TcdAttr.Dmod);
    tcd->SOFF = config->TcdAttr.SrcOffset;
    tcd->DOFF = config->TcdAttr.DestOffset;

    if (DMA->CR & DMA_CR_EMLM) { //小循环使能
        if (config->TcdMinorLoop.SmolEn || config->TcdMinorLoop.DmolEn) { //小循环moff存在
            tcd->NBYTES_MLOFFYES    = config->TcdMinorLoop.MinorLoopBytes & DMA_WORD2_NBYTES_Msk;
            if (config->TcdMinorLoop.SmolEn)
                tcd->NBYTES_MLOFFYES |= DMA_WORD2_SMLOENBYTES;
            if (config->TcdMinorLoop.DmolEn)
                tcd->NBYTES_MLOFFYES |= DMA_WORD2_DMLOENBYTES;
            tcd->NBYTES_MLOFFYES   |= (config->TcdMinorLoop.Moff << DMA_WORD2_MOFFNBYTES_Pos) & DMA_WORD2_MOFFNBYTES_Msk ;
        } else {
            tcd->NBYTES_MLOFFNO = (config->TcdMinorLoop.MinorLoopBytes & DMA_WORD2_NBYTES_Msk); //smole,dmloe,moff全置0
        }
    } else
        tcd->NBYTES_MLNO = config->TcdMinorLoop.MinorLoopBytes;


    if (config->TcdMinorLoop.MinorLinkEn) {
        tcd->BITER_CITER_ELINKYES  = (config->TcdMajorLoop.MajorLoopCounts & 0xfff); //biter赋值,有效位数同citer
        tcd->BITER_CITER_ELINKYES |= (config->TcdMajorLoop.MajorLoopCounts << DMA_WORD5_CITER_Pos) & 0xfff ; //citer
        tcd->BITER_CITER_ELINKYES |= (config->TcdMinorLoop.MinorLinkch << DMA_WORD5_MINORLINKCH_Pos) & DMA_WORD5_MINORLINKCH_Msk ;  //minor_linkch
        tcd->BITER_CITER_ELINKYES |= DMA_WORD5_MINORELINK; //minor_link enable
    } else {
        tcd->BITER_CITER_ELINKNO  =  config->TcdMajorLoop.MajorLoopCounts & DMA_WORD5_BITER_Msk; //biter赋值
        tcd->BITER_CITER_ELINKNO |= (config->TcdMajorLoop.MajorLoopCounts << DMA_WORD5_CITER_Pos)& DMA_WORD5_CITER_Msk ;  //citer赋值
    }

    //配置tcd word6
    if (config->TcdMajorLoop.MajorLinkEn)
        tcd->CSR = (DMA_WORD6_MAJORELINK | (config->TcdMajorLoop.MajorLinkch << DMA_WORD6_MAJORLINKCH_Pos));
    else
        tcd->CSR = 0;
    if (config->TcdMajorLoop.TcdScatterEn) { //scatter enable
        tcd->CSR  |= DMA_WORD6_ESG;
        tcd->SGA   = (uint32_t)nextTcd;
    } else {
        tcd->w3.DLAST = config->TcdMajorLoop.DLastSGA; //不scatter, 大循环无offset时置为0
        tcd->w3.SLAST = config->TcdMajorLoop.SLastSGA;
    }
}

void DMA_SetTransferConfig(uint32_t channel, LL_DMA_TransferConfig *config, dma_tcd_t *nextTcd)
{
    dma_tcd_t *tcd = (dma_tcd_t *)&DMA->TCD[channel];
    LL_DMA_ConfigTcd(tcd, channel, config, nextTcd);
}


/**
 * @brief  Aborts the DMA Transfer.
 * @param  Channel    DMA Channel.
 * @note  After disabling a DMA Stream,the current data will be transferred
 *        and the Stream will be effectively disabled only after the transfer of
 *        this minorloop is finished.
 * @retval HAL status
 */
ErrorStatus LL_DMA_Abort(uint32_t Channel)
{
    /* Disable all the transfer interrupts */
    LL_DMA_DISABLE_IT(Channel, LL_DMA_IT_ERR);
    LL_DMA_DISABLE_IT(Channel, LL_DMA_IT_TC | LL_DMA_IT_MAJORLOOP_HT);

    /* Abort the stream */
    LL_DMA_ABORT();

    /* Clear all interrupt flags at correct offset within the register */
    LL_DMA_CLEAR_FLAG_IT(hdma);

    return (SUCCESS);
}

/**
 * @brief  Starts the DMA Transfer.
 * @param  Channel    DMA Channel.
 * @param  SrcAddress   The source memory Buffer address
 * @param  DstAddress   The destination memory Buffer address
 * @param  MajorLoopNum The Number of MajorLoop
 * @retval status
 */
ErrorStatus LL_DMA_Start(uint32_t Channel, uint32_t SrcAddress, uint32_t DstAddress, uint32_t MajorLoopNum)
{
    /* Check DMA is Free*/
    if (LL_DMA_GET_ACTIVE(Channel)) {
        /* Return error status -busy */
        return (ERROR);
    }

    /* Clear the transfer complete flag */
    LL_DMA_CLEAR_DONE(Channel);

    /* Clear the transfer error flag*/
    LL_DMA_CLEAR_FLAG_ERR(Channel);

    /* Configure Address */
    DMA->TCD[Channel].SADDR = SrcAddress;
    DMA->TCD[Channel].DADDR = DstAddress;

    /* MajorLoop Num*/
    DMA->TCD[Channel].BITER_CITER_ELINKNO = (MajorLoopNum & DMA_WORD5_BITER_Msk) | ((MajorLoopNum  << DMA_WORD5_CITER_Pos) & DMA_WORD5_CITER_Msk);

    if (LL_DMA_TRIG_SOFTWARE != LL_DMA_GET_TRIGSRC(Channel)) {
        /* Enable Reset Trig after Major Loop*/
        DMA->TCD[Channel].CSR |= DMA_WORD6_DREQ;
        /* Enable Trig*/
        DMA->ERQ |= (0x1 << Channel);

        /* Enable the Peripheral */
        LL_DMA_MUX_ENABLE(Channel);
    }

    return (SUCCESS);
}

/**
 * @brief  Start the DMA Transfer with interrupt enabled.
 * @param  hdma  pointer to a DMA_HandleTypeDef structure that contains
 *               the configuration information for the specified DMA Stream.
 * @param  SrcAddress   The source memory Buffer address
 * @param  DstAddress   The destination memory Buffer address
 * @param  MajorLoopNum The Number of MajorLoop
 * @param  IT    specifies the DMA interrupt sources to be enabled or disabled.
 *               This parameter can be any combination of the following values:
 *               @arg LL_DMA_IT_TC: Transfer complete interrupt.
 *               @arg LL_DMA_IT_MAJORLOOP_HT: Half transfer complete interrupt.
 *               @arg LL_DMA_IT_ERR: Transfer error interrupt.
 * @retval HAL status
 */
ErrorStatus LL_DMA_Start_IT(uint32_t Channel, uint32_t SrcAddress, uint32_t DstAddress, uint32_t MajorLoopNum, uint32_t IT)
{
    /* Check DMA is Free*/
    if (LL_DMA_GET_ACTIVE(Channel)) {
        /* Return error status -busy */
        return (ERROR);
    }

    /* Clear the transfer complete flag */
    LL_DMA_CLEAR_DONE(Channel);

    /* Clear the transfer error flag*/
    LL_DMA_CLEAR_FLAG_ERR(Channel);

    /* Clear all interrupt at correct offset within the register */
    LL_DMA_DISABLE_IT(Channel, LL_DMA_IT_ERR);
    LL_DMA_DISABLE_IT(Channel, LL_DMA_IT_TC | LL_DMA_IT_MAJORLOOP_HT);

    /* Clear the transfer complete int */
    LL_DMA_CLEAR_FLAG_IT(Channel);

    /* Configure Address */
    DMA->TCD[Channel].SADDR = SrcAddress;
    DMA->TCD[Channel].DADDR = DstAddress;

    /* MajorLoop Num*/
    DMA->TCD[Channel].BITER_CITER_ELINKNO = (MajorLoopNum & DMA_WORD5_BITER_Msk) | ((MajorLoopNum  << DMA_WORD5_CITER_Pos) & DMA_WORD5_CITER_Msk);

    /* Enable Common interrupts*/
    if (IT & LL_DMA_IT_MAJORLOOP_MASK) {
        LL_DMA_ENABLE_IT(Channel, IT & LL_DMA_IT_MAJORLOOP_MASK);
    }
    /* Enable Error interrupts*/
    if (IT & LL_DMA_IT_ERR_MASK) {
        LL_DMA_ENABLE_IT(Channel, IT & LL_DMA_IT_ERR_MASK);
    }

    if (LL_DMA_TRIG_SOFTWARE != LL_DMA_GET_TRIGSRC(Channel)) {
        /* Enable Reset Trig after Major Loop*/
        DMA->TCD[Channel].CSR |= DMA_WORD6_DREQ;
        /* Enable Trig*/
        DMA->ERQ |= (0x1 << Channel);

        /* Enable the Peripheral */
        LL_DMA_MUX_ENABLE(Channel);
    }

    return (SUCCESS);
}

/**
 * @brief  MajorLoop Xfer complete callback in non blocking mode
 * @param  Channel DMA Channel
 * @retval None
 */
__weak void LL_DMA_XferHalfCpltCallback(uint32_t Channel)
{
    /* Prevent unused argument(s) compilation warning */
    UNUSED(Channel);

    /* NOTE : This function Should not be modified, when the callback is needed,
              the LL_DMA_XferHalfCpltCallback could be implemented in the user file
     */
}

/**
 * @brief  MajorLoop Xfer complete callback in non blocking mode
 * @param  Channel DMA Channel
 * @retval None
 */
__weak void LL_DMA_XferCpltCallback(uint32_t Channel)
{
    /* Prevent unused argument(s) compilation warning */
    UNUSED(Channel);

    /* NOTE : This function Should not be modified, when the callback is needed,
              the LL_DMA_XferCpltCallback could be implemented in the user file
     */
}

/**
 * @brief  Error callback in non blocking mode
 * @param  hdma pointer to a DMA_HandleTypeDef structure that contains
 *              the configuration information for the specified DMA Stream.
 * @retval None
 */
__weak void LL_DMA_ErrorCallback(uint32_t error)
{
    /* Prevent unused argument(s) compilation warning */
    UNUSED(error);

    /* NOTE : This function Should not be modified, when the callback is needed,
              the HAL_DMA_ErrorCallback could be implemented in the user file
     */
}
/**
 * @brief  Handles DMA interrupt request.
 * @param  None
 * @retval None
 */
void LL_DMA_Error_IRQHandler(void)
{
    uint32_t error_flag = LL_DMA_GET_ERR();
    /* Transfer Error Interrupt management ***************************************/
    if (0 != error_flag) {
        /* Clear the transfer error flag*/
        LL_DMA_CLEAR_ERR();

        /* Transfer error callback */
        LL_DMA_ErrorCallback(error_flag);
    }
}


/**
 * @brief  Handles DMA interrupt request.
 * @param  Channel DMA Channel
 *
 * @retval None
 */
void LL_DMA_IRQHandler(uint32_t Channel)
{
    if (LL_DMA_GET_FLAG_IT(Channel) != 0) {
        /* Clear the transfer half or complete int */
        LL_DMA_CLEAR_FLAG_IT(Channel);
        /* Half Transfer Complete Interrupt management ******************************/
        if (LL_DMA_GET_DONE(Channel) == 0) {
            /* Half transfer callback */
            LL_DMA_XferHalfCpltCallback(Channel);
        }
        /* Transfer Complete Interrupt management ***********************************/
        else if (LL_DMA_GET_DONE(Channel) != 0) {
            /* Clear the transfer complete flag */
            LL_DMA_CLEAR_DONE(Channel);
            /* Transfer complete callback */
            LL_DMA_XferCpltCallback(Channel);
        }
    }
}


/**
  * @}
  */

/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT FudanMicroelectronics *****END OF FILE****/
