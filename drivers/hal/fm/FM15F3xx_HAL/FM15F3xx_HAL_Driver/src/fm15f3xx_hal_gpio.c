/**
 ******************************************************************************
 * @file    fm15f3xx_hal_gpio.c
 * @author  WYL
 * @brief   GPIO HAL module driver.
 *          This file provides firmware functions to manage the following
 *          functionalities of the General Purpose Input/Output (GPIO) peripheral:
 *           + Initialization and de-initialization functions
 *           + IO operation functions
 *
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2020 FudanMicroelectronics.
 * All rights reserved.</center></h2>
 *
 ******************************************************************************
 */


/* Includes ------------------------------------------------------------------*/
#include "fm15f3xx_hal.h"


/** @addtogroup FM15F3xx_HAL_Driver
 * @{
 */


/** @defgroup GPIO GPIO
 * @brief GPIO HAL module driver
 * @{
 */

#ifdef GPIO_MODULE_ENABLED

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/


/** @addtogroup GPIO_Private_Constants GPIO Private Constants
 * @{
 */


/**
 * @}
 */
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/


/** @addtogroup GPIO_Private_Functions GPIO Private Functions
 * @{
 */
static void __GPIO_Config(GPIO_TypeDef *GPIOx, GPIO_InitTypeDef *GPIO_InitStruct);


/**
 * @}
 */

/* Private functions ---------------------------------------------------------*/
/* Exported functions --------------------------------------------------------*/


/** @defgroup GPIO_Exported_Functions GPIO Exported Functions
 * @{
 */


/** @defgroup GPIO_Exported_Functions_Group1 Initialization and de-initialization functions
 *  @brief    Initialization and Configuration functions
 *
   @verbatim
   ===============================================================================
 ##### Initialization and de-initialization functions #####
   ===============================================================================
   @endverbatim
 * @{
 */


/**
 * @brief  Initializes the GPIOx peripheral according to the specified parameters in the GPIO_Init.
 * @param  GPIOx where x can be (A..F) to select the GPIO peripheral
 * @param  GPIO_Init pointer to a GPIO_InitTypeDef structure that contains
 *         the configuration information for the specified GPIO peripheral.
 * @retval None
 */
void HAL_GPIO_Init(GPIO_TypeDef  *GPIOx, GPIO_InitTypeDef *GPIO_Init)
{
    /* Check the parameters */
    assert_param(IS_GPIO_ALL_INSTANCE(GPIOx));
    assert_param(IS_GPIO_PIN(GPIO_Init->Pin));
    assert_param(IS_GPIO_AF(GPIO_Init->Alt));
    assert_param(IS_GPIO_PULL(GPIO_Init->PuPd));

    /* Configure the port pins */
    if (GPIO_Init->Alt == GPIO_AF1_GPIO) {
        assert_param(IS_GPIO_DIRECTION(GPIO_Init->Dir));
        assert_param(IS_GPIO_SLEWSPEED(GPIO_Init->Speed));
        assert_param(IS_GPIO_OUTPUT(GPIO_Init->OType));
        assert_param(IS_GPIO_DRIVE(GPIO_Init->DriveStrength));
        assert_param(IS_GPIO_DIGITFILT(GPIO_Init->DigitFiltEn));
        assert_param(IS_GPIO_LOCK(GPIO_Init->Lock));
        assert_param(IS_GPIO_IRQorDMA(GPIO_Init->Irq));
        assert_param(IS_GPIO_WAKEUP(GPIO_Init->WECconfig));
        __GPIO_Config(GPIOx, GPIO_Init);
    } else {
        LL_GPIO_SetPinMux(GPIOx, GPIO_Init->Pin, GPIO_Init->Alt);
        LL_GPIO_SetPinPull(GPIOx, GPIO_Init->Pin, GPIO_Init->PuPd);
    }
}


/**
 * @brief  De-initializes the GPIOx peripheral registers to their default reset values.
 * @param  GPIOx where x can be (A..F) to select the GPIO peripheral
 * @param  GPIO_Pin specifies the port bit to be written.
 *          This parameter can be one of GPIO_PIN_x where x can be (0..15).
 * @retval None
 */
void HAL_GPIO_DeInit(GPIO_TypeDef  *GPIOx, uint32_t GPIO_Pin)
{
    /* Prevent unused argument(s) compilation warning */
    UNUSED(0);
}


/**
 * @}
 */


/** @defgroup GPIO_Exported_Functions_Group2 IO operation functions
 *  @brief   GPIO Read and Write
 *
   @verbatim
   ===============================================================================
 ##### IO operation functions #####
   ===============================================================================

   @endverbatim
 * @{
 */


/**
 * @brief  Reads the specified input port pin.
 * @param  GPIOx where x can be (A..F) to select the GPIO peripheral
 * @param  GPIO_Pin specifies the port bit to read.
 *         This parameter can be GPIO_PIN_x where x can be (0..15).
 * @retval The input port pin value.
 */
GPIO_PinState HAL_GPIO_ReadPin(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin)
{
    /* Check the parameters */
    assert_param(IS_GPIO_PIN(GPIO_Pin));

    return (LL_GPIO_ReadInputDataBit(GPIOx, GPIO_Pin));
}


/**
 * @brief  Sets or clears the selected data port bit.
 * @param  GPIOx where x can be (A..K) to select the GPIO peripheral.
 * @param  GPIO_Pin specifies the port bit to be written.
 *          This parameter can be one of GPIO_PIN_x where x can be (0..15).
 * @param  PinState specifies the value to be written to the selected bit.
 *          This parameter can be one of the GPIO_PinState enum values:
 *            @arg GPIO_PIN_RESET: to clear the port pin
 *            @arg GPIO_PIN_SET: to set the port pin
 * @retval None
 */
void HAL_GPIO_WritePin(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin, GPIO_PinState PinState)
{
    /* Check the parameters */
    assert_param(IS_GPIO_PIN(GPIO_Pin));
    assert_param(IS_GPIO_PIN_ACTION(PinState));

    if (PinState != GPIO_PIN_RESET) {
        LL_GPIO_SetOutputPin(GPIOx, GPIO_Pin);
    } else {
        LL_GPIO_ResetOutputPin(GPIOx, GPIO_Pin);
    }
}


/**
 * @brief  Toggles the specified GPIO pins.
 * @param  GPIOx Where x can be (A..F) to select the GPIO peripheral.
 * @param  GPIO_Pin Specifies the pins to be toggled.
 * @retval None
 */
void HAL_GPIO_TogglePin(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin)
{
    /* Check the parameters */
    assert_param(IS_GPIO_PIN(GPIO_Pin));

    LL_GPIO_TogglePin(GPIOx, GPIO_Pin);
}


/**
 * @brief  This function handles interrupt request.
 * @param  GPIOx Where x can be (A..F) to select the GPIO peripheral.
 * @param  GPIO_Pin Specifies the pins
 * @retval None
 */
void HAL_GPIO_IRQHandler(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin)
{
    /* EXTI line interrupt detected */
    if (LL_GPIO_GetFlagPin_IT(GPIOx, GPIO_Pin) != RESET) {
        LL_GPIO_ClearFlagPin_IT(GPIOx, GPIO_Pin);
        HAL_GPIO_IRQCallback(GPIOx, GPIO_Pin);
    }
}


/**
 * @brief  IRQ callbacks.
 * @param  GPIOx Where x can be (A..F) to select the GPIO peripheral.
 * @param  GPIO_Pin Specifies the pins
 * @retval None
 */
__weak void HAL_GPIO_IRQCallback(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin)
{
    /* Prevent unused argument(s) compilation warning */
    UNUSED(GPIO_Pin);


    /* NOTE: This function Should not be modified, when the callback is needed,
             the HAL_GPIO_EXTI_Callback could be implemented in the user file
     */
}


/* Private functions ---------------------------------------------------------*/


/**
 * @brief Initialize the GPIO according to the specified parameters
 *         in the GPIO_InitTypeDef and initialize the associated handle.
 * @param GPIO Instance
 * @param GPIO_InitStruct
 * @retval
 */
static void __GPIO_Config(GPIO_TypeDef *GPIOx, GPIO_InitTypeDef *GPIO_InitStruct)
{
    uint32_t Pin = GPIO_InitStruct->Pin;
    for (int i = 0; i < 16; i++) {
        if (Pin & 0x01) {
            MODIFY_REG(LL_GPIO_GetCFG(GPIOx)->PCR[i], GPIO_CFG_PCR_MUX_Msk, GPIO_InitStruct->Alt);
            MODIFY_REG(LL_GPIO_GetCFG(GPIOx)->PCR[i],
                       GPIO_CFG_PCR_SRE_Msk |
                       GPIO_CFG_PCR_ODE_Msk |
                       GPIO_CFG_PCR_PS_Msk | GPIO_CFG_PCR_PE_Msk |
                       GPIO_CFG_PCR_DSE_Msk |
                       GPIO_CFG_PCR_LK_Msk |
                       GPIO_CFG_PCR_IRQC_Msk |
                       GPIO_CFG_PCR_WKUPCFG_Msk,
                       GPIO_InitStruct->Speed |
                       GPIO_InitStruct->OType |
                       GPIO_InitStruct->PuPd |
                       GPIO_InitStruct->DriveStrength |
                       GPIO_InitStruct->Lock |
                       GPIO_InitStruct->Irq |
                       GPIO_InitStruct->WECconfig);
        }
        Pin >>= 1;
    }
    LL_GPIO_SetPinDir(GPIOx, GPIO_InitStruct->Pin, GPIO_InitStruct->Dir);
    MODIFY_REG(LL_GPIO_GetCFG(GPIOx)->DFER, GPIO_InitStruct->Pin, GPIO_InitStruct->DigitFiltEn);
}


/**
 * @}
 */


/**
 * @}
 */

#endif /* GPIO_MODULE_ENABLED */


/**
 * @}
 */


/**
 * @}
 */

/************************ (C) COPYRIGHT FudanMicroelectronics *****END OF FILE****/
