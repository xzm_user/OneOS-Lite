/**
  ******************************************************************************
  * @file    fm15f3xx_ll_dcmi_dma.c
  * @author  SRG
  * @version V1.0.0
  * @date    2020-03-17
  * @brief
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 FudanMicroelectronics.
  * All rights reserved.</center></h2>
  *
  ******************************************************************************
  */
#include "fm15f3xx_ll_dcmi_dma.h"


/**
  * @brief  Initialize the I2C registers according to the specified parameters in I2C_InitStruct.
  * @param  I2Cx I2C Instance.
  * @param  I2C_InitStruct pointer to a @ref LL_I2C_InitTypeDef structure.
  */
void LL_DCMI_DMA_WinConfig(uint16_t width, uint16_t height, uint8_t winsize)
{
    uint32_t winParam = 0;      //窗口参数

    uint8_t pWidth = width / 16 - 1;    //图像实际宽度为width = (width+1)*16
    uint8_t pHeight = height / 16 - 1;  //图像实际高度为height = (height+1)*16
    uint8_t pWinSize = winsize / 8 - 1; //窗口实际大小为wsize = (wsize+1)*8

    winParam = (pWidth << DCMI_DMA_CFG_C_WIDTH_POS)  |   \
               (pHeight << DCMI_DMA_CFG_C_HEIGHT_POS) |   \
               (pWinSize << DCMI_DMA_CFG_C_WSIZE_POS) |   \
               DCMI_DMA_CFG_C_WRBINPAD | DCMI_DMA_CFG_C_RDWORDEN;  //图像每行二值化结束后继续往后追加存储,DCMI_FIFO里的数据按照dword并行给到二值化引擎

    LL_DCMI_DMA_ConfigWin(winParam);
    //    DCMI_DMA->cfg_c = (0x27<<24)|(0x1d<<16)|(3<<8)|(1<<3)|(1<<0); // 设置窗口大小

}



/************************ (C) COPYRIGHT FudanMicroelectronics *****END OF FILE****/

