/**
 ******************************************************************************
 * @file    fm15f3xx_hal_qspi.c
 * @author  MCD Application Team
 * @brief   QSPI HAL module driver.
 *          This file provides firmware functions to manage the following
 *          functionalities of the QuadSPI interface (QSPI).
 *           + Initialization and de-initialization functions
 *           + Indirect functional mode management
 *           + Memory-mapped functional mode management
 *           + Interrupts and flags management
 *           + Errors management and abort functionality
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2020 FudanMicroelectronics.
 * All rights reserved.</center></h2>
 *
 ******************************************************************************
 */


/* Includes ------------------------------------------------------------------*/
#include "fm15f3xx_hal.h"


/** @addtogroup FM15F3xx_HAL_Driver
 * @{
 */


/** @defgroup QSPI QSPI
 * @brief HAL QSPI module driver
 * @{
 */
#ifdef QSPI_MODULE_ENABLED

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/


/** @addtogroup QSPI_Private_Functions QSPI Private Functions
 * @{
 */
static HAL_StatusTypeDef __QSPI_WaitFlagStateUntilTimeout(QSPI_HandleTypeDef *hqspi, uint32_t Flag, FlagStatus State, uint32_t tickstart, uint32_t Timeout);


static HAL_StatusTypeDef __QSPI_WaitTxFifoStateUntilTimeout(QSPI_HandleTypeDef *hqspi, uint32_t Flag, FlagStatus State, uint32_t tickstart, uint32_t Timeout);


static HAL_StatusTypeDef __QSPI_WaitRxFifoEmptyUntilTimeout(QSPI_HandleTypeDef *hqspi, uint32_t tickstart, uint32_t Timeout);


static void __QSPI_Config(QSPI_HandleTypeDef *hqspi);


static void __QSPI_CommunicateConfig(QSPI_HandleTypeDef *hqspi, QSPI_CommandTypeDef *cmd);


/**
 * @}
 */

/* Exported functions ---------------------------------------------------------*/


/** @defgroup QSPI_Exported_Functions QSPI Exported Functions
 * @{
 */


/** @defgroup QSPI_Exported_Functions_Group1 Initialization/de-initialization functions
 *  @brief    Initialization and Configuration functions
 *
   @verbatim
   ===============================================================================
 ##### Initialization and Configuration functions #####
   ===============================================================================
    [..]
    This subsection provides a set of functions allowing to :
      (+) Initialize the QuadSPI.
      (+) De-initialize the QuadSPI.

   @endverbatim
 * @{
 */


/**
 * @brief Initializes the QSPI mode according to the specified parameters
 *        in the QSPI_InitTypeDef and creates the associated handle.
 * @param hqspi qspi handle
 * @retval HAL status
 */
HAL_StatusTypeDef HAL_QSPI_Init(QSPI_HandleTypeDef *hqspi)
{
    HAL_StatusTypeDef status = HAL_ERROR;

    /* Check the QSPI handle allocation */
    if (hqspi == NULL) {
        return (HAL_ERROR);
    }

    /* Check the parameters */
    assert_param(IS_QSPI_ALL_INSTANCE(hqspi->Instance));
    assert_param(IS_QSPI_WORKMODE(hqspi->Init.WorkMode));
    assert_param(IS_QSPI_ACCESSFLASH(hqspi->Init.AccessFlash));
    assert_param(IS_QSPI_SPEEDMODE(hqspi->Init.SpeedMode));
    assert_param(IS_QSPI_ENDIANMODE(hqspi->Init.EndianMode));
    assert_param(IS_QSPI_SIGNIFICANTBIT(hqspi->Init.SignificantBit));
    assert_param(IS_QSPI_IOMODE(hqspi->Init.IoMode));
    assert_param(IS_QSPI_SSHIF(hqspi->Init.SampleShift));
    assert_param(IS_QSPI_SPOINT(hqspi->Init.SamplePoint));
    assert_param(IS_QSPI_CLKMODE(hqspi->Init.ClkMode));
    assert_param(IS_QSPI_CLKDIV(hqspi->Init.ClkDiv));

    if (hqspi->State == HAL_QSPI_STATE_RESET) {
        /* Init the low level hardware : GPIO, CLOCK */
        HAL_QSPI_MspInit(hqspi);
    }

    __QSPI_Config(hqspi);

    /* Enable the QSPI peripheral */
    __HAL_QSPI_ENABLE(hqspi);

    /* Set QSPI error code to none */
    hqspi->ErrorCode = HAL_QSPI_ERROR_NONE;

    /* Initialize the QSPI state */
    hqspi->State = HAL_QSPI_STATE_READY;

    /* Return function status */
    return (status);
}


/**
 * @brief DeInitializes the QSPI peripheral
 * @param hqspi qspi handle
 * @retval HAL status
 */
HAL_StatusTypeDef HAL_QSPI_DeInit(QSPI_HandleTypeDef *hqspi)
{
    /* Check the QSPI handle allocation */
    if (hqspi == NULL) {
        return (HAL_ERROR);
    }

    /* Disable the QSPI Peripheral Clock */
    __HAL_QSPI_DISABLE(hqspi);

    /* DeInit the low level hardware: GPIO, CLOCK, NVIC... */
    HAL_QSPI_MspDeInit(hqspi);

    /* Set QSPI error code to none */
    hqspi->ErrorCode = HAL_QSPI_ERROR_NONE;

    /* Initialize the QSPI state */
    hqspi->State = HAL_QSPI_STATE_RESET;

    return (HAL_OK);
}


/**
 * @brief QSPI MSP Init
 * @param hqspi QSPI handle
 * @retval None
 */
__weak void HAL_QSPI_MspInit(QSPI_HandleTypeDef *hqspi)
{
    /* Prevent unused argument(s) compilation warning */
    UNUSED(hqspi);


    /* NOTE : This function should not be modified, when the callback is needed,
              the HAL_QSPI_MspInit can be implemented in the user file
     */
}


/**
 * @brief QSPI MSP DeInit
 * @param hqspi QSPI handle
 * @retval None
 */
__weak void HAL_QSPI_MspDeInit(QSPI_HandleTypeDef *hqspi)
{
    /* Prevent unused argument(s) compilation warning */
    UNUSED(hqspi);


    /* NOTE : This function should not be modified, when the callback is needed,
              the HAL_QSPI_MspDeInit can be implemented in the user file
     */
}


/**
 * @}
 */


/** @defgroup QSPI_Exported_Functions_Group2 IO operation functions
 *  @brief QSPI Transmit/Receive functions
 *
   @verbatim
   ===============================================================================
 ##### IO operation functions #####
   ===============================================================================
   @endverbatim
 * @{
 */


/**
 * @brief This function handles QSPI interrupt request.
 * @param hqspi QSPI handle
 * @retval None.
 */
void HAL_QSPI_IRQHandler(QSPI_HandleTypeDef *hqspi)
{
    __I uint32_t    *data_reg;
    uint32_t        flag        = hqspi->Instance->STATUS;
    uint32_t        itsource    = hqspi->Instance->INTEN;

    /* QSPI RxFifo Threshold interrupt occurred ----------------------------------*/
    if (((flag & (QSPI_FLAG_RxFIFO_FULL | QSPI_FLAG_RxFIFO_NEARFULL)) != RESET) && ((itsource & (QSPI_INT_RxFIFONFULL | QSPI_INT_RxFIFOFULL)) != RESET)) {
        data_reg = &hqspi->Instance->RXFIFOD;

        /* Receiving Process */
        while ((hqspi->Instance->RXFIFOS & QSPI_FLAG_RxFIFO_CNT) != 0) {
            if (hqspi->RxXferCount > 0) {
                /* Read the FIFO until it is empty */
                *hqspi->pRxBuffPtr++ = *(__IO uint8_t *) data_reg;
                hqspi->RxXferCount--;
            } else {
                break;
            }
        }
        /* FIFO Threshold callback */
        HAL_QSPI_FifoThresholdCallback(hqspi);
    }

    /* QSPI TxFifo Threshold interrupt occurred ----------------------------------*/
    else if (((flag & (QSPI_FLAG_TxFIFO_EMPTY | QSPI_FLAG_TxFIFO_NEAREMPTY)) != RESET) && ((itsource & (QSPI_INT_TxFIFONEMPTY | QSPI_INT_TxFIFOEMPTY)) != RESET)) {
        data_reg = &hqspi->Instance->TXFIFOD;
        /* Transmission process */
        while ((hqspi->Instance->TXFIFOS & QSPI_FLAG_TxFIFO_CNT) <= 0x10) {
            if (hqspi->TxXferCount > 0) {
                /* Fill the FIFO until it is full */
                *(__IO uint8_t *) data_reg = *hqspi->pTxBuffPtr++;
                hqspi->TxXferCount--;
            } else {
                break;
            }
        }
        /* FIFO Threshold callback */
        HAL_QSPI_FifoThresholdCallback(hqspi);
    }

    /* QSPI Transfer Complete interrupt occurred -------------------------------*/
    else if (((flag & QSPI_FLAG_TRANSOVER) != RESET) && ((itsource & QSPI_INT_TRANSOVER) != RESET)) {
        /* Transfer complete callback */
        if (hqspi->State == HAL_QSPI_STATE_BUSY_INDIRECT_Tx) {
            /* Change state of QSPI */
            hqspi->State = HAL_QSPI_STATE_READY;

            /* TX Complete callback */
            HAL_QSPI_TxCpltCallback(hqspi);
        } else if (hqspi->State == HAL_QSPI_STATE_BUSY_INDIRECT_Rx) {
            data_reg = &hqspi->Instance->RXFIFOD;
            while ((hqspi->Instance->RXFIFOS & QSPI_FLAG_RxFIFO_CNT) != 0) {
                if (hqspi->RxXferCount > 0) {
                    /* Read the last data received in the FIFO until it is empty */
                    *hqspi->pRxBuffPtr++ = *(__IO uint8_t *) data_reg;
                    hqspi->RxXferCount--;
                } else {
                    /* All data have been received for the transfer */
                    break;
                }
            }

            /* Change state of QSPI */
            hqspi->State = HAL_QSPI_STATE_READY;

            /* RX Complete callback */
            HAL_QSPI_RxCpltCallback(hqspi);
        } else if (hqspi->State == HAL_QSPI_STATE_BUSY) {
            /* Change state of QSPI */
            hqspi->State = HAL_QSPI_STATE_READY;

            /* Command Complete callback */
            HAL_QSPI_CmdCpltCallback(hqspi);
        } else if (hqspi->State == HAL_QSPI_STATE_ABORT) {
            /* Change state of QSPI */
            hqspi->State = HAL_QSPI_STATE_READY;

            if (hqspi->ErrorCode == HAL_QSPI_ERROR_NONE) {
                /* Abort Complete callback */
                HAL_QSPI_AbortCpltCallback(hqspi);
            } else {
                /* Error callback */
                HAL_QSPI_ErrorCallback(hqspi);
            }
        }
    }

    /* QSPI Transfer Stall interrupt occurred ----------------------------------*/
    else if (((flag & QSPI_FLAG_STALL) != RESET) && ((itsource & QSPI_INT_STALL) != RESET)) {
        /* Change state of QSPI */
        hqspi->State = HAL_QSPI_STATE_READY;

        /* Quit Stall callback */
        HAL_QSPI_QuitStallCallback(hqspi);
    }

    /* QSPI Abort interrupt occurred -----------------------------------------*/
    else if (((flag & QSPI_FLAG_TRANSABORT) != RESET) && ((itsource & QSPI_INT_ABORT) != RESET)) {
        /* Change state of QSPI */
        hqspi->State = HAL_QSPI_STATE_READY;

        /* Abort callback */
        HAL_QSPI_AbortCpltCallback(hqspi);
    }
}


/**
 * @brief Sets the command configuration.
 * @param hqspi QSPI handle
 * @param cmd  structure that contains the command configuration information
 * @param Timeout  Time out duration
 * @note   This function is used only in Indirect Read or Write Modes
 * @retval HAL status
 */
HAL_StatusTypeDef HAL_QSPI_Command(QSPI_HandleTypeDef *hqspi, QSPI_CommandTypeDef *cmd, uint32_t Timeout)
{
    HAL_StatusTypeDef   status      = HAL_ERROR;
    uint32_t            tickstart   = HAL_GetTick();

    if (hqspi->State == HAL_QSPI_STATE_READY) {
        hqspi->ErrorCode = HAL_QSPI_ERROR_NONE;

        /* Update QSPI state */
        hqspi->State = HAL_QSPI_STATE_BUSY;

        /* Call the configuration function */
        __QSPI_CommunicateConfig(hqspi, cmd);

        if (cmd->DataLines == QSPI_LINE_NONE) {
            __HAL_QSPI_START_INDIRECT(hqspi);


            /* When there is no data phase, the transfer start as soon as the configuration is done
               so wait until TC flag is set to go back in idle state */
            status = __QSPI_WaitFlagStateUntilTimeout(hqspi, QSPI_FLAG_TRANSOVER, SET, tickstart, Timeout);

            if (status == HAL_OK) {
                /* Update QSPI state */
                hqspi->State = HAL_QSPI_STATE_READY;
            }
        } else {
            /* Update QSPI state */
            hqspi->State    = HAL_QSPI_STATE_READY;
            status          = HAL_OK;
        }
    } else {
        status = HAL_BUSY;
    }

    /* Return function status */
    return (status);
}


/**
 * @brief Transmit an amount of data in blocking mode.
 * @param hqspi QSPI handle
 * @param pData pointer to data buffer
 * @param Timeout  Time out duration
 * @note   This function is used only in Indirect Write Mode
 * @retval HAL status
 */
HAL_StatusTypeDef HAL_QSPI_Transmit(QSPI_HandleTypeDef *hqspi, uint8_t *pData, uint32_t Timeout)
{
    HAL_StatusTypeDef   status      = HAL_OK;
    uint32_t            tickstart   = 0;
    __IO uint32_t       *data_reg   = &hqspi->Instance->TXFIFOD;

    if (hqspi->State == HAL_QSPI_STATE_READY) {
        hqspi->ErrorCode = HAL_QSPI_ERROR_NONE;

        if (pData != NULL) {
            /* Update state */
            hqspi->State = HAL_QSPI_STATE_BUSY_INDIRECT_Tx;

            /* Configure counters and size of the handle */
            hqspi->TxXferCount  = hqspi->Instance->DLEN;
            hqspi->pTxBuffPtr   = pData;

            __HAL_QSPI_START_INDIRECT(hqspi);

            while (hqspi->TxXferCount > 0) {
                /* Wait until Txfifo Full flag is reset to send data */
                while (__HAL_QSPI_IS_ACTIVE_FLAG(hqspi->Instance->TXFIFOS, QSPI_FLAG_TxFIFO_FULL))
                    ;

                *(__IO uint8_t *) data_reg = *hqspi->pTxBuffPtr++;
                hqspi->TxXferCount--;
            }

            while (1) {
                if (__HAL_QSPI_IS_ACTIVE_FLAG(hqspi->Instance->STATUS, QSPI_FLAG_STALL)) {
                    *(__IO uint8_t *) data_reg = 0xFF;
                } else if (__HAL_QSPI_IS_ACTIVE_FLAG(hqspi->Instance->STATUS, QSPI_FLAG_TRANSOVER)) {
                    break;
                }
            }


            /* Wait until TC flag is set to go back in idle state */
            tickstart   = HAL_GetTick();
            status      = __QSPI_WaitFlagStateUntilTimeout(hqspi, QSPI_FLAG_TRANSOVER, SET, tickstart, Timeout);
            __HAL_QSPI_CLEAR_FLAG(hqspi, QSPI_FLAG_CLEAR_TxFIFO);
            /* Update QSPI state */
            hqspi->State = HAL_QSPI_STATE_READY;
        } else {
            hqspi->ErrorCode    |= HAL_QSPI_ERROR_INVALID_PARAM;
            status              = HAL_ERROR;
        }
    } else {
        status = HAL_BUSY;
    }

    return (status);
}


/**
 * @brief Receive an amount of data in blocking mode
 * @param hqspi QSPI handle
 * @param pData pointer to data buffer
 * @param Timeout  Time out duration
 * @note   This function is used only in Indirect Read Mode
 * @retval HAL status
 */
HAL_StatusTypeDef HAL_QSPI_Receive(QSPI_HandleTypeDef *hqspi, uint8_t *pData, uint32_t Timeout)
{
    HAL_StatusTypeDef   status      = HAL_OK;
    uint32_t            tickstart   = 0;
    __I uint32_t        *data_reg   = &hqspi->Instance->RXFIFOD;

    if (hqspi->State == HAL_QSPI_STATE_READY) {
        hqspi->ErrorCode = HAL_QSPI_ERROR_NONE;
        if (pData != NULL) {
            /* Update state */
            hqspi->State = HAL_QSPI_STATE_BUSY_INDIRECT_Rx;

            /* Configure counters and size of the handle */
            hqspi->RxXferCount  = hqspi->Instance->DLEN;
            hqspi->pRxBuffPtr   = pData;


            __HAL_QSPI_START_INDIRECT(hqspi);

            while (hqspi->RxXferCount > 0) {
                /* Wait until Rxfifo don't empty to read received data */
                while ((hqspi->Instance->RXFIFOS & QSPI_FLAG_RxFIFO_CNT) == 0)
                    ;

                *hqspi->pRxBuffPtr++ = *(__IO uint8_t *) data_reg;
                hqspi->RxXferCount--;
            }

            /* Wait until TC flag is set to go back in idle state */
            tickstart   = HAL_GetTick();
            status      = __QSPI_WaitFlagStateUntilTimeout(hqspi, QSPI_FLAG_TRANSOVER, SET, tickstart, Timeout);

            /* Update QSPI state */
            hqspi->State = HAL_QSPI_STATE_READY;
        } else {
            hqspi->ErrorCode    |= HAL_QSPI_ERROR_INVALID_PARAM;
            status              = HAL_ERROR;
        }
    } else {
        status = HAL_BUSY;
    }

    return (status);
}


/**
 * @brief Sets the command configuration in interrupt mode.
 * @param hqspi QSPI handle
 * @param cmd  structure that contains the command configuration information
 * @param  IT specifies the QSPI interrupt source to enable.
 * @note   This function is used only in Indirect Read or Write Modes
 * @retval HAL status
 */
HAL_StatusTypeDef HAL_QSPI_Command_IT(QSPI_HandleTypeDef *hqspi, QSPI_CommandTypeDef *cmd, uint32_t IT)
{
    HAL_StatusTypeDef   status      = HAL_OK;
    uint32_t            tickstart   = HAL_GetTick();

    if (hqspi->State == HAL_QSPI_STATE_READY) {
        hqspi->ErrorCode = HAL_QSPI_ERROR_NONE;

        /* Update QSPI state */
        hqspi->State = HAL_QSPI_STATE_BUSY;

        /* Call the configuration function */
        __QSPI_CommunicateConfig(hqspi, cmd);

        __HAL_QSPI_ENABLE_IT(hqspi, IT);

        __HAL_QSPI_START_INDIRECT(hqspi);

        /* Update QSPI state */
        hqspi->State = HAL_QSPI_STATE_READY;
    } else {
        status = HAL_BUSY;
    }

    /* Return function status */
    return (status);
}


/**
 * @brief  Send an amount of data in interrupt mode
 * @param  hqspi QSPI handle
 * @param  pData pointer to data buffer
 * @note   This function is used only in Indirect Write Mode
 * @retval HAL status
 */
HAL_StatusTypeDef HAL_QSPI_Transmit_IT(QSPI_HandleTypeDef *hqspi, uint8_t *pData)
{
    HAL_StatusTypeDef status = HAL_OK;

    if (hqspi->State == HAL_QSPI_STATE_READY) {
        hqspi->ErrorCode = HAL_QSPI_ERROR_NONE;
        if (pData != NULL) {
            /* Update state */
            hqspi->State = HAL_QSPI_STATE_BUSY_INDIRECT_Tx;

            /* Configure counters and size of the handle */
            hqspi->TxXferCount  = hqspi->Instance->DLEN;
            hqspi->TxXferSize   = hqspi->Instance->DLEN;
            hqspi->pTxBuffPtr   = pData;
        } else {
            hqspi->ErrorCode    |= HAL_QSPI_ERROR_INVALID_PARAM;
            status              = HAL_ERROR;
        }
    } else {
        status = HAL_BUSY;
    }

    return (status);
}


/**
 * @brief  Receive an amount of data in no-blocking mode with Interrupt
 * @param  hqspi QSPI handle
 * @param  pData pointer to data buffer
 * @note   This function is used only in Indirect Read Mode
 * @retval HAL status
 */
HAL_StatusTypeDef HAL_QSPI_Receive_IT(QSPI_HandleTypeDef *hqspi, uint8_t *pData)
{
    HAL_StatusTypeDef status = HAL_OK;

    if (hqspi->State == HAL_QSPI_STATE_READY) {
        hqspi->ErrorCode = HAL_QSPI_ERROR_NONE;

        if (pData != NULL) {
            /* Update state */
            hqspi->State = HAL_QSPI_STATE_BUSY_INDIRECT_Rx;

            /* Configure counters and size of the handle */
            hqspi->RxXferCount  = hqspi->Instance->DLEN;
            hqspi->RxXferSize   = hqspi->Instance->DLEN;
            hqspi->pRxBuffPtr   = pData;
        } else {
            hqspi->ErrorCode    |= HAL_QSPI_ERROR_INVALID_PARAM;
            status              = HAL_ERROR;
        }
    } else {
        status = HAL_BUSY;
    }

    return (status);
}


/**
 * @brief  Configure the Memory Mapped mode.
 * @param  hqspi QSPI handle
 * @param  cmd structure that contains the command configuration information.
 * @param  cfg structure that contains the memory mapped configuration information.
 * @note   This function is used only in Memory mapped Mode
 * @retval HAL status
 */
HAL_StatusTypeDef HAL_QSPI_MemoryMapped(QSPI_HandleTypeDef *hqspi, QSPI_CommandTypeDef *cmd)
{
    HAL_StatusTypeDef   status      = HAL_OK;
    uint32_t            tickstart   = HAL_GetTick();

    if (hqspi->State == HAL_QSPI_STATE_READY) {
        hqspi->ErrorCode = HAL_QSPI_ERROR_NONE;

        /* Update state */
        hqspi->State = HAL_QSPI_STATE_BUSY_MEM_MAPPED;

        /* Call the configuration function */
        __QSPI_CommunicateConfig(hqspi, cmd);
    } else {
        status = HAL_BUSY;
    }

    /* Return function status */
    return (status);
}


/**
 * @brief  Transfer Error callbacks
 * @param  hqspi QSPI handle
 * @retval None
 */
__weak void HAL_QSPI_ErrorCallback(QSPI_HandleTypeDef *hqspi)
{
    /* Prevent unused argument(s) compilation warning */
    UNUSED(hqspi);


    /* NOTE : This function Should not be modified, when the callback is needed,
              the HAL_QSPI_ErrorCallback could be implemented in the user file
     */
}


/**
 * @brief  Quit Stall callback.
 * @param  hqspi QSPI handle
 * @retval None
 */
__weak void HAL_QSPI_QuitStallCallback(QSPI_HandleTypeDef *hqspi)
{
    /* Prevent unused argument(s) compilation warning */
    UNUSED(hqspi);


    /* NOTE: This function should not be modified, when the callback is needed,
             the HAL_QSPI_QuitStallCallback could be implemented in the user file
     */
}


/**
 * @brief  Abort completed callback.
 * @param  hqspi QSPI handle
 * @retval None
 */
__weak void HAL_QSPI_AbortCpltCallback(QSPI_HandleTypeDef *hqspi)
{
    /* Prevent unused argument(s) compilation warning */
    UNUSED(hqspi);


    /* NOTE: This function should not be modified, when the callback is needed,
             the HAL_QSPI_AbortCpltCallback could be implemented in the user file
     */
}


/**
 * @brief  Command completed callback.
 * @param  hqspi QSPI handle
 * @retval None
 */
__weak void HAL_QSPI_CmdCpltCallback(QSPI_HandleTypeDef *hqspi)
{
    /* Prevent unused argument(s) compilation warning */
    UNUSED(hqspi);


    /* NOTE: This function Should not be modified, when the callback is needed,
             the HAL_QSPI_CmdCpltCallback could be implemented in the user file
     */
}


/**
 * @brief  Rx Transfer completed callbacks.
 * @param  hqspi QSPI handle
 * @retval None
 */
__weak void HAL_QSPI_RxCpltCallback(QSPI_HandleTypeDef *hqspi)
{
    /* Prevent unused argument(s) compilation warning */
    UNUSED(hqspi);


    /* NOTE: This function Should not be modified, when the callback is needed,
             the HAL_QSPI_RxCpltCallback could be implemented in the user file
     */
}


/**
 * @brief  Tx Transfer completed callbacks.
 * @param  hqspi QSPI handle
 * @retval None
 */
__weak void HAL_QSPI_TxCpltCallback(QSPI_HandleTypeDef *hqspi)
{
    /* Prevent unused argument(s) compilation warning */
    UNUSED(hqspi);


    /* NOTE: This function Should not be modified, when the callback is needed,
             the HAL_QSPI_TxCpltCallback could be implemented in the user file
     */
}


/**
 * @brief  Rx Half Transfer completed callbacks.
 * @param  hqspi QSPI handle
 * @retval None
 */
__weak void HAL_QSPI_RxHalfCpltCallback(QSPI_HandleTypeDef *hqspi)
{
    /* Prevent unused argument(s) compilation warning */
    UNUSED(hqspi);


    /* NOTE: This function Should not be modified, when the callback is needed,
             the HAL_QSPI_RxHalfCpltCallback could be implemented in the user file
     */
}


/**
 * @brief  Tx Half Transfer completed callbacks.
 * @param  hqspi QSPI handle
 * @retval None
 */
__weak void HAL_QSPI_TxHalfCpltCallback(QSPI_HandleTypeDef *hqspi)
{
    /* Prevent unused argument(s) compilation warning */
    UNUSED(hqspi);


    /* NOTE: This function Should not be modified, when the callback is needed,
             the HAL_QSPI_TxHalfCpltCallback could be implemented in the user file
     */
}


/**
 * @brief  FIFO Threshold callbacks
 * @param  hqspi QSPI handle
 * @retval None
 */
__weak void HAL_QSPI_FifoThresholdCallback(QSPI_HandleTypeDef *hqspi)
{
    /* Prevent unused argument(s) compilation warning */
    UNUSED(hqspi);


    /* NOTE : This function Should not be modified, when the callback is needed,
              the HAL_QSPI_FIFOThresholdCallback could be implemented in the user file
     */
}


/**
 * @}
 */


/** @defgroup QSPI_Exported_Functions_Group3 Peripheral Control and State functions
 *  @brief   QSPI control and State functions
 *
   @verbatim
   ===============================================================================
 ##### Peripheral Control and State functions #####
   ===============================================================================
   @endverbatim
 * @{
 */


/**
 * @brief  Return the QSPI handle state.
 * @param  hqspi QSPI handle
 * @retval HAL state
 */
HAL_QSPI_StateTypeDef HAL_QSPI_GetState(QSPI_HandleTypeDef *hqspi)
{
    /* Return QSPI handle state */
    return (hqspi->State);
}


/**
 * @brief  Return the QSPI error code
 * @param  hqspi QSPI handle
 * @retval QSPI Error Code
 */
uint32_t HAL_QSPI_GetError(QSPI_HandleTypeDef *hqspi)
{
    return (hqspi->ErrorCode);
}


/**
 * @brief  Abort the current transmission
 * @param  hqspi QSPI handle
 * @retval HAL status
 */
HAL_StatusTypeDef HAL_QSPI_Abort(QSPI_HandleTypeDef *hqspi, uint32_t Timeout)
{
    HAL_StatusTypeDef   status      = HAL_OK;
    uint32_t            tickstart   = HAL_GetTick();

    /* Check if the state is in one of the busy states */
    if ((hqspi->State & 0x2) != 0) {
        /* Configure QSPI: Abort request */
        SET_BIT(hqspi->Instance->ABORTCFG, QSPI_ABORTCFG_EN);
        SET_BIT(hqspi->Instance->ABORTCFG, QSPI_ABORTCFG_ABORT);

        /* Wait until TC flag is set to go back in idle state */
        status = __QSPI_WaitFlagStateUntilTimeout(hqspi, QSPI_FLAG_TRANSOVER, SET, tickstart, Timeout);

        if (status == HAL_OK) {
            __HAL_QSPI_CLEAR_FLAG(hqspi, QSPI_FLAG_CLEAR_TRANSABORT);

            /* Update state */
            hqspi->State = HAL_QSPI_STATE_READY;
        }
    }

    return (status);
}


/**
 * @brief  Abort the current transmission (non-blocking function)
 * @param  hqspi QSPI handle
 * @retval HAL status
 */
HAL_StatusTypeDef HAL_QSPI_Abort_IT(QSPI_HandleTypeDef *hqspi)
{
    HAL_StatusTypeDef status = HAL_OK;

    /* Check if the state is in one of the busy states */
    if ((hqspi->State & 0x2) != 0) {
        /* Update QSPI state */
        hqspi->State = HAL_QSPI_STATE_ABORT;

        /* Disable all interrupts */
        __HAL_QSPI_DISABLE_IT(hqspi, QSPI_INT_ALL);

        /* Clear interrupt */
        __HAL_QSPI_CLEAR_FLAG(hqspi, QSPI_FLAG_CLEAR_TRANSABORT);

        /* Enable the QSPI Abort Interrupt */
        __HAL_QSPI_ENABLE_IT(hqspi, QSPI_INT_ABORT);

        /* Configure QSPI: Abort request */
        SET_BIT(hqspi->Instance->ABORTCFG, QSPI_ABORTCFG_EN);
        SET_BIT(hqspi->Instance->ABORTCFG, QSPI_ABORTCFG_ABORT);
    }

    return (status);
}


/**
 * @}
 */

/* Private functions ---------------------------------------------------------*/


/**
 * @brief  Wait for a flag state until timeout.
 * @param  hqspi QSPI handle
 * @param  Flag Flag checked
 * @param  State Value of the flag expected
 * @param  tickstart Start tick value
 * @param  Timeout Duration of the time out
 * @retval HAL status
 */
static HAL_StatusTypeDef __QSPI_WaitFlagStateUntilTimeout(QSPI_HandleTypeDef *hqspi, uint32_t Flag,
        FlagStatus State, uint32_t tickstart, uint32_t Timeout)
{
    /* Wait until flag is in expected state */
    while ((__HAL_QSPI_IS_ACTIVE_FLAG(hqspi->Instance->STATUS, Flag)) != State) {
        /* Check for the Timeout */
        if (Timeout != HAL_MAX_DELAY) {
            if ((Timeout == 0) || ((HAL_GetTick() - tickstart) > Timeout)) {
                hqspi->State        = HAL_QSPI_STATE_ERROR;
                hqspi->ErrorCode    |= HAL_QSPI_ERROR_TIMEOUT;

                return (HAL_ERROR);
            }
        }
    }
    return (HAL_OK);
}


/**
 * @brief   Wait for Txfifo state until timeout.
 * @param   hqspi QSPI handle
 * @param   tickstart Start tick value
 * @param   Timeout Duration of the time out
 * @retval HAL status
 */
static HAL_StatusTypeDef __QSPI_WaitTxFifoStateUntilTimeout(QSPI_HandleTypeDef *hqspi, uint32_t Flag,
        FlagStatus State, uint32_t tickstart, uint32_t Timeout)
{
    /* Wait until flag is in expected state */
    while ((__HAL_QSPI_IS_ACTIVE_FLAG(hqspi->Instance->TXFIFOS, Flag)) != State) {
        /* Check for the Timeout */
        if (Timeout != HAL_MAX_DELAY) {
            if ((Timeout == 0) || ((HAL_GetTick() - tickstart) > Timeout)) {
                hqspi->State        = HAL_QSPI_STATE_ERROR;
                hqspi->ErrorCode    |= HAL_QSPI_ERROR_TIMEOUT;

                return (HAL_ERROR);
            }
        }
    }
    return (HAL_OK);
}


/**
 * @brief  Wait for Rxfifo empty state until timeout.
 * @param  hqspi QSPI handle
 * @param  tickstart Start tick value
 * @param  Timeout Duration of the time out
 * @retval HAL status
 */
static HAL_StatusTypeDef __QSPI_WaitRxFifoEmptyUntilTimeout(QSPI_HandleTypeDef *hqspi,
        uint32_t tickstart, uint32_t Timeout)
{
    /* Wait until flag is in expected state */
    while ((hqspi->Instance->RXFIFOS & QSPI_FLAG_RxFIFO_CNT) == 0) {
        /* Check for the Timeout */
        if (Timeout != HAL_MAX_DELAY) {
            if ((Timeout == 0) || ((HAL_GetTick() - tickstart) > Timeout)) {
                hqspi->State        = HAL_QSPI_STATE_ERROR;
                hqspi->ErrorCode    |= HAL_QSPI_ERROR_TIMEOUT;

                return (HAL_ERROR);
            }
        }
    }
    return (HAL_OK);
}


/**
 * @brief Initialize the QSPI mode according to the specified parameters
 *         in the QSPI_InitTypeDef and initialize the associated handle.
 * @param QSPI Instance
 * @retval None
 */
static void __QSPI_Config(QSPI_HandleTypeDef *hqspi)
{
    /* Configure QSPI CGF */
    MODIFY_REG(hqspi->Instance->CFG, 0x27, hqspi->Init.AccessFlash | hqspi->Init.SpeedMode);

    /* Configure QSPI Data Order */
    MODIFY_REG(hqspi->Instance->DORDER, 0xC, hqspi->Init.EndianMode | hqspi->Init.SignificantBit);

    /* Configure QSPI mode */
    MODIFY_REG(hqspi->Instance->MODE, 0xB, hqspi->Init.WorkMode | hqspi->Init.IoMode);

    /* Configure QSPI SAMPLE */
    MODIFY_REG(hqspi->Instance->SAMPCFG, 0x3F, hqspi->Init.SamplePoint | hqspi->Init.SampleShift);

    /* Configure QSPI Clock Prescaler and Clock Mode */
    MODIFY_REG(hqspi->Instance->CLKCFG, 0xF, hqspi->Init.ClkMode | hqspi->Init.ClkDiv);

    /* Configure QSPI Txfifo */
    MODIFY_REG(hqspi->Instance->TXFIFOCFG, 0x7F, QSPI_TXFIFOCFG_EN | (hqspi->Init.TxfifoWm << QSPI_TXFIFOCFG_WM_Pos));

    /* Configure QSPI Rxfifo */
    MODIFY_REG(hqspi->Instance->RXFIFOCFG, 0x1FFF, QSPI_RXFIFOCFG_EN | (hqspi->Init.RxfifoWm << QSPI_RXFIFOCFG_QUADWM_Pos) | \
               (hqspi->Init.RxfifoWm << QSPI_RXFIFOCFG_NORMWM_Pos));

    /* Configure QSPI Stall */
    if (hqspi->Init.QuitStallCnt == LL_QSPI_STALL_QUITCNT_DISABLE) {
        MODIFY_REG(hqspi->Instance->ABORTCFG, QSPI_ABORTCFG_CSTALLEN, LL_QSPI_STALL_QUITCNT_DISABLE);
    } else {
        MODIFY_REG(hqspi->Instance->ABORTCFG, QSPI_ABORTCFG_CSTALLEN, LL_QSPI_STALL_QUITCNT_ENABLE);
        WRITE_REG(hqspi->Instance->SCNTTHR, hqspi->Init.QuitStallCnt);
    }
}


/**
 * @brief  Configure the communication registers.
 * @param  hqspi QSPI handle
 * @param  cmd structure that contains the command configuration information
 * @param  FunctionalMode functional mode to configured
 *           This parameter can be one of the following values:
 *            @arg QSPI_INDIRECT_WRITE: Indirect write mode
 *            @arg QSPI_INDIRECT_READ: Indirect read mode
 *            @arg QSPI_MEMORY_MAPPED: Memory map mode
 * @retval None
 */
static void __QSPI_CommunicateConfig(QSPI_HandleTypeDef *hqspi, QSPI_CommandTypeDef *cmd)
{
    /* Check the parameters */
    assert_param(IS_QSPI_FUNCTIONAL_MODE(cmd->TxOrRxData));
    assert_param(IS_QSPI_INSTRUCTION_LINES(cmd->InstructionLines));
    if (cmd->InstructionLines != QSPI_LINE_NONE) {
        assert_param(IS_QSPI_INSTRUCTION(cmd->Instruction));
    }

    assert_param(IS_QSPI_ADDRESS_LINES(cmd->AddressLines));
    if (cmd->AddressLines != QSPI_LINE_NONE) {
        assert_param(IS_QSPI_ADDRESS_SIZE(cmd->AddressSize));
    }

    assert_param(IS_QSPI_ALTERNATE_LINES(cmd->AlternateLines));
    if (cmd->AlternateLines != QSPI_LINE_NONE) {
        assert_param(IS_QSPI_ALTERNATE_SIZE(cmd->AlternateSize));
    }

    assert_param(IS_QSPI_DUMMY_CYCLES(cmd->DummyCycles));
    assert_param(IS_QSPI_DATA_LINES(cmd->DataLines));

    /* Clear all Flag */
    __HAL_QSPI_CLEAR_FLAG(hqspi, 0x4F);

    /* Configure single mode transfer */
    if (cmd->InstructionLines == QSPI_LINE_SINGLE || cmd->AddressLines == QSPI_LINE_SINGLE || \
        cmd->AlternateLines == QSPI_LINE_SINGLE || cmd->DataLines == QSPI_LINE_SINGLE) {
        SET_BIT(hqspi->Instance->CFG, QSPI_SINGLEMODE_ENABLE);
    } else {
        CLEAR_BIT(hqspi->Instance->CFG, QSPI_SINGLEMODE_ENABLE);
    }

    if (cmd->DataLines != QSPI_LINE_NONE) {
        /* Configure QSPI: the number of data to read or write */
        WRITE_REG(hqspi->Instance->DLEN, cmd->NumData);
    } else {
        WRITE_REG(hqspi->Instance->DLEN, 0);
    }

    if (cmd->InstructionLines != QSPI_LINE_NONE) {
        if (cmd->AlternateLines != QSPI_LINE_NONE) {
            /* Configure QSPI: alternate bytes  */
            WRITE_REG(hqspi->Instance->MODEALT, cmd->Alternate);

            if (cmd->AddressLines != QSPI_LINE_NONE) {
                /*---- Command with instruction, address and alternate bytes ----*/
                /* Configure QSPI: communications parameters*/
                WRITE_REG(hqspi->Instance->IOMODE, (cmd->AlternateSize << 15) | (cmd->AddressSize << 13) | (cmd->DummyCycles << 8) |
                          (cmd->DataLines << 6) | (cmd->AlternateLines << 4) | (cmd->AddressLines << 2) |
                          (cmd->InstructionLines));
                /* Configure QSPI: address */
                WRITE_REG(hqspi->Instance->FADDR, cmd->Address);

                /* Configure QSPI: instruction */
                WRITE_REG(hqspi->Instance->CMD, cmd->Instruction | cmd->TxOrRxData);
            } else {
                /*---- Command with instruction and alternate bytes ----*/
                /* Configure QSPI: communications parameters*/
                WRITE_REG(hqspi->Instance->IOMODE, (cmd->AlternateSize << 15) | (cmd->DummyCycles << 8) |
                          (cmd->DataLines << 6) | (cmd->AlternateLines << 4) |
                          (cmd->InstructionLines));
                /* Configure QSPI: instruction */
                WRITE_REG(hqspi->Instance->CMD, cmd->Instruction | cmd->TxOrRxData);
            }
        } else {
            if (cmd->AddressLines != QSPI_LINE_NONE) {
                /*---- Command with instruction and address ----*/
                /* Configure QSPI: communications parameters*/
                WRITE_REG(hqspi->Instance->IOMODE, (cmd->AddressSize << 13) | (cmd->DummyCycles << 8) |
                          (cmd->DataLines << 6) | (cmd->AddressLines << 2) |
                          (cmd->InstructionLines));
                /* Configure QSPI: address */
                WRITE_REG(hqspi->Instance->FADDR, cmd->Address);
                /* Configure QSPI: instruction */
                WRITE_REG(hqspi->Instance->CMD, cmd->Instruction | cmd->TxOrRxData);
            } else {
                /*---- Command with only instruction ----*/
                /* Configure QSPI: communications parameters*/
                WRITE_REG(hqspi->Instance->IOMODE, (cmd->DummyCycles << 8) |
                          (cmd->DataLines << 6) |
                          (cmd->InstructionLines));
                /* Configure QSPI: instruction */
                WRITE_REG(hqspi->Instance->CMD, cmd->Instruction | cmd->TxOrRxData);
            }
        }
    }
}


/**
 * @}
 */

#endif /* QSPI_MODULE_ENABLED */


/**
 * @}
 */


/**
 * @}
 */

/************************ (C) COPYRIGHT FudanMicroelectronics *****END OF FILE****/
