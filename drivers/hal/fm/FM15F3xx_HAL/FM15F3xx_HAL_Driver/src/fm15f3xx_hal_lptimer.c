/**
 ******************************************************************************
 * @file    fm15f3xx_hal_lptimer.c
 * @author  MCD Application Team
 * @brief   LPTIM HAL module driver.
 *          This file provides firmware functions to manage the following
 *          functionalities of the Low Power Timer (LPTIM) peripheral:
 *           + Initialization and de-initialization functions
 *           + IO operation functions
 *           + Peripheral State, Mode and Error functions
 *
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2020 FudanMicroelectronics.
 * All rights reserved.</center></h2>
 *
 ******************************************************************************
 */

/* Includes ------------------------------------------------------------------*/
#include "fm15f3xx_hal.h"


/** @addtogroup FM15F3xx_HAL_Driver
 * @{
 */


/** @defgroup LPTIM LPTIM
 * @brief LPTIM HAL module driver.
 * @{
 */

#ifdef LPTIM_MODULE_ENABLED
/* Private types -------------------------------------------------------------*/


/** @defgroup LPTIM_Private_Types LPTIM Private Types
 * @{
 */


/**
 * @}
 */

/* Private defines -----------------------------------------------------------*/


/** @defgroup LPTIM_Private_Defines LPTIM Private Defines
 * @{
 */


/**
 * @}
 */

/* Private variables ---------------------------------------------------------*/


/** @addtogroup LPTIM_Private_Variables LPTIM Private Variables
 * @{
 */


/**
 * @}
 */

/* Private constants ---------------------------------------------------------*/


/** @addtogroup LPTIM_Private_Constants LPTIM Private Constants
 * @{
 */


/**
 * @}
 */

/* Private macros ------------------------------------------------------------*/


/** @addtogroup LPTIM_Private_Macros LPTIM Private Macros
 * @{
 */


/**
 * @}
 */

/* Private function prototypes -----------------------------------------------*/


/** @addtogroup LPTIM_Private_Functions_Prototypes LPTIM Private Functions Prototypes
 * @{
 */


/**
 * @}
 */

/* Private functions ---------------------------------------------------------*/


/** @addtogroup LPTIM_Private_Functions LPTIM Private Functions
 * @{
 */
static void __LPTIM_Config(LPTIM_HandleTypeDef *hlptim);


/**
 * @}
 */

/* Exported functions ---------------------------------------------------------*/


/** @defgroup LPTIM_Exported_Functions LPTIM Exported Functions
 * @{
 */


/** @defgroup LPTIM_Group1 Initialization/de-initialization functions
 *  @brief    Initialization and Configuration functions.
 *
   @verbatim
   ==============================================================================
 ##### Initialization and de-initialization functions #####
   ==============================================================================
   @endverbatim
 * @{
 */


/**
 * @brief  Initializes the LPTIM according to the specified parameters in the
 *         LPTIM_InitTypeDef and creates the associated handle.
 * @param  hlptim LPTIM handle
 * @retval HAL status
 */
HAL_StatusTypeDef HAL_LPTIM_Init(LPTIM_HandleTypeDef *hlptim)
{
    /* Check the LPTIM handle allocation */
    if (hlptim == NULL) {
        return (HAL_ERROR);
    }

    /* Check the parameters */
    assert_param(IS_LPTIM_INSTANCE(hlptim->Instance));
    assert_param(IS_LPTIM_CLOCK_SOURCE(hlptim->Init.Clock));
    if (LPTIM_CLKSRC_PAD == hlptim->Init.Clock) {
        assert_param(IS_LPTIM_CLOCK_PAD_CLK(hlptim->Init.PadClock.Pad));
    }
    assert_param(IS_LPTIM_WORK_MODE(hlptim->Init.WorkMode));
    if (LPTIM_WORKMODE_TRIG == hlptim->Init.WorkMode) {
        assert_param(IS_LPTIM_CLOCK_PRESCALER_TRIG(hlptim->Init.Prescaler));
        assert_param(IS_LPTIM_TRIG_SOURCE(hlptim->Init.Trigger.TrigSrc));
        assert_param(IS_LPTIM_INPUT_INVERT(hlptim->Init.Trigger.InputInvert));
    } else {
        assert_param(IS_LPTIM_CLOCK_PRESCALER_CNT(hlptim->Init.Prescaler));
    }

    if (hlptim->State == HAL_LPTIM_STATE_RESET) {
        /* Init the low level hardware */
        HAL_LPTIM_MspInit(hlptim);
    }

    /* Change the LPTIM state */
    hlptim->State = HAL_LPTIM_STATE_BUSY;

    /* Configure LPTIM */
    __LPTIM_Config(hlptim);

    /* Change the LPTIM state */
    hlptim->State = HAL_LPTIM_STATE_READY;

    /* Return function status */
    return (HAL_OK);
}


/**
 * @brief  DeInitializes the LPTIM peripheral.
 * @param  hlptim LPTIM handle
 * @retval HAL status
 */
HAL_StatusTypeDef HAL_LPTIM_DeInit(LPTIM_HandleTypeDef *hlptim)
{
    /* Check the LPTIM handle allocation */
    if (hlptim == NULL) {
        return (HAL_ERROR);
    }

    /* Change the LPTIM state */
    hlptim->State = HAL_LPTIM_STATE_BUSY;

    /* Disable the LPTIM Peripheral Clock */
    __HAL_LPTIM_DISABLE(hlptim);

    /* DeInit the low level hardware: CLOCK, NVIC.*/
    HAL_LPTIM_MspDeInit(hlptim);

    /* Change the LPTIM state */
    hlptim->State = HAL_LPTIM_STATE_RESET;

    /* Return function status */
    return (HAL_OK);
}


/**
 * @brief  Initializes the LPTIM MSP.
 * @param  hlptim LPTIM handle
 * @retval None
 */
__weak void HAL_LPTIM_MspInit(LPTIM_HandleTypeDef *hlptim)
{
    /* Prevent unused argument(s) compilation warning */
    UNUSED(hlptim);


    /* NOTE : This function Should not be modified, when the callback is needed,
              the HAL_LPTIM_MspInit could be implemented in the user file
     */
}


/**
 * @brief  DeInitializes LPTIM MSP.
 * @param  hlptim LPTIM handle
 * @retval None
 */
__weak void HAL_LPTIM_MspDeInit(LPTIM_HandleTypeDef *hlptim)
{
    /* Prevent unused argument(s) compilation warning */
    UNUSED(hlptim);


    /* NOTE : This function Should not be modified, when the callback is needed,
              the HAL_LPTIM_MspDeInit could be implemented in the user file
     */
}


/**
 * @}
 */


/**
 * @brief  Starts the LPTIM Trigger generation.
 * @param  hlptim  LPTIM handle
 * @param  Period  Specifies the Autoreload value.
 *         This parameter must be a value between 0x0000 and 0xFFFF.
 * @retval HAL status
 */
HAL_StatusTypeDef HAL_LPTIM_TRIG_Start(LPTIM_HandleTypeDef *hlptim, uint16_t Period)
{
    /* Check the parameters */
    assert_param(IS_LPTIM_INSTANCE(hlptim->Instance));
    assert_param(IS_LPTIM_AUTORELOAD(Period));

    /* Set the LPTIM state */
    hlptim->State = HAL_LPTIM_STATE_BUSY;

    /* Load the period value in the autoreload register */
    __HAL_LPTIM_AUTORELOAD_SET(hlptim, Period);

    /* Enable the Peripheral */
    __HAL_LPTIM_ENABLE(hlptim);

    /* Change the TIM state*/
    hlptim->State = HAL_LPTIM_STATE_READY;

    /* Return function status */
    return (HAL_OK);
}


/**
 * @brief  Stops the LPTIM Trigger generation.
 * @param  hlptim  LPTIM handle
 * @retval HAL status
 */
HAL_StatusTypeDef HAL_LPTIM_TRIG_Stop(LPTIM_HandleTypeDef *hlptim)
{
    /* Check the parameters */
    assert_param(IS_LPTIM_INSTANCE(hlptim->Instance));

    /* Set the LPTIM state */
    hlptim->State = HAL_LPTIM_STATE_BUSY;

    /* Disable the Peripheral */
    __HAL_LPTIM_DISABLE(hlptim);

    /* Change the TIM state*/
    hlptim->State = HAL_LPTIM_STATE_READY;

    /* Return function status */
    return (HAL_OK);
}


/**
 * @brief  Starts the LPTIM Trigger generation in interrupt mode.
 * @param  hlptim  LPTIM handle
 * @param  Period  Specifies the Autoreload value.
 *         This parameter must be a value between 0x0000 and 0xFFFF.
 * @retval HAL status
 */
HAL_StatusTypeDef HAL_LPTIM_TRIG_Start_IT(LPTIM_HandleTypeDef *hlptim, uint16_t Period)
{
    /* Check the parameters */
    assert_param(IS_LPTIM_INSTANCE(hlptim->Instance));
    assert_param(IS_LPTIM_AUTORELOAD(Period));

    /* Set the LPTIM state */
    hlptim->State = HAL_LPTIM_STATE_BUSY;

    /* Load the period value in the autoreload register */
    __HAL_LPTIM_AUTORELOAD_SET(hlptim, Period);

    /* Enable Compare interrupt */
    __HAL_LPTIM_ENABLE_IT(hlptim);

    /* Enable the Peripheral */
    __HAL_LPTIM_ENABLE(hlptim);

    /* Change the TIM state*/
    hlptim->State = HAL_LPTIM_STATE_READY;

    /* Return function status */
    return (HAL_OK);
}


/**
 * @brief  Stops the LPTIM Trigger generation in interrupt mode.
 * @param  hlptim  LPTIM handle
 * @retval HAL status
 */
HAL_StatusTypeDef HAL_LPTIM_TRIG_Stop_IT(LPTIM_HandleTypeDef *hlptim)
{
    /* Check the parameters */
    assert_param(IS_LPTIM_INSTANCE(hlptim->Instance));

    /* Set the LPTIM state */
    hlptim->State = HAL_LPTIM_STATE_BUSY;

    /* Disable the Peripheral */
    __HAL_LPTIM_DISABLE(hlptim);

    /* Disable interrupt */
    __HAL_LPTIM_DISABLE_IT(hlptim);

    /* Change the TIM state*/
    hlptim->State = HAL_LPTIM_STATE_READY;

    /* Return function status */
    return (HAL_OK);
}


/**
 * @brief  Starts the Counter mode.
 * @param  hlptim  LPTIM handle
 * @param  Period  Specifies the Autoreload value.
 *         This parameter must be a value between 0x0000 and 0xFFFF.
 * @retval HAL status
 */
HAL_StatusTypeDef HAL_LPTIM_Counter_Start(LPTIM_HandleTypeDef *hlptim, uint16_t Period)
{
    /* Check the parameters */
    assert_param(IS_LPTIM_INSTANCE(hlptim->Instance));
    assert_param(IS_LPTIM_AUTORELOAD(Period));

    /* Set the LPTIM state */
    hlptim->State = HAL_LPTIM_STATE_BUSY;

    /* Load the period value in the autoreload register */
    __HAL_LPTIM_AUTORELOAD_SET(hlptim, Period);

    /* Enable the Peripheral */
    __HAL_LPTIM_ENABLE(hlptim);

    /* Change the TIM state*/
    hlptim->State = HAL_LPTIM_STATE_READY;

    /* Return function status */
    return (HAL_OK);
}


/**
 * @brief  Stops the Counter mode.
 * @param  hlptim  LPTIM handle
 * @retval HAL status
 */
HAL_StatusTypeDef HAL_LPTIM_Counter_Stop(LPTIM_HandleTypeDef *hlptim)
{
    /* Check the parameters */
    assert_param(IS_LPTIM_INSTANCE(hlptim->Instance));

    /* Set the LPTIM state */
    hlptim->State = HAL_LPTIM_STATE_BUSY;

    /* Disable the Peripheral */
    __HAL_LPTIM_DISABLE(hlptim);

    /* Change the TIM state*/
    hlptim->State = HAL_LPTIM_STATE_READY;

    /* Return function status */
    return (HAL_OK);
}


/**
 * @brief  Starts the Counter mode in interrupt mode.
 * @param  hlptim  LPTIM handle
 * @param  Period  Specifies the Autoreload value.
 *         This parameter must be a value between 0x0000 and 0xFFFF.
 * @retval HAL status
 */
HAL_StatusTypeDef HAL_LPTIM_Counter_Start_IT(LPTIM_HandleTypeDef *hlptim, uint16_t Period)
{
    /* Check the parameters */
    assert_param(IS_LPTIM_INSTANCE(hlptim->Instance));
    assert_param(IS_LPTIM_AUTORELOAD(Period));

    /* Set the LPTIM state */
    hlptim->State = HAL_LPTIM_STATE_BUSY;

    /* Load the period value in the autoreload register */
    __HAL_LPTIM_AUTORELOAD_SET(hlptim, Period);

    /* Enable interrupt */
    __HAL_LPTIM_ENABLE_IT(hlptim);

    /* Enable the Peripheral */
    __HAL_LPTIM_ENABLE(hlptim);

    /* Change the TIM state*/
    hlptim->State = HAL_LPTIM_STATE_READY;

    /* Return function status */
    return (HAL_OK);
}


/**
 * @brief  Stops the Counter mode in interrupt mode.
 * @param  hlptim  LPTIM handle
 * @retval HAL status
 */
HAL_StatusTypeDef HAL_LPTIM_Counter_Stop_IT(LPTIM_HandleTypeDef *hlptim)
{
    /* Check the parameters */
    assert_param(IS_LPTIM_INSTANCE(hlptim->Instance));

    /* Set the LPTIM state */
    hlptim->State = HAL_LPTIM_STATE_BUSY;

    /* Disable the Peripheral */
    __HAL_LPTIM_DISABLE(hlptim);

    /* Disable interrupt */
    __HAL_LPTIM_DISABLE_IT(hlptim);

    /* Change the TIM state*/
    hlptim->State = HAL_LPTIM_STATE_READY;

    /* Return function status */
    return (HAL_OK);
}


/**
 * @}
 */


/** @defgroup LPTIM_Group3 LPTIM Read operation functions
 *  @brief  Read operation functions.
 *
   @verbatim
   ==============================================================================
 ##### LPTIM Read operation functions #####
   ==============================================================================
   @endverbatim
 * @{
 */


/**
 * @brief  This function returns the current counter value.
 * @param  hlptim LPTIM handle
 * @retval Counter value.
 */
uint32_t HAL_LPTIM_ReadCounter(LPTIM_HandleTypeDef *hlptim)
{
    /* Check the parameters */
    assert_param(IS_LPTIM_INSTANCE(hlptim->Instance));

    return (hlptim->Instance->CVAL);
}


/**
 * @brief  This function return the current Autoreload (Period) value.
 * @param  hlptim LPTIM handle
 * @retval Autoreload value.
 */
uint32_t HAL_LPTIM_ReadAutoReload(LPTIM_HandleTypeDef *hlptim)
{
    /* Check the parameters */
    assert_param(IS_LPTIM_INSTANCE(hlptim->Instance));

    return (hlptim->Instance->MOD);
}


/**
 * @}
 */


/** @defgroup LPTIM_Group4 LPTIM IRQ handler
 *  @brief  LPTIM  IRQ handler.
 *
   @verbatim
   ==============================================================================
 ##### LPTIM IRQ handler  #####
   ==============================================================================
   [..]  This section provides LPTIM IRQ handler function.

   @endverbatim
 * @{
 */


/**
 * @brief  This function handles LPTIM interrupt request.
 * @param  hlptim LPTIM handle
 * @retval None
 */
void HAL_LPTIM_IRQHandler(LPTIM_HandleTypeDef *hlptim)
{
    /* Compare match interrupt */
    if (__HAL_LPTIM_GET_FLAG_OVER(hlptim) != 0) {
        if (__HAL_LPTIM_GET_IT_SOURCE(hlptim) != 0) {
            /* Clear Compare match flag */
            __HAL_LPTIM_CLEAR_FLAG_OVER(hlptim);
            /* Compare Over Callback */
            HAL_LPTIM_OverCallback(hlptim);
        }
    }
}


/**
 * @brief  Compare match callback in non blocking mode
 * @param  hlptim  LPTIM handle
 * @retval None
 */
__weak void HAL_LPTIM_OverCallback(LPTIM_HandleTypeDef *hlptim)
{
    /* Prevent unused argument(s) compilation warning */
    UNUSED(hlptim);


    /* NOTE : This function Should not be modified, when the callback is needed,
              the HAL_LPTIM_CompareMatchCallback could be implemented in the user file
     */
}


/**
 * @}
 */


/** @defgroup LPTIM_Group5 Peripheral State functions
 *  @brief   Peripheral State functions.
 *
   @verbatim
   ==============================================================================
 ##### Peripheral State functions #####
   ==============================================================================
    [..]
    This subsection permits to get in run-time the status of the peripheral.

   @endverbatim
 * @{
 */


/**
 * @brief  Returns the LPTIM state.
 * @param  hlptim LPTIM handle
 * @retval HAL state
 */
HAL_LPTIM_StateTypeDef HAL_LPTIM_GetState(LPTIM_HandleTypeDef *hlptim)
{
    return (hlptim->State);
}


/**
 * @}
 */


static void __LPTIM_Config(LPTIM_HandleTypeDef *hlptim)
{
    /* Reset LPTIM */
    CLEAR_BIT(hlptim->Instance->CTRL, LPTIMER_CTRL_CRSTN);
    SET_BIT(hlptim->Instance->CTRL, LPTIMER_CTRL_CRSTN);

    /* Configure LPTIM CGF */

    if (LPTIM_WORKMODE_TRIG == hlptim->Init.WorkMode) {
        MODIFY_REG(hlptim->Instance->CFG, 0x1F7, hlptim->Init.WorkMode | hlptim->Init.Trigger.TrigSrc | hlptim->Init.Trigger.InputInvert);
    } else {
        MODIFY_REG(hlptim->Instance->CFG, 0x1F7, hlptim->Init.WorkMode);
    }

    /* Configure LPTIM CLKCFG */
    MODIFY_REG(hlptim->Instance->CLKCFG, 0xFFF, hlptim->Init.Clock | hlptim->Init.Prescaler);
}


/**
 * @}
 */

#endif /* LPTIM_MODULE_ENABLED */


/**
 * @}
 */


/**
 * @}
 */

/************************ (C) COPYRIGHT FudanMicroelectronics *****END OF FILE****/
