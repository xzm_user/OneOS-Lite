/**
 ******************************************************************************
 * @file    fm15f3xx_ll_spi.c
 * @author  SRG
 * @version V1.0.0
 * @date    2020-03-17
 * @brief
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2020 FudanMicroelectronics.
 * All rights reserved.</center></h2>
 *
 ******************************************************************************
 */
#include "fm15f3xx_ll_spi.h"


/** @addtogroup FM15f3XX_LL_Driver
 * @{
 */

#if defined (SPI0) || defined (SPI1) || defined (SPI2) || defined (SPI3)


/** @addtogroup ATIM_LL
 * @{
 */

/* Private types -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private constants ---------------------------------------------------------*/

/* Private macros ------------------------------------------------------------*/


/** @addtogroup ATIM_LL_Private_Macros
 * @{
 */


/**
 * @}
 */


/**
 * @brief  Set each @ref LL_SPI_InitParamTypeDef field to default value.
 * @param  SPI_InitParamStruct pointer to a @ref LL_SPI_InitParamTypeDef structure
 * whose fields will be set to default values.
 * @retval None
 */
void LL_SPI_StructInitParam(LL_SPI_InitParamTypeDef *SPI_InitParamStruct)
{
    /* Set SPI_InitParamStruct fields to default values */
    /*SPCR1*/
    SPI_InitParamStruct->ClockPhase         = LL_SPI_CPHA_1EDGE;                //LL_SPI_CPHA_2Edge;LL_SPI_CPHA_1Edge
    SPI_InitParamStruct->ClockPolarity      = LL_SPI_CPOL_LOW;                  //LL_SPI_CPOL_High;LL_SPI_CPOL_Low
    SPI_InitParamStruct->Mode               = LL_SPI_MODE_MASTER;               //LL_SPI_Mode_Slave;LL_SPI_Mode_Master
    SPI_InitParamStruct->BitOrder           = LL_SPI_MSB_FIRST;                 //LL_SPI_LSB_First;LL_SPI_MSB_First
    SPI_InitParamStruct->SSNMode            = LL_SPI_SSNMODE_HIGH;              //LL_SPI_SSNMode_High;LL_SPI_SSNMode_Low
    SPI_InitParamStruct->BaudRate           = LL_SPI_BAUDRATE_DIV4;             //LL_SPI_BaudRatePrescaler_DIV2;...;LL_SPI_BaudRatePrescaler_DIV256;
    SPI_InitParamStruct->T_BYTE             = LL_SPI_TBYTE_1BYTE;               //LL_SPI_TByte_2byte;LL_SPI_TByte_3byte;LL_SPI_TByte_4byte
    SPI_InitParamStruct->WAIT_CNT           = LL_SPI_WAITCNT_0CYCLE;            //LL_SPI_WaitCNT_1cycle;LL_SPI_WaitCNT_2cycle;LL_SPI_WaitCNT_3cycle
    SPI_InitParamStruct->SSN_MCU_EN         = LL_SPI_SSNMCUEN_HARDWARE;         //LL_SPI_SSNMCUEN_Mcu;LL_SPI_SSNMCUEN_Hardware
    SPI_InitParamStruct->SPI_EN             = LL_SPI_SPIEN_DISABLE;             //LL_SPI_SPIEN_Enable;LL_SPI_SPIEN_Disable
    SPI_InitParamStruct->TXONLY             = LL_SPI_TXONLY_DISABLE;            //LL_SPI_TXONLY_Enable
    SPI_InitParamStruct->SamplePosition     = LL_SPI_SAMPLEPOSITION_NORMAL;     //LL_SPI_SamplePosition_DelayHalfCycle;LL_SPI_SamplePosition_Normal
    SPI_InitParamStruct->SlaveDataSendMode  = LL_SPI_SLAVEDATASENDMODE_NORMAL;  //LL_SPI_SlaveDataSendMode_AheadHalfCycle
    SPI_InitParamStruct->SignalFilterSSN    = LL_SPI_SIGNALFILTERSSN_DISABLE;   //LL_SPI_SignalFilterSSN_Enable;
    SPI_InitParamStruct->SignalFilterSCK    = LL_SPI_SIGNALFILTERSCK_DISABLE;   //LL_SPI_SignalFilterSCK_Enable
    SPI_InitParamStruct->SignalFilterMOSI   = LL_SPI_SIGNALFILTERMOSI_DISABLE;  //LL_SPI_SignalFilterMOSI_Enable
    SPI_InitParamStruct->SSNODEN            = LL_SPI_SSNODEN_DISABLE;           //LL_SPI_SSNODEn_Disable
    /*SPCR2*/
    SPI_InitParamStruct->SSNMCU = LL_SPI_SSNMCU_HIGH;                           //LL_SPI_SSNMCU_Low;LL_SPI_SSNMCU_High
    /*SPCR3*/
    SPI_InitParamStruct->TXONLY_EN = LL_SPI_TXONLYEN_DISABLE;                   //LL_SPI_TXONLYEN_Disable;LL_SPI_TXONLYEN_Enable
    /*SPIIE*/
    SPI_InitParamStruct->RXNEIE     = LL_SPI_RXNEIE_DISABLE;                    //LL_SPI_RXNEIE_Enable
    SPI_InitParamStruct->TXEIE      = LL_SPI_TXEIE_DISABLE;                     //LL_SPI_TXEIE_Enable
    SPI_InitParamStruct->RXFHALFIE  = LL_SPI_RXFHALFIE_DISABLE;                 //LL_SPI_RXFHalfIE_Enable
    SPI_InitParamStruct->TXEHalfIE  = LL_SPI_TXEHALFIE_DISABLE;                 //LL_SPI_TXEHalfIE_Enable
    SPI_InitParamStruct->ErrorIE    = LL_SPI_ERRORIE_DISABLE;                   //LL_SPI_ErrorIE_Enable
}


void LL_SPIx_Init(SPI_TypeDef *SPIx, LL_SPI_InitParamTypeDef *SPI_InitParamStruct)
{
    MODIFY_REG(SPIx->SPCR1,
               SPI_SPCR1_CPHA | SPI_SPCR1_CPOL | SPI_SPCR1_MSTR | SPI_SPCR1_LSBFIRST | SPI_SPCR1_SSNMODE |
               SPI_SPCR1_BR | SPI_SPCR1_TBYTE | SPI_SPCR1_WAITCNT | SPI_SPCR1_SSNMCUEN | SPI_SPCR1_SPIEN |
               SPI_SPCR1_TXONLY | SPI_SPCR1_SAMPLEP | SPI_SPCR1_SDOUTSEND | SPI_SPCR1_SSNFILT |
               SPI_SPCR1_SCKFILT | SPI_SPCR1_MOSIFILT | SPI_SPCR1_SSNODEN,

               SPI_InitParamStruct->ClockPhase | SPI_InitParamStruct->ClockPolarity | SPI_InitParamStruct->Mode | SPI_InitParamStruct->BitOrder | SPI_InitParamStruct->SSNMode | SPI_InitParamStruct->BaudRate |
               SPI_InitParamStruct->T_BYTE | SPI_InitParamStruct->WAIT_CNT | SPI_InitParamStruct->SSN_MCU_EN | SPI_InitParamStruct->SPI_EN | SPI_InitParamStruct->TXONLY | SPI_InitParamStruct->SamplePosition |
               SPI_InitParamStruct->SlaveDataSendMode | SPI_InitParamStruct->SignalFilterSSN | SPI_InitParamStruct->SignalFilterSCK | SPI_InitParamStruct->SignalFilterMOSI | SPI_InitParamStruct->SSNODEN);

    MODIFY_REG(SPIx->SPCR2, SPI_SPCR2_SSNMCU, SPI_InitParamStruct->SSNMCU);

    MODIFY_REG(SPIx->SPCR3, SPI_SPCR3_TXONLYEN, SPI_InitParamStruct->TXONLY_EN);

    MODIFY_REG(SPIx->SPIIE,
               SPI_SPIIE_RXNEIE | SPI_SPIIE_TXEIE | SPI_SPIIE_RXHFIE | SPI_SPIIE_TXHEIE | SPI_SPIIE_ERRIE,
               SPI_InitParamStruct->RXNEIE | SPI_InitParamStruct->TXEIE | SPI_InitParamStruct->RXFHALFIE | SPI_InitParamStruct->TXEHalfIE | SPI_InitParamStruct->ErrorIE);
}


/* Private function prototypes -----------------------------------------------*/


/**
 * @}
 */


/**
 * @}
 */

#endif /* SPIx */

/************************ (C) COPYRIGHT FudanMicroelectronics *****END OF FILE****/

