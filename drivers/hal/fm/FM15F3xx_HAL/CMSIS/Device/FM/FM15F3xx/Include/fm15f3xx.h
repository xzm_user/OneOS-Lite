

/****************************************************************************************************//**
 * @file     fm15f3xx.h
 *
 * @brief    CMSIS Cortex-M3 Peripheral Access Layer Header File for
 *           fm15f3xx from Keil.
 *
 * @version  V1.4
 * @date     2019.08.12
 *
 * @note     Generated with SVDConv V2.87e
 *           from CMSIS SVD File 'fm15f3xx.svd' Version 1.2,
 *
 * @par      ARM Limited (ARM) is supplying this software for use with Cortex-M
 *           processor based microcontroller, but can be equally used for other
 *           suitable processor architectures. This file can be freely distributed.
 *           Modifications to this file shall be clearly marked.
 *
 *           THIS SOFTWARE IS PROVIDED "AS IS". NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 *           OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 *           MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 *           ARM SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL, OR
 *           CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 *******************************************************************************************************/


/** @addtogroup CMSIS
 * @{
 */


/** @addtogroup fm15f3xx
 * @{
 */

#ifndef __FM15F3XX_H
#define __FM15F3XX_H

#ifdef __cplusplus
extern "C" {
#endif

// <<< Use Configuration Wizard in Context Menu >>>


/** @addtogroup Exported_types
 * @{
 */
typedef enum {
    RESET   = 0,
    SET     = !RESET
} FlagStatus, ITStatus;

typedef enum {
    DISABLE = 0,
    ENABLE  = !DISABLE
} FunctionalState;
#define IS_FUNCTIONAL_STATE( STATE ) ( ( (STATE) == DISABLE) || ( (STATE) == ENABLE) )
typedef enum {
    ERROR   = 0,
    SUCCESS = !ERROR
} ErrorStatus;

#ifndef FALSE
#define FALSE 0
#endif
#ifndef TRUE
#define TRUE !FALSE
#endif


/**
 * @}
 */


/** @addtogroup Exported_macro
 * @{
 */
#define SET_BIT( REG, BIT ) ( (REG) |= (BIT) )

#define CLEAR_BIT( REG, BIT ) ( (REG) &= ~(BIT) )

#define READ_BIT( REG, BIT ) ( (REG) &(BIT) )

#define CLEAR_REG( REG ) ( (REG) = (0x0) )

#define WRITE_REG( REG, VAL ) ( (REG) = (VAL) )

#define READ_REG( REG ) ( (REG) )

#define MODIFY_REG( REG, CLEARMASK, SETMASK ) WRITE_REG( (REG), ( ( (READ_REG( REG ) ) & (~(CLEARMASK) ) ) | ( (CLEARMASK) &(SETMASK) ) ) )
//#define MODIFY_REG(REG, CLEARMASK, SETMASK)  WRITE_REG((REG), (((READ_REG(REG)) & (~(CLEARMASK))) | (SETMASK)))

#define POSITION_VAL( VAL ) (__CLZ( __RBIT( VAL ) ) )


/** @addtogroup Configuration_of_CMSIS
 * @{
 */


/* ================================================================================ */
/* ================      Processor and Core Peripheral Section     ================ */
/* ================================================================================ */

/* ----------------Configuration of the Cortex-M3 Processor and Core Peripherals---------------- */
#define __CM3_REV               0x0201              /*!< Cortex-M3 Core Revision                      */
#define __MPU_PRESENT           0                   /*!< MPU present or not                           */
#define __FPU_PRESENT           0                   /*!< FPU present or not                           */
#define __NVIC_PRIO_BITS        4                   /*!< Number of Bits used for Priority Levels      */
#define __Vendor_SysTickConfig  0                   /*!< Set to 1 if different SysTick Config is used */
/** @} */ /* End of group Configuration_of_CMSIS */


/** @addtogroup Peripheral_interrupt_number_definition
 * @{
 */
//<h>Interrupt Number Definition


/**
 * @brief fm15f3xx Interrupt Number Definition, according to the selected device
 *        in @ref Library_configuration_section
 */
/* -------------------------  Interrupt Number Definition  ------------------------ */

typedef enum {
    /* -------------------  Cortex-M3 Processor Exceptions Numbers  ------------------- */
    NonMaskableInt_IRQn     = -14,      /*!< 2 Non Maskable Interrupt                                          */
    HardFault_IRQn          = -13,      /*!< 3 Cortex-M3 Hard Fault Interrupt */
    MemoryManagement_IRQn   = -12,      /*!< 4 Cortex-M3 Memory Management Interrupt                           */
    BusFault_IRQn           = -11,      /*!< 5 Cortex-M3 Bus Fault Interrupt                                   */
    UsageFault_IRQn         = -10,      /*!< 6 Cortex-M3 Usage Fault Interrupt                                 */
    SVCall_IRQn             = -5,       /*!< 11 Cortex-M3 SV Call Interrupt                                    */
    DebugMonitor_IRQn       = -4,       /*!< 12 Cortex-M3 Debug Monitor Interrupt                              */
    PendSV_IRQn             = -2,       /*!< 14 Cortex-M3 Pend SV Interrupt                                    */
    SysTick_IRQn            = -1,       /*!< 15 Cortex-M3 System Tick Interrupt                                */
    /* ----------------------  fm15f3xx Specific Interrupt Numbers  ---------------------- */
    DMA_Channel0_IRQn   = 0,            /*!< DMA channel 0 transfer complete                                   */
    DMA_Channel1_IRQn   = 1,            /*!< DMA channel 1 transfer complete                                   */
    DMA_Channel2_IRQn   = 2,            /*!< DMA channel 2 transfer complete                                   */
    DMA_Channel3_IRQn   = 3,            /*!< DMA channel 3 transfer complete                                   */
    DMA_Channel4_IRQn   = 4,            /*!< DMA channel 4 transfer complete                                   */
    DMA_Channel5_IRQn   = 5,            /*!< DMA channel 5 transfer complete                                   */
    DMA_Channel6_IRQn   = 6,            /*!< DMA channel 6 transfer complete                                   */
    DMA_Channel7_IRQn   = 7,            /*!< DMA channel 7 transfer complete                                   */
    DMA_Error_IRQn      = 8,            /*!< DMA channel 0 - 7 error                                           */
    PAE_IRQn            = 9,            /*!< PAE interrupt                                                     */
    HASH_IRQn           = 10,           /*!< HASH interrupt                                                    */
    BCA_IRQn            = 11,           /*!< BCA interrupt                                                     */
    DSP_IRQn            = 12,           /*!< DSP interrupt                                                     */
    DSP_Error_IRQn      = 13,           /*!< Dsp error interrupt                                               */
    FLASH_Int_IRQn      = 14,           /*!< Flash         interrupt                                           */
    FLASH_RdcolInt_IRQn = 15,           /*!< Flash read collision interrupt                                    */
    FLASH_LvtInt_IRQn   = 16,           /*!< Flash low V interrupt                                             */
    PM_Int_IRQn         = 17,           /*!< PM interrupt                                                      */
    RANDOM_IRQn         = 18,           /*!< Random  interrupt                                                 */
    Reserved35_IRQn     = 19,           /*!< Reserved interrupt                                                */
    SPI0_IRQn           = 20,           /*!< SPI0 interrupt                                                    */
    SPI1_IRQn           = 21,           /*!< SPI1 interrupt                                                    */
    SPI2_IRQn           = 22,           /*!< SPI2 interrupt                                                    */
    SPI3_IRQn           = 23,           /*!< SPI3 interrupt                                                    */
    UART0_IRQn          = 24,           /*!< UART0 interrupt                                                   */
    UART1_IRQn          = 25,           /*!< UART1 interrupt                                                   */
    UART2_IRQn          = 26,           /*!< UART2 interrupt                                                   */
    Reserved43_IRQn     = 27,           /*!< Reserved interrupt                                                */
    I2C0_IRQn           = 28,           /*!< I2C0 interrupt                                                    */
    I2C1_IRQn           = 29,           /*!< I2C1 interrupt                                                    */
    Reserved46_IRQn     = 30,           /*!< Reserved interrupt                                                */
    Reserved47_IRQn     = 31,           /*!< Reserved interrupt                                                */
    CT0_IRQn            = 32,           /*!< CT0 interrupt                                                     */
    CT1_IRQn            = 33,           /*!< CT1 interrupt                                                     */
    FSMC_IRQn           = 34,           /*!< FSMC interrupt                                                    */
    QSPI_IRQn           = 35,           /*!< QSPI interrupt                                                    */
    USB_IRQn            = 36,           /*!< USB interrupt                                                     */
    DCMI_IRQn           = 37,           /*!< DCMI interrupt                                                    */
    DCMI_DMA_IRQn       = 38,           /*!< DCMI_DMA interrupt                                                */
    SDMA_IRQn           = 39,           /*!< Security DMA interrupt                                                */
    STIMER0_IRQn        = 40,           /*!< Stimer0 interrupt                                                 */
    STIMER1_IRQn        = 41,           /*!< Stimer1 interrupt                                                 */
    STIMER2_IRQn        = 42,           /*!< Stimer2 interrupt                                                 */
    STIMER3_IRQn        = 43,           /*!< Stimer3 interrupt                                                 */
    STIMER4_IRQn        = 44,           /*!< Stimer4 interrupt                                                 */
    STIMER5_IRQn        = 45,           /*!< Stimer5 interrupt                                                 */
    LPTIMER0_IRQn       = 46,           /*!< LPtimer0 interrupt                                                */
    Reserved63_IRQn     = 47,           /*!< Reserved interrupt                                                */
    GPIOA_IRQn          = 48,           /*!< GPIO A interrupt                                                  */
    GPIOB_IRQn          = 49,           /*!< GPIO B interrupt                                                  */
    GPIOC_IRQn          = 50,           /*!< GPIO C interrupt                                                  */
    GPIOD_IRQn          = 51,           /*!< GPIO D interrupt                                                  */
    GPIOE_IRQn          = 52,           /*!< GPIO E interrupt                                                  */
    GPIOF_IRQn          = 53,           /*!< GPIO F interrupt                                                  */
    RTC_CheckErr_IRQn   = 54,           /*!< RTC check error interrupt                                         */
    BKP_Tamper_IRQn     = 55,           /*!< BKP tamper interrupt                                              */
    RTC_Alarm_IRQn      = 56,           /*!< RTC time interrupt                                                */
    RTC_1s_IRQn         = 57,           /*!< RTC second interrupt                                              */
    Reserved74_IRQn     = 58,           /*!< Reserved interrupt                                                */
    ADC0_IRQn           = 59,           /*!< ADC0 interrupt                                                    */
    CMP0_IRQn           = 60,           /*!< CMP0 interrupt                                                    */
    DAC_Watermark_IRQn  = 61,           /*!< DAC watermark interrupt                                           */
    DAC_Top_IRQn        = 62,           /*!< DAC top interrupt                                                 */
    DAC_Bottom_IRQn     = 63            /*!< DAC bottom interrupt                                              */
} IRQn_Type;
//</h>


#include "core_cm3.h"                   /*!< Cortex-M3 processor and core peripherals              */
#include "system_fm15f3xx.h"            /*!< fm15f3xx System                                                                              */


/* ================================================================================ */
/* ================       Device Specific Peripheral Section       ================ */
/* ================================================================================ */


/** @addtogroup Device_Peripheral_Registers
 * @{
 */


/*
** Start of section using anonymous unions
*/
/*------------------ ARM Compiler V6 -------------------*/
#if defined(__ARMCC_VERSION) && (__ARMCC_VERSION >= 6010050)
/* anonymous unions are enabled by default */
#elif defined(__ARMCC_VERSION)
#pragma push
#pragma anon_unions
#elif defined(__CWCC__)
#pragma push
#pragma cpp_extensions on
#elif defined(__GNUC__)
/* anonymous unions are enabled by default */
#elif defined(__IAR_SYSTEMS_ICC__)
#pragma language=extended
#else
#error Not supported compiler type
#endif

#if defined (__CC_ARM)
#pragma anon_unions
#endif
//<h>Register Definition
//<h>ADC_TypeDef


/**
 * @brief ADC_CSR
 */

typedef struct {                /*!< ADC  Structure                                          */
    __IO uint32_t   CTRL;       /*!< ADC control register                      00H           */
    __IO uint32_t   CFG;        /*!< ADC configure register                    04H           */
    __IO uint32_t   TIME;       /*!< ADC configure register                    08H           */
    __IO uint32_t   WORK;       /*!< ADC work configure register               0CH           */
    __IO uint32_t   POLLSEQ;    /*!< ADC software trigger configure register   10H           */
    __IO uint32_t   POLLDLY;    /*!< ADC software trigger configure register   14H           */
    __IO uint32_t   SWTRG;      /*!< ADC software trigger configure register   18H           */
    struct {                    /*!< channel register a:14-24H;b:28-38H;c:3C-4CH;d:50-60H    */
        __IO uint32_t   CFG;    /*!< ADC channel A-D  cfg register             1CH           */
        __IO uint32_t   DRS;    /*!< adc_cha-d_dr_stage   register             20H           */
        __I uint32_t    RESULT; /*!< adc_cha-d_result     register             24H           */
        __IO uint32_t   OFFSET; /*!< adc_cha-d_offset     register             28H           */
        __IO uint32_t   CMPVAL; /*!< adc_cha-d_cmpval     register             2CH           */
    }               CHANNEL[4];
    __I uint32_t    STATUS    ; /*!< Reserved_0                                6CH           */
    __IO uint32_t   INTEN;      /*!< ADC interrupt enable register             70H           */
    __I uint32_t    STATUSRCD;  /*!< ADC status record register                74H           */
    __IO uint32_t   SRCL;       /*!< ADC status record clear register          78H           */
} ADC_TypeDef;

typedef struct {
    __IO uint32_t   CTRL;       /*00H*/
    __IO uint32_t   TRIM;       /*04H*/
    __IO uint32_t   BGFLAG;     /*08H*/
} ADC_VREFTypeDef;
//</h>

//<h>BCA_TypeDef


/**
 * @brief BCA
 */
typedef struct {                                    /*!< BCA Structure                                     */
    __IO uint32_t   DATA_IN[4];                     /*!< BCA_DATA_IN[127-0]             00-0CH             */
    __IO uint32_t   KEY_IN[12];                     /*!< BCA_KEY_IN[383-0]              10-3CH             */
    __IO uint32_t   CBC_DATA_IN[4];                 /*!< BCA_CBC_DATA_IN[127-0]         40-4CH             */
    __I uint32_t    DATA_OUT[4];                    /*!< BCA_DATA_OUT[127-0]            50-5CH             */
    __IO uint32_t   CSR;                            /*!< BCA_CSR                        60H                */
    __IO uint32_t   STATUS;                         /*!< BCA_STATUS                     64H                */
    __IO uint32_t   RUN;                            /*!< BCA_RUN                        68H                */
    __IO uint32_t   DESTROY_KEY;                    /*!< BCA_DESTROY_KEY                6CH                */
} BCA_TypeDef;
//</h>
//<h>BUSMAT_TypeDef


/**
 * @brief Bus Matrix
 */

typedef struct {                                    /*!< BusMatrix Structure                               */
    __IO uint32_t   S0PR;                           /*!< Slave 0 priority register              00H        */
    __IO uint32_t   S0CR;                           /*!< Slave 0 control register               04H        */
    __IO uint32_t   S1PR;                           /*!< Slave 1 priority register              08H        */
    __IO uint32_t   S1CR;                           /*!< Slave 1 control register               0CH        */
    __IO uint32_t   S2PR;                           /*!< Slave 2 priority register              10H        */
    __IO uint32_t   S2CR;                           /*!< Slave 2 control register               14H        */
    __IO uint32_t   S3PR;                           /*!< Slave 3 priority register              18H        */
    __IO uint32_t   S3CR;                           /*!< Slave 3 control register               1CH        */
    __IO uint32_t   S4PR;                           /*!< Slave 4 priority register              20H        */
    __IO uint32_t   S4CR;                           /*!< Slave 4 control register               24H        */
    __IO uint32_t   S5PR;                           /*!< Slave 5 priority register              28H        */
    __IO uint32_t   S5CR;                           /*!< Slave 5 control register               2CH        */
    __IO uint32_t   S6PR;                           /*!< Slave 6 priority register              28H        */
    __IO uint32_t   S6CR;                           /*!< Slave 6 control register               2CH        */
    __IO uint32_t   LFSREN;                         /*!< Enable bus mask register               30H        */
    __O uint32_t    LFSRD;                          /*!< Data of bus mask's lfsr register       34H        */
} BUSMAT_TypeDef;
//</h>
//<h>CACHE_TypeDef


/**
 * @brief CACHE_CSR
 */


typedef struct {                                    /*!< CACHE Structure                                   */
    __IO uint32_t CTRL;                             /*!< REG_CACHE_CTRL  register            00H           */
} CACHE_TypeDef;
//</h>
//<h>CMP_TypeDef


/**
 * @brief CMP_CSR
 */

typedef struct {                                    /*!< CMP  Structure                                          */
    __IO uint32_t   CTRL;                           /*!< CMP control register                      00H           */
    __IO uint32_t   IN;                             /*!< CMP_IN      register                      04H           */
    __IO uint32_t   DAC;                            /*!< CMP_DAC     register                      08H           */
    __IO uint32_t   DACDATA;                        /*!< CMP_ADC_DATA    register                  0CH           */
    __IO uint32_t   TRIG;                           /*!< CMP_TRIG        register                  10H           */
    __IO uint32_t   SWTRIG;                         /*!< CMP_SW_TRIG     register                  14H           */
    uint32_t        Reserved_0[2];                  /*!< Reserved array offset: 0x18, array step:0x4             */
    __IO uint32_t   OUT;                            /*!< CMP_OUT        register                   20H           */
    __I uint32_t    STATUS;                         /*!< CMP_STATUS  read-only   register          24H           */
    uint32_t        Reserved_1[2];                  /*!< Reserved array offset: 0x18, array step:0x4             */
    __IO uint32_t   INTEN;                          /*!< CMP_INT_EN        register                30H           */
    __I uint32_t    RCD;                            /*!< CMP_RCD  read-only    register            34H           */
    __IO uint32_t   RCDCL;                          /*!< CMP_RCDCL        register                 38H           */
} CMP_TypeDef;
//</h>
//<h>CRC_TypeDef


/**
 * @brief CRC
 */
typedef struct {                                    /*!< CRC Structure                                        */
    __IO uint32_t   CFG;                            /*!< REG_CRC_CFG       register        00H                */
    __O uint32_t    DATA_IN;                        /*!< REG_CRC_DATA_IN   register        04H                */
    __IO uint32_t   DATA;                           /*!< REG_CRC_DATA      register        08H                */
    uint32_t        Reserved;                       /*!< Reserved                          0CH                */
    __I uint32_t    DATA0;                          /*!< REG_CRC_DATA00    register        10H                */
    __I uint32_t    DATA1;                          /*!< REG_CRC_DATA01    register        14H                */
    __I uint32_t    DATA2;                          /*!< REG_CRC_DATA10    register        18H                */
    __I uint32_t    DATA3;                          /*!< REG_CRC_DATA11    register        1CH                */
} CRC_TypeDef;
//</h>
//<h>CT_TypeDef


/**
 * @brief CT
 */

typedef struct {                                    /*!< CT Structure                                               */
    __IO uint32_t   IO_CFG;                         /*!< REG_CT_CFG_IO             register          00H                */
    __IO uint32_t   CFG;                            /*!< REG_CT_CFG_TRANS          register          04H                */

    uint32_t        Reserved_0[2];                  /*!< Reserved                                    08-0CH         */
    __IO uint32_t   IO_CTRL;                        /*!< REG_CT_CTR_IO             register          10H                */
    __IO uint32_t   CTRL;                           /*!< REG_CT_CTR_TRANS          register          14H                */

    uint32_t        Reserved_1[2];                  /*!< Reserved                                    18-1CH         */
    __IO uint32_t   INT_CTRL;                       /*!< REG_CT_INT_EN             register          20H                */
    __IO uint32_t   STATUS_CLR;                     /*!< REG_CT_STS_CLR_TRANS      register          24H                */

    uint32_t        Reserved_2[2];                  /*!< Reserved                                    28-2CH         */
    __IO uint32_t   COUNT_THRESHOLD;                /*!< REG_CT_THD_CNT            register          30H                */
    __IO uint32_t   ETU_COUNT_THRESHOLD;            /*!< REG_CT_THD_ETU_CNT        register          34H                */

    uint32_t        Reserved_3[2];                  /*!< Reserved                                    28-3CH                */
    __O uint32_t    TXFIFO;                         /*!< REG_CT_TX_FIFO            register          40H                */
    __I uint32_t    RXFIFO;                         /*!< REG_CT_RX_FIFO            register          44H                */
    uint32_t        Reserved_4[30];                 /*!< Reserved     array offset: 0x48, array step:0x4                */

    __I uint32_t    IO_STATUS;                      /*!< REG_CT_IO_STATUS          register          C0H                */
    __I uint32_t    STATUS;                         /*!< REG_CT_CT_STATUS          register          C4H                */
    __I uint32_t    FIFO_STATUS;                    /*!< REG_CT_FIFO_STATUS        register          C8H                */

    uint32_t        Reserved_5[5];                  /*!< Reserved     array offset: 0xD0, array step:0x4                */
    __I uint32_t    ETU_COUNT;                      /*!< REG_CT_ETU_CNT            register          E0H                */
    __I uint32_t    RX_ETU_COUNT;                   /*!< REG_CT_RX_ETU_CNT         register          E4H                */
    __I uint32_t    TX_ETU_COUNT;                   /*!< REG_CT_TX_ETU_CNT         register          E8H                */
    __I uint32_t    COUNT;                          /*!< REG_CT_CNT                register          ECH                */
} CT_TypeDef;
//</h>
//<h>DAC_TypeDef


/**
 * @brief DAC_CSR
 */

typedef struct {                                    /*!< DAC  Structure                                          */
    __IO uint32_t   CTRL;                           /*!< DAC control register                      00H           */
    __IO uint32_t   CFG;                            /*!< DAC configure register                    04H           */
    __IO uint32_t   WORK;                           /*!< DAC work configure register               08H           */
    __IO uint32_t   TRIG;                           /*!< DAC trigger configure register            0CH           */
    __IO uint32_t   SWTRIG;                         /*!< DAC software trigger configure register   10H           */
    __IO uint32_t   DPTR;                           /*!< DAC data pointer register                 14H           */
    __IO uint32_t   DPCFG;                          /*!< DAC data pointer configure register       18H           */
    __IO uint32_t   DATA[8];                        /*!< DAC data0~7 register                  1CH~38H           */
    __I uint32_t    FLAG;                           /*!< DAC flag register read-only               3CH           */
    __IO uint32_t   INTEN;                          /*!< DAC interrupt enable register             40H           */
    __I uint32_t    INT;                            /*!< DAC interrupt register                    44H           */
    __IO uint32_t   IFCL;                           /*!< DAC interrupt and flag clear register     48H           */
} DAC_TypeDef;
//</h>
//<h>DCMI_DMA_TypeDef
typedef struct {
    __IO uint32_t   cfg_a;
    __IO uint32_t   cfg_b;
    __IO uint32_t   cfg_c;
    uint32_t        rfu1[1];
    __IO uint32_t   rcv_cfg_a;
    __IO uint32_t   rcv_cfg_b;
    __IO uint32_t   rcv_cfg_c;
    uint32_t        rfu2[1];
    __IO uint32_t   auto_buf_lu_cfg_a;
    __IO uint32_t   auto_buf_lu_cfg_b;
    uint32_t        rfu3[2];
    __IO uint32_t   auto_result_cfg_a;
    __IO uint32_t   auto_result_cfg_b;
    uint32_t        rfu4[2];
    __IO uint32_t   r11311_cfg_a;
    __IO uint32_t   r11311_cfg_b;
    __IO uint32_t   r11311_cfg_c;
    uint32_t        rfu5[1];
    uint32_t        rfu6[4];
    __IO uint32_t   status_a;
    __IO uint32_t   status_b;
    __IO uint32_t   status_c;
    __IO uint32_t   status_d;
    __IO uint32_t   status_e;
    uint32_t        rfu[3];
    __IO uint32_t   clr;
    __IO uint32_t   int_en;
} DCMI_DMA_TypeDef;
//</h>
//<h>DCMI_TypeDef


/**
 * @brief DCMI
 */

typedef struct {
    __IO uint32_t   CFG;                        //!< Config Register, offset: 0x0
    __I uint32_t    STATUS;                     //!< Status Register, offset: 0x4
    __I uint32_t    RISTATUS;                   //!< Int_Raw_Statys Register, offset: 0x8
    __IO uint32_t   INTEN;                      //!< Enable Interrupt Register, offset: 0xC
    __I uint32_t    MISTATUS;                   //!< Interrupt masked status Register, offset: 0x10
    __IO uint32_t   ICL;                        //!< Interrupt clear Register, offset: 0x14
    __IO uint32_t   ESC;                        //!< Embedded code config Register, offset: 0x18
    __IO uint32_t   ESUC;                       //!< Embedded unmask config Register, offset: 0x1C
    __IO uint32_t   CWSTRT;                     //!< Corp window start Register, offset: 0x20
    __IO uint32_t   CWSIZE;                     //!< Corp window size Register, offset: 0x24
    __I uint32_t    FIFOD;                      //!< Dcmi fifo Register, offset: 0x28
    __IO uint32_t   MAPBASE;                    //!< Map base Register, offset: 0x2C
    __IO uint32_t   MAP[4];                     //!< Map0-4 Register, offset: 0x30~3C
} DCMI_TypeDef;
//</h>


//<h>dma_tcd_t


/**
 * @brief DMA
 */
typedef struct _dma_tcd_w3 {
    __IO int16_t    DLAST;
    __IO int16_t    SLAST;
} dma_tcd_w3_t;

typedef struct _dma_tcd {
    __IO uint32_t   SADDR;                      //word0
    __IO int8_t DOFF;                         //word1
    __IO int8_t SOFF;
    __IO uint16_t   ATTR;
    union {
        __IO uint32_t   NBYTES_MLNO;            //word2
        __IO uint32_t   NBYTES_MLOFFNO;
        __IO uint32_t   NBYTES_MLOFFYES;
    };
    union {
        __IO uint32_t   SGA;                    //word3
        dma_tcd_w3_t w3;
    };
    __IO uint32_t   DADDR;                      //word4
    union {
        __IO uint32_t   BITER_CITER_ELINKNO;    //word5
        __IO uint32_t   BITER_CITER_ELINKYES;
    };
    __IO uint32_t   CSR;                        //word6
    uint32_t        Reserved_1;                 // TCD Reserved ,array step: 0x20
} dma_tcd_t;
//</h>
//<h>DMA_TypeDef
typedef struct {
    __IO uint32_t   CR;                         //!< Control Register, offset: 0x0
    __I uint32_t    ES;                         //!< Error Status Register, offset: 0x4
    __IO uint32_t   ERQ;                        //!< Enable Request Register, offset: 0x8
    __IO uint32_t   EEI;                        //!< Enable Error Interrupt Register, offset: 0xC
    __O uint32_t    CEEI;                       //!< Clear Enable Error Interrupt Register, offset: 0x10
    __O uint32_t    SEEI;                       //!< Set Enable Error Interrupt Register, offset: 0x14
    __O uint32_t    CERQ;                       //!< Clear Enable Request Register, offset: 0x18
    __O uint32_t    SERQ;                       //!< Set Enable Request Register, offset: 0x1C
    __O uint32_t    CDNE;                       //!< Clear DONE Status Bit Register, offset: 0x20
    __O uint32_t    SSRT;                       //!< Set START Bit Register, offset: 0x24
    __O uint32_t    CERR;                       //!< Clear Error Register, offset: 0x28
    __O uint32_t    CINT;                       //!< Clear Interrupt Request Register, offset: 0x2C
    __IO uint32_t   INT;                        //!< Interrupt Request Register, offset: 0x30
    __IO uint32_t   ERR;                        //!< Error Register, offset: 0x34
    __I uint32_t    HRS;                        //!< Hardware Request Status Register, offset: 0x38
    __IO uint32_t   EARS;                       //!< Enable Asynchronous Request in Stop Register, offset: 0x3C
    __IO uint32_t   DONE;                       //!< Transfer completion Register, offset: 0x40
    __I uint32_t    ACTIVE;                     //!< Active state Register, offset: 0x44
    __IO uint32_t   CHPRI;                      //!< Channel Priority Configure Register, offset: 0x48
    uint32_t        Reserved_0[1005];           //!< Reserved   offset: 0x4C-0xFFC
    dma_tcd_t       TCD[8];                     // offset: 0x1000, array step: 0x20
} DMA_TypeDef;
//</h>
//<h>DMAMUX_TypeDef


/**
 * @brief DMAMUX
 */
typedef struct {
    __IO uint32_t CHCFG[8];                         /*!< Channel Configuration register, array offset: 0x0, array step: 0x4 */
} DMAMUX_TypeDef;
//</h>
//<h>EFCR_TypeDef


/**
 * @brief EFUSE
 */
typedef struct {
    __IO uint32_t   PROGT;                          /*!< EFCR_PROGEN register                         00H          */
    uint32_t        Reserved[3];                    /*!< Reserved     array offset: 0x04, array step:0x4           */
    __IO uint32_t   CMDSTAT;                        /*!< EFCR_CMDSTAT register                        10H          */
    __IO uint32_t   CMDEN;                          /*!< EFCR_CMDEN register                          14H          */
    __IO uint32_t   COMM;                           /*!< EFCR_COMM register                           18H          */
    __IO uint32_t   CMDPARA;                        /*!< EFCR_CMDPARA register                        1CH          */
    __I uint32_t    DOUT;                           /*!< EFCR_DOUT register                           20H          */
} EFCR_TypeDef;
//</h>
//<h>FLASH_TypeDef


/**
 * @brief Flash control
 */

typedef struct {                                    /*!< flashctrl Structure                                         */
    __IO uint32_t   CMDSTAT;                        /*!< state register                                00H           */
    __IO uint32_t   CMDINTE;                        /*!< Flash init enable register                    04H           */
    __IO uint32_t   CMDINTF;                        /*!< Reserved                                      08H           */
    __IO uint32_t   PORT;                           /*!< port register                                 0CH           */
    __IO uint32_t   RDCFG;                          /*!< FLASH_RDCFG register                            10H           */
    __IO uint32_t   RDT;                            /*!< FLASH_RDT register                              14H           */
    __IO uint32_t   LDOEN;                          /*!< FLASH_LDOEN register                            18H           */
    uint32_t        Reserved_0;                     /*!< Reserved                                      1CH           */
    __IO uint32_t   ERAST0;                         /*!< FLASH_ERAST0 register                           20H           */
    __IO uint32_t   ERAST1;                         /*!< FLASH_ERAST1 register                           24H           */
    __IO uint32_t   PROGT0;                         /*!< FLASH_PROGT0 register                           28H           */
    __IO uint32_t   PROGT1;                         /*!< FLASH_PROGT1 register                           2CH           */
    __IO uint32_t   CFGT;                           /*!< FLASH_CFGT register                             30H           */
    __IO uint32_t   VRDT;                           /*!< FLASH_VRD1T register                            34H           */
    uint32_t        Reserved_1[2];                  /*!< Reserved                                  38H,3CH           */
    __IO uint32_t   CMDCFG;                         /*!< FLASH_CMDCFG register                           40H           */
    __IO uint32_t   CMDEN;                          /*!< FLASH_CMDEN register                            44H           */
    __IO uint32_t   COMM;                           /*!< FLASH_COMM register                             48H           */
    __IO uint32_t   CMDADDR;                        /*!< FLASH_CMDADDR register                          4CH           */
    __IO uint32_t   CMDPARA[32];                    /*!< FLASH_CMDPARA0-31 register   start:50H   end    CCH           */
    __IO uint32_t   CMDPF;                          /*!< FLASH_CMDPF register                            D0H           */
    __IO uint32_t   MCCFG;                          /*!< FLASH_MCCFG register                            D4H           */
    __IO uint32_t   ENCRY;                          /*!< FLASH_ENCRY register                            D8H           */
    __IO uint32_t   RDNEN;                          /*!< FLASH_RDNEN register                            DCH           */
    __IO uint32_t   AKEY;                           /*!< FLASH_FKEY0 register                            E0H           */
    __IO uint32_t   DKEY;                           /*!< FLASH_FKEY1 register                            E4H           */
    __IO uint32_t   RDN0;                           /*!< FLASH_RDN0 register                             E8H           */
    __IO uint32_t   RDN1;                           /*!< FLASH_RDN1 register                             ECH           */
    __IO uint32_t   RDN2;                           /*!< FLASH_RDN2 register                             F0H           */
    __IO uint32_t   RDN3;                           /*!< FLASH_RDN3 register                             F4H           */
    __IO uint32_t   MAINLOCK;                       /*!< FLASH_MAIN_LOCK register                        F8H           */
    __IO uint32_t   NVRULOCK;                       /*!< FLASH_NVRU_LOCK register                        FCH           */
    __IO uint32_t   OTPLOCK;                        /*!< FLASH_OTP_LOCK register                        100H          */
} FLASH_TypeDef;
//</h>
//<h>FSMC_TypeDef


/**
 * @brief FSMC
 */

typedef struct {                                    /*!< FSMC Structure                                             */
    __IO uint32_t   CTRL;                           /*!< FSMCR_CTRL       register                00H                */
    __IO uint32_t   ARDT;                           /*!< FSMCR_ARDT       register                04H                */
    __IO uint32_t   AWRT;                           /*!< FSMCR_AWRT       register                08H                */
    __IO uint32_t   SYNCT;                          /*!< FSMCR_SYNCT      register                0CH                */
    __IO uint32_t   EXTM;                           /*!< FSMCR_EXTM       register                10H                */
    __IO uint32_t   STAT;                           /*!< FSMCR_STAT       register                14H                */
    __IO uint32_t   INTE;                           /*!< FSMCR_INTE       register                18H                */
    __IO uint32_t   INTF;                           /*!< FSMCR_INTF       register                1CH                */
    __IO uint32_t   FADDR;                          /*!< FSMCR_ADDR       register                20H                */
    __IO uint32_t   WNUM;                           /*!< FSMCR_WNUM       register                24H                */
    __IO uint32_t   FDATA;                          /*!< FSMCR_DATA       register                28H                */
    uint32_t        Reserved_0[1];                  /*!<                                          2CH                */
    __IO uint32_t   FCFG;                           /*!< FSMCR_WNUM       register                30H                */
    __IO uint32_t   FSTAT;                          /*!< FSMCR_DATA       register                34H                */
} FSMC_TypeDef;
//</h>
//<h>GPIO_CFG_TypeDef


/**
 * @brief GPIO CFG
 */

typedef struct {                                    /*!< port control Structure                                               */
    __IO uint32_t   PCR[16];                        /*!< pin control register n    array offset: 0x0, array step: 0x4         */
    uint32_t        Reserved_0[16];                 /*!< Reserved                  array offset: 0x40, array step:0x4         */
    __O uint32_t    GPCLR;                          /*!< Global Pin Control Low Register, offset: 0x80                        */
    __O uint32_t    GPCHR;                          /*!< Global Pin Control High Register, offset: 0x84                       */
    uint32_t        Reserved_1[6];                  /*!< Reserved                  array offset: 0x88, array step:0x4         */
    __IO uint32_t   ISFR;                           /*!< Interrupt Status Flag Register, offset: 0xA0                         */
    uint32_t        Reserved_2[7];                  /*!< Reserved                  array offset: 0xA4, array step:0x4         */
    __IO uint32_t   DFER;                           /*!< Digital Filter Enable Register, offset: 0xC0                         */
    __IO uint32_t   DFCR;                           /*!< Digital Filter Clock Register,  offset: 0xC4                         */
    __IO uint32_t   DFWR;                           /*!< Digital Filter Width Register,  offset: 0xC8                         */
} GPIO_CFG_TypeDef;
//</h>
//<h>GPIO_TypeDef


/**
 * @brief GPIO
 */

typedef struct {
    __IO uint32_t   PDOR;                                                   /**< Port Data Output Register, offset: 0x0 */
    __O uint32_t    PSOR;                                                   /**< Port Set Output Register, offset:  0x4 */
    __O uint32_t    PCOR;                                                   /**< Port Clear Output Register, offset: 0x8 */
    __O uint32_t    PTOR;                                                   /**< Port Toggle Output Register, offset: 0xC */
    __I uint32_t    PDIR;                                                   /**< Port Data Input Register, offset: 0x10 */
    __IO uint32_t   PDDR;                                                   /**< Port Data Direction Register, offset: 0x14 */
    __IO uint32_t   ODER;                                                   /**< Open drain enable Register, offset: 0x18 */
} GPIO_TypeDef;
#define GPIO_CLK_CTRL_ADDR  ( (uint32_t) 0x400FF200)                        /*!< GPIO_CLK_CTRL base address    */
#define GPIO_CLK_CTRL       (*(volatile uint32_t *) (GPIO_CLK_CTRL_ADDR) )  /*GPIO_CLK_CTRL*/

#define GPIO_VBUS_DET_ADDR  ( (uint32_t) 0x4007B024)                        /*!< GPIO_VBUS_DET base address    */
#define GPIO_VBUS_DET       (*(volatile uint32_t *) (GPIO_VBUS_DET_ADDR) )  /*GPIO_VBUS_DET*/
//</h>
//<h>HASH_TypeDef


/**
 * @brief HASH
 */

typedef struct {                                                            /*!< HASH Structure                                         */
    __IO uint32_t   CFG;                                                    /*!< HASH_CFG         register           00H                */
    __IO uint32_t   STAT;                                                   /*!< HASH_STATUS      register           04H                */
    __IO uint32_t   FIFO_CTRL;                                              /*!< HASH_FIFO_CTRL   register           08H                */
    __IO uint32_t   CTRL;                                                   /*!< HASH_CTRL        register           0CH                */
    __IO uint32_t   SOFT_MESSAGE_LEN_L;                                     /*!< HASH_SOFT_MESSAGE_LEN_L             10H                */
    __IO uint32_t   SOFT_MESSAGE_LEN_H;                                     /*!< HASH_SOFT_MESSAGE_LEN_H             14H                */
    __IO uint32_t   HARD_MESSAGE_LEN_L;                                     /*!< HASH_HARD_MESSAGE_LEN_L             18H                */
    __IO uint32_t   HARD_MESSAGE_LEN_H;                                     /*!< HASH_HARD_MESSAGE_LEN_H             1CH                */
    __IO uint32_t   MSG_BUF[16];                                            /*!< HASH_MESSAGE_BUF_0-F   register     20-5CH             */
    uint32_t        Reserved_0[4];                                          /*!< Reserved  array offset: 0x60, array step:0x4           */
    __IO uint32_t   MSG_FIFO;                                               /*!< HASH_MESSAGE_FIFO   register        70H                */
    uint32_t        Reserved_1[3];                                          /*!< Reserved  array offset: 0x70, array step:0x3           */
    __IO uint32_t   DIG[8];                                                 /*!< HASH_DIGEST_0-7   register          80-9CH                */
} HASH_TypeDef;
//</h>
//<h>I2C_TypeDef


/**
 * @brief I2C
 */

typedef struct {                                    /*!< I2C Structure                                              */
    //I2C Master
    __IO uint32_t   MEN;                            /*!< I2C master enable  register             00H                */
    __IO uint32_t   MSSPCON;                        /*!< I2C master SSPCON  register             04H                */
    __IO uint32_t   MSSPSTAT;                       /*!< I2C master SSPSTAT  register            08H                */
    __IO uint32_t   MSSPBRG;                        /*!< I2C master SSPBRG  register             0CH                */
    __IO uint32_t   MSSPBUF;                        /*!< I2C master SSPBUF  register             10H                */
    __IO uint32_t   MSSPIR;                         /*!< I2C master SSPIR  register              14H                */
    uint32_t        Reserved[58];                   /*!< Reserved array offset: 0x18,array step:0x4                  */
    //I2C Slave
    __IO uint32_t   SSSPCON;                        /*!< I2C Slave SSPCON  register             100H                */
    __IO uint32_t   SSSPSTAT;                       /*!< I2C Slave SSPSTAT  register            104H                */
    __IO uint32_t   SSSPBUF;                        /*!< I2C Slave SSPBUF  register             108H                */
    __IO uint32_t   SSSPADDR;                       /*!< I2C Slave SSPADDR  register            10CH                */
    __IO uint32_t   SSSPIR;                         /*!< I2C Slave SSPIR  register              110H                */
} I2C_TypeDef;
//</h>
//<h>INTC_TypeDef


/**
 * @brief INTC
 */

typedef struct {
    __IO uint32_t NMICTRL;                                                          /**< INTC_NMI_CTRL, offset: 0x0 */
} INTC_TypeDef;
#define SYSTICK_SYSTIKREF_ADDR  ( (uint32_t) 0x4007C0A0)                            /*!< SYSTICK_SYSTIKREF base address    */
#define SYSTICK_SYSTIKREF       (*(volatile uint32_t *) (SYSTICK_SYSTIKREF_ADDR) )  /*SYSTICK_SYSTIKREF*/
//</h>
//<h>LPTIMER_TypeDef


/**
 * @brief LPTIMER
 */

typedef struct {                                                                    /*!< LPTIMER  Structure                                      */
    __IO uint32_t   CTRL;                                                           /*!< LPTIMER control register                  00H           */
    __IO uint32_t   CLKCFG;                                                         /*!< LPTIMER_CLK_CFG register                  04H           */
    __IO uint32_t   CFG;                                                            /*!< LPTIMER_CFG register                      08H           */
    __IO uint32_t   MOD;                                                            /*!< LPTIMER_MOD_VALUE register                0CH           */
    __I uint32_t    CVAL;                                                           /*!< LPTIMER_CURRENT_VALUE register            10H           */
    __IO uint32_t   FLAGCLR;                                                        /*!< LPTIMER_FLAG_CLR  register               14H           */
    uint32_t        Reserved;                                                       /*!< Reserved                                  18H           */
    __I uint32_t    FLAGSTATUS;                                                     /*!< LPTIMER_FLAG_STATUS register              1CH           */
} LPTIMER_TypeDef;
//</h>
//<h>CMU_TypeDef


/**
 * @brief CMU_CFG offset:0x30
 */
typedef struct {
    __IO uint32_t   CLKLP;              /*B030H*/
    uint32_t        Reserved_0[7];      /*B034H~B04CH*/
    __IO uint32_t   IRC4MRDY;           /*B050H*/
    uint32_t        Reserved_1[7];      /*B054H~B06CH*/
    __IO uint32_t   CLKSTOP;            /*B070H*/
    uint32_t        Reserved_2;         /*B074H*/
    __IO uint32_t   CLKIDLE;            /*B078H*/
    uint32_t        Reserved_3;         /*B07CH*/
    __IO uint32_t   CFG_CLKS0;          /*B080H*/
    __IO uint32_t   CFG_CLKS1;          /*B084H*/
    uint32_t        Reserved_4[990];    /*B088H~BFFCH*/
    __I uint32_t    PLLSTA;             /*C000H*/
    uint32_t        Reserved_5[131];    /*C004H~C20CH*/
    __IO uint32_t   IRC16MRDY;          /*C210H*/
    uint32_t        Reserved_6[4];      /*C214H~C220H*/
    __IO uint32_t   OSCRDY;             /*C224H*/
    __IO uint32_t   PLLRDY;             /*C228H*/
    uint32_t        Reserved_7;         /*C22CH*/
    __IO uint32_t   USB48MRDY;          /*C230H*/
    uint32_t        Reserved_8;         /*C234H*/
    __IO uint32_t   GPIORDY;            /*C238H*/
    uint32_t        Reserved_9;         /*C23CH*/
    __I uint32_t    CLKVLDF;            /*C240H*/
    uint32_t        Reserved_10[3];     /*C244H~C24CH*/
    __IO uint32_t   MCGCLKA;            /*C250H*/
    __IO uint32_t   MCGCLKB;            /*C254H*/
    __IO uint32_t   MCLKS;              /*C258H*/
    uint32_t        Reserved_11;        /*C25CH*/
    __IO uint32_t   MDLCLKA;            /*C260H*/
    __IO uint32_t   MDLCLKB;            /*C264H*/
    __IO uint32_t   MDLCLKC;            /*C268H*/
    __IO uint32_t   MDLCLKD;            /*C26CH*/
    uint32_t        Reserved_12[4];     /*C270H~C27CH*/
    __IO uint32_t   GMDLCLK;            /*C280H*/
    __IO uint32_t   PMGMDLCLK;          /*C284H*/
} CMU_TypeDef;
//</h>

//<h>ALM_SENSORSTypeDef


/**
 * @brief ALM_SENSORS offset:0x00
 */
typedef struct {                    /*!< ALM_SENSORS  Structure                      */
    __IO uint32_t   EN;             /*!< REG_EN_SENSORS0               90H           */
    __IO uint32_t   CFG;            /*!< REG_CFG_SENSORS0        94H             */
} ALM_SENSORS0TypeDef;

typedef struct {                    /*!< ALM_SENSORS  Structure                                  */
    __IO uint32_t   EN;             /*!< REG_CFG_EN_SENSORS                        10H           */
    uint32_t        Reserved_1[7];  /*!< Reserved array offset: 0x1C, array step:0x4             */
    __IO uint32_t   BTCLKC;         /*!< REG_CFG_BASE_TIMER_CLKCFG                 30H           */
    uint32_t        Reserved_2[3];  /*!< Reserved array offset: 0x34, array step:0x4             */
    struct {                        /*!< channel register a:14-24H;b:28-38H;c:3C-4CH;d:50-60H    */
        __IO uint32_t   CFG;        /*!< REG_CFG_BASE_TIMER0_CFG                           40H               */
        __IO uint32_t   CLKAC;      /*!< REG_CFG_BASE_TIMER0_CLKA_CFG                    44H                 */
        __IO uint32_t   CLKBCL;     /*!< REG_CFG_BASE_TIMER0_CLKB_CFGL               48H                 */
        __IO uint32_t   CLKBCH;     /*!< REG_CFG_BASE_TIMER0_CLKB_CFGH               4CH                 */
        __IO uint32_t   CTRL;       /*!< REG_CFG_BASE_TIMER0_CTRL                          50H               */
        __I uint32_t    SR;         /*!< REG_CFG_BASE_TIMER0_STATUS                      54H                 */
        __I uint32_t    RPT;        /*!< REG_CFG_BASE_TIMER0_REPORT                      58H                 */
        uint32_t        Reserved_3; /*!< Reserved array offset: 0x5C, array step:0x4                   */
    }               BT[2];
    uint32_t        Reserved_4[4];  /*!< Reserved array offset: 0x88, array step:0x4             */
    __O uint32_t    ALARMA;         /*!< REG_SELFGEN_ACTIVE_ALARM_A                90H           */
    __O uint32_t    ALARMB;         /*!< REG_SELFGEN_ACTIVE_ALARM_B                94H           */
} ALM_SENSORS1TypeDef;
//</h>

//<h>ALM_MONITORTypeDef


/**
 * @brief ALM_MONITOR offset:0x300
 */

typedef struct {                                    /*!< ALM_MONITOR  Structure                                       */
    __IO uint32_t   GROUPA0;                        /*!< REG_MONITOR_CFG_GROUP_A_0                 300H          */
    __IO uint32_t   GROUPA1;                        /*!< REG_MONITOR_CFG_GROUP_A_1                 304H          */
    __IO uint32_t   GROUPA2;                        /*!< REG_MONITOR_CFG_GROUP_A_2                 308H          */
    __IO uint32_t   GROUPA3;                        /*!< REG_MONITOR_CFG_GROUP_A_3                 30CH          */
    __IO uint32_t   GROUPB0;                        /*!< REG_MONITOR_CFG_GROUP_B_0                 310H          */
    uint32_t        Reserved_0[3];                  /*!< Reserved  array offset: 0x314, array step:0x4           */
    __IO uint32_t   GROUPC0;                        /*!< REG_MONITOR_CFG_GROUP_C_0                 320H          */
    __IO uint32_t   GROUPC1;                        /*!< REG_MONITOR_CFG_GROUP_C_1                 324H          */
    __IO uint32_t   GROUPC2;                        /*!< REG_MONITOR_CFG_GROUP_C_2                 328H          */
    __IO uint32_t   GROUPC3;                        /*!< REG_MONITOR_CFG_GROUP_C_3                 32CH          */
    __IO uint32_t   GROUPC4;                        /*!< REG_MONITOR_CFG_GROUP_C_4                 330H          */
    __IO uint32_t   GROUPC5;                        /*!< REG_MONITOR_CFG_GROUP_C_5                 334H          */
    uint32_t        Reserved_1[2];                  /*!< Reserved array offset: 0x38, array step:0x4             */
    __IO uint32_t   GROUPD0;                        /*!< REG_MONITOR_CFG_GROUP_D_0                 340H          */
    __IO uint32_t   GROUPD1;                        /*!< REG_MONITOR_CFG_GROUP_D_1                 344H          */
    uint32_t        Reserved_2[2];                  /*!< Reserved  array offset: 0x48, array step:0x4            */
    __IO uint32_t   ALMRCDCLR;                      /*!< REG_ALARM_RECORD_CLR                      350H           */
    uint32_t        Reserved_3[3];                  /*!< Reserved  array offset: 0x54, array step:0x4            */
    __I uint32_t    ALMRCDA;                        /*!< REG_ALARM_RECORD_A                        360H          */
    __I uint32_t    ALMRCDB;                        /*!< REG_ALARM_RECORD_B                        364H          */
    __I uint32_t    ALMRCDC;                        /*!< REG_ALARM_RECORD_C                        368H          */
    __I uint32_t    ALMRCDD;                        /*!< REG_ALARM_RECORD_D                        36CH          */
} ALM_MONITORTypeDef;
//</h>
//<h>ALM_NRSTTypeDef


/**
 * @brief ALM_NRST offset:0x300
 */
typedef struct {                                    /*!< ALM_NRST  Structure                                       */
    __IO uint32_t RSTBP;                            /*!< REG_MONITOR_CFG_GROUP_A_0                 300H          */
} ALM_NRSTTypeDef;
//</h>
//<h>ALM_RSTTypeDef


/**
 * @brief ALM_RST offset:0x160
 */
typedef struct {                                    /*!< ALM  Structure                                          */
    __I uint32_t    RCDA;                           /*!< REG_RST_RECORD_A                          160H          */
    __I uint32_t    RCDB;                           /*!< REG_RST_RECORD_B                          164H          */
    __I uint32_t    RCDC;                           /*!< REG_RST_RECORD_C                          168H          */
    __I uint32_t    RCDD;                           /*!< REG_RST_RECORD_D                          16CH          */
    __I uint32_t    RCDE;                           /*!< REG_RST_RECORD_E                          170H          */
    uint32_t        Reserved_0[3];                  /*!< Reserved     array offset: 0x174, array step:0x4        */
    __IO uint32_t   RCDCLR;                         /*!< REG_RST_RECORD_CLR                        180H          */
    uint32_t        Reserved_1[3];                  /*!< Reserved     array offset: 0x184, array step:0x4        */
    __IO uint32_t   ENGROUPE;                       /*!< REG_RST_EN_GROUP_E                        190H          */
} ALM_RSTTypeDef;
//</h>

//<h>PMU_TypeDef


/**
 * @brief PMU offset:0x010
 */
typedef struct {
    __IO uint32_t   PSTOP;              /*B010H*/
    __IO uint32_t   PSTBY;              /*B014H*/
    uint32_t        Reserved_0[2];      /*B018H~B01CH*/
    __IO uint32_t   HOLDENLP;           /*B020H*/
    uint32_t        Reserved_1[7];      /*B024H~B03CH*/
    __IO uint32_t   LDORDY;             /*B040H*/
    uint32_t        Reserved_2[7];      /*B044H~B05CH*/
    __IO uint32_t   PSWRDY;             /*B060H*/
    __IO uint32_t   PSWEN;              /*B064H*/
    uint32_t        Reserved_4[19];     /*B068H~B0B0H*/
    __IO uint32_t   RTCISO;             /*B0B4H*/
    uint32_t        Reserved_5[6];      /*B0B8H~B0CCH*/
    __IO uint32_t   CHGERCFG;           /*B0D0H*/
    uint32_t        Reserved_6[2];      /*B0D4H~B0D8H*/
    __I uint32_t    CHGERSTS;           /*B0DCH*/
    uint32_t        Reserved_7;         /*B0E0H*/
    __IO uint32_t   LDO12PDCFG1;        /*B0E4H*/
    __IO uint32_t   LDO12ILSWCFG;       /*B0E8H*/
    uint32_t        Reserved_8[3];      /*B0ECH~B0F4H*/
    __IO uint32_t   LDO16ILSWCFG;       /*B0F8H*/
    uint32_t        Reserved_9;         /*B0FCH*/
    __IO uint32_t   FILTS;              /*B100H*/
    __IO uint32_t   PADEN;              /*B104H*/
    __IO uint32_t   INTFE;              /*B108H*/
    __IO uint32_t   FILTE;              /*B10CH*/
    uint32_t        Reserved_10[4];     /*B110H~B11CH*/
    __IO uint32_t   PCFGA;              /*B120H*/
    uint32_t        Reserved_11;        /*B124H*/
    __IO uint32_t   INTFCFGA;           /*B128H*/
    __IO uint32_t   FILTCFGA;           /*B12CH*/
    __IO uint32_t   CFG;                /*B130H*/
    uint32_t        Reserved_12[3];     /*B134H~B13CH*/
    __IO uint32_t   PHLDR;              /*B140H*/
    __I uint32_t    PHLDS;              /*B144H*/
    uint32_t        Reserved_13[2];     /*B148H~B14CH*/
    __I uint32_t    RCDPAD;             /*B150H*/
    __I uint32_t    RCDINTF;            /*B154H*/
    __I uint32_t    RCDFILT;            /*B158H*/
    __IO uint32_t   RCDCLR;             /*B15CH*/
    uint32_t        Reserved_14[40];    /*B160H~B1FCH*/
    __IO uint32_t   REG[8];             /*B200H~B21CH*/
    uint32_t        Reserved_15[1144];  /*B220H~C3FCH*/
    __IO uint32_t   LPM;                /*C400H*/
    uint32_t        Reserved_16[3];     /*C404H~C40CH*/
    __IO uint32_t   WKUPA;              /*C410H*/
    __IO uint32_t   WKUPB;              /*C414H*/
    __IO uint32_t   LWKUPRCDCLR;        /*C418H*/
    uint32_t        Reserved_17;        /*C41CH*/
    __I uint32_t    LWKUPRCDC;          /*C420H*/
    __I uint32_t    LWKUPRCDA;          /*C424H*/
    __I uint32_t    LWKUPRCDB;          /*C428H*/
    uint32_t        Reserved_18;        /*C42CH*/
    __IO uint32_t   INTCLR;             /*C430H*/
    uint32_t        Reserved_19[3];     /*C434H~C43CH*/
    __I uint32_t    INT;                /*C440H*/
} PMU_TypeDef;
//</h>

//<h>MBIST_TypeDef


/**
 * @brief MBIST
 */

typedef struct {                                    /*!< MBIST  Structure                                        */
    __IO uint32_t   CTRL;                           /*!< MBIST_CTRL control register               00H           */
    __IO uint32_t   TMCTRL0;                        /*!< MBIST_TM_CTRL0 register                   04H           */
    __IO uint32_t   TMCTRL1;                        /*!< MBIST_TM_CTRL1 register                   08H           */
    __IO uint32_t   PCTRL;                          /*!< MBIST_PIN_CTRL register                   0CH           */
    __IO uint32_t   ADDR;                           /*!< MBIST_ADDR    register                    10H           */
    __IO uint32_t   AEND;                           /*!< MBIST_AEND    register                    14H           */
    __IO uint32_t   DIN0;                           /*!< MBIST_DIN0    register                    18H           */
    __IO uint32_t   DIN1;                           /*!< MBIST_DIN1    register                    1CH           */
    __I uint32_t    DOUT0L;                         /*!< MBIST_DOUT0L  register                    20H           */
    __I uint32_t    DOUT0H;                         /*!< MBIST_DOUT0H  register                    24H           */
    __I uint32_t    DOUT1L;                         /*!< MBIST_DOUT1L  register                    28H           */
    __I uint32_t    DOUT1H;                         /*!< MBIST_DOUT1H  register                    2CH           */
    __I uint32_t    DOUT2L;                         /*!< MBIST_DOUT2L  register                    30H           */
    __I uint32_t    DOUT2H;                         /*!< MBIST_DOUT2H  register                    34H           */
    __I uint32_t    DOUT3L;                         /*!< MBIST_DOUT3L  register                    38H           */
    __I uint32_t    DOUT3H;                         /*!< MBIST_DOUT3H  register                    3CH           */
    __IO uint32_t   CHKRSLT;                        /*!< MBIST_CHKRSLT  register                   40H           */
    __IO uint32_t   DFCTSECT0;                      /*!< MBIST_DFCT_SECT0  register                44H           */
    __IO uint32_t   DFCTSECT1;                      /*!< MBIST_DFCT_SECT1  register                48H           */
} MBIST_TypeDef;
//</h>
//<h>MCR_TypeDef


/**
 * @brief ModeCtrl MCFGRFU0
 */
typedef struct {                                    /*!< ModeCtrl Structure                                      */
    __I uint32_t    MODE;                           /*!< MCR_CHIP_MODE  register                00H              */
    __I uint32_t    NVMF;                           /*!< MCR_NVM_FLAG register                  04H              */
    __I uint32_t    ORGCFG;                         /*!< MCR_ORG_CFG register                   08H              */
    uint32_t        Reserved_0;                     /*!< Reserved                               0CH              */
    __I uint32_t    MANUCFG;                        /*!< MCR_MANU_CFG register                  10H              */
    __IO uint32_t   USERCFG;                        /*!< MCR_USER_CFG register                  14H              */
    __I uint32_t    OTPCFG;                         /*!< MCR_OTP_CFG register                   18H              */
    __I uint32_t    MPUEN;                          /*!< MCR_MPUEN  register                    1CH              */
    __I uint32_t    MCFGRFU0;                       /*!< MCR_MCFG_RFU0  register                20H              */
    __I uint32_t    MCFGRFU1;                       /*!< MCR_MCFG_RFU1  register                24H              */
    __IO uint32_t   UCFGRFU0;                       /*!< MCR_UCFG_RFU0  register                28H              */
    __IO uint32_t   UCFGRFU1;                       /*!< MCR_UCFG_RFU0  register                2CH              */
    __I uint32_t    MODEENP;                        /*!< MCR_MODE_EN_P  register                30H              */
    __I uint32_t    MODEENN;                        /*!< MCR_MODE_EN_N  register                34H              */
    __I uint32_t    MODEWORDP;                      /*!< MCR_MODE_WORD_P  register              38H              */
    __I uint32_t    MODEWORDN;                      /*!< MCR_MODE_WORD_N  register              3CH              */
} MCR_TypeDef;
//</h>
//<h>MPU_TypeDef


/**
 * @brief MPU
 */

typedef struct {
    __IO uint32_t CSR;                          /*!< Control Status Register, offset: 0x0 */
    struct {                                    /* offset: 0x04, array step: 0x8 */
        __I uint32_t    EAR;                    /*!< Error Address Register, slave port n, array offset: 0x4, array step: 0x8 */
        __I uint32_t    EDR;                    /*!< Error Detail Register, slave port n, array offset: 0x8, array step: 0x8 */
    } SP[4];
    struct {                                    /* offset: 0x24, array step: 0x8 */
        __IO uint32_t   SADDR;                  /*!< Start Address Register, region n, array offset: 0x24, array step: 0x1C */
        __IO uint32_t   EADDR;                  /*!< End Address Register, region n, array offset: 0x28, array step: 0x1C */
        __IO uint32_t   AUTHA;                  /*!< Authority A Register, region n, array offset: 0x2C, array step: 0x1C */
        __IO uint32_t   AUTHB;                  /*!< Authority B Register, region n, array offset: 0x30, array step: 0x1C */
        __IO uint32_t   AUTHC;                  /*!< Authority C Register, region n, array offset: 0x34, array step: 0x1C */
        __IO uint32_t   CTRL;                   /*!< Control Register, region n, array offset: 0x38, array step: 0x1C */
        __IO uint32_t   SWCTL;                  /*!< Software control Register, region n, array offset: 0x3C, array step: 0x1C */
    } REGION[8];
} MPU_TypeDef;
//</h>
//<h>PAE_TypeDef


/**
 * @brief PAE
 */

typedef struct {                                    /*!< PAE Structure                                              */
    __IO uint32_t   CSR;                            /*!< REG_PAE_CFG   register                  00H                */
    __IO uint32_t   MLR;                            /*!< REG_PAE_MLR register                    04H                */
    __IO uint32_t   MPR;                            /*!< REG_PAE_MPR   register                  08H                */
    __IO uint32_t   WORD;                           /*!< REG_PAE_WORD   register                 0CH                */
    __IO uint32_t   M0CFG;                          /*!< REG_PAE_M0CFG   register                10H                */
    __IO uint32_t   M1CFG;                          /*!< REG_PAE_M1CFG   register                14H                */
    __IO uint32_t   M2CFG;                          /*!< REG_PAE_M2CFG   register                18H                */
    __IO uint32_t   M3CFG;                          /*!< REG_PAE_M3CFG   register                1CH                */
    __IO uint32_t   M4CFG;                          /*!< REG_PAE_M4CFG   register                20H                */
    __IO uint32_t   M5CFG;                          /*!< REG_PAE_M5CFG   register                24H                */
    __IO uint32_t   M6CFG;                          /*!< REG_PAE_M6CFG   register                28H                */
    __IO uint32_t   M7CFG;                          /*!< REG_PAE_M7CFG   register                2CH                */
    __IO uint32_t   M8_CFG;                         /*!< REG_PAE_M8_CFG  register                30H                */
    __IO uint32_t   M8_PR0;                         /*!< REG_PAE_M8_PR0  register                34H                */
    __IO uint32_t   M8_PR1;                         /*!< REG_PAE_M8_PR1  register                38H                */
    __IO uint32_t   M8_PR2;                         /*!< REG_PAE_M8_PR2  register                3CH                */
    __IO uint32_t   M8_PR3;                         /*!< REG_PAE_M8_PR3  register                40H                */
    __IO uint32_t   M8_PR4;                         /*!< REG_PAE_M8_PR4  register                44H                */
    __IO uint32_t   M9_CFG;                         /*!< REG_PAE_M9_CFG  register                48H                */
    __IO uint32_t   M10_CFG;                        /*!< REG_PAE_M10_CFG register                4CH                */
} PAE_TypeDef;
//</h>
//<h>PAE_RAMTypeDef


/**
 * @brief PAE_RAM offset 100H
 */

typedef struct {                                    /*!< PAE Structure                                              */
    __IO uint32_t   RAMPATH_CTRL;                   /*!< REG_PAE_RAMPATH_CTRL   register        100H                */
    __IO uint32_t   RAMENCRY_CTRL;                  /*!< REG_PAE_RAMENCRY_CTRL register         104H                */
    __IO uint32_t   RAMENCRY_KEY;                   /*!< REG_PAE_RAMENCRY_KEY register          108H                */
    __IO uint32_t   ADDR_MASK;                      /*!< REG_PAE_RAMENCRY_KEY register          10CH                */
    __IO uint32_t   DATA_MASK;                      /*!< REG_PAE_RAMENCRY_KEY register          110H                */
} PAE_RAMTypeDef;
//</h>
//<h>PAE_RAMBISTTypeDef


/**
 * @brief PAE_RAMBIST offset 200H
 */

typedef struct {                                    /*!< PAE Structure                                              */
    __IO uint32_t   CTRL;                           /*!< REG_PAE_RAMBIST_CTRL   register        200H                */
    __IO uint32_t   STATUS;                         /*!< REG_PAE_RAMBIST_STATUS register        204H                */
    __IO uint32_t   STATUSCLR;                      /*!< REG_PAE_RAMBIST_STATUS_CLR register    208H                */
} PAE_RAMBISTTypeDef;
//</h>

//<h>QSPI_TypeDef


/**
 * @brief QuadSPI
 */

typedef struct {                                    /*!< QSPI Structure                                             */
    __IO uint32_t   CFG;                            /*!< qspi_cfg       register                 00H                */
    __IO uint32_t   DORDER;                         /*!< qspi_data_order  register               04H                */
    __IO uint32_t   MODE;                           /*!< qspi_mode_cfg   register                08H                */
    __IO uint32_t   IOMODE;                         /*!< qspi_iomode_cfg  register               0CH                */
    __IO uint32_t   SCNTTHR;                        /*!< qspi_stall_cnt_thr  register            10H                */
    __IO uint32_t   DLEN;                           /*!< qspi_data_len    register               14H                */
    __IO uint32_t   CMD;                            /*!< qspi_cmd_cfg     register               18H                */
    __IO uint32_t   FADDR;                          /*!< qspi_flash_addr     register            1CH                */
    __IO uint32_t   MODEALT;                        /*!< qspi_modealt_cfg     register           20H                */
    __I uint32_t    RXFIFOD;                        /*!< qspi_rxfifo_dout     register           24H                */
    __IO uint32_t   RXFIFOCFG;                      /*!< qspi_rxifo_cfg       register           28H                */
    __I uint32_t    RXFIFOS;                        /*!< qspi_rxfifo_status   register           2CH                */
    __O uint32_t    TXFIFOD;                        /*!< qspi_txfifo_din      register           30H                */
    __IO uint32_t   TXFIFOCFG;                      /*!< qspi_txifo_cfg       register           34H                */
    __I uint32_t    TXFIFOS;                        /*!< qspi_txfifo_status   register           38H                */
    __IO uint32_t   SOFTCLR;                        /*!< qspi_soft_clr        register           3CH                */
    __IO uint32_t   SAMPCFG;                        /*!< qspi_sample_cfg      register           40H                */
    __IO uint32_t   Reserved_0[16];                 /*!< Reserved             register        44-80H                */
    __IO uint32_t   CLKCFG;                         /*!< qspi_clk_cfg         register           84H                */
    __IO uint32_t   ABORTCFG;                       /*!< qspi_abort_cfg       register           88H                */
    __I uint32_t    STATUS;                         /*!< qspi_status          register           8CH                */
    __IO uint32_t   INTEN;                          /*!< qspi_inten           register           90H                */
} QSPI_TypeDef;
//</h>
//<h>RAND_TypeDef


/**
 * @brief RANDOM_CTRL
 */

typedef struct {                                    /*!< RANDOM_CTRL Structure                                  */
    __IO uint32_t   CTRL;                           /*!< REG_RAND_CTRL  register             00H                */
    __IO uint32_t   STATUS;                         /*!< REG_RAND_STATUS   register          04H                */
    __I uint32_t    TRNG_DATA1;                     /*!< REG_RAND_TRNG_DATA1   register      08H                */
    __I uint32_t    TRNG_DATA2;                     /*!< REG_RAND_TRNG_DATA2   register      0CH                */
    __I uint32_t    TRNG_DATA3;                     /*!< REG_RAND_TRNG_DATA3   register      10H                */
    __I uint32_t    TRNG_DATA4;                     /*!< REG_RAND_TRNG_DATA4   register      14H                */
    __I uint32_t    DRNG_DATA;                      /*!< REG_RAND_DRNG_DATA    register      18H                */
    __IO uint32_t   DRNG_SEED;                      /*!< REG_RAND_INNER_DRNG_SEED register   1CH                */
} RAND_TypeDef;
//</h>
//<h>RAMC_TypeDef


/**
 * @brief RAMCTRL_CSR
 */

typedef struct {                                    /*!< RAMCFG Structure                                       */
    __IO uint32_t   RAMBISTCFG;                     /*!< REG_RAMBIST_CFG  register           00H                */
    __IO uint32_t   RAMBISTS;                       /*!< REG_RAMBIST_STATUS  register        04H                */
    __IO uint32_t   HAMCNTTHRL;                     /*!< REG_HAM_CNT_THRESH_L  register      08H                */
    __IO uint32_t   HAMCNTTHRU1;                    /*!< REG_HAM_CNT_THRESH_U1  register     0CH                */
    __IO uint32_t   HAMCNTTHRU2;                    /*!< REG_HAM_CNT_THRESH_U2  register     10H                */
    __IO uint32_t   RAMREPAIR;                      /*!< REG_RAMREPAIR_CFG      register     14H                */
    __IO uint32_t   RAMFLTS;                        /*!< REG_RAMFAULT_STATUS    register     18H                */
    __IO uint32_t   RAMENCRY;                       /*!< REG_RAM_ENCRY_CFG      register     1CH                */
    __IO uint32_t   RAMENCRYKL;                     /*!< REG_RAM_ENCRY_KEY_L    register     20H                */
    __IO uint32_t   RAMENCRYKU1;                    /*!< REG_RAM_ENCRY_KEY_U1   register     24H                */
    __IO uint32_t   RAMENCRYKU2;                    /*!< REG_RAM_ENCRY_KEY_U2   register     28H                */
    __IO uint32_t   RAMEMA;                         /*!< REG_RAM_EMA_CFG        register     2CH                */
    __IO uint32_t   RAMFLTA1S;                      /*!< REG_RAMFAULT_ADDR1_STATUS           30H                */
    __IO uint32_t   RAMFLTA2S;                      /*!< REG_RAMFAULT_ADDR2_STATUS           34H                */
    __IO uint32_t   RAMREPA1;                       /*!< REG_RAMREPAIR_ADDR1_CFG             38H                */
    __IO uint32_t   RAMREPA2;                       /*!< REG_RAMREPAIR_ADDR2_CFG             3CH                */
    __IO uint32_t   RAMPRTYS;                       /*!< REG_RAM_PARITY_STATUS               40H                */
} RAMC_TypeDef;
//</h>
//<h>RTC_VMAIN_TypeDef


/**
 * @brief RTC_VMAIN
 */

typedef struct {                    /*!< RTC_VMAIN Structure                                                  */
    __IO uint32_t   TWA;            /*!< RTC time register write authority register         00H               */
    __IO uint32_t   TRA;            /*!< RTC time register read authority register          04H               */
    uint32_t        Reserved_0[6];  /*08H~1CH*/
    __IO uint32_t   RDPE;           /*!< RTC read data parity error register                20H               */
} RTC_VMAIN_TypeDef;
//</h>
//<h>RTC_TIME_TypeDef


/**
 * @brief RTC_TIME
 */

typedef struct {                                    /*!< RTC_TIME Structure                                              */
    __IO uint32_t   SECONDS;                        /*!< RTC time seconds register                          00H               */
    __IO uint32_t   CYCLE;                          /*!< RTC time cycle register                            04H               */
    __IO uint32_t   PSC;                            /*!< RTC time prescaler register                        08H               */
    uint32_t        Reserved_0[1];                  /*!< Reserved               array offset: 0x0C, array step:0x4            */
    __IO uint32_t   SECALARM;                       /*!< RTC time seconds alarm register                    10H               */
    __IO uint32_t   CYCALARM;                       /*!< RTC time cycle alarm register                      14H               */
    uint32_t        Reserved_1[2];                  /*!< Reserved               array offset: 0x18, array step:0x4            */
    __IO uint32_t   COMPENSATION;                   /*!< RTC time compensation register                     20H               */
    uint32_t        Reserved_2[3];                  /*!< Reserved               array offset: 0x24, array step:0x4            */
    __IO uint32_t   CTRL;                           /*!< RTC time control register                          30H               */
    __IO uint32_t   CLKCFG;                         /*!< RTC time clk cfg register                          34H               */
    __IO uint32_t   CNTEN;                          /*!< RTC time CNT en  register                          38H               */
    __IO uint32_t   WAKUPCFG;                       /*!< RTC time wake up cfg register                      3CH               */
    __IO uint32_t   TIMEIE;                         /*!< RTC time int en register                           40H               */
    __IO uint32_t   WAKUPEN;                        /*!< RTC time wake up en register                       44H               */
    __IO uint32_t   RECORDCLR;                      /*!< RTC time record clr register                       48H               */
    __IO uint32_t   SOFTRST;                        /*!< RTC time soft reset register                       4CH               */
    __IO uint32_t   LOCK;                           /*!< RTC time lock register                             50H               */
    uint32_t        Reserved_3[3];                  /*!< Reserved               array offset: 0x54, array step:0x4            */
    __I uint32_t    STATUS;                         /*!< RTC time status register                           60H               */
    __I uint32_t    COMPSTAUTS;                     /*!< RTC time compensation status                       64H               */
    __I uint32_t    TAMTSEC;                        /*!< RTC time tamper time seconds register              68H               */
} RTC_TIME_TypeDef;
//</h>
//<h>BKP_VMAIN_TypeDef


/**
 * @brief BKP_VMAIN
 */

typedef struct {                    /*!< BKP_VMAIN Structure                                                 */
    uint32_t        Reserved_0[2];  /*00H~04H*/
    __IO uint32_t   TAMWA;          /*!< BKP tamper register write authority register       08H               */
    __IO uint32_t   TAMRA;          /*!< BKP tamper register read authority register        0CH               */
    __IO uint32_t   KWA;            /*!< BKP key register write authority register          10H               */
    __IO uint32_t   KRA;            /*!< BKP key register read  authority register          14H               */
    uint32_t        Reserved_1[3];  /*18H~20H*/
    __IO uint32_t   ISOC;           /*!< RTC isolation control register                     24H               */
    __IO uint32_t   BATCC;          /*!< RTC bat connect count register                     28H               */
    uint32_t        Reserved_2;     /*2CH*/
    __IO uint32_t   TSTCLKEN;       /*!< BKP 4M clock output enable register                30H               */
} BKP_VMAIN_TypeDef;
//</h>

//<h>BKP_TypeDef


/**
 * @brief BKP
 */

typedef struct {                    /*!< BKP Structure                                            */
    __IO uint32_t   EN;             /*!< BKP vbat tamper enable register                    200H               */
    __IO uint32_t   AUTOEN;         /*!< BKP tamper auto enable register                    204H               */
    __I uint32_t    AUTOINVL0;      /*!< BKP tamper auto interval0 register                 208H               */
    __IO uint32_t   INTEN;          /*!< BKP tamper interrupt enable register               20CH               */
    __IO uint32_t   CTRL;           /*!< BKP tamper control  register                       210H               */
    __IO uint32_t   SOFTRST;        /*!< BKP tamper soft reset  register                    214H               */
    __IO uint32_t   STATUS;         /*!< BKP tamper status  register                        218H               */
    __IO uint32_t   LOCK;           /*!< BKP tamper lock  register                          21CH               */
    __IO uint32_t   TRIM;           /*!< BKP tamper trim  register                          220H               */
    // uint32_t  Reserved_0;                             /*220H*/
    __IO uint32_t   PCFG;           /*!< BKP tamper pin configure  register                 224H               */
    __IO uint32_t   PDIR;           /*!< BKP tamper pin direction  register                 228H               */
    __IO uint32_t   PLRTY;          /*!< BKP tamper pin polarity  register                  22CH               */
    __IO uint32_t   PxGF[8];        /*!< BKP tamper pinx glitch filter  register            230H~24CH          */
    __I uint32_t    ACTTAM0;        /*!< BKP active tamper0 register                        250H               */
    __I uint32_t    ACTTAM1;        /*!< BKP active tamper1 register                        254H               */
    uint32_t        Reserved_1;     /*258H*/
    __IO uint32_t   GPIOEN;         /*!< BKP gpio en register                               25CH               */
    __IO uint32_t   GPIODIR;        /*!< BKP gpio dir register                              260H               */
    __IO uint32_t   GPIODOUT;       /*!< BKP gpio dout register                             264H               */
    __IO uint32_t   GPIODOS;        /*!< BKP gpio dout set register                         268H               */
    __IO uint32_t   GPIODOC;        /*!< BKP gpio en register                               26CH               */
    __IO uint32_t   GPIODOT;        /*!< BKP gpio dout tog register                         270H               */
    __I uint32_t    GPIODIN;        /*!< BKP gpio din register                              274H               */
    __IO uint32_t   GPIOCLKC;       /*!< BKP gpio clk ctrl register                         278H               */
    uint32_t        Reserved_2;     /*27CH*/
    __IO uint32_t   APPLOCK;        /*!< BKP vbat tamper app LOCK register                  280H               */
    __IO uint32_t   APPCFG;         /*!< BKP vbat tamper app CFG register                   284H               */
    uint32_t        Reserved_3[6];  /*288H~29CH*/
    __IO uint32_t   TAMLFSRDATA;    /*!< BKP vbat tamper app tamper lfsr data register      2A0H               */
    __IO uint32_t   TAMLFSRSEQ;     /*!< BKP vbat tamper app tamper lfsr data sequence      2A4H               */
    __IO uint32_t   TAMPASSCFG;     /*!< BKP vbat tamper app tamper pass cfg                2A8H               */
    __IO uint32_t   TAMFILTERCFG;   /*!< BKP vbat tamper app tamper filter cfg              2ACH               */
    uint32_t        Reserved_4[20]; /*2B0H~2FCH*/
    __IO uint32_t   VALID;          /*!< BKP key valid register                             300H               */
    __IO uint32_t   WLOCK;          /*!< BKP write lock register                            304H               */
    __IO uint32_t   RLOCK;          /*!< BKP read lock register                             308H               */
    __IO uint32_t   KLOCK;          /*!< BKP key lock register                              30CH               */
    __IO uint32_t   RFKEY;          /*!< BKP regfile key register                           310H               */
    uint32_t        Reserved_5[59]; /*314H~3FCH*/
    __IO uint32_t   KEYRF[8];       /*!< BKP key regfile register                      400H~41CH               */
    uint32_t        Reserved_6[56]; /*420H~4FCH*/
    __IO uint32_t   NORMRF[32];     /*!< BKP normal regfile register                   500H~57CH               */
} BKP_TypeDef;
//</h>

//<h>SDMA_TypeDef


/**
 * @brief security dma
 */
typedef struct {                                    /*!< SECURITY DMA Structure                                */
    __IO uint32_t   CFG;                            /*!< REG_SDMA_CFG               register        00H                */
    __IO uint32_t   APP_CFG;                        /*!< REG_SDMA_APP_CFG           register        04H                */
    __IO uint32_t   MASK;                           /*!< REG_SDMA_MASK              register        08H                */
    __IO uint32_t   CTRL;                           /*!< REG_SDMA_CTRL              register        0CH                */
    __IO uint32_t   INT_EN;                         /*!< REG_SDMA_INT_EN            register        10H                */
    __IO uint32_t   INT_CLR;                        /*!< REG_SDMA_INT_CLR           register        14H                */
    uint32_t        Reserved_0[2];
    __IO uint32_t   SADDR;                          /*!< REG_SDMA_S_ADDR            register        20H                */
    __IO uint32_t   DADDR;                          /*!< REG_SDMA_D_ADDR            register        24H                */
    __IO uint32_t   TRANS_LEN;                      /*!< REG_SDMA_TRANS_LENGTH      register        28H                */
    uint32_t        Reserved_1;
    __IO uint32_t   CURRENT_CNT;                    /*!< REG_SDMA_TRANS_LENGTH      register        30H                */
    __IO uint32_t   STATUS;                         /*!< REG_SDMA_TRANS_LENGTH      register        34H                */
} SDMA_TypeDef;
//</h>
//<h>SPI_TypeDef


/**
 * @brief SPI
 */

typedef struct {                                    /*!< SPI_NEW Structure                                          */
    __IO uint32_t   SPCR1;                          /*!< SPCR1         register                  00H                */
    __IO uint32_t   SPCR2;                          /*!< SPCR2         register                  04H                */
    __IO uint32_t   TXFIFO;                         /*!< TXFIFO        register                  08H                        */
    __IO uint32_t   RXFIFO;                         /*!< RXFIFO        register                  0CH                */
    __IO uint32_t   SPSR;                           /*!< STATUS        register                  10H                */
    __IO uint32_t   SPIIE;                          /*!< SPIIE          register                  14H                */
    uint32_t        Reserved_0[2];                  /*!< Reserved               array offset: 0x18, array step:0x4                */
    __IO uint32_t   SPCR3;                          /*!< SPCR3         register                  20H                */
    __IO uint32_t   SPCR4;                          /*!< SPCR4         register                  24H                */
    uint32_t        Reserved_1[2];                  /*!< Reserved               array offset: 0x28, array step:0x4                */
    __IO uint32_t   SPDF;                           /*!< FIFOSTATUS    register                  30H                */
} SPI_TypeDef;
//</h>
//<h>ST_TypeDef


/**
 * @brief Simple Timer / Counter ST(0-5)
 */
typedef struct {                                    /*!< Simple TIMER Structure                                             */
    __IO uint32_t   CTRL;                           /*!< Control Register                                00H                */
    __IO uint32_t   RESET;                          /*!< RESET Register                                  04H                */
    __IO uint32_t   LOAD;                           /*!< LOAD  register                                  08H                */
    __IO uint32_t   PSC[6];                         /*!< Simple timer0~5 prescaler  register          0C~20H                */
    __IO uint32_t   CFG[6];                         /*!< Simple timer0~5 configure  register          24~38H                */
    __IO uint32_t   TGSCH[8];                       /*!< TGS channel0~7   register                    3C~58H                */
    __IO uint32_t   TGSSWTRIG;                      /*!< TGS sw trigg Register                           5CH                */
    __IO uint32_t   TRINCFG[6];                     /*!< Trigger in configure0~5  register            60~74H                */
    __IO uint32_t   FAULTCFG[6];                    /*!< Fault configure0~5  register                 78~8CH                */
    __IO uint32_t   MOD0[6];                        /*!< Stimer0~5 mod0 value  register               90~A4H                */
    __IO uint32_t   MOD1[6];                        /*!< Stimer0~5 mod1 value  register               A8~BCH                */
    __I uint32_t    CVAL[6];                        /*!< Stimer0~5 current value  register            C0~D4H                */
    uint32_t        Reserved0;                      /*!<                                                  D8H                */
    __I uint32_t    FLAG;                           /*!< Simple timer flag status  register              DCH                */
    __IO uint32_t   IFCL;                           /*!< Simple timer interrupt and flag clear  register E0H                */
} ST_TypeDef;
//</h>
//<h>UART_TypeDef


/**
 * @brief UART
 */

typedef struct {                                    /*!< UART Structure                                             */
    __IO uint32_t   CR;                             /*!< Uart control  register                  00H                */
    __IO uint32_t   BIT9CR;                         /*!< Uart bit9 control register              04H                */
    __IO uint32_t   INTCFG;                         /*!< Interrupt configure register            08H                      */
    __IO uint32_t   STATUS;                         /*!< Uart status register                    0CH                */
    __I uint32_t    RXFIFO;                         /*!< Uart receive register                   10H                */
    __O uint32_t    TXFIFO;                         /*!< Uart transfer register                  14H                */
    __IO uint32_t   ACR;                            /*!< UART_ACR register                       18H                */
    __IO uint32_t   SPEED;                          /*!< Uart speed register                     1CH                */
} UART_TypeDef;
//</h>
//<h>USB_PHYTypeDef


/**
 * @brief USB_PHY
 */

typedef struct {                                    /*!< USB_PHY Structure                                          */
    __IO uint32_t   RAMBISTCFG;                     /*!< REG_USB_RAMBIST_CFG       register      00H                */
    __I uint32_t    RAMBISTS;                       /*!< REG_USB_RAMBIST_STATUS register         04H                */
    __IO uint32_t   HAMCNTTHR;                      /*!< REG_USB_HAM_CNT_THRESH     register     08H                */
    __IO uint32_t   RAMEMACFG;                      /*!< REG_USB_RAM_EMA_CFG        register     0CH                */
    __IO uint32_t   PHYCFG;                         /*!< REG_USB_PHY_CFG            register     10H                */
    __I uint32_t    PHYSTAT;                        /*!< REG_USB_PHY_STATUS         register     14H                */
} USB_PHYTypeDef;
//</h>
//<h>USB_OTG_GlobalTypeDef


/**
 * @brief __USB_OTG_Core_register
 */
typedef struct {
    __IO uint32_t   GOTGCTL;            /*!<  USB_OTG Control and Status Register    Address offset : 0x00      */
    __IO uint32_t   GOTGINT;            /*!<  USB_OTG Interrupt Register             Address offset : 0x04      */
    __IO uint32_t   GAHBCFG;            /*!<  Core AHB Configuration Register        Address offset : 0x08      */
    __IO uint32_t   GUSBCFG;            /*!<  Core USB Configuration Register        Address offset : 0x0C      */
    __IO uint32_t   GRSTCTL;            /*!<  Core Reset Register                    Address offset : 0x10      */
    __IO uint32_t   GINTSTS;            /*!<  Core Interrupt Register                Address offset : 0x14      */
    __IO uint32_t   GINTMSK;            /*!<  Core Interrupt Mask Register           Address offset : 0x18      */
    __IO uint32_t   GRXSTSR;            /*!<  Receive Sts Q Read Register            Address offset : 0x1C      */
    __IO uint32_t   GRXSTSP;            /*!<  Receive Sts Q Read & POP Register      Address offset : 0x20      */
    __IO uint32_t   GRXFSIZ;            /* Receive FIFO Size Register                Address offset : 0x24      */
    __IO uint32_t   DIEPTXF0_HNPTXFSIZ; /*!<  EP0 / Non Periodic Tx FIFO Size Register Address offset : 0x28    */
    __IO uint32_t   HNPTXSTS;           /*!<  Non Periodic Tx FIFO/Queue Sts reg     Address offset : 0x2C      */
    uint32_t        Reserved30[2];      /* Reserved                                  Address offset : 0x30      */
    __IO uint32_t   GCCFG;              /*!<  General Purpose IO Register            Address offset : 0x38      */
    __IO uint32_t   CID;                /*!< User ID Register                        Address offset : 0x3C      */
    uint32_t        Reserved40[48];     /*!< Reserved                                Address offset : 0x40-0xFF */
    __IO uint32_t   HPTXFSIZ;           /*!< Host Periodic Tx FIFO Size Reg          Address offset : 0x100     */
    __IO uint32_t   DIEPTXF[0x0F];      /*!< dev Periodic Transmit FIFO */
} USB_OTG_GlobalTypeDef;
//</h>

//<h>USB_OTG_DeviceTypeDef


/**
 * @brief __device_Registers
 */
typedef struct {
    __IO uint32_t   DCFG;           /*!< dev Configuration Register   Address offset : 0x800 */
    __IO uint32_t   DCTL;           /*!< dev Control Register         Address offset : 0x804 */
    __IO uint32_t   DSTS;           /*!< dev Status Register (RO)     Address offset : 0x808 */
    uint32_t        Reserved0C;     /*!< Reserved                     Address offset : 0x80C */
    __IO uint32_t   DIEPMSK;        /* !< dev IN Endpoint Mask        Address offset : 0x810 */
    __IO uint32_t   DOEPMSK;        /*!< dev OUT Endpoint Mask        Address offset : 0x814 */
    __IO uint32_t   DAINT;          /*!< dev All Endpoints Itr Reg    Address offset : 0x818 */
    __IO uint32_t   DAINTMSK;       /*!< dev All Endpoints Itr Mask   Address offset : 0x81C */
    uint32_t        Reserved20;     /*!< Reserved                     Address offset : 0x820 */
    uint32_t        Reserved9;      /*!< Reserved                     Address offset : 0x824 */
    __IO uint32_t   DVBUSDIS;       /*!< dev VBUS discharge Register  Address offset : 0x828 */
    __IO uint32_t   DVBUSPULSE;     /*!< dev VBUS Pulse Register      Address offset : 0x82C */
    __IO uint32_t   DTHRCTL;        /*!< dev thr                      Address offset : 0x830 */
    __IO uint32_t   DIEPEMPMSK;     /*!< dev empty msk                Address offset : 0x834 */
    __IO uint32_t   DEACHINT;       /*!< dedicated EP interrupt       Address offset : 0x838 */
    __IO uint32_t   DEACHMSK;       /*!< dedicated EP msk             Address offset : 0x83C */
    uint32_t        Reserved40;     /*!< dedicated EP mask            Address offset : 0x840 */
    __IO uint32_t   DINEP1MSK;      /*!< dedicated EP mask            Address offset : 0x844 */
    uint32_t        Reserved44[15]; /*!< Reserved                     Address offset : 0x844-0x87C */
    __IO uint32_t   DOUTEP1MSK;     /*!< dedicated EP msk             Address offset : 0x884 */
} USB_OTG_DeviceTypeDef;
//</h>
//<h>USB_OTG_INEndpointTypeDef


/**
 * @brief __IN_Endpoint-Specific_Register
 */
typedef struct {
    __IO uint32_t   DIEPCTL;    /* dev IN Endpoint Control Reg 900h + (ep_num * 20h) + 00h     */
    uint32_t        Reserved04; /* Reserved                       900h + (ep_num * 20h) + 04h  */
    __IO uint32_t   DIEPINT;    /* dev IN Endpoint Itr Reg     900h + (ep_num * 20h) + 08h     */
    uint32_t        Reserved0C; /* Reserved                       900h + (ep_num * 20h) + 0Ch  */
    __IO uint32_t   DIEPTSIZ;   /* IN Endpoint Txfer Size   900h + (ep_num * 20h) + 10h        */
    __IO uint32_t   DIEPDMA;    /* IN Endpoint DMA Address Reg    900h + (ep_num * 20h) + 14h  */
    __IO uint32_t   DTXFSTS;    /*IN Endpoint Tx FIFO Status Reg 900h + (ep_num * 20h) + 18h   */
    uint32_t        Reserved18; /* Reserved  900h+(ep_num*20h)+1Ch-900h+ (ep_num * 20h) + 1Ch  */
} USB_OTG_INEndpointTypeDef;
//</h>
//<h>USB_OTG_OUTEndpointTypeDef


/**
 * @brief __OUT_Endpoint-Specific_Registers
 */
typedef struct {
    __IO uint32_t   DOEPCTL;        /* dev OUT Endpoint Control Reg  B00h + (ep_num * 20h) + 00h*/
    uint32_t        Reserved04;     /* Reserved                      B00h + (ep_num * 20h) + 04h*/
    __IO uint32_t   DOEPINT;        /* dev OUT Endpoint Itr Reg      B00h + (ep_num * 20h) + 08h*/
    uint32_t        Reserved0C;     /* Reserved                      B00h + (ep_num * 20h) + 0Ch*/
    __IO uint32_t   DOEPTSIZ;       /* dev OUT Endpoint Txfer Size   B00h + (ep_num * 20h) + 10h*/
    __IO uint32_t   DOEPDMA;        /* dev OUT Endpoint DMA Address  B00h + (ep_num * 20h) + 14h*/
    uint32_t        Reserved18[2];  /* Reserved B00h + (ep_num * 20h) + 18h - B00h + (ep_num * 20h) + 1Ch*/
} USB_OTG_OUTEndpointTypeDef;
//</h>
//<h>USB_OTG_HostTypeDef


/**
 * @brief __Host_Mode_Register_Structures
 */
typedef struct {
    __IO uint32_t   HCFG;           /* Host Configuration Register    400h*/
    __IO uint32_t   HFIR;           /* Host Frame Interval Register   404h*/
    __IO uint32_t   HFNUM;          /* Host Frame Nbr/Frame Remaining 408h*/
    uint32_t        Reserved40C;    /* Reserved                       40Ch*/
    __IO uint32_t   HPTXSTS;        /* Host Periodic Tx FIFO/ Queue Status 410h*/
    __IO uint32_t   HAINT;          /* Host All Channels Interrupt Register 414h*/
    __IO uint32_t   HAINTMSK;       /* Host All Channels Interrupt Mask 418h*/
} USB_OTG_HostTypeDef;
//</h>
//<h>USB_OTG_HostChannelTypeDef


/**
 * @brief __Host_Channel_Specific_Registers
 */
typedef struct {
    __IO uint32_t   HCCHAR;
    __IO uint32_t   HCSPLT;
    __IO uint32_t   HCINT;
    __IO uint32_t   HCINTMSK;
    __IO uint32_t   HCTSIZ;
    __IO uint32_t   HCDMA;
    uint32_t        Reserved[2];
} USB_OTG_HostChannelTypeDef;
//</h>
//<h>VREF_TypeDef


/**
 * @brief VREF_CSR
 */

typedef struct {                                    /*!< VREF  Structure                                         */
    __IO uint32_t   CTRL;                           /*!< VREF control register                     00H           */
    __IO uint32_t   TRIM;                           /*!< VREF_TRIM     register                    04H           */
    __I uint32_t    BG_FLAG;                        /*!< VREF_BG_FLAG     register                 08H           */
} VREF_TypeDef;
//</h>
//<h>WDT_TypeDef


/**
 * @brief WDT
 */

typedef struct {                                    /*!< WDT Structure                                              */
    __IO uint32_t   CFG;                            /*!< WDT_CFG       register                  00H                */
    __IO uint32_t   WRCTRL;                         /*!< WDT_WRCTRL register                     04H                */
    __I uint32_t    CNT;                            /*!< WDT_CNT       register                  08H                */
} WDT_TypeDef;
//</h>
//</h>
//<h>Peripheral Memory Map


/**
 * @brief Peripheral_memory_map
 */
#define FLASH_BASE              ( (uint32_t) 0x00000000)    /*!< FLASH(up to 640KB) base address in the alias region    */
#define FLASH_MAIN_END          ( (uint32_t) 0x00097FFF)    /*!< FLASH(User Accessible) end address                 */
#define FLASH_END               ( (uint32_t) 0x0009FFFF)    /*!< FLASH end address                                      */
#define QSPI_BASE_ADDR          ( (uint32_t) 0x04000000)    //not register address
#define FSMC_BASE_ADDR          ( (uint32_t) 0x18000000)    //not register address
#define SRAML_BASE              ( (uint32_t) 0x1FFF8000)    /*!< SRAML(32 KB) base address in the alias region          */
#define SRAMH_BASE              ( (uint32_t) 0x20000000)    /*!< SRAMH(64 KB) base address in the alias region          */
#define SRAMH_BB_BASE           ( (uint32_t) 0x22000000)    /*!< SRAMH(2 MB) base address in the bit-band region        */
#define NVR0_USER               ( (uint32_t) 0x3FFF0000)    //NVR0_USER start address
#define NVR_END                 ( (uint32_t) 0x3FFF27FF)    //NVR end
#define UID_BASE                ( (uint32_t) 0x3FFF2098)    /*!< UID  register address   */
#define PERIPH_BASE             ( (uint32_t) 0x40000000)    /*!< Peripheral base address in the alias region            */
#define PERIPH_BB_BASE          ( (uint32_t) 0x42000000)    /*!< Peripheral base address in the bit-band region         */
#define FSMC_WRITE_BACK_BASE    ( (uint32_t) 0x60000000)    //not register address
#define QSPI_EXTERNAL_BASE      ( (uint32_t) 0x68000000)    //not register address
#define FSMC_WRITE_TR_BASE      ( (uint32_t) 0x98000000)    //not register address
#define FSMC_NEX_BASE           ( (uint32_t) 0xA0000000)    //not register address

/* Legacy defines */
#define SRAM_BASE SRAML_BASE


/*!< Peripheral memory map */
#define AIPS0PERIPH_BASE    PERIPH_BASE
#define AIPS1PERIPH_BASE    (PERIPH_BASE + 0x00080000)


/*!< AIPS0 peripherals */

//USB_OTG
#define USBREG_BASE             AIPS0PERIPH_BASE
#define USB_GLOBAL_OFFSET       0x000U
#define USB_DEVICE_OFFSET       0x800U
#define USB_IN_ENDPOINT_OFFSET  0x900U
#define USB_OUT_ENDPOINT_OFFSET 0xB00U
#define USB_EP_REG_SIZE         0x20U
#define USB_HOST_OFFSET         0x400U
#define USB_HOST_PORT_OFFSET    0x440U
#define USB_HOST_CHANNEL_OFFSET 0x500U
#define USB_HOST_CHANNEL_SIZE   0x20U
#define USB_PCGCCTL_OFFSET      0xE00U
#define USB_FIFO_OFFSET         0x1000U
#define USB_FIFO_SIZE           0x1000U


#define BUSMATRIX_BASE  (AIPS0PERIPH_BASE + 0x4000)
#define MPU_BASE        (AIPS0PERIPH_BASE + 0x5000)
#define DMACTRL_BASE    (AIPS0PERIPH_BASE + 0x8000)
#define DMACTCD_BASE    (AIPS0PERIPH_BASE + 0x9000)
#define DSP_BASE        (AIPS0PERIPH_BASE + 0xB000)
#define EFUSE_BASE      (AIPS0PERIPH_BASE + 0xC000)
#define FLASHMEM_BASE   (AIPS0PERIPH_BASE + 0xD000)
#define RAMCTRL_BASE    (AIPS0PERIPH_BASE + 0xE000)
#define CACHE_BASE      (AIPS0PERIPH_BASE + 0xF000)
#define DCMI_BASE       (AIPS0PERIPH_BASE + 0x10000)
#define DCMIDMA_BASE    (AIPS0PERIPH_BASE + 0x11000)
#define QSPI_BASE       (AIPS0PERIPH_BASE + 0x12000)
#define FSMC_BASE       (AIPS0PERIPH_BASE + 0x13000)

#define INTC_BASE   (AIPS0PERIPH_BASE + 0x1B000)
#define CRC_BASE    (AIPS0PERIPH_BASE + 0x1C000)

#define PAEREG_BASE (AIPS0PERIPH_BASE + 0x21000)
#define PAERAM_BASE (AIPS0PERIPH_BASE + 0x22000)
#define PAERF_BASE  (AIPS0PERIPH_BASE + 0x23000)
#define HASH_BASE   (AIPS0PERIPH_BASE + 0x24000)
#define BCA_BASE    (AIPS0PERIPH_BASE + 0x25000)

#define DMACHMUX_BASE   (AIPS0PERIPH_BASE + 0x30000)
#define SPI0_BASE       (AIPS0PERIPH_BASE + 0x31000)
#define SPI1_BASE       (AIPS0PERIPH_BASE + 0x32000)

#define TDC_BASE    (AIPS0PERIPH_BASE + 0x36000)
#define MBIST_BASE  TDC_BASE

#define STIMER_BASE     (AIPS0PERIPH_BASE + 0x37000)
#define ADC_BASE        (AIPS0PERIPH_BASE + 0x3B000)
#define ADC_MCH_BASE    (AIPS0PERIPH_BASE + 0x3B100)
#define DAC_BASE        (AIPS0PERIPH_BASE + 0x3F000)

#define PADCTRLA_BASE   (AIPS0PERIPH_BASE + 0x49000)
#define PADCTRLB_BASE   (AIPS0PERIPH_BASE + 0x4A000)
#define PADCTRLC_BASE   (AIPS0PERIPH_BASE + 0x4B000)
#define PADCTRLD_BASE   (AIPS0PERIPH_BASE + 0x4C000)
#define PADCTRLE_BASE   (AIPS0PERIPH_BASE + 0x4D000)
#define PADCTRLF_BASE   (AIPS0PERIPH_BASE + 0x4E000)

#define SDMA_BASE (AIPS0PERIPH_BASE + 0x50000)                     //security dma

#define I2C0_BASE   (AIPS0PERIPH_BASE + 0x66000)
#define I2C1_BASE   (AIPS0PERIPH_BASE + 0x67000)

#define CMP_BASE    (AIPS0PERIPH_BASE + 0x6A000)
#define JTAG_BASE   (AIPS0PERIPH_BASE + 0x6B000)
#define WDT0_BASE   (AIPS0PERIPH_BASE + 0x6C000)

#define USBPHYREG_BASE (AIPS0PERIPH_BASE + 0x6E000)

#define RTC_BASE        (AIPS0PERIPH_BASE + 0x79000)
#define RTC_VMAIN_BASE  (RTC_BASE)
#define RTC_TIME_BASE   (RTC_BASE + 0x100)

#define BKP_BASE_ADDR   (AIPS0PERIPH_BASE + 0x79000)
#define BKP_VMAIN_BASE  (BKP_BASE_ADDR)
#define BKP_BASE        (BKP_BASE_ADDR + 0x200)

/*!< LPTIMER  registers base address */
#define LPTIMER0_BASE (AIPS0PERIPH_BASE + 0x7B300)

/*!< ALM  registers base address */
#define ALM_BASE            (AIPS0PERIPH_BASE + 0x7B000)
#define ALM_RSTP_BASE       (ALM_BASE + 0xA0)
#define ALM_RST_BASE        (ALM_BASE + 0x160)
#define ALM_SENSORS0_BASE   (ALM_BASE + 0x90)
#define ALM_SENSORS1_BASE   (ALM_BASE + 0x1010)
#define ALM_MONITOR_BASE    (ALM_BASE + 0x1300)

/*!< PMU  registers base address */
#define PMU_BASE (AIPS0PERIPH_BASE + 0x7B010)


/*!< CMU  registers base address */
#define CMU_BASE (AIPS0PERIPH_BASE + 0x7B030)


#define PMC_BASE        (AIPS0PERIPH_BASE + 0x7D000)
#define MODECTRL_BASE   (AIPS0PERIPH_BASE + 0x7E000)
#define RSTCTRL_BASE    (AIPS0PERIPH_BASE + 0x7F000)

/*!< AIPS1 peripherals */
#define VREF_BASE       (AIPS1PERIPH_BASE + 0x1F000)
#define RANDCTRL_BASE   (AIPS1PERIPH_BASE + 0x20000)

#define SPI2_BASE   (AIPS1PERIPH_BASE + 0x2C000)
#define SPI3_BASE   (AIPS1PERIPH_BASE + 0x2D000)

#define UART0_BASE  (AIPS1PERIPH_BASE + 0x44000)
#define UART1_BASE  (AIPS1PERIPH_BASE + 0x45000)
#define UART2_BASE  (AIPS1PERIPH_BASE + 0x46000)

#define EMVSIM0_BASE    (AIPS1PERIPH_BASE + 0x54000)
#define EMVSIM1_BASE    (AIPS1PERIPH_BASE + 0x55000)

#define GPIOA_BASE  (AIPS1PERIPH_BASE + 0x7F000)
#define GPIOB_BASE  (AIPS1PERIPH_BASE + 0x7F040)
#define GPIOC_BASE  (AIPS1PERIPH_BASE + 0x7F080)
#define GPIOD_BASE  (AIPS1PERIPH_BASE + 0x7F0C0)
#define GPIOE_BASE  (AIPS1PERIPH_BASE + 0x7F100)
#define GPIOF_BASE  (AIPS1PERIPH_BASE + 0x7F140)


/**
 * @}
 */


/** @addtogroup Peripheral_declaration
 * @{
 */
#define BUSMAT      ( (BUSMAT_TypeDef *) BUSMATRIX_BASE)
#define MPU         ( (MPU_TypeDef *) MPU_BASE)
#define DMA         ( (DMA_TypeDef *) DMACTRL_BASE)
#define EFCR        ( (EFCR_TypeDef *) EFUSE_BASE)
#define FLASH       ( (FLASH_TypeDef *) FLASHMEM_BASE)
#define RAMC        ( (RAMC_TypeDef *) RAMCTRL_BASE)
#define CACHE       ( (CACHE_TypeDef *) CACHE_BASE)
#define DCMI_DMA    ( (DCMI_DMA_TypeDef *) DCMIDMA_BASE)
#define DCMI        ( (DCMI_TypeDef *) DCMI_BASE)
#define QSPI        ( (QSPI_TypeDef *) QSPI_BASE)
#define TGS         ( (TGS_TypeDef *) TGS_BASE)
#define DMAMUX      ( (DMAMUX_TypeDef *) DMACHMUX_BASE)
#define MBIST       ( (MBIST_TypeDef *) MBIST_BASE)
#define STIMER      ( (ST_TypeDef *) STIMER_BASE)
//#define ATIMER0                ((AT_TypeDef *) ATIMER0_BASE)
#define ADC0        ( (ADC_TypeDef *) ADC_BASE)
#define ADC_VREF    ( (ADC_VREFTypeDef *) VREF_BASE)
#define ADC_MCH     ( (ADC_MCH_TypeDef *) ADC_MCH_BASE)

#define DAC0        ( (DAC_TypeDef *) DAC_BASE)
#define GPIOA_CFG   ( (GPIO_CFG_TypeDef *) PADCTRLA_BASE)
#define GPIOB_CFG   ( (GPIO_CFG_TypeDef *) PADCTRLB_BASE)
#define GPIOC_CFG   ( (GPIO_CFG_TypeDef *) PADCTRLC_BASE)
#define GPIOD_CFG   ( (GPIO_CFG_TypeDef *) PADCTRLD_BASE)
#define GPIOE_CFG   ( (GPIO_CFG_TypeDef *) PADCTRLE_BASE)
#define GPIOF_CFG   ( (GPIO_CFG_TypeDef *) PADCTRLF_BASE)
#define CMP0        ( (CMP_TypeDef *) CMP_BASE)
#define I2C0        ( (I2C_TypeDef *) I2C0_BASE)
#define I2C1        ( (I2C_TypeDef *) I2C1_BASE)
#define WDT0        ( (WDT_TypeDef *) WDT0_BASE)
#define USB_PHY     ( (USB_PHYTypeDef *) USBPHYREG_BASE)
#define FSMC        ( (FSMC_TypeDef *) FSMC_BASE)
#define LPTIMER0    ( (LPTIMER_TypeDef *) LPTIMER0_BASE)


//uart
#define UART0   ( (UART_TypeDef *) UART0_BASE)
#define UART1   ( (UART_TypeDef *) UART1_BASE)
#define UART2   ( (UART_TypeDef *) UART2_BASE)

//ct
#define CT0 ( (CT_TypeDef *) EMVSIM0_BASE)
#define CT1 ( (CT_TypeDef *) EMVSIM1_BASE)

//RANDOM
#define RAND ( (RAND_TypeDef *) RANDCTRL_BASE)

//BCA
#define BCA ( (BCA_TypeDef *) BCA_BASE)

//HASH
#define HASH                ( (HASH_TypeDef *) HASH_BASE)
#define HASH_MSG_FIFO_BYTE  (*( (volatile unsigned char *) (&HASH->MSG_FIFO) ) )

//PAE
#define PAE         ( (PAE_TypeDef *) PAEREG_BASE)
#define PAE_RAMCTRL ( (PAE_RAMTypeDef *) (PAEREG_BASE + 0x100) )

//CRC
#define CRC ( (CRC_TypeDef *) CRC_BASE)

//SDMA
#define SDMA ( (SDMA_TypeDef *) SDMA_BASE)

//usb
#define USB_GLB ( (USB_OTG_GlobalTypeDef *) (USBREG_BASE + USB_GLOBAL_OFFSET) )
#define USB_DEV ( (USB_OTG_DeviceTypeDef *) (USBREG_BASE + USB_DEVICE_OFFSET) )

////////////////////////以下参考souli PAE定义///////////////
#define _SM9_PAE_32BLOCK_   //SM9,支持SM9硬件加速
#define PAE_                PAEREG_BASE
#define RSA_RAMA_           PAERAM_BASE
#define PAE_RAM_BASE        ( (volatile unsigned long *) (RSA_RAMA_) )

#define PAE_CSR     (*(volatile unsigned long *) (PAE_ + 0x000) )
#define PAE_MLR     (*(volatile unsigned long *) (PAE_ + 0x004) )
#define PAE_MPR     (*(volatile unsigned long *) (PAE_ + 0x008) )
#define PAE_WORD    (*(volatile unsigned long *) (PAE_ + 0x00C) )
#define PAE_M0CFG   (*(volatile unsigned long *) (PAE_ + 0x010) )
#define PAE_M1CFG   (*(volatile unsigned long *) (PAE_ + 0x014) )
#define PAE_M2CFG   (*(volatile unsigned long *) (PAE_ + 0x018) )
#define PAE_M3CFG   (*(volatile unsigned long *) (PAE_ + 0x01C) )
#define PAE_M4CFG   (*(volatile unsigned long *) (PAE_ + 0x020) )
#define PAE_M5CFG   (*(volatile unsigned long *) (PAE_ + 0x024) )
#define PAE_M6CFG   (*(volatile unsigned long *) (PAE_ + 0x028) )
#define PAE_M7CFG   (*(volatile unsigned long *) (PAE_ + 0x02C) )
#ifdef _SM9_PAE_32BLOCK_
#define PAE_M8_CFG  (*(volatile unsigned long *) (PAE_ + 0x30) )
#define PAE_M8_PR0  (*(volatile unsigned long *) (PAE_ + 0x34) )
#define PAE_M8_PR1  (*(volatile unsigned long *) (PAE_ + 0x38) )
#define PAE_M8_PR2  (*(volatile unsigned long *) (PAE_ + 0x3c) )
#define PAE_M8_PR3  (*(volatile unsigned long *) (PAE_ + 0x40) )
#define PAE_M8_PR4  (*(volatile unsigned long *) (PAE_ + 0x44) )
#define PAE_M9_CFG  (*(volatile unsigned long *) (PAE_ + 0x48) )
#define PAE_M10_CFG (*(volatile unsigned long *) (PAE_ + 0x4c) )
#endif
#define PAE_RAMPATH_CTRL    (*(volatile unsigned long *) (PAE_ + 0x100) )
#define PAE_RAMENCRY_CTRL   (*(volatile unsigned long *) (PAE_ + 0x104) )
#define PAE_RAMENCRY_KEY    (*(volatile unsigned long *) (PAE_ + 0x108) )
#define PAE_ADDR_MASK       (*(volatile unsigned long *) (PAE_ + 0x10C) )
#define PAE_DATA_MASK       (*(volatile unsigned long *) (PAE_ + 0x110) )


#ifdef _SM9_PAE_32BLOCK_
#define PAE_CARRY_FLAG      BIT12
#define PAE_EXCEPTION_FLAG  BIT9
#define PAE_START           BIT8
#define PAE_MODE            (BIT7 | BIT6 | BIT5 | BIT4)
#define PAE_SOFT_RESET      BIT3
#define PAE_BUSY            BIT0

#define PAE_RUN_MODE_0  0x00
#define PAE_RUN_MODE_1  (BIT4)                  //mode 1 ,bit[7:4]=0001
#define PAE_RUN_MODE_2  (BIT5)                  //mode 2 ,bit[7:4]=0010
#define PAE_RUN_MODE_3  (BIT5 | BIT4)           //mode 3 ,bit[7:4]=0011
#define PAE_RUN_MODE_4  (BIT6)                  //mode 4 ,bit[7:4]=0100
#define PAE_RUN_MODE_5  (BIT6 | BIT4)           //mode 5 ,bit[7:4]=0101
#define PAE_RUN_MODE_6  (BIT6 | BIT5)           //mode 6 ,bit[7:4]=0110
#define PAE_RUN_MODE_7  (BIT6 | BIT5 | BIT4)    //mode 7 ,bit[7:4]=0111
#define PAE_RUN_MODE_8  (BIT7)                  //mode 8 ,bit[7:4]=1000
#define PAE_RUN_MODE_9  (BIT7 | BIT4)           //mode 9 ,bit[7:4]=1001
#define PAE_RUN_MODE_10 (BIT7 | BIT5)           //mode 10,bit[7:4]=1010

#define PAE_ADC16_CMD   BIT30
#define PAE_ADC32_CMD   BIT31

#define PAE_SM9_NN2_ModAdd          0x00        //00000
#define PAE_SM9_NN2_ModSub          0x01        //00001
#define PAE_SM9_NN2_ModMult         0x02        //00010
#define PAE_SM9_NN2_ModSqr          0x03        //00011
#define PAE_SM9_NN4_ModAdd          0x04        //00100
#define PAE_SM9_NN4_ModSub          0x05        //00101
#define PAE_SM9_NN4_ModSqr          0x06        //00110
#define PAE_SM9_NN4_ModMult         0x07        //00111
#define PAE_SM9_NN4_Const_ModMult   0x08        //01000
#define PAE_SM9_NN6_B1              0x09        //01001
#define PAE_SM9_NN6_B0              0x0A        //01010
#define PAE_SM9_ECDBL_2             0x0B        //01011
#define PAE_SM9_ECADD_2             0x0C        //01100
#define PAE_SM9_ECADD_2_F           0x0D        //01101
#define PAE_SM9_NN12_ModSqr_M2      0x0E        //01110
#define PAE_SM9_LINE_1              0x0F        //01111
#define PAE_SM9_LINE_2              0x10        //10000
#define PAE_SM9_NN12_FixedT0_S1     0x11        //10001
#define PAE_SM9_NN12_FixedT0_S2     0x12        //10010
#define PAE_SM9_NN12_FixedT0_S3     0x13        //10011
#else
#define PAE_CARRY_FLAG      BIT12
#define PAE_EXCEPTION_FLAG  BIT8
#define PAE_START           BIT7
#define PAE_MODE            (BIT6 | BIT5 | BIT4)
#define PAE_SOFT_RESET      BIT3
#define PAE_BUSY            BIT0

#define PAE_RUN_MODE_0  0x00
#define PAE_RUN_MODE_1  (BIT4)                              //mode 1 ,bit[6:4]=001
#define PAE_RUN_MODE_2  (BIT5)                              //mode 2 ,bit[6:4]=010
#define PAE_RUN_MODE_3  (BIT5 | BIT4)                       //mode 3 ,bit[6:4]=011
#define PAE_RUN_MODE_4  (BIT6)                              //mode 4 ,bit[6:4]=100
#define PAE_RUN_MODE_5  (BIT6 | BIT4)                       //mode 5 ,bit[6:4]=101
#define PAE_RUN_MODE_6  (BIT6 | BIT5)                       //mode 6 ,bit[6:4]=110
#define PAE_RUN_MODE_7  (BIT6 | BIT5 | BIT4)                //mode 7 ,bit[6:4]=111
#define PAE_ADC16_CMD   BIT15
#endif

#define SPI0    ( (SPI_TypeDef *) SPI0_BASE)
#define SPI1    ( (SPI_TypeDef *) SPI1_BASE)
#define SPI2    ( (SPI_TypeDef *) SPI2_BASE)
#define SPI3    ( (SPI_TypeDef *) SPI3_BASE)


//ALM
#define ALM_SENSORS0    ( (ALM_SENSORS0TypeDef *) ALM_SENSORS0_BASE)
#define ALM_SENSORS1    ( (ALM_SENSORS1TypeDef *) ALM_SENSORS1_BASE)
#define ALM_MONITOR     ( (ALM_MONITORTypeDef *) ALM_MONITOR_BASE)
#define ALM_NRST        ( (ALM_NRSTTypeDef *) ALM_RSTP_BASE)
#define ALM_RST         ( (ALM_RSTTypeDef *) ALM_RST_BASE)

//PMU
#define PMU ( (PMU_TypeDef *) PMU_BASE)

//CMU
#define CMU ( (CMU_TypeDef *) CMU_BASE)

//---------------------------------------------------------------------------------
#define VREF ( (VREF_TypeDef *) VREF_BASE)
//RTC
#define RTC_VMAIN   ( (RTC_VMAIN_TypeDef *) RTC_VMAIN_BASE)
#define RTC_TIME    ( (RTC_TIME_TypeDef *) RTC_TIME_BASE)

//---------------------------------------------------------------------------------
//BKP
#define BKP_VMAIN   ( (BKP_VMAIN_TypeDef *) BKP_VMAIN_BASE)
#define BKP         ( (BKP_TypeDef *) BKP_BASE)


//---------------------------------------------------------------------------------
#define MCR     ( (MCR_TypeDef *) MODECTRL_BASE)
#define GPIOA   ( (GPIO_TypeDef *) GPIOA_BASE)
#define GPIOB   ( (GPIO_TypeDef *) GPIOB_BASE)
#define GPIOC   ( (GPIO_TypeDef *) GPIOC_BASE)
#define GPIOD   ( (GPIO_TypeDef *) GPIOD_BASE)
#define GPIOE   ( (GPIO_TypeDef *) GPIOE_BASE)
#define GPIOF   ( (GPIO_TypeDef *) GPIOF_BASE)

#define INTC ( (INTC_TypeDef *) INTC_BASE)
//</h>


/** @addtogroup Peripheral_Registers_Bits_Definition
 * @{
 */
//<h>Register Bits Definition
/******************************************************************************/
/*                         Peripheral Registers_Bits_Definition               */
/******************************************************************************/
//<h>ADC
/******************************************************************************/
/*                                                                            */
/*                                    ADC_CSR                                    */
/*                                                                            */
/******************************************************************************/
/*******************   Bit definition for ADC_CTRL register   ********************/
#define ADC_CTRL_ADCEN_Pos  (0U)
#define ADC_CTRL_ADCEN_Msk  (0x1U << ADC_CTRL_ADCEN_Pos)                            /*!< 0x00000001 */
#define ADC_CTRL_ADCEN      ADC_CTRL_ADCEN_Msk                                      /*!<adc_en */

#define ADC_CTRL_ADCRSTN_Pos    (1U)
#define ADC_CTRL_ADCRSTN_Msk    (0x1U << ADC_CTRL_ADCRSTN_Pos)                      /*!< 0x00000002 */
#define ADC_CTRL_ADCRSTN        ADC_CTRL_ADCRSTN_Msk                                /*!<adc_rstn */

/*******************   Bit definition for ADC_CFG register   ********************/
#define ADC_CFG_CLKSEL_Pos  (0U)
#define ADC_CFG_CLKSEL_Msk  (0x7U << ADC_CFG_CLKSEL_Pos)                            /*!< 0x00000007 */
#define ADC_CFG_CLKSEL      ADC_CFG_CLKSEL_Msk                                      /*!<clk_sel[2:0] */

#define ADC_CFG_VREFSEL_Pos (4U)
#define ADC_CFG_VREFSEL_Msk (0x1U << ADC_CFG_VREFSEL_Pos)                           /*!< 0x00000010 */
#define ADC_CFG_VREFSEL     ADC_CFG_VREFSEL_Msk                                     /*!<vref_sel */

#define ADC_CFG_LPM_Pos (5U)
#define ADC_CFG_LPM_Msk (0x1U << ADC_CFG_LPM_Pos)                                   /*!< 0x00000020 */
#define ADC_CFG_LPM     ADC_CFG_LPM_Msk                                             /*!<low_power_mode */

#define ADC_CFG_ASLEEP_Pos  (6U)
#define ADC_CFG_ASLEEP_Msk  (0x1U << ADC_CFG_ASLEEP_Pos)                            /*!< 0x00000040 */
#define ADC_CFG_ASLEEP      ADC_CFG_ASLEEP_Msk                                      /*!<auto_sleep */

#define ADC_CFG_VCOMEN_Pos  (7U)
#define ADC_CFG_VCOMEN_Msk  (0x1U << ADC_CFG_VCOMEN_Pos)                            /*!< 0x00000080 */
#define ADC_CFG_VCOMEN      ADC_CFG_VCOMEN_Msk                                      /*!<auto_sleep */

#define ADC_CFG_CLKWAITGATE_Pos (8U)
#define ADC_CFG_CLKWAITGATE_Msk (0x1U << ADC_CFG_CLKWAITGATE_Pos)                   /*!< 0x00000100 */
#define ADC_CFG_CLKWAITGATE     ADC_CFG_CLKWAITGATE_Msk                             /*!<auto_sleep */

#define ADC_CFG_CLKIDLEGATE_Pos (9U)
#define ADC_CFG_CLKIDLEGATE_Msk (0x1U << ADC_CFG_CLKIDLEGATE_Pos)                   /*!< 0x00000200 */
#define ADC_CFG_CLKIDLEGATE     ADC_CFG_CLKIDLEGATE_Msk                             /*!<auto_sleep */

#define ADC_CFG_CLKOUTPOR_Pos   (10U)
#define ADC_CFG_CLKOUTPOR_Msk   (0x1U << ADC_CFG_CLKOUTPOR_Pos)                     /*!< 0x00000400 */
#define ADC_CFG_CLKOUTPOR       ADC_CFG_CLKOUTPOR_Msk                               /*!<auto_sleep */

#define ADC_CFG_TSLEEP_Pos  (16U)
#define ADC_CFG_TSLEEP_Msk  (0xFFFU << ADC_CFG_TSLEEP_Pos)                          /*!< 0x0FFF0000 */
#define ADC_CFG_TSLEEP      ADC_CFG_TSLEEP_Msk                                      /*!<sleep_time[11:0] */

/*******************   Bit definition for ADC_TIME cfg register   ********************/
#define ADC_TIME_SAMPLE_Pos (0U)
#define ADC_TIME_SAMPLE_Msk (0xFFFU << ADC_TIME_SAMPLE_Pos)                         /*!< 0x00000FFF */
#define ADC_TIME_SAMPLE     ADC_TIME_SAMPLE_Msk                                     /*!<sample_time[11:0] */

#define ADC_TIME_READY_Pos  (12U)
#define ADC_TIME_READY_Msk  (0x7FFU << ADC_TIME_READY_Pos)                          /*!< 0x007FF000 */
#define ADC_TIME_READY      ADC_TIME_READY_Msk                                      /*!<ready_time[10:0] */

#define ADC_TIME_CONVERT_Pos    (24U)
#define ADC_TIME_CONVERT_Msk    (0xFU << ADC_TIME_CONVERT_Pos)                      /*!< 0x0F000000 */
#define ADC_TIME_CONVERT        ADC_TIME_CONVERT_Msk                                /*!<convert_time[3:0] */

#define ADC_TIME_SAMPLHOLD_Pos  (28U)
#define ADC_TIME_SAMPLHOLD_Msk  (0xFU << ADC_TIME_SAMPLHOLD_Pos)                    /*!< 0xF0000000 */
#define ADC_TIME_SAMPLHOLD      ADC_TIME_SAMPLHOLD_Msk                              /*!<sample_hold_time[3:0] */

/*******************   Bit definition for ADC_WORK register   ********************/
#define ADC_WORK_MODE_Pos   (0U)
#define ADC_WORK_MODE_Msk   (0x1U << ADC_WORK_MODE_Pos)                             /*!< 0x00000001 */
#define ADC_WORK_MODE       ADC_WORK_MODE_Msk                                       /*!<work_mode */

#define ADC_WORK_SAMPLETIMEEN_Pos   (1U)
#define ADC_WORK_SAMPLETIMEEN_Msk   (0x1U << ADC_WORK_SAMPLETIMEEN_Pos)             /*!< 0x00000002 */
#define ADC_WORK_SAMPLETIMEEN       ADC_WORK_SAMPLETIMEEN_Msk                       /*!<sample_time_en */

#define ADC_WORK_ONESHOT_Pos    (2U)
#define ADC_WORK_ONESHOT_Msk    (0x1U << ADC_WORK_ONESHOT_Pos)                      /*!< 0x00000004 */
#define ADC_WORK_ONESHOT        ADC_WORK_ONESHOT_Msk                                /*!<one_shot */

#define ADC_WORK_DONECHECKEN_Pos    (3U)
#define ADC_WORK_DONECHECKEN_Msk    (0x1U << ADC_WORK_DONECHECKEN_Pos)              /*!< 0x00000008 */
#define ADC_WORK_DONECHECKEN        ADC_WORK_DONECHECKEN_Msk                        /*!<adc_done_check_en */

#define ADC_WORK_DONESTOPEN_Pos (4U)
#define ADC_WORK_DONESTOPEN_Msk (0x1U << ADC_WORK_DONESTOPEN_Pos)                   /*!< 0x00000010 */
#define ADC_WORK_DONESTOPEN     ADC_WORK_DONESTOPEN_Msk                             /*!<adc_done_stop_en */

#define ADC_WORK_CONFLICTCONTEN_Pos (6U)
#define ADC_WORK_CONFLICTCONTEN_Msk (0x1U << ADC_WORK_CONFLICTCONTEN_Pos)           /*!< 0x00000040 */
#define ADC_WORK_CONFLICTCONTEN     ADC_WORK_CONFLICTCONTEN_Msk                     /*!<conflict_continue_en */

#define ADC_WORK_CONFLICTMODE_Pos   (7U)
#define ADC_WORK_CONFLICTMODE_Msk   (0x1U << ADC_WORK_CONFLICTMODE_Pos)             /*!< 0x00000080 */
#define ADC_WORK_CONFLICTMODE       ADC_WORK_CONFLICTMODE_Msk                       /*!<conflict_mode */

#define ADC_WORK_POLLINGNUM_Pos (8U)
#define ADC_WORK_POLLINGNUM_Msk (0xFU << ADC_WORK_POLLINGNUM_Pos)                   /*!< 0x00000F00 */
#define ADC_WORK_POLLINGNUM     ADC_WORK_POLLINGNUM_Msk                             /*!<polling_num[3:0] */

#define ADC_WORK_DONESAMPMODE_Pos   (12U)
#define ADC_WORK_DONESAMPMODE_Msk   (0x3U << ADC_WORK_DONESAMPMODE_Pos)             /*!< 0x00003000 */
#define ADC_WORK_DONESAMPMODE       ADC_WORK_DONESAMPMODE_Msk                       /*!<adc_done_sample_mode */

#define ADC_WORK_RESULTGETMODE_Pos  (20U)
#define ADC_WORK_RESULTGETMODE_Msk  (0x1U << ADC_WORK_RESULTGETMODE_Pos)            /*!< 0x00100000 */
#define ADC_WORK_RESULTGETMODE      ADC_WORK_RESULTGETMODE_Msk                      /*!<result_get_mode */

#define ADC_WORK_RESULTGETTM_Pos    (24U)
#define ADC_WORK_RESULTGETTM_Msk    (0xFU << ADC_WORK_RESULTGETTM_Pos)              /*!< 0x0F000000 */
#define ADC_WORK_RESULTGETTM        ADC_WORK_RESULTGETTM_Msk                        /*!<result_get_time */

/*******************   Bit definition for ADC_poll_seq register   ********************/
#define ADC_POLLSEQ_CH00SEL_Pos (0U)
#define ADC_POLLSEQ_CH00SEL_Msk (0x3U << ADC_POLLSEQ_CH00SEL_Pos)                   /*!< 0x00000003 */
#define ADC_POLLSEQ_CH00SEL     ADC_POLLSEQ_CH00SEL_Msk                             /*!<pseq00_ch_sel[1:0] */

#define ADC_POLLSEQ_CH01SEL_Pos (2U)
#define ADC_POLLSEQ_CH01SEL_Msk (0x3U << ADC_POLLSEQ_CH01SEL_Pos)                   /*!< 0x0000000C */
#define ADC_POLLSEQ_CH01SEL     ADC_POLLSEQ_CH01SEL_Msk                             /*!<pseq01_ch_sel[1:0] */

#define ADC_POLLSEQ_CH02SEL_Pos (4U)
#define ADC_POLLSEQ_CH02SEL_Msk (0x3U << ADC_POLLSEQ_CH02SEL_Pos)                   /*!< 0x00000030 */
#define ADC_POLLSEQ_CH02SEL     ADC_POLLSEQ_CH02SEL_Msk                             /*!<pseq02_ch_sel[1:0] */

#define ADC_POLLSEQ_CH03SEL_Pos (6U)
#define ADC_POLLSEQ_CH03SEL_Msk (0x3U << ADC_POLLSEQ_CH03SEL_Pos)                   /*!< 0x000000C0 */
#define ADC_POLLSEQ_CH03SEL     ADC_POLLSEQ_CH03SEL_Msk                             /*!<pseq03_ch_sel[1:0] */

#define ADC_POLLSEQ_CH04SEL_Pos (8U)
#define ADC_POLLSEQ_CH04SEL_Msk (0x3U << ADC_POLLSEQ_CH04SEL_Pos)                   /*!< 0x00000300 */
#define ADC_POLLSEQ_CH04SEL     ADC_POLLSEQ_CH04SEL_Msk                             /*!<pseq04_ch_sel[1:0] */

#define ADC_POLLSEQ_CH05SEL_Pos (10U)
#define ADC_POLLSEQ_CH05SEL_Msk (0x3U << ADC_POLLSEQ_CH05SEL_Pos)                   /*!< 0x00000C00 */
#define ADC_POLLSEQ_CH05SEL     ADC_POLLSEQ_CH05SEL_Msk                             /*!<pseq05_ch_sel[1:0] */

#define ADC_POLLSEQ_CH06SEL_Pos (12U)
#define ADC_POLLSEQ_CH06SEL_Msk (0x3U << ADC_POLLSEQ_CH06SEL_Pos)                   /*!< 0x00003000 */
#define ADC_POLLSEQ_CH06SEL     ADC_POLLSEQ_CH06SEL_Msk                             /*!<pseq06_ch_sel[1:0] */

#define ADC_POLLSEQ_CH07SEL_Pos (14U)
#define ADC_POLLSEQ_CH07SEL_Msk (0x3U << ADC_POLLSEQ_CH07SEL_Pos)                   /*!< 0x0000C000 */
#define ADC_POLLSEQ_CH07SEL     ADC_POLLSEQ_CH07SEL_Msk                             /*!<pseq07_ch_sel[1:0] */

#define ADC_POLLSEQ_CH08SEL_Pos (16U)
#define ADC_POLLSEQ_CH08SEL_Msk (0x3U << ADC_POLLSEQ_CH08SEL_Pos)                   /*!< 0x00030000 */
#define ADC_POLLSEQ_CH08SEL     ADC_POLLSEQ_CH08SEL_Msk                             /*!<pseq08_ch_sel[1:0] */

#define ADC_POLLSEQ_CH09SEL_Pos (18U)
#define ADC_POLLSEQ_CH09SEL_Msk (0x3U << ADC_POLLSEQ_CH09SEL_Pos)                   /*!< 0x000C0000 */
#define ADC_POLLSEQ_CH09SEL     ADC_POLLSEQ_CH09SEL_Msk                             /*!<pseq09_ch_sel[1:0] */

#define ADC_POLLSEQ_CH10SEL_Pos (20U)
#define ADC_POLLSEQ_CH10SEL_Msk (0x3U << ADC_POLLSEQ_CH10SEL_Pos)                   /*!< 0x00300000 */
#define ADC_POLLSEQ_CH10SEL     ADC_POLLSEQ_CH10SEL_Msk                             /*!<pseq10_ch_sel[1:0] */

#define ADC_POLLSEQ_CH11SEL_Pos (22U)
#define ADC_POLLSEQ_CH11SEL_Msk (0x3U << ADC_POLLSEQ_CH11SEL_Pos)                   /*!< 0x00C00000 */
#define ADC_POLLSEQ_CH11SEL     ADC_POLLSEQ_CH11SEL_Msk                             /*!<pseq11_ch_sel[1:0] */

#define ADC_POLLSEQ_CH12SEL_Pos (24U)
#define ADC_POLLSEQ_CH12SEL_Msk (0x3U << ADC_POLLSEQ_CH12SEL_Pos)                   /*!< 0x03000000 */
#define ADC_POLLSEQ_CH12SEL     ADC_POLLSEQ_CH12SEL_Msk                             /*!<pseq12_ch_sel[1:0] */

#define ADC_POLLSEQ_CH13SEL_Pos (26U)
#define ADC_POLLSEQ_CH13SEL_Msk (0x3U << ADC_POLLSEQ_CH13SEL_Pos)                   /*!< 0x0C000000 */
#define ADC_POLLSEQ_CH13SEL     ADC_POLLSEQ_CH13SEL_Msk                             /*!<pseq13_ch_sel[1:0] */

#define ADC_POLLSEQ_CH14SEL_Pos (28U)
#define ADC_POLLSEQ_CH14SEL_Msk (0x3U << ADC_POLLSEQ_CH14SEL_Pos)                   /*!< 0x30000000 */
#define ADC_POLLSEQ_CH14SEL     ADC_POLLSEQ_CH14SEL_Msk                             /*!<pseq14_ch_sel[1:0] */

#define ADC_POLLSEQ_CH15SEL_Pos (30U)
#define ADC_POLLSEQ_CH15SEL_Msk (0x3U << ADC_POLLSEQ_CH15SEL_Pos)                   /*!< 0xC0000000 */
#define ADC_POLLSEQ_CH15SEL     ADC_POLLSEQ_CH15SEL_Msk                             /*!<pseq15_ch_sel[1:0] */

/*******************   Bit definition for ADC_poll_delay register   ********************/
#define ADC_POLLDLY_DELAY_Pos   (0U)
#define ADC_POLLDLY_DELAY_Msk   (0xFFFFU << ADC_POLLDLY_DELAY_Pos)                  /*!< 0x0000FFFF */
#define ADC_POLLDLY_DELAY       ADC_POLLDLY_DELAY_Msk                               /*!<poll_delay[15:0] */


/*******************   Bit definition for ADC_SWTRG register   ********************/
#define ADC_SWTRG_CTRL_Pos  (0U)
#define ADC_SWTRG_CTRL_Msk  (0x1U << ADC_SWTRG_CTRL_Pos)                            /*!< 0x00000001 */
#define ADC_SWTRG_CTRL      ADC_SWTRG_CTRL_Msk                                      /*!<sw_trg_ctrl */

/*******************   Bit definition for ADC_CHA register   ********************/
#define ADC_CHxCFG_EN_Pos   (0U)
#define ADC_CHxCFG_EN_Msk   (0x1U << ADC_CHxCFG_EN_Pos)                             /*!< 0x00000001 */
#define ADC_CHxCFG_EN       ADC_CHxCFG_EN_Msk                                       /*!<chx_en */

#define ADC_CHxCFG_DIFF_Pos (1U)
#define ADC_CHxCFG_DIFF_Msk (0x1U << ADC_CHxCFG_DIFF_Pos)                           /*!< 0x00000002 */
#define ADC_CHxCFG_DIFF     ADC_CHxCFG_DIFF_Msk                                     /*!<chx_diff */

#define ADC_CHxCFG_IN_Pos   (2U)
#define ADC_CHxCFG_IN_Msk   (0x1FU << ADC_CHxCFG_IN_Pos)                            /*!< 0x0000007C */
#define ADC_CHxCFG_IN       ADC_CHxCFG_IN_Msk                                       /*!<chx_in[4:0] */

#define ADC_CHxCFG_TRGSC_Pos    (8U)
#define ADC_CHxCFG_TRGSC_Msk    (0xFU << ADC_CHxCFG_TRGSC_Pos)                      /*!< 0x00000F00 */
#define ADC_CHxCFG_TRGSC        ADC_CHxCFG_TRGSC_Msk                                /*!<chx_trg_sc[3:0] */

#define ADC_CHxCFG_TRGINV_Pos   (12U)
#define ADC_CHxCFG_TRGINV_Msk   (0x1U << ADC_CHxCFG_TRGINV_Pos)                     /*!< 0x00001000 */
#define ADC_CHxCFG_TRGINV       ADC_CHxCFG_TRGINV_Msk                               /*!<chx_trg_inv */

#define ADC_CHxCFG_TRGEDGE_Pos  (13U)
#define ADC_CHxCFG_TRGEDGE_Msk  (0x3U << ADC_CHxCFG_TRGEDGE_Pos)                    /*!< 0x00006000 */
#define ADC_CHxCFG_TRGEDGE      ADC_CHxCFG_TRGEDGE_Msk                              /*!<chx_trg_edge[1:0] */

#define ADC_CHxCFG_TRGDLY_Pos   (16U)
#define ADC_CHxCFG_TRGDLY_Msk   (0xFFFFU << ADC_CHxCFG_TRGDLY_Pos)                  /*!< 0xFFFF0000 */
#define ADC_CHxCFG_TRGDLY       ADC_CHxCFG_TRGDLY_Msk                               /*!<chx_trg_dly[15:0] */

/*******************   Bit definition for ADC_CHxDRS register   ********************/
#define ADC_CHxDRS_OFFEN_Pos    (0U)
#define ADC_CHxDRS_OFFEN_Msk    (0x1U << ADC_CHxDRS_OFFEN_Pos)                      /*!< 0x00000001 */
#define ADC_CHxDRS_OFFEN        ADC_CHxDRS_OFFEN_Msk                                /*!<chx_offset_en */

#define ADC_CHxDRS_AVGEN_Pos    (4U)
#define ADC_CHxDRS_AVGEN_Msk    (0x1U << ADC_CHxDRS_AVGEN_Pos)                      /*!< 0x00000010 */
#define ADC_CHxDRS_AVGEN        ADC_CHxDRS_AVGEN_Msk                                /*!<chx_average_en */

#define ADC_CHxDRS_AVGNUM_Pos   (6U)
#define ADC_CHxDRS_AVGNUM_Msk   (0x3U << ADC_CHxDRS_AVGNUM_Pos)                     /*!< 0x000000C0 */
#define ADC_CHxDRS_AVGNUM       ADC_CHxDRS_AVGNUM_Msk                               /*!<chx_average_num[1:0] */

#define ADC_CHxDRS_CMPEN_Pos    (8U)
#define ADC_CHxDRS_CMPEN_Msk    (0x1U << ADC_CHxDRS_CMPEN_Pos)                      /*!< 0x00000100 */
#define ADC_CHxDRS_CMPEN        ADC_CHxDRS_CMPEN_Msk                                /*!<chx_compare_en */

#define ADC_CHxDRS_CMPM_Pos (9U)
#define ADC_CHxDRS_CMPM_Msk (0x3U << ADC_CHxDRS_CMPM_Pos)                           /*!< 0x00000600 */
#define ADC_CHxDRS_CMPM     ADC_CHxDRS_CMPM_Msk                                     /*!<chx_compare_mode[1:0] */

/*******************   Bit definition for ADC_CHxRESULT register   ********************/
#define ADC_CHxRESULT_RESULT_Pos    (0U)
#define ADC_CHxRESULT_RESULT_Msk    (0xFFFFU << ADC_CHxRESULT_RESULT_Pos)           /*!< 0x0000FFFF */
#define ADC_CHxRESULT_RESULT        ADC_CHxRESULT_RESULT_Msk                        /*!<chx_result[15:0] */

/*******************   Bit definition for ADC_CHxOFFAVAL register   ********************/
#define ADC_CHxOFFSET_OFFAVAL_Pos   (0U)
#define ADC_CHxOFFSET_OFFAVAL_Msk   (0xFFFFU << ADC_CHxOFFSET_OFFAVAL_Pos)          /*!< 0x0000FFFF */
#define ADC_CHxOFFSET_OFFAVAL       ADC_CHxOFFSET_OFFAVAL_Msk                       /*!<chx_offset_val[15:0] */

/*******************   Bit definition for ADC_CHxCV register   ********************/
#define ADC_CHxCMPVAL_CV1_Pos   (0U)
#define ADC_CHxCMPVAL_CV1_Msk   (0xFFFFU << ADC_CHxCMPVAL_CV1_Pos)                  /*!< 0x0000FFFF */
#define ADC_CHxCMPVAL_CV1       ADC_CHxCMPVAL_CV1_Msk                               /*!<chx_compare_val1[15:0] */

#define ADC_CHxCMPVAL_CV2_Pos   (16U)
#define ADC_CHxCMPVAL_CV2_Msk   (0xFFFFU << ADC_CHxCMPVAL_CV2_Pos)                  /*!< 0xFFFF0000 */
#define ADC_CHxCMPVAL_CV2       ADC_CHxCMPVAL_CV2_Msk                               /*!<chx_compare_val2[15:0] */


/*******************   Bit definition for ADC_INTEN register   ********************/
#define ADC_INTEN_CH0CONVCO_Pos (0U)
#define ADC_INTEN_CH0CONVCO_Msk (0x1U << ADC_INTEN_CH0CONVCO_Pos)                   /*!< 0x00000001 */
#define ADC_INTEN_CH0CONVCO     ADC_INTEN_CH0CONVCO_Msk                             /*!<ch0_convco_int_en */

#define ADC_INTEN_CH1CONVCO_Pos (1U)
#define ADC_INTEN_CH1CONVCO_Msk (0x1U << ADC_INTEN_CH1CONVCO_Pos)                   /*!< 0x00000002 */
#define ADC_INTEN_CH1CONVCO     ADC_INTEN_CH1CONVCO_Msk                             /*!<ch1_convco_int_en */

#define ADC_INTEN_CH2CONVCO_Pos (2U)
#define ADC_INTEN_CH2CONVCO_Msk (0x1U << ADC_INTEN_CH2CONVCO_Pos)                   /*!< 0x00000004 */
#define ADC_INTEN_CH2CONVCO     ADC_INTEN_CH2CONVCO_Msk                             /*!<ch2_convco_int_en */

#define ADC_INTEN_CH3CONVCO_Pos (3U)
#define ADC_INTEN_CH3CONVCO_Msk (0x1U << ADC_INTEN_CH3CONVCO_Pos)                   /*!< 0x00000008 */
#define ADC_INTEN_CH3CONVCO     ADC_INTEN_CH3CONVCO_Msk                             /*!<ch3_convco_int_en */

#define ADC_INTEN_CH0CODMAS_Pos (4U)
#define ADC_INTEN_CH0CODMAS_Msk (0x1U << ADC_INTEN_CH0CODMAS_Pos)                   /*!< 0x00000010 */
#define ADC_INTEN_CH0CODMAS     ADC_INTEN_CH0CODMAS_Msk                             /*!<ch0_convco_dma_sel */

#define ADC_INTEN_CH1CODMAS_Pos (5U)
#define ADC_INTEN_CH1CODMAS_Msk (0x1U << ADC_INTEN_CH1CODMAS_Pos)                   /*!< 0x00000020 */
#define ADC_INTEN_CH1CODMAS     ADC_INTEN_CH1CODMAS_Msk                             /*!<ch1_convco_dma_sel */

#define ADC_INTEN_CH2CODMAS_Pos (6U)
#define ADC_INTEN_CH2CODMAS_Msk (0x1U << ADC_INTEN_CH2CODMAS_Pos)                   /*!< 0x00000040 */
#define ADC_INTEN_CH2CODMAS     ADC_INTEN_CH2CODMAS_Msk                             /*!<ch2_convco_dma_sel */

#define ADC_INTEN_CH3CODMAS_Pos (7U)
#define ADC_INTEN_CH3CODMAS_Msk (0x1U << ADC_INTEN_CH3CODMAS_Pos)                   /*!< 0x00000080 */
#define ADC_INTEN_CH3CODMAS     ADC_INTEN_CH3CODMAS_Msk                             /*!<ch3_convco_dma_sel */

#define ADC_INTEN_CH0OVRUN_Pos  (8U)
#define ADC_INTEN_CH0OVRUN_Msk  (0x1U << ADC_INTEN_CH0OVRUN_Pos)                    /*!< 0x00000100 */
#define ADC_INTEN_CH0OVRUN      ADC_INTEN_CH0OVRUN_Msk                              /*!<ch0_overrun_int_en */

#define ADC_INTEN_CH1OVRUN_Pos  (9U)
#define ADC_INTEN_CH1OVRUN_Msk  (0x1U << ADC_INTEN_CH1OVRUN_Pos)                    /*!< 0x00000200 */
#define ADC_INTEN_CH1OVRUN      ADC_INTEN_CH1OVRUN_Msk                              /*!<ch1_overrun_int_en */

#define ADC_INTEN_CH2OVRUN_Pos  (10U)
#define ADC_INTEN_CH2OVRUN_Msk  (0x1U << ADC_INTEN_CH2OVRUN_Pos)                    /*!< 0x00000400 */
#define ADC_INTEN_CH2OVRUN      ADC_INTEN_CH2OVRUN_Msk                              /*!<ch2_overrun_int_en */

#define ADC_INTEN_CH3OVRUN_Pos  (11U)
#define ADC_INTEN_CH3OVRUN_Msk  (0x1U << ADC_INTEN_CH3OVRUN_Pos)                    /*!< 0x00000800 */
#define ADC_INTEN_CH3OVRUN      ADC_INTEN_CH3OVRUN_Msk                              /*!<ch3_overrun_int_en */

#define ADC_INTEN_READY_Pos (12U)
#define ADC_INTEN_READY_Msk (0x1U << ADC_INTEN_READY_Pos)                           /*!< 0x00001000 */
#define ADC_INTEN_READY     ADC_INTEN_READY_Msk                                     /*!<adc_ready_int_en */

#define ADC_INTEN_DONEERR_Pos   (13U)
#define ADC_INTEN_DONEERR_Msk   (0x1U << ADC_INTEN_DONEERR_Pos)                     /*!< 0x00002000 */
#define ADC_INTEN_DONEERR       ADC_INTEN_DONEERR_Msk                               /*!<adc_done_err_int_en */
/*******************   Bit definition for ADC_STATUS register   ********************/
#define ADC_STATUS_READY_Pos (8U)
#define ADC_STATUS_READY_Msk (0x1U << ADC_STATUS_READY_Pos)                   /*!< 0x00000100 */
#define ADC_STATUS_READY         ADC_STATUS_READY_Msk                             /*!< ADC READY*/

/*******************   Bit definition for ADC_STATUS_RECORD register   ********************/
#define ADC_STATUSRCD_CH0CONVCO_Pos (0U)
#define ADC_STATUSRCD_CH0CONVCO_Msk (0x1U << ADC_STATUSRCD_CH0CONVCO_Pos)           /*!< 0x00000001 */
#define ADC_STATUSRCD_CH0CONVCO     ADC_STATUSRCD_CH0CONVCO_Msk                     /*!<ch0_convco_record */

#define ADC_STATUSRCD_CH1CONVCO_Pos (1U)
#define ADC_STATUSRCD_CH1CONVCO_Msk (0x1U << ADC_STATUSRCD_CH1CONVCO_Pos)           /*!< 0x00000002 */
#define ADC_STATUSRCD_CH1CONVCO     ADC_STATUSRCD_CH1CONVCO_Msk                     /*!<ch1_convco_record */

#define ADC_STATUSRCD_CH2CONVCO_Pos (2U)
#define ADC_STATUSRCD_CH2CONVCO_Msk (0x1U << ADC_STATUSRCD_CH2CONVCO_Pos)           /*!< 0x00000004 */
#define ADC_STATUSRCD_CH2CONVCO     ADC_STATUSRCD_CH2CONVCO_Msk                     /*!<ch2_convco_record */

#define ADC_STATUSRCD_CH3CONVCO_Pos (3U)
#define ADC_STATUSRCD_CH3CONVCO_Msk (0x1U << ADC_STATUSRCD_CH3CONVCO_Pos)           /*!< 0x00000008 */
#define ADC_STATUSRCD_CH3CONVCO     ADC_STATUSRCD_CH3CONVCO_Msk                     /*!<ch3_convco_record */

#define ADC_STATUSRCD_CH0OVRUN_Pos  (8U)
#define ADC_STATUSRCD_CH0OVRUN_Msk  (0x1U << ADC_STATUSRCD_CH0OVRUN_Pos)            /*!< 0x00000100 */
#define ADC_STATUSRCD_CH0OVRUN      ADC_STATUSRCD_CH0OVRUN_Msk                      /*!<ch1_overrun_record */

#define ADC_STATUSRCD_CH1OVRUN_Pos  (9U)
#define ADC_STATUSRCD_CH1OVRUN_Msk  (0x1U << ADC_STATUSRCD_CH1OVRUN_Pos)            /*!< 0x00000200 */
#define ADC_STATUSRCD_CH1OVRUN      ADC_STATUSRCD_CH1OVRUN_Msk                      /*!<ch2_overrun_record */

#define ADC_STATUSRCD_CH2OVRUN_Pos  (10U)
#define ADC_STATUSRCD_CH2OVRUN_Msk  (0x1U << ADC_STATUSRCD_CH2OVRUN_Pos)            /*!< 0x00000400 */
#define ADC_STATUSRCD_CH2OVRUN      ADC_STATUSRCD_CH2OVRUN_Msk                      /*!<ch3_overrun_record */

#define ADC_STATUSRCD_CH3OVRUN_Pos  (11U)
#define ADC_STATUSRCD_CH3OVRUN_Msk  (0x1U << ADC_STATUSRCD_CH3OVRUN_Pos)            /*!< 0x00000800 */
#define ADC_STATUSRCD_CH3OVRUN      ADC_STATUSRCD_CH3OVRUN_Msk                      /*!<ch4_overrun_record */

#define ADC_STATUSRCD_READY_Pos (12U)
#define ADC_STATUSRCD_READY_Msk (0x1U << ADC_STATUSRCD_READY_Pos)                   /*!< 0x00001000 */
#define ADC_STATUSRCD_READY     ADC_STATUSRCD_READY_Msk                             /*!<adc_ready_record */

#define ADC_STATUSRCD_DONEERR_Pos   (13U)
#define ADC_STATUSRCD_DONEERR_Msk   (0x1U << ADC_STATUSRCD_DONEERR_Pos)             /*!< 0x00002000 */
#define ADC_STATUSRCD_DONEERR       ADC_STATUSRCD_DONEERR_Msk                       /*!<adc_done_err_record */

/*******************   Bit definition for ADC_SRCL register   ********************/
#define ADC_SRCL_CH0CONVCO_Pos  (0U)
#define ADC_SRCL_CH0CONVCO_Msk  (0x1U << ADC_SRCL_CH0CONVCO_Pos)                    /*!< 0x00000001 */
#define ADC_SRCL_CH0CONVCO      ADC_SRCL_CH0CONVCO_Msk                              /*!<ch0_convco_srcl */

#define ADC_SRCL_CH1CONVCO_Pos  (1U)
#define ADC_SRCL_CH1CONVCO_Msk  (0x1U << ADC_SRCL_CH1CONVCO_Pos)                    /*!< 0x00000002 */
#define ADC_SRCL_CH1CONVCO      ADC_SRCL_CH1CONVCO_Msk                              /*!<ch1_convco_srcl */

#define ADC_SRCL_CH2CONVCO_Pos  (2U)
#define ADC_SRCL_CH2CONVCO_Msk  (0x1U << ADC_SRCL_CH2CONVCO_Pos)                    /*!< 0x00000004 */
#define ADC_SRCL_CH2CONVCO      ADC_SRCL_CH2CONVCO_Msk                              /*!<ch2_convco_srcl */

#define ADC_SRCL_CH3CONVCO_Pos  (3U)
#define ADC_SRCL_CH3CONVCO_Msk  (0x1U << ADC_SRCL_CH3CONVCO_Pos)                    /*!< 0x00000008 */
#define ADC_SRCL_CH3CONVCO      ADC_SRCL_CH3CONVCO_Msk                              /*!<ch3_convco_srcl */

#define ADC_SRCL_CH0OVRUN_Pos   (8U)
#define ADC_SRCL_CH0OVRUN_Msk   (0x1U << ADC_SRCL_CH0OVRUN_Pos)                     /*!< 0x00000100 */
#define ADC_SRCL_CH0OVRUN       ADC_SRCL_CH0OVRUN_Msk                               /*!<ch0_overrun_srcl */

#define ADC_SRCL_CH1OVRUN_Pos   (9U)
#define ADC_SRCL_CH1OVRUN_Msk   (0x1U << ADC_SRCL_CH1OVRUN_Pos)                     /*!< 0x00000200 */
#define ADC_SRCL_CH1OVRUN       ADC_SRCL_CH1OVRUN_Msk                               /*!<ch1_overrun_srcl */

#define ADC_SRCL_CH2OVRUN_Pos   (10U)
#define ADC_SRCL_CH2OVRUN_Msk   (0x1U << ADC_SRCL_CH2OVRUN_Pos)                     /*!< 0x00000400 */
#define ADC_SRCL_CH2OVRUN       ADC_SRCL_CH2OVRUN_Msk                               /*!<ch2_overrun_srcl */

#define ADC_SRCL_CH3OVRUN_Pos   (11U)
#define ADC_SRCL_CH3OVRUN_Msk   (0x1U << ADC_SRCL_CH3OVRUN_Pos)                     /*!< 0x00000800 */
#define ADC_SRCL_CH3OVRUN       ADC_SRCL_CH3OVRUN_Msk                               /*!<ch3_overrun_srcl */

#define ADC_SRCL_READY_Pos  (12U)
#define ADC_SRCL_READY_Msk  (0x1U << ADC_SRCL_READY_Pos)                            /*!< 0x00001000 */
#define ADC_SRCL_READY      ADC_SRCL_READY_Msk                                      /*!<adc_ready_srcl */

#define ADC_SRCL_DONEERR_Pos    (13U)
#define ADC_SRCL_DONEERR_Msk    (0x1U << ADC_SRCL_DONEERR_Pos)                      /*!< 0x00002000 */
#define ADC_SRCL_DONEERR        ADC_SRCL_DONEERR_Msk                                /*!<adc_done_err_srcl */

/*******************   Bit definition for ADC_VREF_CTRL register   ********************/
#define ADC_VREF_CTRL_EN_Pos    (0U)
#define ADC_VREF_CTRL_EN_Msk    (0x1U << ADC_VREF_CTRL_EN_Pos)
#define ADC_VREF_CTRL_EN        ADC_VREF_CTRL_EN_Msk

#define ADC_VREF_CTRL_TODACEN_Pos   (1U)
#define ADC_VREF_CTRL_TODACEN_Msk   (0x1U << ADC_VREF_CTRL_TODACEN_Pos)
#define ADC_VREF_CTRL_TODACEN       ADC_VREF_CTRL_TODACEN_Msk

#define ADC_VREF_CTRL_SCMODE_Pos    (2U)
#define ADC_VREF_CTRL_SCMODE_Msk    (0x3U << ADC_VREF_CTRL_SCMODE_Pos)
#define ADC_VREF_CTRL_SCMODE        ADC_VREF_CTRL_SCMODE_Msk

/*******************   Bit definition for ADC_VREF_TRIM register   ********************/
#define ADC_VREF_TRIM_Pos   (0U)
#define ADC_VREF_TRIM_Msk   (0xFFU << ADC_VREF_TRIM_Pos)
#define ADC_VREF_TRIM_EN    ADC_VREF_TRIM_Msk

/*******************   Bit definition for ADC_VREF_BGFLAG register   ********************/
#define ADC_VREF_BGFLAG_Pos (1U)
#define ADC_VREF_BGFLAG_Msk (0x1U << ADC_VREF_BGFLAG_Pos)
#define ADC_VREF_BGFLAG_EN  ADC_VREF_BGFLAG_Msk
//</h>
//<h>PMU
/******************************************************************************/
/*                                                                            */
/*                                    PMU                                     */
/*                                                                            */
/******************************************************************************/
/*******************  Bit definition for PMU_POWER_AT_STOP register  ********************/
#define PMU_PSTOP_LDO12ENLP_Pos (5U)
#define PMU_PSTOP_LDO12ENLP_Msk (0x1U << PMU_PSTOP_LDO12ENLP_Pos)                       /*!< 0x00000020 */
#define PMU_PSTOP_LDO12ENLP     PMU_PSTOP_LDO12ENLP_Msk                                 /*!<csr_stop_ldo12_en_lpmode */

#define PMU_PSTOP_LDO16ENLP_Pos (7U)
#define PMU_PSTOP_LDO16ENLP_Msk (0x1U << PMU_PSTOP_LDO16ENLP_Pos)                       /*!< 0x00000080 */
#define PMU_PSTOP_LDO16ENLP     PMU_PSTOP_LDO16ENLP_Msk                                 /*!<csr_stop_ldo16_en_lpmode */

#define PMU_PSTOP_LDO12CFG_Pos  (8U)
#define PMU_PSTOP_LDO12CFG_Msk  (0x3U << PMU_PSTOP_LDO12CFG_Pos)                        /*!< 0x00000300 */
#define PMU_PSTOP_LDO12CFG      PMU_PSTOP_LDO12CFG_Msk                                  /*!<csr_stop_ldo12_cfg[1:0] */

/*******************  Bit definition for PMU_POWER_AT_STANDBY register  ********************/
#define PMU_PSTBY_LDO16EN_Pos   (6U)
#define PMU_PSTBY_LDO16EN_Msk   (0x1U << PMU_PSTBY_LDO16EN_Pos)                         /*!< 0x00000040 */
#define PMU_PSTBY_LDO16EN       PMU_PSTBY_LDO16EN_Msk                                   /*!<csr_stby_ldo16_en */

/*******************  Bit definition for PMU_HOLD_EN_AT_LPMODE register  ********************/
#define PMU_HOLDENLP_STBYPAD_Pos    (1U)
#define PMU_HOLDENLP_STBYPAD_Msk    (0x1U << PMU_HOLDENLP_STBYPAD_Pos)                  /*!< 0x00000002 */
#define PMU_HOLDENLP_STBYPAD        PMU_HOLDENLP_STBYPAD_Msk                            /*!<csr_stby_pad_hold_en */

#define PMU_HOLDENLP_STBYANA_Pos    (2U)
#define PMU_HOLDENLP_STBYANA_Msk    (0x1U << PMU_HOLDENLP_STBYANA_Pos)                  /*!< 0x00000004 */
#define PMU_HOLDENLP_STBYANA        PMU_HOLDENLP_STBYANA_Msk                            /*!<csr_stby_ana_hold_en */

#define PMU_HOLDENLP_STAHAR_Pos (3U)
#define PMU_HOLDENLP_STAHAR_Msk (0x1U << PMU_HOLDENLP_STAHAR_Pos)                       /*!< 0x00000008 */
#define PMU_HOLDENLP_STAHAR     PMU_HOLDENLP_STAHAR_Msk                                 /*!<csr_status_hold_autorelease */

/*******************  Bit definition for PMU_POWER_SWITCH_RDY register  ********************/
#define PMU_PSWRDY_CNT4MDIG_Pos (0U)
#define PMU_PSWRDY_CNT4MDIG_Msk (0xFU << PMU_PSWRDY_CNT4MDIG_Pos)                       /*!< 0x0000000F */
#define PMU_PSWRDY_CNT4MDIG     PMU_PSWRDY_CNT4MDIG_Msk                                 /*!<csr_power_switch_cnt4m_dig[3:0] */

#define PMU_PSWRDY_CNT4MRAM_Pos (8U)
#define PMU_PSWRDY_CNT4MRAM_Msk (0xFU << PMU_PSWRDY_CNT4MRAM_Pos)                       /*!< 0x00000F00 */
#define PMU_PSWRDY_CNT4MRAM     PMU_PSWRDY_CNT4MRAM_Msk                                 /*!<csr_power_switch_cnt4m_ram[3:0] */

#define PMU_PSWRDY_CNT4MFLASH_Pos   (16U)
#define PMU_PSWRDY_CNT4MFLASH_Msk   (0xFU << PMU_PSWRDY_CNT4MFLASH_Pos)                 /*!< 0x000F0000 */
#define PMU_PSWRDY_CNT4MFLASH       PMU_PSWRDY_CNT4MFLASH_Msk                           /*!<csr_power_switch_cnt4m_flash[3:0] */

#define PMU_PSWRDY_CNT4MUSB_Pos (24U)
#define PMU_PSWRDY_CNT4MUSB_Msk (0xFU << PMU_PSWRDY_CNT4MUSB_Pos)                       /*!< 0x0F000000 */
#define PMU_PSWRDY_CNT4MUSB     PMU_PSWRDY_CNT4MUSB_Msk                                 /*!<csr_power_switch_cnt4m_usb[3:0] */

/*******************  Bit definition for PMU_POWER_SWITCH_EN register  ********************/
#define PMU_PSWEN_ENDIG_Pos (0U)
#define PMU_PSWEN_ENDIG_Msk (0x1U << PMU_PSWEN_ENDIG_Pos)                               /*!< 0x00000001 */
#define PMU_PSWEN_ENDIG     PMU_PSWEN_ENDIG_Msk                                         /*!<csr_power_switch_en_dig*/

#define PMU_PSWEN_ENFLASH_Pos   (1U)
#define PMU_PSWEN_ENFLASH_Msk   (0x1U << PMU_PSWEN_ENFLASH_Pos)                         /*!< 0x00000002 */
#define PMU_PSWEN_ENFLASH       PMU_PSWEN_ENFLASH_Msk                                   /*!<csr_power_switch_en_flash*/

#define PMU_PSWEN_ENRAMB_Pos    (2U)
#define PMU_PSWEN_ENRAMB_Msk    (0x1U << PMU_PSWEN_ENRAMB_Pos)                          /*!< 0x00000004 */
#define PMU_PSWEN_ENRAMB        PMU_PSWEN_ENRAMB_Msk                                    /*!<csr_power_switch_en_ram_b*/

#define PMU_PSWEN_ENRAMA_Pos    (3U)
#define PMU_PSWEN_ENRAMA_Msk    (0x1U << PMU_PSWEN_ENRAMA_Pos)                          /*!< 0x00000008 */
#define PMU_PSWEN_ENRAMA        PMU_PSWEN_ENRAMA_Msk                                    /*!<csr_power_switch_en_ram_a*/

#define PMU_PSWEN_ENUSB_Pos (4U)
#define PMU_PSWEN_ENUSB_Msk (0x1U << PMU_PSWEN_ENUSB_Pos)                               /*!< 0x00000010 */
#define PMU_PSWEN_ENUSB     PMU_PSWEN_ENUSB_Msk                                         /*!<csr_power_switch_en_usb*/

#define PMU_PSWEN_ENEFUSE_Pos   (5U)
#define PMU_PSWEN_ENEFUSE_Msk   (0x1U << PMU_PSWEN_ENEFUSE_Pos)                         /*!< 0x00000020 */
#define PMU_PSWEN_ENEFUSE       PMU_PSWEN_ENEFUSE_Msk                                   /*!<csr_power_switch_en_efuse*/

/*******************  Bit definition for PMU_RTC_ISO register  ********************/
#define PMU_RTCISO_OPEN_Pos (0U)
#define PMU_RTCISO_OPEN_Msk (0x1U << PMU_RTCISO_OPEN_Pos)                               /*!< 0x00000001 */
#define PMU_RTCISO_OPEN     PMU_RTCISO_OPEN_Msk                                         /*!<csr_rtc_iso_open */

#define PMU_RTCISO_VDINEN_Pos   (4U)
#define PMU_RTCISO_VDINEN_Msk   (0xFU << PMU_RTCISO_VDINEN_Pos)                         /*!< 0x000000F0 */
#define PMU_RTCISO_VDINEN       PMU_RTCISO_VDINEN_Msk                                   /*!<vbus_det_int_en[3:0] */
/*******************  Bit definition for PMU_CHARGER_CFG register  ****************/
#define PMU_CHGERCFG_HDEN_Pos   (0U)
#define PMU_CHGERCFG_HDEN_Msk   (0x1U << PMU_CHGERCFG_HDEN_Pos)                         /*!< 0x00000001 */
#define PMU_CHGERCFG_HDEN       PMU_CHGERCFG_HDEN_Msk                                   /*!<csr_charger_hd_en */

#define PMU_CHGERCFG_LSEN_Pos   (1U)
#define PMU_CHGERCFG_LSEN_Msk   (0x1U << PMU_CHGERCFG_LSEN_Pos)                         /*!< 0x00000002 */
#define PMU_CHGERCFG_LSEN       PMU_CHGERCFG_LSEN_Msk                                   /*!<csr_charger_ls_en */

#define PMU_CHGERCFG_FILTCFG_Pos    (4U)
#define PMU_CHGERCFG_FILTCFG_Msk    (0x3U << PMU_CHGERCFG_FILTCFG_Pos)                  /*!< 0x00000030 */
#define PMU_CHGERCFG_FILTCFG        PMU_CHGERCFG_FILTCFG_Msk                            /*!<csr_charger_filter_cfg[1:0] */
/*******************  Bit definition for AON_CHARGER_STATUS register  *************/
#define PMU_CHGERSTS_CSR90PER_Pos   (16U)
#define PMU_CHGERSTS_CSR90PER_Msk   (0x1U << PMU_CHGERSTS_CSR90PER_Pos)                 /*!< 0x00010000 */
#define PMU_CHGERSTS_CSR90PER       PMU_CHGERSTS_CSR90PER_Msk                           /*!<csr_chg_90per */

#define PMU_CHGERSTS_CSRIECOFLG_Pos (18U)
#define PMU_CHGERSTS_CSRIECOFLG_Msk (0x1U << PMU_CHGERSTS_CSRIECOFLG_Pos)               /*!< 0x00040000 */
#define PMU_CHGERSTS_CSRIECOFLG     PMU_CHGERSTS_CSRIECOFLG_Msk                         /*!<csr_chg_ieco_flg */

/*******************  Bit definition for PMU_FILTS register  ********************/
#define PMU_FILTS_FILT0SEL_Pos  (0U)
#define PMU_FILTS_FILT0SEL_Msk  (0x1FU << PMU_FILTS_FILT0SEL_Pos)                       /*!< 0x0000001F */
#define PMU_FILTS_FILT0SEL      PMU_FILTS_FILT0SEL_Msk                                  /*!<llwu_filter_0_sel[4:0] */

#define PMU_FILTS_FILT1SEL_Pos  (8U)
#define PMU_FILTS_FILT1SEL_Msk  (0x1FU << PMU_FILTS_FILT1SEL_Pos)                       /*!< 0x00001F00 */
#define PMU_FILTS_FILT1SEL      PMU_FILTS_FILT1SEL_Msk                                  /*!<llwu_filter_1_sel[4:0] */

#define PMU_FILTS_FILT2SEL_Pos  (16U)
#define PMU_FILTS_FILT2SEL_Msk  (0x1FU << PMU_FILTS_FILT2SEL_Pos)                       /*!< 0x001F0000 */
#define PMU_FILTS_FILT2SEL      PMU_FILTS_FILT2SEL_Msk                                  /*!<llwu_filter_2_sel[4:0] */

#define PMU_FILTS_FILT3SEL_Pos  (24U)
#define PMU_FILTS_FILT3SEL_Msk  (0x1FU << PMU_FILTS_FILT3SEL_Pos)                       /*!< 0x1F000000 */
#define PMU_FILTS_FILT3SEL      PMU_FILTS_FILT3SEL_Msk                                  /*!<llwu_filter_3_sel[4:0] */

/*******************  Bit definition for PMU_PADEN register  ********************/
#define PMU_PADEN_LLWUPE_Pos    (0U)
#define PMU_PADEN_LLWUPE_Msk    (0xFFFFFFFFU << PMU_PADEN_LLWUPE_Pos)                   /*!< 0x0000001F */
#define PMU_PADEN_LLWUPE        PMU_PADEN_LLWUPE_Msk                                    /*!<llwu_pad_en[31:0] */

/*******************  Bit definition for PMU_INTFE register  ********************/
#define PMU_INTFE_LLWUIFE_Pos   (0U)
#define PMU_INTFE_LLWUIFE_Msk   (0xFFFFU << PMU_INTFE_LLWUIFE_Pos)                      /*!< 0x0000FFFF */
#define PMU_INTFE_LLWUIFE       PMU_INTFE_LLWUIFE_Msk                                   /*!<llwu_intf_en[15:0] */

/*******************  Bit definition for PMU_FILTE register  ********************/
#define PMU_FILTE_LLWUFE_Pos    (0U)
#define PMU_FILTE_LLWUFE_Msk    (0xFU << PMU_FILTE_LLWUFE_Pos)                          /*!< 0x0000000F */
#define PMU_FILTE_LLWUFE        PMU_FILTE_LLWUFE_Msk                                    /*!<llwu_filter_en[3:0] */

/*******************  Bit definition for PMU_PCFGA register  ********************/
#define PMU_PCFGA_P00CFG_Pos    (0U)
#define PMU_PCFGA_P00CFG_Msk    (0x3U << PMU_PCFGA_P00CFG_Pos)                          /*!< 0x00000003 */
#define PMU_PCFGA_P00CFG        PMU_PCFGA_P00CFG_Msk                                    /*!<llwu_pad00_cfg[1:0] */

#define PMU_PCFGA_P01CFG_Pos    (2U)
#define PMU_PCFGA_P01CFG_Msk    (0x3U << PMU_PCFGA_P01CFG_Pos)                          /*!< 0x0000000C */
#define PMU_PCFGA_P01CFG        PMU_PCFGA_P01CFG_Msk                                    /*!<llwu_pad01_cfg[1:0] */

#define PMU_PCFGA_P02CFG_Pos    (4U)
#define PMU_PCFGA_P02CFG_Msk    (0x3U << PMU_PCFGA_P02CFG_Pos)                          /*!< 0x00000030 */
#define PMU_PCFGA_P02CFG        PMU_PCFGA_P02CFG_Msk                                    /*!<llwu_pad02_cfg[1:0] */

#define PMU_PCFGA_P03CFG_Pos    (6U)
#define PMU_PCFGA_P03CFG_Msk    (0x3U << PMU_PCFGA_P03CFG_Pos)                          /*!< 0x000000C0 */
#define PMU_PCFGA_P03CFG        PMU_PCFGA_P03CFG_Msk                                    /*!<llwu_pad03_cfg[1:0] */

#define PMU_PCFGA_P04CFG_Pos    (8U)
#define PMU_PCFGA_P04CFG_Msk    (0x3U << PMU_PCFGA_P04CFG_Pos)                          /*!< 0x00000300 */
#define PMU_PCFGA_P04CFG        PMU_PCFGA_P04CFG_Msk                                    /*!<llwu_pad04_cfg[1:0] */

#define PMU_PCFGA_P05CFG_Pos    (10U)
#define PMU_PCFGA_P05CFG_Msk    (0x3U << PMU_PCFGA_P05CFG_Pos)                          /*!< 0x00000C00 */
#define PMU_PCFGA_P05CFG        PMU_PCFGA_P05CFG_Msk                                    /*!<llwu_pad05_cfg[1:0] */

#define PMU_PCFGA_P06CFG_Pos    (12U)
#define PMU_PCFGA_P06CFG_Msk    (0x3U << PMU_PCFGA_P06CFG_Pos)                          /*!< 0x00003000 */
#define PMU_PCFGA_P06CFG        PMU_PCFGA_P06CFG_Msk                                    /*!<llwu_pad06_cfg[1:0] */

#define PMU_PCFGA_P07CFG_Pos    (14U)
#define PMU_PCFGA_P07CFG_Msk    (0x3U << PMU_PCFGA_P07CFG_Pos)                          /*!< 0x0000C000 */
#define PMU_PCFGA_P07CFG        PMU_PCFGA_P07CFG_Msk                                    /*!<llwu_pad07_cfg[1:0] */

#define PMU_PCFGA_P08CFG_Pos    (16U)
#define PMU_PCFGA_P08CFG_Msk    (0x3U << PMU_PCFGA_P08CFG_Pos)                          /*!< 0x00030000 */
#define PMU_PCFGA_P08CFG        PMU_PCFGA_P08CFG_Msk                                    /*!<llwu_pad08_cfg[1:0] */

#define PMU_PCFGA_P09CFG_Pos    (18U)
#define PMU_PCFGA_P09CFG_Msk    (0x3U << PMU_PCFGA_P09CFG_Pos)                          /*!< 0x000C0000 */
#define PMU_PCFGA_P09CFG        PMU_PCFGA_P09CFG_Msk                                    /*!<llwu_pad09_cfg[1:0] */

#define PMU_PCFGA_P0ACFG_Pos    (20U)
#define PMU_PCFGA_P0ACFG_Msk    (0x3U << PMU_PCFGA_P0ACFG_Pos)                          /*!< 0x00300000 */
#define PMU_PCFGA_P0ACFG        PMU_PCFGA_P0ACFG_Msk                                    /*!<llwu_pad0a_cfg[1:0] */

#define PMU_PCFGA_P0BCFG_Pos    (22U)
#define PMU_PCFGA_P0BCFG_Msk    (0x3U << PMU_PCFGA_P0BCFG_Pos)                          /*!< 0x00C00000 */
#define PMU_PCFGA_P0BCFG        PMU_PCFGA_P0BCFG_Msk                                    /*!<llwu_pad0b_cfg[1:0] */

#define PMU_PCFGA_P0CCFG_Pos    (24U)
#define PMU_PCFGA_P0CCFG_Msk    (0x3U << PMU_PCFGA_P0CCFG_Pos)                          /*!< 0x03000000 */
#define PMU_PCFGA_P0CCFG        PMU_PCFGA_P0CCFG_Msk                                    /*!<llwu_pad0c_cfg[1:0] */

#define PMU_PCFGA_P0DCFG_Pos    (26U)
#define PMU_PCFGA_P0DCFG_Msk    (0x3U << PMU_PCFGA_P0DCFG_Pos)                          /*!< 0x0C000000 */
#define PMU_PCFGA_P0DCFG        PMU_PCFGA_P0DCFG_Msk                                    /*!<llwu_pad0d_cfg[1:0] */

#define PMU_PCFGA_P0ECFG_Pos    (28U)
#define PMU_PCFGA_P0ECFG_Msk    (0x3U << PMU_PCFGA_P0ECFG_Pos)                          /*!< 0x30000000 */
#define PMU_PCFGA_P0ECFG        PMU_PCFGA_P0ECFG_Msk                                    /*!<llwu_pad0e_cfg[1:0] */

#define PMU_PCFGA_P0FCFG_Pos    (30U)
#define PMU_PCFGA_P0FCFG_Msk    (0x3U << PMU_PCFGA_P0FCFG_Pos)                          /*!< 0xC0000000 */
#define PMU_PCFGA_P0FCFG        PMU_PCFGA_P0FCFG_Msk                                    /*!<llwu_pad0f_cfg[1:0] */

/*******************  Bit definition for PMU_INTFCFGA register  ********************/
#define PMU_INTFCFGA_INTF0_Pos  (0U)
#define PMU_INTFCFGA_INTF0_Msk  (0x3U << PMU_INTFCFGA_INTF0_Pos)                        /*!< 0x00000003 */
#define PMU_INTFCFGA_INTF0      PMU_INTFCFGA_INTF0_Msk                                  /*!<llwu_intf00_cfg[1:0] */

#define PMU_INTFCFGA_INTF1_Pos  (2U)
#define PMU_INTFCFGA_INTF1_Msk  (0x3U << PMU_INTFCFGA_INTF1_Pos)                        /*!< 0x0000000C */
#define PMU_INTFCFGA_INTF1      PMU_INTFCFGA_INTF1_Msk                                  /*!<llwu_intf01_cfg[1:0] */

#define PMU_INTFCFGA_INTF2_Pos  (4U)
#define PMU_INTFCFGA_INTF2_Msk  (0x3U << PMU_INTFCFGA_INTF2_Pos)                        /*!< 0x00000030 */
#define PMU_INTFCFGA_INTF2      PMU_INTFCFGA_INTF2_Msk                                  /*!<llwu_intf02_cfg[1:0] */

#define PMU_INTFCFGA_INTF3_Pos  (6U)
#define PMU_INTFCFGA_INTF3_Msk  (0x3U << PMU_INTFCFGA_INTF3_Pos)                        /*!< 0x000000C0 */
#define PMU_INTFCFGA_INTF3      PMU_INTFCFGA_INTF3_Msk                                  /*!<llwu_intf03_cfg[1:0] */

#define PMU_INTFCFGA_INTF4_Pos  (8U)
#define PMU_INTFCFGA_INTF4_Msk  (0x3U << PMU_INTFCFGA_INTF4_Pos)                        /*!< 0x00000300 */
#define PMU_INTFCFGA_INTF4      PMU_INTFCFGA_INTF4_Msk                                  /*!<llwu_intf04_cfg[1:0] */

#define PMU_INTFCFGA_INTF5_Pos  (10U)
#define PMU_INTFCFGA_INTF5_Msk  (0x3U << PMU_INTFCFGA_INTF5_Pos)                        /*!< 0x00000C00 */
#define PMU_INTFCFGA_INTF5      PMU_INTFCFGA_INTF5_Msk                                  /*!<llwu_intf05_cfg[1:0] */

#define PMU_INTFCFGA_INTF6_Pos  (12U)
#define PMU_INTFCFGA_INTF6_Msk  (0x3U << PMU_INTFCFGA_INTF6_Pos)                        /*!< 0x00003000 */
#define PMU_INTFCFGA_INTF6      PMU_INTFCFGA_INTF6_Msk                                  /*!<llwu_intf06_cfg[1:0] */

#define PMU_INTFCFGA_INTF7_Pos  (14U)
#define PMU_INTFCFGA_INTF7_Msk  (0x3U << PMU_INTFCFGA_INTF7_Pos)                        /*!< 0x0000C000 */
#define PMU_INTFCFGA_INTF7      PMU_INTFCFGA_INTF7_Msk                                  /*!<llwu_intf07_cfg[1:0] */

#define PMU_INTFCFGA_INTF8_Pos  (16U)
#define PMU_INTFCFGA_INTF8_Msk  (0x3U << PMU_INTFCFGA_INTF8_Pos)                        /*!< 0x00030000 */
#define PMU_INTFCFGA_INTF8      PMU_INTFCFGA_INTF8_Msk                                  /*!<llwu_intf08_cfg[1:0] */

#define PMU_INTFCFGA_INTF9_Pos  (18U)
#define PMU_INTFCFGA_INTF9_Msk  (0x3U << PMU_INTFCFGA_INTF9_Pos)                        /*!< 0x000C0000 */
#define PMU_INTFCFGA_INTF9      PMU_INTFCFGA_INTF9_Msk                                  /*!<llwu_intf09_cfg[1:0] */

#define PMU_INTFCFGA_INTFA_Pos  (20U)
#define PMU_INTFCFGA_INTFA_Msk  (0x3U << PMU_INTFCFGA_INTFA_Pos)                        /*!< 0x00300000 */
#define PMU_INTFCFGA_INTFA      PMU_INTFCFGA_INTFA_Msk                                  /*!<llwu_intf0a_cfg[1:0] */

#define PMU_INTFCFGA_INTFB_Pos  (22U)
#define PMU_INTFCFGA_INTFB_Msk  (0x3U << PMU_INTFCFGA_INTFB_Pos)                        /*!< 0x00C00000 */
#define PMU_INTFCFGA_INTFB      PMU_INTFCFGA_INTFB_Msk                                  /*!<llwu_intf0b_cfg[1:0] */

#define PMU_INTFCFGA_INTFC_Pos  (24U)
#define PMU_INTFCFGA_INTFC_Msk  (0x3U << PMU_INTFCFGA_INTFC_Pos)                        /*!< 0x03000000 */
#define PMU_INTFCFGA_INTFC      PMU_INTFCFGA_INTFC_Msk                                  /*!<llwu_intf0c_cfg[1:0] */

#define PMU_INTFCFGA_INTFD_Pos  (26U)
#define PMU_INTFCFGA_INTFD_Msk  (0x3U << PMU_INTFCFGA_INTFD_Pos)                        /*!< 0x0C000000 */
#define PMU_INTFCFGA_INTFD      PMU_INTFCFGA_INTFD_Msk                                  /*!<llwu_intf0d_cfg[1:0] */

#define PMU_INTFCFGA_INTFE_Pos  (28U)
#define PMU_INTFCFGA_INTFE_Msk  (0x3U << PMU_INTFCFGA_INTFE_Pos)                        /*!< 0x30000000 */
#define PMU_INTFCFGA_INTFE      PMU_INTFCFGA_INTFE_Msk                                  /*!<llwu_intf0e_cfg[1:0] */

#define PMU_INTFCFGA_INTFF_Pos  (30U)
#define PMU_INTFCFGA_INTFF_Msk  (0x3U << PMU_INTFCFGA_INTFF_Pos)                        /*!< 0xC0000000 */
#define PMU_INTFCFGA_INTFF      PMU_INTFCFGA_INTFF_Msk                                  /*!<llwu_intf0f_cfg[1:0] */

/*******************  Bit definition for PMU_FILTCFGA register  ********************/
#define PMU_FILTCFGA_FILT0_Pos  (0U)
#define PMU_FILTCFGA_FILT0_Msk  (0x3U << PMU_FILTCFGA_FILT0_Pos)                        /*!< 0x00000003 */
#define PMU_FILTCFGA_FILT0      PMU_FILTCFGA_FILT0_Msk                                  /*!<llwu_filter_0_cfg[1:0] */

#define PMU_FILTCFGA_FILT1_Pos  (2U)
#define PMU_FILTCFGA_FILT1_Msk  (0x3U << PMU_FILTCFGA_FILT1_Pos)                        /*!< 0x0000000C */
#define PMU_FILTCFGA_FILT1      PMU_FILTCFGA_FILT1_Msk                                  /*!<llwu_filter_1_cfg[1:0] */

#define PMU_FILTCFGA_FILT2_Pos  (4U)
#define PMU_FILTCFGA_FILT2_Msk  (0x3U << PMU_FILTCFGA_FILT2_Pos)                        /*!< 0x00000030 */
#define PMU_FILTCFGA_FILT2      PMU_FILTCFGA_FILT2_Msk                                  /*!<llwu_filter_2_cfg[1:0] */

#define PMU_FILTCFGA_FILT3_Pos  (6U)
#define PMU_FILTCFGA_FILT3_Msk  (0x3U << PMU_FILTCFGA_FILT3_Pos)                        /*!< 0x000000C0 */
#define PMU_FILTCFGA_FILT3      PMU_FILTCFGA_FILT3_Msk                                  /*!<llwu_filter_3_cfg[1:0] */

/*******************  Bit definition for PMU_LLWU_CFG register  ********************/
#define PMU_LLWU_CHRST_Pos  (0U)
#define PMU_LLWU_CHRST_Msk  (0x1U << PMU_LLWU_CHRST_Pos)                                /*!< 0x00000001 */
#define PMU_LLWU_CHRST      PMU_LLWU_CHRST_Msk                                          /*!<llwu_channel_rst_cfg */

#define PMU_LLWU_CHCLK_Pos  (1U)
#define PMU_LLWU_CHCLK_Msk  (0x1U << PMU_LLWU_CHCLK_Pos)                                /*!< 0x00000002 */
#define PMU_LLWU_CHCLK      PMU_LLWU_CHCLK_Msk                                          /*!<llwu_channel_clk_cfg */
/*******************  Bit definition for PMU_HOLDENLP register  ********************/
#define PMU_HOLDENLP_PADHOLDEN_Pos  (1U)
#define PMU_HOLDENLP_PADHOLDEN_Msk  (0x1U << PMU_HOLDENLP_PADHOLDEN_Pos)
#define PMU_HOLDENLP_PADHOLDEN      PMU_HOLDENLP_PADHOLDEN_Msk

#define PMU_HOLDENLP_ANAHOLDEN_Pos  (2U)
#define PMU_HOLDENLP_ANAHOLDEN_Msk  (0x1U << PMU_HOLDENLP_ANAHOLDEN_Pos)
#define PMU_HOLDENLP_ANAHOLDEN      PMU_HOLDENLP_ANAHOLDEN_Msk

#define PMU_HOLDENLP_AUTORES_Pos    (3U)
#define PMU_HOLDENLP_AUTORES_Msk    (0x1U << PMU_HOLDENLP_AUTORES_Pos)
#define PMU_HOLDENLP_AUTORES        PMU_HOLDENLP_AUTORES_Msk

/*******************  Bit definition for PMU_PHLDR register  ********************/
#define PMU_PHLDR_PHOLDRLS_Pos  (0U)
#define PMU_PHLDR_PHOLDRLS_Msk  (0x1U << PMU_PHLDR_PHOLDRLS_Pos)                    /*!< 0x00000001 */
#define PMU_PHLDR_PHOLDRLS      PMU_PHLDR_PHOLDRLS_Msk                              /*!<pm_pad_hold_csr_release */

/*******************  Bit definition for PMU_PHLDS register  ********************/
#define PMU_PHLDS_PADHOLD_Pos   (0U)
#define PMU_PHLDS_PADHOLD_Msk   (0x1U << PMU_PHLDS_PADHOLD_Pos)                     /*!< 0x00000001 */
#define PMU_PHLDS_PADHOLD       PMU_PHLDS_PADHOLD_Msk                               /*!<pm_pad_hold */

#define PMU_PHLDS_ANAHOLD_Pos   (1U)
#define PMU_PHLDS_ANAHOLD_Msk   (0x1U << PMU_PHLDS_ANAHOLD_Pos)                     /*!< 0x00000002 */
#define PMU_PHLDS_ANAHOLD       PMU_PHLDS_ANAHOLD_Msk                               /*!<pm_ana_hold */

/*******************  Bit definition for PMU_RCDPAD register  ********************/
#define PMU_RCDPAD_WAKEUP_Pos   (0U)
#define PMU_RCDPAD_WAKEUP_Msk   (0xFFFFFFFFU << PMU_RCDPAD_WAKEUP_Pos)              /*!< 0xFFFFFFFF */
#define PMU_RCDPAD_WAKEUP       PMU_RCDPAD_WAKEUP_Msk                               /*!<wakeup_record_pad[31:0] */

/*******************  Bit definition for PMU_RCDINTF register  ********************/
#define PMU_RCDINTF_WAKEUP_Pos  (0U)
#define PMU_RCDINTF_WAKEUP_Msk  (0xFFFFU << PMU_RCDINTF_WAKEUP_Pos)                 /*!< 0x0000FFFF */
#define PMU_RCDINTF_WAKEUP      PMU_RCDINTF_WAKEUP_Msk                              /*!<wakeup_record_intf[15:0] */

/*******************  Bit definition for PMU_RCDFILT register  ********************/
#define PMU_RCDFILT_WAKEUP_Pos  (0U)
#define PMU_RCDFILT_WAKEUP_Msk  (0xFU << PMU_RCDFILT_WAKEUP_Pos)                    /*!< 0x0000000F */
#define PMU_RCDFILT_WAKEUP      PMU_RCDFILT_WAKEUP_Msk                              /*!<wakeup_record_filter[3:0] */

/*******************  Bit definition for PMU_WKUP_RCDCLR register  ********************/
#define PMU_RCDCLR_WAKEUP_Pos   (0U)
#define PMU_RCDCLR_WAKEUP_Msk   (0x1U << PMU_RCDCLR_WAKEUP_Pos)                     /*!< 0x00000001 */
#define PMU_RCDCLR_WAKEUP       PMU_RCDCLR_WAKEUP_Msk                               /*!<wakeup_record_clr */

/*******************  Bit definition for PMU_BUCKUP_REG_0 register  ***************/
#define PMU_BKUP0_REG ( (uint32_t) 0xFFFFFFFF)                                      /*!<reg_backup_0[31:0] */

/*******************  Bit definition for PMU_BUCKUP_REG_1 register  ***************/
#define PMU_BKUP1_REG ( (uint32_t) 0xFFFFFFFF)                                      /*!<reg_backup_1[31:0] */

/*******************  Bit definition for PMU_BUCKUP_REG_2 register  ***************/
#define PMU_BKUP2_REG ( (uint32_t) 0xFFFFFFFF)                                      /*!<reg_backup_2[31:0] */

/*******************  Bit definition for PMU_BUCKUP_REG_3 register  ***************/
#define PMU_BKUP3_REG ( (uint32_t) 0xFFFFFFFF)                                      /*!<reg_backup_3[31:0] */

/*******************  Bit definition for PMU_BUCKUP_REG_4 register  ***************/
#define PMU_BKUP4_REG ( (uint32_t) 0xFFFFFFFF)                                      /*!<reg_backup_4[31:0] */

/*******************  Bit definition for PMU_BUCKUP_REG_5 register  ***************/
#define PMU_BKUP5_REG ( (uint32_t) 0xFFFFFFFF)                                      /*!<reg_backup_5[31:0] */

/*******************  Bit definition for PMU_BUCKUP_REG_6 register  ***************/
#define PMU_BKUP6_REG ( (uint32_t) 0xFFFFFFFF)                                      /*!<reg_backup_6[31:0] */

/*******************  Bit definition for PMU_BUCKUP_REG_7 register  ***************/
#define PMU_BKUP7_REG ( (uint32_t) 0xFFFFFFFF)                                      /*!<reg_backup_7[31:0] */

/*******************  Bit definition for PMU_LPM register  ********************/
#define PMU_LPM_SLEEP_Pos   (0U)
#define PMU_LPM_SLEEP_Msk   (0x1U << PMU_LPM_SLEEP_Pos)                             /*!< 0x00000001 */
#define PMU_LPM_SLEEP       PMU_LPM_SLEEP_Msk                                       /*!<cfg_pm_sleep */

#define PMU_LPM_DEEPSLEEP_Pos   (1U)
#define PMU_LPM_DEEPSLEEP_Msk   (0x1U << PMU_LPM_DEEPSLEEP_Pos)                     /*!< 0x00000002 */
#define PMU_LPM_DEEPSLEEP       PMU_LPM_DEEPSLEEP_Msk                               /*!<cfg_pm_deep_sleep */

/*******************  Bit definition for PMU_WKUPA register  ********************/
#define PMU_WKUPA_MODULE_Pos    (0U)
#define PMU_WKUPA_MODULE_Msk    (0xFFFFFFFFU << PMU_WKUPA_MODULE_Pos)               /*!< 0xFFFFFFFF */
#define PMU_WKUPA_MODULE        PMU_WKUPA_MODULE_Msk                                /*!<cfg_module_wakeup_a[31:0] */

/*******************  Bit definition for PMU_WKUPB register  ********************/
#define PMU_WKUPB_MODULE_Pos    (0U)
#define PMU_WKUPB_MODULE_Msk    (0xFFU << PMU_WKUPB_MODULE_Pos)                     /*!< 0x000000FF */
#define PMU_WKUPB_MODULE        PMU_WKUPB_MODULE_Msk                                /*!<cfg_module_wakeup_b[7:0] */

/*******************  Bit definition for PMU_LWKUPRCDC register  ********************/
#define PMU_LWKUPRCDC_LWRC_Pos  (0U)
#define PMU_LWKUPRCDC_LWRC_Msk  (0x1U << PMU_LWKUPRCDC_LWRC_Pos)                    /*!< 0x00000001 */
#define PMU_LWKUPRCDC_LWRC      PMU_LWKUPRCDC_LWRC_Msk                              /*!<last_wakeup_record_clr */

/*******************  Bit definition for PMU_LWKUPRCD0 register  ********************/
#define PMU_LWKUPRCD0_LLWU_Pos  (0U)
#define PMU_LWKUPRCD0_LLWU_Msk  (0x1U << PMU_LWKUPRCD0_LLWU_Pos)                    /*!< 0x00000001 */
#define PMU_LWKUPRCD0_LLWU      PMU_LWKUPRCD0_LLWU_Msk                              /*!<last_wakeup_record_llwu */

#define PMU_LWKUPRCD0_CMP_Pos   (1U)
#define PMU_LWKUPRCD0_CMP_Msk   (0x1U << PMU_LWKUPRCD0_CMP_Pos)                     /*!< 0x00000002 */
#define PMU_LWKUPRCD0_CMP       PMU_LWKUPRCD0_CMP_Msk                               /*!<last_wakeup_record_cmp */

#define PMU_LWKUPRCD0_ADC_Pos   (2U)
#define PMU_LWKUPRCD0_ADC_Msk   (0x1U << PMU_LWKUPRCD0_ADC_Pos)                     /*!< 0x00000004 */
#define PMU_LWKUPRCD0_ADC       PMU_LWKUPRCD0_ADC_Msk                               /*!<last_wakeup_record_adc */

#define PMU_LWKUPRCD0_DMA_Pos   (3U)
#define PMU_LWKUPRCD0_DMA_Msk   (0x1U << PMU_LWKUPRCD0_DMA_Pos)                     /*!< 0x00000008 */
#define PMU_LWKUPRCD0_DMA       PMU_LWKUPRCD0_DMA_Msk                               /*!<last_wakeup_record_dma */

/*******************  Bit definition for PMU_LWKUPRCD1 register  ********************/
#define PMU_LWKUPRCD1_MDLA_Pos  (0U)
#define PMU_LWKUPRCD1_MDLA_Msk  (0xFFFFFFFFU << PMU_LWKUPRCD1_MDLA_Pos)             /*!< 0xFFFFFFFF */
#define PMU_LWKUPRCD1_MDLA      PMU_LWKUPRCD1_MDLA_Msk                              /*!<last_wakeup_record_modules_a[31:0] */

/*******************  Bit definition for PMU_LWKUPRCD2 register  ********************/
#define PMU_LWKUPRCD2_MDLB_Pos  (0U)
#define PMU_LWKUPRCD2_MDLB_Msk  (0xFFU << PMU_LWKUPRCD2_MDLB_Pos)                   /*!< 0x000000FF */
#define PMU_LWKUPRCD2_MDLB      PMU_LWKUPRCD2_MDLB_Msk                              /*!<last_wakeup_record_modules_b[7:0] */

/*******************  Bit definition for PMU_INTCLR register  ********************/
#define PMU_INTCLR_PM_Pos   (0U)
#define PMU_INTCLR_PM_Msk   (0x1U << PMU_INTCLR_PM_Pos)                             /*!< 0x00000001 */
#define PMU_INTCLR_PM       PMU_INTCLR_PM_Msk                                       /*!<pm_int_clr*/

/*******************  Bit definition for PMU_INT register  ********************/
#define PMU_INT_PM_Pos  (0U)
#define PMU_INT_PM_Msk  (0x1U << PMU_INT_PM_Pos)                                    /*!< 0x00000001 */
#define PMU_INT_PM      PMU_INT_PM_Msk                                              /*!<pm_int*/
//</h>
//<h>Bus Matrix
/******************************************************************************/
/*                                                                            */
/*                                    Bus Matrix                              */
/*                                                                            */
/******************************************************************************/
/*******************  Bit definition for BUSMAT_S0PR register  ********************/
#define BUSMAT_S0PR_M0_Pos  (0U)
#define BUSMAT_S0PR_M0_Msk  (0x7U << BUSMAT_S0PR_M0_Pos)                        /*!< 0x00000007 */
#define BUSMAT_S0PR_M0      BUSMAT_S0PR_M0_Msk                                  /*!<M0[2:0](Master 0 priority) */
#define BUSMAT_S0PR_M0_0    (0x1U << BUSMAT_S0PR_M0_Pos)                        /*!< 0x00000001 */
#define BUSMAT_S0PR_M0_1    (0x2U << BUSMAT_S0PR_M0_Pos)                        /*!< 0x00000002 */
#define BUSMAT_S0PR_M0_2    (0x4U << BUSMAT_S0PR_M0_Pos)                        /*!< 0x00000004 */

#define BUSMAT_S0PR_M1_Pos  (4U)
#define BUSMAT_S0PR_M1_Msk  (0x7U << BUSMAT_S0PR_M1_Pos)                        /*!< 0x00000070 */
#define BUSMAT_S0PR_M1      BUSMAT_S0PR_M1_Msk                                  /*!<M1[2:0](Master 1 priority) */
#define BUSMAT_S0PR_M1_0    (0x1U << BUSMAT_S0PR_M1_Pos)                        /*!< 0x00000010 */
#define BUSMAT_S0PR_M1_1    (0x2U << BUSMAT_S0PR_M1_Pos)                        /*!< 0x00000020 */
#define BUSMAT_S0PR_M1_2    (0x4U << BUSMAT_S0PR_M1_Pos)                        /*!< 0x00000040 */

#define BUSMAT_S0PR_M2_Pos  (8U)
#define BUSMAT_S0PR_M2_Msk  (0x7U << BUSMAT_S0PR_M2_Pos)                        /*!< 0x00000700 */
#define BUSMAT_S0PR_M2      BUSMAT_S0PR_M2_Msk                                  /*!<M2[2:0](Master 2 priority) */
#define BUSMAT_S0PR_M2_0    (0x1U << BUSMAT_S0PR_M2_Pos)                        /*!< 0x00000100 */
#define BUSMAT_S0PR_M2_1    (0x2U << BUSMAT_S0PR_M2_Pos)                        /*!< 0x00000200 */
#define BUSMAT_S0PR_M2_2    (0x4U << BUSMAT_S0PR_M2_Pos)                        /*!< 0x00000400 */

#define BUSMAT_S0PR_M3_Pos  (12U)
#define BUSMAT_S0PR_M3_Msk  (0x7U << BUSMAT_S0PR_M3_Pos)                        /*!< 0x00007000 */
#define BUSMAT_S0PR_M3      BUSMAT_S0PR_M3_Msk                                  /*!<M3[2:0](Master 3 priority) */
#define BUSMAT_S0PR_M3_0    (0x1U << BUSMAT_S0PR_M3_Pos)                        /*!< 0x00001000 */
#define BUSMAT_S0PR_M3_1    (0x2U << BUSMAT_S0PR_M3_Pos)                        /*!< 0x00002000 */
#define BUSMAT_S0PR_M3_2    (0x4U << BUSMAT_S0PR_M3_Pos)                        /*!< 0x00004000 */

#define BUSMAT_S0PR_M4_Pos  (16U)
#define BUSMAT_S0PR_M4_Msk  (0x7U << BUSMAT_S0PR_M4_Pos)                        /*!< 0x00070000 */
#define BUSMAT_S0PR_M4      BUSMAT_S0PR_M4_Msk                                  /*!<M4[2:0](Master 4 priority) */
#define BUSMAT_S0PR_M4_0    (0x1U << BUSMAT_S0PR_M4_Pos)                        /*!< 0x00010000 */
#define BUSMAT_S0PR_M4_1    (0x2U << BUSMAT_S0PR_M4_Pos)                        /*!< 0x00020000 */
#define BUSMAT_S0PR_M4_2    (0x4U << BUSMAT_S0PR_M4_Pos)                        /*!< 0x00040000 */

#define BUSMAT_S0PR_M5_Pos  (20U)
#define BUSMAT_S0PR_M5_Msk  (0x7U << BUSMAT_S0PR_M5_Pos)                        /*!< 0x00700000 */
#define BUSMAT_S0PR_M5      BUSMAT_S0PR_M5_Msk                                  /*!<M5[2:0](Master 5 priority) */
#define BUSMAT_S0PR_M5_0    (0x1U << BUSMAT_S0PR_M5_Pos)                        /*!< 0x00100000 */
#define BUSMAT_S0PR_M5_1    (0x2U << BUSMAT_S0PR_M5_Pos)                        /*!< 0x00200000 */
#define BUSMAT_S0PR_M5_2    (0x4U << BUSMAT_S0PR_M5_Pos)                        /*!< 0x00400000 */

#define BUSMAT_S0PR_PS_Pos  (31U)
#define BUSMAT_S0PR_PS_Msk  (0x1U << BUSMAT_S0PR_PS_Pos)                        /*!< 0x80000000 */
#define BUSMAT_S0PR_PS      BUSMAT_S0PR_PS_Msk                                  /*!<priority same */

/*******************  Bit definition for BUSMAT_S0CR register  ********************/
#define BUSMAT_S0CR_PARK_Pos    (0U)
#define BUSMAT_S0CR_PARK_Msk    (0x7U << BUSMAT_S0CR_PARK_Pos)                  /*!< 0x00000007 */
#define BUSMAT_S0CR_PARK        BUSMAT_S0CR_PARK_Msk                            /*!<PARK[2:0](park which master) */
#define BUSMAT_S0CR_PARK_0      (0x1U << BUSMAT_S0CR_PARK_Pos)                  /*!< 0x00000001 */
#define BUSMAT_S0CR_PARK_1      (0x2U << BUSMAT_S0CR_PARK_Pos)                  /*!< 0x00000002 */
#define BUSMAT_S0CR_PARK_2      (0x4U << BUSMAT_S0CR_PARK_Pos)                  /*!< 0x00000004 */

#define BUSMAT_S0CR_PCTL_Pos    (4U)
#define BUSMAT_S0CR_PCTL_Msk    (0x3U << BUSMAT_S0CR_PCTL_Pos)                  /*!< 0x00000030 */
#define BUSMAT_S0CR_PCTL        BUSMAT_S0CR_PCTL_Msk                            /*!<PCTL[2:0](park control,3 park mode) */
#define BUSMAT_S0CR_PCTL_0      (0x1U << BUSMAT_S0CR_PCTL_Pos)                  /*!< 0x00000010 */
#define BUSMAT_S0CR_PCTL_1      (0x2U << BUSMAT_S0CR_PCTL_Pos)                  /*!< 0x00000020 */

#define BUSMAT_S0CR_ARBM_Pos    (8U)
#define BUSMAT_S0CR_ARBM_Msk    (0x1U << BUSMAT_S0CR_ARBM_Pos)                  /*!< 0x00000100 */
#define BUSMAT_S0CR_ARBM        BUSMAT_S0CR_ARBM_Msk                            /*!<arbitration mode*/

#define BUSMAT_S0CR_AULB_Pos    (16U)
#define BUSMAT_S0CR_AULB_Msk    (0x3U << BUSMAT_S0CR_AULB_Pos)                  /*!< 0x00030000 */
#define BUSMAT_S0CR_AULB        BUSMAT_S0CR_AULB_Msk                            /*!<AULB[2:0](arbitration undefined length burst) */
#define BUSMAT_S0CR_AULB_0      (0x1U << BUSMAT_S0CR_AULB_Pos)                  /*!< 0x00010000 */
#define BUSMAT_S0CR_AULB_1      (0x2U << BUSMAT_S0CR_AULB_Pos)                  /*!< 0x00020000 */

#define BUSMAT_S0CR_ULBL_Pos    (18U)
#define BUSMAT_S0CR_ULBL_Msk    (0x1U << BUSMAT_S0CR_ULBL_Pos)                  /*!< 0x00040000 */
#define BUSMAT_S0CR_ULBL        BUSMAT_S0CR_ULBL_Msk                            /*!< undefined length burst block */

#define BUSMAT_S0CR_RO_Pos  (31U)
#define BUSMAT_S0CR_RO_Msk  (0x1U << BUSMAT_S0CR_RO_Pos)                        /*!< 0x80000000 */
#define BUSMAT_S0CR_RO      BUSMAT_S0CR_RO_Msk                                  /*!< s0pr and s0cr read only */

/*******************  Bit definition for BUSMAT_S1PR register  ********************/
#define BUSMAT_S1PR_M0_Pos  (0U)
#define BUSMAT_S1PR_M0_Msk  (0x7U << BUSMAT_S1PR_M0_Pos)                        /*!< 0x00000007 */
#define BUSMAT_S1PR_M0      BUSMAT_S1PR_M0_Msk                                  /*!<M0[2:0](Master 0 priority) */
#define BUSMAT_S1PR_M0_0    (0x1U << BUSMAT_S1PR_M0_Pos)                        /*!< 0x00000001 */
#define BUSMAT_S1PR_M0_1    (0x2U << BUSMAT_S1PR_M0_Pos)                        /*!< 0x00000002 */
#define BUSMAT_S1PR_M0_2    (0x4U << BUSMAT_S1PR_M0_Pos)                        /*!< 0x00000004 */

#define BUSMAT_S1PR_M1_Pos  (4U)
#define BUSMAT_S1PR_M1_Msk  (0x7U << BUSMAT_S1PR_M1_Pos)                        /*!< 0x00000070 */
#define BUSMAT_S1PR_M1      BUSMAT_S1PR_M1_Msk                                  /*!<M1[2:0](Master 1 priority) */
#define BUSMAT_S1PR_M1_0    (0x1U << BUSMAT_S1PR_M1_Pos)                        /*!< 0x00000010 */
#define BUSMAT_S1PR_M1_1    (0x2U << BUSMAT_S1PR_M1_Pos)                        /*!< 0x00000020 */
#define BUSMAT_S1PR_M1_2    (0x4U << BUSMAT_S1PR_M1_Pos)                        /*!< 0x00000040 */

#define BUSMAT_S1PR_M2_Pos  (8U)
#define BUSMAT_S1PR_M2_Msk  (0x7U << BUSMAT_S1PR_M2_Pos)                        /*!< 0x00000700 */
#define BUSMAT_S1PR_M2      BUSMAT_S1PR_M2_Msk                                  /*!<M2[2:0](Master 2 priority) */
#define BUSMAT_S1PR_M2_0    (0x1U << BUSMAT_S1PR_M2_Pos)                        /*!< 0x00000100 */
#define BUSMAT_S1PR_M2_1    (0x2U << BUSMAT_S1PR_M2_Pos)                        /*!< 0x00000200 */
#define BUSMAT_S1PR_M2_2    (0x4U << BUSMAT_S1PR_M2_Pos)                        /*!< 0x00000400 */

#define BUSMAT_S1PR_M3_Pos  (12U)
#define BUSMAT_S1PR_M3_Msk  (0x7U << BUSMAT_S1PR_M3_Pos)                        /*!< 0x00007000 */
#define BUSMAT_S1PR_M3      BUSMAT_S1PR_M3_Msk                                  /*!<M3[2:0](Master 3 priority) */
#define BUSMAT_S1PR_M3_0    (0x1U << BUSMAT_S1PR_M3_Pos)                        /*!< 0x00001000 */
#define BUSMAT_S1PR_M3_1    (0x2U << BUSMAT_S1PR_M3_Pos)                        /*!< 0x00002000 */
#define BUSMAT_S1PR_M3_2    (0x4U << BUSMAT_S1PR_M3_Pos)                        /*!< 0x00004000 */

#define BUSMAT_S1PR_M4_Pos  (16U)
#define BUSMAT_S1PR_M4_Msk  (0x7U << BUSMAT_S1PR_M4_Pos)                        /*!< 0x00070000 */
#define BUSMAT_S1PR_M4      BUSMAT_S1PR_M4_Msk                                  /*!<M4[2:0](Master 4 priority) */
#define BUSMAT_S1PR_M4_0    (0x1U << BUSMAT_S1PR_M4_Pos)                        /*!< 0x00010000 */
#define BUSMAT_S1PR_M4_1    (0x2U << BUSMAT_S1PR_M4_Pos)                        /*!< 0x00020000 */
#define BUSMAT_S1PR_M4_2    (0x4U << BUSMAT_S1PR_M4_Pos)                        /*!< 0x00040000 */

#define BUSMAT_S1PR_M5_Pos  (20U)
#define BUSMAT_S1PR_M5_Msk  (0x7U << BUSMAT_S1PR_M5_Pos)                        /*!< 0x00700000 */
#define BUSMAT_S1PR_M5      BUSMAT_S1PR_M5_Msk                                  /*!<M5[2:0](Master 5 priority) */
#define BUSMAT_S1PR_M5_0    (0x1U << BUSMAT_S1PR_M5_Pos)                        /*!< 0x00100000 */
#define BUSMAT_S1PR_M5_1    (0x2U << BUSMAT_S1PR_M5_Pos)                        /*!< 0x00200000 */
#define BUSMAT_S1PR_M5_2    (0x4U << BUSMAT_S1PR_M5_Pos)                        /*!< 0x00400000 */

#define BUSMAT_S1PR_PS_Pos  (31U)
#define BUSMAT_S1PR_PS_Msk  (0x1U << BUSMAT_S1PR_PS_Pos)                        /*!< 0x80000000 */
#define BUSMAT_S1PR_PS      BUSMAT_S1PR_PS_Msk                                  /*!<priority same */

/*******************  Bit definition for BUSMAT_S1CR register  ********************/
#define BUSMAT_S1CR_PARK_Pos    (0U)
#define BUSMAT_S1CR_PARK_Msk    (0x7U << BUSMAT_S1CR_PARK_Pos)                  /*!< 0x00000007 */
#define BUSMAT_S1CR_PARK        BUSMAT_S1CR_PARK_Msk                            /*!<PARK[2:0](park) */
#define BUSMAT_S1CR_PARK_0      (0x1U << BUSMAT_S1CR_PARK_Pos)                  /*!< 0x00000001 */
#define BUSMAT_S1CR_PARK_1      (0x2U << BUSMAT_S1CR_PARK_Pos)                  /*!< 0x00000002 */
#define BUSMAT_S1CR_PARK_2      (0x4U << BUSMAT_S1CR_PARK_Pos)                  /*!< 0x00000004 */

#define BUSMAT_S1CR_PCTL_Pos    (4U)
#define BUSMAT_S1CR_PCTL_Msk    (0x3U << BUSMAT_S1CR_PCTL_Pos)                  /*!< 0x00000030 */
#define BUSMAT_S1CR_PCTL        BUSMAT_S1CR_PCTL_Msk                            /*!<PCTL[2:0](park control) */
#define BUSMAT_S1CR_PCTL_0      (0x1U << BUSMAT_S1CR_PCTL_Pos)                  /*!< 0x00000010 */
#define BUSMAT_S1CR_PCTL_1      (0x2U << BUSMAT_S1CR_PCTL_Pos)                  /*!< 0x00000020 */

#define BUSMAT_S1CR_ARBM_Pos    (8U)
#define BUSMAT_S1CR_ARBM_Msk    (0x1U << BUSMAT_S1CR_ARBM_Pos)                  /*!< 0x00000100 */
#define BUSMAT_S1CR_ARBM        BUSMAT_S1CR_ARBM_Msk                            /*!<arbitration mode*/

#define BUSMAT_S1CR_AULB_Pos    (16U)
#define BUSMAT_S1CR_AULB_Msk    (0x3U << BUSMAT_S1CR_AULB_Pos)                  /*!< 0x00030000 */
#define BUSMAT_S1CR_AULB        BUSMAT_S1CR_AULB_Msk                            /*!<AULB[2:0](arbitration undefined length burst) */
#define BUSMAT_S1CR_AULB_0      (0x1U << BUSMAT_S1CR_AULB_Pos)                  /*!< 0x00010000 */
#define BUSMAT_S1CR_AULB_1      (0x2U << BUSMAT_S1CR_AULB_Pos)                  /*!< 0x00020000 */

#define BUSMAT_S1CR_ULBL_Pos    (18U)
#define BUSMAT_S1CR_ULBL_Msk    (0x1U << BUSMAT_S1CR_ULBL_Pos)                  /*!< 0x00040000 */
#define BUSMAT_S1CR_ULBL        BUSMAT_S1CR_ULBL_Msk                            /*!< undefined length burst block */

#define BUSMAT_S1CR_RO_Pos  (31U)
#define BUSMAT_S1CR_RO_Msk  (0x1U << BUSMAT_S1CR_RO_Pos)                        /*!< 0x80000000 */
#define BUSMAT_S1CR_RO      BUSMAT_S1CR_RO_Msk                                  /*!< read only */

/*******************  Bit definition for BUSMAT_S2PR register  ********************/
#define BUSMAT_S2PR_M0_Pos  (0U)
#define BUSMAT_S2PR_M0_Msk  (0x7U << BUSMAT_S2PR_M0_Pos)                        /*!< 0x00000007 */
#define BUSMAT_S2PR_M0      BUSMAT_S2PR_M0_Msk                                  /*!<M0[2:0](Master 0 priority) */
#define BUSMAT_S2PR_M0_0    (0x1U << BUSMAT_S2PR_M0_Pos)                        /*!< 0x00000001 */
#define BUSMAT_S2PR_M0_1    (0x2U << BUSMAT_S2PR_M0_Pos)                        /*!< 0x00000002 */
#define BUSMAT_S2PR_M0_2    (0x4U << BUSMAT_S2PR_M0_Pos)                        /*!< 0x00000004 */

#define BUSMAT_S2PR_M1_Pos  (4U)
#define BUSMAT_S2PR_M1_Msk  (0x7U << BUSMAT_S2PR_M1_Pos)                        /*!< 0x00000070 */
#define BUSMAT_S2PR_M1      BUSMAT_S2PR_M1_Msk                                  /*!<M1[2:0](Master 1 priority) */
#define BUSMAT_S2PR_M1_0    (0x1U << BUSMAT_S2PR_M1_Pos)                        /*!< 0x00000010 */
#define BUSMAT_S2PR_M1_1    (0x2U << BUSMAT_S2PR_M1_Pos)                        /*!< 0x00000020 */
#define BUSMAT_S2PR_M1_2    (0x4U << BUSMAT_S2PR_M1_Pos)                        /*!< 0x00000040 */

#define BUSMAT_S2PR_M2_Pos  (8U)
#define BUSMAT_S2PR_M2_Msk  (0x7U << BUSMAT_S2PR_M2_Pos)                        /*!< 0x00000700 */
#define BUSMAT_S2PR_M2      BUSMAT_S2PR_M2_Msk                                  /*!<M2[2:0](Master 2 priority) */
#define BUSMAT_S2PR_M2_0    (0x1U << BUSMAT_S2PR_M2_Pos)                        /*!< 0x00000100 */
#define BUSMAT_S2PR_M2_1    (0x2U << BUSMAT_S2PR_M2_Pos)                        /*!< 0x00000200 */
#define BUSMAT_S2PR_M2_2    (0x4U << BUSMAT_S2PR_M2_Pos)                        /*!< 0x00000400 */

#define BUSMAT_S2PR_M3_Pos  (12U)
#define BUSMAT_S2PR_M3_Msk  (0x7U << BUSMAT_S2PR_M3_Pos)                        /*!< 0x00007000 */
#define BUSMAT_S2PR_M3      BUSMAT_S2PR_M3_Msk                                  /*!<M3[2:0](Master 3 priority) */
#define BUSMAT_S2PR_M3_0    (0x1U << BUSMAT_S2PR_M3_Pos)                        /*!< 0x00001000 */
#define BUSMAT_S2PR_M3_1    (0x2U << BUSMAT_S2PR_M3_Pos)                        /*!< 0x00002000 */
#define BUSMAT_S2PR_M3_2    (0x4U << BUSMAT_S2PR_M3_Pos)                        /*!< 0x00004000 */

#define BUSMAT_S2PR_M4_Pos  (16U)
#define BUSMAT_S2PR_M4_Msk  (0x7U << BUSMAT_S2PR_M4_Pos)                        /*!< 0x00070000 */
#define BUSMAT_S2PR_M4      BUSMAT_S2PR_M4_Msk                                  /*!<M4[2:0](Master 4 priority) */
#define BUSMAT_S2PR_M4_0    (0x1U << BUSMAT_S2PR_M4_Pos)                        /*!< 0x00010000 */
#define BUSMAT_S2PR_M4_1    (0x2U << BUSMAT_S2PR_M4_Pos)                        /*!< 0x00020000 */
#define BUSMAT_S2PR_M4_2    (0x4U << BUSMAT_S2PR_M4_Pos)                        /*!< 0x00040000 */

#define BUSMAT_S2PR_M5_Pos  (20U)
#define BUSMAT_S2PR_M5_Msk  (0x7U << BUSMAT_S2PR_M5_Pos)                        /*!< 0x00700000 */
#define BUSMAT_S2PR_M5      BUSMAT_S2PR_M5_Msk                                  /*!<M5[2:0](Master 5 priority) */
#define BUSMAT_S2PR_M5_0    (0x1U << BUSMAT_S2PR_M5_Pos)                        /*!< 0x00100000 */
#define BUSMAT_S2PR_M5_1    (0x2U << BUSMAT_S2PR_M5_Pos)                        /*!< 0x00200000 */
#define BUSMAT_S2PR_M5_2    (0x4U << BUSMAT_S2PR_M5_Pos)                        /*!< 0x00400000 */

#define BUSMAT_S2PR_PS_Pos  (31U)
#define BUSMAT_S2PR_PS_Msk  (0x1U << BUSMAT_S2PR_PS_Pos)                        /*!< 0x80000000 */
#define BUSMAT_S2PR_PS      BUSMAT_S2PR_PS_Msk                                  /*!<priority same */

/*******************  Bit definition for BUSMAT_S2CR register  ********************/
#define BUSMAT_S2CR_PARK_Pos    (0U)
#define BUSMAT_S2CR_PARK_Msk    (0x7U << BUSMAT_S2CR_PARK_Pos)                  /*!< 0x00000007 */
#define BUSMAT_S2CR_PARK        BUSMAT_S2CR_PARK_Msk                            /*!<PARK[2:0](park) */
#define BUSMAT_S2CR_PARK_0      (0x1U << BUSMAT_S2CR_PARK_Pos)                  /*!< 0x00000001 */
#define BUSMAT_S2CR_PARK_1      (0x2U << BUSMAT_S2CR_PARK_Pos)                  /*!< 0x00000002 */
#define BUSMAT_S2CR_PARK_2      (0x4U << BUSMAT_S2CR_PARK_Pos)                  /*!< 0x00000004 */

#define BUSMAT_S2CR_PCTL_Pos    (4U)
#define BUSMAT_S2CR_PCTL_Msk    (0x3U << BUSMAT_S2CR_PCTL_Pos)                  /*!< 0x00000030 */
#define BUSMAT_S2CR_PCTL        BUSMAT_S2CR_PCTL_Msk                            /*!<PCTL[2:0](park control) */
#define BUSMAT_S2CR_PCTL_0      (0x1U << BUSMAT_S2CR_PCTL_Pos)                  /*!< 0x00000010 */
#define BUSMAT_S2CR_PCTL_1      (0x2U << BUSMAT_S2CR_PCTL_Pos)                  /*!< 0x00000020 */

#define BUSMAT_S2CR_ARBM_Pos    (8U)
#define BUSMAT_S2CR_ARBM_Msk    (0x1U << BUSMAT_S2CR_ARBM_Pos)                  /*!< 0x00000100 */
#define BUSMAT_S2CR_ARBM        BUSMAT_S2CR_ARBM_Msk                            /*!<arbitration mode*/

#define BUSMAT_S2CR_AULB_Pos    (16U)
#define BUSMAT_S2CR_AULB_Msk    (0x3U << BUSMAT_S2CR_AULB_Pos)                  /*!< 0x00030000 */
#define BUSMAT_S2CR_AULB        BUSMAT_S2CR_AULB_Msk                            /*!<AULB[2:0](arbitration undefined length burst) */
#define BUSMAT_S2CR_AULB_0      (0x1U << BUSMAT_S2CR_AULB_Pos)                  /*!< 0x00010000 */
#define BUSMAT_S2CR_AULB_1      (0x2U << BUSMAT_S2CR_AULB_Pos)                  /*!< 0x00020000 */

#define BUSMAT_S2CR_ULBL_Pos    (18U)
#define BUSMAT_S2CR_ULBL_Msk    (0x1U << BUSMAT_S2CR_ULBL_Pos)                  /*!< 0x00040000 */
#define BUSMAT_S2CR_ULBL        BUSMAT_S2CR_ULBL_Msk                            /*!< undefined length burst block */

#define BUSMAT_S2CR_RO_Pos  (31U)
#define BUSMAT_S2CR_RO_Msk  (0x1U << BUSMAT_S2CR_RO_Pos)                        /*!< 0x80000000 */
#define BUSMAT_S2CR_RO      BUSMAT_S2CR_RO_Msk                                  /*!< read only */

/*******************  Bit definition for BUSMAT_S3PR register  ********************/
#define BUSMAT_S3PR_M0_Pos  (0U)
#define BUSMAT_S3PR_M0_Msk  (0x7U << BUSMAT_S3PR_M0_Pos)                        /*!< 0x00000007 */
#define BUSMAT_S3PR_M0      BUSMAT_S3PR_M0_Msk                                  /*!<M0[2:0](Master 0 priority) */
#define BUSMAT_S3PR_M0_0    (0x1U << BUSMAT_S3PR_M0_Pos)                        /*!< 0x00000001 */
#define BUSMAT_S3PR_M0_1    (0x2U << BUSMAT_S3PR_M0_Pos)                        /*!< 0x00000002 */
#define BUSMAT_S3PR_M0_2    (0x4U << BUSMAT_S3PR_M0_Pos)                        /*!< 0x00000004 */

#define BUSMAT_S3PR_M1_Pos  (4U)
#define BUSMAT_S3PR_M1_Msk  (0x7U << BUSMAT_S3PR_M1_Pos)                        /*!< 0x00000070 */
#define BUSMAT_S3PR_M1      BUSMAT_S3PR_M1_Msk                                  /*!<M1[2:0](Master 1 priority) */
#define BUSMAT_S3PR_M1_0    (0x1U << BUSMAT_S3PR_M1_Pos)                        /*!< 0x00000010 */
#define BUSMAT_S3PR_M1_1    (0x2U << BUSMAT_S3PR_M1_Pos)                        /*!< 0x00000020 */
#define BUSMAT_S3PR_M1_2    (0x4U << BUSMAT_S3PR_M1_Pos)                        /*!< 0x00000040 */

#define BUSMAT_S3PR_M2_Pos  (8U)
#define BUSMAT_S3PR_M2_Msk  (0x7U << BUSMAT_S3PR_M2_Pos)                        /*!< 0x00000700 */
#define BUSMAT_S3PR_M2      BUSMAT_S3PR_M2_Msk                                  /*!<M2[2:0](Master 2 priority) */
#define BUSMAT_S3PR_M2_0    (0x1U << BUSMAT_S3PR_M2_Pos)                        /*!< 0x00000100 */
#define BUSMAT_S3PR_M2_1    (0x2U << BUSMAT_S3PR_M2_Pos)                        /*!< 0x00000200 */
#define BUSMAT_S3PR_M2_2    (0x4U << BUSMAT_S3PR_M2_Pos)                        /*!< 0x00000400 */

#define BUSMAT_S3PR_M3_Pos  (12U)
#define BUSMAT_S3PR_M3_Msk  (0x7U << BUSMAT_S3PR_M3_Pos)                        /*!< 0x00007000 */
#define BUSMAT_S3PR_M3      BUSMAT_S3PR_M3_Msk                                  /*!<M3[2:0](Master 3 priority) */
#define BUSMAT_S3PR_M3_0    (0x1U << BUSMAT_S3PR_M3_Pos)                        /*!< 0x00001000 */
#define BUSMAT_S3PR_M3_1    (0x2U << BUSMAT_S3PR_M3_Pos)                        /*!< 0x00002000 */
#define BUSMAT_S3PR_M3_2    (0x4U << BUSMAT_S3PR_M3_Pos)                        /*!< 0x00004000 */

#define BUSMAT_S3PR_M4_Pos  (16U)
#define BUSMAT_S3PR_M4_Msk  (0x7U << BUSMAT_S3PR_M4_Pos)                        /*!< 0x00070000 */
#define BUSMAT_S3PR_M4      BUSMAT_S3PR_M4_Msk                                  /*!<M4[2:0](Master 4 priority) */
#define BUSMAT_S3PR_M4_0    (0x1U << BUSMAT_S3PR_M4_Pos)                        /*!< 0x00010000 */
#define BUSMAT_S3PR_M4_1    (0x2U << BUSMAT_S3PR_M4_Pos)                        /*!< 0x00020000 */
#define BUSMAT_S3PR_M4_2    (0x4U << BUSMAT_S3PR_M4_Pos)                        /*!< 0x00040000 */

#define BUSMAT_S3PR_M5_Pos  (20U)
#define BUSMAT_S3PR_M5_Msk  (0x7U << BUSMAT_S3PR_M5_Pos)                        /*!< 0x00700000 */
#define BUSMAT_S3PR_M5      BUSMAT_S3PR_M5_Msk                                  /*!<M5[2:0](Master 5 priority) */
#define BUSMAT_S3PR_M5_0    (0x1U << BUSMAT_S3PR_M5_Pos)                        /*!< 0x00100000 */
#define BUSMAT_S3PR_M5_1    (0x2U << BUSMAT_S3PR_M5_Pos)                        /*!< 0x00200000 */
#define BUSMAT_S3PR_M5_2    (0x4U << BUSMAT_S3PR_M5_Pos)                        /*!< 0x00400000 */

#define BUSMAT_S3PR_PS_Pos  (31U)
#define BUSMAT_S3PR_PS_Msk  (0x1U << BUSMAT_S3PR_PS_Pos)                        /*!< 0x80000000 */
#define BUSMAT_S3PR_PS      BUSMAT_S3PR_PS_Msk                                  /*!<priority same */

/*******************  Bit definition for BUSMAT_S3CR register  ********************/
#define BUSMAT_S3CR_PARK_Pos    (0U)
#define BUSMAT_S3CR_PARK_Msk    (0x7U << BUSMAT_S3CR_PARK_Pos)                  /*!< 0x00000007 */
#define BUSMAT_S3CR_PARK        BUSMAT_S3CR_PARK_Msk                            /*!<PARK[2:0](park) */
#define BUSMAT_S3CR_PARK_0      (0x1U << BUSMAT_S3CR_PARK_Pos)                  /*!< 0x00000001 */
#define BUSMAT_S3CR_PARK_1      (0x2U << BUSMAT_S3CR_PARK_Pos)                  /*!< 0x00000002 */
#define BUSMAT_S3CR_PARK_2      (0x4U << BUSMAT_S3CR_PARK_Pos)                  /*!< 0x00000004 */

#define BUSMAT_S3CR_PCTL_Pos    (4U)
#define BUSMAT_S3CR_PCTL_Msk    (0x3U << BUSMAT_S3CR_PCTL_Pos)                  /*!< 0x00000030 */
#define BUSMAT_S3CR_PCTL        BUSMAT_S3CR_PCTL_Msk                            /*!<PCTL[2:0](park control) */
#define BUSMAT_S3CR_PCTL_0      (0x1U << BUSMAT_S3CR_PCTL_Pos)                  /*!< 0x00000010 */
#define BUSMAT_S3CR_PCTL_1      (0x2U << BUSMAT_S3CR_PCTL_Pos)                  /*!< 0x00000020 */

#define BUSMAT_S3CR_ARBM_Pos    (8U)
#define BUSMAT_S3CR_ARBM_Msk    (0x1U << BUSMAT_S3CR_ARBM_Pos)                  /*!< 0x00000100 */
#define BUSMAT_S3CR_ARBM        BUSMAT_S3CR_ARBM_Msk                            /*!<arbitration mode*/

#define BUSMAT_S3CR_AULB_Pos    (16U)
#define BUSMAT_S3CR_AULB_Msk    (0x3U << BUSMAT_S3CR_AULB_Pos)                  /*!< 0x00030000 */
#define BUSMAT_S3CR_AULB        BUSMAT_S3CR_AULB_Msk                            /*!<AULB[2:0](arbitration undefined length burst) */
#define BUSMAT_S3CR_AULB_0      (0x1U << BUSMAT_S3CR_AULB_Pos)                  /*!< 0x00010000 */
#define BUSMAT_S3CR_AULB_1      (0x2U << BUSMAT_S3CR_AULB_Pos)                  /*!< 0x00020000 */

#define BUSMAT_S3CR_ULBL_Pos    (18U)
#define BUSMAT_S3CR_ULBL_Msk    (0x1U << BUSMAT_S3CR_ULBL_Pos)                  /*!< 0x00040000 */
#define BUSMAT_S3CR_ULBL        BUSMAT_S3CR_ULBL_Msk                            /*!< undefined length burst block */

#define BUSMAT_S3CR_RO_Pos  (31U)
#define BUSMAT_S3CR_RO_Msk  (0x1U << BUSMAT_S3CR_RO_Pos)                        /*!< 0x80000000 */
#define BUSMAT_S3CR_RO      BUSMAT_S3CR_RO_Msk                                  /*!< read only */

/*******************  Bit definition for BUSMAT_S4PR register  ********************/
#define BUSMAT_S4PR_M0_Pos  (0U)
#define BUSMAT_S4PR_M0_Msk  (0x7U << BUSMAT_S4PR_M0_Pos)                        /*!< 0x00000007 */
#define BUSMAT_S4PR_M0      BUSMAT_S4PR_M0_Msk                                  /*!<M0[2:0](Master 0 priority) */
#define BUSMAT_S4PR_M0_0    (0x1U << BUSMAT_S4PR_M0_Pos)                        /*!< 0x00000001 */
#define BUSMAT_S4PR_M0_1    (0x2U << BUSMAT_S4PR_M0_Pos)                        /*!< 0x00000002 */
#define BUSMAT_S4PR_M0_2    (0x4U << BUSMAT_S4PR_M0_Pos)                        /*!< 0x00000004 */

#define BUSMAT_S4PR_M1_Pos  (4U)
#define BUSMAT_S4PR_M1_Msk  (0x7U << BUSMAT_S4PR_M1_Pos)                        /*!< 0x00000070 */
#define BUSMAT_S4PR_M1      BUSMAT_S4PR_M1_Msk                                  /*!<M1[2:0](Master 1 priority) */
#define BUSMAT_S4PR_M1_0    (0x1U << BUSMAT_S4PR_M1_Pos)                        /*!< 0x00000010 */
#define BUSMAT_S4PR_M1_1    (0x2U << BUSMAT_S4PR_M1_Pos)                        /*!< 0x00000020 */
#define BUSMAT_S4PR_M1_2    (0x4U << BUSMAT_S4PR_M1_Pos)                        /*!< 0x00000040 */

#define BUSMAT_S4PR_M2_Pos  (8U)
#define BUSMAT_S4PR_M2_Msk  (0x7U << BUSMAT_S4PR_M2_Pos)                        /*!< 0x00000700 */
#define BUSMAT_S4PR_M2      BUSMAT_S4PR_M2_Msk                                  /*!<M2[2:0](Master 2 priority) */
#define BUSMAT_S4PR_M2_0    (0x1U << BUSMAT_S4PR_M2_Pos)                        /*!< 0x00000100 */
#define BUSMAT_S4PR_M2_1    (0x2U << BUSMAT_S4PR_M2_Pos)                        /*!< 0x00000200 */
#define BUSMAT_S4PR_M2_2    (0x4U << BUSMAT_S4PR_M2_Pos)                        /*!< 0x00000400 */

#define BUSMAT_S4PR_M3_Pos  (12U)
#define BUSMAT_S4PR_M3_Msk  (0x7U << BUSMAT_S4PR_M3_Pos)                        /*!< 0x00007000 */
#define BUSMAT_S4PR_M3      BUSMAT_S4PR_M3_Msk                                  /*!<M3[2:0](Master 3 priority) */
#define BUSMAT_S4PR_M3_0    (0x1U << BUSMAT_S4PR_M3_Pos)                        /*!< 0x00001000 */
#define BUSMAT_S4PR_M3_1    (0x2U << BUSMAT_S4PR_M3_Pos)                        /*!< 0x00002000 */
#define BUSMAT_S4PR_M3_2    (0x4U << BUSMAT_S4PR_M3_Pos)                        /*!< 0x00004000 */

#define BUSMAT_S4PR_M4_Pos  (16U)
#define BUSMAT_S4PR_M4_Msk  (0x7U << BUSMAT_S4PR_M4_Pos)                        /*!< 0x00070000 */
#define BUSMAT_S4PR_M4      BUSMAT_S4PR_M4_Msk                                  /*!<M4[2:0](Master 4 priority) */
#define BUSMAT_S4PR_M4_0    (0x1U << BUSMAT_S4PR_M4_Pos)                        /*!< 0x00010000 */
#define BUSMAT_S4PR_M4_1    (0x2U << BUSMAT_S4PR_M4_Pos)                        /*!< 0x00020000 */
#define BUSMAT_S4PR_M4_2    (0x4U << BUSMAT_S4PR_M4_Pos)                        /*!< 0x00040000 */

#define BUSMAT_S4PR_M5_Pos  (20U)
#define BUSMAT_S4PR_M5_Msk  (0x7U << BUSMAT_S4PR_M5_Pos)                        /*!< 0x00700000 */
#define BUSMAT_S4PR_M5      BUSMAT_S4PR_M5_Msk                                  /*!<M5[2:0](Master 5 priority) */
#define BUSMAT_S4PR_M5_0    (0x1U << BUSMAT_S4PR_M5_Pos)                        /*!< 0x00100000 */
#define BUSMAT_S4PR_M5_1    (0x2U << BUSMAT_S4PR_M5_Pos)                        /*!< 0x00200000 */
#define BUSMAT_S4PR_M5_2    (0x4U << BUSMAT_S4PR_M5_Pos)                        /*!< 0x00400000 */

#define BUSMAT_S4PR_PS_Pos  (31U)
#define BUSMAT_S4PR_PS_Msk  (0x1U << BUSMAT_S4PR_PS_Pos)                        /*!< 0x80000000 */
#define BUSMAT_S4PR_PS      BUSMAT_S4PR_PS_Msk                                  /*!<priority same */

/*******************  Bit definition for BUSMAT_S4CR register  ********************/
#define BUSMAT_S4CR_PARK_Pos    (0U)
#define BUSMAT_S4CR_PARK_Msk    (0x7U << BUSMAT_S4CR_PARK_Pos)                  /*!< 0x00000007 */
#define BUSMAT_S4CR_PARK        BUSMAT_S4CR_PARK_Msk                            /*!<PARK[2:0](park) */
#define BUSMAT_S4CR_PARK_0      (0x1U << BUSMAT_S4CR_PARK_Pos)                  /*!< 0x00000001 */
#define BUSMAT_S4CR_PARK_1      (0x2U << BUSMAT_S4CR_PARK_Pos)                  /*!< 0x00000002 */
#define BUSMAT_S4CR_PARK_2      (0x4U << BUSMAT_S4CR_PARK_Pos)                  /*!< 0x00000004 */

#define BUSMAT_S4CR_PCTL_Pos    (4U)
#define BUSMAT_S4CR_PCTL_Msk    (0x3U << BUSMAT_S4CR_PCTL_Pos)                  /*!< 0x00000030 */
#define BUSMAT_S4CR_PCTL        BUSMAT_S4CR_PCTL_Msk                            /*!<PCTL[2:0](park control) */
#define BUSMAT_S4CR_PCTL_0      (0x1U << BUSMAT_S4CR_PCTL_Pos)                  /*!< 0x00000010 */
#define BUSMAT_S4CR_PCTL_1      (0x2U << BUSMAT_S4CR_PCTL_Pos)                  /*!< 0x00000020 */

#define BUSMAT_S4CR_ARBM_Pos    (8U)
#define BUSMAT_S4CR_ARBM_Msk    (0x1U << BUSMAT_S4CR_ARBM_Pos)                  /*!< 0x00000100 */
#define BUSMAT_S4CR_ARBM        BUSMAT_S4CR_ARBM_Msk                            /*!<arbitration mode*/

#define BUSMAT_S4CR_AULB_Pos    (16U)
#define BUSMAT_S4CR_AULB_Msk    (0x3U << BUSMAT_S4CR_AULB_Pos)                  /*!< 0x00030000 */
#define BUSMAT_S4CR_AULB        BUSMAT_S4CR_AULB_Msk                            /*!<AULB[2:0](arbitration undefined length burst) */
#define BUSMAT_S4CR_AULB_0      (0x1U << BUSMAT_S4CR_AULB_Pos)                  /*!< 0x00010000 */
#define BUSMAT_S4CR_AULB_1      (0x2U << BUSMAT_S4CR_AULB_Pos)                  /*!< 0x00020000 */

#define BUSMAT_S4CR_ULBL_Pos    (18U)
#define BUSMAT_S4CR_ULBL_Msk    (0x1U << BUSMAT_S4CR_ULBL_Pos)                  /*!< 0x00040000 */
#define BUSMAT_S4CR_ULBL        BUSMAT_S4CR_ULBL_Msk                            /*!< undefined length burst block */

#define BUSMAT_S4CR_RO_Pos  (31U)
#define BUSMAT_S4CR_RO_Msk  (0x1U << BUSMAT_S4CR_RO_Pos)                        /*!< 0x80000000 */
#define BUSMAT_S4CR_RO      BUSMAT_S4CR_RO_Msk                                  /*!< read only */

/*******************  Bit definition for BUSMAT_S5PR register  ********************/
#define BUSMAT_S5PR_M0_Pos  (0U)
#define BUSMAT_S5PR_M0_Msk  (0x7U << BUSMAT_S5PR_M0_Pos)                        /*!< 0x00000007 */
#define BUSMAT_S5PR_M0      BUSMAT_S5PR_M0_Msk                                  /*!<M0[2:0](Master 0 priority) */
#define BUSMAT_S5PR_M0_0    (0x1U << BUSMAT_S5PR_M0_Pos)                        /*!< 0x00000001 */
#define BUSMAT_S5PR_M0_1    (0x2U << BUSMAT_S5PR_M0_Pos)                        /*!< 0x00000002 */
#define BUSMAT_S5PR_M0_2    (0x4U << BUSMAT_S5PR_M0_Pos)                        /*!< 0x00000004 */

#define BUSMAT_S5PR_M1_Pos  (4U)
#define BUSMAT_S5PR_M1_Msk  (0x7U << BUSMAT_S5PR_M1_Pos)                        /*!< 0x00000070 */
#define BUSMAT_S5PR_M1      BUSMAT_S5PR_M1_Msk                                  /*!<M1[2:0](Master 1 priority) */
#define BUSMAT_S5PR_M1_0    (0x1U << BUSMAT_S5PR_M1_Pos)                        /*!< 0x00000010 */
#define BUSMAT_S5PR_M1_1    (0x2U << BUSMAT_S5PR_M1_Pos)                        /*!< 0x00000020 */
#define BUSMAT_S5PR_M1_2    (0x4U << BUSMAT_S5PR_M1_Pos)                        /*!< 0x00000040 */

#define BUSMAT_S5PR_M2_Pos  (8U)
#define BUSMAT_S5PR_M2_Msk  (0x7U << BUSMAT_S5PR_M2_Pos)                        /*!< 0x00000700 */
#define BUSMAT_S5PR_M2      BUSMAT_S5PR_M2_Msk                                  /*!<M2[2:0](Master 2 priority) */
#define BUSMAT_S5PR_M2_0    (0x1U << BUSMAT_S5PR_M2_Pos)                        /*!< 0x00000100 */
#define BUSMAT_S5PR_M2_1    (0x2U << BUSMAT_S5PR_M2_Pos)                        /*!< 0x00000200 */
#define BUSMAT_S5PR_M2_2    (0x4U << BUSMAT_S5PR_M2_Pos)                        /*!< 0x00000400 */

#define BUSMAT_S5PR_M3_Pos  (12U)
#define BUSMAT_S5PR_M3_Msk  (0x7U << BUSMAT_S5PR_M3_Pos)                        /*!< 0x00007000 */
#define BUSMAT_S5PR_M3      BUSMAT_S5PR_M3_Msk                                  /*!<M3[2:0](Master 3 priority) */
#define BUSMAT_S5PR_M3_0    (0x1U << BUSMAT_S5PR_M3_Pos)                        /*!< 0x00001000 */
#define BUSMAT_S5PR_M3_1    (0x2U << BUSMAT_S5PR_M3_Pos)                        /*!< 0x00002000 */
#define BUSMAT_S5PR_M3_2    (0x4U << BUSMAT_S5PR_M3_Pos)                        /*!< 0x00004000 */

#define BUSMAT_S5PR_M4_Pos  (16U)
#define BUSMAT_S5PR_M4_Msk  (0x7U << BUSMAT_S5PR_M4_Pos)                        /*!< 0x00070000 */
#define BUSMAT_S5PR_M4      BUSMAT_S5PR_M4_Msk                                  /*!<M4[2:0](Master 4 priority) */
#define BUSMAT_S5PR_M4_0    (0x1U << BUSMAT_S5PR_M4_Pos)                        /*!< 0x00010000 */
#define BUSMAT_S5PR_M4_1    (0x2U << BUSMAT_S5PR_M4_Pos)                        /*!< 0x00020000 */
#define BUSMAT_S5PR_M4_2    (0x4U << BUSMAT_S5PR_M4_Pos)                        /*!< 0x00040000 */

#define BUSMAT_S5PR_M5_Pos  (20U)
#define BUSMAT_S5PR_M5_Msk  (0x7U << BUSMAT_S5PR_M5_Pos)                        /*!< 0x00700000 */
#define BUSMAT_S5PR_M5      BUSMAT_S5PR_M5_Msk                                  /*!<M5[2:0](Master 5 priority) */
#define BUSMAT_S5PR_M5_0    (0x1U << BUSMAT_S5PR_M5_Pos)                        /*!< 0x00100000 */
#define BUSMAT_S5PR_M5_1    (0x2U << BUSMAT_S5PR_M5_Pos)                        /*!< 0x00200000 */
#define BUSMAT_S5PR_M5_2    (0x4U << BUSMAT_S5PR_M5_Pos)                        /*!< 0x00400000 */

#define BUSMAT_S5PR_PS_Pos  (31U)
#define BUSMAT_S5PR_PS_Msk  (0x1U << BUSMAT_S5PR_PS_Pos)                        /*!< 0x80000000 */
#define BUSMAT_S5PR_PS      BUSMAT_S5PR_PS_Msk                                  /*!<priority same */

/*******************  Bit definition for BUSMAT_S5CR register  ********************/
#define BUSMAT_S5CR_PARK_Pos    (0U)
#define BUSMAT_S5CR_PARK_Msk    (0x7U << BUSMAT_S5CR_PARK_Pos)                  /*!< 0x00000007 */
#define BUSMAT_S5CR_PARK        BUSMAT_S5CR_PARK_Msk                            /*!<PARK[2:0](park) */
#define BUSMAT_S5CR_PARK_0      (0x1U << BUSMAT_S5CR_PARK_Pos)                  /*!< 0x00000001 */
#define BUSMAT_S5CR_PARK_1      (0x2U << BUSMAT_S5CR_PARK_Pos)                  /*!< 0x00000002 */
#define BUSMAT_S5CR_PARK_2      (0x4U << BUSMAT_S5CR_PARK_Pos)                  /*!< 0x00000004 */

#define BUSMAT_S5CR_PCTL_Pos    (4U)
#define BUSMAT_S5CR_PCTL_Msk    (0x3U << BUSMAT_S5CR_PCTL_Pos)                  /*!< 0x00000030 */
#define BUSMAT_S5CR_PCTL        BUSMAT_S5CR_PCTL_Msk                            /*!<PCTL[2:0](park control) */
#define BUSMAT_S5CR_PCTL_0      (0x1U << BUSMAT_S5CR_PCTL_Pos)                  /*!< 0x00000010 */
#define BUSMAT_S5CR_PCTL_1      (0x2U << BUSMAT_S5CR_PCTL_Pos)                  /*!< 0x00000020 */

#define BUSMAT_S5CR_ARBM_Pos    (8U)
#define BUSMAT_S5CR_ARBM_Msk    (0x1U << BUSMAT_S5CR_ARBM_Pos)                  /*!< 0x00000100 */
#define BUSMAT_S5CR_ARBM        BUSMAT_S5CR_ARBM_Msk                            /*!<arbitration mode*/

#define BUSMAT_S5CR_AULB_Pos    (16U)
#define BUSMAT_S5CR_AULB_Msk    (0x3U << BUSMAT_S5CR_AULB_Pos)                  /*!< 0x00030000 */
#define BUSMAT_S5CR_AULB        BUSMAT_S5CR_AULB_Msk                            /*!<AULB[2:0](arbitration undefined length burst) */
#define BUSMAT_S5CR_AULB_0      (0x1U << BUSMAT_S5CR_AULB_Pos)                  /*!< 0x00010000 */
#define BUSMAT_S5CR_AULB_1      (0x2U << BUSMAT_S5CR_AULB_Pos)                  /*!< 0x00020000 */

#define BUSMAT_S5CR_ULBL_Pos    (18U)
#define BUSMAT_S5CR_ULBL_Msk    (0x1U << BUSMAT_S5CR_ULBL_Pos)                  /*!< 0x00040000 */
#define BUSMAT_S5CR_ULBL        BUSMAT_S5CR_ULBL_Msk                            /*!< undefined length burst block */

#define BUSMAT_S5CR_RO_Pos  (31U)
#define BUSMAT_S5CR_RO_Msk  (0x1U << BUSMAT_S5CR_RO_Pos)                        /*!< 0x80000000 */
#define BUSMAT_S5CR_RO      BUSMAT_S5CR_RO_Msk                                  /*!< read only */

/*******************  Bit definition for BUSMAT_S6PR register  ********************/
#define BUSMAT_S6PR_M0_Pos  (0U)
#define BUSMAT_S6PR_M0_Msk  (0x7U << BUSMAT_S6PR_M0_Pos)                        /*!< 0x00000007 */
#define BUSMAT_S6PR_M0      BUSMAT_S6PR_M0_Msk                                  /*!<M0[2:0](Master 0 priority) */
#define BUSMAT_S6PR_M0_0    (0x1U << BUSMAT_S6PR_M0_Pos)                        /*!< 0x00000001 */
#define BUSMAT_S6PR_M0_1    (0x2U << BUSMAT_S6PR_M0_Pos)                        /*!< 0x00000002 */
#define BUSMAT_S6PR_M0_2    (0x4U << BUSMAT_S6PR_M0_Pos)                        /*!< 0x00000004 */

#define BUSMAT_S6PR_M1_Pos  (4U)
#define BUSMAT_S6PR_M1_Msk  (0x7U << BUSMAT_S6PR_M1_Pos)                        /*!< 0x00000070 */
#define BUSMAT_S6PR_M1      BUSMAT_S6PR_M1_Msk                                  /*!<M1[2:0](Master 1 priority) */
#define BUSMAT_S6PR_M1_0    (0x1U << BUSMAT_S6PR_M1_Pos)                        /*!< 0x00000010 */
#define BUSMAT_S6PR_M1_1    (0x2U << BUSMAT_S6PR_M1_Pos)                        /*!< 0x00000020 */
#define BUSMAT_S6PR_M1_2    (0x4U << BUSMAT_S6PR_M1_Pos)                        /*!< 0x00000040 */

#define BUSMAT_S6PR_M2_Pos  (8U)
#define BUSMAT_S6PR_M2_Msk  (0x7U << BUSMAT_S6PR_M2_Pos)                        /*!< 0x00000700 */
#define BUSMAT_S6PR_M2      BUSMAT_S6PR_M2_Msk                                  /*!<M2[2:0](Master 2 priority) */
#define BUSMAT_S6PR_M2_0    (0x1U << BUSMAT_S6PR_M2_Pos)                        /*!< 0x00000100 */
#define BUSMAT_S6PR_M2_1    (0x2U << BUSMAT_S6PR_M2_Pos)                        /*!< 0x00000200 */
#define BUSMAT_S6PR_M2_2    (0x4U << BUSMAT_S6PR_M2_Pos)                        /*!< 0x00000400 */

#define BUSMAT_S6PR_M3_Pos  (12U)
#define BUSMAT_S6PR_M3_Msk  (0x7U << BUSMAT_S6PR_M3_Pos)                        /*!< 0x00007000 */
#define BUSMAT_S6PR_M3      BUSMAT_S6PR_M3_Msk                                  /*!<M3[2:0](Master 3 priority) */
#define BUSMAT_S6PR_M3_0    (0x1U << BUSMAT_S6PR_M3_Pos)                        /*!< 0x00001000 */
#define BUSMAT_S6PR_M3_1    (0x2U << BUSMAT_S6PR_M3_Pos)                        /*!< 0x00002000 */
#define BUSMAT_S6PR_M3_2    (0x4U << BUSMAT_S6PR_M3_Pos)                        /*!< 0x00004000 */

#define BUSMAT_S6PR_M4_Pos  (16U)
#define BUSMAT_S6PR_M4_Msk  (0x7U << BUSMAT_S6PR_M4_Pos)                        /*!< 0x00070000 */
#define BUSMAT_S6PR_M4      BUSMAT_S6PR_M4_Msk                                  /*!<M4[2:0](Master 4 priority) */
#define BUSMAT_S6PR_M4_0    (0x1U << BUSMAT_S6PR_M4_Pos)                        /*!< 0x00010000 */
#define BUSMAT_S6PR_M4_1    (0x2U << BUSMAT_S6PR_M4_Pos)                        /*!< 0x00020000 */
#define BUSMAT_S6PR_M4_2    (0x4U << BUSMAT_S6PR_M4_Pos)                        /*!< 0x00040000 */

#define BUSMAT_S6PR_M5_Pos  (20U)
#define BUSMAT_S6PR_M5_Msk  (0x7U << BUSMAT_S6PR_M5_Pos)                        /*!< 0x00700000 */
#define BUSMAT_S6PR_M5      BUSMAT_S6PR_M5_Msk                                  /*!<M5[2:0](Master 5 priority) */
#define BUSMAT_S6PR_M5_0    (0x1U << BUSMAT_S6PR_M5_Pos)                        /*!< 0x00100000 */
#define BUSMAT_S6PR_M5_1    (0x2U << BUSMAT_S6PR_M5_Pos)                        /*!< 0x00200000 */
#define BUSMAT_S6PR_M5_2    (0x4U << BUSMAT_S6PR_M5_Pos)                        /*!< 0x00400000 */

#define BUSMAT_S6PR_PS_Pos  (31U)
#define BUSMAT_S6PR_PS_Msk  (0x1U << BUSMAT_S6PR_PS_Pos)                        /*!< 0x80000000 */
#define BUSMAT_S6PR_PS      BUSMAT_S6PR_PS_Msk                                  /*!<priority same */

/*******************  Bit definition for BUSMAT_S6CR register  ********************/
#define BUSMAT_S6CR_PARK_Pos    (0U)
#define BUSMAT_S6CR_PARK_Msk    (0x7U << BUSMAT_S6CR_PARK_Pos)                  /*!< 0x00000007 */
#define BUSMAT_S6CR_PARK        BUSMAT_S6CR_PARK_Msk                            /*!<PARK[2:0](park) */
#define BUSMAT_S6CR_PARK_0      (0x1U << BUSMAT_S6CR_PARK_Pos)                  /*!< 0x00000001 */
#define BUSMAT_S6CR_PARK_1      (0x2U << BUSMAT_S6CR_PARK_Pos)                  /*!< 0x00000002 */
#define BUSMAT_S6CR_PARK_2      (0x4U << BUSMAT_S6CR_PARK_Pos)                  /*!< 0x00000004 */

#define BUSMAT_S6CR_PCTL_Pos    (4U)
#define BUSMAT_S6CR_PCTL_Msk    (0x3U << BUSMAT_S6CR_PCTL_Pos)                  /*!< 0x00000030 */
#define BUSMAT_S6CR_PCTL        BUSMAT_S6CR_PCTL_Msk                            /*!<PCTL[2:0](park control) */
#define BUSMAT_S6CR_PCTL_0      (0x1U << BUSMAT_S6CR_PCTL_Pos)                  /*!< 0x00000010 */
#define BUSMAT_S6CR_PCTL_1      (0x2U << BUSMAT_S6CR_PCTL_Pos)                  /*!< 0x00000020 */

#define BUSMAT_S6CR_ARBM_Pos    (8U)
#define BUSMAT_S6CR_ARBM_Msk    (0x1U << BUSMAT_S6CR_ARBM_Pos)                  /*!< 0x00000100 */
#define BUSMAT_S6CR_ARBM        BUSMAT_S6CR_ARBM_Msk                            /*!<arbitration mode*/

#define BUSMAT_S6CR_AULB_Pos    (16U)
#define BUSMAT_S6CR_AULB_Msk    (0x3U << BUSMAT_S6CR_AULB_Pos)                  /*!< 0x00030000 */
#define BUSMAT_S6CR_AULB        BUSMAT_S6CR_AULB_Msk                            /*!<AULB[2:0](arbitration undefined length burst) */
#define BUSMAT_S6CR_AULB_0      (0x1U << BUSMAT_S6CR_AULB_Pos)                  /*!< 0x00010000 */
#define BUSMAT_S6CR_AULB_1      (0x2U << BUSMAT_S6CR_AULB_Pos)                  /*!< 0x00020000 */

#define BUSMAT_S6CR_ULBL_Pos    (18U)
#define BUSMAT_S6CR_ULBL_Msk    (0x1U << BUSMAT_S6CR_ULBL_Pos)                  /*!< 0x00040000 */
#define BUSMAT_S6CR_ULBL        BUSMAT_S6CR_ULBL_Msk                            /*!< undefined length burst block */

#define BUSMAT_S6CR_RO_Pos  (31U)
#define BUSMAT_S6CR_RO_Msk  (0x1U << BUSMAT_S6CR_RO_Pos)                        /*!< 0x80000000 */
#define BUSMAT_S6CR_RO      BUSMAT_S6CR_RO_Msk                                  /*!< read only */

/*******************  Bit definition for BUSMAT_LFSREN register  ********************/
#define BUSMAT_LFSREN_LFSREN_Pos    (0U)
#define BUSMAT_LFSREN_LFSREN_Msk    (0x1U << BUSMAT_LFSREN_LFSREN_Pos)          /*!< 0x00000001 */
#define BUSMAT_LFSREN_LFSREN        BUSMAT_LFSREN_LFSREN_Msk                    /*!< Enable bus mask */

/*******************  Bit definition for BUSMAT_LFSRD register  ********************/
#define BUSMAT_LFSRD_LFSRD_Pos  (0U)
#define BUSMAT_LFSRD_LFSRD_Msk  (0xFFFFFFFFU << BUSMAT_LFSRD_LFSRD_Pos)         /*!< 0xFFFFFFFF */
#define BUSMAT_LFSRD_LFSRD      BUSMAT_LFSRD_LFSRD_Msk                          /*!< bus_lfsr_data[31:0] Data of bus mask lfsr */
//</h>
//<h>CACHE
/******************************************************************************/
/*                                                                            */
/*                                    CACHE                                   */
/*                                                                            */
/******************************************************************************/
#define CODE_CACHE_EN_Pos   (0U)
#define CODE_CACHE_EN_Msk   (0x1U << CODE_CACHE_EN_Pos)
#define CODE_CACHE_EN       CODE_CACHE_EN_Msk

#define DATA_CACHE_EN_Pos   (1U)
#define DATA_CACHE_EN_Msk   (0x1U << DATA_CACHE_EN_Pos)
#define DATA_CACHE_EN       DATA_CACHE_EN_Msk

#define CODE_CACHE_FLUSH_Pos    (2U)
#define CODE_CACHE_FLUSH_Msk    (0x1U << CODE_CACHE_FLUSH_Pos)
#define CODE_CACHE_FLUSH        CODE_CACHE_FLUSH_Msk

#define DATA_CACHE_FLUSH_Pos    (3U)
#define DATA_CACHE_FLUSH_Msk    (0x1U << DATA_CACHE_FLUSH_Pos)
#define DATA_CACHE_FLUSH        DATA_CACHE_FLUSH_Msk

#define CACHE_RAMBIST_START_Pos (8U)
#define CACHE_RAMBIST_START_Msk (0x1U << CACHE_RAMBIST_START_Pos)
#define CACHE_RAMBIST_START     CACHE_RAMBIST_START_Msk

#define CACHE_RAMBIST_VALID_Pos (9U)
#define CACHE_RAMBIST_VALID_Msk (0x1U << CACHE_RAMBIST_VALID_Pos)
#define CACHE_RAMBIST_VALID     CACHE_RAMBIST_VALID_Msk

#define CODE_DATA_WAY0_RAMBIST_SUCCESS_Pos  (10U)
#define CODE_DATA_WAY0_RAMBIST_SUCCESS_Msk  (0x1U << CODE_DATA_WAY0_RAMBIST_SUCCESS_Pos)
#define CODE_DATA_WAY0_RAMBIST_SUCCESS      CODE_DATA_WAY0_RAMBIST_SUCCESS_Msk

#define CODE_DATA_WAY1_RAMBIST_SUCCESS_Pos  (11U)
#define CODE_DATA_WAY1_RAMBIST_SUCCESS_Msk  (0x1U << CODE_DATA_WAY1_RAMBIST_SUCCESS_Pos)
#define CODE_DATA_WAY1_RAMBIST_SUCCESS      CODE_DATA_WAY1_RAMBIST_SUCCESS_Msk

#define CODE_TAG_WAY0_RAMBIST_SUCCESS_Pos   (12U)
#define CODE_TAG_WAY0_RAMBIST_SUCCESS_Msk   (0x1U << CODE_TAG_WAY0_RAMBIST_SUCCESS_Pos)
#define CODE_TAG_WAY0_RAMBIST_SUCCESS       CODE_TAG_WAY0_RAMBIST_SUCCESS_Msk

#define CODE_TAG_WAY1_RAMBIST_SUCCESS_Pos   (13U)
#define CODE_TAG_WAY1_RAMBIST_SUCCESS_Msk   (0x1U << CODE_TAG_WAY1_RAMBIST_SUCCESS_Pos)
#define CODE_TAG_WAY1_RAMBIST_SUCCESS       CODE_TAG_WAY1_RAMBIST_SUCCESS_Msk

#define DATA_TAG_WAY0_RAMBIST_SUCCESS_Pos   (14U)
#define DATA_TAG_WAY0_RAMBIST_SUCCESS_Msk   (0x1U << DATA_TAG_WAY0_RAMBIST_SUCCESS_Pos)
#define DATA_TAG_WAY0_RAMBIST_SUCCESS       DATA_TAG_WAY0_RAMBIST_SUCCESS_Msk

#define DATA_TAG_WAY1_RAMBIST_SUCCESS_Pos   (15U)
#define DATA_TAG_WAY1_RAMBIST_SUCCESS_Msk   (0x1U << DATA_TAG_WAY1_RAMBIST_SUCCESS_Pos)
#define DATA_TAG_WAY1_RAMBIST_SUCCESS       DATA_TAG_WAY1_RAMBIST_SUCCESS_Msk
//</h>
//<h>CMP
/******************************************************************************/
/*                                                                            */
/*                                    CMP                                     */
/*                                                                            */
/******************************************************************************/
/*******************   Bit definition for CMP_CTRL register    ********************/
#define CMP_CTRL_CMPEN_Pos  (0U)
#define CMP_CTRL_CMPEN_Msk  (0x1U << CMP_CTRL_CMPEN_Pos)                                /*!< 0x00000001 */
#define CMP_CTRL_CMPEN      CMP_CTRL_CMPEN_Msk                                          /*!< module Enable */

#define CMP_CTRL_HPEN_Pos   (1U)
#define CMP_CTRL_HPEN_Msk   (0x1U << CMP_CTRL_HPEN_Pos)                                 /*!< 0x00000002 */
#define CMP_CTRL_HPEN       CMP_CTRL_HPEN_Msk                                           /*!< high power en Enable */

#define CMP_CTRL_HYSTER_Pos (2U)
#define CMP_CTRL_HYSTER_Msk (0x3U << CMP_CTRL_HYSTER_Pos)                               /*!< 0x00000006 */
#define CMP_CTRL_HYSTER     CMP_CTRL_HYSTER_Msk                                         /*!< HYSTER[1:0] hyster mode select  */

#define CMP_CTRL_RDYTIME_Pos    (4U)
#define CMP_CTRL_RDYTIME_Msk    (0x3U << CMP_CTRL_RDYTIME_Pos)                          /*!< 0x0000FFF0 */
#define CMP_CTRL_RDYTIME        CMP_CTRL_RDYTIME_Msk                                    /*!< ready_time[11:0] */

/*******************   Bit definition for CMP_INSEL register    ********************/
#define CMP_INSEL_INPSEL_Pos    (0U)
#define CMP_INSEL_INPSEL_Msk    (0x7U << CMP_INSEL_INPSEL_Pos)                          /*!< 0x00000007 */
#define CMP_INSEL_INPSEL        CMP_INSEL_INPSEL_Msk                                    /*!< INPSEL[2:0] inp port select channel  */

#define CMP_INSEL_INNSEL_Pos    (4U)
#define CMP_INSEL_INNSEL_Msk    (0x7U << CMP_INSEL_INNSEL_Pos)                          /*!< 0x00000070 */
#define CMP_INSEL_INNSEL        CMP_INSEL_INNSEL_Msk                                    /*!< INNSEL[2:0] inn port select channel */

/*******************   Bit definition for CMP_DAC register      ********************/
#define CMP_DAC_DACEN_Pos   (0U)
#define CMP_DAC_DACEN_Msk   (0x1U << CMP_DAC_DACEN_Pos)                                 /*!< 0x00000001 */
#define CMP_DAC_DACEN       CMP_DAC_DACEN_Msk                                           /*!< CMP DAC en */

#define CMP_DAC_VREFSEL_Pos (1U)
#define CMP_DAC_VREFSEL_Msk (0x1U << CMP_DAC_VREFSEL_Pos)                               /*!< 0x00000002 */
#define CMP_DAC_VREFSEL     CMP_DAC_VREFSEL_Msk                                         /*!< CMP DAC select vref */

/*******************   Bit definition for CMP_DACDATA register  ********************/
#define CMP_DACDATA_DATA_Pos    (0U)
#define CMP_DACDATA_DATA_Msk    (0x3FU << CMP_DACDATA_DATA_Pos)                         /*!< 0x0000003F */
#define CMP_DACDATA_DATA        CMP_DACDATA_DATA_Msk                                    /*!< cmp_dac_data[5:0] set DAC data */

/*******************   Bit definition for CMP_TRIGCFG register  ********************/
#define CMP_TRIGCFG_MODE_Pos    (0U)
#define CMP_TRIGCFG_MODE_Msk    (0x1U << CMP_TRIGCFG_MODE_Pos)                          /*!< 0x00000001 */
#define CMP_TRIGCFG_MODE        CMP_TRIGCFG_MODE_Msk                                    /*!< CMP trigger mode select */

#define CMP_TRIGCFG_INV_Pos (1U)
#define CMP_TRIGCFG_INV_Msk (0x1U << CMP_TRIGCFG_INV_Pos)                               /*!< 0x00000002 */
#define CMP_TRIGCFG_INV     CMP_TRIGCFG_INV_Msk                                         /*!< CMP trigger sourse inverse the signal */

#define CMP_TRIGCFG_EDGE_Pos    (2U)
#define CMP_TRIGCFG_EDGE_Msk    (0x3U << CMP_TRIGCFG_EDGE_Pos)                          /*!< 0x0000000C */
#define CMP_TRIGCFG_EDGE        CMP_TRIGCFG_EDGE_Msk                                    /*!< trig_edge[1:0] set trigger edge mode */

#define CMP_TRIGCFG_SC_Pos  (4U)
#define CMP_TRIGCFG_SC_Msk  (0xFU << CMP_TRIGCFG_SC_Pos)                                /*!< 0x000000F0 */
#define CMP_TRIGCFG_SC      CMP_TRIGCFG_SC_Msk                                          /*!< trig_sc[3:0] select trigger source */

/*******************   Bit definition for CMP_SWTRIG register  ********************/
#define CMP_SWTRIG_CTRL_Pos (0U)
#define CMP_SWTRIG_CTRL_Msk (0x1U << CMP_SWTRIG_CTRL_Pos)                               /*!< 0x00000001 */
#define CMP_SWTRIG_CTRL     CMP_SWTRIG_CTRL_Msk                                         /*!< software trigger ctrl  */

/*******************   Bit definition for CMP_OUTCTRL register  ********************/
#define CMP_OUTCTRL_INV_Pos (0U)
#define CMP_OUTCTRL_INV_Msk (0x1U << CMP_OUTCTRL_INV_Pos)                               /*!< 0x00000001 */
#define CMP_OUTCTRL_INV     CMP_OUTCTRL_INV_Msk                                         /*!< CMP output inverse  */

#define CMP_OUTCTRL_WINEN_Pos   (4U)
#define CMP_OUTCTRL_WINEN_Msk   (0x1U << CMP_OUTCTRL_WINEN_Pos)                         /*!< 0x00000010 */
#define CMP_OUTCTRL_WINEN       CMP_OUTCTRL_WINEN_Msk                                   /*!< CMP window en  */

#define CMP_OUTCTRL_WINSC_Pos   (5U)
#define CMP_OUTCTRL_WINSC_Msk   (0x3U << CMP_OUTCTRL_WINSC_Pos)                         /*!< 0x00000060 */
#define CMP_OUTCTRL_WINSC       CMP_OUTCTRL_WINSC_Msk                                   /*!< windows_sc[1:0] select window source  */

#define CMP_OUTCTRL_FILTEN_Pos  (8U)
#define CMP_OUTCTRL_FILTEN_Msk  (0x1U << CMP_OUTCTRL_FILTEN_Pos)                        /*!< 0x00000100 */
#define CMP_OUTCTRL_FILTEN      CMP_OUTCTRL_FILTEN_Msk                                  /*!< CMP filter en  */

#define CMP_OUTCTRL_FILTCNT_Pos (12U)
#define CMP_OUTCTRL_FILTCNT_Msk (0xFU << CMP_OUTCTRL_FILTCNT_Pos)                       /*!< 0x0000F000 */
#define CMP_OUTCTRL_FILTCNT     CMP_OUTCTRL_FILTCNT_Msk                                 /*!< filter_cnt[3:0] set filter period  */

/*******************   Bit definition for CMP_STATUS register  ********************/
#define CMP_STATUS_RESULT_Pos   (0U)
#define CMP_STATUS_RESULT_Msk   (0x1U << CMP_STATUS_RESULT_Pos)                         /*!< 0x00000001 */
#define CMP_STATUS_RESULT       CMP_STATUS_RESULT_Msk                                   /*!< CMP result record */

#define CMP_STATUS_ANAOUT_Pos   (1U)
#define CMP_STATUS_ANAOUT_Msk   (0x1U << CMP_STATUS_ANAOUT_Pos)                         /*!< 0x00000002 */
#define CMP_STATUS_ANAOUT       CMP_STATUS_ANAOUT_Msk                                   /*!< CMP analog out  signal*/

#define CMP_STATUS_ANARDY_Pos   (2U)
#define CMP_STATUS_ANARDY_Msk   (0x1U << CMP_STATUS_ANARDY_Pos)                         /*!< 0x00000004 */
#define CMP_STATUS_ANARDY       CMP_STATUS_ANARDY_Msk                                   /*!< CMP analog out Stable Flag*/

/*******************   Bit definition for CMP_INTEN register  ********************/
#define CMP_INTEN_INTDMAEN_Pos  (0U)
#define CMP_INTEN_INTDMAEN_Msk  (0x1U << CMP_INTEN_INTDMAEN_Pos)                        /*!< 0x00000001 */
#define CMP_INTEN_INTDMAEN      CMP_INTEN_INTDMAEN_Msk                                  /*!< CMP int & dma  enable */

#define CMP_INTEN_DMASEL_Pos    (1U)
#define CMP_INTEN_DMASEL_Msk    (0x1U << CMP_INTEN_DMASEL_Pos)                          /*!< 0x00000002 */
#define CMP_INTEN_DMASEL        CMP_INTEN_DMASEL_Msk                                    /*!< CMP record produce int or DMA  */

#define CMP_INTEN_MODE_Pos  (4U)
#define CMP_INTEN_MODE_Msk  (0x3U << CMP_INTEN_MODE_Pos)                                /*!< 0x00000030 */
#define CMP_INTEN_MODE      CMP_INTEN_MODE_Msk                                          /*!< cmp_rcd_int_mode[1:0] select mode to produce record  */

/*******************   Bit definition for CMP_RCD register  ********************/
#define CMP_RCD_RCD_Pos (0U)
#define CMP_RCD_RCD_Msk (0x1U << CMP_RCD_RCD_Pos)                                       /*!< 0x00000001 */
#define CMP_RCD_RCD     CMP_RCD_RCD_Msk                                                 /*!< CMP produce result record from cmp_rcd_int_mode[1:0] */

#define CMP_RCD_INT_Pos (0U)
#define CMP_RCD_INT_Msk (0x1U << CMP_RCD_INT_Pos)                                       /*!< 0x00000002 */
#define CMP_INT_RCD     CMP_RCD_INT_Msk                                                 /*!< CMP produce result record */

/*******************   Bit definition for CMP_RCDCLR register  ********************/
#define CMP_RCDCLR_CLEAR_Pos    (0U)
#define CMP_RCDCLR_CLEAR_Msk    (0x1U << CMP_RCDCLR_CLEAR_Pos)                          /*!< 0x00000001 */
#define CMP_RCDCLR_CLEAR        CMP_RCDCLR_CLEAR_Msk                                    /*!< CMP record clear  */
//</h>
//<h>CRC
/******************************************************************************/
/*                                                                            */
/*                                    CRC_CSR                                 */
/*                                                                            */
/******************************************************************************/
/*******************   Bit definition for CRC_CFG register   ********************/
#define CRC_CFG_RUN_Pos (0U)
#define CRC_CFG_RUN_Msk (0x1U << CRC_CFG_RUN_Pos)                                   /*!< 0x00000001 */
#define CRC_CFG_RUN     CRC_CFG_RUN_Msk                                             /*!< crc_run */

#define CRC_CFG_MODE_Pos    (1U)
#define CRC_CFG_MODE_Msk    (0x1U << CRC_CFG_MODE_Pos)                              /*!< 0x00000002 */
#define CRC_CFG_MODE        CRC_CFG_MODE_Msk                                        /*!< crc_mode  */

#define CRC_CFG_DATAWIDTH_Pos   (2U)
#define CRC_CFG_DATAWIDTH_Msk   (0x1U << CRC_CFG_DATAWIDTH_Pos)                     /*!< 0x00000004 */
#define CRC_CFG_DATAWIDTH       CRC_CFG_DATAWIDTH_Msk                               /*!< crc_data_width  */

#define CRC_CFG_ENDIAN_BIT_Pos  (3U)
#define CRC_CFG_ENDIAN_BIT_Msk  (0x1U << CRC_CFG_ENDIAN_BIT_Pos)                    /*!< 0x00000008 */
#define CRC_CFG_ENDIAN_BIT      CRC_CFG_ENDIAN_BIT_Msk                              /*!< crc_endian_bit_fix  */

#define CRC_CFG_ENDIAN_BYTE_Pos (4U)
#define CRC_CFG_ENDIAN_BYTE_Msk (0x1U << CRC_CFG_ENDIAN_BYTE_Pos)                   /*!< 0x00000010 */
#define CRC_CFG_ENDIAN_BYTE     CRC_CFG_ENDIAN_BYTE_Msk                             /*!< crc_endian_byte_fix  */

//</h>
//<h>CT
/******************************************************************************/
/*                                                                            */
/*                                    CT_CSR                                    */
/*                                                                            */
/******************************************************************************/
/*******************   Bit definition for CT_CFGIO register   ********************/
#define CT_CFGIO_MODLEN_Pos (0U)
#define CT_CFGIO_MODLEN_Msk (0x1U << CT_CFGIO_MODLEN_Pos)                               /*!< 0x00000001 */
#define CT_CFGIO_MODLEN     CT_CFGIO_MODLEN_Msk                                         /*!<ct_module_en */

#define CT_CFGIO_IOMDSEL_Pos    (1U)
#define CT_CFGIO_IOMDSEL_Msk    (0x1U << CT_CFGIO_IOMDSEL_Pos)                          /*!< 0x00000002 */
#define CT_CFGIO_IOMDSEL        CT_CFGIO_IOMDSEL_Msk                                    /*!<ct_io_mode_sel */

#define CT_CFGIO_IOONSEN_Pos    (3U)
#define CT_CFGIO_IOONSEN_Msk    (0x1U << CT_CFGIO_IOONSEN_Pos)                          /*!< 0x00000004 */
#define CT_CFGIO_IOONSEN        CT_CFGIO_IOONSEN_Msk                                    /*!<ct_io_one_shoot_en */

#define CT_CFGIO_CLKPTCFG_Pos   (4U)
#define CT_CFGIO_CLKONSEN_Msk   (0x3U << CT_CFGIO_CLKPTCFG_Pos)                         /*!< 0x00000030 */
#define CT_CFGIO_CLKONSEN       CT_CFGIO_CLKONSEN_Msk                                   /*!<ct_clk_port_cfg[1:0] */

#define CT_CFGIO_RSTRXPTCFG_Pos (6U)
#define CT_CFGIO_RSTRXPTCFG_Msk (0x1U << CT_CFGIO_RSTRXPTCFG_Pos)                       /*!< 0x000000c0 */
#define CT_CFGIO_RSTRXPTCFG     CT_CFGIO_RSTRXPTCFG_Msk                                 /*!<Ct_7816rst_uart_rx_port_cfg[1:0] */

#define CT_CFGIO_CLKFRQCFGA_Pos (16U)
#define CT_CFGIO_CLKFRQCFGA_Msk (0x1FU << CT_CFGIO_CLKFRQCFGA_Pos)                      /*!< 0x001F0000 */
#define CT_CFGIO_CLKFRQCFGA     CT_CFGIO_CLKFRQCFGA_Msk                                 /*!<ct_clk_freq_cfga[4:0] */

#define CT_CFGIO_CLKFRQCFGB_Pos (24U)
#define CT_CFGIO_CLKFRQCFGB_Msk (0xFU << CT_CFGIO_CLKFRQCFGB_Pos)                       /*!< 0x0F000000 */
#define CT_CFGIO_CLKFRQCFGB     CT_CFGIO_CLKFRQCFGB_Msk                                 /*!<ct_clk_freq_cfgb[3:0] */

/*******************   Bit definition for CT_CFGTRNS register   ********************/
#define CT_CFGTRNS_DICONV_Pos   (0U)
#define CT_CFGTRNS_DICONV_Msk   (0x1U << CT_CFGTRNS_DICONV_Pos)                         /*!< 0x00000001 */
#define CT_CFGTRNS_DICONV       CT_CFGTRNS_DICONV_Msk                                   /*!<Diconv */

#define CT_CFGTRNS_TXREPCFG_Pos (1U)
#define CT_CFGTRNS_TXREPCFG_Msk (0x7U << CT_CFGTRNS_TXREPCFG_Pos)                       /*!< 0x0000000E */
#define CT_CFGTRNS_TXREPCFG     CT_CFGTRNS_TXREPCFG_Msk                                 /*!<tx_rep_cfg[2:0] */

#define CT_CFGTRNS_RXREPCFG_Pos (4U)
#define CT_CFGTRNS_RXREPCFG_Msk (0x7U << CT_CFGTRNS_RXREPCFG_Pos)                       /*!< 0x00000070 */
#define CT_CFGTRNS_RXREPCFG     CT_CFGTRNS_RXREPCFG_Msk                                 /*!<rx_rep_cfg[2:0] */

#define CT_CFGTRNS_RXGTCFG_Pos  (7U)
#define CT_CFGTRNS_RXGTCFG_Msk  (0x1U << CT_CFGTRNS_RXGTCFG_Pos)                        /*!< 0x00000080 */
#define CT_CFGTRNS_RXGTCFG      CT_CFGTRNS_RXGTCFG_Msk                                  /*!<Rx_gt_cfg */

#define CT_CFGTRNS_TXGTCFG_Pos  (8U)
#define CT_CFGTRNS_TXGTCFG_Msk  (0x3U << CT_CFGTRNS_TXGTCFG_Pos)                        /*!< 0x00000300 */
#define CT_CFGTRNS_TXGTCFG      CT_CFGTRNS_TXGTCFG_Msk                                  /*!<Tx_gt_cfg[1:0] */

#define CT_CFGTRNS_PRTYCFG_Pos  (10U)
#define CT_CFGTRNS_PRTYCFG_Msk  (0x3U << CT_CFGTRNS_PRTYCFG_Pos)                        /*!< 0x00000C00 */
#define CT_CFGTRNS_PRTYCFG      CT_CFGTRNS_PRTYCFG_Msk                                  /*!<Parity_cfg[1:0] */

#define CT_CFGTRNS_IECKPEN_Pos  (12U)
#define CT_CFGTRNS_IECKPEN_Msk  (0x1U << CT_CFGTRNS_IECKPEN_Pos)                        /*!< 0x00001000 */
#define CT_CFGTRNS_IECKPEN      CT_CFGTRNS_IECKPEN_Msk                                  /*!<Ignore_check_p_en */

#define CT_CFGTRNS_IOCLCFG_Pos  (13U)
#define CT_CFGTRNS_IOCLCFG_Msk  (0x7U << CT_CFGTRNS_IOCLCFG_Pos)                        /*!< 0x0000E000 */
#define CT_CFGTRNS_IOCLCFG      CT_CFGTRNS_IOCLCFG_Msk                                  /*!<Io_coll _cfg */

#define CT_CFGTRNS_DI_Pos   (16U)
#define CT_CFGTRNS_DI_Msk   (0xFU << CT_CFGTRNS_DI_Pos)                                 /*!< 0x000F0000 */
#define CT_CFGTRNS_DI       CT_CFGTRNS_DI_Msk                                           /*!<Di[3:0] */

#define CT_CFGTRNS_FI_Pos   (20U)
#define CT_CFGTRNS_FI_Msk   (0xFU << CT_CFGTRNS_FI_Pos)                                 /*!< 0x00F00000 */
#define CT_CFGTRNS_FI       CT_CFGTRNS_FI_Msk                                           /*!<Fi[3:0] */

#define CT_CFGTRNS_TXEGT_Pos    (24U)
#define CT_CFGTRNS_TXEGT_Msk    (0xFFU << CT_CFGTRNS_TXEGT_Pos)                         /*!< 0xFF000000 */
#define CT_CFGTRNS_TXEGT        CT_CFGTRNS_TXEGT_Msk                                    /*!<tx_egt[7:0] */

/*******************   Bit definition for CT_CTRLIO register   ********************/
#define CT_CTRLIO_RSTREG_Pos    (0U)
#define CT_CTRLIO_RSTREG_Msk    (0x1U << CT_CTRLIO_RSTREG_Pos)                          /*!< 0x00000001 */
#define CT_CTRLIO_RSTREG        CT_CTRLIO_RSTREG_Msk                                    /*!<Ct_rst_reg */

#define CT_CTRLIO_CLKOUTSTP_Pos (1U)
#define CT_CTRLIO_CLKOUTSTP_Msk (0x3U << CT_CTRLIO_CLKOUTSTP_Pos)                       /*!< 0x00000006 */
#define CT_CTRLIO_CLKOUTSTP     CT_CTRLIO_CLKOUTSTP_Msk                                 /*!<Ct_clk_out_stop[1:0] */

#define CT_CTRLIO_CFGRSTEN_Pos  (3U)
#define CT_CTRLIO_CFGRSTEN_Msk  (0x1U << CT_CTRLIO_CFGRSTEN_Pos)                        /*!< 0x00000008 */
#define CT_CTRLIO_CFGRSTEN      CT_CTRLIO_CFGRSTEN_Msk                                  /*!<Ct_cfg_rst_en */

#define CT_CTRLIO_RSTEGDTMD_Pos (4U)
#define CT_CTRLIO_RSTEGDTMD_Msk (0x1U << CT_CTRLIO_RSTEGDTMD_Pos)                       /*!< 0x00000010 */
#define CT_CTRLIO_RSTEGDTMD     CT_CTRLIO_RSTEGDTMD_Msk                                 /*!<Rst_edge_detect_mode */

#define CT_CTRLIO_STRSTCFG_Pos  (5U)
#define CT_CTRLIO_STRSTCFG_Msk  (0x1U << CT_CTRLIO_STRSTCFG_Pos)                        /*!< 0x00000020 */
#define CT_CTRLIO_STRSTCFG      CT_CTRLIO_STRSTCFG_Msk                                  /*!<ct_state_rst_cfg */

#define CT_CTRLIO_CKDTEN_Pos    (6U)
#define CT_CTRLIO_CKDTEN_Msk    (0x1U << CT_CTRLIO_CKDTEN_Pos)                          /*!< 0x00000040 */
#define CT_CTRLIO_CKDTEN        CT_CTRLIO_CKDTEN_Msk                                    /*!<Clock_detect_en */

#define CT_CTRLIO_RSTEGDTEN_Pos (7U)
#define CT_CTRLIO_RSTEGDTEN_Msk (0x1U << CT_CTRLIO_RSTEGDTEN_Pos)                       /*!< 0x00000080 */
#define CT_CTRLIO_RSTEGDTEN     CT_CTRLIO_RSTEGDTEN_Msk                                 /*!<rst_edge_detect_en */

/*******************   Bit definition for CT_CTRLTRNS register   ********************/
#define CT_CTRLTRNS_TXEN_Pos    (0U)
#define CT_CTRLTRNS_TXEN_Msk    (0x1U << CT_CTRLTRNS_TXEN_Pos)                          /*!< 0x00000001 */
#define CT_CTRLTRNS_TXEN        CT_CTRLTRNS_TXEN_Msk                                    /*!<tx_en */

#define CT_CTRLTRNS_RXEN_Pos    (1U)
#define CT_CTRLTRNS_RXEN_Msk    (0x1U << CT_CTRLTRNS_RXEN_Pos)                          /*!< 0x00000002 */
#define CT_CTRLTRNS_RXEN        CT_CTRLTRNS_RXEN_Msk                                    /*!<rx_en */

#define CT_CTRLTRNS_TXFLAGCFG_Pos   (4U)
#define CT_CTRLTRNS_TXFLAGCFG_Msk   (0x1U << CT_CTRLTRNS_TXFLAGCFG_Pos)                 /*!< 0x00000010 */
#define CT_CTRLTRNS_TXFLAGCFG       CT_CTRLTRNS_TXFLAGCFG_Msk                           /*!<Tx_flag_cfg */

#define CT_CTRLTRNS_TRMODE_Pos  (5U)
#define CT_CTRLTRNS_TRMODE_Msk  (0x1U << CT_CTRLTRNS_TRMODE_Pos)                        /*!< 0x00000020 */
#define CT_CTRLTRNS_TRMODE      CT_CTRLTRNS_TRMODE_Msk                                  /*!<tr_mode */

/*******************   Bit definition for CT_INTEN register   ********************/
#define CT_INTEN_TXINTEN_Pos    (0U)
#define CT_INTEN_TXINTEN_Msk    (0x1U << CT_INTEN_TXINTEN_Pos)                          /*!< 0x00000001 */
#define CT_INTEN_TXINTEN        CT_INTEN_TXINTEN_Msk                                    /*!<Tx_int_en */

#define CT_INTEN_RXINTEN_Pos    (1U)
#define CT_INTEN_RXINTEN_Msk    (0x1U << CT_INTEN_RXINTEN_Pos)                          /*!< 0x00000002 */
#define CT_INTEN_RXINTEN        CT_INTEN_RXINTEN_Msk                                    /*!<Rx_int_en */

#define CT_INTEN_ERRINTEN_Pos   (2U)
#define CT_INTEN_ERRINTEN_Msk   (0x1U << CT_INTEN_ERRINTEN_Pos)                         /*!< 0x00000004 */
#define CT_INTEN_ERRINTEN       CT_INTEN_ERRINTEN_Msk                                   /*!<Error_int_en */

#define CT_INTEN_CLKSTPINTEN_Pos    (3U)
#define CT_INTEN_CLKSTPINTEN_Msk    (0x1U << CT_INTEN_CLKSTPINTEN_Pos)                  /*!< 0x00000008 */
#define CT_INTEN_CLKSTPINTEN        CT_INTEN_CLKSTPINTEN_Msk                            /*!<ct_clock_stop_int_en */

#define CT_INTEN_CLKRSMINTEN_Pos    (4U)
#define CT_INTEN_CLKRSMINTEN_Msk    (0x1U << CT_INTEN_CLKRSMINTEN_Pos)                  /*!< 0x00000010 */
#define CT_INTEN_CLKRSMINTEN        CT_INTEN_CLKRSMINTEN_Msk                            /*!<ct_clock_resume_int_en */

#define CT_INTEN_RSTINTEN_Pos   (5U)
#define CT_INTEN_RSTINTEN_Msk   (0x1U << CT_INTEN_RSTINTEN_Pos)                         /*!< 0x00000020 */
#define CT_INTEN_RSTINTEN       CT_INTEN_RSTINTEN_Msk                                   /*!<rst_int_en */

#define CT_INTEN_CLKCNTINTEN_Pos    (6U)
#define CT_INTEN_CLKCNTINTEN_Msk    (0x1U << CT_INTEN_CLKCNTINTEN_Pos)                  /*!< 0x00000040 */
#define CT_INTEN_CLKCNTINTEN        CT_INTEN_CLKCNTINTEN_Msk                            /*!<Clk_cnt_int_en */

#define CT_INTEN_ETUCNTINTEN_Pos    (7U)
#define CT_INTEN_ETUCNTINTEN_Msk    (0x1U << CT_INTEN_ETUCNTINTEN_Pos)                  /*!< 0x00000080 */
#define CT_INTEN_ETUCNTINTEN        CT_INTEN_ETUCNTINTEN_Msk                            /*!<Etu_cnt_int_en */

#define CT_INTEN_RXFOERINTEN_Pos    (8U)
#define CT_INTEN_RXFOERINTEN_Msk    (0x1U << CT_INTEN_RXFOERINTEN_Pos)                  /*!< 0x00000100 */
#define CT_INTEN_RXFOERINTEN        CT_INTEN_RXFOERINTEN_Msk                            /*!<Rx_fifo_err_int_en */

#define CT_INTEN_TXFOERINTEN_Pos    (9U)
#define CT_INTEN_TXFOERINTEN_Msk    (0x1U << CT_INTEN_TXFOERINTEN_Pos)                  /*!< 0x00000200 */
#define CT_INTEN_TXFOERINTEN        CT_INTEN_TXFOERINTEN_Msk                            /*!<Tx_fifo_err_int_en */

#define CT_INTEN_RXFRMERINTEN_Pos   (10U)
#define CT_INTEN_RXFRMERINTEN_Msk   (0x1U << CT_INTEN_RXFRMERINTEN_Pos)                 /*!< 0x00000400 */
#define CT_INTEN_RXFRMERINTEN       CT_INTEN_RXFRMERINTEN_Msk                           /*!<rx_frame_err_int_en */

#define CT_INTEN_TXRSDERINTEN_Pos   (11U)
#define CT_INTEN_TXRSDERINTEN_Msk   (0x1U << CT_INTEN_TXRSDERINTEN_Pos)                 /*!< 0x00000800 */
#define CT_INTEN_TXRSDERINTEN       CT_INTEN_TXRSDERINTEN_Msk                           /*!<tx_resend_err_int_en */

#define CT_INTEN_RXPRTYERINTEN_Pos  (12U)
#define CT_INTEN_RXPRTYERINTEN_Msk  (0x1U << CT_INTEN_RXPRTYERINTEN_Pos)                /*!< 0x00001000 */
#define CT_INTEN_RXPRTYERINTEN      CT_INTEN_RXPRTYERINTEN_Msk                          /*!<rx_parity_err_int_en */

#define CT_INTEN_COLLERINTEN_Pos    (13U)
#define CT_INTEN_COLLERINTEN_Msk    (0x1U << CT_INTEN_COLLERINTEN_Pos)                  /*!< 0x00002000 */
#define CT_INTEN_COLLERINTEN        CT_INTEN_COLLERINTEN_Msk                            /*!<coll_err_int_en */

#define CT_INTEN_RXFOPRYERINTEN_Pos (14U)
#define CT_INTEN_RXFOPRYERINTEN_Msk (0x1U << CT_INTEN_RXFOPRYERINTEN_Pos)               /*!< 0x00004000 */
#define CT_INTEN_RXFOPRYERINTEN     CT_INTEN_RXFOPRYERINTEN_Msk                         /*!<rx_fifo_parity_err_int_en */

#define CT_INTEN_TXFOPRYERINTEN_Pos (15U)
#define CT_INTEN_TXFOPRYERINTEN_Msk (0x1U << CT_INTEN_TXFOPRYERINTEN_Pos)               /*!< 0x00008000 */
#define CT_INTEN_TXFOPRYERINTEN     CT_INTEN_TXFOPRYERINTEN_Msk                         /*!<tx_fifo_parity_err_int_en */

#define CT_INTEN_TXDMAEN_Pos    (24U)
#define CT_INTEN_TXDMAEN_Msk    (0x1U << CT_INTEN_TXDMAEN_Pos)                          /*!< 0x00010000 */
#define CT_INTEN_TXDMAEN        CT_INTEN_TXDMAEN_Msk                                    /*!<tx_dma_en */

#define CT_INTEN_RXDMAEN_Pos    (25U)
#define CT_INTEN_RXDMAEN_Msk    (0x1U << CT_INTEN_RXDMAEN_Pos)                          /*!< 0x00020000 */
#define CT_INTEN_RXDMAEN        CT_INTEN_RXDMAEN_Msk                                    /*!<rx_dma_en */

/*******************   Bit definition for CT_STSCLRTRNS register   ********************/
#define CT_STSCLRTRNS_CLRERR_Pos    (0U)
#define CT_STSCLRTRNS_CLRERR_Msk    (0x1U << CT_STSCLRTRNS_CLRERR_Pos)                  /*!< 0x00000001 */
#define CT_STSCLRTRNS_CLRERR        CT_STSCLRTRNS_CLRERR_Msk                            /*!<clr_error */

#define CT_STSCLRTRNS_CLRTXFIFO_Pos (1U)
#define CT_STSCLRTRNS_CLRTXFIFO_Msk (0x1U << CT_STSCLRTRNS_CLRTXFIFO_Pos)               /*!< 0x00000002 */
#define CT_STSCLRTRNS_CLRTXFIFO     CT_STSCLRTRNS_CLRTXFIFO_Msk                         /*!<clr_tx_fifo */

#define CT_STSCLRTRNS_CLRRXFIFO_Pos (2U)
#define CT_STSCLRTRNS_CLRRXFIFO_Msk (0x1U << CT_STSCLRTRNS_CLRRXFIFO_Pos)               /*!< 0x00000004 */
#define CT_STSCLRTRNS_CLRRXFIFO     CT_STSCLRTRNS_CLRRXFIFO_Msk                         /*!<clr_rx_fifo */

#define CT_STSCLRTRNS_CLRCLKFLG_Pos (3U)
#define CT_STSCLRTRNS_CLRCLKFLG_Msk (0x1U << CT_STSCLRTRNS_CLRCLKFLG_Pos)               /*!< 0x00000008 */
#define CT_STSCLRTRNS_CLRCLKFLG     CT_STSCLRTRNS_CLRCLKFLG_Msk                         /*!<Clr_clock_flag */

#define CT_STSCLRTRNS_CLRRSTFLG_Pos (4U)
#define CT_STSCLRTRNS_CLRRSTFLG_Msk (0x1U << CT_STSCLRTRNS_CLRRSTFLG_Pos)               /*!< 0x00000010 */
#define CT_STSCLRTRNS_CLRRSTFLG     CT_STSCLRTRNS_CLRRSTFLG_Msk                         /*!<Clr_rst_flag */

#define CT_STSCLRTRNS_CLRCTCNT_Pos  (8U)
#define CT_STSCLRTRNS_CLRCTCNT_Msk  (0x1U << CT_STSCLRTRNS_CLRCTCNT_Pos)                /*!< 0x00000100 */
#define CT_STSCLRTRNS_CLRCTCNT      CT_STSCLRTRNS_CLRCTCNT_Msk                          /*!<Clr_ct_cnt */

#define CT_STSCLRTRNS_CLRETUCNT_Pos (9U)
#define CT_STSCLRTRNS_CLRETUCNT_Msk (0x1U << CT_STSCLRTRNS_CLRETUCNT_Pos)               /*!< 0x00000200 */
#define CT_STSCLRTRNS_CLRETUCNT     CT_STSCLRTRNS_CLRETUCNT_Msk                         /*!<Clr_etu_cnt */

#define CT_STSCLRTRNS_CLRRXETUCNT_Pos   (10U)
#define CT_STSCLRTRNS_CLRRXETUCNT_Msk   (0x1U << CT_STSCLRTRNS_CLRRXETUCNT_Pos)         /*!< 0x00000400 */
#define CT_STSCLRTRNS_CLRRXETUCNT       CT_STSCLRTRNS_CLRRXETUCNT_Msk                   /*!<Clr_rx_etu_cnt */

#define CT_STSCLRTRNS_CLRTXETUCNT_Pos   (11U)
#define CT_STSCLRTRNS_CLRTXETUCNT_Msk   (0x1U << CT_STSCLRTRNS_CLRTXETUCNT_Pos)         /*!< 0x00000800 */
#define CT_STSCLRTRNS_CLRTXETUCNT       CT_STSCLRTRNS_CLRTXETUCNT_Msk                   /*!<Clr_tx_etu_cnt */

/*******************   Bit definition for CT_THDCNT register   ********************/
#define CT_THDCNT_THD_Pos   (0U)
#define CT_THDCNT_THD_Msk   (0xFFFFU << CT_THDCNT_THD_Pos)                              /*!< 0x0000FFFF */
#define CT_THDCNT_THD       CT_THDCNT_THD_Msk                                           /*!<Ct_cnt_ threshold [15:0] */

/*******************   Bit definition for CT_THDETUCNT register   ********************/
#define CT_THDETUCNT_THD_Pos    (0U)
#define CT_THDETUCNT_THD_Msk    (0xFFFFU << CT_THDCNT_THD_Pos)                          /*!< 0x0000FFFF */
#define CT_THDETUCNT_THD        CT_THDCNT_THD_Msk                                       /*!<etu_count_threshold[15:0] */

/*******************   Bit definition for CT_TXFIFO register   ********************/
#define CT_TXFIFO_TXFIFO_Pos    (0U)
#define CT_TXFIFO_TXFIFO_Msk    (0xFFU << CT_TXFIFO_TXFIFO_Pos)                         /*!< 0x000000FF */
#define CT_TXFIFO_TXFIFO        CT_TXFIFO_TXFIFO_Msk                                    /*!<ct_tx_fifo[7:0] */

/*******************   Bit definition for CT_RXFIFO register   ********************/
#define CT_TXFIFO_RXFIFO_Pos    (0U)
#define CT_TXFIFO_RXFIFO_Msk    (0xFFU << CT_TXFIFO_RXFIFO_Pos)                         /*!< 0x000000FF */
#define CT_TXFIFO_RXFIFO        CT_TXFIFO_RXFIFO_Msk                                    /*!<ct_rx_fifo[7:0] */

/*******************   Bit definition for CT_IOSTAT register   ********************/
#define CT_IOSTAT_CLKSTPFLG_Pos (3U)
#define CT_IOSTAT_CLKSTPFLG_Msk (0x1U << CT_IOSTAT_CLKSTPFLG_Pos)                       /*!< 0x00000008 */
#define CT_IOSTAT_CLKSTPFLG     CT_IOSTAT_CLKSTPFLG_Msk                                 /*!<Clock_stop_flag */

#define CT_IOSTAT_CLKRSMFLG_Pos (4U)
#define CT_IOSTAT_CLKRSMFLG_Msk (0x1U << CT_IOSTAT_CLKRSMFLG_Pos)                       /*!< 0x00000010 */
#define CT_IOSTAT_CLKRSMFLG     CT_IOSTAT_CLKRSMFLG_Msk                                 /*!<Clock_resume_flag */

#define CT_IOSTAT_CLKATSTAT_Pos (5U)
#define CT_IOSTAT_CLKATSTAT_Msk (0x1U << CT_IOSTAT_CLKATSTAT_Pos)                       /*!< 0x00000020 */
#define CT_IOSTAT_CLKATSTAT     CT_IOSTAT_CLKATSTAT_Msk                                 /*!<ct_clock_at_status */

#define CT_IOSTAT_RSTNGFLG_Pos  (6U)
#define CT_IOSTAT_RSTNGFLG_Msk  (0x1U << CT_IOSTAT_RSTNGFLG_Pos)                        /*!< 0x00000040 */
#define CT_IOSTAT_RSTNGFLG      CT_IOSTAT_RSTNGFLG_Msk                                  /*!<Ct_rst_negedge_flag */

#define CT_IOSTAT_RSTPGFLG_Pos  (7U)
#define CT_IOSTAT_RSTPGFLG_Msk  (0x1U << CT_IOSTAT_RSTPGFLG_Pos)                        /*!< 0x00000080 */
#define CT_IOSTAT_RSTPGFLG      CT_IOSTAT_RSTPGFLG_Msk                                  /*!<Ct_rst_posedge_flag */

/*******************   Bit definition for CT_STATUS register   ********************/
#define CT_STATUS_RXFLAG_Pos    (0U)
#define CT_STATUS_RXFLAG_Msk    (0x1U << CT_STATUS_RXFLAG_Pos)                          /*!< 0x00000001 */
#define CT_STATUS_RXFLAG        CT_STATUS_RXFLAG_Msk                                    /*!<rx_flag */

#define CT_STATUS_TXFLAG_Pos    (1U)
#define CT_STATUS_TXFLAG_Msk    (0x1U << CT_STATUS_TXFLAG_Pos)                          /*!< 0x00000002 */
#define CT_STATUS_TXFLAG        CT_STATUS_TXFLAG_Msk                                    /*!<tx_flag */

#define CT_STATUS_ERRFLAG_Pos   (2U)
#define CT_STATUS_ERRFLAG_Msk   (0x1U << CT_STATUS_ERRFLAG_Pos)                         /*!< 0x00000004 */
#define CT_STATUS_ERRFLAG       CT_STATUS_ERRFLAG_Msk                                   /*!<error_flag */

#define CT_STATUS_RXBUSY_Pos    (8U)
#define CT_STATUS_RXBUSY_Msk    (0x1U << CT_STATUS_RXBUSY_Pos)                          /*!< 0x00000100 */
#define CT_STATUS_RXBUSY        CT_STATUS_RXBUSY_Msk                                    /*!<rx_busy */

#define CT_STATUS_TXBUSY_Pos    (9U)
#define CT_STATUS_TXBUSY_Msk    (0x1U << CT_STATUS_TXBUSY_Pos)                          /*!< 0x00000200 */
#define CT_STATUS_TXBUSY        CT_STATUS_TXBUSY_Msk                                    /*!<tx_busy */

#define CT_STATUS_RXFIFOERR_Pos (10U)
#define CT_STATUS_RXFIFOERR_Msk (0x1U << CT_STATUS_RXFIFOERR_Pos)                       /*!< 0x00000400 */
#define CT_STATUS_RXFIFOERR     CT_STATUS_RXFIFOERR_Msk                                 /*!<Rx_fifo_err */

#define CT_STATUS_TXFIFOERR_Pos (11U)
#define CT_STATUS_TXFIFOERR_Msk (0x1U << CT_STATUS_TXFIFOERR_Pos)                       /*!< 0x00000800 */
#define CT_STATUS_TXFIFOERR     CT_STATUS_TXFIFOERR_Msk                                 /*!<Tx_fifo_err */

#define CT_STATUS_RXFRMERR_Pos  (12U)
#define CT_STATUS_RXFRMERR_Msk  (0x1U << CT_STATUS_RXFRMERR_Pos)                        /*!< 0x00001000 */
#define CT_STATUS_RXFRMERR      CT_STATUS_RXFRMERR_Msk                                  /*!<Rx_frame_err */

#define CT_STATUS_TXRSDERR_Pos  (13U)
#define CT_STATUS_TXRSDERR_Msk  (0x1U << CT_STATUS_TXRSDERR_Pos)                        /*!< 0x00002000 */
#define CT_STATUS_TXRSDERR      CT_STATUS_TXRSDERR_Msk                                  /*!<Tx_resend_err */

#define CT_STATUS_RXPRYERR_Pos  (14U)
#define CT_STATUS_RXPRYERR_Msk  (0x1U << CT_STATUS_RXPRYERR_Pos)                        /*!< 0x00004000 */
#define CT_STATUS_RXPRYERR      CT_STATUS_RXPRYERR_Msk                                  /*!<rx_parity_err */

#define CT_STATUS_COLLERR_Pos   (15U)
#define CT_STATUS_COLLERR_Msk   (0x1U << CT_STATUS_COLLERR_Pos)                         /*!< 0x00008000 */
#define CT_STATUS_COLLERR       CT_STATUS_COLLERR_Msk                                   /*!<coll_err */

#define CT_STATUS_RXFIFOPRYERR_Pos  (16U)
#define CT_STATUS_RXFIFOPRYERR_Msk  (0x1U << CT_STATUS_RXFIFOPRYERR_Pos)                /*!< 0x00010000 */
#define CT_STATUS_RXFIFOPRYERR      CT_STATUS_RXFIFOPRYERR_Msk                          /*!<rx_fifo_parity_err */

#define CT_STATUS_EXTETUCNT_Pos (27U)
#define CT_STATUS_EXTETUCNT_Msk (0x3U << CT_STATUS_EXTETUCNT_Pos)                       /*!< 0x18000000 */
#define CT_STATUS_EXTETUCNT     CT_STATUS_EXTETUCNT_Msk                                 /*!<extra_etu_cnt[1:0] */

#define CT_STATUS_CLKCNTFLG_Pos (29U)
#define CT_STATUS_CLKCNTFLG_Msk (0x1U << CT_STATUS_CLKCNTFLG_Pos)                       /*!< 0x20000000 */
#define CT_STATUS_CLKCNTFLG     CT_STATUS_CLKCNTFLG_Msk                                 /*!<Clk_cnt_flag */

#define CT_STATUS_ETUCNTFLG_Pos (30U)
#define CT_STATUS_ETUCNTFLG_Msk (0x1U << CT_STATUS_ETUCNTFLG_Pos)                       /*!< 0x40000000 */
#define CT_STATUS_ETUCNTFLG     CT_STATUS_ETUCNTFLG_Msk                                 /*!<Etu_cnt_flag */

#define CT_STATUS_TXFOPRYERR_Pos    (31U)
#define CT_STATUS_TXFOPRYERR_Msk    (0x1U << CT_STATUS_TXFOPRYERR_Pos)                  /*!< 0x80000000 */
#define CT_STATUS_TXFOPRYERR        CT_STATUS_TXFOPRYERR_Msk                            /*!<tx_fifo_parity_err */

/*******************   Bit definition for CT_FFSTATUS register   ********************/
#define CT_FFSTATUS_TXFFSTATS_Pos   (0U)
#define CT_FFSTATUS_TXFFSTATS_Msk   (0xFU << CT_FFSTATUS_TXFFSTATS_Pos)                 /*!< 0x0000000F */
#define CT_FFSTATUS_TXFFSTATS       CT_FFSTATUS_TXFFSTATS_Msk                           /*!<Tx_fifo_status[3:0] */

#define CT_FFSTATUS_TXFFEMPTY_Pos   (8U)
#define CT_FFSTATUS_TXFFEMPTY_Msk   (0x1U << CT_FFSTATUS_TXFFEMPTY_Pos)                 /*!< 0x00000100 */
#define CT_FFSTATUS_TXFFEMPTY       CT_FFSTATUS_TXFFEMPTY_Msk                           /*!<Tx_fifo_empty */

#define CT_FFSTATUS_TXFFFULL_Pos    (9U)
#define CT_FFSTATUS_TXFFFULL_Msk    (0x1U << CT_FFSTATUS_TXFFFULL_Pos)                  /*!< 0x00000200 */
#define CT_FFSTATUS_TXFFFULL        CT_FFSTATUS_TXFFFULL_Msk                            /*!<Tx_fifo_full */

#define CT_FFSTATUS_RXFFSTATS_Pos   (16U)
#define CT_FFSTATUS_RXFFSTATS_Msk   (0xFU << CT_FFSTATUS_RXFFSTATS_Pos)                 /*!< 0x000F0000 */
#define CT_FFSTATUS_RXFFSTATS       CT_FFSTATUS_RXFFSTATS_Msk                           /*!<Rx_fifo_status[3:0] */

#define CT_FFSTATUS_RXFFEMPTY_Pos   (24U)
#define CT_FFSTATUS_RXFFEMPTY_Msk   (0x1U << CT_FFSTATUS_RXFFEMPTY_Pos)                 /*!< 0x01000000 */
#define CT_FFSTATUS_RXFFEMPTY       CT_FFSTATUS_RXFFEMPTY_Msk                           /*!<Rx_fifo_empty */

#define CT_FFSTATUS_RXFFFULL_Pos    (25U)
#define CT_FFSTATUS_RXFFFULL_Msk    (0x1U << CT_FFSTATUS_RXFFFULL_Pos)                  /*!< 0x02000000 */
#define CT_FFSTATUS_RXFFFULL        CT_FFSTATUS_RXFFFULL_Msk                            /*!<Rx_fifo_full */

/*******************   Bit definition for CT_ETUCNT register   ********************/
#define CT_ETUCNT_ETUCNT_Pos    (0U)
#define CT_ETUCNT_ETUCNT_Msk    (0xFFFFU << CT_ETUCNT_ETUCNT_Pos)                       /*!< 0x0000FFFF */
#define CT_ETUCNT_ETUCNT        CT_ETUCNT_ETUCNT_Msk                                    /*!<etu_count[15:0] */

/*******************   Bit definition for CT_RXETUCNT register   ********************/
#define CT_RXETUCNT_RXETUCNT_Pos    (0U)
#define CT_RXETUCNT_RXETUCNT_Msk    (0xFFFFFFFFU << CT_RXETUCNT_RXETUCNT_Pos)           /*!< 0xFFFFFFFF */
#define CT_RXETUCNT_RXETUCNT        CT_RXETUCNT_RXETUCNT_Msk                            /*!<Rx_etu_count[31:0] */

/*******************   Bit definition for CT_TXETUCNT register   ********************/
#define CT_TXETUCNT_TXETUCNT_Pos    (0U)
#define CT_TXETUCNT_TXETUCNT_Msk    (0x0000FFFFU << CT_TXETUCNT_TXETUCNT_Pos)           /*!< 0x0000FFFF */
#define CT_TXETUCNT_TXETUCNT        CT_TXETUCNT_TXETUCNT_Msk                            /*!<tx_etu_count[15:0] */

/*******************   Bit definition for CT_CNT register   ********************/
#define CT_CNT_CLKCNT_Pos   (0U)
#define CT_CNT_CLKCNT_Msk   (0x0000FFFFU << CT_CNT_CLKCNT_Pos)                          /*!< 0x0000FFFF */
#define CT_CNT_CLKCNT       CT_CNT_CLKCNT_Msk                                           /*!<ct_clk_count[15:0] */
//</h>
//<h>DAC
/******************************************************************************/
/*                                                                            */
/*                                    DAC_CSR                                 */
/*                                                                            */
/******************************************************************************/
/*******************   Bit definition for DAC_CTRL register   ********************/
#define DAC_CTRL_DACEN_Pos  (0U)
#define DAC_CTRL_DACEN_Msk  (0x1U << DAC_CTRL_DACEN_Pos)                                /*!< 0x00000001 */
#define DAC_CTRL_DACEN      DAC_CTRL_DACEN_Msk                                          /*!<dac_en */

/*******************   Bit definition for DAC_CFG register   ********************/
#define DAC_CFG_VSEL_Pos    (0U)
#define DAC_CFG_VSEL_Msk    (0x1U << DAC_CFG_VSEL_Pos)                                  /*!< 0x00000001 */
#define DAC_CFG_VSEL        DAC_CFG_VSEL_Msk                                            /*!<vref_sel */

#define DAC_CFG_HPEN_Pos    (1U)
#define DAC_CFG_HPEN_Msk    (0x1U << DAC_CFG_HPEN_Pos)                                  /*!< 0x00000002 */
#define DAC_CFG_HPEN        DAC_CFG_HPEN_Msk                                            /*!<high_power_en */

/*******************   Bit definition for DAC_WORK register   ********************/
#define DAC_WORK_PTRM_Pos   (0U)
#define DAC_WORK_PTRM_Msk   (0x3U << DAC_WORK_PTRM_Pos)                                 /*!< 0x00000003 */
#define DAC_WORK_PTRM       DAC_WORK_PTRM_Msk                                           /*!<pointer_mode[1:0] */

#define DAC_WORK_SCANME_Pos (2U)
#define DAC_WORK_SCANME_Msk (0x1U << DAC_WORK_SCANME_Pos)                               /*!< 0x00000004 */
#define DAC_WORK_SCANME     DAC_WORK_SCANME_Msk                                         /*!<scan_mode_en */

#define DAC_WORK_SCANOS_Pos (3U)
#define DAC_WORK_SCANOS_Msk (0x1U << DAC_WORK_SCANOS_Pos)                               /*!< 0x00000008 */
#define DAC_WORK_SCANOS     DAC_WORK_SCANOS_Msk                                         /*!<scan_one_shot */

#define DAC_WORK_SCANINVL_Pos   (8U)
#define DAC_WORK_SCANINVL_Msk   (0xFFFFU << DAC_WORK_SCANINVL_Pos)                      /*!< 0x00FFFF00 */
#define DAC_WORK_SCANINVL       DAC_WORK_SCANINVL_Msk                                   /*!<scan_interval[15:0] */

/*******************   Bit definition for DAC_TRIG register   ********************/
#define DAC_TRIG_TSC_Pos    (0U)
#define DAC_TRIG_TSC_Msk    (0xFU << DAC_TRIG_TSC_Pos)                                  /*!< 0x0000000F */
#define DAC_TRIG_TSC        DAC_TRIG_TSC_Msk                                            /*!<trig_sc[3:0] */

#define DAC_TRIG_TINV_Pos   (4U)
#define DAC_TRIG_TINV_Msk   (0x1U << DAC_TRIG_TINV_Pos)                                 /*!< 0x00000010 */
#define DAC_TRIG_TINV       DAC_TRIG_TINV_Msk                                           /*!<trig_inv */

#define DAC_TRIG_TEDGE_Pos  (5U)
#define DAC_TRIG_TEDGE_Msk  (0x3U << DAC_TRIG_TEDGE_Pos)                                /*!< 0x00000060 */
#define DAC_TRIG_TEDGE      DAC_TRIG_TEDGE_Msk                                          /*!<trig_edge[1:0] */

#define DAC_TRIG_TFILT_Pos  (8U)
#define DAC_TRIG_TFILT_Msk  (0xFU << DAC_TRIG_TFILT_Pos)                                /*!< 0x00000F00 */
#define DAC_TRIG_TFILT      DAC_TRIG_TFILT_Msk                                          /*!<trig_filt[3:0] */

/*******************   Bit definition for DAC_SWTRIG register   ********************/
#define DAC_SWTRIG_CTRL_Pos (0U)
#define DAC_SWTRIG_CTRL_Msk (0x1U << DAC_SWTRIG_CTRL_Pos)                               /*!< 0x00000001 */
#define DAC_SWTRIG_CTRL     DAC_SWTRIG_CTRL_Msk                                         /*!<sw_trig_ctrl */

/*******************   Bit definition for DAC_DPTR register   ********************/
#define DAC_DPTR_DPTR_Pos   (0U)
#define DAC_DPTR_DPTR_Msk   (0xFU << DAC_DPTR_DPTR_Pos)                                 /*!< 0x0000000F */
#define DAC_DPTR_DPTR       DAC_DPTR_DPTR_Msk                                           /*!<data_pointer[3:0] */

/*******************   Bit definition for DAC_DPCFG register   ********************/
#define DAC_DPCFG_TOP_Pos   (0U)
#define DAC_DPCFG_TOP_Msk   (0x7U << DAC_DPCFG_TOP_Pos)                                 /*!< 0x00000007 */
#define DAC_DPCFG_TOP       DAC_DPCFG_TOP_Msk                                           /*!<top[2:0] */

#define DAC_DPCFG_WMK_Pos   (4U)
#define DAC_DPCFG_WMK_Msk   (0x7U << DAC_DPCFG_WMK_Pos)                                 /*!< 0x00000070 */
#define DAC_DPCFG_WMK       DAC_DPCFG_WMK_Msk                                           /*!<watermark[2:0] */

/*******************   Bit definition for DAC_DATAN register   ********************/    //N=0-7
#define DAC_DATAN_DATA_Pos  (0U)
#define DAC_DATAN_DATA_Msk  (0xFFFU << DAC_DATAN_DATA_Pos)                              /*!< 0x00000FFF */
#define DAC_DATAN_DATA      DAC_DATAN_DATA_Msk                                          /*!<data[11:0] */

#define DAC_DATAN_COUNT 8                                                               //DAC_DATA寄存器数

/*******************   Bit definition for DAC_FLAG register   ********************/
#define DAC_FLAG_BOTTOMF_Pos    (0U)
#define DAC_FLAG_BOTTOMF_Msk    (0x1U << DAC_FLAG_BOTTOMF_Pos)                          /*!< 0x00000001 */
#define DAC_FLAG_BOTTOMF        DAC_FLAG_BOTTOMF_Msk                                    /*!<bottom_flag */

#define DAC_FLAG_TOPF_Pos   (1U)
#define DAC_FLAG_TOPF_Msk   (0x1U << DAC_FLAG_TOPF_Pos)                                 /*!< 0x00000002 */
#define DAC_FLAG_TOPF       DAC_FLAG_TOPF_Msk                                           /*!<top_flag */

#define DAC_FLAG_WMKF_Pos   (2U)
#define DAC_FLAG_WMKF_Msk   (0x1U << DAC_FLAG_WMKF_Pos)                                 /*!< 0x00000004 */
#define DAC_FLAG_WMKF       DAC_FLAG_WMKF_Msk                                           /*!<watermark_flag */

/*******************   Bit definition for DAC_INTEN register   ********************/
#define DAC_INTEN_BTMIDE_Pos    (0U)
#define DAC_INTEN_BTMIDE_Msk    (0x1U << DAC_INTEN_BTMIDE_Pos)                          /*!< 0x00000001 */
#define DAC_INTEN_BTMIDE        DAC_INTEN_BTMIDE_Msk                                    /*!<bottom_int_dma_en */

#define DAC_INTEN_TOPIDE_Pos    (1U)
#define DAC_INTEN_TOPIDE_Msk    (0x1U << DAC_INTEN_TOPIDE_Pos)                          /*!< 0x00000002 */
#define DAC_INTEN_TOPIDE        DAC_INTEN_TOPIDE_Msk                                    /*!<top_int_dma_en */

#define DAC_INTEN_WMKIDE_Pos    (2U)
#define DAC_INTEN_WMKIDE_Msk    (0x1U << DAC_INTEN_WMKIDE_Pos)                          /*!< 0x00000004 */
#define DAC_INTEN_WMKIDE        DAC_INTEN_WMKIDE_Msk                                    /*!<watermark_int_dma_en */

#define DAC_INTEN_BTMDMAS_Pos   (4U)
#define DAC_INTEN_BTMDMAS_Msk   (0x1U << DAC_INTEN_BTMDMAS_Pos)                         /*!< 0x00000010 */
#define DAC_INTEN_BTMDMAS       DAC_INTEN_BTMDMAS_Msk                                   /*!<bottom_dma_sel */

#define DAC_INTEN_TOPDMAS_Pos   (5U)
#define DAC_INTEN_TOPDMAS_Msk   (0x1U << DAC_INTEN_TOPDMAS_Pos)                         /*!< 0x00000020 */
#define DAC_INTEN_TOPDMAS       DAC_INTEN_TOPDMAS_Msk                                   /*!<top_dma_sel */

#define DAC_INTEN_WMKDMAS_Pos   (6U)
#define DAC_INTEN_WMKDMAS_Msk   (0x1U << DAC_INTEN_WMKDMAS_Pos)                         /*!< 0x00000040 */
#define DAC_INTEN_WMKDMAS       DAC_INTEN_WMKDMAS_Msk                                   /*!<watermark_dma_sel */

/*******************   Bit definition for DAC_INT register   ********************/
#define DAC_INT_BTMINT_Pos  (0U)
#define DAC_INT_BTMINT_Msk  (0x1U << DAC_INT_BTMINT_Pos)                                /*!< 0x00000001 */
#define DAC_INT_BTMINT      DAC_INT_BTMINT_Msk                                          /*!<bottom_int */

#define DAC_INT_TOPINT_Pos  (1U)
#define DAC_INT_TOPINT_Msk  (0x1U << DAC_INT_TOPINT_Pos)                                /*!< 0x00000002 */
#define DAC_INT_TOPINT      DAC_INT_TOPINT_Msk                                          /*!<top_int */

#define DAC_INT_WMKINT_Pos  (2U)
#define DAC_INT_WMKINT_Msk  (0x1U << DAC_INT_WMKINT_Pos)                                /*!< 0x00000004 */
#define DAC_INT_WMKINT      DAC_INT_WMKINT_Msk                                          /*!<watermark_int */

/*******************   Bit definition for DAC_IFCL register   ********************/
#define DAC_IFCL_BTMICL_Pos (0U)
#define DAC_IFCL_BTMICL_Msk (0x1U << DAC_IFCL_BTMICL_Pos)                               /*!< 0x00000001 */
#define DAC_IFCL_BTMICL     DAC_IFCL_BTMICL_Msk                                         /*!<bottom_icl */

#define DAC_IFCL_TOPICL_Pos (1U)
#define DAC_IFCL_TOPICL_Msk (0x1U << DAC_IFCL_TOPICL_Pos)                               /*!< 0x00000002 */
#define DAC_IFCL_TOPICL     DAC_IFCL_TOPICL_Msk                                         /*!<top_icl */

#define DAC_IFCL_WMKICL_Pos (2U)
#define DAC_IFCL_WMKICL_Msk (0x1U << DAC_IFCL_WMKICL_Pos)                               /*!< 0x00000004 */
#define DAC_IFCL_WMKICL     DAC_IFCL_WMKICL_Msk                                         /*!<watermark_icl */
//</h>
//<h>DCMI
/******************************************************************************/
/*                                                                            */
/*                                    DCMI                                     */
/*                                                                            */
/******************************************************************************/
/*******************  Bit definition for DCMI_CFG register  *******************/
#define DCMI_CFG_CAPTURE_Pos    (0U)
#define DCMI_CFG_CAPTURE_Msk    (0x1U << DCMI_CFG_CAPTURE_Pos)                      /*!< 0x00000001 */
#define DCMI_CFG_CAPTURE        DCMI_CFG_CAPTURE_Msk                                /*!<capture */

#define DCMI_CFG_CM_Pos (1U)
#define DCMI_CFG_CM_Msk (0x1U << DCMI_CFG_CM_Pos)                                   /*!< 0x00000002 */
#define DCMI_CFG_CM     DCMI_CFG_CM_Msk                                             /*!<capture mode */

#define DCMI_CFG_CROP_Pos   (2U)
#define DCMI_CFG_CROP_Msk   (0x1U << DCMI_CFG_CROP_Pos)                             /*!< 0x00000004 */
#define DCMI_CFG_CROP       DCMI_CFG_CROP_Msk                                       /*!<crop */

#define DCMI_CFG_JPEG_Pos   (3U)
#define DCMI_CFG_JPEG_Msk   (0x1U << DCMI_CFG_JPEG_Pos)                             /*!< 0x00000008 */
#define DCMI_CFG_JPEG       DCMI_CFG_JPEG_Msk                                       /*!<jpeg */

#define DCMI_CFG_ESS_Pos    (4U)
#define DCMI_CFG_ESS_Msk    (0x1U << DCMI_CFG_ESS_Pos)                              /*!< 0x00000010 */
#define DCMI_CFG_ESS        DCMI_CFG_ESS_Msk                                        /*!<embedded sel */

#define DCMI_CFG_PCKPOR_Pos (5U)
#define DCMI_CFG_PCKPOR_Msk (0x1U << DCMI_CFG_PCKPOR_Pos)                           /*!< 0x00000020 */
#define DCMI_CFG_PCKOR      DCMI_CFG_PCKPOR_Msk                                     /*!<pck por */

#define DCMI_CFG_HSPOL_Pos  (6U)
#define DCMI_CFG_HSPOL_Msk  (0x1U << DCMI_CFG_HSPOL_Pos)                            /*!< 0x00000040 */
#define DCMI_CFG_HSPOL      DCMI_CFG_HSPOL_Msk                                      /*!<hsync_por */

#define DCMI_CFG_VSPOL_Pos  (7U)
#define DCMI_CFG_VSPOL_Msk  (0x1U << DCMI_CFG_VSPOL_Pos)                            /*!< 0x00000080 */
#define DCMI_CFG_VSPOL      DCMI_CFG_VSPOL_Msk                                      /*!<vsync_por */

#define DCMI_CFG_FCRC_Pos   (8U)
#define DCMI_CFG_FCRC_Msk   (0x3U << DCMI_CFG_FCRC_Pos)                             /*!< 0x00000300 */
#define DCMI_CFG_FCRC       DCMI_CFG_FCRC_Msk                                       /*!<capture_rate[1:0] */

#define DCMI_CFG_DWM_Pos    (10U)
#define DCMI_CFG_DWM_Msk    (0x3U << DCMI_CFG_DWM_Pos)                              /*!< 0x00000C00 */
#define DCMI_CFG_DWM        DCMI_CFG_DWM_Msk                                        /*!<data_width_mode[1:0] */

#define DCMI_CFG_ENABLE_Pos (14U)
#define DCMI_CFG_ENABLE_Msk (0x1U << DCMI_CFG_ENABLE_Pos)                           /*!< 0x00004000 */
#define DCMI_CFG_ENABLE     DCMI_CFG_ENABLE_Msk                                     /*!<enable */

#define DCMI_CFG_BSELMODE_Pos   (16U)
#define DCMI_CFG_BSELMODE_Msk   (0x3U << DCMI_CFG_BSELMODE_Pos)                     /*!< 0x00030000 */
#define DCMI_CFG_BSELMODE       DCMI_CFG_BSELMODE_Msk                               /*!<byte_sel_mode[1:0] */

#define DCMI_CFG_BSEL_Pos   (18U)
#define DCMI_CFG_BSEL_Msk   (0x1U << DCMI_CFG_BSEL_Pos)                             /*!< 0x00040000 */
#define DCMI_CFG_BSEL       DCMI_CFG_BSEL_Msk                                       /*!<byte_sel */

#define DCMI_CFG_LSELMODE_Pos   (19U)
#define DCMI_CFG_LSELMODE_Msk   (0x1U << DCMI_CFG_LSELMODE_Pos)                     /*!< 0x00080000 */
#define DCMI_CFG_LSELMODE       DCMI_CFG_LSELMODE_Msk                               /*!<line_sel_mode */

#define DCMI_CFG_LSEL_Pos   (20U)
#define DCMI_CFG_LSEL_Msk   (0x1U << DCMI_CFG_LSEL_Pos)                             /*!< 0x00100000 */
#define DCMI_CFG_LSEL       DCMI_CFG_LSEL_Msk                                       /*!<line_sel */

#define DCMI_CFG_SHIFTNUM_Pos   (24U)
#define DCMI_CFG_SHIFTNUM_Msk   (0x7U << DCMI_CFG_SHIFTNUM_Pos)                     /*!< 0x07000000 */
#define DCMI_CFG_SHIFTNUM       DCMI_CFG_SHIFTNUM_Msk                               /*!<shift_num[2:0] */

#define DCMI_CFG_MAPEN_Pos  (27U)
#define DCMI_CFG_MAPEN_Msk  (0x1U << DCMI_CFG_MAPEN_Pos)                            /*!< 0x08000001 */
#define DCMI_CFG_MAPEN      DCMI_CFG_MAPEN_Msk                                      /*!<map_en */

#define DCMI_CFG_SYNCL_Pos  (28U)
#define DCMI_CFG_SYNCL_Msk  (0x7U << DCMI_CFG_SYNCL_Pos)                            /*!< 0x70000000 */
#define DCMI_CFG_SYNCL      DCMI_CFG_SYNCL_Msk                                      /*!<sync_limit[2:0] */

#define DCMI_CFG_SYNCEDGESEL_Pos    (31U)
#define DCMI_CFG_SYNCEDGESEL_Msk    (0x1U << DCMI_CFG_SYNCEDGESEL_Pos)              /*!< 0x80000000 */
#define DCMI_CFG_SYNCEDGESEL        DCMI_CFG_SYNCEDGESEL_Msk                        /*!<sync_edge_sel */

/*******************  Bit definition for DCMI_STATUS register  *******************/
#define DCMI_STATUS_HSYNC_Pos   (0U)
#define DCMI_STATUS_HSYNC_Msk   (0x1U << DCMI_STATUS_HSYNC_Pos)                     /*!< 0x00000001 */
#define DCMI_STATUS_HSYNC       DCMI_STATUS_HSYNC_Msk                               /*!<hsync */

#define DCMI_STATUS_VSYNC_Pos   (1U)
#define DCMI_STATUS_VSYNC_Msk   (0x1U << DCMI_STATUS_VSYNC_Pos)                     /*!< 0x00000002 */
#define DCMI_STATUS_VSYNC       DCMI_STATUS_VSYNC_Msk                               /*!<vsync */

#define DCMI_STATUS_FIFONE_Pos  (2U)
#define DCMI_STATUS_FIFONE_Msk  (0x1U << DCMI_STATUS_FIFONE_Pos)                    /*!< 0x00000004 */
#define DCMI_STATUS_FIFONE      DCMI_STATUS_FIFONE_Msk                              /*!<fifo not empty */

/*******************  Bit definition for DCMI_INT_RAW_STATUS register  *******************/
#define DCMI_INTRAWSTA_FRAME_Pos    (0U)
#define DCMI_INTRAWSTA_FRAME_Msk    (0x1U << DCMI_INTRAWSTA_FRAME_Pos)              /*!< 0x00000001 */
#define DCMI_INTRAWSTA_FRAME        DCMI_INTRAWSTA_FRAME_Msk                        /*!<frame_ris */

#define DCMI_INTRAWSTA_OVERR_Pos    (1U)
#define DCMI_INTRAWSTA_OVERR_Msk    (0x1U << DCMI_INTRAWSTA_OVERR_Pos)              /*!< 0x00000002 */
#define DCMI_INTRAWSTA_OVERR        DCMI_INTRAWSTA_OVERR_Msk                        /*!<overrun_ris */

#define DCMI_INTRAWSTA_ERR_Pos  (2U)
#define DCMI_INTRAWSTA_ERR_Msk  (0x1U << DCMI_INTRAWSTA_ERR_Pos)                    /*!< 0x00000004 */
#define DCMI_INTRAWSTA_ERR      DCMI_INTRAWSTA_ERR_Msk                              /*!<err_ris */

#define DCMI_INTRAWSTA_VSYNC_Pos    (3U)
#define DCMI_INTRAWSTA_VSYNC_Msk    (0x1U << DCMI_INTRAWSTA_VSYNC_Pos)              /*!< 0x00000008 */
#define DCMI_INTRAWSTA_VSYNC        DCMI_INTRAWSTA_VSYNC_Msk                        /*!<vsync_ris */

#define DCMI_INTRAWSTA_LINE_Pos (4U)
#define DCMI_INTRAWSTA_LINE_Msk (0x1U << DCMI_INTRAWSTA_LINE_Pos)                   /*!< 0x00000010 */
#define DCMI_INTRAWSTA_LINE     DCMI_INTRAWSTA_LINE_Msk                             /*!<line_ris */

/*******************  Bit definition for DCMI_INT_EN register  *******************/
#define DCMI_INTEN_FRAME_Pos    (0U)
#define DCMI_INTEN_FRAME_Msk    (0x1U << DCMI_INTEN_FRAME_Pos)                      /*!< 0x00000001 */
#define DCMI_INTEN_FRAME        DCMI_INTEN_FRAME_Msk                                /*!<frame_ris_int_en */

#define DCMI_INTEN_OVERR_Pos    (1U)
#define DCMI_INTEN_OVERR_Msk    (0x1U << DCMI_INTEN_OVERR_Pos)                      /*!< 0x00000002 */
#define DCMI_INTEN_OVERR        DCMI_INTEN_OVERR_Msk                                /*!<overrun_ris_int_en */

#define DCMI_INTEN_ERR_Pos  (2U)
#define DCMI_INTEN_ERR_Msk  (0x1U << DCMI_INTEN_ERR_Pos)                            /*!< 0x00000004 */
#define DCMI_INTEN_ERR      DCMI_INTEN_ERR_Msk                                      /*!<err_ris_int_en */

#define DCMI_INTEN_VSYNC_Pos    (3U)
#define DCMI_INTEN_VSYNC_Msk    (0x1U << DCMI_INTEN_VSYNC_Pos)                      /*!< 0x00000008 */
#define DCMI_INTEN_VSYNC        DCMI_INTEN_VSYNC_Msk                                /*!<vsync_ris_int_en */

#define DCMI_INTEN_LINE_Pos (4U)
#define DCMI_INTEN_LINE_Msk (0x1U << DCMI_INTEN_LINE_Pos)                           /*!< 0x00000010 */
#define DCMI_INTEN_LINE     DCMI_INTEN_LINE_Msk                                     /*!<line_ris_int_en */

/*******************  Bit definition for DCMI_INT_MASKED_STATUS register  *******************/
#define DCMI_INTMSKSTA_FRAME_Pos    (0U)
#define DCMI_INTMSKSTA_FRAME_Msk    (0x1U << DCMI_INTMSKSTA_FRAME_Pos)              /*!< 0x00000001 */
#define DCMI_INTMSKSTA_FRAME        DCMI_INTMSKSTA_FRAME_Msk                        /*!<frame_mris */

#define DCMI_INTMSKSTA_OVERR_Pos    (1U)
#define DCMI_INTMSKSTA_OVERR_Msk    (0x1U << DCMI_INTMSKSTA_OVERR_Pos)              /*!< 0x00000002 */
#define DCMI_INTMSKSTA_OVERR        DCMI_INTMSKSTA_OVERR_Msk                        /*!<overrun_mris */

#define DCMI_INTMSKSTA_ERR_Pos  (2U)
#define DCMI_INTMSKSTA_ERR_Msk  (0x1U << DCMI_INTMSKSTA_ERR_Pos)                    /*!< 0x00000004 */
#define DCMI_INTMSKSTA_ERR      DCMI_INTMSKSTA_ERR_Msk                              /*!<err_mris */

#define DCMI_INTMSKSTA_VSYNC_Pos    (3U)
#define DCMI_INTMSKSTA_VSYNC_Msk    (0x1U << DCMI_INTMSKSTA_VSYNC_Pos)              /*!< 0x00000008 */
#define DCMI_INTMSKSTA_VSYNC        DCMI_INTMSKSTA_VSYNC_Msk                        /*!<vsync_mris */

#define DCMI_INTMSKSTA_LINE_Pos (4U)
#define DCMI_INTMSKSTA_LINE_Msk (0x1U << DCMI_INTMSKSTA_LINE_Pos)                   /*!< 0x00000010 */
#define DCMI_INTMSKSTA_LINE     DCMI_INTMSKSTA_LINE_Msk                             /*!<line_mris */

/*******************  Bit definition for DCMI_INT_CLR register  *******************/
#define DCMI_CLR_FRAME_Pos  (0U)
#define DCMI_CLR_FRAME_Msk  (0x1U << DCMI_CLR_FRAME_Pos)                            /*!< 0x00000001 */
#define DCMI_CLR_FRAME      DCMI_CLR_FRAME_Msk                                      /*!<frame_ris_clr */

#define DCMI_CLR_OVERR_Pos  (1U)
#define DCMI_CLR_OVERR_Msk  (0x1U << DCMI_CLR_OVERR_Pos)                            /*!< 0x00000002 */
#define DCMI_CLR_OVERR      DCMI_CLR_OVERR_Msk                                      /*!<overrun_ris_clr */

#define DCMI_CLR_ERR_Pos    (2U)
#define DCMI_CLR_ERR_Msk    (0x1U << DCMI_CLR_ERR_Pos)                              /*!< 0x00000004 */
#define DCMI_CLR_ERR        DCMI_CLR_ERR_Msk                                        /*!<err_ris_clr */

#define DCMI_CLR_VSYNC_Pos  (3U)
#define DCMI_CLR_VSYNC_Msk  (0x1U << DCMI_CLR_VSYNC_Pos)                            /*!< 0x00000008 */
#define DCMI_CLR_VSYNC      DCMI_CLR_VSYNC_Msk                                      /*!<vsync_ris_clr */

#define DCMI_CLR_LINE_Pos   (4U)
#define DCMI_CLR_LINE_Msk   (0x1U << DCMI_CLR_LINE_Pos)                             /*!< 0x00000010 */
#define DCMI_CLR_LINE       DCMI_CLR_LINE_Msk                                       /*!<line_ris_clr */

/*******************  Bit definition for DCMI_EMBEDDED_CODE_CFG register  *******************/
#define DCMI_ECODECFG_FRAMES_Pos    (0U)
#define DCMI_ECODECFG_FRAMES_Msk    (0xFFU << DCMI_ECODECFG_FRAMES_Pos)             /*!< 0x000000FF */
#define DCMI_ECODECFG_FRAMES        DCMI_ECODECFG_FRAMES_Msk                        /*!<frame_start_code[7:0] */

#define DCMI_ECODECFG_LINES_Pos (8U)
#define DCMI_ECODECFG_LINES_Msk (0xFFU << DCMI_ECODECFG_LINES_Pos)                  /*!< 0x0000FF00 */
#define DCMI_ECODECFG_LINES     DCMI_ECODECFG_LINES_Msk                             /*!<line_start_code[7:0] */

#define DCMI_ECODECFG_FRAMEE_Pos    (16U)
#define DCMI_ECODECFG_FRAMEE_Msk    (0xFFU << DCMI_ECODECFG_FRAMEE_Pos)             /*!< 0x00FF0000 */
#define DCMI_ECODECFG_FRAMEE        DCMI_ECODECFG_FRAMEE_Msk                        /*!<line_end_code[7:0] */

#define DCMI_ECODECFG_LINEE_Pos (24U)
#define DCMI_ECODECFG_LINEE_Msk (0xFFU << DCMI_ECODECFG_LINEE_Pos)                  /*!< 0x000000FF */
#define DCMI_ECODECFG_LINEE     DCMI_ECODECFG_LINEE_Msk                             /*!<frame_end_code[7:0] */

/*******************  Bit definition for DCMI_EMBEDDED_UNMASK_CFG register  *******************/
#define DCMI_EUMSKCFG_FRAMES_Pos    (0U)
#define DCMI_EUMSKCFG_FRAMES_Msk    (0xFFU << DCMI_EUMSKCFG_FRAMES_Pos)             /*!< 0x000000FF */
#define DCMI_EUMSKCFG_FRAMES        DCMI_EUMSKCFG_FRAMES_Msk                        /*!<frame_start_unmask[7:0] */

#define DCMI_EUMSKCFG_LINES_Pos (8U)
#define DCMI_EUMSKCFG_LINES_Msk (0xFFU << DCMI_EUMSKCFG_LINES_Pos)                  /*!< 0x0000FF00 */
#define DCMI_EUMSKCFG_LINES     DCMI_EUMSKCFG_LINES_Msk                             /*!<line_start_unmask[7:0] */

#define DCMI_EUMSKCFG_FRAMEE_Pos    (16U)
#define DCMI_EUMSKCFG_FRAMEE_Msk    (0xFFU << DCMI_EUMSKCFG_FRAMEE_Pos)             /*!< 0x00FF0000 */
#define DCMI_EUMSKCFG_FRAMEE        DCMI_EUMSKCFG_FRAMEE_Msk                        /*!<line_end_unmask[7:0] */

#define DCMI_EUMSKCFG_LINEE_Pos (24U)
#define DCMI_EUMSKCFG_LINEE_Msk (0xFFU << DCMI_EUMSKCFG_LINEE_Pos)                  /*!< 0x000000FF */
#define DCMI_EUMSKCFG_LINEE     DCMI_EUMSKCFG_LINEE_Msk                             /*!<frame_end_unmask[7:0] */

/*******************  Bit definition for DCMI_COR_WINDOW_START register  *******************/
#define DCMI_CWINSTART_HOFFSETCNT_Pos   (0U)
#define DCMI_CWINSTART_HOFFSETCNT_Msk   (0x3FFFU << DCMI_CWINSTART_HOFFSETCNT_Pos)  /*!< 0x00003FFF */
#define DCMI_CWINSTART_HOFFSETCNT       DCMI_CWINSTART_HOFFSETCNT_Msk               /*!<h_offset_cnt[13:0] */

#define DCMI_CWINSTART_VSTARTCNT_Pos    (16U)
#define DCMI_CWINSTART_VSTARTCNT_Msk    (0x1FFFU << DCMI_CWINSTART_VSTARTCNT_Pos)   /*!< 0x1FFF0000 */
#define DCMI_CWINSTART_VSTARTCNT        DCMI_CWINSTART_VSTARTCNT_Msk                /*!<v_line_start_cnt[12:0] */

/*******************  Bit definition for DCMI_COR_WINDOW_SIZE register  *******************/
#define DCMI_CWINSIZE_HCAP_Pos  (0U)
#define DCMI_CWINSIZE_HCAP_Msk  (0x3FFFU << DCMI_CWINSIZE_HCAP_Pos)                 /*!< 0x00003FFF */
#define DCMI_CWINSIZE_HCAP      DCMI_CWINSIZE_HCAP_Msk                              /*!<h_cap_size[13:0] */

#define DCMI_CWINSIZE_VLINE_Pos (16U)
#define DCMI_CWINSIZE_VLINE_Msk (0x3FFFU << DCMI_CWINSIZE_VLINE_Pos)                /*!< 0x3FFF0000 */
#define DCMI_CWINSIZE_VLINE     DCMI_CWINSIZE_VLINE_Msk                             /*!<v_line_size[13:0] */

/*******************  Bit definition for DCMI_FIFO register  *******************/
#define DCMI_FIFO_Pos   (0U)
#define DCMI_FIFO_Msk   (0xFFFFFFFFU << DCMI_FIFO_Pos)                              /*!< 0xFFFFFFFF */
#define DCMI_FIFO       DCMI_FIFO_Msk                                               /*!<dcmi_fifo[31:0] 8-word deepth FIFO*/

/*******************  Bit definition for DCMI_MAPBASE register  *******************/
#define DCMI_MAPBASE_0_Pos  (0U)
#define DCMI_MAPBASE_0_Msk  (0xFFU << DCMI_MAPBASE_0_Pos)                           /*!< 0x000000FF */
#define DCMI_MAPBASE_0      DCMI_MAPBASE_0_Msk                                      /*!<dcmi_map0_base[7:0] */

#define DCMI_MAPBASE_1_Pos  (0U)
#define DCMI_MAPBASE_1_Msk  (0xFFU << DCMI_MAPBASE_1_Pos)                           /*!< 0x0000FF00 */
#define DCMI_MAPBASE_1      DCMI_MAPBASE_1_Msk                                      /*!<dcmi_map1_base[7:0] */

#define DCMI_MAPBASE_2_Pos  (0U)
#define DCMI_MAPBASE_2_Msk  (0xFFU << DCMI_MAPBASE_2_Pos)                           /*!< 0x00FF0000 */
#define DCMI_MAPBASE_2      DCMI_MAPBASE_2_Msk                                      /*!<dcmi_map2_base[7:0] */

#define DCMI_MAPBASE_3_Pos  (0U)
#define DCMI_MAPBASE_3_Msk  (0xFFU << DCMI_MAPBASE_3_Pos)                           /*!< 0xFF000000 */
#define DCMI_MAPBASE_3      DCMI_MAPBASE_3_Msk                                      /*!<dcmi_map3_base[7:0] */

/*******************  Bit definition for DCMI_MAPn register  *******************/
#define DCMI_MAP_COFF_Pos   (0U)
#define DCMI_MAP_COFF_Msk   (0xFFFFU << DCMI_MAP_COFF_Pos)                          /*!< 0x0000FFFF */
#define DCMI_MAP_COFF       DCMI_MAP_COFF_Msk                                       /*!<dcmi_map_coff[15:0] */

#define DCMI_MAP_SADDR_Pos  (16U)
#define DCMI_MAP_SADDR_Msk  (0xFFU << DCMI_MAP_SADDR_Pos)                           /*!< 0x00FF0000 */
#define DCMI_MAP_SADDR      DCMI_MAP_SADDR_Msk                                      /*!<dcmi_map_saddr[7:0] */

#define DCMI_MAP_EADDR_Pos  (24U)
#define DCMI_MAP_EADDR_Msk  (0xFFU << DCMI_MAP_EADDR_Pos)                           /*!< 0xFFFF0000 */
#define DCMI_MAP_EADDR      DCMI_MAP_EADDR_Msk                                      /*!<dcmi_map_eaddr[7:0] */

#define DCMI_MAP_NUM 4
//</h>
//<h>DMA
/******************************************************************************/
/*                                                                            */
/*                                    DMA                                     */
/*                                                                            */
/******************************************************************************/
/*******************  Bit definition for DMA_CR register  *********************/
#define DMA_CR_ERCA_Pos (2U)
#define DMA_CR_ERCA_Msk (0x1U << DMA_CR_ERCA_Pos)                               /*!< 0x00000004 */
#define DMA_CR_ERCA     DMA_CR_ERCA_Msk                                         /*!<Enable poll arbitration */

#define DMA_CR_HOE_Pos  (4U)
#define DMA_CR_HOE_Msk  (0x1U << DMA_CR_HOE_Pos)                                /*!< 0x00000010 */
#define DMA_CR_HOE      DMA_CR_HOE_Msk                                          /*!<Halt on error */

#define DMA_CR_HALT_Pos (5U)
#define DMA_CR_HALT_Msk (0x1U << DMA_CR_HALT_Pos)                               /*!< 0x00000020 */
#define DMA_CR_HALT     DMA_CR_HALT_Msk                                         /*!<Halt DMA operations */

#define DMA_CR_CLM_Pos  (6U)
#define DMA_CR_CLM_Msk  (0x1U << DMA_CR_CLM_Pos)                                /*!< 0x00000040 */
#define DMA_CR_CLM      DMA_CR_CLM_Msk                                          /*!<Continuous Link Mode */

#define DMA_CR_EMLM_Pos (7U)
#define DMA_CR_EMLM_Msk (0x1U << DMA_CR_EMLM_Pos)                               /*!< 0x00000080 */
#define DMA_CR_EMLM     DMA_CR_EMLM_Msk                                         /*!<Enable Minor Loop Mapping */

#define DMA_CR_ECX_Pos  (16U)
#define DMA_CR_ECX_Msk  (0x1U << DMA_CR_ECX_Pos)                                /*!< 0x00010000 */
#define DMA_CR_ECX      DMA_CR_ECX_Msk                                          /*!<Error Cancel Transfer */

#define DMA_CR_CX_Pos   (17U)
#define DMA_CR_CX_Msk   (0x1U << DMA_CR_CX_Pos)                                 /*!< 0x00020000 */
#define DMA_CR_CX       DMA_CR_CX_Msk                                           /*!< Cancel Transfer */

/*******************  Bit definition for DMA_ES register  *********************/
#define DMA_ES_BE_Pos   (0U)
#define DMA_ES_BE_Msk   (0x1U << DMA_ES_BE_Pos)                                 /*!< 0x00000001 */
#define DMA_ES_BE       DMA_ES_BE_Msk                                           /*!< Bus error */

#define DMA_ES_SGE_Pos  (2U)
#define DMA_ES_SGE_Msk  (0x1U << DMA_ES_SGE_Pos)                                /*!< 0x00000004 */
#define DMA_ES_SGE      DMA_ES_SGE_Msk                                          /*!<Scatter/Gather Configuration Error */
#define DMA_ES_NCE_Pos  (3U)
#define DMA_ES_NCE_Msk  (0x1U << DMA_ES_NCE_Pos)                                /*!< 0x00000008 */
#define DMA_ES_NCE      DMA_ES_NCE_Msk                                          /*!<NBYTES/CITER Configuration Error */

#define DMA_ES_DOE_Pos  (4U)
#define DMA_ES_DOE_Msk  (0x1U << DMA_ES_DOE_Pos)                                /*!< 0x00000010 */
#define DMA_ES_DOE      DMA_ES_DOE_Msk                                          /*!< Destination Offset Error */
#define DMA_ES_DAE_Pos  (5U)
#define DMA_ES_DAE_Msk  (0x1U << DMA_ES_DAE_Pos)                                /*!< 0x00000020 */
#define DMA_ES_DAE      DMA_ES_DAE_Msk                                          /*!< Destination address Error */
#define DMA_ES_SOE_Pos  (6U)
#define DMA_ES_SOE_Msk  (0x1U << DMA_ES_SOE_Pos)                                /*!< 0x00000040 */
#define DMA_ES_SOE      DMA_ES_SOE_Msk                                          /*!< Source Offset Error */
#define DMA_ES_SAE_Pos  (7U)
#define DMA_ES_SAE_Msk  (0x1U << DMA_ES_SAE_Pos)                                /*!< 0x00000080 */
#define DMA_ES_SAE      DMA_ES_SAE_Msk                                          /*!< Source address Error */

#define DMA_ES_ERRCHN_Pos   (8U)
#define DMA_ES_ERRCHN_Msk   (0x7U << DMA_ES_ERRCHN_Pos)                         /*!< 0x00000700 */
#define DMA_ES_ERRCHN       DMA_ES_ERRCHN_Msk                                   /*!< ERRCHN[2:0],Error Channel Number or Canceled Channel Number */

#define DMA_ES_CPE_Pos  (14U)
#define DMA_ES_CPE_Msk  (0x1U << DMA_ES_CPE_Pos)                                /*!< 0x00004000 */
#define DMA_ES_CPE      DMA_ES_CPE_Msk                                          /*!< Channel Priority Error */

#define DMA_ES_ECX_Pos  (16U)
#define DMA_ES_ECX_Msk  (0x1U << DMA_ES_ECX_Pos)                                /*!< 0x00010000 */
#define DMA_ES_ECX      DMA_ES_ECX_Msk                                          /*!< Errror Cancel Transfer */

#define DMA_ES_VLD_Pos  (31U)
#define DMA_ES_VLD_Msk  (0x1U << DMA_ES_VLD_Pos)                                /*!< 0x80000000 */
#define DMA_ES_VLD      DMA_ES_VLD_Msk                                          /*!< Logical OR of all ERR status bits */

/*******************  Bit definition for DMA_ERQ register  *********************/
#define DMA_ERQ_ERQ0_Pos    (0U)
#define DMA_ERQ_ERQ0_Msk    (0x1U << DMA_ERQ_ERQ0_Pos)                          /*!< 0x00000001 */
#define DMA_ERQ_ERQ0        DMA_ERQ_ERQ0_Msk                                    /*!< Enable DMA Request 0 */

#define DMA_ERQ_ERQ1_Pos    (1U)
#define DMA_ERQ_ERQ1_Msk    (0x1U << DMA_ERQ_ERQ1_Pos)                          /*!< 0x00000002 */
#define DMA_ERQ_ERQ1        DMA_ERQ_ERQ1_Msk                                    /*!< Enable DMA Request 1 */

#define DMA_ERQ_ERQ2_Pos    (2U)
#define DMA_ERQ_ERQ2_Msk    (0x1U << DMA_ERQ_ERQ2_Pos)                          /*!< 0x00000004 */
#define DMA_ERQ_ERQ2        DMA_ERQ_ERQ2_Msk                                    /*!< Enable DMA Request 2 */

#define DMA_ERQ_ERQ3_Pos    (3U)
#define DMA_ERQ_ERQ3_Msk    (0x1U << DMA_ERQ_ERQ3_Pos)                          /*!< 0x00000008 */
#define DMA_ERQ_ERQ3        DMA_ERQ_ERQ3_Msk                                    /*!< Enable DMA Request 3 */

#define DMA_ERQ_ERQ4_Pos    (4U)
#define DMA_ERQ_ERQ4_Msk    (0x1U << DMA_ERQ_ERQ4_Pos)                          /*!< 0x00000010 */
#define DMA_ERQ_ERQ4        DMA_ERQ_ERQ4_Msk                                    /*!< Enable DMA Request 4 */

#define DMA_ERQ_ERQ5_Pos    (5U)
#define DMA_ERQ_ERQ5_Msk    (0x1U << DMA_ERQ_ERQ5_Pos)                          /*!< 0x00000020 */
#define DMA_ERQ_ERQ5        DMA_ERQ_ERQ5_Msk                                    /*!< Enable DMA Request 5 */

#define DMA_ERQ_ERQ6_Pos    (6U)
#define DMA_ERQ_ERQ6_Msk    (0x1U << DMA_ERQ_ERQ6_Pos)                          /*!< 0x00000040 */
#define DMA_ERQ_ERQ6        DMA_ERQ_ERQ6_Msk                                    /*!< Enable DMA Request 6 */

#define DMA_ERQ_ERQ7_Pos    (7U)
#define DMA_ERQ_ERQ7_Msk    (0x1U << DMA_ERQ_ERQ7_Pos)                          /*!< 0x00000080 */
#define DMA_ERQ_ERQ7        DMA_ERQ_ERQ7_Msk                                    /*!< Enable DMA Request 7 */

/*******************  Bit definition for DMA_EEI register  *********************/
#define DMA_EEI_EEI0_Pos    (0U)
#define DMA_EEI_EEI0_Msk    (0x1U << DMA_EEI_EEI0_Pos)                          /*!< 0x00000001 */
#define DMA_EEI_EEI0        DMA_EEI_EEI0_Msk                                    /*!< Enable Error Interrupt 0 */

#define DMA_EEI_EEI1_Pos    (1U)
#define DMA_EEI_EEI1_Msk    (0x1U << DMA_EEI_EEI1_Pos)                          /*!< 0x00000002 */
#define DMA_EEI_EEI1        DMA_EEI_EEI1_Msk                                    /*!< Enable Error Interrupt 1 */

#define DMA_EEI_EEI2_Pos    (2U)
#define DMA_EEI_EEI2_Msk    (0x1U << DMA_EEI_EEI2_Pos)                          /*!< 0x00000004 */
#define DMA_EEI_EEI2        DMA_EEI_EEI2_Msk                                    /*!< Enable Error Interrupt 2 */

#define DMA_EEI_EEI3_Pos    (3U)
#define DMA_EEI_EEI3_Msk    (0x1U << DMA_EEI_EEI3_Pos)                          /*!< 0x00000008 */
#define DMA_EEI_EEI3        DMA_EEI_EEI3_Msk                                    /*!< Enable Error Interrupt 3 */

#define DMA_EEI_EEI4_Pos    (4U)
#define DMA_EEI_EEI4_Msk    (0x1U << DMA_EEI_EEI4_Pos)                          /*!< 0x00000010 */
#define DMA_EEI_EEI4        DMA_EEI_EEI4_Msk                                    /*!< Enable Error Interrupt 4 */

#define DMA_EEI_EEI5_Pos    (5U)
#define DMA_EEI_EEI5_Msk    (0x1U << DMA_EEI_EEI5_Pos)                          /*!< 0x00000020 */
#define DMA_EEI_EEI5        DMA_EEI_EEI5_Msk                                    /*!< Enable Error Interrupt 5 */

#define DMA_EEI_EEI6_Pos    (6U)
#define DMA_EEI_EEI6_Msk    (0x1U << DMA_EEI_EEI6_Pos)                          /*!< 0x00000040 */
#define DMA_EEI_EEI6        DMA_EEI_EEI6_Msk                                    /*!< Enable Error Interrupt 6 */

#define DMA_EEI_EEI7_Pos    (7U)
#define DMA_EEI_EEI7_Msk    (0x1U << DMA_EEI_EEI7_Pos)                          /*!< 0x00000080 */
#define DMA_EEI_EEI7        DMA_EEI_EEI7_Msk                                    /*!< Enable Error Interrupt 7 */

/*******************  Bit definition for DMA_CEEI register  *********************/
#define DMA_CEEI_CEEI0_Pos  (0U)
#define DMA_CEEI_CEEI0_Msk  (0x1U << DMA_CEEI_CEEI0_Pos)                        /*!< 0x00000001 */
#define DMA_CEEI_CEEI0      DMA_CEEI_CEEI0_Msk                                  /*!< Clear Enable Error Interrupt 0 */

#define DMA_CEEI_CEEI1_Pos  (1U)
#define DMA_CEEI_CEEI1_Msk  (0x1U << DMA_CEEI_CEEI1_Pos)                        /*!< 0x00000002 */
#define DMA_CEEI_CEEI1      DMA_CEEI_CEEI1_Msk                                  /*!< Clear Enable Error Interrupt 1 */

#define DMA_CEEI_CEEI2_Pos  (2U)
#define DMA_CEEI_CEEI2_Msk  (0x1U << DMA_CEEI_CEEI2_Pos)                        /*!< 0x00000004 */
#define DMA_CEEI_CEEI2      DMA_CEEI_CEEI2_Msk                                  /*!< Clear Enable Error Interrupt 2 */

#define DMA_CEEI_CEEI3_Pos  (3U)
#define DMA_CEEI_CEEI3_Msk  (0x1U << DMA_CEEI_CEEI3_Pos)                        /*!< 0x00000008 */
#define DMA_CEEI_CEEI3      DMA_CEEI_CEEI3_Msk                                  /*!< Clear Enable Error Interrupt 3 */

#define DMA_CEEI_CEEI4_Pos  (4U)
#define DMA_CEEI_CEEI4_Msk  (0x1U << DMA_CEEI_CEEI4_Pos)                        /*!< 0x00000010 */
#define DMA_CEEI_CEEI4      DMA_CEEI_CEEI4_Msk                                  /*!< Clear Enable Error Interrupt 4 */

#define DMA_CEEI_CEEI5_Pos  (5U)
#define DMA_CEEI_CEEI5_Msk  (0x1U << DMA_CEEI_CEEI5_Pos)                        /*!< 0x00000020 */
#define DMA_CEEI_CEEI5      DMA_CEEI_CEEI5_Msk                                  /*!< Clear Enable Error Interrupt 5 */

#define DMA_CEEI_CEEI6_Pos  (6U)
#define DMA_CEEI_CEEI6_Msk  (0x1U << DMA_CEEI_CEEI6_Pos)                        /*!< 0x00000040 */
#define DMA_CEEI_CEEI6      DMA_CEEI_CEEI6_Msk                                  /*!< Clear Enable Error Interrupt 6 */

#define DMA_CEEI_CEEI7_Pos  (7U)
#define DMA_CEEI_CEEI7_Msk  (0x1U << DMA_CEEI_CEEI7_Pos)                        /*!< 0x00000080 */
#define DMA_CEEI_CEEI7      DMA_CEEI_CEEI7_Msk                                  /*!< Clear Enable Error Interrupt 7 */

/*******************  Bit definition for DMA_SEEI register  *********************/
#define DMA_SEEI_SEEI0_Pos  (0U)
#define DMA_SEEI_SEEI0_Msk  (0x1U << DMA_SEEI_SEEI0_Pos)                        /*!< 0x00000001 */
#define DMA_SEEI_SEEI0      DMA_SEEI_SEEI0_Msk                                  /*!< Set Enable Error Interrupt 0 */

#define DMA_SEEI_SEEI1_Pos  (1U)
#define DMA_SEEI_SEEI1_Msk  (0x1U << DMA_SEEI_SEEI1_Pos)                        /*!< 0x00000002 */
#define DMA_SEEI_SEEI1      DMA_SEEI_SEEI1_Msk                                  /*!< Set Enable Error Interrupt 1 */

#define DMA_SEEI_SEEI2_Pos  (2U)
#define DMA_SEEI_SEEI2_Msk  (0x1U << DMA_SEEI_SEEI2_Pos)                        /*!< 0x00000004 */
#define DMA_SEEI_SEEI2      DMA_SEEI_SEEI2_Msk                                  /*!< Set Enable Error Interrupt 2 */

#define DMA_SEEI_SEEI3_Pos  (3U)
#define DMA_SEEI_SEEI3_Msk  (0x1U << DMA_SEEI_SEEI3_Pos)                        /*!< 0x00000008 */
#define DMA_SEEI_SEEI3      DMA_SEEI_SEEI3_Msk                                  /*!< Set Enable Error Interrupt 3 */

#define DMA_SEEI_SEEI4_Pos  (4U)
#define DMA_SEEI_SEEI4_Msk  (0x1U << DMA_SEEI_SEEI4_Pos)                        /*!< 0x00000010 */
#define DMA_SEEI_SEEI4      DMA_SEEI_SEEI4_Msk                                  /*!< Set Enable Error Interrupt 4 */

#define DMA_SEEI_SEEI5_Pos  (5U)
#define DMA_SEEI_SEEI5_Msk  (0x1U << DMA_SEEI_SEEI5_Pos)                        /*!< 0x00000020 */
#define DMA_SEEI_SEEI5      DMA_SEEI_SEEI5_Msk                                  /*!< Set Enable Error Interrupt 5 */

#define DMA_SEEI_SEEI6_Pos  (6U)
#define DMA_SEEI_SEEI6_Msk  (0x1U << DMA_SEEI_SEEI6_Pos)                        /*!< 0x00000040 */
#define DMA_SEEI_SEEI6      DMA_SEEI_SEEI6_Msk                                  /*!< Set Enable Error Interrupt 6 */

#define DMA_SEEI_SEEI7_Pos  (7U)
#define DMA_SEEI_SEEI7_Msk  (0x1U << DMA_SEEI_SEEI7_Pos)                        /*!< 0x00000080 */
#define DMA_SEEI_SEEI7      DMA_SEEI_SEEI7_Msk                                  /*!< Set Enable Error Interrupt 7 */

/*******************  Bit definition for DMA_CERQ register  *********************/
#define DMA_CERQ_CERQ0_Pos  (0U)
#define DMA_CERQ_CERQ0_Msk  (0x1U << DMA_CERQ_CERQ0_Pos)                        /*!< 0x00000001 */
#define DMA_CERQ_CERQ0      DMA_CERQ_CERQ0_Msk                                  /*!< Clear Enable Request 0 */

#define DMA_CERQ_CERQ1_Pos  (1U)
#define DMA_CERQ_CERQ1_Msk  (0x1U << DMA_CERQ_CERQ1_Pos)                        /*!< 0x00000002 */
#define DMA_CERQ_CERQ1      DMA_CERQ_CERQ1_Msk                                  /*!< Clear Enable Request 1 */

#define DMA_CERQ_CERQ2_Pos  (2U)
#define DMA_CERQ_CERQ2_Msk  (0x1U << DMA_CERQ_CERQ2_Pos)                        /*!< 0x00000004 */
#define DMA_CERQ_CERQ2      DMA_CERQ_CERQ2_Msk                                  /*!< Clear Enable Request 2 */

#define DMA_CERQ_CERQ3_Pos  (3U)
#define DMA_CERQ_CERQ3_Msk  (0x1U << DMA_CERQ_CERQ3_Pos)                        /*!< 0x00000008 */
#define DMA_CERQ_CERQ3      DMA_CERQ_CERQ3_Msk                                  /*!< Clear Enable Request 3 */

#define DMA_CERQ_CERQ4_Pos  (4U)
#define DMA_CERQ_CERQ4_Msk  (0x1U << DMA_CERQ_CERQ4_Pos)                        /*!< 0x00000010 */
#define DMA_CERQ_CERQ4      DMA_CERQ_CERQ4_Msk                                  /*!< Clear Enable Request 4 */

#define DMA_CERQ_CERQ5_Pos  (5U)
#define DMA_CERQ_CERQ5_Msk  (0x1U << DMA_CERQ_CERQ5_Pos)                        /*!< 0x00000020 */
#define DMA_CERQ_CERQ5      DMA_CERQ_CERQ5_Msk                                  /*!< Clear Enable Request 5 */

#define DMA_CERQ_CERQ6_Pos  (6U)
#define DMA_CERQ_CERQ6_Msk  (0x1U << DMA_CERQ_CERQ6_Pos)                        /*!< 0x00000040 */
#define DMA_CERQ_CERQ6      DMA_CERQ_CERQ6_Msk                                  /*!< Clear Enable Request 6 */

#define DMA_CERQ_CERQ7_Pos  (7U)
#define DMA_CERQ_CERQ7_Msk  (0x1U << DMA_CERQ_CERQ7_Pos)                        /*!< 0x00000080 */
#define DMA_CERQ_CERQ7      DMA_CERQ_CERQ7_Msk                                  /*!< Clear Enable Request 7 */

/*******************  Bit definition for DMA_SERQ register  *********************/
#define DMA_SERQ_SERQ0_Pos  (0U)
#define DMA_SERQ_SERQ0_Msk  (0x1U << DMA_SERQ_SERQ0_Pos)                        /*!< 0x00000001 */
#define DMA_SERQ_SERQ0      DMA_SERQ_SERQ0_Msk                                  /*!< Set Enable Request 0 */

#define DMA_SERQ_SERQ1_Pos  (1U)
#define DMA_SERQ_SERQ1_Msk  (0x1U << DMA_SERQ_SERQ1_Pos)                        /*!< 0x00000002 */
#define DMA_SERQ_SERQ1      DMA_SERQ_SERQ1_Msk                                  /*!< Set Enable Request 1 */

#define DMA_SERQ_SERQ2_Pos  (2U)
#define DMA_SERQ_SERQ2_Msk  (0x1U << DMA_SERQ_SERQ2_Pos)                        /*!< 0x00000004 */
#define DMA_SERQ_SERQ2      DMA_SERQ_SERQ2_Msk                                  /*!< Set Enable Request 2 */

#define DMA_SERQ_SERQ3_Pos  (3U)
#define DMA_SERQ_SERQ3_Msk  (0x1U << DMA_SERQ_SERQ3_Pos)                        /*!< 0x00000008 */
#define DMA_SERQ_SERQ3      DMA_SERQ_SERQ3_Msk                                  /*!< Set Enable Request 3 */

#define DMA_SERQ_SERQ4_Pos  (4U)
#define DMA_SERQ_SERQ4_Msk  (0x1U << DMA_SERQ_SERQ4_Pos)                        /*!< 0x00000010 */
#define DMA_SERQ_SERQ4      DMA_SERQ_SERQ4_Msk                                  /*!< Set Enable Request 4 */

#define DMA_SERQ_SERQ5_Pos  (5U)
#define DMA_SERQ_SERQ5_Msk  (0x1U << DMA_SERQ_SERQ5_Pos)                        /*!< 0x00000020 */
#define DMA_SERQ_SERQ5      DMA_SERQ_SERQ5_Msk                                  /*!< Set Enable Request 5 */

#define DMA_SERQ_SERQ6_Pos  (6U)
#define DMA_SERQ_SERQ6_Msk  (0x1U << DMA_SERQ_SERQ6_Pos)                        /*!< 0x00000040 */
#define DMA_SERQ_SERQ6      DMA_SERQ_SERQ6_Msk                                  /*!< Set Enable Request 6 */

#define DMA_SERQ_SERQ7_Pos  (7U)
#define DMA_SERQ_SERQ7_Msk  (0x1U << DMA_SERQ_SERQ7_Pos)                        /*!< 0x00000080 */
#define DMA_SERQ_SERQ7      DMA_SERQ_SERQ7_Msk                                  /*!< Set Enable Request 7 */

/*******************  Bit definition for DMA_CDNE register  *********************/
#define DMA_CDNE_CDNE0_Pos  (0U)
#define DMA_CDNE_CDNE0_Msk  (0x1U << DMA_CDNE_CDNE0_Pos)                        /*!< 0x00000001 */
#define DMA_CDNE_CDNE0      DMA_CDNE_CDNE0_Msk                                  /*!< Clear DONE Status of channel 0 */

#define DMA_CDNE_CDNE1_Pos  (1U)
#define DMA_CDNE_CDNE1_Msk  (0x1U << DMA_CDNE_CDNE1_Pos)                        /*!< 0x00000002 */
#define DMA_CDNE_CDNE1      DMA_CDNE_CDNE1_Msk                                  /*!< Clear DONE Status of channel 1 */

#define DMA_CDNE_CDNE2_Pos  (2U)
#define DMA_CDNE_CDNE2_Msk  (0x1U << DMA_CDNE_CDNE2_Pos)                        /*!< 0x00000004 */
#define DMA_CDNE_CDNE2      DMA_CDNE_CDNE2_Msk                                  /*!< Clear DONE Status of channel 2 */

#define DMA_CDNE_CDNE3_Pos  (3U)
#define DMA_CDNE_CDNE3_Msk  (0x1U << DMA_CDNE_CDNE3_Pos)                        /*!< 0x00000008 */
#define DMA_CDNE_CDNE3      DMA_CDNE_CDNE3_Msk                                  /*!< Clear DONE Status of channel 3 */

#define DMA_CDNE_CDNE4_Pos  (4U)
#define DMA_CDNE_CDNE4_Msk  (0x1U << DMA_CDNE_CDNE4_Pos)                        /*!< 0x00000010 */
#define DMA_CDNE_CDNE4      DMA_CDNE_CDNE4_Msk                                  /*!< Clear DONE Status of channel 4 */

#define DMA_CDNE_CDNE5_Pos  (5U)
#define DMA_CDNE_CDNE5_Msk  (0x1U << DMA_CDNE_CDNE5_Pos)                        /*!< 0x00000020 */
#define DMA_CDNE_CDNE5      DMA_CDNE_CDNE5_Msk                                  /*!< Clear DONE Status of channel 5 */

#define DMA_CDNE_CDNE6_Pos  (6U)
#define DMA_CDNE_CDNE6_Msk  (0x1U << DMA_CDNE_CDNE6_Pos)                        /*!< 0x00000040 */
#define DMA_CDNE_CDNE6      DMA_CDNE_CDNE6_Msk                                  /*!< Clear DONE Status of channel 6 */

#define DMA_CDNE_CDNE7_Pos  (7U)
#define DMA_CDNE_CDNE7_Msk  (0x1U << DMA_CDNE_CDNE7_Pos)                        /*!< 0x00000080 */
#define DMA_CDNE_CDNE7      DMA_CDNE_CDNE7_Msk                                  /*!< Clear DONE Status of channel 7 */

/*******************  Bit definition for DMA_SSRT register  *********************/
#define DMA_SSRT_SSRT0_Pos  (0U)
#define DMA_SSRT_SSRT0_Msk  (0x1U << DMA_SSRT_SSRT0_Pos)                        /*!< 0x00000001 */
#define DMA_SSRT_SSRT0      DMA_SSRT_SSRT0_Msk                                  /*!< Set START Bit of channel 0 */

#define DMA_SSRT_SSRT1_Pos  (1U)
#define DMA_SSRT_SSRT1_Msk  (0x1U << DMA_SSRT_SSRT1_Pos)                        /*!< 0x00000002 */
#define DMA_SSRT_SSRT1      DMA_SSRT_SSRT1_Msk                                  /*!< Set START Bit of channel 1 */

#define DMA_SSRT_SSRT2_Pos  (2U)
#define DMA_SSRT_SSRT2_Msk  (0x1U << DMA_SSRT_SSRT2_Pos)                        /*!< 0x00000004 */
#define DMA_SSRT_SSRT2      DMA_SSRT_SSRT2_Msk                                  /*!< Set START Bit of channel 2 */

#define DMA_SSRT_SSRT3_Pos  (3U)
#define DMA_SSRT_SSRT3_Msk  (0x1U << DMA_SSRT_SSRT3_Pos)                        /*!< 0x00000008 */
#define DMA_SSRT_SSRT3      DMA_SSRT_SSRT3_Msk                                  /*!< Set START Bit of channel 3 */

#define DMA_SSRT_SSRT4_Pos  (4U)
#define DMA_SSRT_SSRT4_Msk  (0x1U << DMA_SSRT_SSRT4_Pos)                        /*!< 0x00000010 */
#define DMA_SSRT_SSRT4      DMA_SSRT_SSRT4_Msk                                  /*!< Set START Bit of channel 4 */

#define DMA_SSRT_SSRT5_Pos  (5U)
#define DMA_SSRT_SSRT5_Msk  (0x1U << DMA_SSRT_SSRT5_Pos)                        /*!< 0x00000020 */
#define DMA_SSRT_SSRT5      DMA_SSRT_SSRT5_Msk                                  /*!< Set START Bit of channel 5 */

#define DMA_SSRT_SSRT6_Pos  (6U)
#define DMA_SSRT_SSRT6_Msk  (0x1U << DMA_SSRT_SSRT6_Pos)                        /*!< 0x00000040 */
#define DMA_SSRT_SSRT6      DMA_SSRT_SSRT6_Msk                                  /*!< Set START Bit of channel 6 */

#define DMA_SSRT_SSRT7_Pos  (7U)
#define DMA_SSRT_SSRT7_Msk  (0x1U << DMA_SSRT_SSRT7_Pos)                        /*!< 0x00000080 */
#define DMA_SSRT_SSRT7      DMA_SSRT_SSRT7_Msk                                  /*!< Set START Bit of channel 7 */

/*******************  Bit definition for DMA_CERR register  *********************/
#define DMA_CERR_CERR0_Pos  (0U)
#define DMA_CERR_CERR0_Msk  (0x1U << DMA_CERR_CERR0_Pos)                        /*!< 0x00000001 */
#define DMA_CERR_CERR0      DMA_CERR_CERR0_Msk                                  /*!< Clear Error Indicator of channel 0 */

#define DMA_CERR_CERR1_Pos  (1U)
#define DMA_CERR_CERR1_Msk  (0x1U << DMA_CERR_CERR1_Pos)                        /*!< 0x00000002 */
#define DMA_CERR_CERR1      DMA_CERR_CERR1_Msk                                  /*!< Clear Error Indicator of channel 1 */

#define DMA_CERR_CERR2_Pos  (2U)
#define DMA_CERR_CERR2_Msk  (0x1U << DMA_CERR_CERR2_Pos)                        /*!< 0x00000004 */
#define DMA_CERR_CERR2      DMA_CERR_CERR2_Msk                                  /*!< Clear Error Indicator of channel 2 */

#define DMA_CERR_CERR3_Pos  (3U)
#define DMA_CERR_CERR3_Msk  (0x1U << DMA_CERR_CERR3_Pos)                        /*!< 0x00000008 */
#define DMA_CERR_CERR3      DMA_CERR_CERR3_Msk                                  /*!< Clear Error Indicator of channel 3 */

#define DMA_CERR_CERR4_Pos  (4U)
#define DMA_CERR_CERR4_Msk  (0x1U << DMA_CERR_CERR4_Pos)                        /*!< 0x00000010 */
#define DMA_CERR_CERR4      DMA_CERR_CERR4_Msk                                  /*!< Clear Error Indicator of channel 4 */

#define DMA_CERR_CERR5_Pos  (5U)
#define DMA_CERR_CERR5_Msk  (0x1U << DMA_CERR_CERR5_Pos)                        /*!< 0x00000020 */
#define DMA_CERR_CERR5      DMA_CERR_CERR5_Msk                                  /*!< Clear Error Indicator of channel 5 */

#define DMA_CERR_CERR6_Pos  (6U)
#define DMA_CERR_CERR6_Msk  (0x1U << DMA_CERR_CERR6_Pos)                        /*!< 0x00000040 */
#define DMA_CERR_CERR6      DMA_CERR_CERR6_Msk                                  /*!< Clear Error Indicator of channel 6 */

#define DMA_CERR_CERR7_Pos  (7U)
#define DMA_CERR_CERR7_Msk  (0x1U << DMA_CERR_CERR7_Pos)                        /*!< 0x00000080 */
#define DMA_CERR_CERR7      DMA_CERR_CERR7_Msk                                  /*!< Clear Error Indicator of channel 7 */

/*******************  Bit definition for DMA_CINT register  *********************/
#define DMA_CINT_CINT0_Pos  (0U)
#define DMA_CINT_CINT0_Msk  (0x1U << DMA_CINT_CINT0_Pos)                        /*!< 0x00000001 */
#define DMA_CINT_CINT0      DMA_CINT_CINT0_Msk                                  /*!< Clear Interrupt Request of channel 0 */

#define DMA_CINT_CINT1_Pos  (1U)
#define DMA_CINT_CINT1_Msk  (0x1U << DMA_CINT_CINT1_Pos)                        /*!< 0x00000002 */
#define DMA_CINT_CINT1      DMA_CINT_CINT1_Msk                                  /*!< Clear Interrupt Request of channel 1 */

#define DMA_CINT_CINT2_Pos  (2U)
#define DMA_CINT_CINT2_Msk  (0x1U << DMA_CINT_CINT2_Pos)                        /*!< 0x00000004 */
#define DMA_CINT_CINT2      DMA_CINT_CINT2_Msk                                  /*!< Clear Interrupt Request of channel 2 */

#define DMA_CINT_CINT3_Pos  (3U)
#define DMA_CINT_CINT3_Msk  (0x1U << DMA_CINT_CINT3_Pos)                        /*!< 0x00000008 */
#define DMA_CINT_CINT3      DMA_CINT_CINT3_Msk                                  /*!< Clear Interrupt Request of channel 3 */

#define DMA_CINT_CINT4_Pos  (4U)
#define DMA_CINT_CINT4_Msk  (0x1U << DMA_CINT_CINT4_Pos)                        /*!< 0x00000010 */
#define DMA_CINT_CINT4      DMA_CINT_CINT4_Msk                                  /*!< Clear Interrupt Request of channel 4 */

#define DMA_CINT_CINT5_Pos  (5U)
#define DMA_CINT_CINT5_Msk  (0x1U << DMA_CINT_CINT5_Pos)                        /*!< 0x00000020 */
#define DMA_CINT_CINT5      DMA_CINT_CINT5_Msk                                  /*!< Clear Interrupt Request of channel 5 */

#define DMA_CINT_CINT6_Pos  (6U)
#define DMA_CINT_CINT6_Msk  (0x1U << DMA_CINT_CINT6_Pos)                        /*!< 0x00000040 */
#define DMA_CINT_CINT6      DMA_CINT_CINT6_Msk                                  /*!< Clear Interrupt Request of channel 6 */

#define DMA_CINT_CINT7_Pos  (7U)
#define DMA_CINT_CINT7_Msk  (0x1U << DMA_CINT_CINT7_Pos)                        /*!< 0x00000080 */
#define DMA_CINT_CINT7      DMA_CINT_CINT7_Msk                                  /*!< Clear Interrupt Request of channel 7 */

/*******************  Bit definition for DMA_INT register  *********************/
#define DMA_INT_INT0_Pos    (0U)
#define DMA_INT_INT0_Msk    (0x1U << DMA_INT_INT0_Pos)                          /*!< 0x00000001 */
#define DMA_INT_INT0        DMA_INT_INT0_Msk                                    /*!< Interrupt Request of channel 0 */

#define DMA_INT_INT1_Pos    (1U)
#define DMA_INT_INT1_Msk    (0x1U << DMA_INT_INT1_Pos)                          /*!< 0x00000002 */
#define DMA_INT_INT1        DMA_INT_INT1_Msk                                    /*!< Interrupt Request of channel 1 */

#define DMA_INT_INT2_Pos    (2U)
#define DMA_INT_INT2_Msk    (0x1U << DMA_INT_INT2_Pos)                          /*!< 0x00000004 */
#define DMA_INT_INT2        DMA_INT_INT2_Msk                                    /*!< Interrupt Request of channel 2 */

#define DMA_INT_INT3_Pos    (3U)
#define DMA_INT_INT3_Msk    (0x1U << DMA_INT_INT3_Pos)                          /*!< 0x00000008 */
#define DMA_INT_INT3        DMA_INT_INT3_Msk                                    /*!< Interrupt Request of channel 3 */

#define DMA_INT_INT4_Pos    (4U)
#define DMA_INT_INT4_Msk    (0x1U << DMA_INT_INT4_Pos)                          /*!< 0x00000010 */
#define DMA_INT_INT4        DMA_INT_INT4_Msk                                    /*!< Interrupt Request of channel 4 */

#define DMA_INT_INT5_Pos    (5U)
#define DMA_INT_INT5_Msk    (0x1U << DMA_INT_INT5_Pos)                          /*!< 0x00000020 */
#define DMA_INT_INT5        DMA_INT_INT5_Msk                                    /*!< Interrupt Request of channel 5 */

#define DMA_INT_INT6_Pos    (6U)
#define DMA_INT_INT6_Msk    (0x1U << DMA_INT_INT6_Pos)                          /*!< 0x00000040 */
#define DMA_INT_INT6        DMA_INT_INT6_Msk                                    /*!< Interrupt Request of channel 6 */

#define DMA_INT_INT7_Pos    (7U)
#define DMA_INT_INT7_Msk    (0x1U << DMA_INT_INT7_Pos)                          /*!< 0x00000080 */
#define DMA_INT_INT7        DMA_INT_INT7_Msk                                    /*!< Interrupt Request of channel 7 */

/*******************  Bit definition for DMA_ERR register  *********************/
#define DMA_ERR_ERR0_Pos    (0U)
#define DMA_ERR_ERR0_Msk    (0x1U << DMA_ERR_ERR0_Pos)                          /*!< 0x00000001 */
#define DMA_ERR_ERR0        DMA_ERR_ERR0_Msk                                    /*!< Error In Channel  0 */

#define DMA_ERR_ERR1_Pos    (1U)
#define DMA_ERR_ERR1_Msk    (0x1U << DMA_ERR_ERR1_Pos)                          /*!< 0x00000002 */
#define DMA_ERR_ERR1        DMA_ERR_ERR1_Msk                                    /*!< Error In Channel  1 */

#define DMA_ERR_ERR2_Pos    (2U)
#define DMA_ERR_ERR2_Msk    (0x1U << DMA_ERR_ERR2_Pos)                          /*!< 0x00000004 */
#define DMA_ERR_ERR2        DMA_ERR_ERR2_Msk                                    /*!< Error In Channel  2 */

#define DMA_ERR_ERR3_Pos    (3U)
#define DMA_ERR_ERR3_Msk    (0x1U << DMA_ERR_ERR3_Pos)                          /*!< 0x00000008 */
#define DMA_ERR_ERR3        DMA_ERR_ERR3_Msk                                    /*!< Error In Channel 3 */

#define DMA_ERR_ERR4_Pos    (4U)
#define DMA_ERR_ERR4_Msk    (0x1U << DMA_ERR_ERR4_Pos)                          /*!< 0x00000010 */
#define DMA_ERR_ERR4        DMA_ERR_ERR4_Msk                                    /*!< Error In Channel 4 */

#define DMA_ERR_ERR5_Pos    (5U)
#define DMA_ERR_ERR5_Msk    (0x1U << DMA_ERR_ERR5_Pos)                          /*!< 0x00000020 */
#define DMA_ERR_ERR5        DMA_ERR_ERR5_Msk                                    /*!< Error In Channel 5 */

#define DMA_ERR_ERR6_Pos    (6U)
#define DMA_ERR_ERR6_Msk    (0x1U << DMA_ERR_ERR6_Pos)                          /*!< 0x00000040 */
#define DMA_ERR_ERR6        DMA_ERR_ERR6_Msk                                    /*!< Error In Channel 6 */

#define DMA_ERR_ERR7_Pos    (7U)
#define DMA_ERR_ERR7_Msk    (0x1U << DMA_ERR_ERR7_Pos)                          /*!< 0x00000080 */
#define DMA_ERR_ERR7        DMA_ERR_ERR7_Msk                                    /*!< Error In Channel 7 */

/*******************  Bit definition for DMA_HRS register  *********************/
#define DMA_HRS_HRS0_Pos    (0U)
#define DMA_HRS_HRS0_Msk    (0x1U << DMA_HRS_HRS0_Pos)                          /*!< 0x00000001 */
#define DMA_HRS_HRS0        DMA_HRS_HRS0_Msk                                    /*!< Hardware Request Status Channel  0 */

#define DMA_HRS_HRS1_Pos    (1U)
#define DMA_HRS_HRS1_Msk    (0x1U << DMA_HRS_HRS1_Pos)                          /*!< 0x00000002 */
#define DMA_HRS_HRS1        DMA_HRS_HRS1_Msk                                    /*!< Hardware Request Status Channel  1 */

#define DMA_HRS_HRS2_Pos    (2U)
#define DMA_HRS_HRS2_Msk    (0x1U << DMA_HRS_HRS2_Pos)                          /*!< 0x00000004 */
#define DMA_HRS_HRS2        DMA_HRS_HRS2_Msk                                    /*!< Hardware Request Status Channel  2 */

#define DMA_HRS_HRS3_Pos    (3U)
#define DMA_HRS_HRS3_Msk    (0x1U << DMA_HRS_HRS3_Pos)                          /*!< 0x00000008 */
#define DMA_HRS_HRS3        DMA_HRS_HRS3_Msk                                    /*!< Hardware Request Status Channel 3 */

#define DMA_HRS_HRS4_Pos    (4U)
#define DMA_HRS_HRS4_Msk    (0x1U << DMA_HRS_HRS4_Pos)                          /*!< 0x00000010 */
#define DMA_HRS_HRS4        DMA_HRS_HRS4_Msk                                    /*!< Hardware Request Status Channel 4 */

#define DMA_HRS_HRS5_Pos    (5U)
#define DMA_HRS_HRS5_Msk    (0x1U << DMA_HRS_HRS5_Pos)                          /*!< 0x00000020 */
#define DMA_HRS_HRS5        DMA_HRS_HRS5_Msk                                    /*!< Hardware Request Status Channel 5 */

#define DMA_HRS_HRS6_Pos    (6U)
#define DMA_HRS_HRS6_Msk    (0x1U << DMA_HRS_HRS6_Pos)                          /*!< 0x00000040 */
#define DMA_HRS_HRS6        DMA_HRS_HRS6_Msk                                    /*!< Hardware Request Status Channel 6 */

#define DMA_HRS_HRS7_Pos    (7U)
#define DMA_HRS_HRS7_Msk    (0x1U << DMA_HRS_HRS7_Pos)                          /*!< 0x00000080 */
#define DMA_HRS_HRS7        DMA_HRS_HRS7_Msk                                    /*!< Hardware Request Status Channel 7 */

/*******************  Bit definition for DMA_EARS register  *********************/
#define DMA_EARS_EARS0_Pos  (0U)
#define DMA_EARS_EARS0_Msk  (0x1U << DMA_EARS_EARS0_Pos)                        /*!< 0x00000001 */
#define DMA_EARS_EARS0      DMA_EARS_EARS0_Msk                                  /*!< Enable asynchronous DMA request wakeup for channel 0 */

#define DMA_EARS_EARS1_Pos  (1U)
#define DMA_EARS_EARS1_Msk  (0x1U << DMA_EARS_EARS1_Pos)                        /*!< 0x00000002 */
#define DMA_EARS_EARS1      DMA_EARS_EARS1_Msk                                  /*!< Enable asynchronous DMA request wakeup for channel 1 */

#define DMA_EARS_EARS2_Pos  (2U)
#define DMA_EARS_EARS2_Msk  (0x1U << DMA_EARS_EARS2_Pos)                        /*!< 0x00000004 */
#define DMA_EARS_EARS2      DMA_EARS_EARS2_Msk                                  /*!< Enable asynchronous DMA request wakeup for channel 2 */

#define DMA_EARS_EARS3_Pos  (3U)
#define DMA_EARS_EARS3_Msk  (0x1U << DMA_EARS_EARS3_Pos)                        /*!< 0x00000008 */
#define DMA_EARS_EARS3      DMA_EARS_EARS3_Msk                                  /*!< Enable asynchronous DMA request wakeup for channel 3 */

#define DMA_EARS_EARS4_Pos  (4U)
#define DMA_EARS_EARS4_Msk  (0x1U << DMA_EARS_EARS4_Pos)                        /*!< 0x00000010 */
#define DMA_EARS_EARS4      DMA_EARS_EARS4_Msk                                  /*!< Enable asynchronous DMA request wakeup for channel 4 */

#define DMA_EARS_EARS5_Pos  (5U)
#define DMA_EARS_EARS5_Msk  (0x1U << DMA_EARS_EARS5_Pos)                        /*!< 0x00000020 */
#define DMA_EARS_EARS5      DMA_EARS_EARS5_Msk                                  /*!< Enable asynchronous DMA request wakeup for channel 5 */

#define DMA_EARS_EARS6_Pos  (6U)
#define DMA_EARS_EARS6_Msk  (0x1U << DMA_EARS_EARS6_Pos)                        /*!< 0x00000040 */
#define DMA_EARS_EARS6      DMA_EARS_EARS6_Msk                                  /*!< Enable asynchronous DMA request wakeup for channel 6 */

#define DMA_EARS_EARS7_Pos  (7U)
#define DMA_EARS_EARS7_Msk  (0x1U << DMA_EARS_EARS7_Pos)                        /*!< 0x00000080 */
#define DMA_EARS_EARS7      DMA_EARS_EARS7_Msk                                  /*!< Enable asynchronous DMA request wakeup for channel 7 */

/*******************  Bit definition for DMA_DONE register  *********************/
#define DMA_DONE_DONE0_Pos  (0U)
#define DMA_DONE_DONE0_Msk  (0x1U << DMA_DONE_DONE0_Pos)                        /*!< 0x00000001 */
#define DMA_DONE_DONE0      DMA_DONE_DONE0_Msk                                  /*!< Transfer completion flag of channel 0 */

#define DMA_DONE_DONE1_Pos  (1U)
#define DMA_DONE_DONE1_Msk  (0x1U << DMA_DONE_DONE1_Pos)                        /*!< 0x00000002 */
#define DMA_DONE_DONE1      DMA_DONE_DONE1_Msk                                  /*!< Transfer completion flag of channel 1 */

#define DMA_DONE_DONE2_Pos  (2U)
#define DMA_DONE_DONE2_Msk  (0x1U << DMA_DONE_DONE2_Pos)                        /*!< 0x00000004 */
#define DMA_DONE_DONE2      DMA_DONE_DONE2_Msk                                  /*!< Transfer completion flag of channel 2 */

#define DMA_DONE_DONE3_Pos  (3U)
#define DMA_DONE_DONE3_Msk  (0x1U << DMA_DONE_DONE3_Pos)                        /*!< 0x00000008 */
#define DMA_DONE_DONE3      DMA_DONE_DONE3_Msk                                  /*!< Transfer completion flag of channel 3 */

#define DMA_DONE_DONE4_Pos  (4U)
#define DMA_DONE_DONE4_Msk  (0x1U << DMA_DONE_DONE4_Pos)                        /*!< 0x00000010 */
#define DMA_DONE_DONE4      DMA_DONE_DONE4_Msk                                  /*!< Transfer completion flag of channel 4 */

#define DMA_DONE_DONE5_Pos  (5U)
#define DMA_DONE_DONE5_Msk  (0x1U << DMA_DONE_DONE5_Pos)                        /*!< 0x00000020 */
#define DMA_DONE_DONE5      DMA_DONE_DONE5_Msk                                  /*!< Transfer completion flag of channel 5 */

#define DMA_DONE_DONE6_Pos  (6U)
#define DMA_DONE_DONE6_Msk  (0x1U << DMA_DONE_DONE6_Pos)                        /*!< 0x00000040 */
#define DMA_DONE_DONE6      DMA_DONE_DONE6_Msk                                  /*!< Transfer completion flag of channel 6 */

#define DMA_DONE_DONE7_Pos  (7U)
#define DMA_DONE_DONE7_Msk  (0x1U << DMA_DONE_DONE7_Pos)                        /*!< 0x00000080 */
#define DMA_DONE_DONE7      DMA_DONE_DONE7_Msk                                  /*!< Transfer completion flag of channel 7 */

/*******************  Bit definition for DMA_ACTIVE register  *********************/
#define DMA_ACTIVE_ACTIVE0_Pos  (0U)
#define DMA_ACTIVE_ACTIVE0_Msk  (0x1U << DMA_ACTIVE_ACTIVE0_Pos)                /*!< 0x00000001 */
#define DMA_ACTIVE_ACTIVE0      DMA_ACTIVE_ACTIVE0_Msk                          /*!< Active state of channel 0 */

#define DMA_ACTIVE_ACTIVE1_Pos  (1U)
#define DMA_ACTIVE_ACTIVE1_Msk  (0x1U << DMA_ACTIVE_ACTIVE1_Pos)                /*!< 0x00000002 */
#define DMA_ACTIVE_ACTIVE1      DMA_ACTIVE_ACTIVE1_Msk                          /*!< Active state of channel 1 */

#define DMA_ACTIVE_ACTIVE2_Pos  (2U)
#define DMA_ACTIVE_ACTIVE2_Msk  (0x1U << DMA_ACTIVE_ACTIVE2_Pos)                /*!< 0x00000004 */
#define DMA_ACTIVE_ACTIVE2      DMA_ACTIVE_ACTIVE2_Msk                          /*!< Active state of channel 2 */

#define DMA_ACTIVE_ACTIVE3_Pos  (3U)
#define DMA_ACTIVE_ACTIVE3_Msk  (0x1U << DMA_ACTIVE_ACTIVE3_Pos)                /*!< 0x00000008 */
#define DMA_ACTIVE_ACTIVE3      DMA_ACTIVE_ACTIVE3_Msk                          /*!< Active state of channel 3 */

#define DMA_ACTIVE_ACTIVE4_Pos  (4U)
#define DMA_ACTIVE_ACTIVE4_Msk  (0x1U << DMA_ACTIVE_ACTIVE4_Pos)                /*!< 0x00000010 */
#define DMA_ACTIVE_ACTIVE4      DMA_ACTIVE_ACTIVE4_Msk                          /*!< Active state of channel 4 */

#define DMA_ACTIVE_ACTIVE5_Pos  (5U)
#define DMA_ACTIVE_ACTIVE5_Msk  (0x1U << DMA_ACTIVE_ACTIVE5_Pos)                /*!< 0x00000020 */
#define DMA_ACTIVE_ACTIVE5      DMA_ACTIVE_ACTIVE5_Msk                          /*!< Active state of channel 5 */

#define DMA_ACTIVE_ACTIVE6_Pos  (6U)
#define DMA_ACTIVE_ACTIVE6_Msk  (0x1U << DMA_ACTIVE_ACTIVE6_Pos)                /*!< 0x00000040 */
#define DMA_ACTIVE_ACTIVE6      DMA_ACTIVE_ACTIVE6_Msk                          /*!< Active state of channel 6 */

#define DMA_ACTIVE_ACTIVE7_Pos  (7U)
#define DMA_ACTIVE_ACTIVE7_Msk  (0x1U << DMA_ACTIVE_ACTIVE7_Pos)                /*!< 0x00000080 */
#define DMA_ACTIVE_ACTIVE7      DMA_ACTIVE_ACTIVE7_Msk                          /*!< Active state of channel 7 */

/*******************  Bit definition for DMA_CHPRI register  *********************/
#define DMA_CHPRI_CH0PRI_Pos    (0U)
#define DMA_CHPRI_CH0PRI_Msk    (0x7U << DMA_CHPRI_CH0PRI_Pos)                  /*!< 0x00000007 */
#define DMA_CHPRI_CH0PRI        DMA_CHPRI_CH0PRI_Msk                            /*!< Channel 0 Arbitration Priority */

#define DMA_CHPRI_CH1PRI_Pos    (4U)
#define DMA_CHPRI_CH1PRI_Msk    (0x7U << DMA_CHPRI_CH1PRI_Pos)                  /*!< 0x00000070 */
#define DMA_CHPRI_CH1PRI        DMA_CHPRI_CH1PRI_Msk                            /*!< Channel 1 Arbitration Priority */

#define DMA_CHPRI_CH2PRI_Pos    (8U)
#define DMA_CHPRI_CH2PRI_Msk    (0x7U << DMA_CHPRI_CH2PRI_Pos)                  /*!< 0x00000700 */
#define DMA_CHPRI_CH2PRI        DMA_CHPRI_CH2PRI_Msk                            /*!< Channel 2 Arbitration Priority */

#define DMA_CHPRI_CH3PRI_Pos    (12U)
#define DMA_CHPRI_CH3PRI_Msk    (0x7U << DMA_CHPRI_CH3PRI_Pos)                  /*!< 0x00007000 */
#define DMA_CHPRI_CH3PRI        DMA_CHPRI_CH3PRI_Msk                            /*!< Channel 3 Arbitration Priority */

#define DMA_CHPRI_CH4PRI_Pos    (16U)
#define DMA_CHPRI_CH4PRI_Msk    (0x7U << DMA_CHPRI_CH4PRI_Pos)                  /*!< 0x00070000 */
#define DMA_CHPRI_CH4PRI        DMA_CHPRI_CH4PRI_Msk                            /*!< Channel 4 Arbitration Priority */

#define DMA_CHPRI_CH5PRI_Pos    (20U)
#define DMA_CHPRI_CH5PRI_Msk    (0x7U << DMA_CHPRI_CH5PRI_Pos)                  /*!< 0x00700000 */
#define DMA_CHPRI_CH5PRI        DMA_CHPRI_CH5PRI_Msk                            /*!< Channel 5 Arbitration Priority */

#define DMA_CHPRI_CH6PRI_Pos    (24U)
#define DMA_CHPRI_CH6PRI_Msk    (0x7U << DMA_CHPRI_CH6PRI_Pos)                  /*!< 0x07000000 */
#define DMA_CHPRI_CH6PRI        DMA_CHPRI_CH6PRI_Msk                            /*!< Channel 6 Arbitration Priority */

#define DMA_CHPRI_CH7PRI_Pos    (28U)
#define DMA_CHPRI_CH7PRI_Msk    (0x7U << DMA_CHPRI_CH7PRI_Pos)                  /*!< 0x70000000 */
#define DMA_CHPRI_CH7PRI        DMA_CHPRI_CH7PRI_Msk                            /*!< Channel 7 Arbitration Priority */

/*******************  Bit definition for DMA_WORD0 register  *********************/
#define DMA_WORD0_SADDR_Pos (0U)
#define DMA_WORD0_SADDR_Msk (0xFFFFFFFFU << DMA_WORD0_SADDR_Pos)                /*!< 0xFFFFFFFF */
#define DMA_WORD0_SADDR     DMA_WORD0_SADDR_Msk                                 /*!< Source Address  */

/* The count of DMA_WORD0 */
#define DMA_WORD0_COUNT (8U)


/* The count of DMA_WORD1 */
#define DMA_WORD1_COUNT (8U)

/*******************  Bit definition for DMA_WORD2 register  *********************/
#define DMA_WORD2_NBYTES_Pos    (0U)
#define DMA_WORD2_NBYTES_Msk    (0x3FFU << DMA_WORD2_NBYTES_Pos)                        /*!< 0x000003FF */
#define DMA_WORD2_NBYTES        DMA_WORD2_NBYTES_Msk                                    /*!< Minor Byte Transfer Count */

#define DMA_WORD2_MOFFNBYTES_Pos    (10U)
#define DMA_WORD2_MOFFNBYTES_Msk    (0xFFFFFU << DMA_WORD2_MOFFNBYTES_Pos)              /*!< 0x3FFFFC00 */
#define DMA_WORD2_MOFFNBYTES        DMA_WORD2_MOFFNBYTES_Msk                            /*!< offset of minor loop completion */

#define DMA_WORD2_DMLOENBYTES_Pos   (30U)
#define DMA_WORD2_DMLOENBYTES_Msk   (0x1U << DMA_WORD2_DMLOENBYTES_Pos)                 /*!< 0x40000000 */
#define DMA_WORD2_DMLOENBYTES       DMA_WORD2_DMLOENBYTES_Msk                           /*!< Enable Destination offset of minor loop completion */

#define DMA_WORD2_SMLOENBYTES_Pos   (31U)
#define DMA_WORD2_SMLOENBYTES_Msk   (0x1U << DMA_WORD2_SMLOENBYTES_Pos)                 /*!< 0x80000000 */
#define DMA_WORD2_SMLOENBYTES       DMA_WORD2_SMLOENBYTES_Msk                           /*!< Enable Source offset of minor loop completion */

/* The count of DMA_WORD2 */
#define DMA_WORD2_COUNT (8U)


/* The count of DMA_WORD3 */
#define DMA_WORD3_COUNT (8U)

/*******************  Bit definition for DMA_WORD4 register  *********************/
#define DMA_WORD4_DADDR_Pos (0U)
#define DMA_WORD4_DADDR_Msk (0xFFFFFFFFU << DMA_WORD4_DADDR_Pos)                        /*!< 0xFFFFFFFF */
#define DMA_WORD4_DADDR     DMA_WORD4_DADDR_Msk                                         /*!< Destination Address  */

/* The count of DMA_WORD4 */
#define DMA_WORD4_COUNT (8U)

/*******************  Bit definition for DMA_WORD5 register  *********************/
#define DMA_WORD5_BITER_Pos (0U)
#define DMA_WORD5_BITER_Msk (0x7FFFU << DMA_WORD5_BITER_Pos)                            /*!< 0x00007FFF */
#define DMA_WORD5_BITER     DMA_WORD5_BITER_Msk                                         /*!< major loop count */

#define DMA_WORD5_CITER_Pos (16U)
#define DMA_WORD5_CITER_Msk (0x7FFFU << DMA_WORD5_CITER_Pos)                             /*!< 0x0FFF0000 */
#define DMA_WORD5_CITER     DMA_WORD5_CITER_Msk                                         /*!< Current Major Iteration Count */

#define DMA_WORD5_MINORLINKCH_Pos   (28U)
#define DMA_WORD5_MINORLINKCH_Msk   (0x7U << DMA_WORD5_MINORLINKCH_Pos)                 /*!< 0x07000000 */
#define DMA_WORD5_MINORLINKCH       DMA_WORD5_MINORLINKCH_Msk                           /*!< Minor Loop Link Channel Number */

#define DMA_WORD5_MINORELINK_Pos    (31U)
#define DMA_WORD5_MINORELINK_Msk    (0x1U << DMA_WORD5_MINORELINK_Pos)                  /*!< 0x80000000 */
#define DMA_WORD5_MINORELINK        DMA_WORD5_MINORELINK_Msk                            /*!< Enable channel-to-channel linking on minor-loop complete */

/* The count of DMA_WORD5 */
#define DMA_WORD5_COUNT (8U)

/*******************  Bit definition for DMA_WORD6 register  *********************/
#define DMA_WORD6_START_Pos (0U)
#define DMA_WORD6_START_Msk (0x1U << DMA_WORD6_START_Pos)                               /*!< 0x00000001 */
#define DMA_WORD6_START     DMA_WORD6_START_Msk                                         /*!< Channel Start */

#define DMA_WORD6_INTMAJ_Pos    (1U)
#define DMA_WORD6_INTMAJ_Msk    (0x1U << DMA_WORD6_INTMAJ_Pos)                          /*!< 0x00000002 */
#define DMA_WORD6_INTMAJ        DMA_WORD6_INTMAJ_Msk                                    /*!< Enable an interrupt when major iteration count completes */

#define DMA_WORD6_INTHALF_Pos   (2U)
#define DMA_WORD6_INTHALF_Msk   (0x1U << DMA_WORD6_INTHALF_Pos)                         /*!< 0x00000004 */
#define DMA_WORD6_INTHALF       DMA_WORD6_INTHALF_Msk                                   /*!< Enable an interrupt when major counter is half complete */

#define DMA_WORD6_DREQ_Pos  (3U)
#define DMA_WORD6_DREQ_Msk  (0x1U << DMA_WORD6_DREQ_Pos)                                /*!< 0x00000008 */
#define DMA_WORD6_DREQ      DMA_WORD6_DREQ_Msk                                          /*!< Disable Request */

#define DMA_WORD6_ESG_Pos   (4U)
#define DMA_WORD6_ESG_Msk   (0x1U << DMA_WORD6_ESG_Pos)                                 /*!< 0x00000010 */
#define DMA_WORD6_ESG       DMA_WORD6_ESG_Msk                                           /*!< Enable Scatter/Gather Processing */

#define DMA_WORD6_EBE_Pos   (5U)
#define DMA_WORD6_EBE_Msk   (0x1U << DMA_WORD6_EBE_Pos)                                 /*!< 0x00000020 */
#define DMA_WORD6_EBE       DMA_WORD6_EBE_Msk                                           /*!< Enable Big endia */

#define DMA_WORD6_BWC_Pos   (6U)
#define DMA_WORD6_BWC_Msk   (0x3U << DMA_WORD6_BWC_Pos)                                 /*!< 0x000000C0 */
#define DMA_WORD6_BWC       DMA_WORD6_BWC_Msk                                           /*!< Bandwidth Control */

#define DMA_WORD6_MAJORLINKCH_Pos   (8U)
#define DMA_WORD6_MAJORLINKCH_Msk   (0x7U << DMA_WORD6_MAJORLINKCH_Pos)                 /*!< 0x00000700 */
#define DMA_WORD6_MAJORLINKCH       DMA_WORD6_MAJORLINKCH_Msk                           /*!< Major Loop Link Channel Number */

#define DMA_WORD6_MAJORELINK_Pos    (11U)
#define DMA_WORD6_MAJORELINK_Msk    (0x1U << DMA_WORD6_MAJORELINK_Pos)                  /*!< 0x00000800 */
#define DMA_WORD6_MAJORELINK        DMA_WORD6_MAJORELINK_Msk                            /*!< Enable channel-to-channel linking on major loop complete */

/* The count of DMA_WORD6 */
#define DMA_WORD6_COUNT (8U)


/*******************  Bit definition for DMA_WORD0_ATTR register  *********************/
//add by luhj
#define DMA_ATTR_DBURST_MASK    (0x3U)
#define DMA_ATTR_DBURST_SHIFT   (0U)
#define DMA_ATTR_DBURST( x ) ( ( (uint16_t) ( ( (uint16_t) (x) ) << DMA_ATTR_DBURST_SHIFT) ) & DMA_ATTR_DBURST_MASK)
#define DMA_ATTR_DSIZE_MASK     (0xCU)
#define DMA_ATTR_DSIZE_SHIFT    (2U)
#define DMA_ATTR_DSIZE( x ) ( ( (uint16_t) ( ( (uint16_t) (x) ) << DMA_ATTR_DSIZE_SHIFT) ) & DMA_ATTR_DSIZE_MASK)
#define DMA_ATTR_SBURST_MASK    (0x30U)
#define DMA_ATTR_SBURST_SHIFT   (4U)
#define DMA_ATTR_SBURST( x ) ( ( (uint16_t) ( ( (uint16_t) (x) ) << DMA_ATTR_SBURST_SHIFT) ) & DMA_ATTR_SBURST_MASK)
#define DMA_ATTR_SSIZE_MASK     (0xC0U)
#define DMA_ATTR_SSIZE_SHIFT    (6U)
#define DMA_ATTR_SSIZE( x ) ( ( (uint16_t) ( ( (uint16_t) (x) ) << DMA_ATTR_SSIZE_SHIFT) ) & DMA_ATTR_SSIZE_MASK)
#define DMA_ATTR_DMOD_MASK  (0xF00U)
#define DMA_ATTR_DMOD_SHIFT (8U)
#define DMA_ATTR_DMOD( x ) ( ( (uint16_t) ( ( (uint16_t) (x) ) << DMA_ATTR_DMOD_SHIFT) ) & DMA_ATTR_DMOD_MASK)
#define DMA_ATTR_SMOD_MASK  (0xF000U)
#define DMA_ATTR_SMOD_SHIFT (12U)
#define DMA_ATTR_SMOD( x ) ( ( (uint16_t) ( ( (uint16_t) (x) ) << DMA_ATTR_SMOD_SHIFT) ) & DMA_ATTR_SMOD_MASK)


/******************************************************************************/
/*                                                                            */
/*                                    DMAMUX                                  */
/*                                                                            */
/******************************************************************************/
/*******************  Bit definition for DMAMUX_CHCFGx register  ********************/
#define BUSMAT_CHCFG_SOURCE_Pos (0U)
#define BUSMAT_CHCFG_SOURCE_Msk (0x3FU << BUSMAT_CHCFG_SOURCE_Pos)              /*!< 0x0000003F */
#define BUSMAT_CHCFG_SOURCE     BUSMAT_CHCFG_SOURCE_Msk                         /*!<CH_SOURCE[5:0] DMA source select */

#define BUSMAT_CHCFG_ENBL_Pos   (7U)
#define BUSMAT_CHCFG_ENBL_Msk   (0x1U << BUSMAT_CHCFG_ENBL_Pos)                 /*!< 0x00000080 */
#define BUSMAT_CHCFG_ENBL       BUSMAT_CHCFG_ENBL_Msk                           /*!<DMA channel enable */

#define BUSMAT_CHCFG_TRIG_Pos   (8U)
#define BUSMAT_CHCFG_TRIG_Msk   (0x1U << BUSMAT_CHCFG_TRIG_Pos)                 /*!< 0x00000100 */
#define BUSMAT_CHCFG_TRIG       BUSMAT_CHCFG_TRIG_Msk                           /*!<DMA channel trigger */

/* The count of DMAMUX_CHCFG */
#define DMAMUX_CHCFG_COUNT (8U)
//</h>
//<h>EFCR
/******************************************************************************/
/*                                                                            */
/*                                    EFCR_CSR                                */
/*                                                                            */
/******************************************************************************/
/*******************  Bit definition for EFCR_PROGT register  ********************/
#define EFCR_PROGT_TPROG_Pos    (0U)
#define EFCR_PROGT_TPROG_Msk    (0xFFU << EFCR_PROGT_TPROG_Pos)                             /*!< 0x000000FF */
#define EFCR_PROGT_TPROG        EFCR_PROGT_TPROG_Msk                                        /*!<TPROG [7:0]time of PROGEN stay High level when proggramming one time */

/*******************  Bit definition for EFCR_CMDSTAT register  ********************/
#define EFCR_CMDSTAT_DONE_Pos   (0U)
#define EFCR_CMDSTAT_DONE_Msk   (0x1U << EFCR_CMDSTAT_DONE_Pos)                             /*!< 0x00000001 */
#define EFCR_CMDSTAT_DONE       EFCR_CMDSTAT_DONE_Msk                                       /*!<eFUSE command finished flag */

/*******************  Bit definition for EFCR_CMDEN register  ********************/
#define EFCR_CMDEN_CMDEN_Pos    (0U)
#define EFCR_CMDEN_CMDEN_Msk    (0xFFFFFFFFU << EFCR_CMDEN_CMDEN_Pos)                       /*!< 0xFFFFFFFF */
#define EFCR_CMDEN_CMDEN        EFCR_CMDEN_CMDEN_Msk                                        /*!<eFUSE command enable flag [31:0]*/

/*******************  Bit definition for EFCR_COMM register  ********************/
#define EFCR_COMM_COMM_Pos  (0U)
#define EFCR_COMM_COMM_Msk  (0xFFFFFFFFU << EFCR_COMM_COMM_Pos)                             /*!< 0xFFFFFFFF */
#define EFCR_COMM_COMM      EFCR_COMM_COMM_Msk                                              /*!<eFUSE command code [31:0]*/

/*******************  Bit definition for EFCR_CMDPARA register  ********************/
#define EFCR_CMDPARA_CMDPARA_Pos    (0U)
#define EFCR_CMDPARA_CMDPARA_Msk    (0xFFFFU << EFCR_CMDPARA_CMDPARA_Pos)                   /*!< 0x0000FFFF */
#define EFCR_CMDPARA_CMDPARA        EFCR_CMDPARA_CMDPARA_Msk                                /*!<eFUSE command para [15:0]*/

/*******************  Bit definition for EFCR_DOUT register  ********************/
#define EFCR_DOUT_DOUT_Pos  (0U)
#define EFCR_DOUT_DOUT_Msk  (0xFFFFU << EFCR_DOUT_DOUT_Pos)                                 /*!< 0x0000FFFF */
#define EFCR_DOUT_DOUT      EFCR_DOUT_DOUT_Msk                                              /*!<eFUSE command read [15:0]*/
//</h>
//<h>FLASH
/******************************************************************************/
/*                                                                            */
/*                                  FLASH_CSR                                 */
/*                                                                            */
/******************************************************************************/
/*******************  Bit definition for FLASH_CMDSTAT register  ********************/
#define FLASH_CMDSTAT_RESULT_Pos    (0U)
#define FLASH_CMDSTAT_RESULT_Msk    (0x1U << FLASH_CMDSTAT_RESULT_Pos)                      /*!< 0x00000001 */
#define FLASH_CMDSTAT_RESULT        FLASH_CMDSTAT_RESULT_Msk                                /*!<FLASH CMD state */

#define FLASH_CMDSTAT_COMMERR_Pos   (1U)
#define FLASH_CMDSTAT_COMMERR_Msk   (0x1U << FLASH_CMDSTAT_COMMERR_Pos)                     /*!< 0x00000002 */
#define FLASH_CMDSTAT_COMMERR       FLASH_CMDSTAT_COMMERR_Msk                               /*!<FLASH CMD state */

#define FLASH_CMDSTAT_LVTERR_Pos    (3U)
#define FLASH_CMDSTAT_LVTERR_Msk    (0x1U << FLASH_CMDSTAT_LVTERR_Pos)                      /*!< 0x00000001 */
#define FLASH_CMDSTAT_LVTERR        FLASH_CMDSTAT_LVTERR_Msk                                /*!<FLASH CMD state */

#define FLASH_CMDSTAT_AUTHERR_Pos   (4U)
#define FLASH_CMDSTAT_AUTHERR_Msk   (0x1U << FLASH_CMDSTAT_AUTHERR_Pos)                     /*!< 0x00000010 */
#define FLASH_CMDSTAT_AUTHERR       FLASH_CMDSTAT_AUTHERR_Msk                               /*!<flash CMD authority error */

#define FLASH_CMDSTAT_ACCERR_Pos    (5U)
#define FLASH_CMDSTAT_ACCERR_Msk    (0x1U << FLASH_CMDSTAT_ACCERR_Pos)                      /*!< 0x00000020 */
#define FLASH_CMDSTAT_ACCERR        FLASH_CMDSTAT_ACCERR_Msk                                /*!<flash CMD access error */

#define FLASH_CMDSTAT_RDCOLERR_Pos  (6U)
#define FLASH_CMDSTAT_RDCOLERR_Msk  (0x1U << FLASH_CMDSTAT_RDCOLERR_Pos)                    /*!< 0x00000040 */
#define FLASH_CMDSTAT_RDCOLERR      FLASH_CMDSTAT_RDCOLERR_Msk                              /*!<flash read collide error */

#define FLASH_CMDSTAT_DONE_Pos  (7U)
#define FLASH_CMDSTAT_DONE_Msk  (0x1U << FLASH_CMDSTAT_DONE_Pos)                            /*!< 0x00000080 */
#define FLASH_CMDSTAT_DONE      FLASH_CMDSTAT_DONE_Msk                                      /*!<flash read collide error */

/*******************  Bit definition for FLASH_CMDINTE register  ********************/
#define FLASH_CMDINTE_LVT_Pos   (3U)
#define FLASH_CMDINTE_LVT_Msk   (0x1U << FLASH_CMDINTE_LVT_Pos)                             /*!< 0x00000040 */
#define FLASH_CMDINTE_LVT       FLASH_CMDINTE_LVT_Msk                                       /*!<Enable flash read collide interrupt */

#define FLASH_CMDINTE_RDCOL_Pos (6U)
#define FLASH_CMDINTE_RDCOL_Msk (0x1U << FLASH_CMDINTE_RDCOL_Pos)                           /*!< 0x00000040 */
#define FLASH_CMDINTE_RDCOL     FLASH_CMDINTE_RDCOL_Msk                                     /*!<Enable flash read collide interrupt */

#define FLASH_CMDINTE_DONE_Pos  (7U)
#define FLASH_CMDINTE_DONE_Msk  (0x1U << FLASH_CMDINTE_DONE_Pos)                            /*!< 0x00000080 */
#define FLASH_CMDINTE_DONE      FLASH_CMDINTE_DONE_Msk                                      /*!< flash CMD completion interrupt flag */

#define FLASH_CMDINTE_LOCK_Pos  (31U)
#define FLASH_CMDINTE_LOCK_Msk  (0x1U << FLASH_CMDINTE_LOCK_Pos)                            /*!< 0x00000080 */
#define FLASH_CMDINTE_LOCK      FLASH_CMDINTE_LOCK_Msk                                      /*!< flash CMD completion interrupt flag */

/*******************  Bit definition for FLASH_CMDINTF register  ********************/
#define FLASH_CMDINTF_LVT_Pos   (3U)
#define FLASH_CMDINTF_LVT_Msk   (0x1U << FLASH_CMDINTF_LVT_Pos)                             /*!< 0x00000040 */
#define FLASH_CMDINTF_LVT       FLASH_CMDINTF_LVT_Msk                                       /*!<Enable flash read collide interrupt */

#define FLASH_CMDINTF_RDCOL_Pos (6U)
#define FLASH_CMDINTF_RDCOL_Msk (0x1U << FLASH_CMDINTF_RDCOL_Pos)                           /*!< 0x00000040 */
#define FLASH_CMDINTF_RDCOL     FLASH_CMDINTF_RDCOL_Msk                                     /*!<Enable flash read collide interrupt */

#define FLASH_CMDINTF_DONE_Pos  (7U)
#define FLASH_CMDINTF_DONE_Msk  (0x1U << FLASH_CMDINTF_DONE_Pos)                            /*!< 0x00000080 */
#define FLASH_CMDINTF_DONE      FLASH_CMDINTF_DONE_Msk                                      /*!< flash CMD completion interrupt flag */

/*******************  Bit definition for FLASH_PORT register  ********************/
#define FLASH_PORT_TRHR_Pos (0U)
#define FLASH_PORT_TRHR_Msk (0xFFU << FLASH_PORT_TRHR_Pos)                                  /*!< 0x000000FF */
#define FLASH_PORT_TRHR     FLASH_PORT_TRHR_Msk                                             /*!<TRHR[7:0] tRHR */

#define FLASH_PORT_TDPDH_Pos    (8U)
#define FLASH_PORT_TDPDH_Msk    (0xFFU << FLASH_PORT_TDPDH_Pos)                             /*!< 0x0000FF00 */
#define FLASH_PORT_TDPDH        FLASH_PORT_TDPDH_Msk                                        /*!<TDPDH[7:0] tDPDH */

#define FLASH_PORT_TRT_Pos  (16U)
#define FLASH_PORT_TRT_Msk  (0xFFU << FLASH_PORT_TRT_Pos)                                   /*!< 0x000F0000 */
#define FLASH_PORT_TRT      FLASH_PORT_TRT_Msk                                              /*!<TDPDSR[3:0] tDPDSR */

#define FLASH_PORT_TDPDSR_Pos   (24U)
#define FLASH_PORT_TDPDSR_Msk   (0xFU << FLASH_PORT_TDPDSR_Pos)                             /*!< 0x000F0000 */
#define FLASH_PORT_TDPDSR       FLASH_PORT_TDPDSR_Msk                                       /*!<TDPDSR[3:0] tDPDSR */

#define FLASH_PORT_LOCK_Pos (31U)
#define FLASH_PORT_LOCK_Msk (0x1U << FLASH_PORT_LOCK_Pos)                                   /*!< 0x80000000 */
#define FLASH_PORT_LOCK     FLASH_PORT_LOCK_Msk

/*******************  Bit definition for FLASH_RDCFG register  ********************/
#define FLASH_RDCFG_RDM_Pos (0U)
#define FLASH_RDCFG_RDM_Msk (0xFFFFU << FLASH_RDCFG_RDM_Pos)                                /*!< 0x0000FFFF */
#define FLASH_RDCFG_RDM     FLASH_RDCFG_RDM_Msk                                             /*!<RDM[15:0] Flash read mode */

#define FLASH_RDCFG_LPWR_Pos    (16U)
#define FLASH_RDCFG_LPWR_Msk    (0xFFU << FLASH_RDCFG_LPWR_Pos)                             /*!< 0x00FF0000 */
#define FLASH_RDCFG_LPWR        FLASH_RDCFG_LPWR_Msk                                        /*!<LPWR[7:0] Enable LPWR read mode */

#define FLASH_RDCFG_RDBUFEN_Pos (24U)
#define FLASH_RDCFG_RDBUFEN_Msk (0x1U << FLASH_RDCFG_RDBUFEN_Pos)                           /*!< 0x01000000 */
#define FLASH_RDCFG_RDBUFEN     FLASH_RDCFG_RDBUFEN_Msk                                     /*!<RD_BUF_EN Read buffer enable*/

#define FLASH_RDCFG_LOCK_Pos    (31U)
#define FLASH_RDCFG_LOCK_Msk    (0x1U << FLASH_RDCFG_LOCK_Pos)                              /*!< 0x80000000 */
#define FLASH_RDCFG_LOCK        FLASH_RDCFG_LOCK_Msk                                        /*!<LOCK Regist write enable */
/*******************  Bit definition for FLASH_RDT register  ********************/
#define FLASH_RDT_TMS_Pos   (0U)
#define FLASH_RDT_TMS_Msk   (0xFFU << FLASH_RDT_TMS_Pos)                                    /*!< 0x000000FF */
#define FLASH_RDT_TMS       FLASH_RDT_TMS_Msk                                               /*!<TMS[7:0] tMS,time of mode switch */

#define FLASH_RDT_TACCN_Pos (8U)
#define FLASH_RDT_TACCN_Msk (0xFU << FLASH_RDT_TACCN_Pos)                                   /*!< 0x00000F00 */
#define FLASH_RDT_TACCN     FLASH_RDT_TACCN_Msk                                             /*!<TACCN[3:0] tACC of normal read mode */

#define FLASH_RDT_TACCRV_Pos    (12U)
#define FLASH_RDT_TACCRV_Msk    (0xFU << FLASH_RDT_TACCRV_Pos)                              /*!< 0x0000F000 */
#define FLASH_RDT_TACCRV        FLASH_RDT_TACCRV_Msk                                        /*!<TACCRV[3:0] tACC of Recall and  Vread mode */

#define FLASH_RDT_TCKH_Pos  (16U)
#define FLASH_RDT_TCKH_Msk  (0xFU << FLASH_RDT_TCKH_Pos)                                    /*!< 0x000F0000 */
#define FLASH_RDT_TCKH      FLASH_RDT_TCKH_Msk                                              /*!<TCKH[3:0] tCKH,time of clock high */

#define FLASH_RDT_LOCK_Pos  (31U)
#define FLASH_RDT_LOCK_Msk  (0x1U << FLASH_RDT_LOCK_Pos)                                    /*!< 0x80000000 */
#define FLASH_RDT_LOCK      FLASH_RDT_LOCK_Msk                                              /*!<LOCK Regist write enable */
/*******************  Bit definition for FLASH_LDOEN register  ********************/
#define FLASH_LDOEN_LDO16NUM_Pos    (0U)
#define FLASH_LDOEN_LDO16NUM_Msk    (0x0FU << FLASH_LDOEN_LDO16NUM_Pos)                     /*!< 0x0000000F */
#define FLASH_LDOEN_LDO16NUM        FLASH_LDOEN_LDO16NUM_Msk                                /*!<LDO16_NUM */

#define FLASH_LDOEN_LDO16WAIT_Pos   (4U)
#define FLASH_LDOEN_LDO16WAIT_Msk   (0x07U << FLASH_LDOEN_LDO16WAIT_Pos)                    /*!< 0x00000070 */
#define FLASH_LDOEN_LDO16WAIT       FLASH_LDOEN_LDO16WAIT_Msk                               /*!<LDO16_WAIT */

#define FLASH_LDOEN_LDO16EN_Pos (7U)
#define FLASH_LDOEN_LDO16EN_Msk (0x1U << FLASH_LDOEN_LDO16EN_Pos)                           /*!< 0x00000080 */
#define FLASH_LDOEN_LDO16EN     FLASH_LDOEN_LDO16EN_Msk                                     /*!<LDO16_EN */

#define FLASH_LDOEN_LVTFCNT_Pos (8U)
#define FLASH_LDOEN_LVTFCNT_Msk (0x0FU << FLASH_LDOEN_LVTFCNT_Pos)                          /*!< 0x00000700 */
#define FLASH_LDOEN_LVTFCNT     FLASH_LDOEN_LVTFCNT_Msk                                     /*!<LVT_FCNT */

#define FLASH_LDOEN_LVTOFF_Pos  (15U)
#define FLASH_LDOEN_LVTOFF_Msk  (0x1U << FLASH_LDOEN_LVTOFF_Pos)                            /*!< 0x80000000 */
#define FLASH_LDOEN_LVTOFF      FLASH_LDOEN_LVTOFF_Msk                                      /*!<LVT_OFF */

#define FLASH_LDOEN_LOCK_Pos    (31U)
#define FLASH_LDOEN_LOCK_Msk    (0x1U << FLASH_LDOEN_LOCK_Pos)                              /*!< 0xFF000000 */
#define FLASH_LDOEN_LOCK        FLASH_LDOEN_LOCK_Msk                                        /*!<LOCK */
/*******************  Bit definition for FLASH_ERAST0 register  ********************/
#define FLASH_ERAST0_TNVSE_Pos  (0U)
#define FLASH_ERAST0_TNVSE_Msk  (0x3FU << FLASH_ERAST0_TNVSE_Pos)                           /*!< 0x0000003F */
#define FLASH_ERAST0_TNVSE      FLASH_ERAST0_TNVSE_Msk                                      /*!< TNVSE[5:0] tNVSE */

#define FLASH_ERAST0_TERSC_Pos  (6U)
#define FLASH_ERAST0_TERSC_Msk  (0x3FFU << FLASH_ERAST0_TERSC_Pos)                          /*!< 0x0000FFC0 */
#define FLASH_ERAST0_TERSC      FLASH_ERAST0_TERSC_Msk                                      /*!< TERSC[9:0] tERSC of chip erase mode */

#define FLASH_ERAST0_TRCVE_Pos  (16U)
#define FLASH_ERAST0_TRCVE_Msk  (0xFFU << FLASH_ERAST0_TRCVE_Pos)                           /*!< 0x00FF0000 */
#define FLASH_ERAST0_TRCVE      FLASH_ERAST0_TRCVE_Msk                                      /*!< TRCVE[7:0],tRCVE,time of recovery */

#define FLASH_ERAST0_TRWE_Pos   (24U)
#define FLASH_ERAST0_TRWE_Msk   (0xFU << FLASH_ERAST0_TRWE_Pos)                             /*!< 0x0F000000 */
#define FLASH_ERAST0_TRWE       FLASH_ERAST0_TRWE_Msk                                       /*!< TRWE[3:0],tRWE */

#define FLASH_ERAST0_LOCK_Pos   (31U)
#define FLASH_ERAST0_LOCK_Msk   (0x1U << FLASH_ERAST0_LOCK_Pos)                             /*!< 0x80000000 */
#define FLASH_ERAST0_LOCK       FLASH_ERAST0_LOCK_Msk                                       /*!< LOCK */
/*******************  Bit definition for FLASH_ERAST1 register  ********************/
#define FLASH_ERAST1_TERSR_Pos  (0U)
#define FLASH_ERAST1_TERSR_Msk  (0xFFU << FLASH_ERAST1_TERSR_Pos)                           /*!< 0x000000FF */
#define FLASH_ERAST1_TERSR      FLASH_ERAST1_TERSR_Msk                                      /*!< TERSR[7:0] time of retry sector erase mode */

#define FLASH_ERAST1_TERSS_Pos  (8U)
#define FLASH_ERAST1_TERSS_Msk  (0xFFU << FLASH_ERAST1_TERSS_Pos)                           /*!< 0x00FF0000 */
#define FLASH_ERAST1_TERSS      FLASH_ERAST1_TERSS_Msk                                      /*!< TERSS[7:0] time of single sector erase mode */

#define FLASH_ERAST1_LOCK_Pos   (31U)
#define FLASH_ERAST1_LOCK_Msk   (0x1U << FLASH_ERAST1_LOCK_Pos)                             /*!< 0x800000000 */
#define FLASH_ERAST1_LOCK       FLASH_ERAST1_LOCK_Msk                                       /*!< LOCK reg */
/*******************  Bit definition for FLASH_PROGT0 register  ********************/
#define FLASH_PROGT0_TNVSP_Pos  (0U)
#define FLASH_PROGT0_TNVSP_Msk  (0x3FU << FLASH_PROGT0_TNVSP_Pos)                           /*!< 0x0000003F */
#define FLASH_PROGT0_TNVSP      FLASH_PROGT0_TNVSP_Msk                                      /*!< TNVSP[5:0] tNVSP */

#define FLASH_PROGT0_TRCVP_Pos  (8U)
#define FLASH_PROGT0_TRCVP_Msk  (0xFFU << FLASH_PROGT0_TRCVP_Pos)                           /*!< 0x0000FF00 */
#define FLASH_PROGT0_TRCVP      FLASH_PROGT0_TRCVP_Msk                                      /*!< TRCVP[7:0],tRCVP */

#define FLASH_PROGT0_TRWP_Pos   (16U)
#define FLASH_PROGT0_TRWP_Msk   (0xFU << FLASH_PROGT0_TRWP_Pos)                             /*!< 0x000F0000 */
#define FLASH_PROGT0_TRWP       FLASH_PROGT0_TRWP_Msk                                       /*!< TRWP[3:0],tRWP */

#define FLASH_PROGT0_LOCK_Pos   (31U)
#define FLASH_PROGT0_LOCK_Msk   (0x1U << FLASH_PROGT0_LOCK_Pos)                             /*!< 0x800000000 */
#define FLASH_PROGT0_LOCK       FLASH_PROGT0_LOCK_Msk                                       /*!< LOCK reg */
/*******************  Bit definition for FLASH_PROGT1 register  ********************/
#define FLASH_PROGT1_TPG_Pos    (0U)
#define FLASH_PROGT1_TPG_Msk    (0xFFU << FLASH_PROGT1_TPG_Pos)                             /*!< 0x0000003F */
#define FLASH_PROGT1_TPG        FLASH_PROGT1_TPG_Msk                                        /*!< TPREPGS0[5:0] tPREPGS0 */

#define FLASH_PROGT1_TPGS_Pos   (8U)
#define FLASH_PROGT1_TPGS_Msk   (0xFFU << FLASH_PROGT1_TPGS_Pos)                            /*!< 0x00003F00 */
#define FLASH_PROGT1_TPGS       FLASH_PROGT1_TPGS_Msk                                       /*!< TPREPGS1[5:0] tPREPGS1 */

#define FLASH_PROGT1_TPREPG_Pos (16U)
#define FLASH_PROGT1_TPREPG_Msk (0x3FU << FLASH_PROGT1_TPREPG_Pos)                          /*!< 0x00FF0000 */
#define FLASH_PROGT1_TPREPG     FLASH_PROGT1_TPREPG_Msk                                     /*!< TPROG[7:0],tPROG */

#define FLASH_PROGT1_TPREPGS_Pos    (24U)
#define FLASH_PROGT1_TPREPGS_Msk    (0x3FU << FLASH_PROGT1_TPREPGS_Pos)                     /*!< 0x3F000000 */
#define FLASH_PROGT1_TPREPGS        FLASH_PROGT1_TPREPGS_Msk                                /*!< TPREPROG[5:0],tPREPROG */

#define FLASH_PROGT1_LOCK_Pos   (31U)
#define FLASH_PROGT1_LOCK_Msk   (0x1U << FLASH_PROGT1_LOCK_Pos)                             /*!< 0x800000000 */
#define FLASH_PROGT1_LOCK       FLASH_PROGT1_LOCK_Msk                                       /*!< LOCK reg */

/*******************  Bit definition for FLASH_CFGT register  ********************/
#define FLASH_CFGT_TCFL_Pos (0U)
#define FLASH_CFGT_TCFL_Msk (0xFFU << FLASH_CFGT_TCFL_Pos)                                  /*!< 0x000000FF */
#define FLASH_CFGT_TCFL     FLASH_CFGT_TCFL_Msk                                             /*!< TCFL[7:0] tCFL */

#define FLASH_CFGT_TCFH_Pos (8U)
#define FLASH_CFGT_TCFH_Msk (0xFU << FLASH_CFGT_TCFH_Pos)                                   /*!< 0x00000F00 */
#define FLASH_CFGT_TCFH     FLASH_CFGT_TCFH_Msk                                             /*!< TCFH[3:0] tCFH */

#define FLASH_CFGT_TCFEN_Pos    (12U)
#define FLASH_CFGT_TCFEN_Msk    (0xFU << FLASH_CFGT_TCFEN_Pos)                              /*!< 0x0000F000 */
#define FLASH_CFGT_TCFEN        FLASH_CFGT_TCFEN_Msk                                        /*!< TCFEN[3:0],tCFEN */

#define FLASH_CFGT_LOCK_Pos (31U)
#define FLASH_CFGT_LOCK_Msk (0x1U << FLASH_CFGT_LOCK_Pos)                                   /*!< 0x800000000 */
#define FLASH_CFGT_LOCK     FLASH_CFGT_LOCK_Msk                                             /*!< LOCK reg */
/*******************  Bit definition for FLASH_VRDT register  ********************/
#define FLASH_VRDT_TMSV_Pos (0U)
#define FLASH_VRDT_TMSV_Msk (0xFFU << FLASH_VRDT_TMSV_Pos)                                  /*!< 0x000000FF */
#define FLASH_VRDT_TMSV     FLASH_VRDT_TMSV_Msk                                             /*!< TMSV[7:0],tMSV */

#define FLASH_VRDT_TACCV_Pos    (8U)
#define FLASH_VRDT_TACCV_Msk    (0xFU << FLASH_VRDT_TACCV_Pos)                              /*!< 0x00000F00 */
#define FLASH_VRDT_TACCV        FLASH_VRDT_TACCV_Msk                                        /*!< TACCV[3:0] tACC of VRead1 mode */

#define FLASH_VRDT_TCKHV_Pos    (16U)
#define FLASH_VRDT_TCKHV_Msk    (0xFU << FLASH_VRDT_TCKHV_Pos)                              /*!< 0x000F0000 */
#define FLASH_VRDT_TCKHV        FLASH_VRDT_TCKHV_Msk                                        /*!< TCKHV[3:0] tCKHV */

#define FLASH_VRDT_LOCK_Pos (31U)
#define FLASH_VRDT_LOCK_Msk (0x1U << FLASH_VRDT_LOCK_Pos)                                   /*!< 0x800000000 */
#define FLASH_VRDT_LOCK     FLASH_VRDT_LOCK_Msk                                             /*!< LOCK reg */
/*******************  Bit definition for FLASH_CMDCFG register  ********************/
#define FLASH_CMDCFG_ERRETRY_Pos    (0U)
#define FLASH_CMDCFG_ERRETRY_Msk    (0x3U << FLASH_CMDCFG_ERRETRY_Pos)                      /*!< 0x00000003 */
#define FLASH_CMDCFG_ERRETRY        FLASH_CMDCFG_ERRETRY_Msk                                /*!< ERRETRY[1:0] Retry config of erase cmd */

#define FLASH_CMDCFG_ERWAY_Pos  (3U)
#define FLASH_CMDCFG_ERWAY_Msk  (0x1U << FLASH_CMDCFG_ERWAY_Pos)                            /*!< 0x00000008 */
#define FLASH_CMDCFG_ERWAY      FLASH_CMDCFG_ERWAY_Msk                                      /*!< erase way */

#define FLASH_CMDCFG_ERVRD_Pos  (4U)
#define FLASH_CMDCFG_ERVRD_Msk  (0x1U << FLASH_CMDCFG_ERVRD_Pos)                            /*!< 0x00000010 */
#define FLASH_CMDCFG_ERVRD      FLASH_CMDCFG_ERVRD_Msk                                      /*!< Enable Vread1 verify */

#define FLASH_CMDCFG_ERNVR_Pos  (5U)
#define FLASH_CMDCFG_ERNVR_Msk  (0x1U << FLASH_CMDCFG_ERNVR_Pos)                            /*!< 0x00000020 */
#define FLASH_CMDCFG_ERNVR      FLASH_CMDCFG_ERNVR_Msk                                      /*!< Enable erase NVR area */

#define FLASH_CMDCFG_ERSIZE_Pos (6U)
#define FLASH_CMDCFG_ERSIZE_Msk (0x3U << FLASH_CMDCFG_ERSIZE_Pos)                           /*!< 0x000000C0 */
#define FLASH_CMDCFG_ERSIZE     FLASH_CMDCFG_ERSIZE_Msk                                     /*!< ERSIZE[1:0],erase size(chip,block,sector) */

#define FLASH_CMDCFG_VRDRECALL_Pos  (8U)
#define FLASH_CMDCFG_VRDRECALL_Msk  (0x1U << FLASH_CMDCFG_VRDRECALL_Pos)                    /*!< 0x00000100 */
#define FLASH_CMDCFG_VRDRECALL      FLASH_CMDCFG_VRDRECALL_Msk                              /*!<VRD_RECALL */

#define FLASH_CMDCFG_VRDVREAD_Pos   (9U)
#define FLASH_CMDCFG_VRDVREAD_Msk   (0x1U << FLASH_CMDCFG_VRDVREAD_Pos)                     /*!< 0x00000200 */
#define FLASH_CMDCFG_VRDVREAD       FLASH_CMDCFG_VRDVREAD_Msk                               /*!< VRD_VREAD */

#define FLASH_CMDCFG_VRDSIZE_Pos    (12U)
#define FLASH_CMDCFG_VRDSIZE_Msk    (0xFU << FLASH_CMDCFG_VRDSIZE_Pos)                      /*!< 0x0000F000 */
#define FLASH_CMDCFG_VRDSIZE        FLASH_CMDCFG_VRDSIZE_Msk                                /*!< VRD_SIZE[3:0],Vread1 verify size*/

#define FLASH_CMDCFG_PREPGEN_Pos    (21U)
#define FLASH_CMDCFG_PREPGEN_Msk    (0x1U << FLASH_CMDCFG_PREPGEN_Pos)                      /*!< 0x00200000 */
#define FLASH_CMDCFG_PREPGEN        FLASH_CMDCFG_PREPGEN_Msk                                /*!< Enable pre program */

#define FLASH_CMDCFG_PGSIZE_Pos (22U)
#define FLASH_CMDCFG_PGSIZE_Msk (0x3U << FLASH_CMDCFG_PGSIZE_Pos)                           /*!< 0x00C00000 */
#define FLASH_CMDCFG_PGSIZE     FLASH_CMDCFG_PGSIZE_Msk                                     /*!< PG_SIZE[1:0],program size */

#define FLASH_CMDCFG_LOCK_Pos   (31U)
#define FLASH_CMDCFG_LOCK_Msk   (0x1U << FLASH_CMDCFG_LOCK_Pos)                             /*!< 0x800000000 */
#define FLASH_CMDCFG_LOCK       FLASH_CMDCFG_LOCK_Msk                                       /*!< LOCK reg */
/*******************  Bit definition for FLASH_CMDEN register  ********************/
#define FLASH_CMDEN_CMDEN_Pos   (0U)
#define FLASH_CMDEN_CMDEN_Msk   (0xFFFFFFFFU << FLASH_CMDEN_CMDEN_Pos)                      /*!< 0xFFFFFFFF */
#define FLASH_CMDEN_CMDEN       FLASH_CMDEN_CMDEN_Pos                                       /*!< CMDEN[31:0] */

/*******************  Bit definition for FLASH_COMM register  ********************/
#define FLASH_COMM_COMM_Pos (0U)
#define FLASH_COMM_COMM_Msk (0xFFFFFFFFU << FLASH_COMM_COMM_Pos)                            /*!< 0xFFFFFFFF */
#define FLASH_COMM_COMM     FLASH_COMM_COMM_Msk                                             /*!< COMM[31:0],select cmd code */

/*******************  Bit definition for FLASH_CMDADDR register  ********************/
#define FLASH_CMDADDR_CMDADDR_Pos   (0U)
#define FLASH_CMDADDR_CMDADDR_Msk   (0xFFFFFFFFU << FLASH_CMDADDR_CMDADDR_Pos)              /*!< 0xFFFFFFFF */
#define FLASH_CMDADDR_CMDADDR       FLASH_CMDADDR_CMDADDR_Msk                               /*!< CMDADDR[31:0], cmd address */

/*******************  Bit definition for FLASH_CMDPARAX register  ********************/
#define FLASH_CMDPARAn_CMDPARA_Pos  (0U)
#define FLASH_CMDPARAn_CMDPARA_Msk  (0xFFFFFFFFU << FLASH_CMDPARAn_CMDPARA_Pos)             /*!< 0xFFFFFFFF */
#define FLASH_CMDPARAn_CMDPARA      FLASH_CMDPARAn_CMDPARA_Msk                              /*!< CMDPARAX[31:0], cmd data */

/*******************  Bit definition for FLASH_CMDPF register  ********************/
#define FLASH_CMDPF_PARAnFLAG_Pos   (0U)
#define FLASH_CMDPF_PARAnFLAG_Msk   (0xFFFFFFFFU << FLASH_CMDPF_PARAnFLAG_Pos)              /*!< 0xFFFFFFFF */
#define FLASH_CMDPF_PARAnFLAG       FLASH_CMDPF_PARAnFLAG_Msk                               /*!< PARAXFLAG[31:0], CMDPARAX active flag */

/*******************  Bit definition for FLASH_MCSCFG register  ********************/
#define FLASH_MCCFG_MCCFG_Pos   (0U)
#define FLASH_MCCFG_MCCFG_Msk   (0xFFFFFFFFU << FLASH_MCCFG_MCCFG_Pos)                      /*!< 0xFFFFFFFF */
#define FLASH_MCCFG_MCCFG       FLASH_MCCFG_MCCFG_Msk                                       /*!< MCSETCFG[31:0], Enable flash ctrl register */

/*******************  Bit definition for FLASH_ENCRY register  ********************/
//read operation
#define FLASH_ENCRY_ADDRENCRYEN_Pos (0U)
#define FLASH_ENCRY_ADDRENCRYEN_Msk (0x1U << FLASH_ENCRY_ADDRENCRYEN_Pos)                   /*!< 0x00000001 */
#define FLASH_ENCRY_ADDRENCRYEN     FLASH_ENCRY_ADDRENCRYEN_Msk                             /*!<ADDR_ENCRY_EN Flash address Encry open enable  */

#define FLASH_ENCRY_DATAENCRYEN_Pos (1U)
#define FLASH_ENCRY_DATAENCRYEN_Msk (0x1U << FLASH_ENCRY_DATAENCRYEN_Pos)                   /*!< 0x00000002 */
#define FLASH_ENCRY_DATAENCRYEN     FLASH_ENCRY_DATAENCRYEN_Msk                             /*!< DATA_ENCRY_EN Data Encry open enable  */

#define FLASH_ENCRY_DATAENCRYCFG_Pos    (2U)
#define FLASH_ENCRY_DATAENCRYCFG_Msk    (0x1U << FLASH_ENCRY_DATAENCRYCFG_Pos)              /*!< 0x00000004 */
#define FLASH_ENCRY_DATAENCRYCFG        FLASH_ENCRY_DATAENCRYCFG_Msk                        /*!< DATA_ENCRY_CFG Data Encry config */

#define FLASH_ENCRY_ECCRCEN_Pos (4U)
#define FLASH_ENCRY_ECCRCEN_Msk (0x1U << FLASH_ENCRY_ECCRCEN_Pos)                           /*!< 0x00000010 */
#define FLASH_ENCRY_ECCRCEN     FLASH_ENCRY_ECCRCEN_Msk                                     /*!<  ECCRC_EN */

#define FLASH_ENCRY_ECCRCCFG_Pos    (5U)
#define FLASH_ENCRY_ECCRCCFG_Msk    (0x1U << FLASH_ENCRY_ECCRCCFG_Pos)                      /*!< 0x00000020 */
#define FLASH_ENCRY_ECCRCCFG        FLASH_ENCRY_ECCRCCFG_Msk                                /*!<  ECCRC_CFG */

#define FLASH_ENCRY_ECCRCPAD_Pos    (6U)
#define FLASH_ENCRY_ECCRCPAD_Msk    (0x1U << FLASH_ENCRY_ECCRCPAD_Pos)                      /*!< 0x00000040 */
#define FLASH_ENCRY_ECCRCPAD        FLASH_ENCRY_ECCRCPAD_Msk                                /*!<  ECCRC_PAD */

#define FLASH_ENCRY_ECCRCCOR_Pos    (7U)
#define FLASH_ENCRY_ECCRCCOR_Msk    (0x1U << FLASH_ENCRY_ECCRCCOR_Pos)                      /*!< 0x00000080 */
#define FLASH_ENCRY_ECCRCCOR        FLASH_ENCRY_ECCRCCOR_Msk                                /*!<  ECCRC_COR */

#define FLASH_ENCRY_RMASKEN_Pos (8U)
#define FLASH_ENCRY_RMASKEN_Msk (0x1U << FLASH_ENCRY_RMASKEN_Pos)                           /*!< 0x00000100 */
#define FLASH_ENCRY_RMASKEN     FLASH_ENCRY_RMASKEN_Msk                                     /*!<  RMASK_EN */

#define FLASH_ENCRY_WMASKEN_Pos (9U)
#define FLASH_ENCRY_WMASKEN_Msk (0x1U << FLASH_ENCRY_WMASKEN_Pos)                           /*!< 0x00000200 */
#define FLASH_ENCRY_WMASKEN     FLASH_ENCRY_WMASKEN_Msk                                     /*!<  WMASK_EN */

#define FLASH_ENCRY_LOCK_Pos    (31U)
#define FLASH_ENCRY_LOCK_Msk    (0x1U << FLASH_ENCRY_LOCK_Pos)                              /*!< 0x80000000 */
#define FLASH_ENCRY_LOCK        FLASH_ENCRY_LOCK_Msk                                        /*!<  LOCK */

/*******************  Bit definition for FLASH_RDNEN register  ********************/
#define FLASH_RDNEN_RDN0EN_Pos  (0U)
#define FLASH_RDNEN_RDN0EN_Msk  (0x1U << FLASH_RDNEN_RDN0EN_Pos)                            /*!< 0x00000001 */
#define FLASH_RDNEN_RDN0EN      FLASH_RDNEN_RDN0EN_Msk                                      /*!<  Open RDN0 area */

#define FLASH_RDNEN_RDN1EN_Pos  (1U)
#define FLASH_RDNEN_RDN1EN_Msk  (0x1U << FLASH_RDNEN_RDN1EN_Pos)                            /*!< 0x00000002 */
#define FLASH_RDNEN_RDN1EN      FLASH_RDNEN_RDN1EN_Msk                                      /*!<  Open RDN1 area */

#define FLASH_RDNEN_RDN2EN_Pos  (2U)
#define FLASH_RDNEN_RDN2EN_Msk  (0x1U << FLASH_RDNEN_RDN2EN_Pos)                            /*!< 0x00000004 */
#define FLASH_RDNEN_RDN2EN      FLASH_RDNEN_RDN2EN_Msk                                      /*!<  Open RDN2 area */

#define FLASH_RDNEN_RDN3EN_Pos  (3U)
#define FLASH_RDNEN_RDN3EN_Msk  (0x1U << FLASH_RDNEN_RDN3EN_Pos)                            /*!< 0x00000008 */
#define FLASH_RDNEN_RDN3EN      FLASH_RDNEN_RDN3EN_Msk                                      /*!<  Open RDN3 area */

#define FLASH_RDNEN_LOCK_Pos    (31U)
#define FLASH_RDNEN_LOCK_Msk    (0x1U << FLASH_RDNEN_LOCK_Pos)                              /*!< 0x80000000 */
#define FLASH_RDNEN_LOCK        FLASH_RDNEN_LOCK_Msk                                        /*!<  LOCK */

/*******************  Bit definition for FLASH_AKEY register  ********************/
#define FLASH_AKEY_AKEY_Pos (0U)
#define FLASH_AKEY_AKEY_Msk (0x7FFU << FLASH_AKEY_AKEY_Pos)                                 /*!< 0x000007FF */
#define FLASH_AKEY_AKEY     FLASH_AKEY_AKEY_Msk                                             /*!< AKEY[10:0] Flash address encry key */

/*******************  Bit definition for FLASH_DKEY register  ********************/
#define FLASH_DKEY_DKEY_Pos (0U)
#define FLASH_DKEY_DKEY_Msk (0xFFFFFFFFU << FLASH_DKEY_DKEY_Pos)                            /*!< 0x000007FF */
#define FLASH_DKEY_DKEY     FLASH_DKEY_DKEY_Msk                                             /*!< DKEY[10:0] Flash data encry key */

/*******************  Bit definition for FLASH_RDNn register  ********************/
//Read operation
#define FLASH_RDNX_RDNX_Pos (0U)
#define FLASH_RDNX_RDNX_Msk (0x1FFU << FLASH_RDNX_RDNX_Pos)                                 /*!< 0x000001FF */
#define FLASH_RDNX_RDNX     FLASH_RDNX_RDNX_Msk                                             /*!< RDNX[8:0]  */

#define FLASH_RDNX_LOCK_Pos (15U)
#define FLASH_RDNX_LOCK_Msk (0x1U << FLASH_RDNX_LOCK_Pos)                                   /*!< 0x80000000 */
#define FLASH_RDNX_LOCK     FLASH_RDNX_LOCK_Msk                                             /*!<  LOCK */

/*******************  Bit definition for FLASH_MAINLOCK register  ********************/
#define FLASH_MBLKLOCK_BLKXLOCKB_Pos    (0U)
#define FLASH_MBLKLOCK_BLKXLOCKB_Msk    (0xFFFFFU << FLASH_MBLKLOCK_BLKXLOCKB_Pos)          /*!< 0x000FFFFF */
#define FLASH_MBLKLOCK_BLKXLOCKB        FLASH_MBLKLOCK_BLKXLOCKB_Msk                        /*!< BLKXLOCK[19:0]  */

/*******************  Bit definition for FLASH_NVRULOCK register  ********************/
//read operation
#define FLASH_NVRULOCK_NVR0LOCKB_Pos    (0U)
#define FLASH_NVRULOCK_NVR0LOCKB_Msk    (0x1U << FLASH_NVRULOCK_NVR0LOCKB_Pos)              /*!< 0x00000001 */
#define FLASH_NVRULOCK_NVR0LOCKB        FLASH_NVRULOCK_NVR0LOCKB_Msk                        /*!<NVR0_LOCKn Enable NVR0 lock  */

#define FLASH_NVRULOCK_NVR1LOCKB_Pos    (1U)
#define FLASH_NVRULOCK_NVR1LOCKB_Msk    (0x1U << FLASH_NVRULOCK_NVR1LOCKB_Pos)              /*!< 0x00000002 */
#define FLASH_NVRULOCK_NVR1LOCKB        FLASH_NVRULOCK_NVR1LOCKB_Msk                        /*!<NVR1_LOCKn Enable NVR1 lock  */

/*******************  Bit definition for FLASH_OTPLOCK register  ********************/
#define FLASH_OTPLOCK_OTPXLOCKB_Pos (0U)
#define FLASH_OTPLOCK_OTPXLOCKB_Msk (0xFFFFFFFFU << FLASH_OTPLOCK_OTPXLOCKB_Pos)            /*!< 0xFFFFFFFF */
#define FLASH_OTPLOCK_OTPXLOCKB     FLASH_OTPLOCK_OTPXLOCKB_Msk                             /*!< NSX_Lock[31:0] SectionX lock flag  */
//</h>
//<h>FSMC
/******************************************************************************/
/*                                                                            */
/*                                    FSMC                                    */
/*                                                                            */
/******************************************************************************/
/*******************  Bit definition for FSMC_CTRL register  ********************/
#define FSMC_CRTL_EN_Pos    (0U)
#define FSMC_CRTL_EN_Msk    (0x1U << FSMC_CRTL_EN_Pos)                          /*!< 0x00000001 */
#define FSMC_CRTL_EN        FSMC_CRTL_EN_Msk                                    /*!<FMC_EN */

#define FSMC_CRTL_MODE_Pos  (1U)
#define FSMC_CRTL_MODE_Msk  (0x7U << FSMC_CRTL_MODE_Pos)                        /*!< 0x0000000E */
#define FSMC_CRTL_MODE      FSMC_CRTL_MODE_Msk                                  /*!<FMC_MODE[2:0] */

#define FSMC_CRTL_WIDTH_Pos (4U)
#define FSMC_CRTL_WIDTH_Msk (0x3U << FSMC_CRTL_WIDTH_Pos)                       /*!< 0x00000030 */
#define FSMC_CRTL_WIDTH     FSMC_CRTL_WIDTH_Msk                                 /*!<MWIDTH[1:0] */

#define FSMC_CRTL_WAITEN_Pos    (6U)
#define FSMC_CRTL_WAITEN_Msk    (0x1U << FSMC_CRTL_WAITEN_Pos)                  /*!< 0x00000040 */
#define FSMC_CRTL_WAITEN        FSMC_CRTL_WAITEN_Msk                            /*!<WAIT_EN */

#define FSMC_CRTL_WAITPOL_Pos   (7U)
#define FSMC_CRTL_WAITPOL_Msk   (0x1U << FSMC_CRTL_WAITPOL_Pos)                 /*!< 0x00000080 */
#define FSMC_CRTL_WAITPOL       FSMC_CRTL_WAITPOL_Msk                           /*!<WAIT_POL */

#define FSMC_CRTL_CKKEN_Pos (8U)
#define FSMC_CRTL_CKKEN_Msk (0x1U << FSMC_CRTL_CKKEN_Pos)                       /*!< 0x00000100 */
#define FSMC_CRTL_CKKEN     FSMC_CRTL_CKKEN_Msk                                 /*!<CLK_EN */

#define FSMC_CRTL_LCDTYPE_Pos   (16U)
#define FSMC_CRTL_LCDTYPE_Msk   (0x1U << FSMC_CRTL_LCDTYPE_Pos)                 /*!< 0x00010000 */
#define FSMC_CRTL_LCDTYPE       FSMC_CRTL_LCDTYPE_Msk                           /*!<LCD_TYPE */

#define FSMC_CRTL_ENPOL_Pos (17U)
#define FSMC_CRTL_ENPOL_Msk (0x1U << FSMC_CRTL_ENPOL_Pos)                       /*!< 0x00020000 */
#define FSMC_CRTL_ENPOL     FSMC_CRTL_ENPOL_Msk                                 /*!<EN_POL */

#define FSMC_CRTL_RWPOL_Pos (18U)
#define FSMC_CRTL_RWPOL_Msk (0x1U << FSMC_CRTL_RWPOL_Pos)                       /*!< 0x00040000 */
#define FSMC_CRTL_RWPOL     FSMC_CRTL_RWPOL_Msk                                 /*!<RW_POL */

#define FSMC_CRTL_LOCK_Pos  (31U)
#define FSMC_CRTL_LOCK_Msk  (0x1U << FSMC_CRTL_LOCK_Pos)                        /*!< 0x80000000 */
#define FSMC_CRTL_LOCK      FSMC_CRTL_LOCK_Msk                                  /*!<LOCK */

/*******************  Bit definition for FSMC_ARDT register  ********************/
#define FSMC_ARDT_RDADDSET_Pos  (0U)
#define FSMC_ARDT_RDADDSET_Msk  (0xFU << FSMC_ARDT_RDADDSET_Pos)                /*!< 0x0000000F */
#define FSMC_ARDT_RDADDSET      FSMC_ARDT_RDADDSET_Msk                          /*!<RD_ADDSET */

#define FSMC_ARDT_RDADDHLD_Pos  (4U)
#define FSMC_ARDT_RDADDHLD_Msk  (0xFU << FSMC_ARDT_RDADDHLD_Pos)                /*!< 0x000000F0 */
#define FSMC_ARDT_RDADDHLD      FSMC_ARDT_RDADDHLD_Msk                          /*!<RD_ADDHLD */

#define FSMC_ARDT_RDDATAST_Pos  (8U)
#define FSMC_ARDT_RDDATAST_Msk  (0xFFU << FSMC_ARDT_RDDATAST_Pos)               /*!< 0x0000FF00 */
#define FSMC_ARDT_RDDATAST      FSMC_ARDT_RDDATAST_Msk                          /*!<RD_DATAST */

#define FSMC_ARDT_RDBUSRDY_Pos  (16U)
#define FSMC_ARDT_RDBUSRDY_Msk  (0xFU << FSMC_ARDT_RDBUSRDY_Pos)                /*!< 0x000F0000 */
#define FSMC_ARDT_RDBUSRDY      FSMC_ARDT_RDBUSRDY_Msk                          /*!<RD_DATAST */

#define FSMC_ARDT_RDDATAHLD_Pos (20U)
#define FSMC_ARDT_RDDATAHLD_Msk (0xFU << FSMC_ARDT_RDDATAHLD_Pos)               /*!< 0x00F00000 */
#define FSMC_ARDT_RDDATAHLD     FSMC_ARDT_RDDATAHLD_Msk                         /*!<RD_DATAHD */

#define FSMC_ARDT_LOCK_Pos  (31U)
#define FSMC_ARDT_LOCK_Msk  (0x1U << FSMC_ARDT_LOCK_Pos)                        /*!< 0x80000000 */
#define FSMC_ARDT_LOCK      FSMC_ARDT_LOCK_Msk                                  /*!<LOCK */

/*******************  Bit definition for FSMC_AWRT register  ********************/
#define FSMC_AWRT_WRADDSET_Pos  (0U)
#define FSMC_AWRT_WRADDSET_Msk  (0xFU << FSMC_AWRT_WRADDSET_Pos)                /*!< 0x0000000F */
#define FSMC_AWRT_WRADDSET      FSMC_AWRT_WRADDSET_Msk                          /*!<WR_ADDSET */

#define FSMC_AWRT_WRADDHLD_Pos  (4U)
#define FSMC_AWRT_WRADDHLD_Msk  (0xFU << FSMC_AWRT_WRADDHLD_Pos)                /*!< 0x000000F0 */
#define FSMC_AWRT_WRADDHLD      FSMC_AWRT_WRADDHLD_Msk                          /*!<WR_ADDHLD */

#define FSMC_AWRT_WRDATAST_Pos  (8U)
#define FSMC_AWRT_WRDATAST_Msk  (0xFFU << FSMC_AWRT_WRDATAST_Pos)               /*!< 0x0000FF00 */
#define FSMC_AWRT_WRDATAST      FSMC_AWRT_WRDATAST_Msk                          /*!<WR_DATAST */

#define FSMC_AWRT_WRBUSRDY_Pos  (16U)
#define FSMC_AWRT_WRBUSRDY_Msk  (0xFU << FSMC_AWRT_WRBUSRDY_Pos)                /*!< 0x000F0000 */
#define FSMC_AWRT_WRBUSRDY      FSMC_AWRT_WRBUSRDY_Msk                          /*!<WR_BUSRDY */

#define FSMC_AWRT_WRDATAHLD_Pos (20U)
#define FSMC_AWRT_WRDATAHLD_Msk (0xFU << FSMC_AWRT_WRDATAHLD_Pos)               /*!< 0x00F00000 */
#define FSMC_AWRT_WRDATAHLD     FSMC_AWRT_WRDATAHLD_Msk                         /*!<WR_DATAHD */

#define FSMC_AWRT_LOCK_Pos  (31U)
#define FSMC_AWRT_LOCK_Msk  (0x1U << FSMC_AWRT_LOCK_Pos)                        /*!< 0x80000000 */
#define FSMC_AWRT_LOCK      FSMC_AWRT_LOCK_Msk                                  /*!<LOCK */

/*******************  Bit definition for FSMC_SYNCT register  ********************/
#define FSMC_SYNCT_CLKDIV_Pos   (0U)
#define FSMC_SYNCT_CLKDIV_Msk   (0xFU << FSMC_SYNCT_CLKDIV_Pos)                 /*!< 0x0000000F */
#define FSMC_SYNCT_CLKDIV       FSMC_SYNCT_CLKDIV_Msk                           /*!<CLK_DIV */

#define FSMC_SYNCT_LOCK_Pos (31U)
#define FSMC_SYNCT_LOCK_Msk (0x1U << FSMC_SYNCT_LOCK_Pos)                       /*!< 0x80000000 */
#define FSMC_SYNCT_LOCK     FSMC_SYNCT_LOCK_Msk                                 /*!<LOCK */

/*******************  Bit definition for FSMC_EXTM register  ********************/
#define FSMC_EXTM_EN_Pos    (0U)
#define FSMC_EXTM_EN_Msk    (0x1U << FSMC_EXTM_EN_Pos)                          /*!< 0x00000001 */
#define FSMC_EXTM_EN        FSMC_EXTM_EN_Msk                                    /*!<EXTM_EN */

#define FSMC_EXTM_RDFIFOEN_Pos  (1U)
#define FSMC_EXTM_RDFIFOEN_Msk  (0x1U << FSMC_EXTM_RDFIFOEN_Pos)                /*!< 0x00000002 */
#define FSMC_EXTM_RDFIFOEN      FSMC_EXTM_RDFIFOEN_Msk                          /*!<RD_FIFO_EN */

#define FSMC_EXTM_WRFIFOEN_Pos  (2U)
#define FSMC_EXTM_WRFIFOEN_Msk  (0x1U << FSMC_EXTM_WRFIFOEN_Pos)                /*!< 0x00000004 */
#define FSMC_EXTM_WRFIFOEN      FSMC_EXTM_WRFIFOEN_Msk                          /*!<WR_FIFO_EN */

#define FSMC_EXTM_GPIOEN_Pos    (3U)
#define FSMC_EXTM_GPIOEN_Msk    (0x1U << FSMC_EXTM_GPIOEN_Pos)                  /*!< 0x00000008 */
#define FSMC_EXTM_GPIOEN        FSMC_EXTM_GPIOEN_Msk                            /*!<GPIO_EN */

#define FSMC_EXTM_LOCK_Pos  (31U)
#define FSMC_EXTM_LOCK_Msk  (0x1U << FSMC_EXTM_LOCK_Pos)                        /*!< 0x80000000 */
#define FSMC_EXTM_LOCK      FSMC_EXTM_LOCK_Msk                                  /*!<LOCK */

/*******************  Bit definition for FSMC_STAT register  ********************/
#define FSMC_STAT_DONE_Pos  (0U)
#define FSMC_STAT_DONE_Msk  (0x1U << FSMC_STAT_DONE_Pos)                        /*!< 0x00000001 */
#define FSMC_STAT_DONE      FSMC_STAT_DONE_Msk                                  /*!<DONE */

#define FSMC_STAT_RSTDONE_Pos   (1U)
#define FSMC_STAT_RSTDONE_Msk   (0x1U << FSMC_STAT_RSTDONE_Pos)                 /*!< 0x00000002 */
#define FSMC_STAT_RSTDONE       FSMC_STAT_RSTDONE_Msk                           /*!<RST_DONE */

#define FSMC_STAT_ERR_Pos   (2U)
#define FSMC_STAT_ERR_Msk   (0x1U << FSMC_STAT_ERR_Pos)                         /*!< 0x00000004 */
#define FSMC_STAT_ERR       FSMC_STAT_ERR_Msk                                   /*!<ERR */

#define FSMC_STAT_FIFORST_Pos   (6U)
#define FSMC_STAT_FIFORST_Msk   (0x1U << FSMC_STAT_FIFORST_Pos)                 /*!< 0x00000040 */
#define FSMC_STAT_FIFORST       FSMC_STAT_FIFORST_Msk                           /*!<FIFO_RST */

#define FSMC_STAT_RW_Pos    (7U)
#define FSMC_STAT_RW_Msk    (0x1U << FSMC_STAT_RW_Pos)                          /*!< 0x00000080 */
#define FSMC_STAT_RW        FSMC_STAT_RW_Msk                                    /*!<RW */

/*******************  Bit definition for FSMC_INTE register  ********************/
#define FSMC_INTE_RD_Pos    (0U)
#define FSMC_INTE_RD_Msk    (0x1U << FSMC_INTE_RD_Pos)                          /*!< 0x00000001 */
#define FSMC_INTE_RD        FSMC_INTE_RD_Msk                                    /*!<INTE_RD */

#define FSMC_INTE_WR_Pos    (1U)
#define FSMC_INTE_WR_Msk    (0x1U << FSMC_INTE_WR_Pos)                          /*!< 0x00000002 */
#define FSMC_INTE_WR        FSMC_INTE_WR_Msk                                    /*!<INTE_WR */

#define FSMC_INTE_ERR_Pos   (2U)
#define FSMC_INTE_ERR_Msk   (0x1U << FSMC_INTE_ERR_Pos)                         /*!< 0x00000004 */
#define FSMC_INTE_ERR       FSMC_INTE_ERR_Msk                                   /*!<INTE_ERR */

#define FSMC_INTE_FIFORD_Pos    (4U)
#define FSMC_INTE_FIFORD_Msk    (0x1U << FSMC_INTE_FIFORD_Pos)                  /*!< 0x00000010 */
#define FSMC_INTE_FIFORD        FSMC_INTE_FIFORD_Msk                            /*!<INTE_RFIFO */

#define FSMC_INTE_FIFOROV_Pos   (5U)
#define FSMC_INTE_FIFOROV_Msk   (0x1U << FSMC_INTE_FIFOROV_Pos)                 /*!< 0x00000020 */
#define FSMC_INTE_FIFOROV       FSMC_INTE_FIFOROV_Msk                           /*!<INTE_RFIFO_OV */

#define FSMC_INTE_FIFOWR_Pos    (6U)
#define FSMC_INTE_FIFOWR_Msk    (0x1U << FSMC_INTE_FIFOWR_Pos)                  /*!< 0x00000040 */
#define FSMC_INTE_FIFOWR        FSMC_INTE_FIFOWR_Msk                            /*!<INTE_WFIFO */

#define FSMC_INTE_FIFOWOV_Pos   (7U)
#define FSMC_INTE_FIFOWOV_Msk   (0x1U << FSMC_INTE_FIFOWOV_Pos)                 /*!< 0x00000080 */
#define FSMC_INTE_FIFOWOV       FSMC_INTE_FIFOWOV_Msk                           /*!<INTE_WFIFO_OV */


#define FSMC_INTE_DMARXE_Pos    (8U)
#define FSMC_INTE_DMARXE_Msk    (0x1U << FSMC_INTE_DMARXE_Pos)                  /*!< 0x00000100 */
#define FSMC_INTE_DMARXE        FSMC_INTE_DMARXE_Msk                            /*!<DMA_RXE */

#define FSMC_INTE_DMATXE_Pos    (9U)
#define FSMC_INTE_DMATXE_Msk    (0x1U << FSMC_INTE_DMATXE_Pos)                  /*!< 0x00000200 */
#define FSMC_INTE_DMATXE        FSMC_INTE_DMATXE_Msk                            /*!<DMA_TXE */

#define FSMC_INTE_LOCK_Pos  (31U)
#define FSMC_INTE_LOCK_Msk  (0x1U << FSMC_INTE_LOCK_Pos)                        /*!< 0x80000000 */
#define FSMC_INTE_LOCK      FSMC_INTE_LOCK_Pos                                  /*!<LOCK */

/*******************  Bit definition for FSMC_INTF register  ********************/
#define FSMC_INTF_RD_Pos    (0U)
#define FSMC_INTF_RD_Msk    (0x1U << FSMC_INTF_RD_Pos)                          /*!< 0x00000001 */
#define FSMC_INTF_RD        FSMC_INTF_RD_Msk                                    /*!<INTF_RD */

#define FSMC_INTF_WR_Pos    (1U)
#define FSMC_INTF_WR_Msk    (0x1U << FSMC_INTF_WR_Pos)                          /*!< 0x00000002 */
#define FSMC_INTF_WR        FSMC_INTF_WR_Msk                                    /*!<INTF_WR */

#define FSMC_INTF_ERR_Pos   (2U)
#define FSMC_INTF_ERR_Msk   (0x1U << FSMC_INTF_ERR_Pos)                         /*!< 0x00000004 */
#define FSMC_INTF_ERR       FSMC_INTF_ERR_Msk                                   /*!<INTF_ERR */

#define FSMC_INTF_FIFORD_Pos    (4U)
#define FSMC_INTF_FIFORD_Msk    (0x1U << FSMC_INTF_FIFORD_Pos)                  /*!< 0x00000010 */
#define FSMC_INTF_FIFORD        FSMC_INTF_FIFORD_Msk                            /*!<INTF_RFIFO */

#define FSMC_INTF_FIFOROV_Pos   (5U)
#define FSMC_INTF_FIFOROV_Msk   (0x1U << FSMC_INTF_FIFOROV_Pos)                 /*!< 0x00000020 */
#define FSMC_INTF_FIFOROV       FSMC_INTF_FIFOROV_Msk                           /*!<INTF_RFIFO_OV */

#define FSMC_INTF_FIFOWR_Pos    (6U)
#define FSMC_INTF_FIFOWR_Msk    (0x1U << FSMC_INTF_FIFOWR_Pos)                  /*!< 0x00000040 */
#define FSMC_INTF_FIFOWR        FSMC_INTF_FIFOWR_Msk                            /*!<INTF_WFIFO */

#define FSMC_INTF_FIFOWOV_Pos   (7U)
#define FSMC_INTF_FIFOWOV_Msk   (0x1U << FSMC_INTF_FIFOWOV_Pos)                 /*!< 0x00000080 */
#define FSMC_INTF_FIFOWOV       FSMC_INTF_FIFOWOV_Msk                           /*!<INTF_WFIFO_OV */

#define FSMC_INTF_DMARXREQ_Pos  (8U)
#define FSMC_INTF_DMARXREQ_Msk  (0x1U << FSMC_INTF_DMARXREQ_Pos)                /*!< 0x00000100 */
#define FSMC_INTF_DMARXREQ      FSMC_INTF_DMARXREQ_Msk                          /*!<DMA_RXE_REQ */

#define FSMC_INTF_DMATXREQ_Pos  (9U)
#define FSMC_INTF_DMATXREQ_Msk  (0x1U << FSMC_INTF_DMATXREQ_Pos)                /*!< 0x00000200 */
#define FSMC_INTF_DMATXREQ      FSMC_INTF_DMATXREQ_Msk                          /*!<DMA_TXE_REQ */
/*******************  Bit definition for FSMC_ADDR register  ********************/
#define FSMC_ADDR_ADDR_Pos  (0U)
#define FSMC_ADDR_ADDR_Msk  (0xFFFFFFFFU << FSMC_ADDR_ADDR_Pos)                 /*!< 0xFFFFFFFF */
#define FSMC_ADDR_ADDR      FSMC_ADDR_ADDR_Msk                                  /*!<ADDR[31:0] */

/*******************  Bit definition for FSMC_WNUM register  ********************/
#define FSMC_WNUM_WORDNUM_Pos   (0U)
#define FSMC_WNUM_WORDNUM_Msk   (0xFFU << FSMC_WNUM_WORDNUM_Pos)                /*!< 0x000000FF */
#define FSMC_WNUM_WORDNUM       FSMC_WNUM_WORDNUM_Msk                           /*!<WORD_NUM[7:0] */

/*******************  Bit definition for FSMC_DATA register  ********************/
#define FSMC_DATA_DATA_Pos  (0U)
#define FSMC_DATA_DATA_Msk  (0xFFFFFFFFU << FSMC_DATA_DATA_Pos)                 /*!< 0xFFFFFFFF */
#define FSMC_DATA_DATA      FSMC_DATA_DATA_Msk                                  /*!<WORD_NUM[31:0] */

/*******************  Bit definition for FSMC_FCFG register  ********************/
#define FSMC_FCFG_RFLEVEL_Pos   (0U)
#define FSMC_FCFG_RFLEVEL_Msk   (0x7U << FSMC_FCFG_RFLEVEL_Pos)                 /*!< 0x00000007 */
#define FSMC_FCFG_RFLEVEL       FSMC_FCFG_RFLEVEL_Msk                           /*!<RFIFO_LEVEL[2:0] */

#define FSMC_FCFG_WFLEVEL_Pos   (4U)
#define FSMC_FCFG_WFLEVEL_Msk   (0x7U << FSMC_FCFG_WFLEVEL_Pos)                 /*!< 0x0x00000070 */
#define FSMC_FCFG_WFLEVEL       FSMC_FCFG_WFLEVEL_Msk                           /*!<WFIFO_LEVEL[2:0] */

#define FSMC_FCFG_LOCK_Pos  (31U)
#define FSMC_FCFG_LOCK_Msk  (0x1U << FSMC_FCFG_LOCK_Pos)                        /*!< 0x80000000 */
#define FSMC_FCFG_LOCK      FSMC_FCFG_LOCK_Msk                                  /*!<LOCK */

/*******************  Bit definition for FSMC_FSTAT register  ********************/
#define FSMC_FSTAT_RFIFOCNT_Pos (0U)
#define FSMC_FSTAT_RFIFOCNT_Msk (0x7U << FSMC_FSTAT_RFIFOCNT_Pos)               /*!< 0x00000007 */
#define FSMC_FSTAT_RFIFOCNT     FSMC_FSTAT_RFIFOCNT_Msk                         /*!<RFIFO_CNT */

#define FSMC_FSTAT_RFEMPTY_Pos  (4U)
#define FSMC_FSTAT_RFEMPTY_Msk  (0x1U << FSMC_FSTAT_RFEMPTY_Pos)                /*!< 0x00000010 */
#define FSMC_FSTAT_RFEMPTY      FSMC_FSTAT_RFEMPTY_Msk                          /*!<RFIFO_EMPTY */

#define FSMC_FSTAT_RFFULL_Pos   (5U)
#define FSMC_FSTAT_RFFULL_Msk   (0x1U << FSMC_FSTAT_RFFULL_Pos)                 /*!< 0x00000020 */
#define FSMC_FSTAT_RFFULL       FSMC_FSTAT_RFFULL_Msk                           /*!<RFIFO_FULL */

#define FSMC_FSTAT_WFCNT_Pos    (8U)
#define FSMC_FSTAT_WFCNT_Msk    (0x7U << FSMC_FSTAT_WFCNT_Pos)                  /*!< 0x00000700 */
#define FSMC_FSTAT_WFCNT        FSMC_FSTAT_WFCNT_Msk                            /*!<WFIFO_CNT */

#define FSMC_FSTAT_WFEMPTY_Pos  (12U)
#define FSMC_FSTAT_WFEMPTY_Msk  (0x1U << FSMC_FSTAT_WFEMPTY_Pos)                /*!< 0x00001000 */
#define FSMC_FSTAT_WFEMPTY      FSMC_FSTAT_WFEMPTY_Msk                          /*!<WFIFO_EMPTY */

#define FSMC_FSTAT_WFFULL_Pos   (13U)
#define FSMC_FSTAT_WFFULL_Msk   (0x1U << FSMC_FSTAT_WFFULL_Pos)                 /*!< 0x00002000 */
#define FSMC_FSTAT_WFFULL       FSMC_FSTAT_WFFULL_Msk                           /*!<WFIFO_FULL */
//</h>
//<h>GPIO_CFG
/******************************************************************************/
/*                                                                            */
/*                                    GPIO_CFG                                    */
/*                                                                            */
/******************************************************************************/
/*******************   Bit definition for GPIO_CFG_PCRn register   ********************/
#define GPIO_CFG_PCR_PS_Pos (0U)
#define GPIO_CFG_PCR_PS_Msk (0x1U << GPIO_CFG_PCR_PS_Pos)                                   /*!< 0x00000001 */
#define GPIO_CFG_PCR_PS     GPIO_CFG_PCR_PS_Msk                                             /*!<Pull select */

#define GPIO_CFG_PCR_PE_Pos (1U)
#define GPIO_CFG_PCR_PE_Msk (0x1U << GPIO_CFG_PCR_PE_Pos)                                   /*!< 0x00000002 */
#define GPIO_CFG_PCR_PE     GPIO_CFG_PCR_PE_Msk                                             /*!<Pull enable */

#define GPIO_CFG_PCR_SRE_Pos    (2U)
#define GPIO_CFG_PCR_SRE_Msk    (0x1U << GPIO_CFG_PCR_SRE_Pos)                              /*!< 0x00000004 */
#define GPIO_CFG_PCR_SRE        GPIO_CFG_PCR_SRE_Msk                                        /*!<Slew rate control */

#define GPIO_CFG_PCR_ODE_Pos    (5U)
#define GPIO_CFG_PCR_ODE_Msk    (0x1U << GPIO_CFG_PCR_ODE_Pos)                              /*!< 0x00000020 */
#define GPIO_CFG_PCR_ODE        GPIO_CFG_PCR_ODE_Msk                                        /*!<Open drain output enable */

#define GPIO_CFG_PCR_DSE_Pos    (6U)
#define GPIO_CFG_PCR_DSE_Msk    (0x1U << GPIO_CFG_PCR_DSE_Pos)                              /*!< 0x00000040 */
#define GPIO_CFG_PCR_DSE        GPIO_CFG_PCR_DSE_Msk                                        /*!<Drive strenth control */

#define GPIO_CFG_PCR_DOCP_Pos   (7U)
#define GPIO_CFG_PCR_DOCP_Msk   (0x1U << GPIO_CFG_PCR_DOCP_Pos)                             /*!< 0x00000080 */
#define GPIO_CFG_PCR_DOCP       GPIO_CFG_PCR_DOCP_Msk                                       /*!<Disable_oeb_ctrl_pe */

#define GPIO_CFG_PCR_MUX_Pos    (8U)
#define GPIO_CFG_PCR_MUX_Msk    (0x7U << GPIO_CFG_PCR_MUX_Pos)                              /*!< 0x00000700 */
#define GPIO_CFG_PCR_MUX        GPIO_CFG_PCR_MUX_Msk                                        /*!< MUX[2:0] Pin mux control */

#define GPIO_CFG_PCR_LK_Pos (15U)
#define GPIO_CFG_PCR_LK_Msk (0x1U << GPIO_CFG_PCR_LK_Pos)                                   /*!< 0x00008000 */
#define GPIO_CFG_PCR_LK     GPIO_CFG_PCR_LK_Msk                                             /*!< Lock register */

#define GPIO_CFG_PCR_IRQC_Pos   (16U)
#define GPIO_CFG_PCR_IRQC_Msk   (0xFU << GPIO_CFG_PCR_IRQC_Pos)                             /*!< 0x000F0000 */
#define GPIO_CFG_PCR_IRQC       GPIO_CFG_PCR_IRQC_Msk                                       /*!< IRQC[3:0] Interrupt config */

#define GPIO_CFG_PCR_WKUPCFG_Pos    (20U)
#define GPIO_CFG_PCR_WKUPCFG_Msk    (0x3U << GPIO_CFG_PCR_WKUPCFG_Pos)                      /*!< 0x00300000 */
#define GPIO_CFG_PCR_WKUPCFG        GPIO_CFG_PCR_WKUPCFG_Msk                                /*!< IRQC[1:0] wakeup edge config */

#define GPIO_CFG_PCR_ISF_Pos    (24U)
#define GPIO_CFG_PCR_ISF_Msk    (0x1U << GPIO_CFG_PCR_ISF_Pos)                              /*!< 0x01000000 */
#define GPIO_CFG_PCR_ISF        GPIO_CFG_PCR_ISF_Msk                                        /*!< Interrupt flag */

/* The count of GPIO_CFG_PCR */
#define GPIO_CFG_PCR_COUNT (16U)

/*******************   Bit definition for GPIO_CFG_GPCLR register   ********************/
#define GPIO_CFG_GPCLR_GPWD_Pos (0U)
#define GPIO_CFG_GPCLR_GPWD_Msk (0xFFFFU << GPIO_CFG_GPCLR_GPWD_Pos)                        /*!< 0x0000FFFF */
#define GPIO_CFG_GPCLR_GPWD     GPIO_CFG_GPCLR_GPWD_Msk                                     /*!< GPWD[15:0] PCR[15-0] write data */

#define GPIO_CFG_GPCLR_GPWE_Pos (16U)
#define GPIO_CFG_GPCLR_GPWE_Msk (0xFFFFU << GPIO_CFG_GPCLR_GPWE_Pos)                        /*!< 0xFFFF0000 */
#define GPIO_CFG_GPCLR_GPWE     GPIO_CFG_GPCLR_GPWE_Msk                                     /*!< GPWE[15:0] PCR[15-0] write enable */

/*******************   Bit definition for GPIO_CFG_GPCHR register   ********************/
#define GPIO_CFG_GPCHR_GPWD_Pos (0U)
#define GPIO_CFG_GPCHR_GPWD_Msk (0xFFFFU << GPIO_CFG_GPCHR_GPWD_Pos)                        /*!< 0x0000FFFF */
#define GPIO_CFG_GPCHR_GPWD     GPIO_CFG_GPCHR_GPWD_Msk                                     /*!< GPWD[15:0] PCR[31-16] write data */

#define GPIO_CFG_GPCHR_GPWE_Pos (16U)
#define GPIO_CFG_GPCHR_GPWE_Msk (0xFFFFU << GPIO_CFG_GPCHR_GPWE_Pos)                        /*!< 0xFFFF0000 */
#define GPIO_CFG_GPCHR_GPWE     GPIO_CFG_GPCHR_GPWE_Msk                                     /*!< GPWE[15:0] PCR[31-16] write enable */

/*******************   Bit definition for GPIO_CFG_ISFR register   ********************/
#define GPIO_CFG_ISFR_ISF_Pos   (0U)
#define GPIO_CFG_ISFR_ISF_Msk   (0xFFFFU << GPIO_CFG_ISFR_ISF_Pos)                          /*!< 0x0000FFFF */
#define GPIO_CFG_ISFR_ISF       GPIO_CFG_ISFR_ISF_Msk                                       /*!< ISF[15:0] Interrupt flag */

/*******************   Bit definition for GPIO_CFG_DFER register   ********************/
#define GPIO_CFG_DFER_DFE_Pos   (0U)
#define GPIO_CFG_DFER_DFE_Msk   (0xFFFFU << GPIO_CFG_DFER_DFE_Pos)                          /*!< 0x0000FFFF */
#define GPIO_CFG_DFER_DFE       GPIO_CFG_DFER_DFE_Msk                                       /*!< DFE[15:0] Digit filter enable */

/*******************   Bit definition for GPIO_CFG_DFCR register   ********************/
#define GPIO_CFG_DFCR_CS_Pos    (0U)
#define GPIO_CFG_DFCR_CS_Msk    (0x1U << GPIO_CFG_DFCR_CS_Pos)                              /*!< 0x00000001 */
#define GPIO_CFG_DFCR_CS        GPIO_CFG_DFCR_CS_Msk                                        /*!< Digit filter clock select */

/*******************   Bit definition for GPIO_CFG_DFWR register   ********************/
#define GPIO_CFG_DFWR_FILT_Pos  (0U)
#define GPIO_CFG_DFWR_FILT_Msk  (0x3FU << GPIO_CFG_DFWR_FILT_Pos)                           /*!< 0x0000000F */
#define GPIO_CFG_DFWR_FILT      GPIO_CFG_DFWR_FILT_Msk                                      /*!< FILT[5:0] Digit filter width */
//</h>
//<h>GPIO
/******************************************************************************/
/*                                                                            */
/*                                    GPIO                                    */
/*                                                                            */
/******************************************************************************/
/*******************   Bit definition for GPIO_PDOR register   ********************/
#define GPIO_PDOR_PDO_Pos   (0U)
#define GPIO_PDOR_PDO_Msk   (0xFFFFU << GPIO_PDOR_PDO_Pos)                              /*!< 0x0000FFFF */
#define GPIO_PDOR_PDO       GPIO_PDOR_PDO_Msk                                           /*!< PDO[15:0] Port data output */

/*******************   Bit definition for GPIO_PSOR register   ********************/
#define GPIO_PSOR_PTSO_Pos  (0U)
#define GPIO_PSOR_PTSO_Msk  (0xFFFFU << GPIO_PSOR_PTSO_Pos)                             /*!< 0x0000FFFF */
#define GPIO_PSOR_PTSO      GPIO_PSOR_PTSO_Msk                                          /*!< PTSO[15:0] Port set output */

/*******************   Bit definition for GPIO_PCOR register   ********************/
#define GPIO_PCOR_PTCO_Pos  (0U)
#define GPIO_PCOR_PTCO_Msk  (0xFFFFU << GPIO_PCOR_PTCO_Pos)                             /*!< 0x0000FFFF */
#define GPIO_PCOR_PTCO      GPIO_PCOR_PTCO_Msk                                          /*!< PTCO[15:0] Port clear output */

/*******************   Bit definition for GPIO_PTOR register   ********************/
#define GPIO_PTOR_PTTO_Pos  (0U)
#define GPIO_PTOR_PTTO_Msk  (0xFFFFU << GPIO_PTOR_PTTO_Pos)                             /*!< 0x0000FFFF */
#define GPIO_PTOR_PTTO      GPIO_PTOR_PTTO_Msk                                          /*!< PTTO[15:0] Port toggle output */

/*******************   Bit definition for GPIO_PDIR register   ********************/
#define GPIO_PDIR_PDI_Pos   (0U)
#define GPIO_PDIR_PDI_Msk   (0xFFFFU << GPIO_PDIR_PDI_Pos)                              /*!< 0x0000FFFF */
#define GPIO_PDIR_PDI       GPIO_PDIR_PDI_Msk                                           /*!< PDI[15:0] Port data input */

/*******************   Bit definition for GPIO_PDDR register   ********************/
#define GPIO_PDDR_PDD_Pos   (0U)
#define GPIO_PDDR_PDD_Msk   (0xFFFFU << GPIO_PDDR_PDD_Pos)                              /*!< 0x0000FFFF */
#define GPIO_PDDR_PDD       GPIO_PDDR_PDD_Msk                                           /*!< PDD[15:0] Port data direction */

/*******************   Bit definition for GPIO_ODER register   ********************/
#define GPIO_ODER_ODE_Pos   (0U)
#define GPIO_ODER_ODE_Msk   (0xFFFFU << GPIO_ODER_ODE_Pos)                              /*!< 0x0000FFFF */
#define GPIO_ODER_ODE       GPIO_ODER_ODE_Msk                                           /*!< ODE[15:0] Open drain enable for dedicated pins*/

/*******************   Bit definition for GPIO_CLKCTRL register   ********************/
#define GPIO_CLKCTRL_PTAEN_Pos  (0U)
#define GPIO_CLKCTRL_PTAEN_Msk  (0x1U << GPIO_CLKCTRL_PTAEN_Pos)                        /*!< 0x00000001 */
#define GPIO_CLKCTRL_PTAEN      GPIO_CLKCTRL_PTAEN_Msk                                  /*!< PortA clock enable*/

#define GPIO_CLKCTRL_PTBEN_Pos  (1U)
#define GPIO_CLKCTRL_PTBEN_Msk  (0x1U << GPIO_CLKCTRL_PTBEN_Pos)                        /*!< 0x00000002 */
#define GPIO_CLKCTRL_PTBEN      GPIO_CLKCTRL_PTBEN_Msk                                  /*!< PortB clock enable*/

#define GPIO_CLKCTRL_PTCEN_Pos  (2U)
#define GPIO_CLKCTRL_PTCEN_Msk  (0x1U << GPIO_CLKCTRL_PTCEN_Pos)                        /*!< 0x00000004 */
#define GPIO_CLKCTRL_PTCEN      GPIO_CLKCTRL_PTCEN_Msk                                  /*!< PortC clock enable*/

#define GPIO_CLKCTRL_PTDEN_Pos  (3U)
#define GPIO_CLKCTRL_PTDEN_Msk  (0x1U << GPIO_CLKCTRL_PTDEN_Pos)                        /*!< 0x00000008 */
#define GPIO_CLKCTRL_PTDEN      GPIO_CLKCTRL_PTDEN_Msk                                  /*!< PortD clock enable*/

#define GPIO_CLKCTRL_PTEEN_Pos  (4U)
#define GPIO_CLKCTRL_PTEEN_Msk  (0x1U << GPIO_CLKCTRL_PTEEN_Pos)                        /*!< 0x00000010 */
#define GPIO_CLKCTRL_PTEEN      GPIO_CLKCTRL_PTEEN_Msk                                  /*!< PortE clock enable*/

#define GPIO_CLKCTRL_PTFEN_Pos  (5U)
#define GPIO_CLKCTRL_PTFEN_Msk  (0x1U << GPIO_CLKCTRL_PTFEN_Pos)                        /*!< 0x00000020 */
#define GPIO_CLKCTRL_PTFEN      GPIO_CLKCTRL_PTFEN_Msk                                  /*!< PortF clock enable*/

#define GPIO_CLKCTRL_PTASEL_Pos (8U)
#define GPIO_CLKCTRL_PTASEL_Msk (0xFU << GPIO_CLKCTRL_PTASEL_Pos)                       /*!< 0x00000F00 */
#define GPIO_CLKCTRL_PTASEL     GPIO_CLKCTRL_PTASEL_Msk                                 /*!<PTASEL[3:0] PortA clock select*/

#define GPIO_CLKCTRL_PTBSEL_Pos (12U)
#define GPIO_CLKCTRL_PTBSEL_Msk (0xFU << GPIO_CLKCTRL_PTBSEL_Pos)                       /*!< 0x0000F000 */
#define GPIO_CLKCTRL_PTBSEL     GPIO_CLKCTRL_PTBSEL_Msk                                 /*!<PTBSEL[3:0] PortB clock select*/

#define GPIO_CLKCTRL_PTCSEL_Pos (16U)
#define GPIO_CLKCTRL_PTCSEL_Msk (0xFU << GPIO_CLKCTRL_PTCSEL_Pos)                       /*!< 0x000F0000 */
#define GPIO_CLKCTRL_PTCSEL     GPIO_CLKCTRL_PTCSEL_Msk                                 /*!<PTCSEL[3:0] PortC clock select*/

#define GPIO_CLKCTRL_PTDSEL_Pos (20U)
#define GPIO_CLKCTRL_PTDSEL_Msk (0xFU << GPIO_CLKCTRL_PTDSEL_Pos)                       /*!< 0x00F00000 */
#define GPIO_CLKCTRL_PTDSEL     GPIO_CLKCTRL_PTDSEL_Msk                                 /*!<PTDSEL[3:0] PortD clock select*/

#define GPIO_CLKCTRL_PTESEL_Pos (24U)
#define GPIO_CLKCTRL_PTESEL_Msk (0xFU << GPIO_CLKCTRL_PTESEL_Pos)                       /*!< 0x0F000000 */
#define GPIO_CLKCTRL_PTESEL     GPIO_CLKCTRL_PTESEL_Msk                                 /*!<PTESEL[3:0] PortE clock select*/

#define GPIO_CLKCTRL_PTFSEL_Pos (28U)
#define GPIO_CLKCTRL_PTFSEL_Msk (0xFU << GPIO_CLKCTRL_PTFSEL_Pos)                       /*!< 0xF0000000 */
#define GPIO_CLKCTRL_PTFSEL     GPIO_CLKCTRL_PTFSEL_Msk                                 /*!<PTFSEL[3:0] PortF clock select*/

/*******************   Bit definition for GPIO_VBUS_DET register   ********************/
#define GPIO_VBUSDET_DET_Pos    (0U)
#define GPIO_VBUSDET_DET_Msk    (0x1U << GPIO_VBUSDET_DET_Pos)
#define GPIO_VBUSDET_DET        GPIO_VBUSDET_DET_Msk

#define GPIO_VBUSDET_PDEN_Pos   (1U)
#define GPIO_VBUSDET_PDEN_Msk   (0x1U << GPIO_VBUSDET_PDEN_Pos)
#define GPIO_VBUSDET_PDEN       GPIO_VBUSDET_PDEN_Msk
//</h>
//<h>I2C
/******************************************************************************/
/*                                                                            */
/*                                    I2C                                     */
/*                                                                            */
/******************************************************************************/
/*******************   Bit definition for I2C_EN register    ********************/
#define I2C_MODE_Pos    (0U)
#define I2C_MODE_Msk    (0x1U << I2C_MODE_Pos)                                                      /*!< 0x00000001 */
#define I2C_MODE        I2C_MODE_Msk                                                                /*!< I2C_MODE as master*/

#define I2C_MASTER_EN_Pos   (1U)
#define I2C_MASTER_EN_Msk   (0x1U << I2C_MASTER_EN_Pos)                                             /*!< 0x00000002 */
#define I2C_MASTER_EN       I2C_MASTER_EN_Msk                                                       /*!< I2C_MASTER_EN     */
/*******************   Bit definition for MASTER SSPCON register    ********************/
#define I2C_MASTERSSPC0N_SEN_Pos    (0U)
#define I2C_MASTERSSPC0N_SEN_Msk    (0x1U << I2C_MASTERSSPC0N_SEN_Pos)                              /*!< 0x00000001 */
#define I2C_MASTERSSPC0N_SEN        I2C_MASTERSSPC0N_SEN_Msk                                        /*!< Start Generation  */

#define I2C_MASTERSSPC0N_RSEN_Pos   (1U)
#define I2C_MASTERSSPC0N_RSEN_Msk   (0x1U << I2C_MASTERSSPC0N_RSEN_Pos)                             /*!< 0x00000002 */
#define I2C_MASTERSSPC0N_RSEN       I2C_MASTERSSPC0N_RSEN_Msk                                       /*!< Repeated Start Generation  */

#define I2C_MASTERSSPC0N_PEN_Pos    (2U)
#define I2C_MASTERSSPC0N_PEN_Msk    (0x1U << I2C_MASTERSSPC0N_PEN_Pos)                              /*!< 0x00000004 */
#define I2C_MASTERSSPC0N_PEN        I2C_MASTERSSPC0N_PEN_Msk                                        /*!< Stop Generation  */

#define I2C_MASTERSSPC0N_RCEN_Pos   (3U)
#define I2C_MASTERSSPC0N_RCEN_Msk   (0x1U << I2C_MASTERSSPC0N_RCEN_Pos)                             /*!< 0x00000008 */
#define I2C_MASTERSSPC0N_RCEN       I2C_MASTERSSPC0N_RCEN_Msk                                       /*!< receive Enable as master receive mode */

#define I2C_MASTERSSPC0N_ACKEN_Pos  (4U)
#define I2C_MASTERSSPC0N_ACKEN_Msk  (0x1U << I2C_MASTERSSPC0N_ACKEN_Pos)                            /*!< 0x00000010 */
#define I2C_MASTERSSPC0N_ACKEN      I2C_MASTERSSPC0N_ACKEN_Msk                                      /*!< Acknowledge Enable  */
/*******************   Bit definition for MASTER SPSTAUS register    ********************/
#define I2C_MASTERSSPSTAUS_ACKDT_Pos    (0U)
#define I2C_MASTERSSPSTAUS_ACKDT_Msk    (0x1U << I2C_MASTERSSPSTAUS_ACKDT_Pos)                      /*!< 0x00000001 */
#define I2C_MASTERSSPSTAUS_ACKDT        I2C_MASTERSSPSTAUS_ACKDT_Msk                                /*!< Acknowledge date status as master receive mode */

#define I2C_MASTERSSPSTAUS_ACKSTAT_Pos  (1U)
#define I2C_MASTERSSPSTAUS_ACKSTAT_Msk  (0x1U << I2C_MASTERSSPSTAUS_ACKSTAT_Pos)                    /*!< 0x00000002 */
#define I2C_MASTERSSPSTAUS_ACKSTAT      I2C_MASTERSSPSTAUS_ACKSTAT_Msk                              /*!< Acknowledge date status as master send mode */

#define I2C_MASTERSSPSTAUS_BF_Pos   (2U)
#define I2C_MASTERSSPSTAUS_BF_Msk   (0x1U << I2C_MASTERSSPSTAUS_BF_Pos)                             /*!< 0x00000004 */
#define I2C_MASTERSSPSTAUS_BF       I2C_MASTERSSPSTAUS_BF_Msk                                       /*!< master SSP buffer status  */

#define I2C_MASTERSSPSTAUS_S_Pos    (3U)
#define I2C_MASTERSSPSTAUS_S_Msk    (0x1U << I2C_MASTERSSPSTAUS_S_Pos)                              /*!< 0x00000008 */
#define I2C_MASTERSSPSTAUS_S        I2C_MASTERSSPSTAUS_S_Msk                                        /*!< START flag dectect  */

#define I2C_MASTERSSPSTAUS_P_Pos    (4U)
#define I2C_MASTERSSPSTAUS_P_Msk    (0x1U << I2C_MASTERSSPSTAUS_P_Pos)                              /*!< 0x00000010 */
#define I2C_MASTERSSPSTAUS_P        I2C_MASTERSSPSTAUS_P_Msk                                        /*!< STOP flag dectect  */

#define I2C_MASTERSSPSTAUS_RW_Pos   (5U)
#define I2C_MASTERSSPSTAUS_RW_Msk   (0x1U << I2C_MASTERSSPSTAUS_RW_Pos)                             /*!< 0x00000020 */
#define I2C_MASTERSSPSTAUS_RW       I2C_MASTERSSPSTAUS_RW_Msk                                       /*!< data transfer status  */

#define I2C_MASTERSSPSTAUS_WCOL_Pos (7U)
#define I2C_MASTERSSPSTAUS_WCOL_Msk (0x1U << I2C_MASTERSSPSTAUS_WCOL_Pos)                           /*!< 0x00000080 */
#define I2C_MASTERSSPSTAUS_WCOL     I2C_MASTERSSPSTAUS_WCOL_Msk                                     /*!< Write collision status  */

#define I2C_MASTERSSPSTAUS_ALL 0xFF
/*******************   Bit definition for MASTER SSPBRG register    ********************/
#define I2C_MASTERSSPBRG_Pos    (0U)
#define I2C_MASTERSSPBRG_Msk    (0xFFU << I2C_MASTERSSPBRG_Pos)                                     /*!< 0x000000FF */
#define I2C_MASTERSSPBRG        I2C_MASTERSSPBRG_Msk                                                /*!< config Baudrate */

/*******************   Bit definition for MASTER SSPBUF register    ********************/
#define I2C_MASTERSSPBUF_Pos    (0U)
#define I2C_MASTERSSPBUF_Msk    (0xFFU << I2C_MASTERSSPBUF_Pos)                                     /*!< 0x000000FF */
#define I2C_MASTERSSPBUF        I2C_MASTERSSPBRG_Msk                                                /*!< master data buffer */

/*******************   Bit definition for MASTER SSPIR register    ********************/
#define I2C_MASTERSSPIR_IF_Pos  (0U)
#define I2C_MASTERSSPIR_IF_Msk  (0x1U << I2C_MASTERSSPIR_IF_Pos)                                    /*!< 0x00000001 */
#define I2C_MASTERSSPIR_IF      I2C_MASTERSSPIR_IF_Msk                                              /*!< master interrupt flag */

#define I2C_MASTERSSPIR_IE_Pos  (1U)
#define I2C_MASTERSSPIR_IE_Msk  (0x1U << I2C_MASTERSSPIR_IE_Pos)                                    /*!< 0x00000002 */
#define I2C_MASTERSSPIR_IE      I2C_MASTERSSPIR_IE_Msk                                              /*!< master interrupt enable */

#define I2C_MASTERSSPIR_RXDE_Pos    (2U)
#define I2C_MASTERSSPIR_RXDE_Msk    (0x1U << I2C_MASTERSSPIR_RXDE_Pos)                              /*!< 0x00000004 */
#define I2C_MASTERSSPIR_RXDE        I2C_MASTERSSPIR_RXDE_Msk                                        /*!< m_i2c_rx_de */

#define I2C_MASTERSSPIR_TXDE_Pos    (3U)
#define I2C_MASTERSSPIR_TXDE_Msk    (0x1U << I2C_MASTERSSPIR_TXDE_Pos)                              /*!< 0x00000008 */
#define I2C_MASTERSSPIR_TXDE        I2C_MASTERSSPIR_TXDE_Msk                                        /*!< m_i2c_tx_de */

#define I2C_MASTERSSPIR_RFU_Pos (6U)
#define I2C_MASTERSSPIR_RFU_Msk (0x3U << I2C_MASTERSSPIR_RFU_Pos)                                   /*!< 0x000000C0 */
#define I2C_MASTERSSPIR_RFU     I2C_MASTERSSPIR_RFU_Msk                                             /*!< m_rfu */


/*******************   Bit definition for SLAVE SSPCON register    ********************/
#define I2C_SLAVESSPC0N_A10EN_Pos   (1U)
#define I2C_SLAVESSPC0N_A10EN_Msk   (0x1U << I2C_SLAVESSPC0N_A10EN_Pos)                             /*!< 0x00000002 */
#define I2C_SLAVESSPC0N_A10EN       I2C_SLAVESSPC0N_A10EN_Msk                                       /*!< slave address mode   */

#define I2C_SLAVESSPC0N_CKP_Pos (3U)
#define I2C_SLAVESSPC0N_CKP_Msk (0x1U << I2C_SLAVESSPC0N_CKP_Pos)                                   /*!< 0x00000008 */
#define I2C_SLAVESSPC0N_CKP     I2C_SLAVESSPC0N_CKP_Msk                                             /*!< CKP scl control  */

#define I2C_SLAVESSPC0N_ACKEN_Pos   (4U)
#define I2C_SLAVESSPC0N_ACKEN_Msk   (0x1U << I2C_SLAVESSPC0N_ACKEN_Pos)                             /*!< 0x00000010 */
#define I2C_SLAVESSPC0N_ACKEN       I2C_SLAVESSPC0N_ACKEN_Msk                                       /*!< slave Acknowledge Enable  */

#define I2C_SLAVESSPC0N_CKSEN_Pos   (5U)
#define I2C_SLAVESSPC0N_CKSEN_Msk   (0x1U << I2C_SLAVESSPC0N_CKSEN_Pos)                             /*!< 0x00000020 */
#define I2C_SLAVESSPC0N_CKSEN       I2C_SLAVESSPC0N_CKSEN_Msk                                       /*!< clock stretching enable  */

#define I2C_SLAVESSPC0N_RST_Pos (7U)
#define I2C_SLAVESSPC0N_RST_Msk (0x1U << I2C_SLAVESSPC0N_RST_Pos)                                   /*!< 0x00000080 */
#define I2C_SLAVESSPC0N_RST     I2C_SLAVESSPC0N_RST_Msk                                             /*!< write 1 generate rst   */

/*******************   Bit definition for SLAVE SSPSTAT register    ********************/
#define I2C_SLAVESSPSTAUS_BF_Pos    (0U)
#define I2C_SLAVESSPSTAUS_BF_Msk    (0x1U << I2C_SLAVESSPSTAUS_BF_Pos)                              /*!< 0x00000001 */
#define I2C_SLAVESSPSTAUS_BF        I2C_SLAVESSPSTAUS_BF_Msk                                        /*!< slave SSP buffer status */

#define I2C_SLAVESSPSTAUS_DA_Pos    (1U)
#define I2C_SLAVESSPSTAUS_DA_Msk    (0x1U << I2C_SLAVESSPSTAUS_DA_Pos)                              /*!< 0x00000002 */
#define I2C_SLAVESSPSTAUS_DA        I2C_SLAVESSPSTAUS_DA_Msk                                        /*!< indicate slave receive data is address or data */

#define I2C_SLAVESSPSTAUS_RW_Pos    (2U)
#define I2C_SLAVESSPSTAUS_RW_Msk    (0x1U << I2C_SLAVESSPSTAUS_RW_Pos)                              /*!< 0x00000004 */
#define I2C_SLAVESSPSTAUS_RW        I2C_SLAVESSPSTAUS_RW_Msk                                        /*!< indicate slave read or write   */

#define I2C_SLAVESSPSTAUS_SSPOV_Pos (3U)
#define I2C_SLAVESSPSTAUS_SSPOV_Msk (0x1U << I2C_SLAVESSPSTAUS_SSPOV_Pos)                           /*!< 0x00000008 */
#define I2C_SLAVESSPSTAUS_SSPOV     I2C_SLAVESSPSTAUS_SSPOV_Msk                                     /*!< slave buffer overflew  */

#define I2C_SLAVESSPSTAUS_WCOL_Pos  (4U)
#define I2C_SLAVESSPSTAUS_WCOL_Msk  (0x1U << I2C_SLAVESSPSTAUS_WCOL_Pos)                            /*!< 0x00000010 */
#define I2C_SLAVESSPSTAUS_WCOL      I2C_SLAVESSPSTAUS_WCOL_Msk                                      /*!< slave write collision status  */

#define I2C_SLAVESSPSTAUS_P_Pos (5U)
#define I2C_SLAVESSPSTAUS_P_Msk (0x1U << I2C_SLAVESSPSTAUS_P_Pos)                                   /*!< 0x00000020 */
#define I2C_SLAVESSPSTAUS_P     I2C_SLAVESSPSTAUS_P_Msk                                             /*!< slave STOP flag dectect  */

#define I2C_SLAVESSPSTAUS_S_Pos (6U)
#define I2C_SLAVESSPSTAUS_S_Msk (0x1U << I2C_SLAVESSPSTAUS_S_Pos)                                   /*!< 0x00000040 */
#define I2C_SLAVESSPSTAUS_S     I2C_SLAVESSPSTAUS_S_Msk                                             /*!< slave START flag dectect  */

#define I2C_SLAVESSPSTAUS_ALL 0x78
/*******************   Bit definition for SLAVE SSPBUF register    ********************/
#define I2C_SLAVESSPBUF_Pos (0U)
#define I2C_SLAVESSPBUF_Msk (0xFFU << I2C_SLAVESSPBUF_Pos)                                          /*!< 0x000000FF */
#define I2C_SLAVESSPBUF     I2C_SLAVESSPBUF_Msk                                                     /*!< slave data buffer */

/*******************   Bit definition for SLAVE ADDR register    ********************/
#define I2C_SLAVESSPADDR_Pos    (0U)
#define I2C_SLAVESSPADDR_Msk    (0x3FFU << I2C_SLAVESSPADDR_Pos)                                    /*!< 0x000003FF */
#define I2C_SLAVESSPADDR        I2C_SLAVESSPADDR_Msk                                                /*!< slave addr */

/*******************   Bit definition for SLAVE SSPIR register    ********************/
#define I2C_SLAVESSPIR_IF_Pos   (0U)
#define I2C_SLAVESSPIR_IF_Msk   (0x1U << I2C_SLAVESSPIR_IF_Pos)                                     /*!< 0x00000001 */
#define I2C_SLAVESSPIR_IF       I2C_SLAVESSPIR_IF_Msk                                               /*!< slave interrupt flag */

#define I2C_SLAVESSPIR_IE_Pos   (1U)
#define I2C_SLAVESSPIR_IE_Msk   (0x1U << I2C_SLAVESSPIR_IE_Pos)                                     /*!< 0x00000002 */
#define I2C_SLAVESSPIR_IE       I2C_SLAVESSPIR_IE_Msk                                               /*!< slave interrupt enable */

#define I2C_SLAVESSPIR_RXDE_Pos (2U)
#define I2C_SLAVESSPIR_RXDE_Msk (0x1U << I2C_SLAVESSPIR_RXDE_Pos)                                   /*!< 0x00000004 */
#define I2C_SLAVESSPIR_RXDE     I2C_SLAVESSPIR_RXDE_Msk                                             /*!< s_i2c_rx_de */

#define I2C_SLAVESSPIR_TXDE_Pos (3U)
#define I2C_SLAVESSPIR_TXDE_Msk (0x1U << I2C_SLAVESSPIR_TXDE_Pos)                                   /*!< 0x00000008 */
#define I2C_SLAVESSPIR_TXDE     I2C_SLAVESSPIR_TXDE_Msk                                             /*!< s_i2c_tx_de */
//</h>
//<h>INTC
/******************************************************************************/
/*                                                                            */
/*                                    INTC                                    */
/*                                                                            */
/******************************************************************************/
/*******************   Bit definition for INTC_NMICTRL register   ********************/
#define INTC_NMICTRL_PNMIEN_Pos (0U)
#define INTC_NMICTRL_PNMIEN_Msk (0x1U << INTC_NMICTRL_PNMIEN_Pos)                       /*!< 0x00000001 */
#define INTC_NMICTRL_PNMIEN     INTC_NMICTRL_PNMIEN_Msk                                 /*!< p_nmi_en*/

#define INTC_NMICTRL_NMIDIS_Pos (8U)
#define INTC_NMICTRL_NMIDIS_Msk (0x1U << INTC_NMICTRL_NMIDIS_Pos)                       /*!< 0x00000100 */
#define INTC_NMICTRL_NMIDIS     INTC_NMICTRL_NMIDIS_Msk                                 /*!< disable_nmi*/
//</h>
//<h>LPTIMER
/******************************************************************************/
/*                                                                            */
/*                                    LPTIMER                                 */
/*                                                                            */
/******************************************************************************/
/*******************  Bit definition for LPTIMER_CTRL register  ********************/
#define LPTIMER_CTRL_CRSTN_Pos  (0U)
#define LPTIMER_CTRL_CRSTN_Msk  (0x1U << LPTIMER_CTRL_CRSTN_Pos)                            /*!< 0x00000001 */
#define LPTIMER_CTRL_CRSTN      LPTIMER_CTRL_CRSTN_Msk                                      /*!<core_rst_n */

#define LPTIMER_CTRL_RUNEN_Pos  (1U)
#define LPTIMER_CTRL_RUNEN_Msk  (0x1U << LPTIMER_CTRL_RUNEN_Pos)                            /*!< 0x00000002 */
#define LPTIMER_CTRL_RUNEN      LPTIMER_CTRL_RUNEN_Msk                                      /*!<run_en */

/*******************  Bit definition for LPTIMER_CLK_CFG register  ********************/
#define LPTIMER_CLKCTRL_CLKSEL_Pos  (0U)
#define LPTIMER_CLKCTRL_CLKSEL_Msk  (0x3U << LPTIMER_CLKCTRL_CLKSEL_Pos)                    /*!< 0x00000003 */
#define LPTIMER_CLKCTRL_CLKSEL      LPTIMER_CLKCTRL_CLKSEL_Msk                              /*!<pte_pad2vcc_data[1:0] clock select */

#define LPTIMER_CLKCTRL_INTEN_Pos   (2U)
#define LPTIMER_CLKCTRL_INTEN_Msk   (0x1U << LPTIMER_CLKCTRL_INTEN_Pos)                     /*!< 0x00000004 */
#define LPTIMER_CLKCTRL_INTEN       LPTIMER_CLKCTRL_INTEN_Msk                               /*!<int_en  */

#define LPTIMER_CLKCTRL_PRESEN_Pos  (3U)
#define LPTIMER_CLKCTRL_PRESEN_Msk  (0x1U << LPTIMER_CLKCTRL_PRESEN_Pos)                    /*!< 0x00000008 */
#define LPTIMER_CLKCTRL_PRESEN      LPTIMER_CLKCTRL_PRESEN_Msk                              /*!<pres_en  */

#define LPTIMER_CLKCTRL_PRESCFG_Pos (4U)
#define LPTIMER_CLKCTRL_PRESCFG_Msk (0xFU << LPTIMER_CLKCTRL_PRESCFG_Pos)                   /*!< 0x000000F0 */
#define LPTIMER_CLKCTRL_PRESCFG     LPTIMER_CLKCTRL_PRESCFG_Msk                             /*!<pres_cfg[3:0]  */

#define LPTIMER_CLKCTRL_PADCLKSEL_Pos   (8U)
#define LPTIMER_CLKCTRL_PADCLKSEL_Msk   (0xFU << LPTIMER_CLKCTRL_PADCLKSEL_Pos)             /*!< 0x00000F00 */
#define LPTIMER_CLKCTRL_PADCLKSEL       LPTIMER_CLKCTRL_PADCLKSEL_Msk                       /*!<pad_clk_sel[3:0]  */

/*******************  Bit definition for LPTIMER_CFG register  ********************/
#define LPTIMER_CFG_RUNM_Pos    (0U)
#define LPTIMER_CFG_RUNM_Msk    (0x1U << LPTIMER_CFG_RUNM_Pos)                              /*!< 0x00000001 */
#define LPTIMER_CFG_RUNM        LPTIMER_CFG_RUNM_Msk                                        /*!<run_mode */

#define LPTIMER_CFG_FREEM_Pos   (1U)
#define LPTIMER_CFG_FREEM_Msk   (0x1U << LPTIMER_CFG_FREEM_Pos)                             /*!< 0x00000002 */
#define LPTIMER_CFG_FREEM       LPTIMER_CFG_FREEM_Msk                                       /*!<free_mode */

#define LPTIMER_CFG_INPOLAR_Pos (2U)
#define LPTIMER_CFG_INPOLAR_Msk (0x1U << LPTIMER_CFG_INPOLAR_Pos)                           /*!< 0x00000004 */
#define LPTIMER_CFG_INPOLAR     LPTIMER_CFG_INPOLAR_Msk                                     /*!<input_polar */

#define LPTIMER_CFG_INSEL_Pos   (4U)
#define LPTIMER_CFG_INSEL_Msk   (0x1FU << LPTIMER_CFG_INSEL_Pos)                            /*!< 0x000001F0 */
#define LPTIMER_CFG_INSEL       LPTIMER_CFG_INSEL_Msk                                       /*!<input_sel[4:0] */

/*******************  Bit definition for LPTIMER_MOD register  ********************/
#define LPTIMER_MOD_MOD_Pos (0U)
#define LPTIMER_MOD_MOD_Msk (0xFFFFU << LPTIMER_MOD_MOD_Pos)                                /*!< 0x0000FFFF */
#define LPTIMER_MOD_MOD     LPTIMER_MOD_MOD_Msk                                             /*!<mod[15:0] */

/*******************  Bit definition for LPTIMER_CVAL register  ********************/
#define LPTIMER_CVAL_CVAL_Pos   (0U)
#define LPTIMER_CVAL_CVAL_Msk   (0xFFFFU << LPTIMER_CVAL_CVAL_Pos)                          /*!< 0x0000FFFF */
#define LPTIMER_CVAL_CVAL       LPTIMER_CVAL_CVAL_Msk                                       /*!<cval[15:0] */

/*******************  Bit definition for LPTIMER_IFCLR register  ********************/
#define LPTIMER_IFCLR_OVF_Pos   (0U)
#define LPTIMER_IFCLR_OVF_Msk   (0x1U << LPTIMER_IFCLR_OVF_Pos)                             /*!< 0x00000001 */
#define LPTIMER_IFCLR_OVF       LPTIMER_IFCLR_OVF_Msk                                       /*!<ovf_flag_clr */

/*******************  Bit definition for LPTIMER_FLAGSTATUS register  ********************/
#define LPTIMER_FLAGSTATUS_OVF_Pos  (0U)
#define LPTIMER_FLAGSTATUS_OVF_Msk  (0x1U << LPTIMER_FLAGSTATUS_OVF_Pos)                    /*!< 0x00000001 */
#define LPTIMER_FLAGSTATUS_OVF      LPTIMER_FLAGSTATUS_OVF_Msk                              /*!<ovf_flag */
//</h>
//<h>ALM
/******************************************************************************/
/*                                                                            */
/*                                    ALM                                     */
/*                                                                            */
/******************************************************************************/
/*******************  Bit definition for ALM_SENSORE register  ********************/
#define ALM_SENSORE0_ENLIGHT_Pos    (0U)                                            /*!< 0x00000000 */
#define ALM_SENSORE0_ENLIGHT_Msk    (0x1U << ALM_SENSORE0_ENLIGHT_Pos)              /*!<sensor_en_light */
#define ALM_SENSORE0_ENLIGHT        ALM_SENSORE0_ENLIGHT_Msk

#define ALM_SENSORE0_ENVDET_Pos (1U)                                                /*!< 0x00000001 */
#define ALM_SENSORE0_ENVDET_Msk (0x1U << ALM_SENSORE0_ENVDET_Pos)                   /*!<sensor_en_vdet */
#define ALM_SENSORE0_ENVDET     ALM_SENSORE0_ENVDET_Msk

#define ALM_SENSORE0_ENLDO12L_Pos   (3U)                                            /*!< 0x00000004 */
#define ALM_SENSORE0_ENLDO12L_Msk   (0x1U << ALM_SENSORE0_ENLDO12L_Pos)             /*!<sensor_en_ldo12l */
#define ALM_SENSORE0_ENLDO12L       ALM_SENSORE0_ENLDO12L_Msk

#define ALM_SENSORE0_ENLDO16L_Pos   (4U)                                            /*!< 0x00000008 */
#define ALM_SENSORE0_ENLDO16L_Msk   (0x1U << ALM_SENSORE0_ENLDO16L_Pos)             /*!<sensor_en_ldo16l */
#define ALM_SENSORE0_ENLDO16L       ALM_SENSORE0_ENLDO16L_Msk

#define ALM_SENSORE0_ENGLITCH_Pos   (5U)                                            /*!< 0x00000010 */
#define ALM_SENSORE0_ENGLITCH_Msk   (0x1U << ALM_SENSORE0_ENGLITCH_Pos)             /*!<sensor_en_glitch */
#define ALM_SENSORE0_ENGLITCH       ALM_SENSORE0_ENGLITCH_Msk

#define ALM_SENSORE0_ENIRC4M_Pos    (6U)                                            /*!< 0x00000020 */
#define ALM_SENSORE0_ENIRC4M_Msk    (0x1U << ALM_SENSORE0_ENIRC4M_Pos)              /*!<sensor_en_irc4m */
#define ALM_SENSORE0_ENIRC4M        ALM_SENSORE0_ENIRC4M_Msk

#define ALM_SENSORE0_ENLDO16LATSTBY_Pos (8U)                                        /*!< 0x00000020 */
#define ALM_SENSORE0_ENLDO16LATSTBY_Msk (0x1U << ALM_SENSORE0_ENLDO16LATSTBY_Pos)   /*!<sensor_en_irc4m */
#define ALM_SENSORE0_ENLDO16LATSTBY     ALM_SENSORE0_ENLDO16LATSTBY_Msk

#define ALM_SENSORE0_ENLDO12LATSTBY_Pos (9U)                                        /*!< 0x00000020 */
#define ALM_SENSORE0_ENLDO12LATSTBY_Msk (0x1U << ALM_SENSORE0_ENLDO12LATSTBY_Pos)   /*!<sensor_en_irc4m */
#define ALM_SENSORE0_ENLDO12LATSTBY     ALM_SENSORE0_ENLDO12LATSTBY_Msk

#define ALM_SENSORE0_ENVDDATSTBY_Pos    (10U)                                       /*!< 0x00000020 */
#define ALM_SENSORE0_ENVDDATSTBY_Msk    (0x1U << ALM_SENSORE0_ENVDDATSTBY_Pos)      /*!<sensor_en_irc4m */
#define ALM_SENSORE0_ENVDDATSTBY        ALM_SENSORE0_ENVDDATSTBY_Msk

#define ALM_SENSORE0_ENLDO16LATPD_Pos   (12U)                                       /*!< 0x00000020 */
#define ALM_SENSORE0_ENLDO16LATPD_Msk   (0x1U << ALM_SENSORE0_ENLDO16LATPD_Pos)     /*!<sensor_en_irc4m */
#define ALM_SENSORE0_ENLDO16LATPD       ALM_SENSORE0_ENLDO16LATPD_Msk

#define ALM_SENSORE0_ENLDO12LATPD_Pos   (13U)                                       /*!< 0x00000020 */
#define ALM_SENSORE0_ENLDO12LATPD_Msk   (0x1U << ALM_SENSORE0_ENLDO12LATPD_Pos)     /*!<sensor_en_irc4m */
#define ALM_SENSORE0_ENLDO12LATPD       ALM_SENSORE0_ENLDO12LATPD_Msk

#define ALM_SENSORE0_ENVDDATPD_Pos  (14U)                                           /*!< 0x00000020 */
#define ALM_SENSORE0_ENVDDATPD_Msk  (0x1U << ALM_SENSORE0_ENVDDATPD_Pos)            /*!<sensor_en_irc4m */
#define ALM_SENSORE0_ENVDDATPD      ALM_SENSORE0_ENVDDATPD_Msk

/*******************  Bit definition for ALM_SENSORE1 register  ********************/
#define ALM_SENSORE1_ENFRPTA_Pos    (7U)                                            /*!< 0x00000080 */
#define ALM_SENSORE1_ENFRPTA_Msk    (0x1U << ALM_SENSORE1_ENFRPTA_Pos)              /*!<sensor_en_frequency_rpt_a */
#define ALM_SENSORE1_ENFRPTA        ALM_SENSORE1_ENFRPTA_Msk

#define ALM_SENSORE1_ENFRPTB_Pos    (8U)                                            /*!< 0x00000100 */
#define ALM_SENSORE1_ENFRPTB_Msk    (0x1U << ALM_SENSORE1_ENFRPTB_Pos)              /*!<sensor_en_frequency_rpt_b */
#define ALM_SENSORE1_ENFRPTB        ALM_SENSORE1_ENFRPTB_Msk

#define ALM_SENSORE1_ENSLD_Pos  (12U)
#define ALM_SENSORE1_ENSLD_Msk  (0x1U << ALM_SENSORE1_ENSLD_Pos)                    /*!< 0x00001000 */
#define ALM_SENSORE1_ENSLD      ALM_SENSORE1_ENSLD_Msk                              /*!<sensor_en_shield */

/*******************  Bit definition for ALM_SENSORE0_CFG register  ********************/
#define ALM_SENSORE0_VDDH_Pos   (3U)                                                /*!< 0x00000008 */
#define ALM_SENSORE0_VDDH_Msk   (0x1U << ALM_SENSORE0_VDDH_Pos)                     /*!<sensor_cfg_vdet_h */
#define ALM_SENSORE0_VDDH       ALM_SENSORE0_VDDH_Msk

#define ALM_SENSORE0_BOR_Pos    (2U)                                                /*!< 0x00000004 */
#define ALM_SENSORE0_BOR_Msk    (0x1U << ALM_SENSORE0_BOR_Pos)                      /*!<sensor_cfg_bor */
#define ALM_SENSORE0_BOR        ALM_SENSORE0_BOR_Msk

#define ALM_SENSORE0_VDDL_Pos   (0U)
#define ALM_SENSORE0_VDDL_Msk   (0x3U << ALM_SENSORE0_VDDL_Pos)                     /*!< 0x00000003 */
#define ALM_SENSORE0_VDDL       ALM_SENSORE0_VDDL_Msk                               /*!<sensor_cfg_vdet_l */


/*******************  Bit definition for ALM_BTCLKCFG register  ********************/
#define ALM_BTCLKCFG_T0CLKASEL_Pos  (0U)
#define ALM_BTCLKCFG_T0CLKASEL_Msk  (0xFU << ALM_BTCLKCFG_T0CLKASEL_Pos)            /*!< 0x0000000F */
#define ALM_BTCLKCFG_T0CLKASEL      ALM_BTCLKCFG_T0CLKASEL_Msk                      /*!<base_timer0_clka_sel[3:0] */

#define ALM_BTCLKCFG_T0CLKBSEL_Pos  (4U)
#define ALM_BTCLKCFG_T0CLKBSEL_Msk  (0xFU << ALM_BTCLKCFG_T0CLKBSEL_Pos)            /*!< 0x000000F0 */
#define ALM_BTCLKCFG_T0CLKBSEL      ALM_BTCLKCFG_T0CLKBSEL_Msk                      /*!<base_timer0_clkb_sel[3:0] */

#define ALM_BTCLKCFG_T1CLKASEL_Pos  (8U)
#define ALM_BTCLKCFG_T1CLKASEL_Msk  (0xFU << ALM_BTCLKCFG_T1CLKASEL_Pos)            /*!< 0x00000F00 */
#define ALM_BTCLKCFG_T1CLKASEL      ALM_BTCLKCFG_T1CLKASEL_Msk                      /*!<base_timer1_clka_sel[3:0] */

#define ALM_BTCLKCFG_T1CLKBSEL_Pos  (12U)
#define ALM_BTCLKCFG_T1CLKBSEL_Msk  (0xFU << ALM_BTCLKCFG_T1CLKBSEL_Pos)            /*!< 0x00000F00 */
#define ALM_BTCLKCFG_T1CLKBSEL      ALM_BTCLKCFG_T1CLKBSEL_Msk                      /*!<base_timer1_clkb_sel[3:0] */

#define ALM_BTCLKCFG_GTBS_T0CLKA_Pos    (16U)
#define ALM_BTCLKCFG_GTBS_T0CLKA_Msk    (0x1U << ALM_BTCLKCFG_GTBS_T0CLKA_Pos)      /*!< 0x00010000 */
#define ALM_BTCLKCFG_GTBS_T0CLKA        ALM_BTCLKCFG_GTBS_T0CLKA_Msk                /*!<cfg_gate_base_timer0_clka */

#define ALM_BTCLKCFG_GTBS_T0CLKB_Pos    (17U)
#define ALM_BTCLKCFG_GTBS_T0CLKB_Msk    (0x1U << ALM_BTCLKCFG_GTBS_T0CLKB_Pos)      /*!< 0x00020000 */
#define ALM_BTCLKCFG_GTBS_T0CLKB        ALM_BTCLKCFG_GTBS_T0CLKB_Msk                /*!<cfg_gate_base_timer0_clkb */

#define ALM_BTCLKCFG_GTBS_T1CLKA_Pos    (18U)
#define ALM_BTCLKCFG_GTBS_T1CLKA_Msk    (0x1U << ALM_BTCLKCFG_GTBS_T1CLKA_Pos)      /*!< 0x00040000 */
#define ALM_BTCLKCFG_GTBS_T1CLKA        ALM_BTCLKCFG_GTBS_T1CLKA_Msk                /*!<cfg_gate_base_timer1_clka */

#define ALM_BTCLKCFG_GTBS_T1CLKB_Pos    (19U)
#define ALM_BTCLKCFG_GTBS_T1CLKB_Msk    (0x1U << ALM_BTCLKCFG_GTBS_T1CLKB_Pos)      /*!< 0x00080000 */
#define ALM_BTCLKCFG_GTBS_T1CLKB        ALM_BTCLKCFG_GTBS_T1CLKB_Msk                /*!<cfg_gate_base_timer1_clkb */

/*******************  Bit definition for ALM_BT0CFG register  ********************/
#define ALM_BT0CFG_BST0EN_Pos   (0U)
#define ALM_BT0CFG_BST0EN_Msk   (0x1U << ALM_BT0CFG_BST0EN_Pos)                     /*!< 0x00000001 */
#define ALM_BT0CFG_BST0EN       ALM_BT0CFG_BST0EN_Msk                               /*!<base_timer0_en */

#define ALM_BT0CFG_BST0FDETEN_Pos   (1U)
#define ALM_BT0CFG_BST0FDETEN_Msk   (0x1U << ALM_BT0CFG_BST0FDETEN_Pos)             /*!< 0x00000002 */
#define ALM_BT0CFG_BST0FDETEN       ALM_BT0CFG_BST0FDETEN_Msk                       /*!<base_timer0_fdet_en */

#define ALM_BT0CFG_BST0MD_Pos   (2U)
#define ALM_BT0CFG_BST0MD_Msk   (0x1U << ALM_BT0CFG_BST0MD_Pos)                     /*!< 0x00000004 */
#define ALM_BT0CFG_BST0MD       ALM_BT0CFG_BST0MD_Msk                               /*!<base_timer0_mode */

/*******************  Bit definition for ALM_BT0CLKAC register  ********************/
#define ALM_BT0CLKAC_TH_Pos (0U)
#define ALM_BT0CLKAC_TH_Msk (0xFFFFFFU << ALM_BT0CLKAC_TH_Pos)                      /*!< 0x00FFFFFF */
#define ALM_BT0CLKAC_TH     ALM_BT0CLKAC_TH_Msk                                     /*!<base_timer0_clka_th */

/*******************  Bit definition for ALM_BT0CLKBCL register  ********************/
#define ALM_BT0CLKBCL_TH_Pos    (0U)
#define ALM_BT0CLKBCL_TH_Msk    (0xFFFFFFU << ALM_BT0CLKBCL_TH_Pos)                 /*!< 0x00FFFFFF */
#define ALM_BT0CLKBCL_TH        ALM_BT0CLKBCL_TH_Msk                                /*!<base_timer0_clkb_th_l */

/*******************  Bit definition for ALM_BT0CLKBCH register  ********************/
#define ALM_BT0CLKBCH_TH_Pos    (0U)
#define ALM_BT0CLKBCH_TH_Msk    (0xFFFFFFU << ALM_BT0CLKBCH_TH_Pos)                 /*!< 0x00FFFFFF */
#define ALM_BT0CLKBCH_TH        ALM_BT0CLKBCH_TH_Msk                                /*!<base_timer0_clkb_th_h */

/*******************  Bit definition for ALM_BT0CTRL register  ********************/
#define ALM_BT0CTRL_CLR_Pos (0U)
#define ALM_BT0CTRL_CLR_Msk (0x1U << ALM_BT0CTRL_CLR_Pos)                           /*!< 0x00000001 */
#define ALM_BT0CTRL_CLR     ALM_BT0CTRL_CLR_Msk                                     /*!<base_timer0_clr */

/*******************  Bit definition for ALM_BT0SR register  ********************/
#define ALM_BT0SR_BUSY_Pos  (0U)
#define ALM_BT0SR_BUSY_Msk  (0x1U << ALM_BT0SR_BUSY_Pos)                            /*!< 0x00000001 */
#define ALM_BT0SR_BUSY      ALM_BT0SR_BUSY_Msk                                      /*!<base_timer0_busy */

/*******************  Bit definition for ALM_BT0RPT register  ********************/
#define ALM_BT0RPT_VALID_Pos    (0U)
#define ALM_BT0RPT_VALID_Msk    (0x1U << ALM_BT0RPT_VALID_Pos)                      /*!< 0x00000001 */
#define ALM_BT0RPT_VALID        ALM_BT0RPT_VALID_Msk                                /*!<base_timer0_valid */

#define ALM_BT0RPT_FWARN_Pos    (1U)
#define ALM_BT0RPT_FWARN_Msk    (0x1U << ALM_BT0RPT_FWARN_Pos)                      /*!< 0x00000002 */
#define ALM_BT0RPT_FWARN        ALM_BT0RPT_FWARN_Msk                                /*!<base_timer0_fwarn */

#define ALM_BT0RPT_LWARN_Pos    (2U)
#define ALM_BT0RPT_LWARN_Msk    (0x1U << ALM_BT0RPT_LWARN_Pos)                      /*!< 0x00000004 */
#define ALM_BT0RPT_LWARN        ALM_BT0RPT_LWARN_Msk                                /*!<base_timer0_lwarn */

#define ALM_BT0RPT_HWARN_Pos    (3U)
#define ALM_BT0RPT_HWARN_Msk    (0x1U << ALM_BT0RPT_HWARN_Pos)                      /*!< 0x00000008 */
#define ALM_BT0RPT_HWARN        ALM_BT0RPT_HWARN_Msk                                /*!<base_timer0_hwarn */

#define ALM_BT0RPT_RESULT_Pos   (8U)
#define ALM_BT0RPT_RESULT_Msk   (0xFFFFFFU << ALM_BT0RPT_RESULT_Pos)                /*!< 0xFFFFFF00 */
#define ALM_BT0RPT_RESULT       ALM_BT0RPT_RESULT_Msk                               /*!<base_timer0_result */

/*******************  Bit definition for ALM_BT1CFG register  ********************/
#define ALM_BT1CFG_BST0EN_Pos   (0U)
#define ALM_BT1CFG_BST0EN_Msk   (0x1U << ALM_BT1CFG_BST0EN_Pos)                     /*!< 0x00000001 */
#define ALM_BT1CFG_BST0EN       ALM_BT1CFG_BST0EN_Msk                               /*!<base_timer0_en */

#define ALM_BT1CFG_BST0FDETEN_Pos   (1U)
#define ALM_BT1CFG_BST0FDETEN_Msk   (0x1U << ALM_BT1CFG_BST0FDETEN_Pos)             /*!< 0x00000002 */
#define ALM_BT1CFG_BST0FDETEN       ALM_BT1CFG_BST0FDETEN_Msk                       /*!<base_timer0_fdet_en */

#define ALM_BT1CFG_BST0MD_Pos   (2U)
#define ALM_BT1CFG_BST0MD_Msk   (0x1U << ALM_BT1CFG_BST0MD_Pos)                     /*!< 0x00000004 */
#define ALM_BT1CFG_BST0MD       ALM_BT1CFG_BST0MD_Msk                               /*!<base_timer0_mode */

/*******************  Bit definition for ALM_BT1CLKAC register  ********************/
#define ALM_BT1CLKAC_TH_Pos (0U)
#define ALM_BT1CLKAC_TH_Msk (0xFFFFFFU << ALM_BT1CLKAC_TH_Pos)                      /*!< 0x00FFFFFF */
#define ALM_BT1CLKAC_TH     ALM_BT1CLKAC_TH_Msk                                     /*!<base_timer0_clka_th */

/*******************  Bit definition for ALM_BT1CLKBCL register  ********************/
#define ALM_BT1CLKBCL_TH_Pos    (0U)
#define ALM_BT1CLKBCL_TH_Msk    (0xFFFFFFU << ALM_BT1CLKBCL_TH_Pos)                 /*!< 0x00FFFFFF */
#define ALM_BT1CLKBCL_TH        ALM_BT1CLKBCL_TH_Msk                                /*!<base_timer0_clkb_th_l */

/*******************  Bit definition for ALM_BT1CLKBCH register  ********************/
#define ALM_BT1CLKBCH_TH_Pos    (0U)
#define ALM_BT1CLKBCH_TH_Msk    (0xFFFFFFU << ALM_BT1CLKBCH_TH_Pos)                 /*!< 0x00FFFFFF */
#define ALM_BT1CLKBCH_TH        ALM_BT1CLKBCH_TH_Msk                                /*!<base_timer0_clkb_th_h */

/*******************  Bit definition for ALM_BT1CTRL register  ********************/
#define ALM_BT1CTRL_CLR_Pos (0U)
#define ALM_BT1CTRL_CLR_Msk (0x1U << ALM_BT1CTRL_CLR_Pos)                           /*!< 0x00000001 */
#define ALM_BT1CTRL_CLR     ALM_BT1CTRL_CLR_Msk                                     /*!<base_timer0_clr */

/*******************  Bit definition for ALM_BT1SR register  ********************/
#define ALM_BT1SR_BUSY_Pos  (0U)
#define ALM_BT1SR_BUSY_Msk  (0x1U << ALM_BT1SR_BUSY_Pos)                            /*!< 0x00000001 */
#define ALM_BT1SR_BUSY      ALM_BT1SR_BUSY_Msk                                      /*!<base_timer0_busy */

/*******************  Bit definition for ALM_BT1RPT register  ********************/
#define ALM_BT1RPT_VALID_Pos    (0U)
#define ALM_BT1RPT_VALID_Msk    (0x1U << ALM_BT1RPT_VALID_Pos)                      /*!< 0x00000001 */
#define ALM_BT1RPT_VALID        ALM_BT1RPT_VALID_Msk                                /*!<base_timer0_valid */

#define ALM_BT1RPT_FWARN_Pos    (1U)
#define ALM_BT1RPT_FWARN_Msk    (0x1U << ALM_BT1RPT_FWARN_Pos)                      /*!< 0x00000002 */
#define ALM_BT1RPT_FWARN        ALM_BT1RPT_FWARN_Msk                                /*!<base_timer0_fwarn */

#define ALM_BT1RPT_LWARN_Pos    (2U)
#define ALM_BT1RPT_LWARN_Msk    (0x1U << ALM_BT1RPT_LWARN_Pos)                      /*!< 0x00000004 */
#define ALM_BT1RPT_LWARN        ALM_BT1RPT_LWARN_Msk                                /*!<base_timer0_lwarn */

#define ALM_BT1RPT_HWARN_Pos    (3U)
#define ALM_BT1RPT_HWARN_Msk    (0x1U << ALM_BT1RPT_HWARN_Pos)                      /*!< 0x00000008 */
#define ALM_BT1RPT_HWARN        ALM_BT1RPT_HWARN_Msk                                /*!<base_timer0_hwarn */

#define ALM_BT1RPT_RESULT_Pos   (8U)
#define ALM_BT1RPT_RESULT_Msk   (0xFFFFFFU << ALM_BT1RPT_RESULT_Pos)                /*!< 0xFFFFFF00 */
#define ALM_BT1RPT_RESULT       ALM_BT1RPT_RESULT_Msk                               /*!<base_timer0_result */

/*******************  Bit definition for ALM_ALARMA register  ********************/
#define ALM_ALARMA_SELFA_Pos    (0U)
#define ALM_ALARMA_SELFA_Msk    (0x1U << ALM_ALARMA_SELFA_Pos)                      /*!< 0x00000001 */
#define ALM_ALARMA_SELFA        ALM_ALARMA_SELFA_Msk                                /*!<alarm_system_self_a */

/*******************  Bit definition for ALM_ALARMB register  ********************/
#define ALM_ALARMB_SELFB_Pos    (0U)
#define ALM_ALARMB_SELFB_Msk    (0x1U << ALM_ALARMB_SELFB_Pos)                      /*!< 0x00000001 */
#define ALM_ALARMB_SELFB        ALM_ALARMB_SELFB_Msk                                /*!<alarm_system_self_b */

/*******************  Bit definition for ALM_GROUPA0 register  ********************/
#define ALM_GROUPA0_A00_Pos (0U)
#define ALM_GROUPA0_A00_Msk (0xFFU << ALM_GROUPA0_A00_Pos)                          /*!< 0x000000FF */
#define ALM_GROUPA0_A00     ALM_GROUPA0_A00_Msk                                     /*!<cfg_group_a_00[7:0] */

#define ALM_GROUPA0_A01_Pos (8U)
#define ALM_GROUPA0_A01_Msk (0xFFU << ALM_GROUPA0_A01_Pos)                          /*!< 0x0000FF00 */
#define ALM_GROUPA0_A01     ALM_GROUPA0_A01_Msk                                     /*!<cfg_group_a_01[7:0] */

#define ALM_GROUPA0_A02_Pos (16U)
#define ALM_GROUPA0_A02_Msk (0xFFU << ALM_GROUPA0_A02_Pos)                          /*!< 0x00FF0000 */
#define ALM_GROUPA0_A02     ALM_GROUPA0_A02_Msk                                     /*!<cfg_group_a_02[7:0] */

#define ALM_GROUPA0_A03_Pos (24U)
#define ALM_GROUPA0_A03_Msk (0xFFU << ALM_GROUPA0_A03_Pos)                          /*!< 0xFF000000 */
#define ALM_GROUPA0_A03     ALM_GROUPA0_A03_Msk                                     /*!<cfg_group_a_03[7:0] */

/*******************  Bit definition for ALM_GROUPA1 register  ********************/
#define ALM_GROUPA1_A04_Pos (0U)
#define ALM_GROUPA1_A04_Msk (0xFFU << ALM_GROUPA1_A04_Pos)                          /*!< 0x000000FF */
#define ALM_GROUPA1_A04     ALM_GROUPA1_A04_Msk                                     /*!<cfg_group_a_04[7:0] */

#define ALM_GROUPA1_A05_Pos (8U)
#define ALM_GROUPA1_A05_Msk (0xFFU << ALM_GROUPA1_A05_Pos)                          /*!< 0x0000FF00 */
#define ALM_GROUPA1_A05     ALM_GROUPA1_A05_Msk                                     /*!<cfg_group_a_05[7:0] */

#define ALM_GROUPA1_A06_Pos (16U)
#define ALM_GROUPA1_A06_Msk (0xFFU << ALM_GROUPA1_A06_Pos)                          /*!< 0x00FF0000 */
#define ALM_GROUPA1_A06     ALM_GROUPA1_A06_Msk                                     /*!<cfg_group_a_06[7:0] */

#define ALM_GROUPA1_A07_Pos (24U)
#define ALM_GROUPA1_A07_Msk (0xFFU << ALM_GROUPA1_A07_Pos)                          /*!< 0xFF000000 */
#define ALM_GROUPA1_A07     ALM_GROUPA1_A07_Msk                                     /*!<cfg_group_a_07[7:0] */

/*******************  Bit definition for ALM_GROUPA2 register  ********************/
#define ALM_GROUPA2_A08_Pos (0U)
#define ALM_GROUPA2_A08_Msk (0xFFU << ALM_GROUPA2_A08_Pos)                          /*!< 0x000000FF */
#define ALM_GROUPA2_A08     ALM_GROUPA2_A08_Msk                                     /*!<cfg_group_a_08[7:0] */

/*******************  Bit definition for ALM_GROUPA3 register  ********************/
#define ALM_GROUPA3_A09_Pos (0U)
#define ALM_GROUPA3_A09_Msk (0xFFU << ALM_GROUPA3_A09_Pos)                          /*!< 0x000000FF */
#define ALM_GROUPA3_A09     ALM_GROUPA3_A09_Msk                                     /*!<cfg_group_a_12[7:0] */

/*******************  Bit definition for ALM_GROUPB0 register  ********************/
#define ALM_GROUPB0_B00_Pos (0U)
#define ALM_GROUPB0_B00_Msk (0xFFU << ALM_GROUPB0_B00_Pos)                          /*!< 0x000000FF */
#define ALM_GROUPB0_B00     ALM_GROUPB0_B00_Msk                                     /*!<cfg_group_b_00[7:0] */

#define ALM_GROUPB0_B01_Pos (8U)
#define ALM_GROUPB0_B01_Msk (0xFFU << ALM_GROUPB0_B01_Pos)                          /*!< 0x0000FF00 */
#define ALM_GROUPB0_B01     ALM_GROUPB0_B01_Msk                                     /*!<cfg_group_b_01[7:0] */

#define ALM_GROUPB0_B02_Pos (16U)
#define ALM_GROUPB0_B02_Msk (0xFFU << ALM_GROUPB0_B02_Pos)                          /*!< 0x00FF0000 */
#define ALM_GROUPB0_B02     ALM_GROUPB0_B02_Msk                                     /*!<cfg_group_b_02[7:0] */

#define ALM_GROUPB0_B03_Pos (24U)
#define ALM_GROUPB0_B03_Msk (0xFFU << ALM_GROUPB0_B03_Pos)                          /*!< 0xFF000000 */
#define ALM_GROUPB0_B03     ALM_GROUPB0_B03_Msk                                     /*!<cfg_group_b_03[7:0] */

/*******************  Bit definition for ALM_GROUPC0 register  ********************/
#define ALM_GROUPC0_C00_Pos (0U)
#define ALM_GROUPC0_C00_Msk (0xFFU << ALM_GROUPC0_C00_Pos)                          /*!< 0x000000FF */
#define ALM_GROUPC0_C00     ALM_GROUPC0_C00_Msk                                     /*!<cfg_group_c_00[7:0] */

#define ALM_GROUPC0_C01_Pos (8U)
#define ALM_GROUPC0_C01_Msk (0xFFU << ALM_GROUPC0_C01_Pos)                          /*!< 0x0000FF00 */
#define ALM_GROUPC0_C01     ALM_GROUPC0_C01_Msk                                     /*!<cfg_group_c_01[7:0] */

#define ALM_GROUPC0_C02_Pos (16U)
#define ALM_GROUPC0_C02_Msk (0xFFU << ALM_GROUPC0_C02_Pos)                          /*!< 0x00FF0000 */
#define ALM_GROUPC0_C02     ALM_GROUPC0_C02_Msk                                     /*!<cfg_group_c_02[7:0] */

#define ALM_GROUPC0_C03_Pos (24U)
#define ALM_GROUPC0_C03_Msk (0xFFU << ALM_GROUPC0_C03_Pos)                          /*!< 0xFF000000 */
#define ALM_GROUPC0_C03     ALM_GROUPC0_C03_Msk                                     /*!<cfg_group_c_03[7:0] */

/*******************  Bit definition for ALM_GROUPC1 register  ********************/
#define ALM_GROUPC1_C04_Pos (0U)
#define ALM_GROUPC1_C04_Msk (0xFFU << ALM_GROUPC1_C04_Pos)                          /*!< 0x000000FF */
#define ALM_GROUPC1_C04     ALM_GROUPC1_C04_Msk                                     /*!<cfg_group_c_04[7:0] */

#define ALM_GROUPC1_C05_Pos (8U)
#define ALM_GROUPC1_C05_Msk (0xFFU << ALM_GROUPC1_C05_Pos)                          /*!< 0x0000FF00 */
#define ALM_GROUPC1_C05     ALM_GROUPC1_C05_Msk                                     /*!<cfg_group_c_05[7:0] */

#define ALM_GROUPC1_C06_Pos (16U)
#define ALM_GROUPC1_C06_Msk (0xFFU << ALM_GROUPC1_C06_Pos)                          /*!< 0x00FF0000 */
#define ALM_GROUPC1_C06     ALM_GROUPC1_C06_Msk                                     /*!<cfg_group_c_06[7:0] */

#define ALM_GROUPC1_C07_Pos (24U)
#define ALM_GROUPC1_C07_Msk (0xFFU << ALM_GROUPC1_C07_Pos)                          /*!< 0xFF000000 */
#define ALM_GROUPC1_C07     ALM_GROUPC1_C07_Msk                                     /*!<cfg_group_c_07[7:0] */

/*******************  Bit definition for ALM_GROUPC2 register  ********************/
#define ALM_GROUPC2_C08_Pos (0U)
#define ALM_GROUPC2_C08_Msk (0xFFU << ALM_GROUPC2_C08_Pos)                          /*!< 0x000000FF */
#define ALM_GROUPC2_C08     ALM_GROUPC2_C08_Msk                                     /*!<cfg_group_c_08[7:0] */

#define ALM_GROUPC2_C09_Pos (8U)
#define ALM_GROUPC2_C09_Msk (0xFFU << ALM_GROUPC2_C09_Pos)                          /*!< 0x0000FF00 */
#define ALM_GROUPC2_C09     ALM_GROUPC2_C09_Msk                                     /*!<cfg_group_c_09[7:0] */

#define ALM_GROUPC2_C10_Pos (16U)
#define ALM_GROUPC2_C10_Msk (0xFFU << ALM_GROUPC2_C10_Pos)                          /*!< 0x00FF0000 */
#define ALM_GROUPC2_C10     ALM_GROUPC2_C10_Msk                                     /*!<cfg_group_c_10[7:0] */

#define ALM_GROUPC2_C11_Pos (24U)
#define ALM_GROUPC2_C11_Msk (0xFFU << ALM_GROUPC2_C11_Pos)                          /*!< 0xFF000000 */
#define ALM_GROUPC2_C11     ALM_GROUPC2_C11_Msk                                     /*!<cfg_group_c_11[7:0] */

/*******************  Bit definition for ALM_GROUPC3 register  ********************/
#define ALM_GROUPC3_C12_Pos (16U)
#define ALM_GROUPC3_C12_Msk (0xFFU << ALM_GROUPC3_C12_Pos)                          /*!< 0x00FF0000 */
#define ALM_GROUPC3_C12     ALM_GROUPC3_C12_Msk                                     /*!<cfg_group_c_14[7:0] */

#define ALM_GROUPC3_C13_Pos (24U)
#define ALM_GROUPC3_C13_Msk (0xFFU << ALM_GROUPC3_C13_Pos)                          /*!< 0xFF000000 */
#define ALM_GROUPC3_C13     ALM_GROUPC3_C13_Msk                                     /*!<cfg_group_c_15[7:0] */

/*******************  Bit definition for ALM_GROUPC4 register  ********************/
#define ALM_GROUPC4_C14_Pos (0U)
#define ALM_GROUPC4_C14_Msk (0xFFU << ALM_GROUPC4_C14_Pos)                          /*!< 0x000000FF */
#define ALM_GROUPC4_C14     ALM_GROUPC4_C14_Msk                                     /*!<cfg_group_c_16[7:0] */

#define ALM_GROUPC4_C15_Pos (8U)
#define ALM_GROUPC4_C15_Msk (0xFFU << ALM_GROUPC4_C15_Pos)                          /*!< 0x0000FF00 */
#define ALM_GROUPC4_C15     ALM_GROUPC4_C15_Msk                                     /*!<cfg_group_c_17[7:0] */

#define ALM_GROUPC4_C16_Pos (16U)
#define ALM_GROUPC4_C16_Msk (0xFFU << ALM_GROUPC4_C16_Pos)                          /*!< 0x00FF0000 */
#define ALM_GROUPC4_C16     ALM_GROUPC4_C16_Msk                                     /*!<cfg_group_c_18[7:0] */

#define ALM_GROUPC4_C17_Pos (24U)
#define ALM_GROUPC4_C17_Msk (0xFFU << ALM_GROUPC4_C17_Pos)                          /*!< 0xFF000000 */
#define ALM_GROUPC4_C17     ALM_GROUPC4_C17_Msk                                     /*!<cfg_group_c_19[7:0] */

/*******************  Bit definition for ALM_GROUPC5 register  ********************/
#define ALM_GROUPC5_C18_Pos (0U)
#define ALM_GROUPC5_C18_Msk (0xFFU << ALM_GROUPC5_C18_Pos)                          /*!< 0x000000FF */
#define ALM_GROUPC5_C18     ALM_GROUPC5_C18_Msk                                     /*!<cfg_group_c_20[7:0] */

/*******************  Bit definition for ALM_GROUPD0 register  ********************/
#define ALM_GROUPD0_D00_Pos (0U)
#define ALM_GROUPD0_D00_Msk (0xFFU << ALM_GROUPD0_D00_Pos)                          /*!< 0x000000FF */
#define ALM_GROUPD0_D00     ALM_GROUPD0_D00_Msk                                     /*!<cfg_group_d_00[7:0] */

#define ALM_GROUPD0_D01_Pos (8U)
#define ALM_GROUPD0_D01_Msk (0xFFU << ALM_GROUPD0_D01_Pos)                          /*!< 0x0000FF00 */
#define ALM_GROUPD0_D01     ALM_GROUPD0_D01_Msk                                     /*!<cfg_group_d_01[7:0] */

#define ALM_GROUPD0_D02_Pos (16U)
#define ALM_GROUPD0_D02_Msk (0xFFU << ALM_GROUPD0_D02_Pos)                          /*!< 0x00FF0000 */
#define ALM_GROUPD0_D02     ALM_GROUPD0_D02_Msk                                     /*!<cfg_group_d_02[7:0] */

#define ALM_GROUPD0_D03_Pos (24U)
#define ALM_GROUPD0_D03_Msk (0xFFU << ALM_GROUPD0_D03_Pos)                          /*!< 0xFF000000 */
#define ALM_GROUPD0_D03     ALM_GROUPD0_D03_Msk                                     /*!<cfg_group_d_03[7:0] */

/*******************  Bit definition for ALM_GROUPD1 register  ********************/
#define ALM_GROUPD1_D04_Pos (0U)
#define ALM_GROUPD1_D04_Msk (0xFFU << ALM_GROUPD1_D04_Pos)                          /*!< 0x000000FF */
#define ALM_GROUPD1_D04     ALM_GROUPD1_D04_Msk                                     /*!<cfg_group_d_04[7:0] */

#define ALM_GROUPD1_D05_Pos (8U)
#define ALM_GROUPD1_D05_Msk (0xFFU << ALM_GROUPD1_D05_Pos)                          /*!< 0x0000FF00 */
#define ALM_GROUPD1_D05     ALM_GROUPD1_D05_Msk                                     /*!<cfg_group_d_05[7:0] */

#define ALM_GROUPD1_D06_Pos (16U)
#define ALM_GROUPD1_D06_Msk (0xFFU << ALM_GROUPD1_D06_Pos)                          /*!< 0x00FF0000 */
#define ALM_GROUPD1_D06     ALM_GROUPD1_D06_Msk                                     /*!<cfg_group_d_06[7:0] */

#define ALM_GROUPD1_D07_Pos (24U)
#define ALM_GROUPD1_D07_Msk (0xFFU << ALM_GROUPD1_D07_Pos)                          /*!< 0xFF000000 */
#define ALM_GROUPD1_D07     ALM_GROUPD1_D07_Msk                                     /*!<cfg_group_d_07[7:0] */

/*******************  Bit definition for ALM_ALMRCDCL register  ********************/
#define ALM_ALMRCDCL_RCDCLR_Pos (0U)
#define ALM_ALMRCDCL_RCDCLR_Msk (0x1U << ALM_ALMRCDCL_RCDCLR_Pos)                   /*!< 0x00000001 */
#define ALM_ALMRCDCL_RCDCLR     ALM_ALMRCDCL_RCDCLR_Msk                             /*!<alarm_record_clr */

#define ALM_ALMRCDCL_RPTCLR_Pos (1U)
#define ALM_ALMRCDCL_RPTCLR_Msk (0x1U << ALM_ALMRCDCL_RPTCLR_Pos)                   /*!< 0x00000002 */
#define ALM_ALMRCDCL_RPTCLR     ALM_ALMRCDCL_RPTCLR_Msk                             /*!<alarm_rpt_clr */

/*******************  Bit definition for ALM_ALMRCDA register  ********************/
#define ALM_ALMRCDA_ARCD_Pos    (0U)
#define ALM_ALMRCDA_ARCD_Msk    (0x7FFFU << ALM_ALMRCDA_ARCD_Pos)                   /*!< 0x00007FFF */
#define ALM_ALMRCDA_ARCD        ALM_ALMRCDA_ARCD_Msk                                /*!<alarm_a_record[14:0] */

/*******************  Bit definition for ALM_ALMRCDB register  ********************/
#define ALM_ALMRCDB_BRCD_Pos    (0U)
#define ALM_ALMRCDB_BRCD_Msk    (0xFU << ALM_ALMRCDB_BRCD_Pos)                      /*!< 0x0000000F */
#define ALM_ALMRCDB_BRCD        ALM_ALMRCDB_BRCD_Msk                                /*!<alarm_b_record[3:0] */

/*******************  Bit definition for ALM_ALMRCDC register  ********************/
#define ALM_ALMRCDC_CRCD_Pos    (0U)
#define ALM_ALMRCDC_CRCD_Msk    (0xFFFFFFU << ALM_ALMRCDC_CRCD_Pos)                 /*!< 0x00FFFFFF */
#define ALM_ALMRCDC_CRCD        ALM_ALMRCDC_CRCD_Msk                                /*!<alarm_c_record[23:0] */
/*******************  Bit definition for ALM_ALMRCDD register  ********************/
#define ALM_ALMRCDD_DRCD_Pos    (0U)
#define ALM_ALMRCDD_DRCD_Msk    (0xFFU << ALM_ALMRCDD_DRCD_Pos)                     /*!< 0x000000FF */
#define ALM_ALMRCDD_DRCD        ALM_ALMRCDD_DRCD_Msk                                /*!<alarm_d_record[7:0] */

/*******************  Bit definition for ALM_CFG1_RSTP register  ****************/
#define ALM_RSTBP_RSTBP_Pos (0U)
#define ALM_RSTBP_RSTBP_Msk (0x1U << ALM_RSTBP_RSTBP_Pos)                           /*!< 0x00000001 */
#define ALM_RSTBP_RSTBP     ALM_RSTBP_RSTBP_Msk                                     /*!<csr_reset_b_pad */
/*******************  Bit definition for ALM_RST_RCDA register  ********************/
#define ALM_RST_RCDA_Pos    (0U)
#define ALM_RST_RCDA_Msk    (0xFFFFU << ALM_RST_RCDA_Pos)                           /*!< 0x0000FFFF */
#define ALM_RST_RCDA        ALM_RST_RCDA_Msk                                        /*!<rst_record_a[15:0] */

/*******************  Bit definition for ALM_RST_RCDB register  ********************/
#define ALM_RST_RCDB_Pos    (0U)
#define ALM_RST_RCDB_Msk    (0xFU << ALM_RST_RCDB_Pos)                              /*!< 0x0000000F */
#define ALM_RST_RCDB        ALM_RST_RCDB_Msk                                        /*!<rst_record_b[3:0] */

/*******************  Bit definition for ALM_RST_RCDC register  ********************/
#define ALM_RST_RCDC_Pos    (0U)
#define ALM_RST_RCDC_Msk    (0xFFFFFFU << ALM_RST_RCDC_Pos)                         /*!< 0x00FFFFFF */
#define ALM_RST_RCDC        ALM_RST_RCDC_Msk                                        /*!<rst_record_c[23:0] */

/*******************  Bit definition for ALM_RST_RCDD register  ********************/
#define ALM_RST_RCDD_Pos    (0U)
#define ALM_RST_RCDD_Msk    (0xFFU << ALM_RST_RCDD_Pos)                             /*!< 0x000000FF */
#define ALM_RST_RCDD        ALM_RST_RCDD_Msk                                        /*!<rst_record_d[7:0] */

/*******************  Bit definition for ALM_RST_RCDE register  ********************/
#define ALM_RST_RCDE_Pos    (0U)
#define ALM_RST_RCDE_Msk    (0x7FU << ALM_RST_RCDE_Pos)                             /*!< 0x0000007F */
#define ALM_RST_RCDE        ALM_RST_RCDE_Msk                                        /*!<rst_record_e[6:0] */

/*******************  Bit definition for ALM_RST_RCDCLR register  ********************/
#define ALM_RST_RCDCL_Pos   (0U)
#define ALM_RST_RCDCL_Msk   (0x1U << ALM_RST_RCDCL_Pos)                             /*!< 0x00000001 */
#define ALM_RST_RCDCL       ALM_RST_RCDCL_Msk                                       /*!<rst_record_clr */

/*******************  Bit definition for ALM_RST_GROUPE register  *************/
#define ALM_RSTGROUPE_BOR_Pos       (0U)
#define ALM_RSTGROUPE_BOR_Msk       (0x1U << ALM_RSTGROUPE_BOR_Pos)                 /*!< 0x00000001 */
#define ALM_RSTGROUPE_BOR_VDETLVT   ALM_RSTGROUPE_BOR_Msk                           /*!<rst_en_vdet_lvd */

#define ALM_RSTGROUPE_RSTBPAD_Pos   (1U)
#define ALM_RSTGROUPE_RSTBPAD_Msk   (0x1U << ALM_RSTGROUPE_RSTBPAD_Pos)             /*!< 0x00000002 */
#define ALM_RSTGROUPE_RSTBPAD       ALM_RSTGROUPE_RSTBPAD_Msk                       /*!<rst_en_reset b_pad */

#define ALM_RSTGROUPE_12LVW_Pos (2U)
#define ALM_RSTGROUPE_12LVW_Msk (0x1U << ALM_RSTGROUPE_12LVW_Pos)                   /*!< 0x00000004 */
#define ALM_RSTGROUPE_12LVW     ALM_RSTGROUPE_12LVW_Msk                             /*!<rst_en_ldo12_lvw */

#define ALM_RSTGROUPE_16LVW_Pos (3U)
#define ALM_RSTGROUPE_16LVW_Msk (0x1U << ALM_RSTGROUPE_16LVW_Pos)                   /*!< 0x00000008 */
#define ALM_RSTGROUPE_16LVW     ALM_RSTGROUPE_16LVW_Msk                             /*!<rst_en_ldo16_lvw */

//</h>
//<h>CMU
/******************************************************************************/
/*                                                                            */
/*                                    CMU                                     */
/*                                                                            */
/******************************************************************************/
/*******************  Bit definition for CMU_CLKLP register  ********************/
#define CMU_CLKLP_PDPOREN_Pos   (0U)
#define CMU_CLKLP_PDPOREN_Msk   (0x1U << CMU_CLKLP_PDPOREN_Pos)                             /*!< 0x00000001 */
#define CMU_CLKLP_PDPOREN       CMU_CLKLP_PDPOREN_Msk                                       /*!<csr_pd_por_en */

#define CMU_CLKLP_PDLPOEN_Pos   (1U)
#define CMU_CLKLP_PDLPOEN_Msk   (0x1U << CMU_CLKLP_PDLPOEN_Pos)                             /*!< 0x00000002 */
#define CMU_CLKLP_PDLPOEN       CMU_CLKLP_PDLPOEN_Msk                                       /*!<csr_pd_lpo_en */

#define CMU_CLKLP_STBYIRC4MEN_Pos   (2U)
#define CMU_CLKLP_STBYIRC4MEN_Msk   (0x1U << CMU_CLKLP_STBYIRC4MEN_Pos)                     /*!< 0x00000004 */
#define CMU_CLKLP_STBYIRC4MEN       CMU_CLKLP_STBYIRC4MEN_Msk                               /*!<csr_stby_irc4m_en */

#define CMU_CLKLP_STOPIRC4MEN_Pos   (3U)
#define CMU_CLKLP_STOPIRC4MEN_Msk   (0x1U << CMU_CLKLP_STOPIRC4MEN_Pos)                     /*!< 0x00000008 */
#define CMU_CLKLP_STOPIRC4MEN       CMU_CLKLP_STOPIRC4MEN_Msk                               /*!<csr_stop_irc4m_en */

#define CMU_CLKLP_IDLEIRC4MEN_Pos   (4U)
#define CMU_CLKLP_IDLEIRC4MEN_Msk   (0x1U << CMU_CLKLP_IDLEIRC4MEN_Pos)                     /*!< 0x00000010 */
#define CMU_CLKLP_IDLEIRC4MEN       CMU_CLKLP_IDLEIRC4MEN_Msk                               /*!<csr_idle_irc4m_en */

#define CMU_CLKLP_USBCLKINS_Pos (5U)
#define CMU_CLKLP_USBCLKINS_Msk (0x1U << CMU_CLKLP_USBCLKINS_Pos)                           /*!< 0x00000020 */
#define CMU_CLKLP_USBCLKINS     CMU_CLKLP_USBCLKINS_Msk                                     /*!<csr_usb_clkin_sel */
/*******************  Bit definition for CMU_CLK_AT_STOP register  ********************/
#define CMU_CLKSTOP_EN32K_Pos   (0U)
#define CMU_CLKSTOP_EN32K_Msk   (0x1U << CMU_CLKSTOP_EN32K_Pos)                             /*!< 0x00000001 */
#define CMU_CLKSTOP_EN32K       CMU_CLKSTOP_EN32K_Msk                                       /*!<csr_stop_en_irc32k*/

#define CMU_CLKSTOP_ENDFS_Pos   (1U)
#define CMU_CLKSTOP_ENDFS_Msk   (0x1U << CMU_CLKSTOP_ENDFS_Pos)                             /*!< 0x00000002 */
#define CMU_CLKSTOP_ENDFS       CMU_CLKSTOP_ENDFS_Msk                                       /*!<csr_stop_en_dfs*/

#define CMU_CLKSTOP_ENFLL_Pos   (2U)
#define CMU_CLKSTOP_ENFLL_Msk   (0x1U << CMU_CLKSTOP_ENFLL_Pos)                             /*!< 0x00000004 */
#define CMU_CLKSTOP_ENFLL       CMU_CLKSTOP_ENFLL_Msk                                       /*!<csr_stop_en_fll*/

#define CMU_CLKSTOP_ENPLL_Pos   (3U)
#define CMU_CLKSTOP_ENPLL_Msk   (0x1U << CMU_CLKSTOP_ENPLL_Pos)                             /*!< 0x00000008 */
#define CMU_CLKSTOP_ENPLL       CMU_CLKSTOP_ENPLL_Msk                                       /*!<csr_stop_en_pll*/

#define CMU_CLKSTOP_ENOSC_Pos   (4U)
#define CMU_CLKSTOP_ENOSC_Msk   (0x1U << CMU_CLKSTOP_ENOSC_Pos)                             /*!< 0x00000010 */
#define CMU_CLKSTOP_ENOSC       CMU_CLKSTOP_ENOSC_Msk                                       /*!<csr_stop_en_osc*/

#define CMU_CLKSTOP_EN16M_Pos   (5U)
#define CMU_CLKSTOP_EN16M_Msk   (0x1U << CMU_CLKSTOP_EN16M_Pos)                             /*!< 0x00000020 */
#define CMU_CLKSTOP_EN16M       CMU_CLKSTOP_EN16M_Msk                                       /*!<csr_stop_en_irc16m*/

/*******************  Bit definition for CMU_CLK_AT_IDLE register  ********************/
#define CMU_CLKIDLE_EN32K_Pos   (0U)
#define CMU_CLKIDLE_EN32K_Msk   (0x1U << CMU_CLKIDLE_EN32K_Pos)                             /*!< 0x00000001 */
#define CMU_CLKIDLE_EN32K       CMU_CLKIDLE_EN32K_Msk                                       /*!<csr_clk_en_irc32k*/

#define CMU_CLKIDLE_ENDFS_Pos   (1U)
#define CMU_CLKIDLE_ENDFS_Msk   (0x1U << CMU_CLKIDLE_ENDFS_Pos)                             /*!< 0x00000002 */
#define CMU_CLKIDLE_ENDFS       CMU_CLKIDLE_ENDFS_Msk                                       /*!<csr_clk_en_dfs*/

#define CMU_CLKIDLE_ENFLL_Pos   (2U)
#define CMU_CLKIDLE_ENFLL_Msk   (0x1U << CMU_CLKIDLE_ENFLL_Pos)                             /*!< 0x00000004 */
#define CMU_CLKIDLE_ENFLL       CMU_CLKIDLE_ENFLL_Msk                                       /*!<csr_clk_en_fll*/

#define CMU_CLKIDLE_ENPLL_Pos   (3U)
#define CMU_CLKIDLE_ENPLL_Msk   (0x1U << CMU_CLKIDLE_ENPLL_Pos)                             /*!< 0x00000008 */
#define CMU_CLKIDLE_ENPLL       CMU_CLKIDLE_ENPLL_Msk                                       /*!<csr_clk_en_pll*/

#define CMU_CLKIDLE_ENOSC_Pos   (4U)
#define CMU_CLKIDLE_ENOSC_Msk   (0x1U << CMU_CLKIDLE_ENOSC_Pos)                             /*!< 0x00000010 */
#define CMU_CLKIDLE_ENOSC       CMU_CLKIDLE_ENOSC_Msk                                       /*!<csr_clk_en_osc*/

#define CMU_CLKIDLE_EN16M_Pos   (5U)
#define CMU_CLKIDLE_EN16M_Msk   (0x1U << CMU_CLKIDLE_EN16M_Pos)                             /*!< 0x00000020 */
#define CMU_CLKIDLE_EN16M       CMU_CLKIDLE_EN16M_Msk                                       /*!<csr_clk_en_irc16m*/

/*******************  Bit definition for CMU_CFG_CLKS0 register  ********************/
#define CMU_CFGCLKS0_16MVREF_Pos    (0U)
#define CMU_CFGCLKS0_16MVREF_Msk    (0x3U << CMU_CFGCLKS0_16MVREF_Pos)
#define CMU_CFGCLKS0_16MVREF        CMU_CFGCLKS0_16MVREF_Msk

#define CMU_CFGCLKS0_16MMVEN_Pos    (2U)
#define CMU_CFGCLKS0_16MMVEN_Msk    (0x3U << CMU_CFGCLKS0_16MMVEN_Pos)
#define CMU_CFGCLKS0_16MMVEN        CMU_CFGCLKS0_16MMVEN_Msk

#define CMU_CFGCLKS0_XTALISEL_Pos   (8U)
#define CMU_CFGCLKS0_XTALISEL_Msk   (0x7U << CMU_CFGCLKS0_XTALISEL_Pos)
#define CMU_CFGCLKS0_XTALISEL       CMU_CFGCLKS0_XTALISEL_Msk

#define CMU_CFGCLKS0_XTALBYPS_Pos   (11U)
#define CMU_CFGCLKS0_XTALBYPS_Msk   (0x1U << CMU_CFGCLKS0_XTALBYPS_Pos)
#define CMU_CFGCLKS0_XTALBYPS       CMU_CFGCLKS0_XTALBYPS_Msk

/*******************  Bit definition for CMU_CFG_CLKS1 register  ********************/
#define CMU_CFGCLKS1_PLLEN_Pos  (0U)
#define CMU_CFGCLKS1_PLLEN_Msk  (0x1U << CMU_CFGCLKS1_PLLEN_Pos)
#define CMU_CFGCLKS1_PLLEN      CMU_CFGCLKS1_PLLEN_Msk

#define CMU_CFGCLKS1_PLLENPWR_Pos   (1U)
#define CMU_CFGCLKS1_PLLENPWR_Msk   (0x1U << CMU_CFGCLKS1_PLLENPWR_Pos)
#define CMU_CFGCLKS1_PLLENPWR       CMU_CFGCLKS1_PLLENPWR_Msk

#define CMU_CFGCLKS1_PLLPD_Pos  (2U)
#define CMU_CFGCLKS1_PLLPD_Msk  (0x1U << CMU_CFGCLKS1_PLLPD_Pos)
#define CMU_CFGCLKS1_PLLPD      CMU_CFGCLKS1_PLLPD_Msk

#define CMU_CFGCLKS1_PLLCLKINS_Pos  (3U)
#define CMU_CFGCLKS1_PLLCLKINS_Msk  (0x1U << CMU_CFGCLKS1_PLLCLKINS_Pos)
#define CMU_CFGCLKS1_PLLCLKINS      CMU_CFGCLKS1_PLLCLKINS_Msk

#define CMU_CFGCLKS1_PLLRSTDIV_Pos  (4U)
#define CMU_CFGCLKS1_PLLRSTDIV_Msk  (0x1U << CMU_CFGCLKS1_PLLRSTDIV_Pos)
#define CMU_CFGCLKS1_PLLRSTDIV      CMU_CFGCLKS1_PLLRSTDIV_Msk

#define CMU_CFGCLKS1_PLLCPICTRL_Pos (8U)
#define CMU_CFGCLKS1_PLLCPICTRL_Msk (0xFU << CMU_CFGCLKS1_PLLCPICTRL_Pos)
#define CMU_CFGCLKS1_PLLCPICTRL     CMU_CFGCLKS1_PLLCPICTRL_Msk

#define CMU_CFGCLKS1_PLLPRECFG_Pos  (12U)
#define CMU_CFGCLKS1_PLLPRECFG_Msk  (0x7U << CMU_CFGCLKS1_PLLPRECFG_Pos)
#define CMU_CFGCLKS1_PLLPRECFG      CMU_CFGCLKS1_PLLPRECFG_Msk

#define CMU_CFGCLKS1_PLLCFG_Pos (16U)
#define CMU_CFGCLKS1_PLLCFG_Msk (0x7U << CMU_CFGCLKS1_PLLCFG_Pos)
#define CMU_CFGCLKS1_PLLCFG     CMU_CFGCLKS1_PLLCFG_Msk

#define CMU_CFGCLKS1_PLLFBCFG_Pos   (20U)
#define CMU_CFGCLKS1_PLLFBCFG_Msk   (0x1FU << CMU_CFGCLKS1_PLLFBCFG_Pos)
#define CMU_CFGCLKS1_PLLFBCFG       CMU_CFGCLKS1_PLLFBCFG_Msk

#define CMU_CFGCLKS1_PLLTRIM_Pos    (26U)
#define CMU_CFGCLKS1_PLLTRIM_Msk    (0x3U << CMU_CFGCLKS1_PLLTRIM_Pos)
#define CMU_CFGCLKS1_PLLTRIM        CMU_CFGCLKS1_PLLTRIM_Msk

#define CMU_CFGCLKS1_PLLLOCKCFG_Pos (28U)
#define CMU_CFGCLKS1_PLLLOCKCFG_Msk (0x7U << CMU_CFGCLKS1_PLLLOCKCFG_Pos)
#define CMU_CFGCLKS1_PLLLOCKCFG     CMU_CFGCLKS1_PLLLOCKCFG_Msk

/*******************  Bit definition for CMU_PLLSTA register  ********************/
#define CMU_PLLSTA_PLLLOCK_Pos  (4U)
#define CMU_PLLSTA_PLLLOCK_Msk  (0x1U << CMU_PLLSTA_PLLLOCK_Pos)                    /*!< 0x00000010 */
#define CMU_PLLSTA_PLLLOCK      CMU_PLLSTA_PLLLOCK_Msk                              /*!<a2m_pll_lock_flag */

/*******************  Bit definition for CMU_TICKREF register  ********************/
#define CMU_TICKREF_TENMS_Pos   (0U)
#define CMU_TICKREF_TENMS_Msk   (0xFFFFFFU << CMU_TICKREF_TENMS_Pos)                /*!< 0x00FFFFFF */
#define CMU_TICKREF_TENMS       CMU_TICKREF_TENMS_Msk                               /*!<sys_tick_ref_tenms[23:0] */

#define CMU_TICKREF_SKEW_Pos    (30U)
#define CMU_TICKREF_SKEW_Msk    (0x1U << CMU_TICKREF_SKEW_Pos)                      /*!< 0x40000000 */
#define CMU_TICKREF_SKEW        CMU_TICKREF_SKEW_Msk                                /*!<sys_tick_ref_skew */

#define CMU_TICKREF_ENB_Pos (31U)
#define CMU_TICKREF_ENB_Msk (0x1U << CMU_TICKREF_ENB_Pos)                           /*!< 0x80000000 */
#define CMU_TICKREF_ENB     CMU_TICKREF_ENB_Msk                                     /*!<sys_tick_ref_enb */

/*******************  Bit definition for CMU_IRC16MRDY register  ********************/
#define CMU_IRC16MRDY_CNT_Pos   (0U)
#define CMU_IRC16MRDY_CNT_Msk   (0xFFU << CMU_IRC16MRDY_CNT_Pos)                    /*!< 0x000000FF */
#define CMU_IRC16MRDY_CNT       CMU_IRC16MRDY_CNT_Msk                               /*!<cfg_cnt_ready_irc16m[7:0] */

/*******************  Bit definition for CMU_OSCRDY register  ********************/
#define CMU_OSCRDY_CNT_Pos  (0U)
#define CMU_OSCRDY_CNT_Msk  (0xFFU << CMU_OSCRDY_CNT_Pos)                           /*!< 0x000000FF */
#define CMU_OSCRDY_CNT      CMU_OSCRDY_CNT_Msk                                      /*!<cfg_cnt_ready_osc[7:0] */

/*******************  Bit definition for CMU_PLLRDY register  ********************/
#define CMU_PLLRDY_CNT_Pos  (0U)
#define CMU_PLLRDY_CNT_Msk  (0xFFFFU << CMU_PLLRDY_CNT_Pos)                         /*!< 0x0000FFFF */
#define CMU_PLLRDY_CNT      CMU_PLLRDY_CNT_Msk                                      /*!<cfg_cnt_ready_pll[15:0] */

/*******************  Bit definition for CMU_DFSRDY register  ********************/
#define CMU_DFSRDY_CNT_Pos  (0U)
#define CMU_DFSRDY_CNT_Msk  (0xFFU << CMU_DFSRDY_CNT_Pos)                           /*!< 0x000000FF */
#define CMU_DFSRDY_CNT      CMU_DFSRDY_CNT_Msk                                      /*!<cfg_cnt_ready_dfs[7:0] */

/*******************  Bit definition for CMU_USB48MRDY register  ********************/
#define CMU_USB48MRDY_CNT_Pos   (0U)
#define CMU_USB48MRDY_CNT_Msk   (0xFFU << CMU_USB48MRDY_CNT_Pos)                    /*!< 0x000000FF */
#define CMU_USB48MRDY_CNT       CMU_USB48MRDY_CNT_Msk                               /*!<cfg_cnt_ready_usbphy48m[7:0] */

/*******************  Bit definition for CMU_GPIORDY register  ********************/
#define CMU_GPIORDY_CNT_Pos (0U)
#define CMU_GPIORDY_CNT_Msk (0xFU << CMU_GPIORDY_CNT_Pos)                           /*!< 0x0000000F */
#define CMU_GPIORDY_CNT     CMU_GPIORDY_CNT_Msk                                     /*!<cfg_cnt_ready_gpio[3:0] */

/*******************  Bit definition for CMU_CLKVLDF register  ********************/
#define CMU_CLKVLDF_GPIO_Pos    (0U)
#define CMU_CLKVLDF_GPIO_Msk    (0x1U << CMU_CLKVLDF_GPIO_Pos)                      /*!< 0x00000001 */
#define CMU_CLKVLDF_GPIO        CMU_CLKVLDF_GPIO_Msk                                /*!<clk_gate_valid_gpio */

#define CMU_CLKVLDF_USB48M_Pos  (2U)
#define CMU_CLKVLDF_USB48M_Msk  (0x1U << CMU_CLKVLDF_USB48M_Pos)                    /*!< 0x00000004 */
#define CMU_CLKVLDF_USB48M      CMU_CLKVLDF_USB48M_Msk                              /*!<clk_gate_valid_usbphy48m */

#define CMU_CLKVLDF_PLL_Pos (4U)
#define CMU_CLKVLDF_PLL_Msk (0x1U << CMU_CLKVLDF_PLL_Pos)                           /*!< 0x00000010 */
#define CMU_CLKVLDF_PLL     CMU_CLKVLDF_PLL_Msk                                     /*!<clk_gate_valid_pll */

#define CMU_CLKVLDF_OSC_Pos (5U)
#define CMU_CLKVLDF_OSC_Msk (0x1U << CMU_CLKVLDF_OSC_Pos)                           /*!< 0x00000020 */
#define CMU_CLKVLDF_OSC     CMU_CLKVLDF_OSC_Msk                                     /*!<clk_gate_valid_osc */

#define CMU_CLKVLDF_IRC16M_Pos  (6U)
#define CMU_CLKVLDF_IRC16M_Msk  (0x1U << CMU_CLKVLDF_IRC16M_Pos)                    /*!< 0x00000040 */
#define CMU_CLKVLDF_IRC16M      CMU_CLKVLDF_IRC16M_Msk                              /*!<clk_gate_valid_irc16m */

/*******************  Bit definition for CMU_MCGCLKA register  ********************/
#define CMU_MCGCLKA_IRCSEL_Pos  (0U)
#define CMU_MCGCLKA_IRCSEL_Msk  (0x1U << CMU_MCGCLKA_IRCSEL_Pos)                    /*!< 0x00000001 */
#define CMU_MCGCLKA_IRCSEL      CMU_MCGCLKA_IRCSEL_Msk                              /*!<cfg_sel_mcg_irc_clks */

#define CMU_MCGCLKA_IRCDIV_Pos  (4U)
#define CMU_MCGCLKA_IRCDIV_Msk  (0x3U << CMU_MCGCLKA_IRCDIV_Pos)                    /*!< 0x00000030 */
#define CMU_MCGCLKA_IRCDIV      CMU_MCGCLKA_IRCDIV_Msk                              /*!<cfg_div_mcg_irc_clks[1:0] */

#define CMU_MCGCLKA_MCLKSEL_Pos (8U)
#define CMU_MCGCLKA_MCLKSEL_Msk (0x7U << CMU_MCGCLKA_MCLKSEL_Pos)                   /*!< 0x00000700 */
#define CMU_MCGCLKA_MCLKSEL     CMU_MCGCLKA_MCLKSEL_Msk                             /*!<cfg_sel_mcg_main_clks[2:0] */

#define CMU_MCGCLKA_MCLKDIV_Pos (12U)
#define CMU_MCGCLKA_MCLKDIV_Msk (0x3U << CMU_MCGCLKA_MCLKDIV_Pos)                   /*!< 0x00003000 */
#define CMU_MCGCLKA_MCLKDIV     CMU_MCGCLKA_MCLKDIV_Msk                             /*!<cfg_div_mcg_main_clks[1:0] */

#define CMU_MCGCLKA_PLLDIV_Pos  (20U)
#define CMU_MCGCLKA_PLLDIV_Msk  (0x1FU << CMU_MCGCLKA_PLLDIV_Pos)                   /*!< 0x01F00000 */
#define CMU_MCGCLKA_PLLDIV      CMU_MCGCLKA_PLLDIV_Msk                              /*!<cfg_div_mcg_pll_div_clk[3:0] */

#define CMU_MCGCLKA_PLLDIVEN_Pos    (28U)
#define CMU_MCGCLKA_PLLDIVEN_Msk    (0x1U << CMU_MCGCLKA_PLLDIVEN_Pos)              /*!< 0x10000000 */
#define CMU_MCGCLKA_PLLDIVEN        CMU_MCGCLKA_PLLDIVEN_Msk                        /*!<cfg_div_mcg_pll_div_clk_en */

/*******************  Bit definition for CMU_MCGCLKB register  ********************/
#define CMU_MCGCLKB_FSCLKS_Pos  (0U)
#define CMU_MCGCLKB_FSCLKS_Msk  (0x1U << CMU_MCGCLKB_FSCLKS_Pos)                    /*!< 0x00000001 */
#define CMU_MCGCLKB_FSCLKS      CMU_MCGCLKB_FSCLKS_Msk                              /*!<cfg_sel_mcg_flash_clks */

#define CMU_MCGCLKB_DIVIOCLKS_Pos   (4U)
#define CMU_MCGCLKB_DIVIOCLKS_Msk   (0x7U << CMU_MCGCLKB_DIVIOCLKS_Pos)             /*!< 0x00000070 */
#define CMU_MCGCLKB_DIVIOCLKS       CMU_MCGCLKB_DIVIOCLKS_Msk                       /*!<cfg_div_mcg_io_clks[2:0] */

#define CMU_MCGCLKB_SELIOCLKS_Pos   (8U)
#define CMU_MCGCLKB_SELIOCLKS_Msk   (0x7U << CMU_MCGCLKB_SELIOCLKS_Pos)             /*!< 0x00000700 */
#define CMU_MCGCLKB_SELIOCLKS       CMU_MCGCLKB_SELIOCLKS_Msk                       /*!<cfg_sel_mcg_io_clks[2:0] */

/*******************  Bit definition for CMU_MCLKS register  ********************/
#define CMU_MCLKS_DFSSCY_Pos    (0U)
#define CMU_MCLKS_DFSSCY_Msk    (0xFU << CMU_MCLKS_DFSSCY_Pos)                      /*!< 0x0000000F */
#define CMU_MCLKS_DFSSCY        CMU_MCLKS_DFSSCY_Msk                                /*!<cfg_dfs_security_clks[3:0] */

#define CMU_MCLKS_BUSDIV_Pos    (8U)
#define CMU_MCLKS_BUSDIV_Msk    (0xFU << CMU_MCLKS_BUSDIV_Pos)                      /*!< 0x00000F00 */
#define CMU_MCLKS_BUSDIV        CMU_MCLKS_BUSDIV_Msk                                /*!<cfg_div_bus_clks[3:0] */

#define CMU_MCLKS_SYSDIV_Pos    (12U)
#define CMU_MCLKS_SYSDIV_Msk    (0xFU << CMU_MCLKS_SYSDIV_Pos)                      /*!< 0x0000F000 */
#define CMU_MCLKS_SYSDIV        CMU_MCLKS_SYSDIV_Msk                                /*!<cfg_div_sys_clks[3:0] */

/*******************  Bit definition for CMU_MDLCLKA register  ********************/
#define CMU_MDLCLKA_TRNGDIV_Pos (0U)
#define CMU_MDLCLKA_TRNGDIV_Msk (0x3U << CMU_MDLCLKA_TRNGDIV_Pos)                   /*!< 0x00000003 */
#define CMU_MDLCLKA_TRNGDIV     CMU_MDLCLKA_TRNGDIV_Msk                             /*!<cfg_div_trng_clks[1:0] */

#define CMU_MDLCLKA_CTDIV_Pos   (12U)
#define CMU_MDLCLKA_CTDIV_Msk   (0x3U << CMU_MDLCLKA_CTDIV_Pos)                     /*!< 0x00003000 */
#define CMU_MDLCLKA_CTDIV       CMU_MDLCLKA_CTDIV_Msk                               /*!<cfg_div_ct_clks[1:0] */

#define CMU_MDLCLKA_CTSEL_Pos   (14U)
#define CMU_MDLCLKA_CTSEL_Msk   (0x3U << CMU_MDLCLKA_CTSEL_Pos)                     /*!< 0x0000C000 */
#define CMU_MDLCLKA_CTSEL       CMU_MDLCLKA_CTSEL_Msk                               /*!<cfg_sel_ct_clks[1:0] */

#define CMU_MDLCLKA_UARTDIV_Pos (20U)
#define CMU_MDLCLKA_UARTDIV_Msk (0x3U << CMU_MDLCLKA_UARTDIV_Pos)                   /*!< 0x00300000 */
#define CMU_MDLCLKA_UARTDIV     CMU_MDLCLKA_UARTDIV_Msk                             /*!<cfg_div_uart_clks[1:0] */

#define CMU_MDLCLKA_UARTSEL_Pos (22U)
#define CMU_MDLCLKA_UARTSEL_Msk (0x3U << CMU_MDLCLKA_UARTSEL_Pos)                   /*!< 0x00C00000 */
#define CMU_MDLCLKA_UARTSEL     CMU_MDLCLKA_UARTSEL_Msk                             /*!<cfg_sel_uart_clks[1:0] */

#define CMU_MDLCLKA_USBFSSEL_Pos    (24U)
#define CMU_MDLCLKA_USBFSSEL_Msk    (0x3U << CMU_MDLCLKA_USBFSSEL_Pos)              /*!< 0x03000000 */
#define CMU_MDLCLKA_USBFSSEL        CMU_MDLCLKA_USBFSSEL_Msk                        /*!<cfg_sel_usb_fs_clks[1:0] */

/*******************  Bit definition for CMU_MDLCLKB register  ********************/
#define CMU_MDLCLKB_SLDDIV_Pos  (0U)
#define CMU_MDLCLKB_SLDDIV_Msk  (0x7U << CMU_MDLCLKB_SLDDIV_Pos)                    /*!< 0x00000007 */
#define CMU_MDLCLKB_SLDDIV      CMU_MDLCLKB_SLDDIV_Msk                              /*!<cfg_div_shield_clks[2:0] */

#define CMU_MDLCLKB_SLDSEL_Pos  (3U)
#define CMU_MDLCLKB_SLDSEL_Msk  (0x1U << CMU_MDLCLKB_SLDSEL_Pos)                    /*!< 0x00000008 */
#define CMU_MDLCLKB_SLDSEL      CMU_MDLCLKB_SLDSEL_Msk                              /*!<cfg_sel_shield_clks */

#define CMU_MDLCLKB_AONBUSCLK_Pos   (6U)
#define CMU_MDLCLKB_AONBUSCLK_Msk   (0x3U << CMU_MDLCLKB_AONBUSCLK_Pos)             /*!< 0x000000C0 */
#define CMU_MDLCLKB_AONBUSCLK       CMU_MDLCLKB_AONBUSCLK_Msk                       /*!<cfg_div_alwayson_bus_clks[1:0] */

#define CMU_MDLCLKB_DIVRTCMBCLK_Pos (8U)
#define CMU_MDLCLKB_DIVRTCMBCLK_Msk (0x3U << CMU_MDLCLKB_DIVRTCMBCLK_Pos)           /*!< 0x00000300 */
#define CMU_MDLCLKB_DIVRTCMBCLK     CMU_MDLCLKB_DIVRTCMBCLK_Msk                     /*!<cfg_div_rtc_main_bus_clks[1:0] */

#define CMU_MDLCLKB_WDTSEL_Pos  (11U)
#define CMU_MDLCLKB_WDTSEL_Msk  (0x1U << CMU_MDLCLKB_WDTSEL_Pos)                    /*!< 0x00000400 */
#define CMU_MDLCLKB_WDTSEL      CMU_MDLCLKB_WDTSEL_Msk                              /*!<cfg_sel_wdt_clks */

#define CMU_MDLCLKB_ADCDIV_Pos  (12U)
#define CMU_MDLCLKB_ADCDIV_Msk  (0x3U << CMU_MDLCLKB_ADCDIV_Pos)                    /*!< 0x00003000 */
#define CMU_MDLCLKB_ADCDIV      CMU_MDLCLKB_ADCDIV_Msk                              /*!<cfg_div_adc_clks[1:0] */

#define CMU_MDLCLKB_ADCSEL_Pos  (14U)
#define CMU_MDLCLKB_ADCSEL_Msk  (0x3U << CMU_MDLCLKB_ADCSEL_Pos)                    /*!< 0x0000C000 */
#define CMU_MDLCLKB_ADCSEL      CMU_MDLCLKB_ADCSEL_Msk                              /*!<cfg_sel_adc_clks[1:0] */

#define CMU_MDLCLKB_TST0SEL_Pos (16U)
#define CMU_MDLCLKB_TST0SEL_Msk (0x7U << CMU_MDLCLKB_TST0SEL_Pos)                   /*!< 0x00070000 */
#define CMU_MDLCLKB_TST0SEL     CMU_MDLCLKB_TST0SEL_Msk                             /*!<cfg_sel_test0_clks[2:0] */

#define CMU_MDLCLKB_TST1SEL_Pos (20U)
#define CMU_MDLCLKB_TST1SEL_Msk (0x7U << CMU_MDLCLKB_TST1SEL_Pos)                   /*!< 0x00700000 */
#define CMU_MDLCLKB_TST1SEL     CMU_MDLCLKB_TST1SEL_Msk                             /*!<cfg_sel_test1_clks[2:0] */

#define CMU_MDLCLKB_TST2SEL_Pos (24U)
#define CMU_MDLCLKB_TST2SEL_Msk (0x7U << CMU_MDLCLKB_TST2SEL_Pos)                   /*!< 0x07000000 */
#define CMU_MDLCLKB_TST2SEL     CMU_MDLCLKB_TST2SEL_Msk                             /*!<cfg_sel_test2_clks[2:0] */

#define CMU_MDLCLKB_DIVBKPTSTCLK_Pos    (28U)
#define CMU_MDLCLKB_DIVBKPTSTCLK_Msk    (0x7U << CMU_MDLCLKB_DIVBKPTSTCLK_Pos)      /*!< 0x70000000 */
#define CMU_MDLCLKB_DIVBKPTSTCLK        CMU_MDLCLKB_DIVBKPTSTCLK_Msk                /*!<cfg_sel_test2_clks[2:0] */

/*******************  Bit definition for CMU_MDLCLKC register  ********************/
#define CMU_MDLCLKC_STMCLKSEL_Pos( n )  (n * 4U)
#define CMU_MDLCLKC_STMCLKSEL_Msk( n )  (0x7U << CMU_MDLCLKC_STMCLKSEL_Pos( n ) )   /*!< 0x00000007 */
#define CMU_MDLCLKC_STMCLKSEL( n )      CMU_MDLCLKC_STMCLKSEL_Msk( n )              /*!<cfg_sel_stimer_n_clks[2:0] */

#define CMU_MDLCLKC_STM0CLKSEL_Pos  (0U)
#define CMU_MDLCLKC_STM0CLKSEL_Msk  (0x7U << CMU_MDLCLKC_STM0CLKSEL_Pos)            /*!< 0x00000007 */
#define CMU_MDLCLKC_STM0CLKSEL      CMU_MDLCLKC_STM0CLKSEL_Msk                      /*!<cfg_sel_stimer0_clks[2:0] */

#define CMU_MDLCLKC_STM1CLKSEL_Pos  (4U)
#define CMU_MDLCLKC_STM1CLKSEL_Msk  (0x7U << CMU_MDLCLKC_STM1CLKSEL_Pos)            /*!< 0x00000007 */
#define CMU_MDLCLKC_STM1CLKSEL      CMU_MDLCLKC_STM1CLKSEL_Msk                      /*!<cfg_sel_stimer1_clks[2:0] */

#define CMU_MDLCLKC_STM2CLKSEL_Pos  (8U)
#define CMU_MDLCLKC_STM2CLKSEL_Msk  (0x7U << CMU_MDLCLKC_STM2CLKSEL_Pos)            /*!< 0x00000700 */
#define CMU_MDLCLKC_STM2CLKSEL      CMU_MDLCLKC_STM2CLKSEL_Msk                      /*!<cfg_sel_stimer2_clks[2:0] */

#define CMU_MDLCLKC_STM3CLKSEL_Pos  (12U)
#define CMU_MDLCLKC_STM3CLKSEL_Msk  (0x7U << CMU_MDLCLKC_STM3CLKSEL_Pos)            /*!< 0x00007000 */
#define CMU_MDLCLKC_STM3CLKSEL      CMU_MDLCLKC_STM3CLKSEL_Msk                      /*!<cfg_sel_stimer3_clks[2:0] */

#define CMU_MDLCLKC_STM4CLKSEL_Pos  (16U)
#define CMU_MDLCLKC_STM4CLKSEL_Msk  (0x7U << CMU_MDLCLKC_STM4CLKSEL_Pos)            /*!< 0x00070000 */
#define CMU_MDLCLKC_STM4CLKSEL      CMU_MDLCLKC_STM4CLKSEL_Msk                      /*!<cfg_sel_stimer4_clks[2:0] */

#define CMU_MDLCLKC_STM5CLKSEL_Pos  (20U)
#define CMU_MDLCLKC_STM5CLKSEL_Msk  (0x7U << CMU_MDLCLKC_STM5CLKSEL_Pos)            /*!< 0x00700000 */
#define CMU_MDLCLKC_STM5CLKSEL      CMU_MDLCLKC_STM5CLKSEL_Msk                      /*!<cfg_sel_stimer5_clks[2:0] */

#define CMU_MDLCLKC_USBPHYCLKSEL_Pos    (24U)
#define CMU_MDLCLKC_USBPHYCLKSEL_Msk    (0x3U << CMU_MDLCLKC_USBPHYCLKSEL_Pos)      /*!< 0x03000000 */
#define CMU_MDLCLKC_USBPHYCLKSEL        CMU_MDLCLKC_USBPHYCLKSEL_Msk                /*!<cfg_sel_usb_phy_clks[1:0] */

#define CMU_MDLCLKC_USBPHYCLKDIV_Pos    (28U)
#define CMU_MDLCLKC_USBPHYCLKDIV_Msk    (0xFU << CMU_MDLCLKC_USBPHYCLKDIV_Pos)      /*!< 0xF0000000 */
#define CMU_MDLCLKC_USBPHYCLKDIV        CMU_MDLCLKC_USBPHYCLKDIV_Msk                /*!<cfg_div_usb_phy_clks[3:0] */

/*******************  Bit definition for CMU_MDLCLKD register  ********************/
#define CMU_MDLCLKC_SELSYSTKRFCLK_Pos   (0U)
#define CMU_MDLCLKC_SELSYSTKRFCLK_Msk   (0x3U << CMU_MDLCLKC_SELSYSTKRFCLK_Pos)     /*!< 0x00000003 */
#define CMU_MDLCLKC_SELSYSTKRFCLK       CMU_MDLCLKC_SELSYSTKRFCLK_Msk               /*!<cfg_sel_sys_tick_ref_clks[1:0] */

#define CMU_MDLCLKC_DIVSYSTKRFCLK_Pos   (4U)
#define CMU_MDLCLKC_DIVSYSTKRFCLK_Msk   (0x7U << CMU_MDLCLKC_DIVSYSTKRFCLK_Pos)     /*!< 0x00000070 */
#define CMU_MDLCLKC_DIVSYSTKRFCLK       CMU_MDLCLKC_DIVSYSTKRFCLK_Msk               /*!<cfg_div_sys_tick_ref_clks[2:0] */

#define CMU_MDLCLKC_SYSDIVCLK_Pos   (8U)
#define CMU_MDLCLKC_SYSDIVCLK_Msk   (0x3U << CMU_MDLCLKC_SYSDIVCLK_Pos)             /*!< 0x00000300 */
#define CMU_MDLCLKC_SYSDIVCLK       CMU_MDLCLKC_SYSDIVCLK_Msk                       /*!<cfg_div_sys_div_clks[1:0] */

#define CMU_MDLCLKC_SYSTESTCLK_Pos  (12U)
#define CMU_MDLCLKC_SYSTESTCLK_Msk  (0xFU << CMU_MDLCLKC_SYSTESTCLK_Pos)            /*!< 0x0000F000 */
#define CMU_MDLCLKC_SYSTESTCLK      CMU_MDLCLKC_SYSTESTCLK_Msk                      /*!<cfg_div_sys_test_clks[3:0] */

#define CMU_MDLCLKC_SELPLLECLK_Pos  (16U)
#define CMU_MDLCLKC_SELPLLECLK_Msk  (0x3U << CMU_MDLCLKC_SELPLLECLK_Pos)            /*!< 0x00030000 */
#define CMU_MDLCLKC_SELPLLECLK      CMU_MDLCLKC_SELPLLECLK_Msk                      /*!<cfg_sel_pll_ext_clks[1:0] */

#define CMU_MDLCLKC_DIVPLLECLK_Pos  (18U)
#define CMU_MDLCLKC_DIVPLLECLK_Msk  (0x3U << CMU_MDLCLKC_DIVPLLECLK_Pos)            /*!< 0x000C0000 */
#define CMU_MDLCLKC_DIVPLLECLK      CMU_MDLCLKC_DIVPLLECLK_Msk                      /*!<cfg_div_pll_ext_clks[1:0] */

/*******************  Bit definition for CMU_GMDLCLK register  ********************/
#define CMU_GMDLCLK_SHIELD_Pos  (0U)
#define CMU_GMDLCLK_SHIELD_Msk  (0x1U << CMU_GMDLCLK_SHIELD_Pos)                    /*!< 0x00000001 */
#define CMU_GMDLCLK_SHIELD      CMU_GMDLCLK_SHIELD_Msk                              /*!<cfg_gate_shield_clks */

#define CMU_GMDLCLK_WDT_Pos (4U)
#define CMU_GMDLCLK_WDT_Msk (0x1U << CMU_GMDLCLK_WDT_Pos)                           /*!< 0x00000010 */
#define CMU_GMDLCLK_WDT     CMU_GMDLCLK_WDT_Msk                                     /*!<cfg_gate_wdt_clks */

#define CMU_GMDLCLK_ADC_Pos (5U)
#define CMU_GMDLCLK_ADC_Msk (0x1U << CMU_GMDLCLK_ADC_Pos)                           /*!< 0x00000020 */
#define CMU_GMDLCLK_ADC     CMU_GMDLCLK_ADC_Msk                                     /*!<cfg_gate_adc_clks */

#define CMU_GMDLCLK_TRNG_Pos    (6U)
#define CMU_GMDLCLK_TRNG_Msk    (0x1U << CMU_GMDLCLK_TRNG_Pos)                      /*!< 0x00000040 */
#define CMU_GMDLCLK_TRNG        CMU_GMDLCLK_TRNG_Msk                                /*!<cfg_gate_trng_clks */

#define CMU_GMDLCLK_CT_Pos  (9U)
#define CMU_GMDLCLK_CT_Msk  (0x1U << CMU_GMDLCLK_CT_Pos)                            /*!< 0x00000200 */
#define CMU_GMDLCLK_CT      CMU_GMDLCLK_CT_Msk                                      /*!<cfg_gate_ct_clks */

#define CMU_GMDLCLK_UART_Pos    (11U)
#define CMU_GMDLCLK_UART_Msk    (0x1U << CMU_GMDLCLK_UART_Pos)                      /*!< 0x00000800 */
#define CMU_GMDLCLK_UART        CMU_GMDLCLK_UART_Msk                                /*!<cfg_gate_uart_clks */

#define CMU_GMDLCLK_USBFS_Pos   (12U)
#define CMU_GMDLCLK_USBFS_Msk   (0x1U << CMU_GMDLCLK_USBFS_Pos)                     /*!< 0x00001000 */
#define CMU_GMDLCLK_USBFS       CMU_GMDLCLK_USBFS_Msk                               /*!<cfg_gate_usb_fs_clks */

#define CMU_GMDLCLK_TST0_Pos    (13U)
#define CMU_GMDLCLK_TST0_Msk    (0x1U << CMU_GMDLCLK_TST0_Pos)                      /*!< 0x00002000 */
#define CMU_GMDLCLK_TST0        CMU_GMDLCLK_TST0_Msk                                /*!<cfg_gate_test0_clks */

#define CMU_GMDLCLK_TST1_Pos    (14U)
#define CMU_GMDLCLK_TST1_Msk    (0x1U << CMU_GMDLCLK_TST1_Pos)                      /*!< 0x00004000 */
#define CMU_GMDLCLK_TST1        CMU_GMDLCLK_TST1_Msk                                /*!<cfg_gate_test1_clks */

#define CMU_GMDLCLK_TST2_Pos    (15U)
#define CMU_GMDLCLK_TST2_Msk    (0x1U << CMU_GMDLCLK_TST2_Pos)                      /*!< 0x00008000 */
#define CMU_GMDLCLK_TST2        CMU_GMDLCLK_TST2_Msk                                /*!<cfg_gate_test2_clks */

#define CMU_GMDLCLK_STM0_Pos    (16U)
#define CMU_GMDLCLK_STM0_Msk    (0x1U << CMU_GMDLCLK_STM0_Pos)                      /*!< 0x00010000 */
#define CMU_GMDLCLK_STM0        CMU_GMDLCLK_STM0_Msk                                /*!<cfg_gate_stimer0_clks */

#define CMU_GMDLCLK_STM1_Pos    (17U)
#define CMU_GMDLCLK_STM1_Msk    (0x1U << CMU_GMDLCLK_STM1_Pos)                      /*!< 0x00020000 */
#define CMU_GMDLCLK_STM1        CMU_GMDLCLK_STM1_Msk                                /*!<cfg_gate_stimer1_clks */

#define CMU_GMDLCLK_STM2_Pos    (18U)
#define CMU_GMDLCLK_STM2_Msk    (0x1U << CMU_GMDLCLK_STM2_Pos)                      /*!< 0x00040000 */
#define CMU_GMDLCLK_STM2        CMU_GMDLCLK_STM2_Msk                                /*!<cfg_gate_stimer2_clks */

#define CMU_GMDLCLK_STM3_Pos    (19U)
#define CMU_GMDLCLK_STM3_Msk    (0x1U << CMU_GMDLCLK_STM3_Pos)                      /*!< 0x00080000 */
#define CMU_GMDLCLK_STM3        CMU_GMDLCLK_STM3_Msk                                /*!<cfg_gate_stimer3_clks */

#define CMU_GMDLCLK_STM4_Pos    (20U)
#define CMU_GMDLCLK_STM4_Msk    (0x1U << CMU_GMDLCLK_STM4_Pos)                      /*!< 0x00100000 */
#define CMU_GMDLCLK_STM4        CMU_GMDLCLK_STM4_Msk                                /*!<cfg_gate_stimer4_clks */

#define CMU_GMDLCLK_STM5_Pos    (21U)
#define CMU_GMDLCLK_STM5_Msk    (0x1U << CMU_GMDLCLK_STM5_Pos)                      /*!< 0x00200000 */
#define CMU_GMDLCLK_STM5        CMU_GMDLCLK_STM5_Msk                                /*!<cfg_gate_stimer5_clks */

#define CMU_GMDLCLK_USBPHY_Pos  (22U)
#define CMU_GMDLCLK_USBPHY_Msk  (0x1U << CMU_GMDLCLK_USBPHY_Pos)                    /*!< 0x00400000 */
#define CMU_GMDLCLK_USBPHY      CMU_GMDLCLK_USBPHY_Msk                              /*!<cfg_gate_usb_phy_clks */

#define CMU_GMDLCLK_BKPTST_Pos  (23U)
#define CMU_GMDLCLK_BKPTST_Msk  (0x1U << CMU_GMDLCLK_BKPTST_Pos)                    /*!< 0x00400000 */
#define CMU_GMDLCLK_BKPTST      CMU_GMDLCLK_BKPTST_Msk

#define CMU_GMDLCLK_SYSTKRF_Pos (24U)
#define CMU_GMDLCLK_SYSTKRF_Msk (0x1U << CMU_GMDLCLK_SYSTKRF_Pos)                   /*!< 0x01000000 */
#define CMU_GMDLCLK_SYSTKRF     CMU_GMDLCLK_SYSTKRF_Msk                             /*!<cfg_gate_sys_tick_ref_clks */

#define CMU_GMDLCLK_SYSDIV_Pos  (25U)
#define CMU_GMDLCLK_SYSDIV_Msk  (0x1U << CMU_GMDLCLK_SYSDIV_Pos)                    /*!< 0x02000000 */
#define CMU_GMDLCLK_SYSDIV      CMU_GMDLCLK_SYSDIV_Msk                              /*!<cfg_gate_sys_div_clks */

#define CMU_GMDLCLK_SYSTST_Pos  (26U)
#define CMU_GMDLCLK_SYSTST_Msk  (0x1U << CMU_GMDLCLK_SYSTST_Pos)                    /*!< 0x04000000 */
#define CMU_GMDLCLK_SYSTST      CMU_GMDLCLK_SYSTST_Msk                              /*!<cfg_gate_sys_test_clks */

#define CMU_GMDLCLK_PLLEXT_Pos  (27U)
#define CMU_GMDLCLK_PLLEXT_Msk  (0x1U << CMU_GMDLCLK_PLLEXT_Pos)                    /*!< 0x08000000 */
#define CMU_GMDLCLK_PLLEXT      CMU_GMDLCLK_PLLEXT_Msk                              /*!<cfg_gate_pll_ext_clks */

/*******************  Bit definition for CMU_PMGMDLCLK register  ********************/
#define CMU_PMGMDLCLK_SHIELD_Pos    (0U)
#define CMU_PMGMDLCLK_SHIELD_Msk    (0x1U << CMU_PMGMDLCLK_SHIELD_Pos)              /*!< 0x00000001 */
#define CMU_PMGMDLCLK_SHIELD        CMU_PMGMDLCLK_SHIELD_Msk                        /*!<cfg_pmgate_en_shield_clks */

#define CMU_PMGMDLCLK_WDT_Pos   (4U)
#define CMU_PMGMDLCLK_WDT_Msk   (0x1U << CMU_PMGMDLCLK_WDT_Pos)                     /*!< 0x00000010 */
#define CMU_PMGMDLCLK_WDT       CMU_PMGMDLCLK_WDT_Msk                               /*!<cfg_pmgate_en_wdt_clks */

#define CMU_PMGMDLCLK_ADC_Pos   (5U)
#define CMU_PMGMDLCLK_ADC_Msk   (0x1U << CMU_PMGMDLCLK_ADC_Pos)                     /*!< 0x00000020 */
#define CMU_PMGMDLCLK_ADC       CMU_PMGMDLCLK_ADC_Msk                               /*!<cfg_pmgate_en_adc_clks */

#define CMU_PMGMDLCLK_TRNG_Pos  (6U)
#define CMU_PMGMDLCLK_TRNG_Msk  (0x1U << CMU_PMGMDLCLK_TRNG_Pos)                    /*!< 0x00000040 */
#define CMU_PMGMDLCLK_TRNG      CMU_PMGMDLCLK_TRNG_Msk                              /*!<cfg_pmgate_en_trng_clks */

#define CMU_PMGMDLCLK_CT_Pos    (9U)
#define CMU_PMGMDLCLK_CT_Msk    (0x1U << CMU_PMGMDLCLK_CT_Pos)                      /*!< 0x00000200 */
#define CMU_PMGMDLCLK_CT        CMU_PMGMDLCLK_CT_Msk                                /*!<cfg_pmgate_en_ct_clks */

#define CMU_PMGMDLCLK_UART_Pos  (11U)
#define CMU_PMGMDLCLK_UART_Msk  (0x1U << CMU_PMGMDLCLK_UART_Pos)                    /*!< 0x00000800 */
#define CMU_PMGMDLCLK_UART      CMU_PMGMDLCLK_UART_Msk                              /*!<cfg_pmgate_en_uart_clks */

#define CMU_PMGMDLCLK_USBFS_Pos (12U)
#define CMU_PMGMDLCLK_USBFS_Msk (0x1U << CMU_PMGMDLCLK_USBFS_Pos)                   /*!< 0x00001000 */
#define CMU_PMGMDLCLK_USBFS     CMU_PMGMDLCLK_USBFS_Msk                             /*!<cfg_pmgate_en_usb_fs_clks */

#define CMU_PMGMDLCLK_TST0_Pos  (13U)
#define CMU_PMGMDLCLK_TST0_Msk  (0x1U << CMU_PMGMDLCLK_TST0_Pos)                    /*!< 0x00002000 */
#define CMU_PMGMDLCLK_TST0      CMU_PMGMDLCLK_TST0_Msk                              /*!<cfg_pmgate_en_test0_clks */

#define CMU_PMGMDLCLK_TST1_Pos  (14U)
#define CMU_PMGMDLCLK_TST1_Msk  (0x1U << CMU_PMGMDLCLK_TST1_Pos)                    /*!< 0x00004000 */
#define CMU_PMGMDLCLK_TST1      CMU_PMGMDLCLK_TST1_Msk                              /*!<cfg_pmgate_en_test1_clks */

#define CMU_PMGMDLCLK_TST2_Pos  (15U)
#define CMU_PMGMDLCLK_TST2_Msk  (0x1U << CMU_PMGMDLCLK_TST2_Pos)                    /*!< 0x00008000 */
#define CMU_PMGMDLCLK_TST2      CMU_PMGMDLCLK_TST2_Msk                              /*!<cfg_pmgate_en_test2_clks */

#define CMU_PMGMDLCLK_STM0_Pos  (16U)
#define CMU_PMGMDLCLK_STM0_Msk  (0x1U << CMU_PMGMDLCLK_STM0_Pos)                    /*!< 0x00010000 */
#define CMU_PMGMDLCLK_STM0      CMU_PMGMDLCLK_STM0_Msk                              /*!<cfg_pmgate_en_stimer0_clks */

#define CMU_PMGMDLCLK_STM1_Pos  (17U)
#define CMU_PMGMDLCLK_STM1_Msk  (0x1U << CMU_PMGMDLCLK_STM1_Pos)                    /*!< 0x00020000 */
#define CMU_PMGMDLCLK_STM1      CMU_PMGMDLCLK_STM1_Msk                              /*!<cfg_pmgate_en_stimer1_clks */

#define CMU_PMGMDLCLK_STM2_Pos  (18U)
#define CMU_PMGMDLCLK_STM2_Msk  (0x1U << CMU_PMGMDLCLK_STM2_Pos)                    /*!< 0x00040000 */
#define CMU_PMGMDLCLK_STM2      CMU_PMGMDLCLK_STM2_Msk                              /*!<cfg_pmgate_en_stimer2_clks */

#define CMU_PMGMDLCLK_STM3_Pos  (19U)
#define CMU_PMGMDLCLK_STM3_Msk  (0x1U << CMU_PMGMDLCLK_STM3_Pos)                    /*!< 0x00080000 */
#define CMU_PMGMDLCLK_STM3      CMU_PMGMDLCLK_STM3_Msk                              /*!<cfg_pmgate_en_stimer3_clks */

#define CMU_PMGMDLCLK_STM4_Pos  (20U)
#define CMU_PMGMDLCLK_STM4_Msk  (0x1U << CMU_PMGMDLCLK_STM4_Pos)                    /*!< 0x00100000 */
#define CMU_PMGMDLCLK_STM4      CMU_PMGMDLCLK_STM4_Msk                              /*!<cfg_pmgate_en_stimer4_clks */

#define CMU_PMGMDLCLK_STM5_Pos  (21U)
#define CMU_PMGMDLCLK_STM5_Msk  (0x1U << CMU_PMGMDLCLK_STM5_Pos)                    /*!< 0x00200000 */
#define CMU_PMGMDLCLK_STM5      CMU_PMGMDLCLK_STM5_Msk                              /*!<cfg_pmgate_en_stimer5_clks */

#define CMU_PMGMDLCLK_USBPHY_Pos    (22U)
#define CMU_PMGMDLCLK_USBPHY_Msk    (0x1U << CMU_PMGMDLCLK_USBPHY_Pos)              /*!< 0x00400000 */
#define CMU_PMGMDLCLK_USBPHY        CMU_PMGMDLCLK_USBPHY_Msk                        /*!<cfg_pmgate_en_usb_phy_clks */

#define CMU_PMGMDLCLK_BKPTST_Pos    (23U)
#define CMU_PMGMDLCLK_BKPTST_Msk    (0x1U << CMU_PMGMDLCLK_BKPTST_Pos)              /*!< 0x01000000 */
#define CMU_PMGMDLCLK_BKPTST        CMU_PMGMDLCLK_BKPTST_Msk

#define CMU_PMGMDLCLK_SYSTKRF_Pos   (24U)
#define CMU_PMGMDLCLK_SYSTKRF_Msk   (0x1U << CMU_PMGMDLCLK_SYSTKRF_Pos)             /*!< 0x01000000 */
#define CMU_PMGMDLCLK_SYSTKRF       CMU_PMGMDLCLK_SYSTKRF_Msk                       /*!<cfg_pmgate_en_sys_tick_ref_clks */

#define CMU_PMGMDLCLK_SYSDIV_Pos    (25U)
#define CMU_PMGMDLCLK_SYSDIV_Msk    (0x1U << CMU_PMGMDLCLK_SYSDIV_Pos)              /*!< 0x02000000 */
#define CMU_PMGMDLCLK_SYSDIV        CMU_PMGMDLCLK_SYSDIV_Msk                        /*!<cfg_pmgate_en_sys_div_clks */

#define CMU_PMGMDLCLK_SYSTST_Pos    (26U)
#define CMU_PMGMDLCLK_SYSTST_Msk    (0x1U << CMU_PMGMDLCLK_SYSTST_Pos)              /*!< 0x04000000 */
#define CMU_PMGMDLCLK_SYSTST        CMU_PMGMDLCLK_SYSTST_Msk                        /*!<cfg_pmgate_en_sys_test_clks */

#define CMU_PMGMDLCLK_PLLEXT_Pos    (27U)
#define CMU_PMGMDLCLK_PLLEXT_Msk    (0x1U << CMU_PMGMDLCLK_PLLEXT_Pos)              /*!< 0x08000000 */
#define CMU_PMGMDLCLK_PLLEXT        CMU_PMGMDLCLK_PLLEXT_Msk                        /*!<cfg_pmgate_en_pll_ext_clks */
//</h>
//<h>MBIST
/******************************************************************************/
/*                                                                            */
/*                                    MBIST                                   */
/*                                                                            */
/******************************************************************************/
/*******************  Bit definition for MBIST_CTRL register  ********************/
#define MBIST_CTRL_MBISTEN_Pos  (0U)
#define MBIST_CTRL_MBISTEN_Msk  (0x1U << MBIST_CTRL_MBISTEN_Pos)                /*!< 0x00000001 */
#define MBIST_CTRL_MBISTEN      MBIST_CTRL_MBISTEN_Msk                          /*!<mbist_en  */

#define MBIST_CTRL_VTRIMEN_Pos  (4U)
#define MBIST_CTRL_VTRIMEN_Msk  (0x3U << MBIST_CTRL_VTRIMEN_Pos)                /*!< 0x00000030 */
#define MBIST_CTRL_VTRIMEN      MBIST_CTRL_VTRIMEN_Msk                          /*!<mbist_vtrim_en[1:0] 01:Enable  */

#define MBIST_CTRL_LDO12HP_Pos  (8U)
#define MBIST_CTRL_LDO12HP_Msk  (0x3U << MBIST_CTRL_LDO12HP_Pos)                /*!< 0x00000300 */
#define MBIST_CTRL_LDO12HP      MBIST_CTRL_LDO12HP_Msk                          /*!<mbist_ldo12_hp_cfg[1:0]  */

#define MBIST_CTRL_LDO16M_Pos   (12U)
#define MBIST_CTRL_LDO16M_Msk   (0x3U << MBIST_CTRL_LDO16M_Pos)                 /*!< 0x00003000 */
#define MBIST_CTRL_LDO16M       MBIST_CTRL_LDO16M_Msk                           /*!<mbist_ldo16_mode[1:0]  */

#define MBIST_CTRL_VDD12TRIM_Pos    (16U)
#define MBIST_CTRL_VDD12TRIM_Msk    (0xFU << MBIST_CTRL_VDD12TRIM_Pos)          /*!< 0x000F0000 */
#define MBIST_CTRL_VDD12TRIM        MBIST_CTRL_VDD12TRIM_Msk                    /*!<Mbist_vdd12_hp_trim[3:0]  */

#define MBIST_CTRL_VDD16TRIM_Pos    (20U)
#define MBIST_CTRL_VDD16TRIM_Msk    (0xFU << MBIST_CTRL_VDD16TRIM_Pos)          /*!< 0x00F00000 */
#define MBIST_CTRL_VDD16TRIM        MBIST_CTRL_VDD16TRIM_Msk                    /*!<Mbist_vdd16_hp_trim[3:0]  */

#define MBIST_CTRL_MODESEL_Pos  (24U)
#define MBIST_CTRL_MODESEL_Msk  (0x3FU << MBIST_CTRL_MODESEL_Pos)               /*!< 0x3F000000 */
#define MBIST_CTRL_MODESEL      MBIST_CTRL_MODESEL_Msk                          /*!<mode_sel[5:0]  */

/*******************  Bit definition for MBIST_TM_CTRL0 register  ********************/
#define MBIST_TMCTRL0_TM1_Pos   (0U)
#define MBIST_TMCTRL0_TM1_Msk   (0x1FU << MBIST_TMCTRL0_TM1_Pos)                /*!< 0x0000001F */
#define MBIST_TMCTRL0_TM1       MBIST_TMCTRL0_TM1_Msk                           /*!<tm1[4:0]  */

#define MBIST_TMCTRL0_TM2_Pos   (5U)
#define MBIST_TMCTRL0_TM2_Msk   (0x1FU << MBIST_TMCTRL0_TM2_Pos)                /*!< 0x000003E0 */
#define MBIST_TMCTRL0_TM2       MBIST_TMCTRL0_TM2_Msk                           /*!<tm2[4:0]  */

#define MBIST_TMCTRL0_TM3_Pos   (10U)
#define MBIST_TMCTRL0_TM3_Msk   (0x1FU << MBIST_TMCTRL0_TM3_Pos)                /*!< 0x00007C00 */
#define MBIST_TMCTRL0_TM3       MBIST_TMCTRL0_TM3_Msk                           /*!<tm3[4:0]  */

#define MBIST_TMCTRL0_TM4_Pos   (15U)
#define MBIST_TMCTRL0_TM4_Msk   (0x1FU << MBIST_TMCTRL0_TM4_Pos)                /*!< 0x000F8000 */
#define MBIST_TMCTRL0_TM4       MBIST_TMCTRL0_TM4_Msk                           /*!<tm4[4:0]  */

#define MBIST_TMCTRL0_TM5_Pos   (20U)
#define MBIST_TMCTRL0_TM5_Msk   (0x1FU << MBIST_TMCTRL0_TM5_Pos)                /*!< 0x01F00000 */
#define MBIST_TMCTRL0_TM5       MBIST_TMCTRL0_TM5_Msk                           /*!<tm5[4:0]  */

#define MBIST_TMCTRL0_TMNUM_Pos (25U)
#define MBIST_TMCTRL0_TMNUM_Msk (0x7U << MBIST_TMCTRL0_TMNUM_Pos)               /*!< 0x0E000000 */
#define MBIST_TMCTRL0_TMNUM     MBIST_TMCTRL0_TMNUM_Msk                         /*!<tm_num[2:0]  */

/*******************  Bit definition for MBIST_TM_CTRL1 register  ********************/
#define MBIST_TMCTRL1_TM6_Pos   (0U)
#define MBIST_TMCTRL1_TM6_Msk   (0x1FU << MBIST_TMCTRL1_TM6_Pos)                /*!< 0x0000001F */
#define MBIST_TMCTRL1_TM6       MBIST_TMCTRL1_TM6_Msk                           /*!<tm6[4:0]  */

#define MBIST_TMCTRL1_TM7_Pos   (5U)
#define MBIST_TMCTRL1_TM7_Msk   (0x1FU << MBIST_TMCTRL1_TM7_Pos)                /*!< 0x000003E0 */
#define MBIST_TMCTRL1_TM7       MBIST_TMCTRL1_TM7_Msk                           /*!<tm7[4:0]  */

/*******************  Bit definition for MBIST_PIN_CTRL register  ********************/
#define MBIST_PCTRL_NVR_Pos (0U)
#define MBIST_PCTRL_NVR_Msk (0x1U << MBIST_PCTRL_NVR_Pos)                       /*!< 0x00000001 */
#define MBIST_PCTRL_NVR     MBIST_PCTRL_NVR_Msk                                 /*!<nvr  */

#define MBIST_PCTRL_NVRCFG_Pos  (1U)
#define MBIST_PCTRL_NVRCFG_Msk  (0x1U << MBIST_PCTRL_NVRCFG_Pos)                /*!< 0x00000002 */
#define MBIST_PCTRL_NVRCFG      MBIST_PCTRL_NVRCFG_Msk                          /*!<nvr_cfg  */

#define MBIST_PCTRL_ARRDN_Pos   (2U)
#define MBIST_PCTRL_ARRDN_Msk   (0xFU << MBIST_PCTRL_ARRDN_Pos)                 /*!< 0x0000003C */
#define MBIST_PCTRL_ARRDN       MBIST_PCTRL_ARRDN_Msk                           /*!<arrdn[3:0]  */

#define MBIST_PCTRL_RETRY_Pos   (6U)
#define MBIST_PCTRL_RETRY_Msk   (0x3U << MBIST_PCTRL_RETRY_Pos)                 /*!< 0x000000C0 */
#define MBIST_PCTRL_RETRY       MBIST_PCTRL_RETRY_Msk                           /*!<retry[1:0]  */

#define MBIST_PCTRL_VREAD1_Pos  (8U)
#define MBIST_PCTRL_VREAD1_Msk  (0x1U << MBIST_PCTRL_VREAD1_Pos)                /*!< 0x00000100 */
#define MBIST_PCTRL_VREAD1      MBIST_PCTRL_VREAD1_Msk                          /*!<vread1  */

#define MBIST_PCTRL_RECALL_Pos  (9U)
#define MBIST_PCTRL_RECALL_Msk  (0x1U << MBIST_PCTRL_RECALL_Pos)                /*!< 0x00000200 */
#define MBIST_PCTRL_RECALL      MBIST_PCTRL_RECALL_Msk                          /*!<recall  */

#define MBIST_PCTRL_RDEN_Pos    (10U)
#define MBIST_PCTRL_RDEN_Msk    (0x1U << MBIST_PCTRL_RDEN_Pos)                  /*!< 0x00000400 */
#define MBIST_PCTRL_RDEN        MBIST_PCTRL_RDEN_Msk                            /*!<rden  */

#define MBIST_PCTRL_CEB_Pos (11U)
#define MBIST_PCTRL_CEB_Msk (0x1U << MBIST_PCTRL_CEB_Pos)                       /*!< 0x00000800 */
#define MBIST_PCTRL_CEB     MBIST_PCTRL_CEB_Msk                                 /*!<ceb  */

#define MBIST_PCTRL_WEB_Pos (12U)
#define MBIST_PCTRL_WEB_Msk (0x1U << MBIST_PCTRL_WEB_Pos)                       /*!< 0x00001000 */
#define MBIST_PCTRL_WEB     MBIST_PCTRL_WEB_Msk                                 /*!<web  */

#define MBIST_PCTRL_PROG_Pos    (13U)
#define MBIST_PCTRL_PROG_Msk    (0x1U << MBIST_PCTRL_PROG_Pos)                  /*!< 0x00002000 */
#define MBIST_PCTRL_PROG        MBIST_PCTRL_PROG_Msk                            /*!<prog  */

#define MBIST_PCTRL_PROG2_Pos   (14U)
#define MBIST_PCTRL_PROG2_Msk   (0x1U << MBIST_PCTRL_PROG2_Pos)                 /*!< 0x00004000 */
#define MBIST_PCTRL_PROG2       MBIST_PCTRL_PROG2_Msk                           /*!<prog2  */

#define MBIST_PCTRL_PREPG_Pos   (15U)
#define MBIST_PCTRL_PREPG_Msk   (0x1U << MBIST_PCTRL_PREPG_Pos)                 /*!< 0x00008000 */
#define MBIST_PCTRL_PREPG       MBIST_PCTRL_PREPG_Msk                           /*!<prepg  */

#define MBIST_PCTRL_ERASE_Pos   (16U)
#define MBIST_PCTRL_ERASE_Msk   (0x1U << MBIST_PCTRL_ERASE_Pos)                 /*!< 0x00010000 */
#define MBIST_PCTRL_ERASE       MBIST_PCTRL_ERASE_Msk                           /*!<erase  */

#define MBIST_PCTRL_CHIP_Pos    (17U)
#define MBIST_PCTRL_CHIP_Msk    (0x1U << MBIST_PCTRL_CHIP_Pos)                  /*!< 0x00020000 */
#define MBIST_PCTRL_CHIP        MBIST_PCTRL_CHIP_Msk                            /*!<chip  */

#define MBIST_PCTRL_CONFEN_Pos  (18U)
#define MBIST_PCTRL_CONFEN_Msk  (0x1U << MBIST_PCTRL_CONFEN_Pos)                /*!< 0x00040000 */
#define MBIST_PCTRL_CONFEN      MBIST_PCTRL_CONFEN_Msk                          /*!<confen  */

#define MBIST_PCTRL_PORB_Pos    (19U)
#define MBIST_PCTRL_PORB_Msk    (0x1U << MBIST_PCTRL_PORB_Pos)                  /*!< 0x00080000 */
#define MBIST_PCTRL_PORB        MBIST_PCTRL_PORB_Msk                            /*!<porb  */

#define MBIST_PCTRL_WP_Pos  (20U)
#define MBIST_PCTRL_WP_Msk  (0x1U << MBIST_PCTRL_WP_Pos)                        /*!< 0x00100000 */
#define MBIST_PCTRL_WP      MBIST_PCTRL_WP_Msk                                  /*!<wp  */

#define MBIST_PCTRL_LPWR_Pos    (21U)
#define MBIST_PCTRL_LPWR_Msk    (0x1U << MBIST_PCTRL_LPWR_Pos)                  /*!< 0x00200000 */
#define MBIST_PCTRL_LPWR        MBIST_PCTRL_LPWR_Msk                            /*!<lpwr  */

#define MBIST_PCTRL_DPD_Pos (22U)
#define MBIST_PCTRL_DPD_Msk (0x1U << MBIST_PCTRL_DPD_Pos)                       /*!< 0x00400000 */
#define MBIST_PCTRL_DPD     MBIST_PCTRL_DPD_Msk                                 /*!<dpd  */

#define MBIST_PCTRL_TMEN_Pos    (23U)
#define MBIST_PCTRL_TMEN_Msk    (0x1U << MBIST_PCTRL_TMEN_Pos)                  /*!< 0x00800000 */
#define MBIST_PCTRL_TMEN        MBIST_PCTRL_TMEN_Msk                            /*!<tmen  */

/*******************  Bit definition for MBIST_ADDR register  ********************/
#define MBIST_ADDR_ADDR_Pos (0U)
#define MBIST_ADDR_ADDR_Msk (0x3FFFFU << MBIST_ADDR_ADDR_Pos)                   /*!< 0x0003FFFF */
#define MBIST_ADDR_ADDR     MBIST_ADDR_ADDR_Msk                                 /*!<addr[17:0]  */

/*******************  Bit definition for MBIST_AEND register  ********************/
#define MBIST_AEND_AEND_Pos (0U)
#define MBIST_AEND_AEND_Msk (0x3FFFFU << MBIST_AEND_AEND_Pos)                   /*!< 0x0003FFFF */
#define MBIST_AEND_AEND     MBIST_AEND_AEND_Msk                                 /*!<aend[17:0]  */

/*******************  Bit definition for MBIST_DIN0 register  ********************/
#define MBIST_DIN0_DIN0_Pos (0U)
#define MBIST_DIN0_DIN0_Msk (0xFFFFFFFFU << MBIST_DIN0_DIN0_Pos)                /*!< 0xFFFFFFFF */
#define MBIST_DIN0_DIN0     MBIST_DIN0_DIN0_Msk                                 /*!<din0[31:0]  */

/*******************  Bit definition for MBIST_DIN1 register  ********************/
#define MBIST_DIN1_DIN1_Pos (0U)
#define MBIST_DIN1_DIN1_Msk (0xFFFFFFFFU << MBIST_DIN1_DIN1_Pos)                /*!< 0xFFFFFFFF */
#define MBIST_DIN1_DIN1     MBIST_DIN1_DIN1_Msk                                 /*!<din1[31:0]  */

/*******************  Bit definition for MBIST_DOUT0L register  ********************/
#define MBIST_DOUT0L_DOUT_Pos   (0U)
#define MBIST_DOUT0L_DOUT_Msk   (0xFFFFFFFFU << MBIST_DOUT0L_DOUT_Pos)          /*!< 0xFFFFFFFF */
#define MBIST_DOUT0L_DOUT       MBIST_DOUT0L_DOUT_Msk                           /*!<Dout[31:0]  */

/*******************  Bit definition for MBIST_DOUT0H register  ********************/
#define MBIST_DOUT0H_DOUT_Pos   (0U)
#define MBIST_DOUT0H_DOUT_Msk   (0x7U << MBIST_DOUT0H_DOUT_Pos)                 /*!< 0x00000007 */
#define MBIST_DOUT0H_DOUT       MBIST_DOUT0H_DOUT_Msk                           /*!<Dout[34:32]  */

/*******************  Bit definition for MBIST_DOUT1L register  ********************/
#define MBIST_DOUT1L_DOUT_Pos   (0U)
#define MBIST_DOUT1L_DOUT_Msk   (0xFFFFFFFFU << MBIST_DOUT1L_DOUT_Pos)          /*!< 0xFFFFFFFF */
#define MBIST_DOUT1L_DOUT       MBIST_DOUT1L_DOUT_Msk                           /*!<Dout[66:35]  */

/*******************  Bit definition for MBIST_DOUT1H register  ********************/
#define MBIST_DOUT1H_DOUT_Pos   (0U)
#define MBIST_DOUT1H_DOUT_Msk   (0x7U << MBIST_DOUT1H_DOUT_Pos)                 /*!< 0x00000007 */
#define MBIST_DOUT1H_DOUT       MBIST_DOUT1H_DOUT_Msk                           /*!<Dout[69:67]  */

/*******************  Bit definition for MBIST_DOUT2L register  ********************/
#define MBIST_DOUT2L_DOUT_Pos   (0U)
#define MBIST_DOUT2L_DOUT_Msk   (0xFFFFFFFFU << MBIST_DOUT2L_DOUT_Pos)          /*!< 0xFFFFFFFF */
#define MBIST_DOUT2L_DOUT       MBIST_DOUT2L_DOUT_Msk                           /*!<Dout[101:70]  */

/*******************  Bit definition for MBIST_DOUT2H register  ********************/
#define MBIST_DOUT2H_DOUT_Pos   (0U)
#define MBIST_DOUT2H_DOUT_Msk   (0x7U << MBIST_DOUT2H_DOUT_Pos)                 /*!< 0x00000007 */
#define MBIST_DOUT2H_DOUT       MBIST_DOUT2H_DOUT_Msk                           /*!<Dout[104:102]  */

/*******************  Bit definition for MBIST_DOUT3L register  ********************/
#define MBIST_DOUT3L_DOUT_Pos   (0U)
#define MBIST_DOUT3L_DOUT_Msk   (0xFFFFFFFFU << MBIST_DOUT3L_DOUT_Pos)          /*!< 0xFFFFFFFF */
#define MBIST_DOUT3L_DOUT       MBIST_DOUT3L_DOUT_Msk                           /*!<Dout[136:105]  */

/*******************  Bit definition for MBIST_DOUT3H register  ********************/
#define MBIST_DOUT3H_DOUT_Pos   (0U)
#define MBIST_DOUT3H_DOUT_Msk   (0x7U << MBIST_DOUT3H_DOUT_Pos)                 /*!< 0x00000007 */
#define MBIST_DOUT3H_DOUT       MBIST_DOUT3H_DOUT_Msk                           /*!<Dout[139:137]  */

/*******************  Bit definition for MBIST_CHKRSLT register  ********************/
#define MBIST_CHKRSLT_DFCTCNT_Pos   (0U)
#define MBIST_CHKRSLT_DFCTCNT_Msk   (0x7U << MBIST_CHKRSLT_DFCTCNT_Pos)         /*!< 0x00000007 */
#define MBIST_CHKRSLT_DFCTCNT       MBIST_CHKRSLT_DFCTCNT_Msk                   /*!<dfct_cnt[2:0]  */

#define MBIST_CHKRSLT_DOUT0_Pos (3U)
#define MBIST_CHKRSLT_DOUT0_Msk (0x1U << MBIST_CHKRSLT_DOUT0_Pos)               /*!< 0x00000008 */
#define MBIST_CHKRSLT_DOUT0     MBIST_CHKRSLT_DOUT0_Msk                         /*!<dout0*/

/*******************  Bit definition for MBIST_DFCT_SECT0 register  ********************/
#define MBIST_DFCTSECT0_DS1_Pos (0U)
#define MBIST_DFCTSECT0_DS1_Msk (0x1FFU << MBIST_DFCTSECT0_DS1_Pos)             /*!< 0x000001FF */
#define MBIST_DFCTSECT0_DS1     MBIST_DFCTSECT0_DS1_Msk                         /*!<dfct_sect1[8:0]  */

#define MBIST_DFCTSECT0_DS2_Pos (16U)
#define MBIST_DFCTSECT0_DS2_Msk (0x1FFU << MBIST_DFCTSECT0_DS2_Pos)             /*!< 0x01FF0000 */
#define MBIST_DFCTSECT0_DS2     MBIST_DFCTSECT0_DS2_Msk                         /*!<dfct_sect2[8:0]  */

/*******************  Bit definition for MBIST_DFCT_SECT1 register  ********************/
#define MBIST_DFCTSECT1_DS3_Pos (0U)
#define MBIST_DFCTSECT1_DS3_Msk (0x1FFU << MBIST_DFCTSECT1_DS3_Pos)             /*!< 0x000001FF */
#define MBIST_DFCTSECT1_DS3     MBIST_DFCTSECT1_DS3_Msk                         /*!<dfct_sect3[8:0]  */

#define MBIST_DFCTSECT1_DS4_Pos (16U)
#define MBIST_DFCTSECT1_DS4_Msk (0x1FFU << MBIST_DFCTSECT1_DS4_Pos)             /*!< 0x01FF0000 */
#define MBIST_DFCTSECT1_DS4     MBIST_DFCTSECT1_DS4_Msk                         /*!<dfct_sect4[8:0]  */
//</h>
//<h>MCR
/******************************************************************************/
/*                                                                            */
/*                                    MCR                                     */
/*                                                                            */
/******************************************************************************/
/*******************  Bit definition for MCR_CHIP_MODE register  ********************/
//read operation
#define MCR_MODE_CHIPMODE_Pos   (0U)
#define MCR_MODE_CHIPMODE_Msk   (0x3U << MCR_MODE_CHIPMODE_Pos)                                 /*!< 0x00000003 */
#define MCR_MODE_CHIPMODE       MCR_MODE_CHIPMODE_Msk                                           /*!<CHIPMODE[1:0] MCR chip mode */

/*******************  Bit definition for MCR_NVM_FLAG register  ********************/
//read operation
#define MCR_NVMF_NVMFLAG_Pos    (0U)
#define MCR_NVMF_NVMFLAG_Msk    (0xFFFFFFFFU << MCR_NVMF_NVMFLAG_Pos)                           /*!< 0xFFFFFFFF */
#define MCR_NVMF_NVMFLAG        MCR_NVMF_NVMFLAG_Msk                                            /*!<NVMFLAG[15:0] Flash memory test flag */

/*******************  Bit definition for MCR_ORG_CFG register  ********************/
//read operation
#define MCR_ORGCFG_CLK4MSELN_Pos    (0U)
#define MCR_ORGCFG_CLK4MSELN_Msk    (0x1U << MCR_ORGCFG_CLK4MSELN_Pos)                          /*!< 0x00000001 */
#define MCR_ORGCFG_CLK4MSELN        MCR_ORGCFG_CLK4MSELN_Msk                                    /*!< CLK4M_SELn */

#define MCR_ORGCFG_NVMADDRCFGN_Pos  (1U)
#define MCR_ORGCFG_NVMADDRCFGN_Msk  (0x1U << MCR_ORGCFG_NVMADDRCFGN_Pos)                        /*!< 0x00000002 */
#define MCR_ORGCFG_NVMADDRCFGN      MCR_ORGCFG_NVMADDRCFGN_Msk                                  /*!< NVM_ADDR_CFGn */

/*******************  Bit definition for MCR_MANU_CFG register  ********************/
//read operation
#define MCR_MANUCFG_DEBUGOFF_Pos    (0U)
#define MCR_MANUCFG_DEBUGOFF_Msk    (0x1U << MCR_MANUCFG_DEBUGOFF_Pos)                          /*!< 0x00000001 */
#define MCR_MANUCFG_DEBUGOFF        MCR_MANUCFG_DEBUGOFF_Msk                                    /*!< b=1 Diable debug */

#define MCR_MANUCFG_RAM64K_Pos  (1U)
#define MCR_MANUCFG_RAM64K_Msk  (0x1U << MCR_MANUCFG_RAM64K_Pos)                                /*!< 0x00000002 */
#define MCR_MANUCFG_RAM64K      MCR_MANUCFG_RAM64K_Msk                                          /*!< 0:128K,1:64K */

#define MCR_MANUCFG_PATCHEN_Pos (2U)
#define MCR_MANUCFG_PATCHEN_Msk (0x1U << MCR_MANUCFG_PATCHEN_Pos)                               /*!< 0x00000004 */
#define MCR_MANUCFG_PATCHEN     MCR_MANUCFG_PATCHEN_Msk                                         /*!< 1:enable */

#define MCR_MANUCFG_BOOTMAPON_Pos   (3U)
#define MCR_MANUCFG_BOOTMAPON_Msk   (0x1U << MCR_MANUCFG_BOOTMAPON_Pos)                         /*!< 0x00000008 */
#define MCR_MANUCFG_BOOTMAPON       MCR_MANUCFG_BOOTMAPON_Msk                                   /*!< 1:Boot remap on */

#define MCR_MANUCFG_PATCHNUM_Pos    (8U)
#define MCR_MANUCFG_PATCHNUM_Msk    (0x3FU << MCR_MANUCFG_PATCHNUM_Pos)                         /*!< 0x00003F00 */
#define MCR_MANUCFG_PATCHNUM        MCR_MANUCFG_PATCHNUM_Msk                                    /*!< PATCH_NUM[5:0]:Patch Number */

#define MCR_MANUCFG_TRNGEN_Pos  (16U)
#define MCR_MANUCFG_TRNGEN_Msk  (0x3FFU << MCR_MANUCFG_TRNGEN_Pos)                              /*!< 0x03FF0000 */
#define MCR_MANUCFG_TRNGEN      MCR_MANUCFG_TRNGEN_Msk                                          /*!< 1:TRNG enable */

/*******************  Bit definition for MCR_USER_CFG register  ********************/
//read operation
#define MCR_USERCFG_DEBUGOFF_Pos    (0U)
#define MCR_USERCFG_DEBUGOFF_Msk    (0x1U << MCR_USERCFG_DEBUGOFF_Pos)                          /*!< 0x00000001 */
#define MCR_USERCFG_DEBUGOFF        MCR_USERCFG_DEBUGOFF_Msk                                    /*!< b=1 Diable debug */

#define MCR_USERCFG_BOOTMAPON_Pos   (1U)
#define MCR_USERCFG_BOOTMAPON_Msk   (0x1U << MCR_USERCFG_BOOTMAPON_Pos)                         /*!< 0x00000002 */
#define MCR_USERCFG_BOOTMAPON       MCR_USERCFG_BOOTMAPON_Msk                                   /*!<BOOTMAP_ON */

#define MCR_USERCFG_LOCK_Pos    (31U)
#define MCR_USERCFG_LOCK_Msk    (0x1U << MCR_USERCFG_LOCK_Pos)                                  /*!< 0x80000000 */
#define MCR_USERCFG_LOCK        MCR_USERCFG_LOCK_Msk                                            /*!< LOCK */

/*******************  Bit definition for MCR_OTP_CFG register  ********************/
//read operation
#define MCR_OTPCFG_DEBUGOFF_Pos (0U)
#define MCR_OTPCFG_DEBUGOFF_Msk (0x1U << MCR_OTPCFG_DEBUGOFF_Pos)                               /*!< 0x00000001 */
#define MCR_OTPCFG_DEBUGOFF     MCR_OTPCFG_DEBUGOFF_Msk                                         /*!< b=1 Diable debug */

#define MCR_OTPCFG_BOOTMAPOFF_Pos   (1U)
#define MCR_OTPCFG_BOOTMAPOFF_Msk   (0x1U << MCR_OTPCFG_BOOTMAPOFF_Pos)                         /*!< 0x00000002 */
#define MCR_OTPCFG_BOOTMAPOFF       MCR_OTPCFG_BOOTMAPOFF_Msk                                   /*!< b=1 Boot addr remap closed forever flag  */

/*******************  Bit definition for MCR_MPUEN register  ********************/
#define MCR_MPUEN_MPU0EN_Pos    (0U)
#define MCR_MPUEN_MPU0EN_Msk    (0x1U << MCR_MPUEN_MPU0EN_Pos)                                  /*!< 0x0000000F */
#define MCR_MPUEN_MPU0EN        MCR_MPUEN_MPU0EN_Msk                                            /*!<Mpu register 0  configure enable */

#define MCR_MPUEN_MPU1EN_Pos    (4U)
#define MCR_MPUEN_MPU1EN_Msk    (0x1U << MCR_MPUEN_MPU1EN_Pos)                                  /*!< 0x000000F0 */
#define MCR_MPUEN_MPU1EN        MCR_MPUEN_MPU1EN_Msk                                            /*!<Mpu register 1 configure enable */

#define MCR_MPUEN_MPU2EN_Pos    (8U)
#define MCR_MPUEN_MPU2EN_Msk    (0x1U << MCR_MPUEN_MPU2EN_Pos)                                  /*!< 0x00000F00 */
#define MCR_MPUEN_MPU2EN        MCR_MPUEN_MPU2EN_Msk                                            /*!<Mpu register 2 configure enable */

#define MCR_MPUEN_MPU3EN_Pos    (12U)
#define MCR_MPUEN_MPU3EN_Msk    (0x1U << MCR_MPUEN_MPU3EN_Pos)                                  /*!< 0x0000F000 */
#define MCR_MPUEN_MPU3EN        MCR_MPUEN_MPU3EN_Msk                                            /*!<Mpu register 3 configure enable */

#define MCR_MPUEN_MPU4EN_Pos    (16U)
#define MCR_MPUEN_MPU4EN_Msk    (0x1U << MCR_MPUEN_MPU4EN_Pos)                                  /*!< 0x000F0000 */
#define MCR_MPUEN_MPU4EN        MCR_MPUEN_MPU4EN_Msk                                            /*!<Mpu register 4 configure enable */

#define MCR_MPUEN_MPU5EN_Pos    (20U)
#define MCR_MPUEN_MPU5EN_Msk    (0x1U << MCR_MPUEN_MPU5EN_Pos)                                  /*!< 0x00F00000 */
#define MCR_MPUEN_MPU5EN        MCR_MPUEN_MPU5EN_Msk                                            /*!<Mpu register 5 configure enable */

#define MCR_MPUEN_MPU6EN_Pos    (24U)
#define MCR_MPUEN_MPU6EN_Msk    (0x1U << MCR_MPUEN_MPU6EN_Pos)                                  /*!< 0x0F000000 */
#define MCR_MPUEN_MPU6EN        MCR_MPUEN_MPU6EN_Msk                                            /*!<Mpu register 6 configure enable */

#define MCR_MPUEN_MPU7EN_Pos    (7U)
#define MCR_MPUEN_MPU7EN_Msk    (0x1U << MCR_MPUEN_MPU7EN_Pos)                                  /*!< 0xF0000000 */
#define MCR_MPUEN_MPU7EN        MCR_MPUEN_MPU7EN_Msk                                            /*!<Mpu register 7 configure enable */

/*******************  Bit definition for MCR_MCFG_RFU0 register  ********************/
#define MCR_MCFG_RFU0_Pos   (0U)
#define MCR_MCFG_RFU0_Msk   (0xFFFFFFFFU << MCR_MCFG_RFU0_Pos)                                  /*!< 0xFFFFFFFF */
#define MCR_MCFG_RFU0       MCR_MCFG_RFU0_Msk                                                   /*!<MCR_MCFG_RFU0*/

/*******************  Bit definition for MCR_MCFG_RFU1 register  ********************/
#define MCR_MCFG_RFU1_Pos   (0U)
#define MCR_MCFG_RFU1_Msk   (0xFFFFFFFFU << MCR_MCFG_RFU1_Pos)                                  /*!< 0xFFFFFFFF */
#define MCR_MCFG_RFU1       MCR_MCFG_RFU1_Msk                                                   /*!<MCR_MCFG_RFU1*/

/*******************  Bit definition for MCR_UCFG_RFU0 register  ********************/
#define MCR_UCFG_RFU0_Pos   (0U)
#define MCR_UCFG_RFU0_Msk   (0xFFFFFFFFU << MCR_UCFG_RFU0_Pos)                                  /*!< 0xFFFFFFFF */
#define MCR_UCFG_RFU0       MCR_UCFG_RFU0_Msk                                                   /*!<MCR_UCFG_RFU0*/

/*******************  Bit definition for MCR_UCFG_RFU1 register  ********************/
#define MCR_UCFG_RFU1_Pos   (0U)
#define MCR_UCFG_RFU1_Msk   (0xFFFFFFFFU << MCR_UCFG_RFU1_Pos)                                  /*!< 0xFFFFFFFF */
#define MCR_UCFG_RFU1       MCR_UCFG_RFU1_Msk                                                   /*!<MCR_UCFG_RFU1*/

/*******************  Bit definition for MCR_MODE_EN_P register  ********************/
#define MCR_MODEENP_MODEENP_Pos (0U)
#define MCR_MODEENP_MODEENP_Msk (0xFFFFFFFFU << MCR_MODEENP_MODEENP_Pos)                        /*!< 0xFFFFFFFF */
#define MCR_MODEENP_MODEENP     MCR_MODEENP_MODEENP_Msk                                         /*!<=0x5AA5_AA55*/

/*******************  Bit definition for MCR_MODE_EN_N register  ********************/
#define MCR_MODEENN_MODEENN_Pos (0U)
#define MCR_MODEENN_MODEENN_Msk (0xFFFFFFFFU << MCR_MODEENN_MODEENN_Pos)                        /*!< 0xFFFFFFFF */
#define MCR_MODEENN_MODEENN     MCR_MODEENN_MODEENN_Msk                                         /*!<=0xA55A_55AA*/

/*******************  Bit definition for MODEWORDP register  ********************/
#define MCR_MODEWORDP_MODEWORDP_Pos (0U)
#define MCR_MODEWORDP_MODEWORDP_Msk (0xFFFFFFFFU << MCR_MODEWORDP_MODEWORDP_Pos)                /*!< 0xFFFFFFFF */
#define MCR_MODEWORDP_MODEWORDP     MCR_MODEWORDP_MODEWORDP_Msk                                 /*!< MODE_WORD_P */

/*******************  Bit definition for MODEWORDN register  ********************/
#define MCR_MODEWORDN_MODEWORDN_Pos (0U)
#define MCR_MODEWORDN_MODEWORDN_Msk (0xFFFFFFFFU << MCR_MODEWORDN_MODEWORDN_Pos)                /*!< 0xFFFFFFFF */
#define MCR_MODEWORDN_MODEWORDN     MCR_MODEWORDN_MODEWORDN_Msk                                 /*!<= MODE_WORD_N */
//</h>
//<h>MPU
/******************************************************************************/
/*                                                                            */
/*                                    MPU                                     */
/*                                                                            */
/******************************************************************************/
/*******************  Bit definition for MPU_CSR register  ********************/
#define MPU_CSR_EN_Pos  (0U)
#define MPU_CSR_EN_Msk  (0x1U << MPU_CSR_EN_Pos)                                /*!< 0x00000001 */
#define MPU_CSR_EN      MPU_CSR_EN_Msk                                          /*!<MPU enable */

#define MPU_CSR_RN_Pos  (4U)
#define MPU_CSR_RN_Msk  (0xFU << MPU_CSR_RN_Pos)                                /*!< 0x000000F0 */
#define MPU_CSR_RN      MPU_CSR_RN_Msk                                          /*!<RN[3:0](MPU region count) */
#define MPU_CSR_RN_0    (0x1U << MPU_CSR_RN_Pos)                                /*!< 0x00000010 */
#define MPU_CSR_RN_1    (0x2U << MPU_CSR_RN_Pos)                                /*!< 0x00000020 */
#define MPU_CSR_RN_2    (0x4U << MPU_CSR_RN_Pos)                                /*!< 0x00000040 */
#define MPU_CSR_RN_3    (0x8U << MPU_CSR_RN_Pos)                                /*!< 0x00000080 */

#define MPU_CSR_SN_Pos  (12U)
#define MPU_CSR_SN_Msk  (0x7U << MPU_CSR_SN_Pos)                                /*!< 0x00007000 */
#define MPU_CSR_SN      MPU_CSR_SN_Msk                                          /*!<SN[2:0](MPU Slave number) */
#define MPU_CSR_SN_0    (0x1U << MPU_CSR_SN_Pos)                                /*!< 0x00001000 */
#define MPU_CSR_SN_1    (0x2U << MPU_CSR_SN_Pos)                                /*!< 0x00002000 */
#define MPU_CSR_SN_2    (0x4U << MPU_CSR_SN_Pos)                                /*!< 0x00004000 */

#define MPU_CSR_PIDAE_Pos   (16U)
#define MPU_CSR_PIDAE_Msk   (0x1U << MPU_CSR_PIDAE_Pos)                         /*!< 0x00010000 */
#define MPU_CSR_PIDAE       MPU_CSR_PIDAE_Msk                                   /*!<MPU PID auto enable */

#define MPU_CSR_PID_Pos (20U)
#define MPU_CSR_PID_Msk (0x7U << MPU_CSR_PID_Pos)                               /*!< 0x00700000 */
#define MPU_CSR_PID     MPU_CSR_PID_Msk                                         /*!<PID[2:0](MPU process id) */
#define MPU_CSR_PID_0   (0x1U << MPU_CSR_PID_Pos)                               /*!< 0x00100000 */
#define MPU_CSR_PID_1   (0x2U << MPU_CSR_PID_Pos)                               /*!< 0x00200000 */
#define MPU_CSR_PID_2   (0x4U << MPU_CSR_PID_Pos)                               /*!< 0x00400000 */

#define MPU_CSR_SERR_Pos    (24U)
#define MPU_CSR_SERR_Msk    (0xFU << MPU_CSR_SERR_Pos)                          /*!< 0x0F000000 */
#define MPU_CSR_SERR        MPU_CSR_SERR_Msk                                    /*!<SERR[3:0](MPU slave error) */
#define MPU_CSR_SERR_0      (0x1U << MPU_CSR_SERR_Pos)                          /*!< 0x01000000 */
#define MPU_CSR_SERR_1      (0x2U << MPU_CSR_SERR_Pos)                          /*!< 0x02000000 */
#define MPU_CSR_SERR_2      (0x4U << MPU_CSR_SERR_Pos)                          /*!< 0x04000000 */
#define MPU_CSR_SERR_3      (0x8U << MPU_CSR_SERR_Pos)                          /*!< 0x08000000 */

/*******************  Bit definition for MPU_EAR register  ********************/
#define MPU_EAR_EADDR_Pos   (0U)
#define MPU_EAR_EADDR_Msk   (0xFFFFFFFFU << MPU_EAR_EADDR_Pos)
#define MPU_EAR_EADDR       MPU_EAR_EADDR_Msk                                   /*!<error address */

/* The count of MPU_EAR */
#define MPU_EAR_COUNT (4U)

/*******************  Bit definition for MPU_EDR register  ********************/
#define MPU_EDR_EWR_Pos (0U)
#define MPU_EDR_EWR_Msk (0x1U << MPU_EDR_EWR_Pos)                               /*!< 0x00000001 */
#define MPU_EDR_EWR     MPU_EDR_EWR_Msk                                         /*!<error write read */

#define MPU_EDR_ERD_Pos (1U)
#define MPU_EDR_ERD_Msk (0x1U << MPU_EDR_ERD_Pos)                               /*!< 0x00000002 */
#define MPU_EDR_ERD     MPU_EDR_ERD_Msk                                         /*!<error  read */

#define MPU_EDR_EEX_Pos (2U)
#define MPU_EDR_EEX_Msk (0x1U << MPU_EDR_EEX_Pos)                               /*!< 0x00000004 */
#define MPU_EDR_EEX     MPU_EDR_EEX_Msk                                         /*!<error execute */

#define MPU_EDR_EFW_Pos (3U)
#define MPU_EDR_EFW_Msk (0x1U << MPU_EDR_EFW_Pos)                               /*!< 0x00000008 */
#define MPU_EDR_EFW     MPU_EDR_EFW_Msk                                         /*!<error firewall */

#define MPU_EDR_EATTR_Pos   (4U)
#define MPU_EDR_EATTR_Msk   (0x1U << MPU_EDR_EATTR_Pos)                         /*!< 0x00000010 */
#define MPU_EDR_EATTR       MPU_EDR_EATTR_Msk                                   /*!<error attribute */

#define MPU_EDR_EMN_Pos (8U)
#define MPU_EDR_EMN_Msk (0x7U << MPU_EDR_EMN_Pos)                               /*!< 0x00000700 */
#define MPU_EDR_EMN     MPU_EDR_EMN_Msk                                         /*!<error master number */
#define MPU_EDR_EMN_0   (0x1U << MPU_EDR_EMN_Pos)                               /*!< 0x00000100 */
#define MPU_EDR_EMN_1   (0x2U << MPU_EDR_EMN_Pos)                               /*!< 0x00000200 */
#define MPU_EDR_EMN_2   (0x4U << MPU_EDR_EMN_Pos)                               /*!< 0x00000400 */

#define MPU_EDR_EPID_Pos    (12U)
#define MPU_EDR_EPID_Msk    (0x7U << MPU_EDR_EPID_Pos)                          /*!< 0x00007000 */
#define MPU_EDR_EPID        MPU_EDR_EPID_Msk                                    /*!<error process ID */
#define MPU_EDR_EPID_0      (0x1U << MPU_EDR_EPID_Pos)                          /*!< 0x00001000 */
#define MPU_EDR_EPID_1      (0x2U << MPU_EDR_EPID_Pos)                          /*!< 0x00002000 */
#define MPU_EDR_EPID_2      (0x4U << MPU_EDR_EPID_Pos)                          /*!< 0x00004000 */

#define MPU_EDR_ERN_Pos (16U)
#define MPU_EDR_ERN_Msk (0xFFU << MPU_EDR_ERN_Pos)                              /*!< 0x000FF0000 */
#define MPU_EDR_ERN     MPU_EDR_ERN_Msk                                         /*!<error region descriptor number */

/* The count of MPU_EAR */
#define MPU_EDR_COUNT (4U)

/*******************  Bit definition for MPU_SADDR register  ********************/
#define MPU_SADDR_SADDR_Pos (10U)
#define MPU_SADDR_SADDR_Msk (0x3FFFFFU << MPU_SADDR_SADDR_Pos)
#define MPU_SADDR_SADDR     MPU_SADDR_SADDR_Msk                                /*!<start address */

/* The count of MPU_SADDR */
#define MPU_SADDR_COUNT (8U)

/*******************  Bit definition for MPU_EADDR register  ********************/
#define MPU_EADDR_EADDR_Pos (10U)
#define MPU_EADDR_EADDR_Msk (0x3FFFFFU << MPU_EADDR_EADDR_Pos)
#define MPU_EADDR_EADDR     MPU_EADDR_EADDR_Msk                                 /*!<end address */

/* The count of MPU_EADDR */
#define MPU_EADDR_COUNT (8U)

/*******************  Bit definition for MPU_AUTHA register  ********************/
#define MPU_AUTHA_DSPWA_Pos (0U)
#define MPU_AUTHA_DSPWA_Msk (0x1U << MPU_AUTHA_DSPWA_Pos)                       /*!< 0x00000001 */
#define MPU_AUTHA_DSPWA     MPU_AUTHA_DSPWA_Msk                                 /*!<dsp write authority */

#define MPU_AUTHA_DSPRA_Pos (1U)
#define MPU_AUTHA_DSPRA_Msk (0x1U << MPU_AUTHA_DSPRA_Pos)                       /*!< 0x00000002 */
#define MPU_AUTHA_DSPRA     MPU_AUTHA_DSPRA_Msk                                 /*!<dsp read authority */

#define MPU_AUTHA_DMAWA_Pos (4U)
#define MPU_AUTHA_DMAWA_Msk (0x1U << MPU_AUTHA_DMAWA_Pos)                       /*!< 0x00000010 */
#define MPU_AUTHA_DMAWA     MPU_AUTHA_DMAWA_Msk                                 /*!<dma write authority */

#define MPU_AUTHA_DMARA_Pos (5U)
#define MPU_AUTHA_DMARA_Msk (0x1U << MPU_AUTHA_DMARA_Pos)                       /*!< 0x00000020 */
#define MPU_AUTHA_DMARA     MPU_AUTHA_DMARA_Msk                                 /*!<dma read authority */

/* The count of MPU_AUTHA */
#define MPU_AUTHA_COUNT (8U)

/*******************  Bit definition for MPU_AUTHB register  ********************/
#define MPU_AUTHB_R0UPX_Pos (0U)
#define MPU_AUTHB_R0UPX_Msk (0x1U << MPU_AUTHB_R0UPX_Pos)                       /*!< 0x00000001 */
#define MPU_AUTHB_R0UPX     MPU_AUTHB_R0UPX_Msk                                 /*!<region 0 unprivilege mode excute authority */
#define MPU_AUTHB_R0UPW_Pos (1U)
#define MPU_AUTHB_R0UPW_Msk (0x1U << MPU_AUTHB_R0UPW_Pos)                       /*!< 0x00000002 */
#define MPU_AUTHB_R0UPW     MPU_AUTHB_R0UPW_Msk                                 /*!<region 0 unprivilege mode write authority */
#define MPU_AUTHB_R0UPR_Pos (2U)
#define MPU_AUTHB_R0UPR_Msk (0x1U << MPU_AUTHB_R0UPR_Pos)                       /*!< 0x00000004 */
#define MPU_AUTHB_R0UPR     MPU_AUTHB_R0UPR_Msk                                 /*!<region 0 unprivilege mode read authority */

#define MPU_AUTHB_R0PX_Pos  (4U)
#define MPU_AUTHB_R0PX_Msk  (0x1U << MPU_AUTHB_R0PX_Pos)                        /*!< 0x00000010 */
#define MPU_AUTHB_R0PX      MPU_AUTHB_R0PX_Msk                                  /*!<region 0 privilege mode excute authority */
#define MPU_AUTHB_R0PW_Pos  (5U)
#define MPU_AUTHB_R0PW_Msk  (0x1U << MPU_AUTHB_R0PW_Pos)                        /*!< 0x00000020 */
#define MPU_AUTHB_R0PW      MPU_AUTHB_R0PW_Msk                                  /*!<region 0 privilege mode write authority */
#define MPU_AUTHB_R0PR_Pos  (6U)
#define MPU_AUTHB_R0PR_Msk  (0x1U << MPU_AUTHB_R0PR_Pos)                        /*!< 0x00000040 */
#define MPU_AUTHB_R0PR      MPU_AUTHB_R0PR_Msk                                  /*!<region 0 privilege mode read authority */

#define MPU_AUTHB_R1UPX_Pos (8U)
#define MPU_AUTHB_R1UPX_Msk (0x1U << MPU_AUTHB_R1UPX_Pos)                       /*!< 0x00000100 */
#define MPU_AUTHB_R1UPX     MPU_AUTHB_R1UPX_Msk                                 /*!<region 1 unprivilege mode excute authority */
#define MPU_AUTHB_R1UPW_Pos (9U)
#define MPU_AUTHB_R1UPW_Msk (0x1U << MPU_AUTHB_R1UPW_Pos)                       /*!< 0x00000200 */
#define MPU_AUTHB_R1UPW     MPU_AUTHB_R1UPW_Msk                                 /*!<region 1 unprivilege mode write authority */
#define MPU_AUTHB_R1UPR_Pos (10U)
#define MPU_AUTHB_R1UPR_Msk (0x1U << MPU_AUTHB_R1UPR_Pos)                       /*!< 0x00000400 */
#define MPU_AUTHB_R1UPR     MPU_AUTHB_R1UPR_Msk                                 /*!<region 1 unprivilege mode read authority */

#define MPU_AUTHB_R1PX_Pos  (12U)
#define MPU_AUTHB_R1PX_Msk  (0x1U << MPU_AUTHB_R1PX_Pos)                        /*!< 0x00001000 */
#define MPU_AUTHB_R1PX      MPU_AUTHB_R1PX_Msk                                  /*!<region 1 privilege mode excute authority */
#define MPU_AUTHB_R1PW_Pos  (13U)
#define MPU_AUTHB_R1PW_Msk  (0x1U << MPU_AUTHB_R1PW_Pos)                        /*!< 0x00002000 */
#define MPU_AUTHB_R1PW      MPU_AUTHB_R1PW_Msk                                  /*!<region 1 privilege mode write authority */
#define MPU_AUTHB_R1PR_Pos  (14U)
#define MPU_AUTHB_R1PR_Msk  (0x1U << MPU_AUTHB_R1PR_Pos)                        /*!< 0x00004000 */
#define MPU_AUTHB_R1PR      MPU_AUTHB_R1PR_Msk                                  /*!<region 1 privilege mode read authority */

#define MPU_AUTHB_R2UPX_Pos (16U)
#define MPU_AUTHB_R2UPX_Msk (0x1U << MPU_AUTHB_R2UPX_Pos)                       /*!< 0x00010000 */
#define MPU_AUTHB_R2UPX     MPU_AUTHB_R2UPX_Msk                                 /*!<region 2 unprivilege mode excute authority */
#define MPU_AUTHB_R2UPW_Pos (17U)
#define MPU_AUTHB_R2UPW_Msk (0x1U << MPU_AUTHB_R2UPW_Pos)                       /*!< 0x00020000 */
#define MPU_AUTHB_R2UPW     MPU_AUTHB_R2UPW_Msk                                 /*!<region 2 unprivilege mode write authority */
#define MPU_AUTHB_R2UPR_Pos (18U)
#define MPU_AUTHB_R2UPR_Msk (0x1U << MPU_AUTHB_R2UPR_Pos)                       /*!< 0x00040000 */
#define MPU_AUTHB_R2UPR     MPU_AUTHB_R2UPR_Msk                                 /*!<region 2 unprivilege mode read authority */

#define MPU_AUTHB_R2PX_Pos  (20U)
#define MPU_AUTHB_R2PX_Msk  (0x1U << MPU_AUTHB_R2PX_Pos)                        /*!< 0x00100000 */
#define MPU_AUTHB_R2PX      MPU_AUTHB_R2PX_Msk                                  /*!<region 2 privilege mode excute authority */
#define MPU_AUTHB_R2PW_Pos  (21U)
#define MPU_AUTHB_R2PW_Msk  (0x1U << MPU_AUTHB_R2PW_Pos)                        /*!< 0x00200000 */
#define MPU_AUTHB_R2PW      MPU_AUTHB_R2PW_Msk                                  /*!<region 2 privilege mode write authority */
#define MPU_AUTHB_R2PR_Pos  (22U)
#define MPU_AUTHB_R2PR_Msk  (0x1U << MPU_AUTHB_R2PR_Pos)                        /*!< 0x00400000 */
#define MPU_AUTHB_R2PR      MPU_AUTHB_R2PR_Msk                                  /*!<region 2 privilege mode read authority */

#define MPU_AUTHB_R3UPX_Pos (24U)
#define MPU_AUTHB_R3UPX_Msk (0x1U << MPU_AUTHB_R3UPX_Pos)                       /*!< 0x01000000 */
#define MPU_AUTHB_R3UPX     MPU_AUTHB_R3UPX_Msk                                 /*!<region 3 unprivilege mode excute authority */
#define MPU_AUTHB_R3UPW_Pos (25U)
#define MPU_AUTHB_R3UPW_Msk (0x1U << MPU_AUTHB_R3UPW_Pos)                       /*!< 0x02000000 */
#define MPU_AUTHB_R3UPW     MPU_AUTHB_R3UPW_Msk                                 /*!<region 3 unprivilege mode write authority */
#define MPU_AUTHB_R3UPR_Pos (26U)
#define MPU_AUTHB_R3UPR_Msk (0x1U << MPU_AUTHB_R3UPR_Pos)                       /*!< 0x04000000 */
#define MPU_AUTHB_R3UPR     MPU_AUTHB_R3UPR_Msk                                 /*!<region 2 unprivilege mode read authority */

#define MPU_AUTHB_R3PX_Pos  (28U)
#define MPU_AUTHB_R3PX_Msk  (0x1U << MPU_AUTHB_R3PX_Pos)                        /*!< 0x10000000 */
#define MPU_AUTHB_R3PX      MPU_AUTHB_R3PX_Msk                                  /*!<region 3 privilege mode excute authority */
#define MPU_AUTHB_R3PW_Pos  (29U)
#define MPU_AUTHB_R3PW_Msk  (0x1U << MPU_AUTHB_R3PW_Pos)                        /*!< 0x20000000 */
#define MPU_AUTHB_R3PW      MPU_AUTHB_R3PW_Msk                                  /*!<region 3 privilege mode write authority */
#define MPU_AUTHB_R3PR_Pos  (30U)
#define MPU_AUTHB_R3PR_Msk  (0x1U << MPU_AUTHB_R3PR_Pos)                        /*!< 0x40000000 */
#define MPU_AUTHB_R3PR      MPU_AUTHB_R3PR_Msk                                  /*!<region 3 privilege mode read authority */

/* The count of MPU_AUTHB */
#define MPU_AUTHB_COUNT (8U)

/*******************  Bit definition for MPU_AUTHC register  ********************/
#define MPU_AUTHC_R4UPX_Pos (0U)
#define MPU_AUTHC_R4UPX_Msk (0x1U << MPU_AUTHC_R4UPX_Pos)                       /*!< 0x00000001 */
#define MPU_AUTHC_R4UPX     MPU_AUTHC_R4UPX_Msk                                 /*!<region 4 unprivilege mode excute authority */
#define MPU_AUTHC_R4UPW_Pos (1U)
#define MPU_AUTHC_R4UPW_Msk (0x1U << MPU_AUTHC_R4UPW_Pos)                       /*!< 0x00000002 */
#define MPU_AUTHC_R4UPW     MPU_AUTHC_R4UPW_Msk                                 /*!<region 4 unprivilege mode write authority */
#define MPU_AUTHC_R4UPR_Pos (2U)
#define MPU_AUTHC_R4UPR_Msk (0x1U << MPU_AUTHC_R4UPR_Pos)                       /*!< 0x00000004 */
#define MPU_AUTHC_R4UPR     MPU_AUTHC_R4UPR_Msk                                 /*!<region 4 unprivilege mode read authority */

#define MPU_AUTHC_R4PX_Pos  (4U)
#define MPU_AUTHC_R4PX_Msk  (0x1U << MPU_AUTHC_R4PX_Pos)                        /*!< 0x00000010 */
#define MPU_AUTHC_R4PX      MPU_AUTHC_R4PX_Msk                                  /*!<region 4 privilege mode excute authority */
#define MPU_AUTHC_R4PW_Pos  (5U)
#define MPU_AUTHC_R4PW_Msk  (0x1U << MPU_AUTHC_R4PW_Pos)                        /*!< 0x00000020 */
#define MPU_AUTHC_R4PW      MPU_AUTHC_R4PW_Msk                                  /*!<region 4 privilege mode write authority */
#define MPU_AUTHC_R4PR_Pos  (6U)
#define MPU_AUTHC_R4PR_Msk  (0x1U << MPU_AUTHC_R4PR_Pos)                        /*!< 0x00000040 */
#define MPU_AUTHC_R4PR      MPU_AUTHC_R4PR_Msk                                  /*!<region 4 privilege mode read authority */

#define MPU_AUTHC_R5UPX_Pos (8U)
#define MPU_AUTHC_R5UPX_Msk (0x1U << MPU_AUTHC_R5UPX_Pos)                       /*!< 0x00000100 */
#define MPU_AUTHC_R5UPX     MPU_AUTHC_R5UPX_Msk                                 /*!<region 5 unprivilege mode excute authority */
#define MPU_AUTHC_R5UPW_Pos (9U)
#define MPU_AUTHC_R5UPW_Msk (0x1U << MPU_AUTHC_R5UPW_Pos)                       /*!< 0x00000200 */
#define MPU_AUTHC_R5UPW     MPU_AUTHC_R5UPW_Msk                                 /*!<region 5 unprivilege mode write authority */
#define MPU_AUTHC_R5UPR_Pos (10U)
#define MPU_AUTHC_R5UPR_Msk (0x1U << MPU_AUTHC_R5UPR_Pos)                       /*!< 0x00000400 */
#define MPU_AUTHC_R5UPR     MPU_AUTHC_R5UPR_Msk                                 /*!<region 5 unprivilege mode read authority */

#define MPU_AUTHC_R5PX_Pos  (12U)
#define MPU_AUTHC_R5PX_Msk  (0x1U << MPU_AUTHC_R5PX_Pos)                        /*!< 0x00001000 */
#define MPU_AUTHC_R5PX      MPU_AUTHC_R5PX_Msk                                  /*!<region 5 privilege mode excute authority */
#define MPU_AUTHC_R5PW_Pos  (13U)
#define MPU_AUTHC_R5PW_Msk  (0x1U << MPU_AUTHC_R5PW_Pos)                        /*!< 0x00002000 */
#define MPU_AUTHC_R5PW      MPU_AUTHC_R5PW_Msk                                  /*!<region 5 privilege mode write authority */
#define MPU_AUTHC_R5PR_Pos  (14U)
#define MPU_AUTHC_R5PR_Msk  (0x1U << MPU_AUTHC_R5PR_Pos)                        /*!< 0x00004000 */
#define MPU_AUTHC_R5PR      MPU_AUTHC_R5PR_Msk                                  /*!<region 5 privilege mode read authority */

#define MPU_AUTHC_R6UPX_Pos (16U)
#define MPU_AUTHC_R6UPX_Msk (0x1U << MPU_AUTHC_R6UPX_Pos)                       /*!< 0x00010000 */
#define MPU_AUTHC_R6UPX     MPU_AUTHC_R6UPX_Msk                                 /*!<region 6 unprivilege mode excute authority */
#define MPU_AUTHC_R6UPW_Pos (17U)
#define MPU_AUTHC_R6UPW_Msk (0x1U << MPU_AUTHC_R6UPW_Pos)                       /*!< 0x00020000 */
#define MPU_AUTHC_R6UPW     MPU_AUTHC_R6UPW_Msk                                 /*!<region 6 unprivilege mode write authority */
#define MPU_AUTHC_R6UPR_Pos (18U)
#define MPU_AUTHC_R6UPR_Msk (0x1U << MPU_AUTHC_R6UPR_Pos)                       /*!< 0x00040000 */
#define MPU_AUTHC_R6UPR     MPU_AUTHC_R6UPR_Msk                                 /*!<region 6 unprivilege mode read authority */

#define MPU_AUTHC_R6PX_Pos  (20U)
#define MPU_AUTHC_R6PX_Msk  (0x1U << MPU_AUTHC_R6PX_Pos)                        /*!< 0x00100000 */
#define MPU_AUTHC_R6PX      MPU_AUTHC_R6PX_Msk                                  /*!<region 6 privilege mode excute authority */
#define MPU_AUTHC_R6PW_Pos  (21U)
#define MPU_AUTHC_R6PW_Msk  (0x1U << MPU_AUTHC_R6PW_Pos)                        /*!< 0x00200000 */
#define MPU_AUTHC_R6PW      MPU_AUTHC_R6PW_Msk                                  /*!<region 6 privilege mode write authority */
#define MPU_AUTHC_R6PR_Pos  (22U)
#define MPU_AUTHC_R6PR_Msk  (0x1U << MPU_AUTHC_R6PR_Pos)                        /*!< 0x00400000 */
#define MPU_AUTHC_R6PR      MPU_AUTHC_R6PR_Msk                                  /*!<region 6 privilege mode read authority */

#define MPU_AUTHC_R7UPX_Pos (24U)
#define MPU_AUTHC_R7UPX_Msk (0x1U << MPU_AUTHC_R7UPX_Pos)                       /*!< 0x01000000 */
#define MPU_AUTHC_R7UPX     MPU_AUTHC_R7UPX_Msk                                 /*!<region 7 unprivilege mode excute authority */
#define MPU_AUTHC_R7UPW_Pos (25U)
#define MPU_AUTHC_R7UPW_Msk (0x1U << MPU_AUTHC_R7UPW_Pos)                       /*!< 0x02000000 */
#define MPU_AUTHC_R7UPW     MPU_AUTHC_R7UPW_Msk                                 /*!<region 7 unprivilege mode write authority */
#define MPU_AUTHC_R7UPR_Pos (26U)
#define MPU_AUTHC_R7UPR_Msk (0x1U << MPU_AUTHC_R7UPR_Pos)                       /*!< 0x04000000 */
#define MPU_AUTHC_R7UPR     MPU_AUTHC_R7UPR_Msk                                 /*!<region 7 unprivilege mode read authority */

#define MPU_AUTHC_R7PX_Pos  (28U)
#define MPU_AUTHC_R7PX_Msk  (0x1U << MPU_AUTHC_R7PX_Pos)                        /*!< 0x10000000 */
#define MPU_AUTHC_R7PX      MPU_AUTHC_R7PX_Msk                                  /*!<region 7 privilege mode excute authority */
#define MPU_AUTHC_R7PW_Pos  (29U)
#define MPU_AUTHC_R7PW_Msk  (0x1U << MPU_AUTHC_R7PW_Pos)                        /*!< 0x20000000 */
#define MPU_AUTHC_R7PW      MPU_AUTHC_R7PW_Msk                                  /*!<region 7 privilege mode write authority */
#define MPU_AUTHC_R7PR_Pos  (30U)
#define MPU_AUTHC_R7PR_Msk  (0x1U << MPU_AUTHC_R7PR_Pos)                        /*!< 0x40000000 */
#define MPU_AUTHC_R7PR      MPU_AUTHC_R7PR_Msk                                  /*!<region 7 privilege mode read authority */

/* The count of MPU_AUTHC */
#define MPU_AUTHC_COUNT (8U)

/*******************  Bit definition for MPU_CTRL register  ********************/
#define MPU_CTRL_REGIONE_Pos    (0U)
#define MPU_CTRL_REGIONE_Msk    (0x1U << MPU_CTRL_REGIONE_Pos)                  /*!< 0x00000001 */
#define MPU_CTRL_REGIONE        MPU_CTRL_REGIONE_Msk                            /*!<region enable */

#define MPU_CTRL_GATEE_Pos  (1U)
#define MPU_CTRL_GATEE_Msk  (0x1U << MPU_CTRL_GATEE_Pos)                        /*!< 0x00000002 */
#define MPU_CTRL_GATEE      MPU_CTRL_GATEE_Msk                                  /*!<firewall enable */

#define MPU_CTRL_LOCK_Pos   (4U)
#define MPU_CTRL_LOCK_Msk   (0x1U << MPU_CTRL_LOCK_Pos)                         /*!< 0x00000010 */
#define MPU_CTRL_LOCK       MPU_CTRL_LOCK_Msk                                   /*!<lock bit */

#define MPU_CTRL_INTE_Pos   (8U)
#define MPU_CTRL_INTE_Msk   (0x1U << MPU_CTRL_INTE_Pos)                         /*!< 0x00000100 */
#define MPU_CTRL_INTE       MPU_CTRL_INTE_Msk                                   /*!<interrupt jumpout firewall enable */

#define MPU_CTRL_AJUMPE_Pos (9U)
#define MPU_CTRL_AJUMPE_Msk (0x1U << MPU_CTRL_AJUMPE_Pos)                       /*!< 0x00000200 */
#define MPU_CTRL_AJUMPE     MPU_CTRL_AJUMPE_Msk                                 /*!<program jumpout firewall enable */

#define MPU_CTRL_MC0RWA_Pos (16U)
#define MPU_CTRL_MC0RWA_Msk (0x1U << MPU_CTRL_MC0RWA_Pos)                       /*!< 0x00010000 */
#define MPU_CTRL_MC0RWA     MPU_CTRL_MC0RWA_Msk                                 /*!<region 0 authority of write register */

#define MPU_CTRL_MC1RWA_Pos (17U)
#define MPU_CTRL_MC1RWA_Msk (0x1U << MPU_CTRL_MC1RWA_Pos)                       /*!< 0x00020000 */
#define MPU_CTRL_MC1RWA     MPU_CTRL_MC1RWA_Msk                                 /*!<region 1 authority of write register */

#define MPU_CTRL_MC2RWA_Pos (18U)
#define MPU_CTRL_MC2RWA_Msk (0x1U << MPU_CTRL_MC2RWA_Pos)                       /*!< 0x00040000 */
#define MPU_CTRL_MC2RWA     MPU_CTRL_MC2RWA_Msk                                 /*!<region 2 authority of write register */

#define MPU_CTRL_MC3RWA_Pos (19U)
#define MPU_CTRL_MC3RWA_Msk (0x1U << MPU_CTRL_MC3RWA_Pos)                       /*!< 0x00080000 */
#define MPU_CTRL_MC3RWA     MPU_CTRL_MC3RWA_Msk                                 /*!<region 3 authority of write register */

#define MPU_CTRL_MC4RWA_Pos (20U)
#define MPU_CTRL_MC4RWA_Msk (0x1U << MPU_CTRL_MC4RWA_Pos)                       /*!< 0x00100000 */
#define MPU_CTRL_MC4RWA     MPU_CTRL_MC4RWA_Msk                                 /*!<region 4 authority of write register */

#define MPU_CTRL_MC5RWA_Pos (21U)
#define MPU_CTRL_MC5RWA_Msk (0x1U << MPU_CTRL_MC5RWA_Pos)                       /*!< 0x00200000 */
#define MPU_CTRL_MC5RWA     MPU_CTRL_MC5RWA_Msk                                 /*!<region 5 authority of write register */

#define MPU_CTRL_MC6RWA_Pos (22U)
#define MPU_CTRL_MC6RWA_Msk (0x1U << MPU_CTRL_MC6RWA_Pos)                       /*!< 0x00400000 */
#define MPU_CTRL_MC6RWA     MPU_CTRL_MC6RWA_Msk                                 /*!<region 6 authority of write register */

#define MPU_CTRL_MC7RWA_Pos (23U)
#define MPU_CTRL_MC7RWA_Msk (0x1U << MPU_CTRL_MC7RWA_Pos)                       /*!< 0x00800000 */
#define MPU_CTRL_MC7RWA     MPU_CTRL_MC7RWA_Msk                                 /*!<region 7 authority of write register */

/* The count of MPU_CTRL */
#define MPU_CTRL_COUNT (8U)

/*******************  Bit definition for MPU_SWCTRL register  ********************/
#define MPU_SWCTRL_SJUMPE_Pos   (0U)
#define MPU_SWCTRL_SJUMPE_Msk   (0x1U << MPU_SWCTRL_SJUMPE_Pos)                 /*!< 0x00000001 */
#define MPU_SWCTRL_SJUMPE       MPU_SWCTRL_SJUMPE_Msk                           /*!<soft jumpout enable */

/* The count of MPU_CTRL */
#define MPU_SWCTRL_COUNT (8U)
//</h>
//<h>QSPI
/******************************************************************************/
/*                                                                            */
/*                                    QSPI                                    */
/*                                                                            */
/******************************************************************************/
/*******************  Bit definition for QSPI_CFG register  ********************/
#define QSPI_CFG_EN_Pos (0U)
#define QSPI_CFG_EN_Msk (0x1U << QSPI_CFG_EN_Pos)                                   /*!< 0x00000001 */
#define QSPI_CFG_EN     QSPI_CFG_EN_Msk                                             /*!<quadspi_en */

#define QSPI_CFG_ACCESSA_Pos    (1U)
#define QSPI_CFG_ACCESSA_Msk    (0x1U << QSPI_CFG_ACCESSA_Pos)                      /*!< 0x00000002 */
#define QSPI_CFG_ACCESSA        QSPI_CFG_ACCESSA_Msk                                /*!<access_a_reg */

#define QSPI_CFG_ACCESSB_Pos    (2U)
#define QSPI_CFG_ACCESSB_Msk    (0x1U << QSPI_CFG_ACCESSB_Pos)                      /*!< 0x00000004 */
#define QSPI_CFG_ACCESSB        QSPI_CFG_ACCESSB_Msk                                /*!<access_b_reg */

#define QSPI_CFG_TXSTARTREG_Pos (4U)
#define QSPI_CFG_TXSTARTREG_Msk (0x1U << QSPI_CFG_TXSTARTREG_Pos)                   /*!< 0x00000010 */
#define QSPI_CFG_TXSTARTREG     QSPI_CFG_TXSTARTREG_Msk                             /*!<tx_start_reg */

#define QSPI_CFG_DDREN_Pos  (5U)
#define QSPI_CFG_DDREN_Msk  (0x1U << QSPI_CFG_DDREN_Pos)                            /*!< 0x00000020 */
#define QSPI_CFG_DDREN      QSPI_CFG_DDREN_Msk                                      /*!<ddren */

#define QSPI_CFG_S2DLEN_Pos (6U)
#define QSPI_CFG_S2DLEN_Msk (0x1U << QSPI_CFG_S2DLEN_Pos)                           /*!< 0x00000040 */
#define QSPI_CFG_S2DLEN     QSPI_CFG_S2DLEN_Msk                                     /*!<single_2dl_en */

/*******************  Bit definition for QSPI_DORDER register  ********************/
#define QSPI_DORDER_RXFIFOMMAP_Pos  (2U)
#define QSPI_DORDER_RXFIFOMMAP_Msk  (0x1U << QSPI_DORDER_RXFIFOMMAP_Pos)            /*!< 0x00000004 */
#define QSPI_DORDER_RXFIFOMMAP      QSPI_DORDER_RXFIFOMMAP_Msk                      /*!<rxfifo_ahb_data_order */

#define QSPI_DORDER_LITENDIAN_Pos   (3U)
#define QSPI_DORDER_LITENDIAN_Msk   (0x1U << QSPI_DORDER_LITENDIAN_Pos)             /*!< 0x00000008 */
#define QSPI_DORDER_LITENDIAN       QSPI_DORDER_LITENDIAN_Msk                       /*!<little_endian */

/*******************  Bit definition for QSPI_MODE register  ********************/
#define QSPI_MODE_STROBEMEN_Pos (0U)
#define QSPI_MODE_STROBEMEN_Msk (0x1U << QSPI_MODE_STROBEMEN_Pos)                   /*!< 0x00000001 */
#define QSPI_MODE_STROBEMEN     QSPI_MODE_STROBEMEN_Msk                             /*!<strobe_mode_en */

#define QSPI_MODE_MMAPMEN_Pos   (1U)
#define QSPI_MODE_MMAPMEN_Msk   (0x1U << QSPI_MODE_MMAPMEN_Pos)                     /*!< 0x00000002 */
#define QSPI_MODE_MMAPMEN       QSPI_MODE_MMAPMEN_Msk                               /*!<ahb_mode_en */

#define QSPI_MODE_IOMODE_Pos    (3U)
#define QSPI_MODE_IOMODE_Msk    (0x1U << QSPI_MODE_IOMODE_Pos)                      /*!< 0x00000008 */
#define QSPI_MODE_IOMODE        QSPI_MODE_IOMODE_Msk                                /*!<io_mode */

/*******************  Bit definition for QSPI_IOMODE register  ********************/
#define QSPI_IOMODE_CMDIOM_Pos  (0U)
#define QSPI_IOMODE_CMDIOM_Msk  (0x3U << QSPI_IOMODE_CMDIOM_Pos)                    /*!< 0x00000003 */
#define QSPI_IOMODE_CMDIOM      QSPI_IOMODE_CMDIOM_Msk                              /*!<cmdio_mode[1:0] */

#define QSPI_IOMODE_ADDRIOM_Pos (2U)
#define QSPI_IOMODE_ADDRIOM_Msk (0x3U << QSPI_IOMODE_ADDRIOM_Pos)                   /*!< 0x0000000C */
#define QSPI_IOMODE_ADDRIOM     QSPI_IOMODE_ADDRIOM_Msk                             /*!<addrio_mode[1:0] */

#define QSPI_IOMODE_MODEIOM_Pos (4U)
#define QSPI_IOMODE_MODEIOM_Msk (0x3U << QSPI_IOMODE_MODEIOM_Pos)                   /*!< 0x00000030 */
#define QSPI_IOMODE_MODEIOM     QSPI_IOMODE_MODEIOM_Msk                             /*!<modeio_mode[1:0] */

#define QSPI_IOMODE_DATAIOM_Pos (6U)
#define QSPI_IOMODE_DATAIOM_Msk (0x3U << QSPI_IOMODE_DATAIOM_Pos)                   /*!< 0x000000C0 */
#define QSPI_IOMODE_DATAIOM     QSPI_IOMODE_DATAIOM_Msk                             /*!<dataio_mode[1:0] */

#define QSPI_IOMODE_DUMMYCYC_Pos    (8U)
#define QSPI_IOMODE_DUMMYCYC_Msk    (0x3U << QSPI_IOMODE_DUMMYCYC_Pos)              /*!< 0x00000300 */
#define QSPI_IOMODE_DUMMYCYC        QSPI_IOMODE_DUMMYCYC_Msk                        /*!<dummy_cycle[1:0] */

#define QSPI_IOMODE_ADDRSIZE_Pos    (13U)
#define QSPI_IOMODE_ADDRSIZE_Msk    (0x3U << QSPI_IOMODE_ADDRSIZE_Pos)              /*!< 0x00006000 */
#define QSPI_IOMODE_ADDRSIZE        QSPI_IOMODE_ADDRSIZE_Msk                        /*!<addr_size[1:0] */

#define QSPI_IOMODE_MODESIZE_Pos    (15U)
#define QSPI_IOMODE_MODESIZE_Msk    (0x3U << QSPI_IOMODE_MODESIZE_Pos)              /*!< 0x00018000 */
#define QSPI_IOMODE_MODESIZE        QSPI_IOMODE_MODESIZE_Msk                        /*!<mode_size[1:0] */

/*******************  Bit definition for QSPI_SCNTTHR register  ********************/
#define QSPI_SCNTTHR_STALL_Pos  (0U)
#define QSPI_SCNTTHR_STALL_Msk  (0xFFU << QSPI_SCNTTHR_STALL_Pos)                   /*!< 0x000000FF */
#define QSPI_SCNTTHR_STALL      QSPI_SCNTTHR_STALL_Msk                              /*!<stall_cnt_thresh[7:0] */

/*******************  Bit definition for QSPI_DLEN register  ********************/
#define QSPI_DLEN_DATALENREG_Pos    (0U)
#define QSPI_DLEN_DATALENREG_Msk    (0xFFU << QSPI_DLEN_DATALENREG_Pos)             /*!< 0x000000FF */
#define QSPI_DLEN_DATALENREG        QSPI_DLEN_DATALENREG_Msk                        /*!<data_len_reg[7:0] */

/*******************  Bit definition for QSPI_CMD register  ********************/
#define QSPI_CMD_INSTRUCT_Pos   (0U)
#define QSPI_CMD_INSTRUCT_Msk   (0xFFU << QSPI_CMD_INSTRUCT_Pos)                    /*!< 0x000000FF */
#define QSPI_CMD_INSTRUCT       QSPI_CMD_INSTRUCT_Msk                               /*!<instruct_cmd[7:0] */

#define QSPI_CMD_RDNWRCMDREG_Pos    (8U)
#define QSPI_CMD_RDNWRCMDREG_Msk    (0x1U << QSPI_CMD_RDNWRCMDREG_Pos)              /*!< 0x00000100 */
#define QSPI_CMD_RDNWRCMDREG        QSPI_CMD_RDNWRCMDREG_Msk                        /*!<rdn_wr_cmd_reg */

/*******************  Bit definition for QSPI_FADDR register  ********************/
#define QSPI_FADDR_FLASHADDR_Pos    (0U)
#define QSPI_FADDR_FLASHADDR_Msk    (0xFFFFFFFFU << QSPI_FADDR_FLASHADDR_Pos)       /*!< 0xFFFFFFFF */
#define QSPI_FADDR_FLASHADDR        QSPI_FADDR_FLASHADDR_Msk                        /*!<flash_addr_reg[31:0] */

/*******************  Bit definition for QSPI_MODEALT register  ********************/
#define QSPI_MODEALT_MODEALT_Pos    (0U)
#define QSPI_MODEALT_MODEALT_Msk    (0xFFFFFFFFU << QSPI_MODEALT_MODEALT_Pos)       /*!< 0xFFFFFFFF */
#define QSPI_MODEALT_MODEALT        QSPI_MODEALT_MODEALT_Msk                        /*!<mode_alt[31:0] */

/*******************  Bit definition for QSPI_RXFIFOADDR register  ********************/
#define QSPI_RXFIFOADDR_DOUT_Pos    (0U)
#define QSPI_RXFIFOADDR_DOUT_Msk    (0xFFU << QSPI_RXFIFOADDR_DOUT_Pos)             /*!< 0x000000FF */
#define QSPI_RXFIFOADDR_DOUT        QSPI_RXFIFOADDR_DOUT_Msk                        /*!<rxfifo_dout[7:0] */

/*******************  Bit definition for QSPI_RXFIFOCFG register  ********************/
#define QSPI_RXFIFOCFG_EN_Pos   (0U)
#define QSPI_RXFIFOCFG_EN_Msk   (0x1U << QSPI_RXFIFOCFG_EN_Pos)                     /*!< 0x00000001 */
#define QSPI_RXFIFOCFG_EN       QSPI_RXFIFOCFG_EN_Msk                               /*!<rxfifo_en */

#define QSPI_RXFIFOCFG_QUADWM_Pos   (1U)
#define QSPI_RXFIFOCFG_QUADWM_Msk   (0x3FU << QSPI_RXFIFOCFG_QUADWM_Pos)            /*!< 0x0000007E */
#define QSPI_RXFIFOCFG_QUADWM       QSPI_RXFIFOCFG_QUADWM_Msk                       /*!<quad_rxfifo_wm[5:0] */

#define QSPI_RXFIFOCFG_NORMWM_Pos   (7U)
#define QSPI_RXFIFOCFG_NORMWM_Msk   (0x3FU << QSPI_RXFIFOCFG_NORMWM_Pos)            /*!< 0x00001F80 */
#define QSPI_RXFIFOCFG_NORMWM       QSPI_RXFIFOCFG_NORMWM_Msk                       /*!<norm_rxfifo_wm[5:0] */

/*******************  Bit definition for QSPI_RXFIFOS register  ********************/
#define QSPI_RXFIFOS_FULL_Pos   (0U)
#define QSPI_RXFIFOS_FULL_Msk   (0x1U << QSPI_RXFIFOS_FULL_Pos)                     /*!< 0x00000001 */
#define QSPI_RXFIFOS_FULL       QSPI_RXFIFOS_FULL_Msk                               /*!<rxfifo_full */

#define QSPI_RXFIFOS_MMAPFULL_Pos   (1U)
#define QSPI_RXFIFOS_MMAPFULL_Msk   (0x1U << QSPI_RXFIFOS_MMAPFULL_Pos)             /*!< 0x00000002 */
#define QSPI_RXFIFOS_MMAPFULL       QSPI_RXFIFOS_MMAPFULL_Msk                       /*!<rxfifo_ahb_full */

#define QSPI_RXFIFOS_NEARFULL_Pos   (2U)
#define QSPI_RXFIFOS_NEARFULL_Msk   (0x1U << QSPI_RXFIFOS_NEARFULL_Pos)             /*!< 0x00000004 */
#define QSPI_RXFIFOS_NEARFULL       QSPI_RXFIFOS_NEARFULL_Msk                       /*!<rxfifo_near_full */

#define QSPI_RXFIFOS_CNTS_Pos   (3U)
#define QSPI_RXFIFOS_CNTS_Msk   (0x3FU << QSPI_RXFIFOS_CNTS_Pos)                    /*!< 0x000001F8 */
#define QSPI_RXFIFOS_CNTS       QSPI_RXFIFOS_CNTS_Msk                               /*!<rxfifo_cnt_s[5:0] */

/*******************  Bit definition for QSPI_TXFIFOADDR register  ********************/
#define QSPI_TXFIFOADDR_DIN_Pos (0U)
#define QSPI_TXFIFOADDR_DIN_Msk (0xFFU << QSPI_TXFIFOADDR_DIN_Pos)                  /*!< 0x000000FF */
#define QSPI_TXFIFOADDR_DIN     QSPI_TXFIFOADDR_DIN_Msk                             /*!<txfifo_din[7:0] */

/*******************  Bit definition for QSPI_TXFIFOCFG register  ********************/
#define QSPI_TXFIFOCFG_EN_Pos   (0U)
#define QSPI_TXFIFOCFG_EN_Msk   (0x1U << QSPI_TXFIFOCFG_EN_Pos)                     /*!< 0x00000001 */
#define QSPI_TXFIFOCFG_EN       QSPI_TXFIFOCFG_EN_Msk                               /*!<txfifo_en */

#define QSPI_TXFIFOCFG_WM_Pos   (1U)
#define QSPI_TXFIFOCFG_WM_Msk   (0x3FU << QSPI_TXFIFOCFG_WM_Pos)                    /*!< 0x0000007E */
#define QSPI_TXFIFOCFG_WM       QSPI_TXFIFOCFG_WM_Msk                               /*!<txfifo_wm[5:0] */

/*******************  Bit definition for QSPI_TXFIFOS register  ********************/
#define QSPI_TXFIFOS_FULL_Pos   (0U)
#define QSPI_TXFIFOS_FULL_Msk   (0x1U << QSPI_TXFIFOS_FULL_Pos)                     /*!< 0x00000001 */
#define QSPI_TXFIFOS_FULL       QSPI_TXFIFOS_FULL_Msk                               /*!<txfifo_full */

#define QSPI_TXFIFOS_EMPTY_Pos  (2U)
#define QSPI_TXFIFOS_EMPTY_Msk  (0x1U << QSPI_TXFIFOS_EMPTY_Pos)                    /*!< 0x00000004 */
#define QSPI_TXFIFOS_EMPTY      QSPI_TXFIFOS_EMPTY_Msk                              /*!<txfifo_empty */

#define QSPI_TXFIFOS_NEMPTY_Pos (3U)
#define QSPI_TXFIFOS_NEMPTY_Msk (0x1U << QSPI_TXFIFOS_NEMPTY_Pos)                   /*!< 0x00000008 */
#define QSPI_TXFIFOS_NEMPTY     QSPI_TXFIFOS_NEMPTY_Msk                             /*!<txfifo_near_empty */

#define QSPI_TXFIFOS_CNTS_Pos   (4U)
#define QSPI_TXFIFOS_CNTS_Msk   (0x3FU << QSPI_TXFIFOS_CNTS_Pos)                    /*!< 0x000003F0 */
#define QSPI_TXFIFOS_CNTS       QSPI_TXFIFOS_CNTS_Msk                               /*!<txfifo_cnt_s[5:0] */

/*******************  Bit definition for QSPI_SOFTCLR register  ********************/
#define QSPI_SOFTCLR_BUFFER0_Pos    (0U)
#define QSPI_SOFTCLR_BUFFER0_Msk    (0x1U << QSPI_SOFTCLR_BUFFER0_Pos)              /*!< 0x00000001 */
#define QSPI_SOFTCLR_BUFFER0        QSPI_SOFTCLR_BUFFER0_Msk                        /*!<buffer0_clr */

#define QSPI_SOFTCLR_BUFFER1_Pos    (1U)
#define QSPI_SOFTCLR_BUFFER1_Msk    (0x1U << QSPI_SOFTCLR_BUFFER1_Pos)              /*!< 0x00000002 */
#define QSPI_SOFTCLR_BUFFER1        QSPI_SOFTCLR_BUFFER1_Msk                        /*!<buffer1_clr */

#define QSPI_SOFTCLR_TXFIFO_Pos (2U)
#define QSPI_SOFTCLR_TXFIFO_Msk (0x1U << QSPI_SOFTCLR_TXFIFO_Pos)                   /*!< 0x00000004 */
#define QSPI_SOFTCLR_TXFIFO     QSPI_SOFTCLR_TXFIFO_Msk                             /*!<txfifo_clr_reg */

#define QSPI_SOFTCLR_RXFIFO_Pos (3U)
#define QSPI_SOFTCLR_RXFIFO_Msk (0x1U << QSPI_SOFTCLR_RXFIFO_Pos)                   /*!< 0x00000008 */
#define QSPI_SOFTCLR_RXFIFO     QSPI_SOFTCLR_RXFIFO_Msk                             /*!<rxfifo_clr_reg */

#define QSPI_SOFTCLR_TRANABORT_Pos  (6U)
#define QSPI_SOFTCLR_TRANABORT_Msk  (0x1U << QSPI_SOFTCLR_TRANABORT_Pos)            /*!< 0x00000040 */
#define QSPI_SOFTCLR_TRANABORT      QSPI_SOFTCLR_TRANABORT_Msk                      /*!<clr_trans_abort */

/*******************  Bit definition for QSPI_SAMPCFG register  ********************/
#define QSPI_SAMPCFG_POINTEN_Pos    (0U)
#define QSPI_SAMPCFG_POINTEN_Msk    (0x1U << QSPI_SAMPCFG_POINTEN_Pos)              /*!< 0x00000001 */
#define QSPI_SAMPCFG_POINTEN        QSPI_SAMPCFG_POINTEN_Msk                        /*!<sample_point_en */

#define QSPI_SAMPCFG_SHIFTEN_Pos    (1U)
#define QSPI_SAMPCFG_SHIFTEN_Msk    (0x1U << QSPI_SAMPCFG_SHIFTEN_Pos)              /*!< 0x00000001 */
#define QSPI_SAMPCFG_SHIFTEN        QSPI_SAMPCFG_SHIFTEN_Msk                        /*!<sample_shift_en */

#define QSPI_SAMPCFG_POINTNUM_Pos   (2U)
#define QSPI_SAMPCFG_POINTNUM_Msk   (0x3U << QSPI_SAMPCFG_POINTNUM_Pos)             /*!< 0x0000000C */
#define QSPI_SAMPCFG_POINTNUM       QSPI_SAMPCFG_POINTNUM_Msk                       /*!<sample_point_num[1:0] */

#define QSPI_SAMPCFG_SHIFTNUM_Pos   (4U)
#define QSPI_SAMPCFG_SHIFTNUM_Msk   (0x3U << QSPI_SAMPCFG_SHIFTNUM_Pos)             /*!< 0x00000030 */
#define QSPI_SAMPCFG_SHIFTNUM       QSPI_SAMPCFG_SHIFTNUM_Msk                       /*!<sample_shift_num[1:0] */

/*******************  Bit definition for QSPI_SAMPPTN register  ********************/
#define QSPI_SAMPPTN_PATTERN_Pos    (0U)
#define QSPI_SAMPPTN_PATTERN_Msk    (0xFFFFFFFFU << QSPI_SAMPPTN_PATTERN_Pos)       /*!< 0xFFFFFFFF */
#define QSPI_SAMPPTN_PATTERN        QSPI_SAMPPTN_PATTERN_Msk                        /*!<sample_pattern */

/* The count of QSPI_SAMPPTN */
#define QSPI_SAMPPTN_COUNT (16U)

/*******************  Bit definition for QSPI_CLKCFG register  ********************/
#define QSPI_CLKCFG_CLKMODE_Pos (0U)
#define QSPI_CLKCFG_CLKMODE_Msk (0x1U << QSPI_CLKCFG_CLKMODE_Pos)                   /*!< 0x00000001 */
#define QSPI_CLKCFG_CLKMODE     QSPI_CLKCFG_CLKMODE_Msk                             /*!<ck_mode */

#define QSPI_CLKCFG_CLKDIV_Pos  (1U)
#define QSPI_CLKCFG_CLKDIV_Msk  (0x7U << QSPI_CLKCFG_CLKDIV_Pos)                    /*!< 0x0000000E */
#define QSPI_CLKCFG_CLKDIV      QSPI_CLKCFG_CLKDIV_Msk                              /*!<clk_div_num[2:0] */

/*******************  Bit definition for QSPI_ABORTCFG register  ********************/
#define QSPI_ABORTCFG_EN_Pos    (0U)
#define QSPI_ABORTCFG_EN_Msk    (0x1U << QSPI_ABORTCFG_EN_Pos)                      /*!< 0x00000001 */
#define QSPI_ABORTCFG_EN        QSPI_ABORTCFG_EN_Msk                                /*!<soft_abort_en */

#define QSPI_ABORTCFG_CSTALLEN_Pos  (1U)
#define QSPI_ABORTCFG_CSTALLEN_Msk  (0x1U << QSPI_ABORTCFG_CSTALLEN_Pos)            /*!< 0x00000002 */
#define QSPI_ABORTCFG_CSTALLEN      QSPI_ABORTCFG_CSTALLEN_Msk                      /*!<cnt_stall_en */

#define QSPI_ABORTCFG_ABORT_Pos (2U)
#define QSPI_ABORTCFG_ABORT_Msk (0x1U << QSPI_ABORTCFG_ABORT_Pos)                   /*!< 0x00000004 */
#define QSPI_ABORTCFG_ABORT     QSPI_ABORTCFG_ABORT_Msk                             /*!<abort */

/*******************  Bit definition for QSPI_STATUS register  ********************/
#define QSPI_STATUS_TRANSOVER_Pos   (0U)
#define QSPI_STATUS_TRANSOVER_Msk   (0x1U << QSPI_STATUS_TRANSOVER_Pos)             /*!< 0x00000001 */
#define QSPI_STATUS_TRANSOVER       QSPI_STATUS_TRANSOVER_Msk                       /*!<trans_over */

#define QSPI_STATUS_STALLSTAT_Pos   (1U)
#define QSPI_STATUS_STALLSTAT_Msk   (0x1U << QSPI_STATUS_STALLSTAT_Pos)             /*!< 0x00000002 */
#define QSPI_STATUS_STALLSTAT       QSPI_STATUS_STALLSTAT_Msk                       /*!<stall_status */

#define QSPI_STATUS_TRANSABORT_Pos  (2U)
#define QSPI_STATUS_TRANSABORT_Msk  (0x1U << QSPI_STATUS_TRANSABORT_Pos)            /*!< 0x00000004 */
#define QSPI_STATUS_TRANSABORT      QSPI_STATUS_TRANSABORT_Msk                      /*!<trans_abort */

/*******************  Bit definition for QSPI_INTEN register  ********************/
#define QSPI_INTEN_TRANSOVER_Pos    (0U)
#define QSPI_INTEN_TRANSOVER_Msk    (0x1U << QSPI_INTEN_TRANSOVER_Pos)              /*!< 0x00000001 */
#define QSPI_INTEN_TRANSOVER        QSPI_INTEN_TRANSOVER_Msk                        /*!<trans_over_inten */

#define QSPI_INTEN_STALL_Pos    (1U)
#define QSPI_INTEN_STALL_Msk    (0x1U << QSPI_INTEN_STALL_Pos)                      /*!< 0x00000002 */
#define QSPI_INTEN_STALL        QSPI_INTEN_STALL_Msk                                /*!<stall_inten */

#define QSPI_INTEN_ABORT_Pos    (2U)
#define QSPI_INTEN_ABORT_Msk    (0x1U << QSPI_INTEN_ABORT_Pos)                      /*!< 0x00000004 */
#define QSPI_INTEN_ABORT        QSPI_INTEN_ABORT_Msk                                /*!<abort_inten */

#define QSPI_INTEN_TFIFONEMPTY_Pos  (4U)
#define QSPI_INTEN_TFIFONEMPTY_Msk  (0x1U << QSPI_INTEN_TFIFONEMPTY_Pos)            /*!< 0x00000010 */
#define QSPI_INTEN_TFIFONEMPTY      QSPI_INTEN_TFIFONEMPTY_Msk                      /*!<txfifo_near_empty_inten */

#define QSPI_INTEN_TXFIFOEMPTY_Pos  (5U)
#define QSPI_INTEN_TXFIFOEMPTY_Msk  (0x1U << QSPI_INTEN_TXFIFOEMPTY_Pos)            /*!< 0x00000020 */
#define QSPI_INTEN_TXFIFOEMPTY      QSPI_INTEN_TXFIFOEMPTY_Msk                      /*!<txfifo_empty_inten */

#define QSPI_INTEN_RXFIFONFULL_Pos  (6U)
#define QSPI_INTEN_RXFIFONFULL_Msk  (0x1U << QSPI_INTEN_RXFIFONFULL_Pos)            /*!< 0x00000040 */
#define QSPI_INTEN_RXFIFONFULL      QSPI_INTEN_RXFIFONFULL_Msk                      /*!<rxfifo_near_full_inten */

#define QSPI_INTEN_RXFIFOFULL_Pos   (7U)
#define QSPI_INTEN_RXFIFOFULL_Msk   (0x1U << QSPI_INTEN_RXFIFOFULL_Pos)             /*!< 0x00000080 */
#define QSPI_INTEN_RXFIFOFULL       QSPI_INTEN_RXFIFOFULL_Msk                       /*!<rxfifo_full_inten */
//</h>
//<h>RAND
/******************************************************************************/
/*                                                                            */
/*                                    RAND_CSR                                */
/*                                                                            */
/******************************************************************************/
/*******************  Bit definition for RANDOM_CTRL register  ********************/
#define RAND_CTRL_OL_TEST_EN_Pos    (0U)
#define RAND_CTRL_OL_TEST_EN__Msk   (0x1U << RAND_CTRL_OL_TEST_EN__Pos)                 /*!< 0x00000001 */
#define RAND_CTRL_OL_TEST_EN_       RAND_CTRL_OL_TEST_EN__Msk                           /*!<online_test_en */

#define RAND_CTRL_SWON_TEST_EN_Pos  (1U)
#define RAND_CTRL_SWON_TEST_EN__Msk (0x1U << RAND_CTRL_SWON_TEST_EN__Pos)               /*!< 0x00000002 */
#define RAND_CTRL_SWON_TEST_EN_     RAND_CTRL_SWON_TEST_EN__Msk                         /*!<switch_on_test_en */

#define RAND_CTRL_DRNG_EN_Pos   (2U)
#define RAND_CTRL_DRNG_EN__Msk  (0x1U << RAND_CTRL_DRNG_EN__Pos)                        /*!< 0x00000004 */
#define RAND_CTRL_DRNG_EN_      RAND_CTRL_DRNG_EN__Msk                                  /*!<drng_en */

#define RAND_CTRL_TRNG_EN_Pos   (8U)
#define RAND_CTRL_TRNG_EN__Msk  (0xFU << RAND_CTRL_TRNG_EN__Pos)                        /*!< 0x00000F00 */
#define RAND_CTRL_TRNG_EN_      RAND_CTRL_TRNG_EN__Msk                                  /*!<trng_en[3:0] */

#define RAND_CTRL_TRNG_CLSEL_Pos    (24U)
#define RAND_CTRL_TRNG_CLSEL__Msk   (0x1U << RAND_CTRL_TRNG_CLSEL__Pos)                 /*!< 0x01000000 */
#define RAND_CTRL_TRNG_CLSEL_       RAND_CTRL_TRNG_CLSEL__Msk                           /*!<trng_clk_sel*/

#define RAND_CTRL_LD_RAMKEY_EN_Pos  (30U)
#define RAND_CTRL_LD_RAMKEY_EN__Msk (0x1U << RAND_CTRL_LD_RAMKEY_EN__Pos)               /*!< 0x40000000 */
#define RAND_CTRL_LD_RAMKEY_EN_     RAND_CTRL_LD_RAMKEY_EN__Msk                         /*!<load_ramkey_en */

#define RAND_CTRL_NSTEST_EN_Pos     (31U)
#define RAND_CTRL_NSTEST_EN__Msk    (0x1U << RAND_CTRL_NSTEST_EN__Pos)                  /*!< 0x80000000 */
#define RAND_CTRL_NSTEST_EN_        RAND_CTRL_NSTEST_EN__Msk                            /*!<noise_sample_test_en */

/*******************  Bit definition for RANDOM_STATUS register  ********************/
#define RAND_STATUS_OL_TEST_RDY_Pos     (0U)
#define RAND_STATUS_OL_TEST_RDY__Msk    (0x1U << RAND_STATUS_OL_TEST_RDY__Pos)          /*!< 0x00000001 */
#define RAND_STATUS_OL_TEST_RDY_        RAND_STATUS_OL_TEST_RDY__Msk                    /*!<online_test_rdy */

#define RAND_STATUS_SWON_TEST_RDY_Pos   (1U)
#define RAND_STATUS_SWON_TEST_RDY__Msk  (0x1U << RAND_STATUS_SWON_TEST_RDY__Pos)        /*!< 0x00000002 */
#define RAND_STATUS_SWON_TEST_RDY_      RAND_STATUS_SWON_TEST_RDY__Msk                  /*!<switch_on_test_rdy */

#define RAND_STATUS_DRNG_RDY_Pos    (2U)
#define RAND_STATUS_DRNG_RDY__Msk   (0x1U << RAND_STATUS_DRNG_RDY__Pos)                 /*!< 0x00000004 */
#define RAND_STATUS_DRNG_RDY_       RAND_STATUS_DRNG_RDY__Msk                           /*!<drng_rdy */

#define RAND_STATUS_RANDOM_FAIL_Pos     (3U)
#define RAND_STATUS_RANDOM_FAIL__Msk    (0x1U << RAND_STATUS_RANDOM_FAIL__Pos)          /*!< 0x00000008 */
#define RAND_STATUS_RANDOM_FAIL_        RAND_STATUS_RANDOM_FAIL__Msk                    /*!<random_fail */

#define RAND_STATUS_TRNG_ATT_Pos    (4U)
#define RAND_STATUS_TRNG_ATT__Msk   (0x1U << RAND_STATUS_TRNG_ATT__Pos)                 /*!< 0x00000010 */
#define RAND_STATUS_TRNG_ATT_       RAND_STATUS_TRNG_ATT__Msk                           /*!<trng_attacked */

#define RAND_STATUS_RANFLAG_ATT_Pos     (5U)
#define RAND_STATUS_RANFLAG_ATT__Msk    (0x1U << RAND_STATUS_RANFLAG_ATT__Pos)          /*!< 0x00000020 */
#define RAND_STATUS_RANFLAG_ATT_        RAND_STATUS_RANFLAG_ATT__Msk                    /*!<random_flag_attacked */

#define RAND_STATUS_INDRNG_ATT_Pos  (6U)
#define RAND_STATUS_INDRNG_ATT__Msk (0x1U << RAND_STATUS_INDRNG_ATT__Pos)               /*!< 0x00000040 */
#define RAND_STATUS_INDRNG_ATT_     RAND_STATUS_INDRNG_ATT__Msk                         /*!<inner_drng_attacked */

#define RAND_STATUS_SFDRNG_ATT_Pos  (7U)
#define RAND_STATUS_SFDRNG_ATT__Msk (0x1U << RAND_STATUS_SFDRNG_ATT__Pos)               /*!< 0x00000080 */
#define RAND_STATUS_SFDRNG_ATT_     RAND_STATUS_SFDRNG_ATT__Msk                         /*!<soft_drng_attacked */

#define RAND_STATUS_NSTEST_RDY_Pos  (31U)
#define RAND_STATUS_NSTEST_RDY__Msk (0x1U << RAND_STATUS_NSTEST_RDY__Pos)               /*!< 0x80000000 */
#define RAND_STATUS_NSTEST_RDY_     RAND_STATUS_NSTEST_RDY__Msk                         /*!<noise_sample_test_rdy */

/*******************  Bit definition for TRNG_DATA1 register  ********************/
#define RAND_TRNG_DATA1_Pos     (0U)
#define RAND_TRNG_DATA1__Msk    (0xFFFFFFFFU << RAND_TRNG_DATA1__Pos)                   /*!< 0xFFFFFFFF */
#define RAND_TRNG_DATA1_        RAND_TRNG_DATA1__Msk                                    /*!<trng_data1 */

/*******************  Bit definition for TRNG_DATA2 register  ********************/
#define RAND_TRNG_DATA2_Pos     (0U)
#define RAND_TRNG_DATA2__Msk    (0xFFFFFFFFU << RAND_TRNG_DATA2__Pos)                   /*!< 0xFFFFFFFF */
#define RAND_TRNG_DATA2_        RAND_TRNG_DATA2__Msk                                    /*!<trng_data2 */

/*******************  Bit definition for TRNG_DATA3 register  ********************/
#define RAND_TRNG_DATA3_Pos     (0U)
#define RAND_TRNG_DATA3__Msk    (0xFFFFFFFFU << RAND_TRNG_DATA3__Pos)                   /*!< 0xFFFFFFFF */
#define RAND_TRNG_DATA3_        RAND_TRNG_DATA3__Msk                                    /*!<trng_data3 */

/*******************  Bit definition for TRNG_DATA4 register  ********************/
#define RAND_TRNG_DATA4_Pos     (0U)
#define RAND_TRNG_DATA4__Msk    (0xFFFFFFFFU << RAND_TRNG_DATA4__Pos)                   /*!< 0xFFFFFFFF */
#define RAND_TRNG_DATA4_        RAND_TRNG_DATA4__Msk                                    /*!<trng_data4 */

/*******************  Bit definition for DRNG_DATA register  ********************/
#define RAND_DRNG_DATA_Pos  (0U)
#define RAND_DRNG_DATA__Msk (0xFFFFFFFFU << RAND_DRNG_DATA__Pos)                        /*!< 0xFFFFFFFF */
#define RAND_DRNG_DATA_     RAND_DRNG_DATA__Msk                                         /*!<drng_data */

/*******************  Bit definition for INNER_DRNG_SEED register  ********************/
#define RAND_DRNG_SEED_Pos  (0U)
#define RAND_DRNG_SEED__Msk (0xFFFFFFFFU << RAND_DRNG_SEED__Pos)                        /*!< 0xFFFFFFFF */
#define RAND_DRNG_SEED_     RAND_DRNG_SEED__Msk                                         /*!<inner_drng_seed Write word only*/
//</h>
//<h>RAMC
/******************************************************************************/
/*                                                                            */
/*                                    RAMCTRL_CSR                             */
/*                                                                            */
/******************************************************************************/
/*******************  Bit definition for RAMC_RAMBISTCFG register  ********************/
#define RAMC_RAMBISTCFG_MARCHCU2_Pos    (0U)
#define RAMC_RAMBISTCFG_MARCHCU2_Msk    (0x1U << RAMC_RAMBISTCFG_MARCHCU2_Pos)              /*!< 0x00000001 */
#define RAMC_RAMBISTCFG_MARCHCU2        RAMC_RAMBISTCFG_MARCHCU2_Msk                        /*!<bist_marchc_start_u2 */

#define RAMC_RAMBISTCFG_MARCHCU1_Pos    (1U)
#define RAMC_RAMBISTCFG_MARCHCU1_Msk    (0x1U << RAMC_RAMBISTCFG_MARCHCU1_Pos)              /*!< 0x00000002 */
#define RAMC_RAMBISTCFG_MARCHCU1        RAMC_RAMBISTCFG_MARCHCU1_Msk                        /*!<bist_marchc_start_u1 */

#define RAMC_RAMBISTCFG_MARCHCL_Pos (2U)
#define RAMC_RAMBISTCFG_MARCHCL_Msk (0x1U << RAMC_RAMBISTCFG_MARCHCL_Pos)                   /*!< 0x00000004 */
#define RAMC_RAMBISTCFG_MARCHCL     RAMC_RAMBISTCFG_MARCHCL_Msk                             /*!<bist_marchc_start_l */

#define RAMC_RAMBISTCFG_DECODU2_Pos (3U)
#define RAMC_RAMBISTCFG_DECODU2_Msk (0x1U << RAMC_RAMBISTCFG_DECODU2_Pos)                   /*!< 0x00000008 */
#define RAMC_RAMBISTCFG_DECODU2     RAMC_RAMBISTCFG_DECODU2_Msk                             /*!<bist_decoder_start_u2 */

#define RAMC_RAMBISTCFG_DECODU1_Pos (4U)
#define RAMC_RAMBISTCFG_DECODU1_Msk (0x1U << RAMC_RAMBISTCFG_DECODU1_Pos)                   /*!< 0x00000010 */
#define RAMC_RAMBISTCFG_DECODU1     RAMC_RAMBISTCFG_DECODU1_Msk                             /*!<bist_decoder_start_u1 */

#define RAMC_RAMBISTCFG_DECODL_Pos  (5U)
#define RAMC_RAMBISTCFG_DECODL_Msk  (0x1U << RAMC_RAMBISTCFG_DECODL_Pos)                    /*!< 0x00000020 */
#define RAMC_RAMBISTCFG_DECODL      RAMC_RAMBISTCFG_DECODL_Msk                              /*!<bist_decoder_start_l */

#define RAMC_RAMBISTCFG_BWEU2_Pos   (6U)
#define RAMC_RAMBISTCFG_BWEU2_Msk   (0x1U << RAMC_RAMBISTCFG_BWEU2_Pos)                     /*!< 0x00000040 */
#define RAMC_RAMBISTCFG_BWEU2       RAMC_RAMBISTCFG_BWEU2_Msk                               /*!<bist_bwe_start_u2 */

#define RAMC_RAMBISTCFG_BWEU1_Pos   (7U)
#define RAMC_RAMBISTCFG_BWEU1_Msk   (0x1U << RAMC_RAMBISTCFG_BWEU1_Pos)                     /*!< 0x00000080 */
#define RAMC_RAMBISTCFG_BWEU1       RAMC_RAMBISTCFG_BWEU1_Msk                               /*!<bist_bwe_start_u1 */

#define RAMC_RAMBISTCFG_BWEL_Pos    (8U)
#define RAMC_RAMBISTCFG_BWEL_Msk    (0x1U << RAMC_RAMBISTCFG_BWEL_Pos)                      /*!< 0x00000100 */
#define RAMC_RAMBISTCFG_BWEL        RAMC_RAMBISTCFG_BWEL_Msk                                /*!<bist_bwe_start_l */

/*******************  Bit definition for RAMC_RAMBISTS register  ********************/
#define RAMC_RAMBISTS_MARCHERRU2_Pos    (0U)
#define RAMC_RAMBISTS_MARCHERRU2_Msk    (0x1U << RAMC_RAMBISTS_MARCHERRU2_Pos)              /*!< 0x00000001 */
#define RAMC_RAMBISTS_MARCHERRU2        RAMC_RAMBISTS_MARCHERRU2_Msk                        /*!<marchc_error_u2 */

#define RAMC_RAMBISTS_MARCHERRU1_Pos    (1U)
#define RAMC_RAMBISTS_MARCHERRU1_Msk    (0x1U << RAMC_RAMBISTS_MARCHERRU1_Pos)              /*!< 0x00000002 */
#define RAMC_RAMBISTS_MARCHERRU1        RAMC_RAMBISTS_MARCHERRU1_Msk                        /*!<marchc_error_u1 */

#define RAMC_RAMBISTS_MARCHERRL_Pos (2U)
#define RAMC_RAMBISTS_MARCHERRL_Msk (0x1U << RAMC_RAMBISTS_MARCHERRL_Pos)                   /*!< 0x00000004 */
#define RAMC_RAMBISTS_MARCHERRL     RAMC_RAMBISTS_MARCHERRL_Msk                             /*!<marchc_error_l */

#define RAMC_RAMBISTS_UNMMARCHU2_Pos    (3U)
#define RAMC_RAMBISTS_UNMMARCHU2_Msk    (0x1U << RAMC_RAMBISTS_UNMMARCHU2_Pos)              /*!< 0x00000008 */
#define RAMC_RAMBISTS_UNMMARCHU2        RAMC_RAMBISTS_UNMMARCHU2_Msk                        /*!<unmatch_marchc_u2 */

#define RAMC_RAMBISTS_UNMMARCHU1_Pos    (4U)
#define RAMC_RAMBISTS_UNMMARCHU1_Msk    (0x1U << RAMC_RAMBISTS_UNMMARCHU1_Pos)              /*!< 0x00000010 */
#define RAMC_RAMBISTS_UNMMARCHU1        RAMC_RAMBISTS_UNMMARCHU1_Msk                        /*!<unmatch_marchc_u1 */

#define RAMC_RAMBISTS_UNMMARCHL_Pos (5U)
#define RAMC_RAMBISTS_UNMMARCHL_Msk (0x1U << RAMC_RAMBISTS_UNMMARCHL_Pos)                   /*!< 0x00000020 */
#define RAMC_RAMBISTS_UNMMARCHL     RAMC_RAMBISTS_UNMMARCHL_Msk                             /*!<unmatch_marchc_u1 */

#define RAMC_RAMBISTS_DECODERRU2_Pos    (6U)
#define RAMC_RAMBISTS_DECODERRU2_Msk    (0x1U << RAMC_RAMBISTS_DECODERRU2_Pos)              /*!< 0x00000040 */
#define RAMC_RAMBISTS_DECODERRU2        RAMC_RAMBISTS_DECODERRU2_Msk                        /*!<decoder_error_u2 */

#define RAMC_RAMBISTS_DECODERRU1_Pos    (7U)
#define RAMC_RAMBISTS_DECODERRU1_Msk    (0x1U << RAMC_RAMBISTS_DECODERRU1_Pos)              /*!< 0x00000080 */
#define RAMC_RAMBISTS_DECODERRU1        RAMC_RAMBISTS_DECODERRU1_Msk                        /*!<decoder_error_u1 */

#define RAMC_RAMBISTS_DECODERRL_Pos (8U)
#define RAMC_RAMBISTS_DECODERRL_Msk (0x1U << RAMC_RAMBISTS_DECODERRL_Pos)                   /*!< 0x00000100 */
#define RAMC_RAMBISTS_DECODERRL     RAMC_RAMBISTS_DECODERRL_Msk                             /*!<decoder_error_l */

#define RAMC_RAMBISTS_UNMDECODU2_Pos    (9U)
#define RAMC_RAMBISTS_UNMDECODU2_Msk    (0x1U << RAMC_RAMBISTS_UNMDECODU2_Pos)              /*!< 0x00000200 */
#define RAMC_RAMBISTS_UNMDECODU2        RAMC_RAMBISTS_UNMDECODU2_Msk                        /*!<unmatch_decoder_u2 */

#define RAMC_RAMBISTS_UNMDECODU1_Pos    (10U)
#define RAMC_RAMBISTS_UNMDECODU1_Msk    (0x1U << RAMC_RAMBISTS_UNMDECODU1_Pos)              /*!< 0x00000400 */
#define RAMC_RAMBISTS_UNMDECODU1        RAMC_RAMBISTS_UNMDECODU1_Msk                        /*!<unmatch_decoder_u1 */

#define RAMC_RAMBISTS_UNMDECODL_Pos (11U)
#define RAMC_RAMBISTS_UNMDECODL_Msk (0x1U << RAMC_RAMBISTS_UNMDECODL_Pos)                   /*!< 0x00000800 */
#define RAMC_RAMBISTS_UNMDECODL     RAMC_RAMBISTS_UNMDECODL_Msk                             /*!<unmatch_decoder_l */

#define RAMC_RAMBISTS_BWEERRU2_Pos  (12U)
#define RAMC_RAMBISTS_BWEERRU2_Msk  (0x1U << RAMC_RAMBISTS_BWEERRU2_Pos)                    /*!< 0x00001000 */
#define RAMC_RAMBISTS_BWEERRU2      RAMC_RAMBISTS_BWEERRU2_Msk                              /*!<bwe_error_u2 */

#define RAMC_RAMBISTS_BWEERRU1_Pos  (13U)
#define RAMC_RAMBISTS_BWEERRU1_Msk  (0x1U << RAMC_RAMBISTS_BWEERRU1_Pos)                    /*!< 0x00002000 */
#define RAMC_RAMBISTS_BWEERRU1      RAMC_RAMBISTS_BWEERRU1_Msk                              /*!<bwe_error_u1 */

#define RAMC_RAMBISTS_BWEERRL_Pos   (14U)
#define RAMC_RAMBISTS_BWEERRL_Msk   (0x1U << RAMC_RAMBISTS_BWEERRL_Pos)                     /*!< 0x00004000 */
#define RAMC_RAMBISTS_BWEERRL       RAMC_RAMBISTS_BWEERRL_Msk                               /*!<bwe_error_l */

#define RAMC_RAMBISTS_UNMBWEU2_Pos  (15U)
#define RAMC_RAMBISTS_UNMBWEU2_Msk  (0x1U << RAMC_RAMBISTS_UNMBWEU2_Pos)                    /*!< 0x00008000 */
#define RAMC_RAMBISTS_UNMBWEU2      RAMC_RAMBISTS_UNMBWEU2_Msk                              /*!<unmatch_bwe_u2 */

#define RAMC_RAMBISTS_UNMBWEU1_Pos  (16U)
#define RAMC_RAMBISTS_UNMBWEU1_Msk  (0x1U << RAMC_RAMBISTS_UNMBWEU1_Pos)                    /*!< 0x00010000 */
#define RAMC_RAMBISTS_UNMBWEU1      RAMC_RAMBISTS_UNMBWEU1_Msk                              /*!<unmatch_bwe_u1 */

#define RAMC_RAMBISTS_UNMBWEL_Pos   (17U)
#define RAMC_RAMBISTS_UNMBWEL_Msk   (0x1U << RAMC_RAMBISTS_UNMBWEL_Pos)                     /*!< 0x00020000 */
#define RAMC_RAMBISTS_UNMBWEL       RAMC_RAMBISTS_UNMBWEL_Msk                               /*!<unmatch_bwe_u1 */

#define RAMC_RAMBISTS_MARCHENDU2_Pos    (18U)
#define RAMC_RAMBISTS_MARCHENDU2_Msk    (0x1U << RAMC_RAMBISTS_MARCHENDU2_Pos)              /*!< 0x00040000 */
#define RAMC_RAMBISTS_MARCHENDU2        RAMC_RAMBISTS_MARCHENDU2_Msk                        /*!<bist_marchc_end_u2 */

#define RAMC_RAMBISTS_MARCHENDU1_Pos    (19U)
#define RAMC_RAMBISTS_MARCHENDU1_Msk    (0x1U << RAMC_RAMBISTS_MARCHENDU1_Pos)              /*!< 0x00080000 */
#define RAMC_RAMBISTS_MARCHENDU1        RAMC_RAMBISTS_MARCHENDU1_Msk                        /*!<bist_marchc_end_u1 */

#define RAMC_RAMBISTS_MARCHENDL_Pos (20U)
#define RAMC_RAMBISTS_MARCHENDL_Msk (0x1U << RAMC_RAMBISTS_MARCHENDL_Pos)                   /*!< 0x00100000 */
#define RAMC_RAMBISTS_MARCHENDL     RAMC_RAMBISTS_MARCHENDL_Msk                             /*!<bist_marchc_end_l */

#define RAMC_RAMBISTS_DECODENDU2_Pos    (21U)
#define RAMC_RAMBISTS_DECODENDU2_Msk    (0x1U << RAMC_RAMBISTS_DECODENDU2_Pos)              /*!< 0x00200000 */
#define RAMC_RAMBISTS_DECODENDU2        RAMC_RAMBISTS_DECODENDU2_Msk                        /*!<bist_decoder_end_u2 */

#define RAMC_RAMBISTS_DECODENDU1_Pos    (22U)
#define RAMC_RAMBISTS_DECODENDU1_Msk    (0x1U << RAMC_RAMBISTS_DECODENDU1_Pos)              /*!< 0x00400000 */
#define RAMC_RAMBISTS_DECODENDU1        RAMC_RAMBISTS_DECODENDU1_Msk                        /*!<bist_decoder_end_u1 */

#define RAMC_RAMBISTS_DECODENDL_Pos (23U)
#define RAMC_RAMBISTS_DECODENDL_Msk (0x1U << RAMC_RAMBISTS_DECODENDL_Pos)                   /*!< 0x00800000 */
#define RAMC_RAMBISTS_DECODENDL     RAMC_RAMBISTS_DECODENDL_Msk                             /*!<bist_decoder_end_l */

#define RAMC_RAMBISTS_BWEENDU2_Pos  (24U)
#define RAMC_RAMBISTS_BWEENDU2_Msk  (0x1U << RAMC_RAMBISTS_BWEENDU2_Pos)                    /*!< 0x01000000 */
#define RAMC_RAMBISTS_BWEENDU2      RAMC_RAMBISTS_BWEENDU2_Msk                              /*!<bist_bwe_end_u2 */

#define RAMC_RAMBISTS_BWEENDU1_Pos  (25U)
#define RAMC_RAMBISTS_BWEENDU1_Msk  (0x1U << RAMC_RAMBISTS_BWEENDU1_Pos)                    /*!< 0x02000000 */
#define RAMC_RAMBISTS_BWEENDU1      RAMC_RAMBISTS_BWEENDU1_Msk                              /*!<bist_bwe_end_u1 */

#define RAMC_RAMBISTS_BWEENDL_Pos   (26U)
#define RAMC_RAMBISTS_BWEENDL_Msk   (0x1U << RAMC_RAMBISTS_BWEENDL_Pos)                     /*!< 0x04000000 */
#define RAMC_RAMBISTS_BWEENDL       RAMC_RAMBISTS_BWEENDL_Msk                               /*!<bist_bwe_end_l */

/*******************  Bit definition for RAMC_HAMCNTTHRL register  ********************/
#define RAMC_HAMCNTTHRL_CNTTHRL_Pos (0U)
#define RAMC_HAMCNTTHRL_CNTTHRL_Msk (0x1FFFFFFU << RAMC_HAMCNTTHRL_CNTTHRL_Pos)             /*!< 0x01FFFFFF */
#define RAMC_HAMCNTTHRL_CNTTHRL     RAMC_HAMCNTTHRL_CNTTHRL_Msk                             /*!<cnt_thresh_l[24:0] */

/*******************  Bit definition for RAMC_HAMCNTTHRU1 register  ********************/
#define RAMC_HAMCNTTHRU1_CNTTHRU1_Pos   (0U)
#define RAMC_HAMCNTTHRU1_CNTTHRU1_Msk   (0x1FFFFFFU << RAMC_HAMCNTTHRU1_CNTTHRU1_Pos)       /*!< 0x01FFFFFF */
#define RAMC_HAMCNTTHRU1_CNTTHRU1       RAMC_HAMCNTTHRU1_CNTTHRU1_Msk                       /*!<cnt_thresh_u1[24:0] */

/*******************  Bit definition for RAMC_HAMCNTTHRU2 register  ********************/
#define RAMC_HAMCNTTHRU1_CNTTHRU2_Pos   (0U)
#define RAMC_HAMCNTTHRU1_CNTTHRU2_Msk   (0x1FFFFFFU << RAMC_HAMCNTTHRU1_CNTTHRU2_Pos)       /*!< 0x01FFFFFF */
#define RAMC_HAMCNTTHRU1_CNTTHRU2       RAMC_HAMCNTTHRU1_CNTTHRU2_Msk                       /*!<cnt_thresh_u2[24:0] */

/*******************  Bit definition for RAMC_RAMREPAIR register  ********************/
#define RAMC_RAMREPAIR_CRE1U2_Pos   (0U)
#define RAMC_RAMREPAIR_CRE1U2_Msk   (0x1U << RAMC_RAMREPAIR_CRE1U2_Pos)                     /*!< 0x00000001 */
#define RAMC_RAMREPAIR_CRE1U2       RAMC_RAMREPAIR_CRE1U2_Msk                               /*!<CRE1_u2 */

#define RAMC_RAMREPAIR_CRE1U1_Pos   (1U)
#define RAMC_RAMREPAIR_CRE1U1_Msk   (0x1U << RAMC_RAMREPAIR_CRE1U1_Pos)                     /*!< 0x00000002 */
#define RAMC_RAMREPAIR_CRE1U1       RAMC_RAMREPAIR_CRE1U1_Msk                               /*!<CRE1_u1 */

#define RAMC_RAMREPAIR_CRE1L_Pos    (2U)
#define RAMC_RAMREPAIR_CRE1L_Msk    (0x1U << RAMC_RAMREPAIR_CRE1L_Pos)                      /*!< 0x00000004 */
#define RAMC_RAMREPAIR_CRE1L        RAMC_RAMREPAIR_CRE1L_Msk                                /*!<CRE1_l */

#define RAMC_RAMREPAIR_CRE2U2_Pos   (3U)
#define RAMC_RAMREPAIR_CRE2U2_Msk   (0x1U << RAMC_RAMREPAIR_CRE2U2_Pos)                     /*!< 0x00000008 */
#define RAMC_RAMREPAIR_CRE2U2       RAMC_RAMREPAIR_CRE2U2_Msk                               /*!<CRE2_u2 */

#define RAMC_RAMREPAIR_CRE2U1_Pos   (4U)
#define RAMC_RAMREPAIR_CRE2U1_Msk   (0x1U << RAMC_RAMREPAIR_CRE2U1_Pos)                     /*!< 0x00000010 */
#define RAMC_RAMREPAIR_CRE2U1       RAMC_RAMREPAIR_CRE2U1_Msk                               /*!<CRE2_u1 */

#define RAMC_RAMREPAIR_CRE2L_Pos    (5U)
#define RAMC_RAMREPAIR_CRE2L_Msk    (0x1U << RAMC_RAMREPAIR_CRE2L_Pos)                      /*!< 0x00000020 */
#define RAMC_RAMREPAIR_CRE2L        RAMC_RAMREPAIR_CRE2L_Msk                                /*!<CRE2_l */

#define RAMC_RAMREPAIR_CFLTMEU2_Pos (6U)
#define RAMC_RAMREPAIR_CFLTMEU2_Msk (0x1U << RAMC_RAMREPAIR_CFLTMEU2_Pos)                   /*!< 0x00000040 */
#define RAMC_RAMREPAIR_CFLTMEU2     RAMC_RAMREPAIR_CFLTMEU2_Msk                             /*!<cal_faultmap_en_u2 */

#define RAMC_RAMREPAIR_CFLTMEU1_Pos (7U)
#define RAMC_RAMREPAIR_CFLTMEU1_Msk (0x1U << RAMC_RAMREPAIR_CFLTMEU1_Pos)                   /*!< 0x00000080 */
#define RAMC_RAMREPAIR_CFLTMEU1     RAMC_RAMREPAIR_CFLTMEU1_Msk                             /*!<cal_faultmap_en_u1 */

#define RAMC_RAMREPAIR_CFLTMEL_Pos  (8U)
#define RAMC_RAMREPAIR_CFLTMEL_Msk  (0x1U << RAMC_RAMREPAIR_CFLTMEL_Pos)                    /*!< 0x00000100 */
#define RAMC_RAMREPAIR_CFLTMEL      RAMC_RAMREPAIR_CFLTMEL_Msk                              /*!<cal_faultmap_en_l */

/*******************  Bit definition for RAMC_RAMFLTS register  ********************/
#define RAMC_RAMFLTS_UNMMAXU2_Pos   (0U)
#define RAMC_RAMFLTS_UNMMAXU2_Msk   (0x1U << RAMC_RAMFLTS_UNMMAXU2_Pos)                     /*!< 0x00000001 */
#define RAMC_RAMFLTS_UNMMAXU2       RAMC_RAMFLTS_UNMMAXU2_Msk                               /*!<unmatch_max_u2 */

#define RAMC_RAMFLTS_UNMMAXU1_Pos   (1U)
#define RAMC_RAMFLTS_UNMMAXU1_Msk   (0x1U << RAMC_RAMFLTS_UNMMAXU1_Pos)                     /*!< 0x00000002 */
#define RAMC_RAMFLTS_UNMMAXU1       RAMC_RAMFLTS_UNMMAXU1_Msk                               /*!<unmatch_max_u1 */

#define RAMC_RAMFLTS_UNMMAXL_Pos    (2U)
#define RAMC_RAMFLTS_UNMMAXL_Msk    (0x1U << RAMC_RAMFLTS_UNMMAXL_Pos)                      /*!< 0x00000004 */
#define RAMC_RAMFLTS_UNMMAXL        RAMC_RAMFLTS_UNMMAXL_Msk                                /*!<unmatch_max_l */

#define RAMC_RAMFLTS_LCREERRU2_Pos  (3U)
#define RAMC_RAMFLTS_LCREERRU2_Msk  (0x1U << RAMC_RAMFLTS_LCREERRU2_Msk)                    /*!< 0x00000008 */
#define RAMC_RAMFLTS_LCREERRU2      RAMC_RAMFLTS_LCREERRU2_Msk                              /*!<left_core_err_u2 */

#define RAMC_RAMFLTS_LCREERRU1_Pos  (4U)
#define RAMC_RAMFLTS_LCREERRU1_Msk  (0x1U << RAMC_RAMFLTS_LCREERRU1_Msk)                    /*!< 0x00000010 */
#define RAMC_RAMFLTS_LCREERRU1      RAMC_RAMFLTS_LCREERRU1_Msk                              /*!<left_core_err_u1 */

#define RAMC_RAMFLTS_LCREERRL_Pos   (5U)
#define RAMC_RAMFLTS_LCREERRL_Msk   (0x1U << RAMC_RAMFLTS_LCREERRL_Pos)                     /*!< 0x00000020 */
#define RAMC_RAMFLTS_LCREERRL       RAMC_RAMFLTS_LCREERRL_Msk                               /*!<left_core_err_l */

#define RAMC_RAMFLTS_RCREERRU2_Pos  (6U)
#define RAMC_RAMFLTS_RCREERRU2_Msk  (0x1U << RAMC_RAMFLTS_RCREERRU2_Pos)                    /*!< 0x00000040 */
#define RAMC_RAMFLTS_RCREERRU2      RAMC_RAMFLTS_RCREERRU2_Msk                              /*!<right_core_err_u2 */

#define RAMC_RAMFLTS_RCREERRU1_Pos  (7U)
#define RAMC_RAMFLTS_RCREERRU1_Msk  (0x1U << RAMC_RAMFLTS_RCREERRU1_Pos)                    /*!< 0x00000080 */
#define RAMC_RAMFLTS_RCREERRU1      RAMC_RAMFLTS_RCREERRU1_Msk                              /*!<right_core_err_u1 */

#define RAMC_RAMFLTS_RCREERRL_Pos   (8U)
#define RAMC_RAMFLTS_RCREERRL_Msk   (0x1U << RAMC_RAMFLTS_RCREERRL_Pos)                     /*!< 0x00000100 */
#define RAMC_RAMFLTS_RCREERRL       RAMC_RAMFLTS_RCREERRL_Msk                               /*!<right_core_err_l */

/*******************  Bit definition for RAMC_RAMENCRY register  ********************/
#define RAMC_RAMENCRY_ENBU2_Pos (0U)
#define RAMC_RAMENCRY_ENBU2_Msk (0x1U << RAMC_RAMENCRY_ENBU2_Pos)                           /*!< 0x00000001 */
#define RAMC_RAMENCRY_ENBU2     RAMC_RAMENCRY_ENBU2_Msk                                     /*!<ENCRY_ENB_u2 */

#define RAMC_RAMENCRY_ENBU1_Pos (1U)
#define RAMC_RAMENCRY_ENBU1_Msk (0x1U << RAMC_RAMENCRY_ENBU1_Pos)                           /*!< 0x00000002 */
#define RAMC_RAMENCRY_ENBU1     RAMC_RAMENCRY_ENBU1_Msk                                     /*!<ENCRY_ENB_u1 */

#define RAMC_RAMENCRY_ENBL_Pos  (2U)
#define RAMC_RAMENCRY_ENBL_Msk  (0x1U << RAMC_RAMENCRY_ENBL_Pos)                            /*!< 0x00000004 */
#define RAMC_RAMENCRY_ENBL      RAMC_RAMENCRY_ENBL_Msk                                      /*!<ENCRY_ENB_l */

/*******************  Bit definition for RAMC_RAMENCRYKL register  ********************/
#define RAMC_RAMENCRYKL_KEYL_Pos    (0U)
#define RAMC_RAMENCRYKL_KEYL_Msk    (0xFFFFFFFFU << RAMC_RAMENCRYKL_KEYL_Pos)               /*!< 0xFFFFFFFF */
#define RAMC_RAMENCRYKL_KEYL        RAMC_RAMENCRYKL_KEYL_Msk                                /*!<ENCRY_KEY_l[31:0] */

/*******************  Bit definition for RAMC_RAMENCRYKU1 register  ********************/
#define RAMC_RAMENCRYKU1_KEYU1_Pos  (0U)
#define RAMC_RAMENCRYKU1_KEYU1_Msk  (0xFFFFFFFFU << RAMC_RAMENCRYKU1_KEYU1_Pos)             /*!< 0xFFFFFFFF */
#define RAMC_RAMENCRYKU1_KEYU1      RAMC_RAMENCRYKU1_KEYU1_Msk                              /*!<ENCRY_KEY_u1[31:0] */

/*******************  Bit definition for RAMC_RAMEMA register  ********************/
#define RAMC_RAMEMA_EMAWU2_Pos  (0U)
#define RAMC_RAMEMA_EMAWU2_Msk  (0x3U << RAMC_RAMEMA_EMAWU2_Pos)                            /*!< 0x00000003 */
#define RAMC_RAMEMA_EMAWU2      RAMC_RAMEMA_EMAWU2_Msk                                      /*!<EMAW_u2[1:0] */

#define RAMC_RAMEMA_EMAWU1_Pos  (2U)
#define RAMC_RAMEMA_EMAWU1_Msk  (0x3U << RAMC_RAMEMA_EMAWU1_Pos)                            /*!< 0x0000000C */
#define RAMC_RAMEMA_EMAWU1      RAMC_RAMEMA_EMAWU1_Msk                                      /*!<EMAW_u1[1:0] */

#define RAMC_RAMEMA_EMAWL_Pos   (4U)
#define RAMC_RAMEMA_EMAWL_Msk   (0x3U << RAMC_RAMEMA_EMAWL_Pos)                             /*!< 0x00000030 */
#define RAMC_RAMEMA_EMAWL       RAMC_RAMEMA_EMAWL_Msk                                       /*!<EMAW_l[1:0] */

#define RAMC_RAMEMA_EMAU2_Pos   (6U)
#define RAMC_RAMEMA_EMAU2_Msk   (0x7U << RAMC_RAMEMA_EMAU2_Pos)                             /*!< 0x000001C0 */
#define RAMC_RAMEMA_EMAU2       RAMC_RAMEMA_EMAU2_Msk                                       /*!<EMA_u2[2:0] */

#define RAMC_RAMEMA_EMAU1_Pos   (9U)
#define RAMC_RAMEMA_EMAU1_Msk   (0x7U << RAMC_RAMEMA_EMAU1_Pos)                             /*!< 0x00000E00 */
#define RAMC_RAMEMA_EMAU1       RAMC_RAMEMA_EMAU1_Msk                                       /*!<EMA_u1[2:0] */

#define RAMC_RAMEMA_EMAL_Pos    (12U)
#define RAMC_RAMEMA_EMAL_Msk    (0x7U << RAMC_RAMEMA_EMAL_Pos)                              /*!< 0x00007000 */
#define RAMC_RAMEMA_EMAL        RAMC_RAMEMA_EMAL_Msk                                        /*!<EMA_L[2:0] */

/*******************  Bit definition for RAMC_RAMFLTA1S register  ********************/
#define RAMC_RAMFLTA1S_MFCA1U2_Pos  (0U)
#define RAMC_RAMFLTA1S_MFCA1U2_Msk  (0x3FU << RAMC_RAMFLTA1S_MFCA1U2_Pos)                   /*!< 0x0000003F */
#define RAMC_RAMFLTA1S_MFCA1U2      RAMC_RAMFLTA1S_MFCA1U2_Msk                              /*!<marchc_fca1_u2[5:0] */

#define RAMC_RAMFLTA1S_MFCA1U1_Pos  (6U)
#define RAMC_RAMFLTA1S_MFCA1U1_Msk  (0x3FU << RAMC_RAMFLTA1S_MFCA1U1_Pos)                   /*!< 0x00000FC0 */
#define RAMC_RAMFLTA1S_MFCA1U1      RAMC_RAMFLTA1S_MFCA1U1_Msk                              /*!<marchc_fca1_u1[5:0] */

#define RAMC_RAMFLTA1S_MFCA1L_Pos   (12U)
#define RAMC_RAMFLTA1S_MFCA1L_Msk   (0x3FU << RAMC_RAMFLTA1S_MFCA1L_Pos)                    /*!< 0x0003F000 */
#define RAMC_RAMFLTA1S_MFCA1L       RAMC_RAMFLTA1S_MFCA1L_Msk                               /*!<marchc_fca1_l[5:0] */

/*******************  Bit definition for RAMC_RAMFLTA2S register  ********************/
#define RAMC_RAMFLTA2S_MFCA2U2_Pos  (0U)
#define RAMC_RAMFLTA2S_MFCA2U2_Msk  (0x3FU << RAMC_RAMFLTA2S_MFCA2U2_Pos)                   /*!< 0x0000003F */
#define RAMC_RAMFLTA2S_MFCA2U2      RAMC_RAMFLTA2S_MFCA2U2_Msk                              /*!<marchc_fca2_u2[5:0] */

#define RAMC_RAMFLTA2S_MFCA2U1_Pos  (6U)
#define RAMC_RAMFLTA2S_MFCA2U1_Msk  (0x3FU << RAMC_RAMFLTA2S_MFCA2U1_Pos)                   /*!< 0x00000FC0 */
#define RAMC_RAMFLTA2S_MFCA2U1      RAMC_RAMFLTA2S_MFCA2U1_Msk                              /*!<marchc_fca2_u1[5:0] */

#define RAMC_RAMFLTA2S_MFCA2L_Pos   (12U)
#define RAMC_RAMFLTA2S_MFCA2L_Msk   (0x3FU << RAMC_RAMFLTA2S_MFCA2L_Pos)                    /*!< 0x0003F000 */
#define RAMC_RAMFLTA2S_MFCA2L       RAMC_RAMFLTA2S_MFCA2L_Msk                               /*!<marchc_fca2_l[5:0] */

/*******************  Bit definition for RAMC_RAMREPA1 register  ********************/
#define RAMC_RAMREPA1_FCA1U2_Pos    (0U)
#define RAMC_RAMREPA1_FCA1U2_Msk    (0x3FU << RAMC_RAMREPA1_FCA1U2_Pos)                     /*!< 0x0000003F */
#define RAMC_RAMREPA1_FCA1U2        RAMC_RAMREPA1_FCA1U2_Msk                                /*!<FCA1_u2[5:0] */

#define RAMC_RAMREPA1_FCA1U1_Pos    (6U)
#define RAMC_RAMREPA1_FCA1U1_Msk    (0x3FU << RAMC_RAMREPA1_FCA1U1_Pos)                     /*!< 0x00000FC0 */
#define RAMC_RAMREPA1_FCA1U1        RAMC_RAMREPA1_FCA1U1_Msk                                /*!<FCA1_u1[5:0] */

#define RAMC_RAMREPA1_FCA1L_Pos (12U)
#define RAMC_RAMREPA1_FCA1L_Msk (0x3FU << RAMC_RAMREPA1_FCA1L_Pos)                          /*!< 0x0003F000 */
#define RAMC_RAMREPA1_FCA1L     RAMC_RAMREPA1_FCA1L_Msk                                     /*!<FCA1_l[5:0] */

/*******************  Bit definition for RAMC_RAMREPA2 register  ********************/
#define RAMC_RAMREPA2_FCA2U2_Pos    (0U)
#define RAMC_RAMREPA2_FCA2U2_Msk    (0x3FU << RAMC_RAMREPA2_FCA2U2_Pos)                     /*!< 0x0000003F */
#define RAMC_RAMREPA2_FCA2U2        RAMC_RAMREPA2_FCA2U2_Msk                                /*!<FCA2_u2[5:0] */

#define RAMC_RAMREPA2_FCA2U1_Pos    (6U)
#define RAMC_RAMREPA2_FCA2U1_Msk    (0x3FU << RAMC_RAMREPA2_FCA2U1_Pos)                     /*!< 0x00000FC0 */
#define RAMC_RAMREPA2_FCA2U1        RAMC_RAMREPA2_FCA2U1_Msk                                /*!<FCA2_u1[5:0] */

#define RAMC_RAMREPA2_FCA2L_Pos (12U)
#define RAMC_RAMREPA2_FCA2L_Msk (0x3FU << RAMC_RAMREPA2_FCA2L_Pos)                          /*!< 0x0003F000 */
#define RAMC_RAMREPA2_FCA2L     RAMC_RAMREPA2_FCA2L_Msk                                     /*!<FCA2_l[5:0] */

/*******************  Bit definition for RAMC_RAMPRTYS register  ********************/
#define RAMC_RAMPRTYS_ERRU2_Pos (0U)
#define RAMC_RAMPRTYS_ERRU2_Msk (0x1U << RAMC_RAMPRTYS_ERRU2_Pos)                           /*!< 0x00000001 */
#define RAMC_RAMPRTYS_ERRU2     RAMC_RAMPRTYS_ERRU2_Msk                                     /*!<ram_parity_err_u2 */

#define RAMC_RAMPRTYS_ERRU1_Pos (1U)
#define RAMC_RAMPRTYS_ERRU1_Msk (0x1U << RAMC_RAMPRTYS_ERRU1_Pos)                           /*!< 0x00000002 */
#define RAMC_RAMPRTYS_ERRU1     RAMC_RAMPRTYS_ERRU1_Msk                                     /*!<ram_parity_err_u1 */

#define RAMC_RAMPRTYS_ERRL_Pos  (2U)
#define RAMC_RAMPRTYS_ERRL_Msk  (0x1U << RAMC_RAMPRTYS_ERRL_Pos)                            /*!< 0x00000004 */
#define RAMC_RAMPRTYS_ERRL      RAMC_RAMPRTYS_ERRL_Msk                                      /*!<ram_parity_err_l */

#define RAMC_RAMPRTYS_ERRU2CLR_Pos  (3U)
#define RAMC_RAMPRTYS_ERRU2CLR_Msk  (0x1U << RAMC_RAMPRTYS_ERRU2CLR_Pos)                    /*!< 0x00000008 */
#define RAMC_RAMPRTYS_ERRU2CLR      RAMC_RAMPRTYS_ERRU2CLR_Msk                              /*!<ram_parity_err_u2_clr */

#define RAMC_RAMPRTYS_ERRU1CLR_Pos  (4U)
#define RAMC_RAMPRTYS_ERRU1CLR_Msk  (0x1U << RAMC_RAMPRTYS_ERRU1CLR_Pos)                    /*!< 0x00000010 */
#define RAMC_RAMPRTYS_ERRU1CLR      RAMC_RAMPRTYS_ERRU1CLR_Msk                              /*!<ram_parity_err_u1_clr */

#define RAMC_RAMPRTYS_ERRLCLR_Pos   (5U)
#define RAMC_RAMPRTYS_ERRLCLR_Msk   (0x1U << RAMC_RAMPRTYS_ERRLCLR_Pos)                     /*!< 0x00000020 */
#define RAMC_RAMPRTYS_ERRLCLR       RAMC_RAMPRTYS_ERRLCLR_Msk                               /*!<ram_parity_err_l_clr */
//</h>
//<h>RTC_VMAIN
/******************************************************************************/
/*                                                                            */
/*                                    RTC_VMAIN                               */
/*                                                                            */
/******************************************************************************/
/*******************  Bit definition for RTC_VMAIN_TWA register  ********************/
#define RTC_VMAIN_TWA_TSWA_Pos  (0U)
#define RTC_VMAIN_TWA_TSWA_Msk  (0x1U << RTC_VMAIN_TWA_TSWA_Pos)                                /*!< 0x00000001 */
#define RTC_VMAIN_TWA_TSWA      RTC_VMAIN_TWA_TSWA_Msk                                          /*!<Time_seconds register write authority */

#define RTC_VMAIN_TWA_TCWA_Pos  (1U)
#define RTC_VMAIN_TWA_TCWA_Msk  (0x1U << RTC_VMAIN_TWA_TCWA_Pos)                                /*!< 0x00000002 */
#define RTC_VMAIN_TWA_TCWA      RTC_VMAIN_TWA_TCWA_Msk                                          /*!<Time_cycle register write authority */

#define RTC_VMAIN_TWA_TPWA_Pos  (2U)
#define RTC_VMAIN_TWA_TPWA_Msk  (0x1U << RTC_VMAIN_TWA_TPWA_Pos)                                /*!< 0x00000004 */
#define RTC_VMAIN_TWA_TPWA      RTC_VMAIN_TWA_TPWA_Msk                                          /*!<Time_prescaler register write authority */

#define RTC_VMAIN_TWA_TSAWA_Pos (3U)
#define RTC_VMAIN_TWA_TSAWA_Msk (0x1U << RTC_VMAIN_TWA_TSAWA_Pos)                               /*!< 0x00000008 */
#define RTC_VMAIN_TWA_TSAWA     RTC_VMAIN_TWA_TSAWA_Msk                                         /*!<Time_seconds_alarm register write authority */

#define RTC_VMAIN_TWA_TCAWA_Pos (4U)
#define RTC_VMAIN_TWA_TCAWA_Msk (0x1U << RTC_VMAIN_TWA_TCAWA_Pos)                               /*!< 0x00000010 */
#define RTC_VMAIN_TWA_TCAWA     RTC_VMAIN_TWA_TCAWA_Msk                                         /*!<Time_cycle_alarm register write authority */

#define RTC_VMAIN_TWA_TCMPWA_Pos    (5U)
#define RTC_VMAIN_TWA_TCMPWA_Msk    (0x1U << RTC_VMAIN_TWA_TCMPWA_Pos)                          /*!< 0x00000020 */
#define RTC_VMAIN_TWA_TCMPWA        RTC_VMAIN_TWA_TCMPWA_Msk                                    /*!<Time_compensation register write authority */

#define RTC_VMAIN_TWA_TCTRLWA_Pos   (6U)
#define RTC_VMAIN_TWA_TCTRLWA_Msk   (0x1U << RTC_VMAIN_TWA_TCTRLWA_Pos)                         /*!< 0x00000040 */
#define RTC_VMAIN_TWA_TCTRLWA       RTC_VMAIN_TWA_TCTRLWA_Msk                                   /*!<Time_control register write authority */

#define RTC_VMAIN_TWA_TCKCFGWA_Pos  (7U)
#define RTC_VMAIN_TWA_TCKCFGWA_Msk  (0x1U << RTC_VMAIN_TWA_TCKCFGWA_Pos)                        /*!< 0x00000080 */
#define RTC_VMAIN_TWA_TCKCFGWA      RTC_VMAIN_TWA_TCKCFGWA_Msk                                  /*!<Time_clk_cfg register write authority */

#define RTC_VMAIN_TWA_TCNTEWA_Pos   (8U)
#define RTC_VMAIN_TWA_TCNTEWA_Msk   (0x1U << RTC_VMAIN_TWA_TCNTEWA_Pos)                         /*!< 0x00000100 */
#define RTC_VMAIN_TWA_TCNTEWA       RTC_VMAIN_TWA_TCNTEWA_Msk                                   /*!<Time_cnt_en register write authority */

#define RTC_VMAIN_TWA_TWKCFGWA_Pos  (9U)
#define RTC_VMAIN_TWA_TWKCFGWA_Msk  (0x1U << RTC_VMAIN_TWA_TWKCFGWA_Pos)                        /*!< 0x00000200 */
#define RTC_VMAIN_TWA_TWKCFGWA      RTC_VMAIN_TWA_TWKCFGWA_Msk                                  /*!<Time_wakeup_cfg register write authority */

#define RTC_VMAIN_TWA_TIEWA_Pos (10U)
#define RTC_VMAIN_TWA_TIEWA_Msk (0x1U << RTC_VMAIN_TWA_TIEWA_Pos)                               /*!< 0x00000400 */
#define RTC_VMAIN_TWA_TIEWA     RTC_VMAIN_TWA_TIEWA_Msk                                         /*!<Time_int_en register write authority */

#define RTC_VMAIN_TWA_TWKEWA_Pos    (11U)
#define RTC_VMAIN_TWA_TWKEWA_Msk    (0x1U << RTC_VMAIN_TWA_TWKEWA_Pos)                          /*!< 0x00000800 */
#define RTC_VMAIN_TWA_TWKEWA        RTC_VMAIN_TWA_TWKEWA_Msk                                    /*!<Time_wakeup_en register write authority */

#define RTC_VMAIN_TWA_TRDCLRWA_Pos  (12U)
#define RTC_VMAIN_TWA_TRDCLRWA_Msk  (0x1U << RTC_VMAIN_TWA_TRDCLRWA_Pos)                        /*!< 0x00001000 */
#define RTC_VMAIN_TWA_TRDCLRWA      RTC_VMAIN_TWA_TRDCLRWA_Msk                                  /*!<Time_record_clr register write authority */

#define RTC_VMAIN_TWA_TSRSTWA_Pos   (13U)
#define RTC_VMAIN_TWA_TSRSTWA_Msk   (0x1U << RTC_VMAIN_TWA_TSRSTWA_Pos)                         /*!< 0x00002000 */
#define RTC_VMAIN_TWA_TSRSTWA       RTC_VMAIN_TWA_TSRSTWA_Msk                                   /*!<Time_softrst register write authority */

#define RTC_VMAIN_TWA_TLWA_Pos  (14U)
#define RTC_VMAIN_TWA_TLWA_Msk  (0x1U << RTC_VMAIN_TWA_TLWA_Pos)                                /*!< 0x00004000 */
#define RTC_VMAIN_TWA_TLWA      RTC_VMAIN_TWA_TLWA_Msk                                          /*!<Time_lock register write authority */

#define RTC_VMAIN_TWA_TSTAWA_Pos    (15U)
#define RTC_VMAIN_TWA_TSTAWA_Msk    (0x1U << RTC_VMAIN_TWA_TSTAWA_Pos)                          /*!< 0x00008000 */
#define RTC_VMAIN_TWA_TSTAWA        RTC_VMAIN_TWA_TSTAWA_Msk                                    /*!<Time_status register write authority */

#define RTC_VMAIN_TWA_TCMPSTAWA_Pos (16U)
#define RTC_VMAIN_TWA_TCMPSTAWA_Msk (0x1U << RTC_VMAIN_TWA_TCMPSTAWA_Pos)                       /*!< 0x00010000 */
#define RTC_VMAIN_TWA_TCMPSTAWA     RTC_VMAIN_TWA_TCMPSTAWA_Msk                                 /*!<Time_compensation_status register write authority */

#define RTC_VMAIN_TWA_TTTSWA_Pos    (17U)
#define RTC_VMAIN_TWA_TTTSWA_Msk    (0x1U << RTC_VMAIN_TWA_TTTSWA_Pos)                          /*!< 0x00020000 */
#define RTC_VMAIN_TWA_TTTSWA        RTC_VMAIN_TWA_TTTSWA_Msk                                    /*!<Time_tamper_time_seconds register write authority */

/*******************  Bit definition for RTC_VMAIN_TRA register  ********************/
#define RTC_VMAIN_TRA_TSRA_Pos  (0U)
#define RTC_VMAIN_TRA_TSRA_Msk  (0x1U << RTC_VMAIN_TRA_TSRA_Pos)                                /*!< 0x00000001 */
#define RTC_VMAIN_TRA_TSRA      RTC_VMAIN_TRA_TSRA_Msk                                          /*!<Time_seconds register read authority */

#define RTC_VMAIN_TRA_TCRA_Pos  (1U)
#define RTC_VMAIN_TRA_TCRA_Msk  (0x1U << RTC_VMAIN_TRA_TCRA_Pos)                                /*!< 0x00000002 */
#define RTC_VMAIN_TRA_TCRA      RTC_VMAIN_TRA_TCRA_Msk                                          /*!<Time_cycle register read authority */

#define RTC_VMAIN_TRA_TPRA_Pos  (2U)
#define RTC_VMAIN_TRA_TPRA_Msk  (0x1U << RTC_VMAIN_TRA_TPRA_Pos)                                /*!< 0x00000004 */
#define RTC_VMAIN_TRA_TPRA      RTC_VMAIN_TRA_TPRA_Msk                                          /*!<Time_prescaler register read authority */

#define RTC_VMAIN_TRA_TSARA_Pos (3U)
#define RTC_VMAIN_TRA_TSARA_Msk (0x1U << RTC_VMAIN_TRA_TSARA_Pos)                               /*!< 0x00000008 */
#define RTC_VMAIN_TRA_TSARA     RTC_VMAIN_TRA_TSARA_Msk                                         /*!<Time_seconds_alarm register read authority */

#define RTC_VMAIN_TRA_TCARA_Pos (4U)
#define RTC_VMAIN_TRA_TCARA_Msk (0x1U << RTC_VMAIN_TRA_TCARA_Pos)                               /*!< 0x00000010 */
#define RTC_VMAIN_TRA_TCARA     RTC_VMAIN_TRA_TCARA_Msk                                         /*!<Time_cycle_alarm register read authority */

#define RTC_VMAIN_TRA_TCMPRA_Pos    (5U)
#define RTC_VMAIN_TRA_TCMPRA_Msk    (0x1U << RTC_VMAIN_TRA_TCMPRA_Pos)                          /*!< 0x00000020 */
#define RTC_VMAIN_TRA_TCMPRA        RTC_VMAIN_TRA_TCMPRA_Msk                                    /*!<Time_compensation register read authority */

#define RTC_VMAIN_TRA_TCTRLRA_Pos   (6U)
#define RTC_VMAIN_TRA_TCTRLRA_Msk   (0x1U << RTC_VMAIN_TRA_TCTRLRA_Pos)                         /*!< 0x00000040 */
#define RTC_VMAIN_TRA_TCTRLRA       RTC_VMAIN_TRA_TCTRLRA_Msk                                   /*!<Time_control register read authority */

#define RTC_VMAIN_TRA_TCKCFGRA_Pos  (7U)
#define RTC_VMAIN_TRA_TCKCFGRA_Msk  (0x1U << RTC_VMAIN_TRA_TCKCFGRA_Pos)                        /*!< 0x00000080 */
#define RTC_VMAIN_TRA_TCKCFGRA      RTC_VMAIN_TRA_TCKCFGRA_Msk                                  /*!<Time_clk_cfg register read authority */

#define RTC_VMAIN_TRA_TCNTERA_Pos   (8U)
#define RTC_VMAIN_TRA_TCNTERA_Msk   (0x1U << RTC_VMAIN_TRA_TCNTERA_Pos)                         /*!< 0x00000100 */
#define RTC_VMAIN_TRA_TCNTERA       RTC_VMAIN_TRA_TCNTERA_Msk                                   /*!<Time_cnt_en register read authority */

#define RTC_VMAIN_TRA_TWKCFGRA_Pos  (9U)
#define RTC_VMAIN_TRA_TWKCFGRA_Msk  (0x1U << RTC_VMAIN_TRA_TWKCFGRA_Pos)                        /*!< 0x00000200 */
#define RTC_VMAIN_TRA_TWKCFGRA      RTC_VMAIN_TRA_TWKCFGRA_Msk                                  /*!<Time_wakeup_cfg register read authority */

#define RTC_VMAIN_TRA_TIERA_Pos (10U)
#define RTC_VMAIN_TRA_TIERA_Msk (0x1U << RTC_VMAIN_TRA_TIERA_Pos)                               /*!< 0x00000400 */
#define RTC_VMAIN_TRA_TIERA     RTC_VMAIN_TRA_TIERA_Msk                                         /*!<Time_int_en register read authority */

#define RTC_VMAIN_TRA_TWKERA_Pos    (11U)
#define RTC_VMAIN_TRA_TWKERA_Msk    (0x1U << RTC_VMAIN_TRA_TWKERA_Pos)                          /*!< 0x00000800 */
#define RTC_VMAIN_TRA_TWKERA        RTC_VMAIN_TRA_TWKERA_Msk                                    /*!<Time_wakeup_en register read authority */

#define RTC_VMAIN_TRA_TRDCLRRA_Pos  (12U)
#define RTC_VMAIN_TRA_TRDCLRRA_Msk  (0x1U << RTC_VMAIN_TRA_TRDCLRRA_Pos)                        /*!< 0x00001000 */
#define RTC_VMAIN_TRA_TRDCLRRA      RTC_VMAIN_TRA_TRDCLRRA_Msk                                  /*!<Time_record_clr register read authority */

#define RTC_VMAIN_TRA_TSRSTRA_Pos   (13U)
#define RTC_VMAIN_TRA_TSRSTRA_Msk   (0x1U << RTC_VMAIN_TRA_TSRSTRA_Pos)                         /*!< 0x00002000 */
#define RTC_VMAIN_TRA_TSRSTRA       RTC_VMAIN_TRA_TSRSTRA_Msk                                   /*!<Time_softrst register read authority */

#define RTC_VMAIN_TRA_TLRA_Pos  (14U)
#define RTC_VMAIN_TRA_TLRA_Msk  (0x1U << RTC_VMAIN_TRA_TLRA_Pos)                                /*!< 0x00004000 */
#define RTC_VMAIN_TRA_TLRA      RTC_VMAIN_TRA_TLRA_Msk                                          /*!<Time_lock register read authority */

#define RTC_VMAIN_TRA_TSTARA_Pos    (15U)
#define RTC_VMAIN_TRA_TSTARA_Msk    (0x1U << RTC_VMAIN_TRA_TSTARA_Pos)                          /*!< 0x00008000 */
#define RTC_VMAIN_TRA_TSTARA        RTC_VMAIN_TRA_TSTARA_Msk                                    /*!<Time_status register read authority */

#define RTC_VMAIN_TRA_TCMPSTARA_Pos (16U)
#define RTC_VMAIN_TRA_TCMPSTARA_Msk (0x1U << RTC_VMAIN_TRA_TCMPSTARA_Pos)                       /*!< 0x00010000 */
#define RTC_VMAIN_TRA_TCMPSTARA     RTC_VMAIN_TRA_TCMPSTARA_Msk                                 /*!<Time_compensation_status register read authority */

#define RTC_VMAIN_TRA_TTTSRA_Pos    (17U)
#define RTC_VMAIN_TRA_TTTSRA_Msk    (0x1U << RTC_VMAIN_TRA_TTTSRA_Pos)                          /*!< 0x00020000 */
#define RTC_VMAIN_TRA_TTTSRA        RTC_VMAIN_TRA_TTTSRA_Msk                                    /*!<Time_tamper_time_seconds register read authority */

/*******************  Bit definition for RTC_VMAIN_RDPE register  ********************/
#define RTC_VMAIN_RDPE_RDPE_Pos (0U)
#define RTC_VMAIN_RDPE_RDPE_Msk (0x1U << RTC_VMAIN_RDPE_RDPE_Pos)                               /*!< 0x00000001 */
#define RTC_VMAIN_RDPE_RDPE     RTC_VMAIN_RDPE_RDPE_Msk                                         /*!<Read parity error */

#define RTC_VMAIN_RDPE_VBRAE_Pos    (1U)
#define RTC_VMAIN_RDPE_VBRAE_Msk    (0x1U << RTC_VMAIN_RDPE_VBRAE_Pos)                          /*!< 0x00000002 */
#define RTC_VMAIN_RDPE_VBRAE        RTC_VMAIN_RDPE_VBRAE_Msk                                    /*!<Vbat register authority error */

//</h>
//<h>RTC_TIME
/******************************************************************************/
/*                                                                            */
/*                                    RTC_TIME                                */
/*                                                                            */
/******************************************************************************/
/*******************  Bit definition for RTC_VBAT_TIME_SECONDS register  ********************/
#define RTC_VBAT_TIME_SECONDS_Pos   (0U)
#define RTC_VBAT_TIME_SECONDS_Msk   (0xFFFFFFFFU << RTC_VBAT_TIME_SECONDS_Pos)                  /*!< 0xFFFFFFFF */
#define RTC_VBAT_TIME_SECONDS       RTC_VBAT_TIME_SECONDS_Msk                                   /*!<Time seconds[31:0]  */

/*******************  Bit definition for RTC_VBAT_TIME_CYCLE register  ********************/
#define RTC_VBAT_TIME_CYCLE_Pos (0U)
#define RTC_VBAT_TIME_CYCLE_Msk (0xFFFFU << RTC_VBAT_TIME_CYCLE_Pos)                            /*!< 0x0000FFFF */
#define RTC_VBAT_TIME_CYCLE     RTC_VBAT_TIME_CYCLE_Msk                                         /*!<Time cycle[15:0]  */

/*******************  Bit definition for RTC_VBAT_TIME_PRESCALER register  ********************/
#define RTC_VBAT_PSC_PSC_Pos    (0U)
#define RTC_VBAT_PSC_PSC_Msk    (0xFFFFU << RTC_VBAT_PSC_PSC_Pos)                               /*!< 0x0000FFFF */
#define RTC_VBAT_PSC_PSC        RTC_VBAT_PSC_PSC_Msk                                            /*!<Time prescaler[15:0]  */

/*******************  Bit definition for RTC_VBAT_TIME_SECONDALARM register  ********************/
#define RTC_VBAT_ALARM_TIME_Pos (0U)
#define RTC_VBAT_ALARM_TIME_Msk (0xFFFFFFFFU << RTC_VBAT_ALARM_TIME_Pos)                        /*!< 0xFFFFFFFF */
#define RTC_VBAT_ALARM_TIME     RTC_VBAT_ALARM_TIME_Msk                                         /*!<Time alarm[31:0]  */

/*******************  Bit definition for RTC_VBAT_TIME_CYCLEALARM register  ********************/
#define RTC_VBAT_CYCLEALARM_Pos (0U)
#define RTC_VBAT_CYCLEALARM_Msk (0xFFFFU << RTC_VBAT_CYCLEALARM_Pos)                            /*!< 0x0000FFFF */
#define RTC_VBAT_CYCLEALARM     RTC_VBAT_CYCLEALARM_Msk                                         /*!<Time cycle alarm[15:0]  */

/*******************  Bit definition for RTC_VBAT_TIME_COMPENSATION register  ********************/
#define RTC_VBAT_COMP_VAL_Pos   (0U)
#define RTC_VBAT_COMP_VAL_Msk   (0x7FU << RTC_VBAT_COMP_VAL_Pos)                                /*!< 0x0000007F */
#define RTC_VBAT_COMP_VAL       RTC_VBAT_COMP_VAL_Msk                                           /*!<UTC_COMPENSATION[6:0]  */

#define RTC_VBAT_COMP_SIGN_Pos  (7U)
#define RTC_VBAT_COMP_SIGN_Msk  (0x1U << RTC_VBAT_COMP_SIGN_Pos)                                /*!< 0x00000080 */
#define RTC_VBAT_COMP_SIGN      RTC_VBAT_COMP_SIGN_Msk                                          /*!<UTC_COMPENSATION_SIGN  */

#define RTC_VBAT_COMP_INVL_Pos  (8U)
#define RTC_VBAT_COMP_INVL_Msk  (0xFFU << RTC_VBAT_COMP_INVL_Pos)                               /*!< 0x0000FF00 */
#define RTC_VBAT_COMP_INVL      RTC_VBAT_COMP_INVL_Msk                                          /*!<UTC_COMPENSATION_INTERVAL[7:0]  */

/*******************  Bit definition for RTC_VBAT_TIME_CONTROL register  ********************/
#define RTC_TIME_CTRL_NACCESS_Pos   (0U)
#define RTC_TIME_CTRL_NACCESS_Msk   (0x1U << RTC_TIME_CTRL_NACCESS_Pos)
#define RTC_TIME_CTRL_NACCESS       RTC_TIME_CTRL_NACCESS_Msk                                   /*!<RTC_TIME_CTRL_NACCESS  */

#define RTC_TIME_CTRL_UPDATEMODE_Pos    (1U)
#define RTC_TIME_CTRL_UPDATEMODE_Msk    (0x1U << RTC_TIME_CTRL_UPDATEMODE_Pos)                  /*!< 0x00000002 */
#define RTC_TIME_CTRL_UPDATEMODE        RTC_TIME_CTRL_UPDATEMODE_Msk                            /*!<RTC_TIME_CTRL_UPDATEMODE */

#define RTC_TIME_CTRL_STOPMODE_Pos  (2U)
#define RTC_TIME_CTRL_STOPMODE_Msk  (0x1U << RTC_TIME_CTRL_STOPMODE_Pos)                        /*!< 0x00000004 */
#define RTC_TIME_CTRL_STOPMODE      RTC_TIME_CTRL_STOPMODE_Msk                                  /*!<RTC_TIME_CTRL_STOPMODE  */

#define RTC_TIME_CTRL_CPMODE_Pos    (3U)
#define RTC_TIME_CTRL_CPMODE_Msk    (0x1U << RTC_TIME_CTRL_CPMODE_Pos)                          /*!< 0x00000008 */
#define RTC_TIME_CTRL_CPMODE        RTC_TIME_CTRL_CPMODE_Msk                                    /*!<RTC_TIME_CTRL_CPMODE  */

/*******************  Bit definition for RTC_VBAT_TIME_CLKCFG register  ********************/
#define RTC_TIME_CLKCFG_CLKSEL_Pos  (0U)
#define RTC_TIME_CLKCFG_CLKSEL_Msk  (0x3U << RTC_TIME_CLKCFG_CLKSEL_Pos)                        /*!< 0x00000003 */
#define RTC_TIME_CLKCFG_CLKSEL      RTC_TIME_CLKCFG_CLKSEL_Msk                                  /*!<RTC_TIME_CLKCFG_CLKSEL  */

#define RTC_TIME_CLKCFG_32XTALEN_Pos    (2U)
#define RTC_TIME_CLKCFG_32XTALEN_Msk    (0x1U << RTC_TIME_CLKCFG_32XTALEN_Pos)                  /*!< 0x0000004 */
#define RTC_TIME_CLKCFG_32XTALEN        RTC_TIME_CLKCFG_32XTALEN_Msk                            /*!<RTC_TIME_CLKCFG_32XTALEN  */

#define RTC_TIME_CLKCFG_32OSCEN_Pos (3U)
#define RTC_TIME_CLKCFG_32OSCEN_Msk (0x1U << RTC_TIME_CLKCFG_32OSCEN_Pos)                       /*!< 0x0000008 */
#define RTC_TIME_CLKCFG_32OSCEN     RTC_TIME_CLKCFG_32OSCEN_Msk                                 /*!<RTC_TIME_CLKCFG_CLKOUTPUT_Msk  */

#define RTC_TIME_CLKCFG_CLKOUTPUT_Pos   (4U)
#define RTC_TIME_CLKCFG_CLKOUTPUT_Msk   (0x1U << RTC_TIME_CLKCFG_CLKOUTPUT_Pos)                 /*!< 0x00000010 */
#define RTC_TIME_CLKCFG_CLKOUTPUT       RTC_TIME_CLKCFG_CLKOUTPUT_Msk                           /*!<CLK_OUTPUT  */

/*******************  Bit definition for RTC_VBAT_TIME_CNTEN register  ********************/
#define RTC_TIME_CNTEN_SECCNTEN_Pos (0U)
#define RTC_TIME_CNTEN_SECCNTEN_Msk (0x1U << RTC_TIME_CNTEN_SECCNTEN_Pos)                       /*!< 0x00000001 */
#define RTC_TIME_CNTEN_SECCNTEN     RTC_TIME_CNTEN_SECCNTEN_Msk                                 /*!<RTC_TIME_CNTEN_SECCNTEN */

#define RTC_TIME_CNTEN_CYCCNTEN_Pos (1U)
#define RTC_TIME_CNTEN_CYCCNTEN_MSK (0x1U << RTC_TIME_CNTEN_CYCCNTEN_Pos)                       /*!< 0x00000002 */
#define RTC_TIME_CNTEN_CYCCCNTEN    RTC_TIME_CNTEN_CYCCNTEN_MSK                                 /*!<RTC_TIME_CNTEN_CYCCCNTEN */

#define RTC_TIME_CNTEN_PSCCNTEN_Pos (2U)
#define RTC_TIME_CNTEN_PSCCNTEN_MSK (0x1U << RTC_TIME_CNTEN_PSCCNTEN_Pos)                       /*!< 0x00000004 */
#define RTC_TIME_CNTEN_PSCCCNTEN    RTC_TIME_CNTEN_PSCCNTEN_MSK                                 /*!<RTC_TIME_CNTEN_PSCCCNTEN */

/*******************  Bit definition for RTC_VBAT_TIME_WAKUPCFG register  ********************/
#define RTC_TIME_WAKUPCFG_PINON_Pos (0U)
#define RTC_TIME_WAKUPCFG_PINON_Msk (0x1U << RTC_TIME_WAKUPCFG_PINON_Pos)                       /*!< 0x00000001 */
#define RTC_TIME_WAKUPCFG_PINON     RTC_TIME_WAKUPCFG_PINON_Msk                                 /*!<RTC_TIME_WAKUPCFG_PINON */

#define RTC_TIME_WAKUPCFG_PININV_Pos    (1U)
#define RTC_TIME_WAKUPCFG_PININV_Msk    (0x1U << RTC_TIME_WAKUPCFG_PININV_Pos)                  /*!< 0x00000002 */
#define RTC_TIME_WAKUPCFG_PININV        RTC_TIME_WAKUPCFG_PININV_Msk                            /*!<RTC_TIME_WAKUPCFG_PININV */

#define RTC_TIME_WAKUPCFG_PINODEN_Pos   (2U)
#define RTC_TIME_WAKUPCFG_PINODEN_Msk   (0x1U << RTC_TIME_WAKUPCFG_PINODEN_Pos)                 /*!< 0x00000004 */
#define RTC_TIME_WAKUPCFG_PINODEN       RTC_TIME_WAKUPCFG_PINODEN_Msk                           /*!<RTC_TIME_WAKUPCFG_PINODEN */
/*******************  Bit definition for RTC_VBAT_TIME_INTEN register  ********************/
#define RTC_TIME_TIMEIE_INVLDIE_Pos (0U)
#define RTC_TIME_TIMEIE_INVLDIE_Msk (0x1U << RTC_TIME_TIMEIE_INVLDIE_Pos)                       /*!< 0x00000001 */
#define RTC_TIME_TIMEIE_INVLDIE     RTC_TIME_TIMEIE_INVLDIE_Msk                                 /*!<RTC_TIME_TIMEIE_INVLDIE  */

#define RTC_TIME_TIMEIE_OVIE_Pos    (1U)
#define RTC_TIME_TIMEIE_OVIE_Msk    (0x1U << RTC_TIME_TIMEIE_OVIE_Pos)                          /*!< 0x00000002 */
#define RTC_TIME_TIMEIE_OVIE        RTC_TIME_TIMEIE_OVIE_Msk                                    /*!<RTC_TIME_TIMEIE_OVIE  */

#define RTC_TIME_TIMEIE_SECONDIE_Pos    (2U)
#define RTC_TIME_TIMEIE_SECONDIE_Msk    (0x1U << RTC_TIME_TIMEIE_SECONDIE_Pos)                  /*!< 0x00000004 */
#define RTC_TIME_TIMEIE_SECONDIE        RTC_TIME_TIMEIE_SECONDIE_Msk                            /*!<RTC_TIME_TIMEIE_SECONDIE  */

#define RTC_TIME_TIMEIE_1SIE_Pos    (3U)
#define RTC_TIME_TIMEIE_1SIE_Msk    (0x1U << RTC_TIME_TIMEIE_1SIE_Pos)                          /*!< 0x00000008 */
#define RTC_TIME_TIMEIE_1SIE        RTC_TIME_TIMEIE_1SIE_Msk                                    /*!<RTC_TIME_TIMEIE_1SIE  */

#define RTC_TIME_TIMEIE_CYCLEIE_Pos (4U)
#define RTC_TIME_TIMEIE_CYCLEIE_Msk (0x1U << RTC_TIME_TIMEIE_CYCLEIE_Pos)                       /*!< 0x00000010 */
#define RTC_TIME_TIMEIE_CYCLEIE     RTC_TIME_TIMEIE_CYCLEIE_Msk                                 /*!<RTC_TIME_TIMEIE_CYCLEIE  */

#define RTC_TIME_TIMEIE_1SFLAGIE_Pos    (11U)
#define RTC_TIME_TIMEIE_1SFLAGIE_Msk    (0x1U << RTC_TIME_TIMEIE_1SFLAGIE_Pos)                  /*!< 0x00000800 */
#define RTC_TIME_TIMEIE_1SFLAGIE        RTC_TIME_TIMEIE_1SFLAGIE_Msk                            /*!<RTC_TIME_TIMEIE_1SFLAGIE  */

/*******************  Bit definition for RTC_VBAT_TIME_WAKUPEN register  ********************/
#define RTC_TIME_WAKUPEN_INVLDWUEN_Pos  (0U)
#define RTC_TIME_WAKUPEN_INVLDWUEN_Msk  (0x1U << RTC_TIME_WAKUPEN_INVLDWUEN_Pos)                /*!< 0x00000001 */
#define RTC_TIME_WAKUPEN_INVLDWUEN      RTC_TIME_WAKUPEN_INVLDWUEN_Msk                          /*!<RTC_TIME_WAKUPEN_INVLDWUEN  */

#define RTC_TIME_WAKUPEN_OVWUEN_Pos (1U)
#define RTC_TIME_WAKUPEN_OVWUEN_Msk (0x1U << RTC_TIME_WAKUPEN_OVWUEN_Pos)                       /*!< 0x00000002 */
#define RTC_TIME_WAKUPEN_OVWUEN     RTC_TIME_WAKUPEN_OVWUEN_Msk                                 /*!<RTC_TIME_WAKUPEN_OVWUEN  */

#define RTC_TIME_WAKUPEN_SECONDWUEN_Pos (2U)
#define RTC_TIME_WAKUPEN_SECONDWUEN_Msk (0x1U << RTC_TIME_WAKUPEN_SECONDWUEN_Pos)               /*!< 0x00000004 */
#define RTC_TIME_WAKUPEN_SECONDWUEN     RTC_TIME_WAKUPEN_SECONDWUEN_Msk                         /*!<RTC_TIME_WAKUPEN_SECONDWUEN  */

#define RTC_TIME_WAKUPEN_1SWUEN_Pos (3U)
#define RTC_TIME_WAKUPEN_1SWUEN_Msk (0x1U << RTC_TIME_WAKUPEN_1SWUEN_Pos)                       /*!< 0x00000008 */
#define RTC_TIME_WAKUPEN_1SWUEN     RTC_TIME_WAKUPEN_1SWUEN_Msk                                 /*!<RTC_TIME_WAKUPEN_1SWUEN  */

#define RTC_TIME_WAKUPEN_CYCWUEN_Pos    (4U)
#define RTC_TIME_WAKUPEN_CYCWUEN_Msk    (0x1U << RTC_TIME_WAKUPEN_CYCWUEN_Pos)                  /*!< 0x00000010 */
#define RTC_TIME_WAKUPEN_CYCWUEN        RTC_TIME_WAKUPEN_CYCWUEN_Msk                            /*!<RTC_TIME_WAKUPEN_CYCWUEN  */

#define RTC_TIME_WAKUPEN_INVLDFGWUEN_Pos    (8U)
#define RTC_TIME_WAKUPEN_INVLDFGWUEN_Msk    (0x1U << RTC_TIME_WAKUPEN_INVLDFGWUEN_Pos)          /*!< 0x00000100 */
#define RTC_TIME_WAKUPEN_INVLDFGWUEN        RTC_TIME_WAKUPEN_INVLDFGWUEN_Msk                    /*!<RTC_TIME_WAKUPEN_INVLDFGWUEN  */

#define RTC_TIME_WAKUPEN_OVFGWUEN_Pos   (9U)
#define RTC_TIME_WAKUPEN_OVFGWUEN_Msk   (0x1U << RTC_TIME_WAKUPEN_OVFGWUEN_Pos)                 /*!< 0x00000200 */
#define RTC_TIME_WAKUPEN_OVFGWUEN       RTC_TIME_WAKUPEN_OVFGWUEN_Msk                           /*!<RTC_TIME_WAKUPEN_OVFGWUEN  */

#define RTC_TIME_WAKUPEN_SECONDFGWUEN_Pos   (10U)
#define RTC_TIME_WAKUPEN_SECONDFGWUEN_Msk   (0x1U << RTC_TIME_WAKUPEN_SECONDFGWUEN_Pos)         /*!< 0x00000400 */
#define RTC_TIME_WAKUPEN_SECONDFGWUEN       RTC_TIME_WAKUPEN_SECONDFGWUEN_Msk                   /*!<RTC_TIME_WAKUPEN_SECONDFGWUEN  */

#define RTC_TIME_WAKUPEN_1SFGWUEN_Pos   (11U)
#define RTC_TIME_WAKUPEN_1SFGWUEN_Msk   (0x1U << RTC_TIME_WAKUPEN_1SFGWUEN_Pos)                 /*!< 0x00000800 */
#define RTC_TIME_WAKUPEN_1SFGWUEN       RTC_TIME_WAKUPEN_1SFGWUEN_Msk                           /*!<RTC_TIME_WAKUPEN_1SFGWUEN  */

#define RTC_TIME_WAKUPEN_CYCFGWUEN_Pos  (12U)
#define RTC_TIME_WAKUPEN_CYCFGWUEN_Msk  (0x1U << RTC_TIME_WAKUPEN_CYCFGWUEN_Pos)                /*!< 0x00001000 */
#define RTC_TIME_WAKUPEN_CYCFGWUEN      RTC_TIME_WAKUPEN_CYCFGWUEN_Msk                          /*!<RTC_TIME_WAKUPEN_CYCFGWUEN  */

/*******************  Bit definition for RTC_VBAT_TIME_RECORDCLR register  ********************/
#define RTC_TIME_RECORDCLR_INVLDCLR_Pos (16U)
#define RTC_TIME_RECORDCLR_INVLDCLR_Msk (0x1U << RTC_TIME_RECORDCLR_INVLDCLR_Pos)               /*!< 0x00010000 */
#define RTC_TIME_RECORDCLR_INVLDCLR     RTC_TIME_RECORDCLR_INVLDCLR_Msk                         /*!<RTC_TIME_RECORDCLR_INVLDCLR  */

#define RTC_TIME_RECORDCLR_OVCLR_Pos    (17U)
#define RTC_TIME_RECORDCLR_OVCLR_Msk    (0x1U << RTC_TIME_RECORDCLR_OVCLR_Pos)                  /*!< 0x00020000 */
#define RTC_TIME_RECORDCLR_OVCLR        RTC_TIME_RECORDCLR_OVCLR_Msk                            /*!<RTC_TIME_RECORDCLR_OVCLR  */

#define RTC_TIME_RECORDCLR_SECONDCLR_Pos    (18U)
#define RTC_TIME_RECORDCLR_SECONDCLR_Msk    (0x1U << RTC_TIME_RECORDCLR_SECONDCLR_Pos)          /*!< 0x00040000 */
#define RTC_TIME_RECORDCLR_SECONDCLR        RTC_TIME_RECORDCLR_SECONDCLR_Msk                    /*!<RTC_TIME_RECORDCLR_SECONDCLR  */

#define RTC_TIME_RECORDCLR_1SCLR_Pos    (19U)
#define RTC_TIME_RECORDCLR_1SCLR_Msk    (0x1U << RTC_TIME_RECORDCLR_1SCLR_Pos)                  /*!< 0x00080000 */
#define RTC_TIME_RECORDCLR_1SCLR        RTC_TIME_RECORDCLR_1SCLR_Msk                            /*!<RTC_TIME_RECORDCLR_1SCLR  */

#define RTC_TIME_RECORDCLR_CYCCLR_Pos   (20U)
#define RTC_TIME_RECORDCLR_CYCCLR_Msk   (0x1U << RTC_TIME_RECORDCLR_CYCCLR_Pos)                 /*!< 0x00100000 */
#define RTC_TIME_RECORDCLR_CYCCLR       RTC_TIME_RECORDCLR_CYCCLR_Msk                           /*!<RTC_TIME_RECORDCLR_CYCCLR*/

/****************************  Bit definition for RTC_VBAT_TIME_SOFTRST register  ********************/
#define RTC_VBAT_TIME_SOFTRST_Pos   (0U)
#define RTC_VBAT_TIME_SOFTRST_Msk   (0xFFFFFFFFU << RTC_VBAT_TIME_SOFTRST_Pos)                  /*!< 0xFFFFFFFF */
#define RTC_VBAT_TIME_SOFTRST       RTC_VBAT_TIME_SOFTRST_Msk                                   /*!<TIME_SOFTRST  */

/*******************  Bit definition for RTC_VBAT_TIME_LOCK register  ********************/
#define RTC_TIME_LOCK_COMPLK_Pos    (0U)
#define RTC_TIME_LOCK_COMPLK_Msk    (0x1U << RTC_TIME_LOCK_COMPLK_Pos)                          /*!< 0x00000001 */
#define RTC_TIME_LOCK_COMPLK        RTC_TIME_LOCK_COMPLK_Msk                                    /*!<RTC_TIME_LOCK_COMPLK  */

#define RTC_TIME_LOCK_CTRLLK_Pos    (1U)
#define RTC_TIME_LOCK_CTRLLK_Msk    (0x1U << RTC_TIME_LOCK_CTRLLK_Pos)                          /*!< 0x00000002 */
#define RTC_TIME_LOCK_CTRLLK        RTC_TIME_LOCK_CTRLLK_Msk                                    /*!<RTC_TIME_LOCK_CTRLLK  */

#define RTC_TIME_LOCK_CLKCFGLK_Pos  (2U)
#define RTC_TIME_LOCK_CLKCFGLK_Msk  (0x1U << RTC_TIME_LOCK_CLKCFGLK_Pos)                        /*!< 0x00000004 */
#define RTC_TIME_LOCK_CLKCFGLK      RTC_TIME_LOCK_CLKCFGLK_Msk                                  /*!<RTC_TIME_LOCK_CLKCFGLK  */

#define RTC_TIME_LOCK_CNTENLK_Pos   (3U)
#define RTC_TIME_LOCK_CNTENLK_Msk   (0x1U << RTC_TIME_LOCK_CNTENLK_Pos)                         /*!< 0x00000008 */
#define RTC_TIME_LOCK_CNTENLK       RTC_TIME_LOCK_CNTENLK_Msk                                   /*!<RTC_TIME_LOCK_CNTENLK  */

#define RTC_TIME_LOCK_WAKUPCFGLK_Pos    (4U)
#define RTC_TIME_LOCK_WAKUPCFGLK_Msk    (0x1U << RTC_TIME_LOCK_WAKUPCFGLK_Pos)                  /*!< 0x00000010 */
#define RTC_TIME_LOCK_WAKUPCFGLK        RTC_TIME_LOCK_WAKUPCFGLK_Msk                            /*!<RTC_TIME_LOCK_WAKUPCFGLK  */

#define RTC_TIME_LOCK_INTENLK_Pos   (5U)
#define RTC_TIME_LOCK_INTENLK_Msk   (0x1U << RTC_TIME_LOCK_INTENLK_Pos)                         /*!< 0x00000020 */
#define RTC_TIME_LOCK_INTENLK       RTC_TIME_LOCK_INTENLK_Msk                                   /*!<RTC_TIME_LOCK_INTENLK  */

#define RTC_TIME_LOCK_WAKUPENLK_Pos (6U)
#define RTC_TIME_LOCK_WAKUPENLK_Msk (0x1U << RTC_TIME_LOCK_WAKUPENLK_Pos)                       /*!< 0x00000040 */
#define RTC_TIME_LOCK_WAKUPENLK     RTC_TIME_LOCK_WAKUPENLK_Msk                                 /*!<RTC_TIME_LOCK_WAKUPCFGLK  */

#define RTC_TIME_LOCK_RECORDCLRLK_Pos   (7U)
#define RTC_TIME_LOCK_RECORDCLRLK_Msk   (0x1U << RTC_TIME_LOCK_RECORDCLRLK_Pos)                 /*!< 0x00000080 */
#define RTC_TIME_LOCK_RECORDCLRLK       RTC_TIME_LOCK_RECORDCLRLK_Msk                           /*!<RTC_TIME_LOCK_RECORDCLRLK  */

#define RTC_TIME_LOCK_SOFTRSTLK_Pos (8U)
#define RTC_TIME_LOCK_SOFTRSTLK_Msk (0x1U << RTC_TIME_LOCK_SOFTRSTLK_Pos)                       /*!< 0x00000100 */
#define RTC_TIME_LOCK_SOFTRSTLK     RTC_TIME_LOCK_SOFTRSTLK_Msk                                 /*!<RTC_TIME_LOCK_SOFTRSTLK  */

#define RTC_TIME_LOCK_LOCKLK_Pos    (9U)
#define RTC_TIME_LOCK_LOCKLK_Msk    (0x1U << RTC_TIME_LOCK_LOCKLK_Pos)                          /*!< 0x00000200 */
#define RTC_TIME_LOCK_LOCKLK        RTC_TIME_LOCK_SOFTRSTLK_Msk                                 /*!<RTC_TIME_LOCK_LOCKLK  */

/*******************  Bit definition for RTC_VBAT_TIME_STATUS register  ********************/
#define RTC_TIME_STATUS_INVLDFG_Pos (0U)
#define RTC_TIME_STATUS_INVLDFG_Msk (0x1U << RTC_TIME_STATUS_INVLDFG_Pos)                       /*!< 0x00000001 */
#define RTC_TIME_STATUS_INVLDFG     RTC_TIME_STATUS_INVLDFG_Msk                                 /*!<RTC_TIME_STATUS_INVLDFG  */

#define RTC_TIME_STATUS_OVFG_Pos    (1U)
#define RTC_TIME_STATUS_OVFG_Msk    (0x1U << RTC_TIME_STATUS_OVFG_Pos)                          /*!< 0x00000002 */
#define RTC_TIME_STATUS_OVFG        RTC_TIME_STATUS_OVFG_Msk                                    /*!<RTC_TIME_STATUS_OVFG  */

#define RTC_TIME_STATUS_SECONDFG_Pos    (2U)
#define RTC_TIME_STATUS_SECONDFG_Msk    (0x1U << RTC_TIME_STATUS_SECONDFG_Pos)                  /*!< 0x00000004 */
#define RTC_TIME_STATUS_SECONDFG        RTC_TIME_STATUS_SECONDFG_Msk                            /*!<RTC_TIME_STATUS_SECONDFG  */

#define RTC_TIME_STATUS_1SFG_Pos    (3U)
#define RTC_TIME_STATUS_1SFG_Msk    (0x1U << RTC_TIME_STATUS_1SFG_Pos)                          /*!< 0x00000008 */
#define RTC_TIME_STATUS_1SFG        RTC_TIME_STATUS_1SFG_Msk                                    /*!<RTC_TIME_STATUS_1SFG  */

#define RTC_TIME_STATUS_CYCFG_Pos   (4U)
#define RTC_TIME_STATUS_CYCFG_Msk   (0x1U << RTC_TIME_STATUS_CYCFG_Pos)                         /*!< 0x00000010 */
#define RTC_TIME_STATUS_CYCFG       RTC_TIME_STATUS_CYCFG_Msk                                   /*!<RTC_TIME_STATUS_CYCFG  */

#define RTC_TIME_STATUS_INVLDRD_Pos (16U)
#define RTC_TIME_STATUS_INVLDRD_Msk (0x1U << RTC_TIME_STATUS_INVLDRD_Pos)                       /*!< 0x00010000 */
#define RTC_TIME_STATUS_INVLDRD     RTC_TIME_STATUS_INVLDRD_Msk                                 /*!<RTC_TIME_STATUS_INVLDRD*/

#define RTC_TIME_STATUS_OVRD_Pos    (17U)
#define RTC_TIME_STATUS_OVRD_Msk    (0x1U << RTC_TIME_STATUS_OVRD_Pos)                          /*!< 0x00020000 */
#define RTC_TIME_STATUS_OVRD        RTC_TIME_STATUS_OVRD_Msk                                    /*!<RTC_TIME_STATUS_OVRD  */

#define RTC_TIME_STATUS_SECONDRD_Pos    (18U)
#define RTC_TIME_STATUS_SECONDRD_Msk    (0x1U << RTC_TIME_STATUS_SECONDRD_Pos)                  /*!< 0x00040000 */
#define RTC_TIME_STATUS_SECONDRD        RTC_TIME_STATUS_SECONDRD_Msk                            /*!<RTC_TIME_STATUS_SECONDRD  */

#define RTC_TIME_STATUS_1SRD_Pos    (19U)
#define RTC_TIME_STATUS_1SRD_Msk    (0x1U << RTC_TIME_STATUS_1SRD_Pos)                          /*!< 0x00080000 */
#define RTC_TIME_STATUS_1SRD        RTC_TIME_STATUS_1SRD_Msk                                    /*!<RTC_TIME_STATUS_1SRD  */

#define RTC_TIME_STATUS_CYCRD_Pos   (20U)
#define RTC_TIME_STATUS_CYCRD_Msk   (0x1U << RTC_TIME_STATUS_CYCRD_Pos)                         /*!< 0x00100000 */
#define RTC_TIME_STATUS_CYCRD       RTC_TIME_STATUS_CYCRD_Msk                                   /*!<RTC_TIME_STATUS_CYCRD  */

/*******************  Bit definition for RTC_VBAT_TIME_COMPENSATION STATUS register  ********************/
#define RTC_VBAT_COMPSTAUTS_VAL_Pos (16U)
#define RTC_VBAT_COMPSTAUTS_VAL_Msk (0x7FU << RTC_VBAT_COMPSTAUTS_VAL_Pos)                      /*!< 0x007F0000 */
#define RTC_VBAT_COMPSTAUTS_VAL     RTC_VBAT_COMPSTAUTS_VAL_Msk                                 /*!<RTC_VBAT_COMPSTAUTS_VAL  */

#define RTC_VBAT_COMPSTAUTS_SIGN_Pos    (23U)
#define RTC_VBAT_COMPSTAUTS_SIGN_Msk    (0x1U << RTC_VBAT_COMPSTAUTS_SIGN_Pos)                  /*!< 0x00000080 */
#define RTC_VBAT_COMPSTAUTS_SIGN        RTC_VBAT_COMPSTAUTS_SIGN_Msk                            /*!<RTC_VBAT_COMPSTAUTS_SIGN  */

#define RTC_VBAT_COMPSTAUTS_INVL_Pos    (24U)
#define RTC_VBAT_COMPSTAUTS_INVL_Msk    (0xFFU << RTC_VBAT_COMPSTAUTS_INVL_Pos)                 /*!< 0x0000FF00 */
#define RTC_VBAT_COMPSTAUTS_INVL        RTC_VBAT_COMPSTAUTS_INVL_Msk                            /*!<RTC_VBAT_COMPSTAUTS_INVL[7:0]  */

/*******************  Bit definition for RTC_VBAT_TIME_TAMPER_TIME_SECONDS register  ********************/
#define RTC_TIME_TAMTSEC_Pos    (0U)
#define RTC_TIME_TAMTSEC_Msk    (0xFFFFFFFFU << RTC_TIME_TAMTSEC_Pos)                           /*!< 0xFFFFFFFF */
#define RTC_TIME_TAMTSEC        RTC_TIME_TAMTSEC_Msk                                            /*!<RTC_TIME_TAMTSEC  */
//</h>

//<h>BKP_VMAIN
/******************************************************************************/
/*                                                                            */
/*                                    BKP_VMAIN                               */
/*                                                                            */
/******************************************************************************/
/*******************  Bit definition for BKP_VMAIN_TAMWA register  ********************/
#define BKP_VMAIN_TAMWA_TAME_Pos    (0U)
#define BKP_VMAIN_TAMWA_TAME_Msk    (0x1U << BKP_VMAIN_TAMWA_TAME_Pos)                  /*!< 0x00000001 */
#define BKP_VMAIN_TAMWA_TAME        BKP_VMAIN_TAMWA_TAME_Msk                            /*!<Tamper_en register write authority */

#define BKP_VMAIN_TAMWA_TAMAE_Pos   (1U)
#define BKP_VMAIN_TAMWA_TAMAE_Msk   (0x1U << BKP_VMAIN_TAMWA_TAMAE_Pos)                 /*!< 0x00000002 */
#define BKP_VMAIN_TAMWA_TAMAE       BKP_VMAIN_TAMWA_TAMAE_Msk                           /*!<Tamper_auto_en register write authority */

#define BKP_VMAIN_TAMWA_TAMAI0_Pos  (2U)
#define BKP_VMAIN_TAMWA_TAMAI0_Msk  (0x1U << BKP_VMAIN_TAMWA_TAMAI0_Pos)                /*!< 0x00000004 */
#define BKP_VMAIN_TAMWA_TAMAI0      BKP_VMAIN_TAMWA_TAMAI0_Msk                          /*!<Tamper_auto_interval0 register write authority */

#define BKP_VMAIN_TAMWA_TAMINTE_Pos (3U)
#define BKP_VMAIN_TAMWA_TAMINTE_Msk (0x1U << BKP_VMAIN_TAMWA_TAMINTE_Pos)               /*!< 0x00000008 */
#define BKP_VMAIN_TAMWA_TAMINTE     BKP_VMAIN_TAMWA_TAMINTE_Msk                         /*!<Tamper_int_en register write authority */

#define BKP_VMAIN_TAMWA_TAMCTRL_Pos (4U)
#define BKP_VMAIN_TAMWA_TAMCTRL_Msk (0x1U << BKP_VMAIN_TAMWA_TAMCTRL_Pos)               /*!< 0x00000010 */
#define BKP_VMAIN_TAMWA_TAMCTRL     BKP_VMAIN_TAMWA_TAMCTRL_Msk                         /*!<Tamper_control register write authority */

#define BKP_VMAIN_TAMWA_TAMSRST_Pos (5U)
#define BKP_VMAIN_TAMWA_TAMSRST_Msk (0x1U << BKP_VMAIN_TAMWA_TAMSRST_Pos)               /*!< 0x00000020 */
#define BKP_VMAIN_TAMWA_TAMSRST     BKP_VMAIN_TAMWA_TAMSRST_Msk                         /*!<Tamper_softrst register write authority */

#define BKP_VMAIN_TAMWA_TAMSTAT_Pos (6U)
#define BKP_VMAIN_TAMWA_TAMSTAT_Msk (0x1U << BKP_VMAIN_TAMWA_TAMSTAT_Pos)               /*!< 0x00000040 */
#define BKP_VMAIN_TAMWA_TAMSTAT     BKP_VMAIN_TAMWA_TAMSTAT_Msk                         /*!<Tamper_status register write authority */

#define BKP_VMAIN_TAMWA_TAMLOCK_Pos (7U)
#define BKP_VMAIN_TAMWA_TAMLOCK_Msk (0x1U << BKP_VMAIN_TAMWA_TAMLOCK_Pos)               /*!< 0x00000080 */
#define BKP_VMAIN_TAMWA_TAMLOCK     BKP_VMAIN_TAMWA_TAMLOCK_Msk                         /*!<Tamper_lock register write authority */

#define BKP_VMAIN_TAMWA_TAMTRIM_Pos (8U)
#define BKP_VMAIN_TAMWA_TAMTRIM_Msk (0x1U << BKP_VMAIN_TAMWA_TAMTRIM_Pos)               /*!< 0x00000100 */
#define BKP_VMAIN_TAMWA_TAMTRIM     BKP_VMAIN_TAMWA_TAMTRIM_Msk                         /*!<Tamper_trim register write authority */

#define BKP_VMAIN_TAMWA_TAMPCFG_Pos (9U)
#define BKP_VMAIN_TAMWA_TAMPCFG_Msk (0x1U << BKP_VMAIN_TAMWA_TAMPCFG_Pos)               /*!< 0x00000200 */
#define BKP_VMAIN_TAMWA_TAMPCFG     BKP_VMAIN_TAMWA_TAMPCFG_Msk                         /*!<BKP_PIN_cfg register write authority */

#define BKP_VMAIN_TAMWA_TAMPDIR_Pos (10U)
#define BKP_VMAIN_TAMWA_TAMPDIR_Msk (0x1U << BKP_VMAIN_TAMWA_TAMPDIR_Pos)               /*!< 0x00000400 */
#define BKP_VMAIN_TAMWA_TAMPDIR     BKP_VMAIN_TAMWA_TAMPDIR_Msk                         /*!<Tamper_pin_direction register write authority */

#define BKP_VMAIN_TAMWA_TAMPPOL_Pos (11U)
#define BKP_VMAIN_TAMWA_TAMPPOL_Msk (0x1U << BKP_VMAIN_TAMWA_TAMPPOL_Pos)               /*!< 0x00000800 */
#define BKP_VMAIN_TAMWA_TAMPPOL     BKP_VMAIN_TAMWA_TAMPPOL_Msk                         /*!<Tamper_pin_polarity register write authority */

#define BKP_VMAIN_TAMWA_TAMP0GF_Pos (12U)
#define BKP_VMAIN_TAMWA_TAMP0GF_Msk (0x1U << BKP_VMAIN_TAMWA_TAMP0GF_Pos)               /*!< 0x00001000 */
#define BKP_VMAIN_TAMWA_TAMP0GF     BKP_VMAIN_TAMWA_TAMP0GF_Msk                         /*!<Tamper_pin0_glitch_filter register write authority */

#define BKP_VMAIN_TAMWA_TAMP1GF_Pos (13U)
#define BKP_VMAIN_TAMWA_TAMP1GF_Msk (0x1U << BKP_VMAIN_TAMWA_TAMP1GF_Pos)               /*!< 0x00002000 */
#define BKP_VMAIN_TAMWA_TAMP1GF     BKP_VMAIN_TAMWA_TAMP1GF_Msk                         /*!<Tamper_pin1_glitch_filter register write authority */

#define BKP_VMAIN_TAMWA_TAMP2GF_Pos (14U)
#define BKP_VMAIN_TAMWA_TAMP2GF_Msk (0x1U << BKP_VMAIN_TAMWA_TAMP2GF_Pos)               /*!< 0x00004000 */
#define BKP_VMAIN_TAMWA_TAMP2GF     BKP_VMAIN_TAMWA_TAMP2GF_Msk                         /*!<Tamper_pin2_glitch_filter register write authority */

#define BKP_VMAIN_TAMWA_TAMP3GF_Pos (15U)
#define BKP_VMAIN_TAMWA_TAMP3GF_Msk (0x1U << BKP_VMAIN_TAMWA_TAMP3GF_Pos)               /*!< 0x00008000 */
#define BKP_VMAIN_TAMWA_TAMP3GF     BKP_VMAIN_TAMWA_TAMP3GF_Msk                         /*!<Tamper_pin3_glitch_filter register write authority */

#define BKP_VMAIN_TAMWA_TAMP4GF_Pos (16U)
#define BKP_VMAIN_TAMWA_TAMP4GF_Msk (0x1U << BKP_VMAIN_TAMWA_TAMP4GF_Pos)               /*!< 0x00010000 */
#define BKP_VMAIN_TAMWA_TAMP4GF     BKP_VMAIN_TAMWA_TAMP4GF_Msk                         /*!<Tamper_pin4_glitch_filter register write authority */

#define BKP_VMAIN_TAMWA_TAMP5GF_Pos (17U)
#define BKP_VMAIN_TAMWA_TAMP5GF_Msk (0x1U << BKP_VMAIN_TAMWA_TAMP5GF_Pos)               /*!< 0x00020000 */
#define BKP_VMAIN_TAMWA_TAMP5GF     BKP_VMAIN_TAMWA_TAMP5GF_Msk                         /*!<Tamper_pin5_glitch_filter register write authority */

#define BKP_VMAIN_TAMWA_TAMP6GF_Pos (18U)
#define BKP_VMAIN_TAMWA_TAMP6GF_Msk (0x1U << BKP_VMAIN_TAMWA_TAMP6GF_Pos)               /*!< 0x00040000 */
#define BKP_VMAIN_TAMWA_TAMP6GF     BKP_VMAIN_TAMWA_TAMP6GF_Msk                         /*!<Tamper_pin6_glitch_filter register write authority */

#define BKP_VMAIN_TAMWA_TAMP7GF_Pos (19U)
#define BKP_VMAIN_TAMWA_TAMP7GF_Msk (0x1U << BKP_VMAIN_TAMWA_TAMP7GF_Pos)               /*!< 0x00080000 */
#define BKP_VMAIN_TAMWA_TAMP7GF     BKP_VMAIN_TAMWA_TAMP7GF_Msk                         /*!<Tamper_pin7_glitch_filter register write authority */

#define BKP_VMAIN_TAMWA_ACTTAM0_Pos (20U)
#define BKP_VMAIN_TAMWA_ACTTAM0_Msk (0x1U << BKP_VMAIN_TAMWA_ACTTAM0_Pos)               /*!< 0x00100000 */
#define BKP_VMAIN_TAMWA_ACTTAM0     BKP_VMAIN_TAMWA_ACTTAM0_Msk                         /*!<Active_tamper0 register write authority */

#define BKP_VMAIN_TAMWA_ACTTAM1_Pos (21U)
#define BKP_VMAIN_TAMWA_ACTTAM1_Msk (0x1U << BKP_VMAIN_TAMWA_ACTTAM1_Pos)               /*!< 0x00200000 */
#define BKP_VMAIN_TAMWA_ACTTAM1     BKP_VMAIN_TAMWA_ACTTAM1_Msk                         /*!<Active_tamper1 register write authority */

#define BKP_VMAIN_TAMWA_SHIELD_Pos  (22U)
#define BKP_VMAIN_TAMWA_SHIELD_Msk  (0x1U << BKP_VMAIN_TAMWA_SHIELD_Pos)                /*!< 0x00400000 */
#define BKP_VMAIN_TAMWA_SHIELD      BKP_VMAIN_TAMWA_SHIELD_Msk                          /*!<Shield register write authority */

#define BKP_VMAIN_TAMWA_GPIOEN_Pos  (23U)
#define BKP_VMAIN_TAMWA_GPIOEN_Msk  (0x1U << BKP_VMAIN_TAMWA_GPIOEN_Pos)                /*!< 0x00800000 */
#define BKP_VMAIN_TAMWA_GPIOEN      BKP_VMAIN_TAMWA_GPIOEN_Msk                          /*!<GPIO en register write authority */

/*******************  Bit definition for BKP_VMAIN_TAMRA register  ********************/
#define BKP_VMAIN_TAMRA_TAME_Pos    (0U)
#define BKP_VMAIN_TAMRA_TAME_Msk    (0x1U << BKP_VMAIN_TAMRA_TAME_Pos)                  /*!< 0x00000001 */
#define BKP_VMAIN_TAMRA_TAME        BKP_VMAIN_TAMRA_TAME_Msk                            /*!<Tamper_en register read authority */

#define BKP_VMAIN_TAMRA_TAMAE_Pos   (1U)
#define BKP_VMAIN_TAMRA_TAMAE_Msk   (0x1U << BKP_VMAIN_TAMRA_TAMAE_Pos)                 /*!< 0x00000002 */
#define BKP_VMAIN_TAMRA_TAMAE       BKP_VMAIN_TAMRA_TAMAE_Msk                           /*!<Tamper_auto_en register read authority */

#define BKP_VMAIN_TAMRA_TAMAI0_Pos  (2U)
#define BKP_VMAIN_TAMRA_TAMAI0_Msk  (0x1U << BKP_VMAIN_TAMRA_TAMAI0_Pos)                /*!< 0x00000004 */
#define BKP_VMAIN_TAMRA_TAMAI0      BKP_VMAIN_TAMRA_TAMAI0_Msk                          /*!<Tamper_auto_interval0 register read authority */

#define BKP_VMAIN_TAMRA_TAMINTE_Pos (3U)
#define BKP_VMAIN_TAMRA_TAMINTE_Msk (0x1U << BKP_VMAIN_TAMRA_TAMINTE_Pos)               /*!< 0x00000008 */
#define BKP_VMAIN_TAMRA_TAMINTE     BKP_VMAIN_TAMRA_TAMINTE_Msk                         /*!<Tamper_int_en register read authority */

#define BKP_VMAIN_TAMRA_TAMCTRL_Pos (4U)
#define BKP_VMAIN_TAMRA_TAMCTRL_Msk (0x1U << BKP_VMAIN_TAMRA_TAMCTRL_Pos)               /*!< 0x00000010 */
#define BKP_VMAIN_TAMRA_TAMCTRL     BKP_VMAIN_TAMRA_TAMCTRL_Msk                         /*!<Tamper_control register read authority */

#define BKP_VMAIN_TAMRA_TAMSRST_Pos (5U)
#define BKP_VMAIN_TAMRA_TAMSRST_Msk (0x1U << BKP_VMAIN_TAMRA_TAMSRST_Pos)               /*!< 0x00000020 */
#define BKP_VMAIN_TAMRA_TAMSRST     BKP_VMAIN_TAMRA_TAMSRST_Msk                         /*!<Tamper_softrst register read authority */

#define BKP_VMAIN_TAMRA_TAMSTAT_Pos (6U)
#define BKP_VMAIN_TAMRA_TAMSTAT_Msk (0x1U << BKP_VMAIN_TAMRA_TAMSTAT_Pos)               /*!< 0x00000040 */
#define BKP_VMAIN_TAMRA_TAMSTAT     BKP_VMAIN_TAMRA_TAMSTAT_Msk                         /*!<Tamper_status register read authority */

#define BKP_VMAIN_TAMRA_TAMLOCK_Pos (7U)
#define BKP_VMAIN_TAMRA_TAMLOCK_Msk (0x1U << BKP_VMAIN_TAMRA_TAMLOCK_Pos)               /*!< 0x00000080 */
#define BKP_VMAIN_TAMRA_TAMLOCK     BKP_VMAIN_TAMRA_TAMLOCK_Msk                         /*!<Tamper_lock register read authority */

#define BKP_VMAIN_TAMRA_TAMTRIM_Pos (8U)
#define BKP_VMAIN_TAMRA_TAMTRIM_Msk (0x1U << BKP_VMAIN_TAMRA_TAMTRIM_Pos)               /*!< 0x00000100 */
#define BKP_VMAIN_TAMRA_TAMTRIM     BKP_VMAIN_TAMRA_TAMTRIM_Msk                         /*!<Tamper_trim register read authority */

#define BKP_VMAIN_TAMRA_TAMPCFG_Pos (9U)
#define BKP_VMAIN_TAMRA_TAMPCFG_Msk (0x1U << BKP_VMAIN_TAMRA_TAMPCFG_Pos)               /*!< 0x00000200 */
#define BKP_VMAIN_TAMRA_TAMPCFG     BKP_VMAIN_TAMRA_TAMPCFG_Msk                         /*!<Tamper_pin_cfg register read authority */

#define BKP_VMAIN_TAMRA_TAMPDIR_Pos (10U)
#define BKP_VMAIN_TAMRA_TAMPDIR_Msk (0x1U << BKP_VMAIN_TAMRA_TAMPDIR_Pos)               /*!< 0x00000400 */
#define BKP_VMAIN_TAMRA_TAMPDIR     BKP_VMAIN_TAMRA_TAMPDIR_Msk                         /*!<Tamper_pin_direction register read authority */

#define BKP_VMAIN_TAMRA_TAMPPOL_Pos (11U)
#define BKP_VMAIN_TAMRA_TAMPPOL_Msk (0x1U << BKP_VMAIN_TAMRA_TAMPPOL_Pos)               /*!< 0x00000800 */
#define BKP_VMAIN_TAMRA_TAMPPOL     BKP_VMAIN_TAMRA_TAMPPOL_Msk                         /*!<Tamper_pin_polarity register read authority */

#define BKP_VMAIN_TAMRA_TAMP0GF_Pos (12U)
#define BKP_VMAIN_TAMRA_TAMP0GF_Msk (0x1U << BKP_VMAIN_TAMRA_TAMP0GF_Pos)               /*!< 0x00001000 */
#define BKP_VMAIN_TAMRA_TAMP0GF     BKP_VMAIN_TAMRA_TAMP0GF_Msk                         /*!<Tamper_pin0_glitch_filter register read authority */

#define BKP_VMAIN_TAMRA_TAMP1GF_Pos (13U)
#define BKP_VMAIN_TAMRA_TAMP1GF_Msk (0x1U << BKP_VMAIN_TAMRA_TAMP1GF_Pos)               /*!< 0x00002000 */
#define BKP_VMAIN_TAMRA_TAMP1GF     BKP_VMAIN_TAMRA_TAMP1GF_Msk                         /*!<Tamper_pin1_glitch_filter register read authority */

#define BKP_VMAIN_TAMRA_TAMP2GF_Pos (14U)
#define BKP_VMAIN_TAMRA_TAMP2GF_Msk (0x1U << BKP_VMAIN_TAMRA_TAMP2GF_Pos)               /*!< 0x00004000 */
#define BKP_VMAIN_TAMRA_TAMP2GF     BKP_VMAIN_TAMRA_TAMP2GF_Msk                         /*!<Tamper_pin2_glitch_filter register read authority */

#define BKP_VMAIN_TAMRA_TAMP3GF_Pos (15U)
#define BKP_VMAIN_TAMRA_TAMP3GF_Msk (0x1U << BKP_VMAIN_TAMRA_TAMP3GF_Pos)               /*!< 0x00008000 */
#define BKP_VMAIN_TAMRA_TAMP3GF     BKP_VMAIN_TAMRA_TAMP3GF_Msk                         /*!<Tamper_pin3_glitch_filter register read authority */

#define BKP_VMAIN_TAMRA_TAMP4GF_Pos (16U)
#define BKP_VMAIN_TAMRA_TAMP4GF_Msk (0x1U << BKP_VMAIN_TAMRA_TAMP4GF_Pos)               /*!< 0x00010000 */
#define BKP_VMAIN_TAMRA_TAMP4GF     BKP_VMAIN_TAMRA_TAMP4GF_Msk                         /*!<Tamper_pin4_glitch_filter register read authority */

#define BKP_VMAIN_TAMRA_TAMP5GF_Pos (17U)
#define BKP_VMAIN_TAMRA_TAMP5GF_Msk (0x1U << BKP_VMAIN_TAMRA_TAMP5GF_Pos)               /*!< 0x00020000 */
#define BKP_VMAIN_TAMRA_TAMP5GF     BKP_VMAIN_TAMRA_TAMP5GF_Msk                         /*!<Tamper_pin5_glitch_filter register read authority */

#define BKP_VMAIN_TAMRA_TAMP6GF_Pos (18U)
#define BKP_VMAIN_TAMRA_TAMP6GF_Msk (0x1U << BKP_VMAIN_TAMRA_TAMP6GF_Pos)               /*!< 0x00040000 */
#define BKP_VMAIN_TAMRA_TAMP6GF     BKP_VMAIN_TAMRA_TAMP6GF_Msk                         /*!<Tamper_pin6_glitch_filter register read authority */

#define BKP_VMAIN_TAMRA_TAMP7GF_Pos (19U)
#define BKP_VMAIN_TAMRA_TAMP7GF_Msk (0x1U << BKP_VMAIN_TAMRA_TAMP7GF_Pos)               /*!< 0x00080000 */
#define BKP_VMAIN_TAMRA_TAMP7GF     BKP_VMAIN_TAMRA_TAMP7GF_Msk                         /*!<Tamper_pin7_glitch_filter register read authority */

#define BKP_VMAIN_TAMRA_ACTTAM0_Pos (20U)
#define BKP_VMAIN_TAMRA_ACTTAM0_Msk (0x1U << BKP_VMAIN_TAMRA_ACTTAM0_Pos)               /*!< 0x00100000 */
#define BKP_VMAIN_TAMRA_ACTTAM0     BKP_VMAIN_TAMRA_ACTTAM0_Msk                         /*!<Active_tamper0 register read authority */

#define BKP_VMAIN_TAMRA_ACTTAM1_Pos (21U)
#define BKP_VMAIN_TAMRA_ACTTAM1_Msk (0x1U << BKP_VMAIN_TAMRA_ACTTAM1_Pos)               /*!< 0x00200000 */
#define BKP_VMAIN_TAMRA_ACTTAM1     BKP_VMAIN_TAMRA_ACTTAM1_Msk                         /*!<Active_tamper1 register read authority */

#define BKP_VMAIN_TAMRA_SHIELD_Pos  (22U)
#define BKP_VMAIN_TAMRA_SHIELD_Msk  (0x1U << BKP_VMAIN_TAMRA_SHIELD_Pos)                /*!< 0x00400000 */
#define BKP_VMAIN_TAMRA_SHIELD      BKP_VMAIN_TAMRA_SHIELD_Msk                          /*!<Shield register read authority */

#define BKP_VMAIN_TAMRA_GPIOEN_Pos  (23U)
#define BKP_VMAIN_TAMRA_GPIOEN_Msk  (0x1U << BKP_VMAIN_TAMRA_GPIOEN_Pos)                /*!< 0x00800000 */
#define BKP_VMAIN_TAMRA_GPIOEN      BKP_VMAIN_TAMRA_GPIOEN_Msk                          /*!<GPIO en register read authority */

/*******************  Bit definition for BKP_VMAIN_KWA register  ********************/
#define BKP_VMAIN_KWA_KVWA_Pos  (0U)
#define BKP_VMAIN_KWA_KVWA_Msk  (0x1U << BKP_VMAIN_KWA_KVWA_Pos)                        /*!< 0x00000001 */
#define BKP_VMAIN_KWA_KVWA      BKP_VMAIN_KWA_KVWA_Msk                                  /*!<Key_valid register write authority */

#define BKP_VMAIN_KWA_KWLWA_Pos (1U)
#define BKP_VMAIN_KWA_KWLWA_Msk (0x1U << BKP_VMAIN_KWA_KWLWA_Pos)                       /*!< 0x00000002 */
#define BKP_VMAIN_KWA_KWLWA     BKP_VMAIN_KWA_KWLWA_Msk                                 /*!<Key_write_lock register write authority */

#define BKP_VMAIN_KWA_KRLWA_Pos (2U)
#define BKP_VMAIN_KWA_KRLWA_Msk (0x1U << BKP_VMAIN_KWA_KRLWA_Pos)                       /*!< 0x00000004 */
#define BKP_VMAIN_KWA_KRLWA     BKP_VMAIN_KWA_KRLWA_Msk                                 /*!<Key_read_lock register write authority */

#define BKP_VMAIN_KWA_KLWA_Pos  (3U)
#define BKP_VMAIN_KWA_KLWA_Msk  (0x1U << BKP_VMAIN_KWA_KLWA_Pos)                        /*!< 0x00000008 */
#define BKP_VMAIN_KWA_KLWA      BKP_VMAIN_KWA_KLWA_Msk                                  /*!<Key_lock register write authority */

#define BKP_VMAIN_KWA_KREG0_Pos (4U)
#define BKP_VMAIN_KWA_KREG0_Msk (0x1U << BKP_VMAIN_KWA_KREG0_Pos)                       /*!< 0x00000010 */
#define BKP_VMAIN_KWA_KREG0     BKP_VMAIN_KWA_KREG0_Msk                                 /*!<Key_regfile0 register write authority */

#define BKP_VMAIN_KWA_KREG1_Pos (5U)
#define BKP_VMAIN_KWA_KREG1_Msk (0x1U << BKP_VMAIN_KWA_KREG1_Pos)                       /*!< 0x00000020 */
#define BKP_VMAIN_KWA_KREG1     BKP_VMAIN_KWA_KREG1_Msk                                 /*!<Key_regfile1 register write authority */

#define BKP_VMAIN_KWA_KREG2_Pos (6U)
#define BKP_VMAIN_KWA_KREG2_Msk (0x1U << BKP_VMAIN_KWA_KREG2_Pos)                       /*!< 0x00000040 */
#define BKP_VMAIN_KWA_KREG2     BKP_VMAIN_KWA_KREG2_Msk                                 /*!<Key_regfile2 register write authority */

#define BKP_VMAIN_KWA_KREG3_Pos (7U)
#define BKP_VMAIN_KWA_KREG3_Msk (0x1U << BKP_VMAIN_KWA_KREG3_Pos)                       /*!< 0x00000080 */
#define BKP_VMAIN_KWA_KREG3     BKP_VMAIN_KWA_KREG3_Msk                                 /*!<Key_regfile3 register write authority */

#define BKP_VMAIN_KWA_KREG4_Pos (8U)
#define BKP_VMAIN_KWA_KREG4_Msk (0x1U << BKP_VMAIN_KWA_KREG4_Pos)                       /*!< 0x00000100 */
#define BKP_VMAIN_KWA_KREG4     BKP_VMAIN_KWA_KREG4_Msk                                 /*!<Key_regfile4 register write authority */

#define BKP_VMAIN_KWA_KREG5_Pos (9U)
#define BKP_VMAIN_KWA_KREG5_Msk (0x1U << BKP_VMAIN_KWA_KREG5_Pos)                       /*!< 0x00000200 */
#define BKP_VMAIN_KWA_KREG5     BKP_VMAIN_KWA_KREG5_Msk                                 /*!<Key_regfile5 register write authority */

#define BKP_VMAIN_KWA_KREG6_Pos (10U)
#define BKP_VMAIN_KWA_KREG6_Msk (0x1U << BKP_VMAIN_KWA_KREG6_Pos)                       /*!< 0x00000400 */
#define BKP_VMAIN_KWA_KREG6     BKP_VMAIN_KWA_KREG6_Msk                                 /*!<Key_regfile6 register write authority */

#define BKP_VMAIN_KWA_KREG7_Pos (11U)
#define BKP_VMAIN_KWA_KREG7_Msk (0x1U << BKP_VMAIN_KWA_KREG7_Pos)                       /*!< 0x00000800 */
#define BKP_VMAIN_KWA_KREG7     BKP_VMAIN_KWA_KREG7_Msk                                 /*!<Key_regfile7 register write authority */

#define BKP_VMAIN_KWA_NMREG_Pos (12U)
#define BKP_VMAIN_KWA_NMREG_Msk (0x1U << BKP_VMAIN_KWA_NMREG_Pos)                       /*!< 0x00001000 */
#define BKP_VMAIN_KWA_NMREG     BKP_VMAIN_KWA_NMREG_Msk                                 /*!<Normal_regfile register write authority */

/*******************  Bit definition for BKP_VMAIN_KRA register  ********************/
#define BKP_VMAIN_KRA_KVRA_Pos  (0U)
#define BKP_VMAIN_KRA_KVRA_Msk  (0x1U << BKP_VMAIN_KRA_KVRA_Pos)                        /*!< 0x00000001 */
#define BKP_VMAIN_KRA_KVRA      BKP_VMAIN_KRA_KVRA_Msk                                  /*!<Key_valid register read authority */

#define BKP_VMAIN_KRA_KWLRA_Pos (1U)
#define BKP_VMAIN_KRA_KWLRA_Msk (0x1U << BKP_VMAIN_KRA_KWLRA_Pos)                       /*!< 0x00000002 */
#define BKP_VMAIN_KRA_KWLRA     BKP_VMAIN_KRA_KWLRA_Msk                                 /*!<Key_read_lock register read authority */

#define BKP_VMAIN_KRA_KRLRA_Pos (2U)
#define BKP_VMAIN_KRA_KRLRA_Msk (0x1U << BKP_VMAIN_KRA_KRLRA_Pos)                       /*!< 0x00000004 */
#define BKP_VMAIN_KRA_KRLRA     BKP_VMAIN_KRA_KRLRA_Msk                                 /*!<Key_read_lock register read authority */

#define BKP_VMAIN_KRA_KLRA_Pos  (3U)
#define BKP_VMAIN_KRA_KLRA_Msk  (0x1U << BKP_VMAIN_KRA_KLRA_Pos)                        /*!< 0x00000008 */
#define BKP_VMAIN_KRA_KLRA      BKP_VMAIN_KRA_KLRA_Msk                                  /*!<Key_lock register read authority */

#define BKP_VMAIN_KRA_KREG0_Pos (4U)
#define BKP_VMAIN_KRA_KREG0_Msk (0x1U << BKP_VMAIN_KRA_KREG0_Pos)                       /*!< 0x00000010 */
#define BKP_VMAIN_KRA_KREG0     BKP_VMAIN_KRA_KREG0_Msk                                 /*!<Key_regfile0 register read authority */

#define BKP_VMAIN_KRA_KREG1_Pos (5U)
#define BKP_VMAIN_KRA_KREG1_Msk (0x1U << BKP_VMAIN_KRA_KREG1_Pos)                       /*!< 0x00000020 */
#define BKP_VMAIN_KRA_KREG1     BKP_VMAIN_KRA_KREG1_Msk                                 /*!<Key_regfile1 register read authority */

#define BKP_VMAIN_KRA_KREG2_Pos (6U)
#define BKP_VMAIN_KRA_KREG2_Msk (0x1U << BKP_VMAIN_KRA_KREG2_Pos)                       /*!< 0x00000040 */
#define BKP_VMAIN_KRA_KREG2     BKP_VMAIN_KRA_KREG2_Msk                                 /*!<Key_regfile2 register read authority */

#define BKP_VMAIN_KRA_KREG3_Pos (7U)
#define BKP_VMAIN_KRA_KREG3_Msk (0x1U << BKP_VMAIN_KRA_KREG3_Pos)                       /*!< 0x00000080 */
#define BKP_VMAIN_KRA_KREG3     BKP_VMAIN_KRA_KREG3_Msk                                 /*!<Key_regfile3 register read authority */

#define BKP_VMAIN_KRA_KREG4_Pos (8U)
#define BKP_VMAIN_KRA_KREG4_Msk (0x1U << BKP_VMAIN_KRA_KREG4_Pos)                       /*!< 0x00000100 */
#define BKP_VMAIN_KRA_KREG4     BKP_VMAIN_KRA_KREG4_Msk                                 /*!<Key_regfile4 register read authority */

#define BKP_VMAIN_KRA_KREG5_Pos (9U)
#define BKP_VMAIN_KRA_KREG5_Msk (0x1U << BKP_VMAIN_KRA_KREG5_Pos)                       /*!< 0x00000200 */
#define BKP_VMAIN_KRA_KREG5     BKP_VMAIN_KRA_KREG5_Msk                                 /*!<Key_regfile5 register read authority */

#define BKP_VMAIN_KRA_KREG6_Pos (10U)
#define BKP_VMAIN_KRA_KREG6_Msk (0x1U << BKP_VMAIN_KRA_KREG6_Pos)                       /*!< 0x00000400 */
#define BKP_VMAIN_KRA_KREG6     BKP_VMAIN_KRA_KREG6_Msk                                 /*!<Key_regfile6 register read authority */

#define BKP_VMAIN_KRA_KREG7_Pos (11U)
#define BKP_VMAIN_KRA_KREG7_Msk (0x1U << BKP_VMAIN_KRA_KREG7_Pos)                       /*!< 0x00000800 */
#define BKP_VMAIN_KRA_KREG7     BKP_VMAIN_KRA_KREG7_Msk                                 /*!<Key_regfile7 register read authority */

#define BKP_VMAIN_KRA_NMREG_Pos (12U)
#define BKP_VMAIN_KRA_NMREG_Msk (0x1U << BKP_VMAIN_KRA_NMREG_Pos)                       /*!< 0x00001000 */
#define BKP_VMAIN_KRA_NMREG     BKP_VMAIN_KRA_NMREG_Msk                                 /*!<Normal_regfile register read authority */

/*******************  Bit definition for BKP_VMAIN_AWA register  ********************/
#define BKP_VMAIN_AWA_ATRIM_Pos (0U)
#define BKP_VMAIN_AWA_ATRIM_Msk (0x1U << BKP_VMAIN_AWA_ATRIM_Pos)                       /*!< 0x00000001 */
#define BKP_VMAIN_AWA_ATRIM     BKP_VMAIN_AWA_ATRIM_Msk                                 /*!<Analog_trim register write authority */

#define BKP_VMAIN_AWA_AAUX_Pos  (1U)
#define BKP_VMAIN_AWA_AAUX_Msk  (0x1U << BKP_VMAIN_AWA_AAUX_Pos)                        /*!< 0x00000002 */
#define BKP_VMAIN_AWA_AAUX      BKP_VMAIN_AWA_AAUX_Msk                                  /*!<Analog_aux register write authority */

/*******************  Bit definition for BKP_VMAIN_ARA register  ********************/
#define BKP_VMAIN_ARA_ATRIM_Pos (0U)
#define BKP_VMAIN_ARA_ATRIM_Msk (0x1U << BKP_VMAIN_ARA_ATRIM_Pos)                       /*!< 0x00000001 */
#define BKP_VMAIN_ARA_ATRIM     BKP_VMAIN_ARA_ATRIM_Msk                                 /*!<Analog_trim register read authority */

#define BKP_VMAIN_ARA_AAUX_Pos  (1U)
#define BKP_VMAIN_ARA_AAUX_Msk  (0x1U << BKP_VMAIN_ARA_AAUX_Pos)                        /*!< 0x00000002 */
#define BKP_VMAIN_ARA_AAUX      BKP_VMAIN_ARA_AAUX_Msk                                  /*!<Analog_aux register read authority */

/*******************  Bit definition for BKP_VMAIN_ISOC register  ********************/
#define BKP_VMAIN_ISOC_BISON_Pos    (0U)
#define BKP_VMAIN_ISOC_BISON_Msk    (0x1U << BKP_VMAIN_ISOC_BISON_Pos)                  /*!< 0x00000001 */
#define BKP_VMAIN_ISOC_BISON        BKP_VMAIN_ISOC_BISON_Msk                            /*!<bat_iso_n */

#define BKP_VMAIN_ISOC_VMISOO_Pos   (1U)
#define BKP_VMAIN_ISOC_VMISOO_Msk   (0x1U << BKP_VMAIN_ISOC_VMISOO_Pos)                 /*!< 0x00000002 */
#define BKP_VMAIN_ISOC_VMISOO       BKP_VMAIN_ISOC_VMISOO_Msk                           /*!<Vmain_iso_open */

#define BKP_VMAIN_ISOC_BCRDY_Pos    (2U)
#define BKP_VMAIN_ISOC_BCRDY_Msk    (0x1U << BKP_VMAIN_ISOC_BCRDY_Pos)                  /*!< 0x00000004 */
#define BKP_VMAIN_ISOC_BCRDY        BKP_VMAIN_ISOC_BCRDY_Msk                            /*!<Bat_connect_ready */

/*******************  Bit definition for BKP_VMAIN_BATCC register  ********************/
#define BKP_VMAIN_BATCC_BATCC_Pos   (0U)
#define BKP_VMAIN_BATCC_BATCC_Msk   (0xFFU << BKP_VMAIN_BATCC_BATCC_Pos)                /*!< 0x000000FF */
#define BKP_VMAIN_BATCC_BATCC       BKP_VMAIN_BATCC_BATCC_Msk                           /*!<bat_connect_cnt[7:0] */

/*******************  Bit definition for BKP_VMAIN_TSTCLKEN register  ********************/
#define BKP_VMAIN_TSTCLKEN_Pos  (0U)
#define BKP_VMAIN_TSTCLKEN_Msk  (0x1U << BKP_VMAIN_TSTCLKEN_Pos)                        /*!< 0x00000001 */
#define BKP_VMAIN_TSTCLKEN      BKP_VMAIN_TSTCLKEN_Msk                                  /*!<Osc_4m_clk_en */

//</h>
//<h>BKP
/*******************  Bit definition for BKP_TAMPER_EN register  ********************/
#define BKP_TAMPER_EN_FDETCTE_Pos   (0U)
#define BKP_TAMPER_EN_FDETCTE_Msk   (0x1U << BKP_TAMPER_EN_FDETCTE_Pos)                 /*!< 0x00000001 */
#define BKP_TAMPER_EN_FDETCTE       BKP_TAMPER_EN_FDETCTE_Msk                           /*!<FREQ_DETECT_EN  */

#define BKP_TAMPER_EN_VDETCTE_Pos   (1U)
#define BKP_TAMPER_EN_VDETCTE_Msk   (0x1U << BKP_TAMPER_EN_VDETCTE_Pos)                 /*!< 0x00000002 */
#define BKP_TAMPER_EN_VDETCTE       BKP_TAMPER_EN_VDETCTE_Msk                           /*!<VOL_DETECT_EN  */

#define BKP_TAMPER_EN_GDETCTE_Pos   (2U)
#define BKP_TAMPER_EN_GDETCTE_Msk   (0x1U << BKP_TAMPER_EN_GDETCTE_Pos)                 /*!< 0x00000004 */
#define BKP_TAMPER_EN_GDETCTE       BKP_TAMPER_EN_GDETCTE_Msk                           /*!<GLITCH_DETECT_EN  */

#define BKP_TAMPER_EN_BGBUFE_Pos    (3U)
#define BKP_TAMPER_EN_BGBUFE_Msk    (0x1U << BKP_TAMPER_EN_BGBUFE_Pos)                  /*!< 0x00000008 */
#define BKP_TAMPER_EN_BGBUFE        BKP_TAMPER_EN_BGBUFE_Msk                            /*!<BKP_BGBUF_EN  */

#define BKP_TAMPER_EN_TDETCTE_Pos   (4U)
#define BKP_TAMPER_EN_TDETCTE_Msk   (0x1U << BKP_TAMPER_EN_TDETCTE_Pos)                 /*!< 0x00000010 */
#define BKP_TAMPER_EN_TDETCTE       BKP_TAMPER_EN_TDETCTE_Msk                           /*!<TAMPER_DETECT_EN  */

#define BKP_TAMPER_EN_BGSWME_Pos    (5U)
#define BKP_TAMPER_EN_BGSWME_Msk    (0x1U << BKP_TAMPER_EN_BGSWME_Pos)                  /*!< 0x00000020 */
#define BKP_TAMPER_EN_BGSWME        BKP_TAMPER_EN_BGSWME_Msk                            /*!<BG_SWITCHMODE_EN  */

#define BKP_TAMPER_EN_FILTE_Pos (6U)
#define BKP_TAMPER_EN_FILTE_Msk (0x1U << BKP_TAMPER_EN_FILTE_Pos)                       /*!< 0x00000040 */
#define BKP_TAMPER_EN_FILTE     BKP_TAMPER_EN_FILTE_Msk                                 /*!<FILTER_EN  */

#define BKP_TAMPER_EN_STESTE_Pos    (7U)
#define BKP_TAMPER_EN_STESTE_Msk    (0x1U << BKP_TAMPER_EN_STESTE_Pos)                  /*!< 0x00000080 */
#define BKP_TAMPER_EN_STESTE        BKP_TAMPER_EN_STESTE_Msk                            /*!<SELFTEST_EN  */

#define BKP_TAMPER_EN_PIN0DE_Pos    (8U)
#define BKP_TAMPER_EN_PIN0DE_Msk    (0x1U << BKP_TAMPER_EN_PIN0DE_Pos)                  /*!< 0x00000100 */
#define BKP_TAMPER_EN_PIN0DE        BKP_TAMPER_EN_PIN0DE_Msk                            /*!<TAMPER_PIN0_DETECT_EN  */

#define BKP_TAMPER_EN_PIN1DE_Pos    (9U)
#define BKP_TAMPER_EN_PIN1DE_Msk    (0x1U << BKP_TAMPER_EN_PIN1DE_Pos)                  /*!< 0x00000200 */
#define BKP_TAMPER_EN_PIN1DE        BKP_TAMPER_EN_PIN1DE_Msk                            /*!<TAMPER_PIN1_DETECT_EN  */

#define BKP_TAMPER_EN_PIN2DE_Pos    (10U)
#define BKP_TAMPER_EN_PIN2DE_Msk    (0x1U << BKP_TAMPER_EN_PIN2DE_Pos)                  /*!< 0x00000400 */
#define BKP_TAMPER_EN_PIN2DE        BKP_TAMPER_EN_PIN2DE_Msk                            /*!<TAMPER_PIN2_DETECT_EN  */

#define BKP_TAMPER_EN_PIN3DE_Pos    (11U)
#define BKP_TAMPER_EN_PIN3DE_Msk    (0x1U << BKP_TAMPER_EN_PIN3DE_Pos)                  /*!< 0x00000800 */
#define BKP_TAMPER_EN_PIN3DE        BKP_TAMPER_EN_PIN3DE_Msk                            /*!<TAMPER_PIN3_DETECT_EN  */

#define BKP_TAMPER_EN_PIN4DE_Pos    (12U)
#define BKP_TAMPER_EN_PIN4DE_Msk    (0x1U << BKP_TAMPER_EN_PIN4DE_Pos)                  /*!< 0x00001000 */
#define BKP_TAMPER_EN_PIN4DE        BKP_TAMPER_EN_PIN4DE_Msk                            /*!<TAMPER_PIN4_DETECT_EN  */

#define BKP_TAMPER_EN_PIN5DE_Pos    (13U)
#define BKP_TAMPER_EN_PIN5DE_Msk    (0x1U << BKP_TAMPER_EN_PIN5DE_Pos)                  /*!< 0x00002000 */
#define BKP_TAMPER_EN_PIN5DE        BKP_TAMPER_EN_PIN5DE_Msk                            /*!<TAMPER_PIN5_DETECT_EN  */

#define BKP_TAMPER_EN_PIN6DE_Pos    (14U)
#define BKP_TAMPER_EN_PIN6DE_Msk    (0x1U << BKP_TAMPER_EN_PIN6DE_Pos)                  /*!< 0x00004000 */
#define BKP_TAMPER_EN_PIN6DE        BKP_TAMPER_EN_PIN6DE_Msk                            /*!<TAMPER_PIN6_DETECT_EN  */

#define BKP_TAMPER_EN_PIN7DE_Pos    (15U)
#define BKP_TAMPER_EN_PIN7DE_Msk    (0x1U << BKP_TAMPER_EN_PIN7DE_Pos)                  /*!< 0x00008000 */
#define BKP_TAMPER_EN_PIN7DE        BKP_TAMPER_EN_PIN7DE_Msk                            /*!<TAMPER_PIN7_DETECT_EN  */

#define BKP_TAMPER_EN_SDETCTE_Pos   (16U)
#define BKP_TAMPER_EN_SDETCTE_Msk   (0x1U << BKP_TAMPER_EN_SDETCTE_Pos)                 /*!< 0x00010000 */
#define BKP_TAMPER_EN_SDETCTE       BKP_TAMPER_EN_SDETCTE_Msk                           /*!<SHIELD_DETECT_EN  */

#define BKP_TAMPER_EN_RSTGALM_Pos   (17U)
#define BKP_TAMPER_EN_RSTGALM_Msk   (0x1U << BKP_TAMPER_EN_RSTGALM_Pos)                 /*!< 0x00020000 */
#define BKP_TAMPER_EN_RSTGALM       BKP_TAMPER_EN_RSTGALM_Msk                           /*!<RST_GLITCH_ALARM  */

/*******************  Bit definition for BKP_TAMPER_AUTOEN register  ********************/
#define BKP_TAMPER_AUTOEN_VDAE_Pos  (0U)
#define BKP_TAMPER_AUTOEN_VDAE_Msk  (0x1U << BKP_TAMPER_AUTOEN_VDAE_Pos)                /*!< 0x00000001 */
#define BKP_TAMPER_AUTOEN_VDAE      BKP_TAMPER_AUTOEN_VDAE_Msk                          /*!<VOL_DETECT_AUTO_EN  */

/*******************  Bit definition for BKP_TAMPER_AUTOINVL0 register  ********************/
#define BKP_AUTOINVL0_INTERVAL_Pos  (0U)
#define BKP_AUTOINVL0_INTERVAL_Msk  (0xFFFFFFU << BKP_AUTOINVL0_INTERVAL_Pos)           /*!< 0x00FFFFFF */
#define BKP_AUTOINVL0_INTERVAL      BKP_AUTOINVL0_INTERVAL_Msk                          /*!<BKP_AUTOINVL0_INTERVAL  */

#define BKP_AUTOINVL0_VDET_INTERVAL_Pos (24U)
#define BKP_AUTOINVL0_VDET_INTERVAL_Msk (0xFFU << BKP_AUTOINVL0_VDET_INTERVAL_Pos)      /*!< 0xFF000000 */
#define BKP_AUTOINVL0_VDET_INTERVAL     BKP_AUTOINVL0_VDET_INTERVAL_Msk                 /*!<BKP_AUTOINVL0_VDET_INTERVAL  */

/*******************  Bit definition for BKP_TAMPER_INTEN register  ********************/
#define BKP_TAMPER_INTEN_Pos    (0U)
#define BKP_TAMPER_INTEN_Msk    (0x1U << BKP_TAMPER_INTEN_Pos)                          /*!< 0x00000001 */
#define BKP_TAMPER_INTEN        BKP_TAMPER_INTEN_Msk                                    /*!<BKP_INT_EN  */

#define BKP_FREQ_INTEN_Pos  (1U)
#define BKP_FREQ_INTEN_Msk  (0x1U << BKP_FREQ_INTEN_Pos)                                /*!< 0x00000002 */
#define BKP_FREQ_INTEN      BKP_FREQ_INTEN_Msk                                          /*!<BKP_FREQ_INT_EN  */

#define BKP_VOL_INTEN_Pos   (2U)
#define BKP_VOL_INTEN_Msk   (0x1U << BKP_VOL_INTEN_Pos)                                 /*!< 0x00000004 */
#define BKP_VOL_INTEN       BKP_VOL_INTEN_Msk                                           /*!<BKP_VOL_INT_EN  */

#define BKP_TEMP_INTEN_Pos  (3U)
#define BKP_TEMP_INTEN_Msk  (0x1U << BKP_TEMP_INTEN_Pos)                                /*!< 0x00000008 */
#define BKP_TEMP_INTEN      BKP_TEMP_INTEN_Msk                                          /*!<BKP_TEMP_INT_EN  */

#define BKP_GLITCH_INTEN_Pos    (4U)
#define BKP_GLITCH_INTEN_Msk    (0x1U << BKP_GLITCH_INTEN_Pos)                          /*!< 0x00000010 */
#define BKP_GLITCH_INTEN        BKP_GLITCH_INTEN_Msk                                    /*!<BKP_GLITCH_INT_EN  */

#define BKP_PIN0_TAMPER_INTEN_Pos   (5U)
#define BKP_PIN0_TAMPER_INTEN_Msk   (0x1U << BKP_PIN0_TAMPER_INTEN_Pos)                 /*!< 0x00000020 */
#define BKP_PIN0_TAMPER_INTEN       BKP_PIN0_TAMPER_INTEN_Msk                           /*!<BKP_PIN0_TAMPER_INT_EN  */

#define BKP_PIN1_TAMPER_INTEN_Pos   (6U)
#define BKP_PIN1_TAMPER_INTEN_Msk   (0x1U << BKP_PIN1_TAMPER_INTEN_Pos)                 /*!< 0x00000040 */
#define BKP_PIN1_TAMPER_INTEN       BKP_PIN1_TAMPER_INTEN_Msk                           /*!<BKP_PIN1_TAMPER_INT_EN  */

#define BKP_PIN2_TAMPER_INTEN_Pos   (7U)
#define BKP_PIN2_TAMPER_INTEN_Msk   (0x1U << BKP_PIN2_TAMPER_INTEN_Pos)                 /*!< 0x00000080 */
#define BKP_PIN2_TAMPER_INTEN       BKP_PIN2_TAMPER_INTEN_Msk                           /*!<BKP_PIN2_TAMPER_INT_EN  */

#define BKP_PIN3_TAMPER_INTEN_Pos   (8U)
#define BKP_PIN3_TAMPER_INTEN_Msk   (0x1U << BKP_PIN3_TAMPER_INTEN_Pos)                 /*!< 0x00000100 */
#define BKP_PIN3_TAMPER_INTEN       BKP_PIN3_TAMPER_INTEN_Msk                           /*!<BKP_PIN0_TAMPER_INT_EN  */

#define BKP_PIN4_TAMPER_INTEN_Pos   (9U)
#define BKP_PIN4_TAMPER_INTEN_Msk   (0x1U << BKP_PIN4_TAMPER_INTEN_Pos)                 /*!< 0x00000200 */
#define BKP_PIN4_TAMPER_INTEN       BKP_PIN4_TAMPER_INTEN_Msk                           /*!<BKP_PIN4_TAMPER_INT_EN  */

#define BKP_PIN5_TAMPER_INTEN_Pos   (10U)
#define BKP_PIN5_TAMPER_INTEN_Msk   (0x1U << BKP_PIN5_TAMPER_INTEN_Pos)                 /*!< 0x00000400 */
#define BKP_PIN5_TAMPER_INTEN       BKP_PIN5_TAMPER_INTEN_Msk                           /*!<BKP_PIN5_TAMPER_INT_EN  */

#define BKP_PIN6_TAMPER_INTEN_Pos   (11U)
#define BKP_PIN6_TAMPER_INTEN_Msk   (0x1U << BKP_PIN6_TAMPER_INTEN_Pos)                 /*!< 0x00000800 */
#define BKP_PIN6_TAMPER_INTEN       BKP_PIN6_TAMPER_INTEN_Msk                           /*!<BKP_PIN6_TAMPER_INT_EN  */

#define BKP_PIN7_TAMPER_INTEN_Pos   (12U)
#define BKP_PIN7_TAMPER_INTEN_Msk   (0x1U << BKP_PIN7_TAMPER_INTEN_Pos)                 /*!< 0x00001000 */
#define BKP_PIN7_TAMPER_INTEN       BKP_PIN7_TAMPER_INTEN_Msk                           /*!<BKP_PIN7_TAMPER_INT_EN  */

#define BKP_SHIELD_INTEN_Pos    (13U)
#define BKP_SHIELD_INTEN_Msk    (0x1U << BKP_SHIELD_INTEN_Pos)                          /*!< 0x00002000 */
#define BKP_SHIELD_INTEN        BKP_SHIELD_INTEN_Msk                                    /*!<BKP_SHIELD_INT_EN  */

#define BKP_ST_FREQ_INTEN_Pos   (14U)
#define BKP_ST_FREQ_INTEN_Msk   (0x1U << BKP_ST_FREQ_INTEN_Pos)                         /*!< 0x00004000 */
#define BKP_ST_FREQ_INTEN       BKP_ST_FREQ_INTEN_Msk                                   /*!<BKP_ST_FREQ_INT_EN  */

#define BKP_ST_VOL_INTEN_Pos    (15U)
#define BKP_ST_VOL_INTEN_Msk    (0x1U << BKP_ST_VOL_INTEN_Pos)                          /*!< 0x00008000 */
#define BKP_ST_VOL_INTEN        BKP_ST_VOL_INTEN_Msk                                    /*!<BKP_ST_VOL_INT_EN  */

#define BKP_ST_TEMP_INTEN_Pos   (16U)
#define BKP_ST_TEMP_INTEN_Msk   (0x1U << BKP_ST_TEMP_INTEN_Pos)                         /*!< 0x00010000 */
#define BKP_ST_TEMP_INTEN       BKP_ST_TEMP_INTEN_Msk                                   /*!<BKP_ST_TEMP_INT_EN  */

#define BKP_ST_GLITCH_INTEN_Pos (17U)
#define BKP_ST_GLITCH_INTEN_Msk (0x1U << BKP_ST_GLITCH_INTEN_Pos)                       /*!< 0x00020000 */
#define BKP_ST_GLITCH_INTEN     BKP_ST_GLITCH_INTEN_Msk                                 /*!<BKP_ST_GLITCH_INT_EN  */

/*******************  Bit definition for BKP_TAMPER_CONTROL register  ********************/
#define BKP_TAMPER_CHIPRST_REQ_Pos  (1U)
#define BKP_TAMPER_CHIPRST_REQ_Msk  (0x1U << BKP_TAMPER_CHIPRST_REQ_Pos)                /*!< 0x00000002 */
#define BKP_TAMPER_CHIPRST_REQ      BKP_TAMPER_CHIPRST_REQ_Msk                          /*!<BKP_TAMPER_CHIPRST_REQ  */

#define BKP_NORMAL_REGFILE_AUTH_Pos (2U)
#define BKP_NORMAL_REGFILE_AUTH_Msk (0x1U << BKP_NORMAL_REGFILE_AUTH_Pos)               /*!< 0x00000004 */
#define BKP_NORMAL_REGFILE_AUTH     BKP_NORMAL_REGFILE_AUTH_Msk                         /*!<BKP_NORMAL_REGFILE_AUTH  */

/*******************  Bit definition for BKP_TAMPER_SOFTRST register  ********************/
#define BKP_TAMPER_SOFTRST_Pos  (0U)
#define BKP_TAMPER_SOFTRST_Msk  (0xFFFFFFFFU << BKP_TAMPER_SOFTRST_Pos)                 /*!< 0xFFFFFFFF */
#define BKP_TAMPER_SOFTRST      BKP_TAMPER_SOFTRST_Msk                                  /*!<BKP_TAMPER_SOFTRST  */

/*******************  Bit definition for BKP_TAMPER_STATUS register  ********************/
#define BKP_TAMPER_FLAG_Pos (0U)
#define BKP_TAMPER_FLAG_Msk (0x1U << BKP_TAMPER_FLAG_Pos)                               /*!< 0x00000001 */
#define BKP_TAMPER_FLAG     BKP_TAMPER_FLAG_Msk                                         /*!<BKP_TAMPER_FLAG  */

#define BKP_TAMPER_ACK_FLAG_Pos (1U)
#define BKP_TAMPER_ACK_FLAG_Msk (0x1U << BKP_TAMPER_ACK_FLAG_Pos)                       /*!< 0x00000002 */
#define BKP_TAMPER_ACK_FLAG     BKP_TAMPER_ACK_FLAG_Msk                                 /*!<BKP_TAMPER_ACK_FLAG  */

#define BKP_FREQ_FLAG_Pos   (2U)
#define BKP_FREQ_FLAG_Msk   (0x1U << BKP_FREQ_FLAG_Pos)                                 /*!< 0x00000004 */
#define BKP_FREQ_FLAG       BKP_FREQ_FLAG_Msk                                           /*!<BKP_FREQ_FLAG  */

#define BKP_VOL_FLAG_Pos    (3U)
#define BKP_VOL_FLAG_Msk    (0x1U << BKP_VOL_FLAG_Pos)                                  /*!< 0x00000008 */
#define BKP_VOL_FLAG        BKP_VOL_FLAG_Msk                                            /*!<BKP_VOL_FLAG  */

#define BKP_TEMP_FLAG_Pos   (4U)
#define BKP_TEMP_FLAG_Msk   (0x1U << BKP_TEMP_FLAG_Pos)                                 /*!< 0x00000010 */
#define BKP_TEMP_FLAG       BKP_TEMP_FLAG_Msk                                           /*!<BKP_TEMP_FLAG  */

#define BKP_GLITCH_FLAG_Pos (5U)
#define BKP_GLITCH_FLAG_Msk (0x1U << BKP_GLITCH_FLAG_Pos)                               /*!< 0x00000020 */
#define BKP_GLITCH_FLAG     BKP_GLITCH_FLAG_Msk                                         /*!<BKP_GLITCH_FLAG  */

#define BKP_PIN0_TAMPER_FLAG_Pos    (6U)
#define BKP_PIN0_TAMPER_FLAG_Msk    (0x1U << BKP_PIN0_TAMPER_FLAG_Pos)                  /*!< 0x00000040 */
#define BKP_PIN0_TAMPER_FLAG        BKP_PIN0_TAMPER_FLAG_Msk                            /*!<BKP_PIN0_TAMPER_FLAG  */

#define BKP_PIN1_TAMPER_FLAG_Pos    (7U)
#define BKP_PIN1_TAMPER_FLAG_Msk    (0x1U << BKP_PIN1_TAMPER_FLAG_Pos)                  /*!< 0x00000080 */
#define BKP_PIN1_TAMPER_FLAG        BKP_PIN1_TAMPER_FLAG_Msk                            /*!<BKP_PIN1_TAMPER_FLAG  */

#define BKP_PIN2_TAMPER_FLAG_Pos    (8U)
#define BKP_PIN2_TAMPER_FLAG_Msk    (0x1U << BKP_PIN2_TAMPER_FLAG_Pos)                  /*!< 0x00000100 */
#define BKP_PIN2_TAMPER_FLAG        BKP_PIN2_TAMPER_FLAG_Msk                            /*!<BKP_PIN2_TAMPER_FLAG  */

#define BKP_PIN3_TAMPER_FLAG_Pos    (9U)
#define BKP_PIN3_TAMPER_FLAG_Msk    (0x1U << BKP_PIN3_TAMPER_FLAG_Pos)                  /*!< 0x00000200 */
#define BKP_PIN3_TAMPER_FLAG        BKP_PIN3_TAMPER_FLAG_Msk                            /*!<BKP_PIN3_TAMPER_FLAG  */

#define BKP_PIN4_TAMPER_FLAG_Pos    (10U)
#define BKP_PIN4_TAMPER_FLAG_Msk    (0x1U << BKP_PIN4_TAMPER_FLAG_Pos)                  /*!< 0x00000400 */
#define BKP_PIN4_TAMPER_FLAG        BKP_PIN4_TAMPER_FLAG_Msk                            /*!<BKP_PIN4_TAMPER_FLAG  */

#define BKP_PIN5_TAMPER_FLAG_Pos    (11U)
#define BKP_PIN5_TAMPER_FLAG_Msk    (0x1U << BKP_PIN5_TAMPER_FLAG_Pos)                  /*!< 0x00000800 */
#define BKP_PIN5_TAMPER_FLAG        BKP_PIN5_TAMPER_FLAG_Msk                            /*!<BKP_PIN5_TAMPER_FLAG  */

#define BKP_PIN6_TAMPER_FLAG_Pos    (12U)
#define BKP_PIN6_TAMPER_FLAG_Msk    (0x1U << BKP_PIN6_TAMPER_FLAG_Pos)                  /*!< 0x00001000 */
#define BKP_PIN6_TAMPER_FLAG        BKP_PIN6_TAMPER_FLAG_Msk                            /*!<BKP_PIN6_TAMPER_FLAG  */

#define BKP_PIN7_TAMPER_FLAG_Pos    (13U)
#define BKP_PIN7_TAMPER_FLAG_Msk    (0x1U << BKP_PIN7_TAMPER_FLAG_Pos)                  /*!< 0x00002000 */
#define BKP_PIN7_TAMPER_FLAG        BKP_PIN7_TAMPER_FLAG_Msk                            /*!<BKP_PIN7_TAMPER_FLAG  */

#define BKP_SHIELD_FLAG_Pos (14U)
#define BKP_SHIELD_FLAG_Msk (0x1U << BKP_SHIELD_FLAG_Pos)                               /*!< 0x00004000 */
#define BKP_SHIELD_FLAG     BKP_SHIELD_FLAG_Msk                                         /*!<BKP_SHIELD_FLAG  */

#define BKP_ST_FREQ_FLAG_Pos    (15U)
#define BKP_ST_FREQ_FLAG_Msk    (0x1U << BKP_ST_FREQ_FLAG_Pos)                          /*!< 0x00008000 */
#define BKP_ST_FREQ_FLAG        BKP_ST_FREQ_FLAG_Msk                                    /*!<BKP_ST_FREQ_FLAG  */

#define BKP_ST_VOL_FLAG_Pos (16U)
#define BKP_ST_VOL_FLAG_Msk (0x1U << BKP_ST_VOL_FLAG_Pos)                               /*!< 0x00010000 */
#define BKP_ST_VOL_FLAG     BKP_ST_VOL_FLAG_Msk                                         /*!<BKP_ST_VOL_FLAG  */

#define BKP_ST_TEMP_FLAG_Pos    (17U)
#define BKP_ST_TEMP_FLAG_Msk    (0x1U << BKP_ST_TEMP_FLAG_Pos)                          /*!< 0x00020000 */
#define BKP_ST_TEMP_FLAG        BKP_ST_TEMP_FLAG_Msk                                    /*!<BKP_ST_TEMP_FLAG  */

#define BKP_ST_GLITCH_FLAG_Pos  (18U)
#define BKP_ST_GLITCH_FLAG_Msk  (0x1U << BKP_ST_GLITCH_FLAG_Pos)                        /*!< 0x00040000 */
#define BKP_ST_GLITCH_FLAG      BKP_ST_GLITCH_FLAG_Msk                                  /*!<BKP_ST_GLITCH_FLAG  */

#define BKP_STOP_FDET_STATUS_Pos    (20U)
#define BKP_STOP_FDET_STATUS_Msk    (0x1U << BKP_STOP_FDET_STATUS_Pos)                  /*!< 0x00100000 */
#define BKP_STOP_FDET_STATUS        BKP_STOP_FDET_STATUS_Msk                            /*!<BKP_STOP_FDET_STATUS  */

#define BKP_LOW_FDET_STATUS_Pos (21U)
#define BKP_LOW_FDET_STATUS_Msk (0x1U << BKP_LOW_FDET_STATUS_Pos)                       /*!< 0x00200000 */
#define BKP_LOW_FDET_STATUS     BKP_LOW_FDET_STATUS_Msk                                 /*!<BKP_LOW_FDET_STATUS  */

#define BKP_HIGH_FDET_STATUS_Pos    (22U)
#define BKP_HIGH_FDET_STATUS_Msk    (0x1U << BKP_HIGH_FDET_STATUS_Pos)                  /*!< 0x00400000 */
#define BKP_HIGH_FDET_STATUS        BKP_HIGH_FDET_STATUS_Msk                            /*!<BKP_HIGH_FDET_STATUS  */

#define BKP_LOW_VOL_STATUS_Pos  (23U)
#define BKP_LOW_VOL_STATUS_Msk  (0x1U << BKP_LOW_VOL_STATUS_Pos)                        /*!< 0x00800000 */
#define BKP_LOW_VOL_STATUS      BKP_LOW_VOL_STATUS_Msk                                  /*!<BKP_LOW_VOL_STATUS  */

#define BKP_HIGH_VOL_STATUS_Pos (24U)
#define BKP_HIGH_VOL_STATUS_Msk (0x1U << BKP_HIGH_VOL_STATUS_Pos)                       /*!< 0x01000000 */
#define BKP_HIGH_VOL_STATUS     BKP_HIGH_VOL_STATUS_Msk                                 /*!<BKP_HIGH_VOL_STATUS  */

#define BKP_LOW_TEMP_STATUS_Pos (25U)
#define BKP_LOW_TEMP_STATUS_Msk (0x1U << BKP_LOW_TEMP_STATUS_Pos)                       /*!< 0x02000000 */
#define BKP_LOW_TEMP_STATUS     BKP_LOW_TEMP_STATUS_Msk                                 /*!<BKP_LOW_TEMP_STATUS  */

#define BKP_HIGH_TEMP_STATUS_Pos    (26U)
#define BKP_HIGH_TEMP_STATUS_Msk    (0x1U << BKP_HIGH_TEMP_STATUS_Pos)                  /*!< 0x04000000 */
#define BKP_HIGH_TEMP_STATUS        BKP_HIGH_TEMP_STATUS_Msk                            /*!<BKP_HIGH_TEMP_STATUS  */

/*******************  Bit definition for BKP_TAMPER_LOCK register  ********************/
#define BKP_TAMPER_EN_LOCK_Pos  (0U)
#define BKP_TAMPER_EN_LOCK_Msk  (0x1U << BKP_TAMPER_EN_LOCK_Pos)                        /*!< 0x00000001 */
#define BKP_TAMPER_EN_LOCK      BKP_TAMPER_EN_LOCK_Msk                                  /*!<BKP_TAMPER_EN_LOCK  */

#define BKP_AUTO_EN_LOCK_Pos    (1U)
#define BKP_AUTO_EN_LOCK_Msk    (0x1U << BKP_AUTO_EN_LOCK_Pos)                          /*!< 0x00000002 */
#define BKP_AUTO_EN_LOCK        BKP_AUTO_EN_LOCK_Msk                                    /*!<BKP_AUTO_EN_LOCK  */

#define BKP_AUTO_INVL0_LOCK_Pos (2U)
#define BKP_AUTO_INVL0_LOCK_Msk (0x1U << BKP_AUTO_INVL0_LOCK_Pos)                       /*!< 0x00000004 */
#define BKP_AUTO_INVL0_LOCK     BKP_AUTO_INVL0_LOCK_Msk                                 /*!<BKP_AUTO_INTVERVAL0_LOCK  */

#define BKP_INT_EN_LOCK_Pos (3U)
#define BKP_INT_EN_LOCK_Msk (0x1U << BKP_INT_EN_LOCK_Pos)                               /*!< 0x00000008 */
#define BKP_INT_EN_LOCK     BKP_INT_EN_LOCK_Msk                                         /*!<BKP_INT_EN_LOCK  */

#define BKP_CONTROL_LOCK_Pos    (4U)
#define BKP_CONTROL_LOCK_Msk    (0x1U << BKP_CONTROL_LOCK_Pos)                          /*!< 0x00000010 */
#define BKP_CONTROL_LOCK        BKP_CONTROL_LOCK_Msk                                    /*!<BKP_CONTROL_LOCK  */

#define BKP_SOFTRST_LOCK_Pos    (5U)
#define BKP_SOFTRST_LOCK_Msk    (0x1U << BKP_SOFTRST_LOCK_Pos)                          /*!< 0x00000020 */
#define BKP_SOFTRST_LOCK        BKP_SOFTRST_LOCK_Msk                                    /*!<BKP_SOFTRST_LOCK  */

#define BKP_STATUS_LOCK_Pos (6U)
#define BKP_STATUS_LOCK_Msk (0x1U << BKP_STATUS_LOCK_Pos)                               /*!< 0x00000040 */
#define BKP_STATUS_LOCK     BKP_STATUS_LOCK_Msk                                         /*!<BKP_STATUS_LOCK  */

#define BKP_LOCK_LOCK_Pos   (7U)
#define BKP_LOCK_LOCK_Msk   (0x1U << BKP_LOCK_LOCK_Pos)                                 /*!< 0x00000080 */
#define BKP_LOCK_LOCK       BKP_LOCK_LOCK_Msk                                           /*!<BKP_LOCK_LOCK  */

#define BKP_TRIM_LOCK_Pos   (8U)
#define BKP_TRIM_LOCK_Msk   (0x1U << BKP_TRIM_LOCK_Pos)                                 /*!< 0x00000100 */
#define BKP_TRIM_LOCK       BKP_TRIM_LOCK_Msk                                           /*!<BKP_TRIM_LOCK  */

#define BKP_PIN_CFG_LOCK_Pos    (9U)
#define BKP_PIN_CFG_LOCK_Msk    (0x1U << BKP_PIN_CFG_LOCK_Pos)                          /*!< 0x00000200 */
#define BKP_PIN_CFG_LOCK        BKP_PIN_CFG_LOCK_Msk                                    /*!<BKP_PIN_CFG_LOCK  */

#define BKP_PIN_DIR_LOCK_Pos    (10U)
#define BKP_PIN_DIR_LOCK_Msk    (0x1U << BKP_PIN_DIR_LOCK_Pos)                          /*!< 0x00000400 */
#define BKP_PIN_DIR_LOCK        BKP_PIN_DIR_LOCK_Msk                                    /*!<BKP_PIN_DIRECTION_LOCK  */

#define BKP_PIN_POLARITY_LOCK_Pos   (11U)
#define BKP_PIN_POLARITY_LOCK_Msk   (0x1U << BKP_PIN_POLARITY_LOCK_Pos)                 /*!< 0x00000800 */
#define BKP_PIN_POLARITY_LOCK       BKP_PIN_POLARITY_LOCK_Msk                           /*!<BKP_PIN_POLARITY_LOCK  */

#define BKP_PIN0_GFILT_LOCK_Pos (12U)
#define BKP_PIN0_GFILT_LOCK_Msk (0x1U << BKP_PIN0_GFILT_LOCK_Pos)                       /*!< 0x00001000 */
#define BKP_PIN0_GFILT_LOCK     BKP_PIN0_GFILT_LOCK_Msk                                 /*!<BKP_PIN0_GLITCH_FILTER_LOCK  */

#define BKP_PIN1_GFILT_LOCK_Pos (13U)
#define BKP_PIN1_GFILT_LOCK_Msk (0x1U << BKP_PIN1_GFILT_LOCK_Pos)                       /*!< 0x00002000 */
#define BKP_PIN1_GFILT_LOCK     BKP_PIN1_GFILT_LOCK_Msk                                 /*!<BKP_PIN1_GLITCH_FILTER_LOCK  */

#define BKP_PIN2_GFILT_LOCK_Pos (14U)
#define BKP_PIN2_GFILT_LOCK_Msk (0x1U << BKP_PIN2_GFILT_LOCK_Pos)                       /*!< 0x00004000 */
#define BKP_PIN2_GFILT_LOCK     BKP_PIN2_GFILT_LOCK_Msk                                 /*!<BKP_PIN2_GLITCH_FILTER_LOCK  */

#define BKP_PIN3_GFILT_LOCK_Pos (15U)
#define BKP_PIN3_GFILT_LOCK_Msk (0x1U << BKP_PIN3_GFILT_LOCK_Pos)                       /*!< 0x00008000 */
#define BKP_PIN3_GFILT_LOCK     BKP_PIN3_GFILT_LOCK_Msk                                 /*!<BKP_PIN3_GLITCH_FILTER_LOCK  */

#define BKP_PIN4_GFILT_LOCK_Pos (16U)
#define BKP_PIN4_GFILT_LOCK_Msk (0x1U << BKP_PIN4_GFILT_LOCK_Pos)                       /*!< 0x00010000 */
#define BKP_PIN4_GFILT_LOCK     BKP_PIN4_GFILT_LOCK_Msk                                 /*!<BKP_PIN4_GLITCH_FILTER_LOCK  */

#define BKP_PIN5_GFILT_LOCK_Pos (17U)
#define BKP_PIN5_GFILT_LOCK_Msk (0x1U << BKP_PIN5_GFILT_LOCK_Pos)                       /*!< 0x00020000 */
#define BKP_PIN5_GFILT_LOCK     BKP_PIN5_GFILT_LOCK_Msk                                 /*!<BKP_PIN5_GLITCH_FILTER_LOCK  */

#define BKP_PIN6_GFILT_LOCK_Pos (18U)
#define BKP_PIN6_GFILT_LOCK_Msk (0x1U << BKP_PIN6_GFILT_LOCK_Pos)                       /*!< 0x00040000 */
#define BKP_PIN6_GFILT_LOCK     BKP_PIN6_GFILT_LOCK_Msk                                 /*!<BKP_PIN6_GLITCH_FILTER_LOCK  */

#define BKP_PIN7_GFILT_LOCK_Pos (19U)
#define BKP_PIN7_GFILT_LOCK_Msk (0x1U << BKP_PIN7_GFILT_LOCK_Pos)                       /*!< 0x00080000 */
#define BKP_PIN7_GFILT_LOCK     BKP_PIN7_GFILT_LOCK_Msk                                 /*!<BKP_PIN7_GLITCH_FILTER_LOCK  */

#define BKP_ACTIVE_TAMPER0_LOCK_Pos (20U)
#define BKP_ACTIVE_TAMPER0_LOCK_Msk (0x1U << BKP_ACTIVE_TAMPER0_LOCK_Pos)               /*!< 0x00100000 */
#define BKP_ACTIVE_TAMPER0_LOCK     BKP_ACTIVE_TAMPER0_LOCK_Msk                         /*!<BKP_ACTIVE_TAMPER0_LOCK  */

#define BKP_ACTIVE_TAMPER1_LOCK_Pos (21U)
#define BKP_ACTIVE_TAMPER1_LOCK_Msk (0x1U << BKP_ACTIVE_TAMPER1_LOCK_Pos)               /*!< 0x00200000 */
#define BKP_ACTIVE_TAMPER1_LOCK     BKP_ACTIVE_TAMPER1_LOCK_Msk                         /*!<BKP_ACTIVE_TAMPER1_LOCK  */

#define BKP_SHIELD_LOCK_Pos (22U)
#define BKP_SHIELD_LOCK_Msk (0x1U << BKP_SHIELD_LOCK_Pos)                               /*!< 0x00400000 */
#define BKP_SHIELD_LOCK     BKP_SHIELD_LOCK_Msk                                         /*!<BKP_SHIELD_LOCK  */

/*******************  Bit definition for BKP_TAMPER_PCFG register  ********************/
#define BKP_PIN_HYSTERESIS_SEL_Pos  (0U)
#define BKP_PIN_HYSTERESIS_SEL_Msk  (0x1U << BKP_PIN_HYSTERESIS_SEL_Pos)                /*!< 0x00000001 */
#define BKP_PIN_HYSTERESIS_SEL      BKP_PIN_HYSTERESIS_SEL_Msk                          /*!<BKP_PIN_HYSTERESIS_SELECT  */

#define BKP_PIN_PASSIVE_FILTEN_Pos  (1U)
#define BKP_PIN_PASSIVE_FILTEN_Msk  (0x1U << BKP_PIN_PASSIVE_FILTEN_Pos)                /*!< 0x00000002 */
#define BKP_PIN_PASSIVE_FILTEN      BKP_PIN_PASSIVE_FILTEN_Msk                          /*!<BKP_PIN_PASSIVE_FILTER_EN  */

#define BKP_PIN_DRI_STRENGTHEN_Pos  (2U)
#define BKP_PIN_DRI_STRENGTHEN_Msk  (0x1U << BKP_PIN_DRI_STRENGTHEN_Pos)                /*!< 0x00000004 */
#define BKP_PIN_DRI_STRENGTHEN      BKP_PIN_DRI_STRENGTHEN_Msk                          /*!<BKP_PIN_DRIVE_STRENGTH_EN  */

#define BKP_PIN_SLEW_RATE_Pos   (3U)
#define BKP_PIN_SLEW_RATE_Msk   (0x1U << BKP_PIN_SLEW_RATE_Pos)                         /*!< 0x00000008 */
#define BKP_PIN_SLEW_RATE       BKP_PIN_SLEW_RATE_Msk                                   /*!<BKP_PIN_SLEW_RATE  */

#define BKP_DIG_SIGNAL_OUT_EN_Pos   (4U)
#define BKP_DIG_SIGNAL_OUT_EN_Msk   (0x1U << BKP_DIG_SIGNAL_OUT_EN_Pos)                 /*!< 0x00000010 */
#define BKP_DIG_SIGNAL_OUT_EN       BKP_DIG_SIGNAL_OUT_EN_Msk                           /*!<BKP_DIG_SIGNAL_OUT_EN  */

#define BKP_DIG_SIGNAL_SEL_Pos  (5U)
#define BKP_DIG_SIGNAL_SEL_Msk  (0x7U << BKP_DIG_SIGNAL_SEL_Pos)                        /*!< 0x000000E0 */
#define BKP_DIG_SIGNAL_SEL      BKP_DIG_SIGNAL_SEL_Msk                                  /*!<BKP_DIG_SIGNAL_SEL  */

/*******************  Bit definition for BKP_TAMPER_PDIR register  ********************/
#define BKP_TAMPER_PDIR_PDIR_Pos    (0U)
#define BKP_TAMPER_PDIR_PDIR_Msk    (0xFFU << BKP_TAMPER_PDIR_PDIR_Pos)                 /*!< 0x000000FF */
#define BKP_TAMPER_PDIR_PDIR        BKP_TAMPER_PDIR_PDIR_Msk                            /*!<BKP_PIN_DIR[7:0]  */

#define BKP_TAMPER_PDIR_POD_Pos (8U)
#define BKP_TAMPER_PDIR_POD_Msk (0xFFU << BKP_TAMPER_PDIR_POD_Pos)                      /*!< 0x0000FF00 */
#define BKP_TAMPER_PDIR_POD     BKP_TAMPER_PDIR_POD_Msk                                 /*!<BKP_PIN_OUTPUT_DATA  */

/*******************  Bit definition for BKP_TAMPER_PLRTY register  ********************/
#define BKP_TAMPER_PLRTY_PPOL_Pos   (0U)
#define BKP_TAMPER_PLRTY_PPOL_Msk   (0xFFU << BKP_TAMPER_PLRTY_PPOL_Pos)                /*!< 0x000000FF */
#define BKP_TAMPER_PLRTY_PPOL       BKP_TAMPER_PLRTY_PPOL_Msk                           /*!<BKP_PIN_POL[7:0]  */

#define BKP_TAMPER_PLRTY_PID_Pos    (8U)
#define BKP_TAMPER_PLRTY_PID_Msk    (0xFFU << BKP_TAMPER_PLRTY_PID_Pos)                 /*!< 0x0000FF00 */
#define BKP_TAMPER_PLRTY_PID        BKP_TAMPER_PLRTY_PID_Msk                            /*!<BKP_PIN_INPUT_DATA[7:0]  */

/*******************  Bit definition for BKP_TAMPER_PnGF register  ********************/
#define BKP_PIN_GLITCH_FILTER_EN_Pos    (0U)
#define BKP_PIN_GLITCH_FILTER_EN_Msk    (0x1U << BKP_PIN_GLITCH_FILTER_EN_Pos)          /*!< 0x00000001 */
#define BKP_PIN_GLITCH_FILTER_EN        BKP_PIN_GLITCH_FILTER_EN_Msk                    /*!<BKP_PIN_GLITCH_FILTER_EN  */

#define BKP_PIN_GLITCH_FILTER_WIDTH_Pos (1U)
#define BKP_PIN_GLITCH_FILTER_WIDTH_Msk (0x3FU << BKP_PIN_GLITCH_FILTER_WIDTH_Pos)      /*!< 0x0000007E */
#define BKP_PIN_GLITCH_FILTER_WIDTH     BKP_PIN_GLITCH_FILTER_WIDTH_Msk                 /*!<BKP_PIN_GLITCH_FILTER_WIDTH[5:0]  */

#define BKP_PIN_GLITCH_FILTER_PSC_Pos   (7U)
#define BKP_PIN_GLITCH_FILTER_PSC_Msk   (0x1U << BKP_PIN_GLITCH_FILTER_PSC_Pos)         /*!< 0x00000080 */
#define BKP_PIN_GLITCH_FILTER_PSC       BKP_PIN_GLITCH_FILTER_PSC_Msk                   /*!<BKP_PIN_GLITCH_FILTER_PRESCALER  */

#define BKP_PIN_SAMPLE_WIDTH_Pos    (8U)
#define BKP_PIN_SAMPLE_WIDTH_Msk    (0x3U << BKP_PIN_SAMPLE_WIDTH_Pos)                  /*!< 0x00000300 */
#define BKP_PIN_SAMPLE_WIDTH        BKP_PIN_SAMPLE_WIDTH_Msk                            /*!<BKP_PIN_SAMPLE_WIDTH[1:0]  */

#define BKP_PIN_SAMPLE_FREQ_Pos (10U)
#define BKP_PIN_SAMPLE_FREQ_Msk (0x3U << BKP_PIN_SAMPLE_FREQ_Pos)                       /*!< 0x00000C00 */
#define BKP_PIN_SAMPLE_FREQ     BKP_PIN_SAMPLE_FREQ_Msk                                 /*!<BKP_PIN_SAMPLE_FREQ[1:0]  */

#define BKP_PIN_PULL_ENABLE_Pos (12U)
#define BKP_PIN_PULL_ENABLE_Msk (0x1U << BKP_PIN_PULL_ENABLE_Pos)                       /*!< 0x00001000 */
#define BKP_PIN_PULL_ENABLE     BKP_PIN_PULL_ENABLE_Msk                                 /*!<BKP_PIN_PULL_ENABLE  */

#define BKP_PIN_PULL_SELECT_Pos (13U)
#define BKP_PIN_PULL_SELECT_Msk (0x1U << BKP_PIN_PULL_SELECT_Pos)                       /*!< 0x00002000 */
#define BKP_PIN_PULL_SELECT     BKP_PIN_PULL_SELECT_Msk                                 /*!<BKP_PIN_PULL_SELECT  */

#define BKP_PIN_EXPECT_DATA_Pos (16U)
#define BKP_PIN_EXPECT_DATA_Msk (0x3U << BKP_PIN_EXPECT_DATA_Pos)                       /*!< 0x00030000 */
#define BKP_PIN_EXPECT_DATA     BKP_PIN_EXPECT_DATA_Msk                                 /*!<BKP_PIN_EXPECT_DATA[1:0]  */

#define BKP_TAMPER_PNGF_COUNT 8

/*******************  Bit definition for BKP_ACTIVE_TAMPERn register  ********************/
#define BKP_ACTIVE_TAMPERN_SHIFTREG_Pos (0U)
#define BKP_ACTIVE_TAMPERN_SHIFTREG_Msk (0xFFFFU << BKP_ACTIVE_TAMPERN_SHIFTREG_Pos)    /*!< 0x0000FFFF */
#define BKP_ACTIVE_TAMPERN_SHIFTREG     BKP_ACTIVE_TAMPERN_SHIFTREG_Msk                 /*!<BKP_ACTIVE_TAMPERN_SHIFT_REG[15:0]  */

#define BKP_ACTIVE_TAMPERN_POLY_Pos (16U)
#define BKP_ACTIVE_TAMPERN_POLY_Msk (0xFFFFU << BKP_ACTIVE_TAMPERN_POLY_Pos)            /*!< 0xFFFF0000 */
#define BKP_ACTIVE_TAMPERN_POLY     BKP_ACTIVE_TAMPERN_POLY_Msk                         /*!<BKP_ACTIVE_TAMPERN_POLY[15:0]  */

/*******************  Bit definition for BKP_GPIO_EN register  ********************/
#define BKP_GPIO_EN_Pos (0U)
#define BKP_GPIO_EN_Msk (0x1U << BKP_GPIO_EN_Pos)                                       /*!< 0x00000001 */
#define BKP_GPIO_EN     BKP_GPIO_EN_Msk                                                 /*!<GPIO_EN  */

/*******************  Bit definition for BKP_GPIO_DIR register  ********************/
#define BKP_GPIO_DIR_Pos    (0U)
#define BKP_GPIO_DIR_Msk    (0xFFU << BKP_GPIO_DIR_Pos)                                 /*!< 0x000000FF */
#define BKP_GPIO_DIR        BKP_GPIO_DIR_Msk                                            /*!<GPIO_DIR[7:0]  */

/*******************  Bit definition for BKP_GPIO_DOUT register  ********************/
#define BKP_GPIO_DOUT_Pos   (0U)
#define BKP_GPIO_DOUT_Msk   (0xFFU << BKP_GPIO_DOUT_Pos)                                /*!< 0x000000FF */
#define BKP_GPIO_DOUT       BKP_GPIO_DOUT_Msk                                           /*!<GPIO_DOUT[7:0]  */

/*******************  Bit definition for BKP_GPIO_DOUT_SEL register  ********************/
#define BKP_GPIO_DOUT_SEL_Pos   (0U)
#define BKP_GPIO_DOUT_SEL_Msk   (0xFFU << BKP_GPIO_DOUT_SEL_Pos)                        /*!< 0x000000FF */
#define BKP_GPIO_DOUT_SEL       BKP_GPIO_DOUT_SEL_Msk                                   /*!<GPIO_DOUT_SEL[7:0]  */

/*******************  Bit definition for BKP_GPIO_DOUT_CLR register  ********************/
#define BKP_GPIO_DOUT_CLR_Pos   (0U)
#define BKP_GPIO_DOUT_CLR_Msk   (0xFFU << BKP_GPIO_DOUT_CLR_Pos)                        /*!< 0x000000FF */
#define BKP_GPIO_DOUT_CLR       BKP_GPIO_DOUT_CLR_Msk                                   /*!<GPIO_DOUT_CLR[7:0]  */

/*******************  Bit definition for BKP_GPIO_DOUT_TOG register  ********************/
#define BKP_GPIO_DOUT_TOG_Pos   (0U)
#define BKP_GPIO_DOUT_TOG_Msk   (0xFFU << BKP_GPIO_DOUT_TOG_Pos)                        /*!< 0x000000FF */
#define BKP_GPIO_DOUT_TOG       BKP_GPIO_DOUT_TOG_Msk                                   /*!<GPIO_DOUT_TOG[7:0]  */

/*******************  Bit definition for BKP_GPIO_DIN register  ********************/
#define BKP_GPIO_DIN_Pos    (0U)
#define BKP_GPIO_DIN_Msk    (0xFFU << BKP_GPIO_DIN_Pos)                                 /*!< 0x000000FF */
#define BKP_GPIO_DIN        BKP_GPIO_DIN_Msk                                            /*!<GPIO_DIN[7:0]  */

/*******************  Bit definition for BKP_GPIO_CLK_CTRL register  ********************/
#define BKP_GPIO_CLK_EN_Pos (0U)
#define BKP_GPIO_CLK_EN_Msk (0x1U << BKP_GPIO_CLK_EN_Pos)                               /*!< 0x00000001 */
#define BKP_GPIO_CLK_EN     BKP_GPIO_CLK_EN_Msk                                         /*!<GPIO_CLK_EN  */

#define BKP_GPIO_CLK_SEL_Pos    (4U)
#define BKP_GPIO_CLK_SEL_Msk    (0xFFU << BKP_GPIO_CLK_SEL_Pos)                         /*!< 0x00000070 */
#define BKP_GPIO_CLK_SEL        BKP_GPIO_CLK_SEL_Msk                                    /*!<GPIO_CLK_SEL[2:0]  */

#define BKP_TAMPER_APPLOCK_Pos  (0U)
#define BKP_TAMPER_APPLOCK_Msk  (0x1U << BKP_TAMPER_APPLOCK_Pos)
#define BKP_TAMPER_APPLOCK      BKP_VBAT_TIME_SECONDS_Msk

#define BKP_TAMPER_APPCFG_Pos   (1U)
#define BKP_TAMPER_APPCFG_Msk   (0x1U << BKP_TAMPER_APPCFG_Pos)
#define BKP_TAMPER_APPCFG       BKP_TAMPER_APPCFG_Msk

#define BKP_LOCK_CPPERMIT_Pos   (2U)
#define BKP_LOCK_CPPERMIT_Msk   (0x1U << BKP_LOCK_CPPERMIT_Pos)
#define BKP_LOCK_CPPERMIT       BKP_LOCK_CPPERMIT_Msk

#define BKP_CPNET_IOCFG_Pos (3U)
#define BKP_CPNET_IOCFG_Msk (0x1U << BKP_CPNET_IOCFG_Pos)
#define BKP_CPNET_IOCFG     BKP_CPNET_IOCFG_Msk

#define BKP_CPNET_CLKCFG_Pos    (4U)
#define BKP_CPNET_CLKCFG_Msk    (0x1U << BKP_CPNET_CLKCFG_Pos)
#define BKP_CPNET_CLKCFG        BKP_CPNET_CLKCFG_Msk

#define BKP_CPNET_TAMPERCFG_Pos (5U)
#define BKP_CPNET_TAMPERCFG_Msk (0x1U << BKP_CPNET_TAMPERCFG_Pos)
#define BKP_CPNET_TAMPERCFG     BKP_CPNET_TAMPERCFG_Msk

#define BKP_LOCKTAMP_LFSRDATA_Pos   (6U)
#define BKP_LOCKTAMP_LFSRDATA_Msk   (0x1U << BKP_LOCKTAMP_LFSRDATA_Pos)
#define BKP_LOCKTAMP_LFSRDATA       BKP_LOCKTAMP_LFSRDATA_Msk

#define BKP_TAMP_LFSRSEQUENCE_Pos   (7U)
#define BKP_TAMP_LFSRSEQUENCE_Msk   (0x1U << BKP_TAMP_LFSRSEQUENCE_Pos)
#define BKP_TAMP_LFSRSEQUENCE       BKP_TAMP_LFSRSEQUENCE_Msk

#define BKP_TAMP_TAMPASSCFG_Pos (8U)
#define BKP_TAMP_TAMPASSCFG_Msk (0x1U << BKP_TAMP_TAMPASSCFG_Pos)
#define BKP_TAMP_TAMPASSCFG     BKP_TAMP_TAMPASSCFG_Msk

#define BKP_TAMP_FILTER_Pos (9U)
#define BKP_TAMP_FILTER_Msk (0x1U << BKP_TAMP_FILTER_Pos)
#define BKP_TAMP_FILTER     BKP_TAMP_FILTER_Msk

/*******************  Bit definition for TAMPER_APP_CFG register  ********************/
#define BKP_SELFTEST_MODE_Pos   (0U)
#define BKP_SELFTEST_MODE_Msk   (0x1U << BKP_SELFTEST_MODE_Pos)
#define BKP_SELFTEST_MODE       BKP_SELFTEST_MODE_Msk

#define BKP_GLITCH_FRONTFILEN_Pos   (1U)
#define BKP_GLITCH_FRONTFILEN_Msk   (0x1U << BKP_GLITCH_FRONTFILEN_Pos)
#define BKP_GLITCH_FRONTFILEN       BKP_GLITCH_FRONTFILEN_Msk

#define BKP_GLITCH_ACTUALFILEN_Pos  (2U)
#define BKP_GLITCH_ACTUALFILEN_Msk  (0x1U << BKP_GLITCH_ACTUALFILEN_Pos)
#define BKP_GLITCH_ACTUALFILEN      BKP_GLITCH_ACTUALFILEN_Msk

#define BKP_WARN_FILTERCFG_Pos  (4U)
#define BKP_WARN_FILTERCFG_Msk  (0x3U << BKP_WARN_FILTERCFG_Pos)
#define BKP_WARN_FILTERCFG      BKP_WARN_FILTERCFG_Msk

#define BKP_VOLDET_AUTOEN_Pos   (8U)
#define BKP_VOLDET_AUTOEN_Msk   (0x1U << BKP_VOLDET_AUTOEN_Pos)
#define BKP_VOLDET_AUTOEN       BKP_VOLDET_AUTOEN_Msk

#define BKP_VOLDET_TIMECFG_Pos  (12U)
#define BKP_VOLDET_TIMECFG_Msk  (0x7U << BKP_VOLDET_TIMECFG_Pos)
#define BKP_VOLDET_TIMECFG      BKP_VOLDET_TIMECFG_Msk

#define BKP_VOLDET_TIME_Pos (16U)
#define BKP_VOLDET_TIME_Msk (0xFU << BKP_VOLDET_TIME_Pos)
#define BKP_VOLDET_TIME     BKP_VOLDET_TIME_Msk

/*******************  Bit definition for TAMPER_LFSR_DATAS register  ********************/
#define BKP_TAMPER_LFSRINITDA_Pos   (0U)
#define BKP_TAMPER_LFSRINITDA_Msk   (0xFFFFU << BKP_TAMPER_LFSRINITDA_Pos)
#define BKP_TAMPER_LFSRINITDA       BKP_TAMPER_LFSRINITDA_Msk

#define BKP_ACTIVE_LFSR0TAPSEL_Pos  (16U)
#define BKP_ACTIVE_LFSR0TAPSEL_Msk  (0x7U << BKP_ACTIVE_LFSR0TAPSEL_Pos)
#define BKP_ACTIVE_LFSR0TAPSEL      BKP_ACTIVE_LFSR0TAPSEL_Msk

#define BKP_ACTIVE_LFSR0EN_Pos  (19U)
#define BKP_ACTIVE_LFSR0EN_Msk  (0x1U << BKP_ACTIVE_LFSR0EN_Pos)
#define BKP_ACTIVE_LFSR0EN      BKP_ACTIVE_LFSR0EN_Msk

#define BKP_ACTIVE_LFSR1TAPSEL_Pos  (20U)
#define BKP_ACTIVE_LFSR1TAPSEL_Msk  (0x7U << BKP_ACTIVE_LFSR1TAPSEL_Pos)
#define BKP_ACTIVE_LFSR1TAPSEL      BKP_ACTIVE_LFSR1TAPSEL_Msk

#define BKP_ACTIVE_LFSR1EN_Pos  (23U)
#define BKP_ACTIVE_LFSR1EN_Msk  (0x1U << BKP_ACTIVE_LFSR1EN_Pos)
#define BKP_ACTIVE_LFSR1EN      BKP_ACTIVE_LFSR1EN_Msk

#define BKP_LFSR_INTDATASEL_Pos (29U)
#define BKP_LFSR_INTDATASEL_Msk (0x1U << BKP_LFSR_INTDATASEL_Pos)
#define BKP_LFSR_INTDATASEL     BKP_LFSR_INTDATASEL_Msk

/*******************  Bit definition for TAMPER_LFSR_SEQUENCE register  ********************/
#define BKP_LFSR_SEQUENCE_Pos   (0U)
#define BKP_LFSR_SEQUENCE_Msk   (0xFFFFFFFFU << BKP_LFSR_SEQUENCE_Pos)
#define BKP_LFSR_SEQUENCE       BKP_LFSR_SEQUENCE_Msk


/*******************  Bit definition for TAMPER_PASS_CFG register  ********************/
#define BKP_PASS_FREQTAMFLAG_Pos    (0U)
#define BKP_PASS_FREQTAMFLAG_Msk    (0x1U << BKP_PASS_FREQTAMFLAG_Pos)
#define BKP_PASS_FREQTAMFLAG        BKP_PASS_FREQTAMFLAG_Msk

#define BKP_PASS_VOLTAMFLAG_Pos (1U)
#define BKP_PASS_VOLTAMFLAG_Msk (0x1U << BKP_PASS_VOLTAMFLAG_Pos)
#define BKP_PASS_VOLTAMFLAG     BKP_PASS_VOLTAMFLAG_Msk

#define BKP_PASS_TEMPTAMFLAG_Pos    (2U)
#define BKP_PASS_TEMPTAMFLAG_Msk    (0x1U << BKP_PASS_TEMPTAMFLAG_Pos)
#define BKP_PASS_TEMPTAMFLAG        BKP_PASS_TEMPTAMFLAG_Msk

#define BKP_PASS_GLIHTAMFLAG_Pos    (3U)
#define BKP_PASS_GLIHTAMFLAG_Msk    (0x1U << BKP_PASS_GLIHTAMFLAG_Pos)
#define BKP_PASS_GLIHTAMFLAG        BKP_PASS_GLIHTAMFLAG_Msk

#define BKP_PASS_PIN0TAMFLAG_Pos    (4U)
#define BKP_PASS_PIN0TAMFLAG_Msk    (0x1U << BKP_PASS_PIN0TAMFLAG_Pos)
#define BKP_PASS_PIN0TAMFLAG        BKP_PASS_PIN0TAMFLAG_Msk

#define BKP_PASS_PIN1TAMFLAG_Pos    (5U)
#define BKP_PASS_PIN1TAMFLAG_Msk    (0x1U << BKP_PASS_PIN1TAMFLAG_Pos)
#define BKP_PASS_PIN1TAMFLAG        BKP_PASS_PIN1TAMFLAG_Msk

#define BKP_PASS_PIN2TAMFLAG_Pos    (6U)
#define BKP_PASS_PIN2TAMFLAG_Msk    (0x1U << BKP_PASS_PIN2TAMFLAG_Pos)
#define BKP_PASS_PIN2TAMFLAG        BKP_PASS_PIN2TAMFLAG_Msk

#define BKP_PASS_PIN3TAMFLAG_Pos    (7U)
#define BKP_PASS_PIN3TAMFLAG_Msk    (0x1U << BKP_PASS_PIN3TAMFLAG_Pos)
#define BKP_PASS_PIN3TAMFLAG        BKP_PASS_PIN3TAMFLAG_Msk

#define BKP_PASS_PIN4TAMFLAG_Pos    (8U)
#define BKP_PASS_PIN4TAMFLAG_Msk    (0x1U << BKP_PASS_PIN4TAMFLAG_Pos)
#define BKP_PASS_PIN4TAMFLAG        BKP_PASS_PIN4TAMFLAG_Msk

#define BKP_PASS_PIN5TAMFLAG_Pos    (9U)
#define BKP_PASS_PIN5TAMFLAG_Msk    (0x1U << BKP_PASS_PIN5TAMFLAG_Pos)
#define BKP_PASS_PIN5TAMFLAG        BKP_PASS_PIN5TAMFLAG_Msk

#define BKP_PASS_PIN6TAMFLAG_Pos    (10U)
#define BKP_PASS_PIN6TAMFLAG_Msk    (0x1U << BKP_PASS_PIN6TAMFLAG_Pos)
#define BKP_PASS_PIN6TAMFLAG        BKP_PASS_PIN6TAMFLAG_Msk

#define BKP_PASS_PIN7TAMFLAG_Pos    (11U)
#define BKP_PASS_PIN7TAMFLAG_Msk    (0x1U << BKP_PASS_PIN7TAMFLAG_Pos)
#define BKP_PASS_PIN7TAMFLAG        BKP_PASS_PIN7TAMFLAG_Msk

#define BKP_PASS_STFREQTAMPFLAG_Pos (12U)
#define BKP_PASS_STFREQTAMPFLAG_Msk (0x1U << BKP_PASS_STFREQTAMPFLAG_Pos)
#define BKP_PASS_STFREQTAMPFLAG     BKP_PASS_STFREQTAMPFLAG_Msk

#define BKP_PASS_STVOLTAMPFLAG_Pos  (13U)
#define BKP_PASS_STVOLTAMPFLAG_Msk  (0x1U << BKP_PASS_STVOLTAMPFLAG_Pos)
#define BKP_PASS_STVOLTAMPFLAG      BKP_PASS_STVOLTAMPFLAG_Msk

#define BKP_PASS_STTEMPTAMPFLAG_Pos (14U)
#define BKP_PASS_STTEMPTAMPFLAG_Msk (0x1U << BKP_PASS_STTEMPTAMPFLAG_Pos)
#define BKP_PASS_STTEMPTAMPFLAG     BKP_PASS_STTEMPTAMPFLAG_Msk

#define BKP_PASS_STGLIHTAMPFLAG_Pos (15U)
#define BKP_PASS_STGLIHTAMPFLAG_Msk (0x1U << BKP_PASS_STGLIHTAMPFLAG_Pos)
#define BKP_PASS_STGLIHTAMPFLAG     BKP_PASS_STGLIHTAMPFLAG_Msk

#define BKP_PASS_OSCSTOPTAMFLAG_Pos (16U)
#define BKP_PASS_OSCSTOPTAMFLAG_Msk (0x1U << BKP_PASS_OSCSTOPTAMFLAG_Pos)
#define BKP_PASS_OSCSTOPTAMFLAG     BKP_PASS_OSCSTOPTAMFLAG_Msk

/*******************  Bit definition for TAMPER_FILTER_CFG register  ********************/
#define BKP_GLITCH_ANACLR_Pos   (0U)
#define BKP_GLITCH_ANACLR_Msk   (0x1U << BKP_GLITCH_ANACLR_Pos)
#define BKP_GLITCH_ANACLR       BKP_GLITCH_ANACLR_Msk

#define BKP_GLITCH_ACTFILTERWAITMAX_Pos (4U)
#define BKP_GLITCH_ACTFILTERWAITMAX_Msk (0xFU << BKP_GLITCH_ACTFILTERWAITMAX_Pos)
#define BKP_GLITCH_ACTFILTERWAITMAX     BKP_GLITCH_ACTFILTERWAITMAX_Msk

#define BKP_GLITCH_ACTFILTERRUNMAX_Pos  (8U)
#define BKP_GLITCH_ACTFILTERRUNMAX_Msk  (0x7U << BKP_GLITCH_ACTFILTERWAITMAX_Pos)
#define BKP_GLITCH_ACTFILTERRUNMAX      BKP_GLITCH_ACTFILTERWAITMAX_Msk

#define BKP_GLITCH_ACTFILTERTIMERSMAX_Pos   (12U)
#define BKP_GLITCH_ACTFILTERTIMERSMAX_Msk   (0x7U << BKP_GLITCH_ACTFILTERTIMERSMAX_Pos)
#define BKP_GLITCH_ACTFILTERTIMERSMAX       BKP_GLITCH_ACTFILTERTIMERSMAX_Msk

#define BKP_ALARM_AUTOMASKTIME_Pos  (16U)
#define BKP_ALARM_AUTOMASKTIME_Msk  (0xFU << BKP_ALARM_AUTOMASKTIME_Pos)
#define BKP_ALARM_AUTOMASKTIME      BKP_ALARM_AUTOMASKTIME_Msk

/*******************  Bit definition for BKP_KEY_VALID register  ********************/
#define BKP_KEY_VALID_Pos   (0U)
#define BKP_KEY_VALID_Msk   (0xFFU << BKP_KEY_VALID_Pos)                        /*!< 0x000000FF */
#define BKP_KEY_VALID       BKP_KEY_VALID_Msk                                   /*!<KEY_VALID[7:0]  */

#define BKP_KEYRF_PARITY_ERROR_Pos  (8U)
#define BKP_KEYRF_PARITY_ERROR_Msk  (0x1U << BKP_KEYRF_PARITY_ERROR_Pos)        /*!< 0x00000100 */
#define BKP_KEYRF_PARITY_ERROR      BKP_KEYRF_PARITY_ERROR_Msk                  /*!<BKP_KEYRF_PARITY_ERROR  */

#define BKP_NORMRF_PARITY_ERROR_Pos (9U)
#define BKP_NORMRF_PARITY_ERROR_Msk (0x1U << BKP_NORMRF_PARITY_ERROR_Pos)       /*!< 0x00000200 */
#define BKP_NORMRF_PARITY_ERROR     BKP_NORMRF_PARITY_ERROR_Msk                 /*!<NORMAL_REGFILE_PARITY_ERROR  */

/*******************  Bit definition for BKP_KEY_WRITE_LOCK register  ********************/
#define BKP_KEY_WLOCK_WLOCK_Pos (0U)
#define BKP_KEY_WLOCK_WLOCK_Msk (0xFFU << BKP_KEY_WLOCK_WLOCK_Pos)              /*!< 0x000000FF */
#define BKP_KEY_WLOCK_WLOCK     BKP_KEY_WLOCK_WLOCK_Msk                         /*!<KEY_WRITE_LOCK[7:0]  */

/*******************  Bit definition for BKP_KEY_READ_LOCK register  ********************/
#define BKP_KEY_RLOCK_RLOCK_Pos (0U)
#define BKP_KEY_RLOCK_RLOCK_Msk (0xFFU << BKP_KEY_RLOCK_RLOCK_Pos)              /*!< 0x000000FF */
#define BKP_KEY_RLOCK_RLOCK     BKP_KEY_RLOCK_RLOCK_Msk                         /*!<KEY_READ_LOCK[7:0]  */

/*******************  Bit definition for BKP_KEY_LOCK register  ********************/
#define BKP_KEY_VALID_LOCK_Pos  (0U)
#define BKP_KEY_VALID_LOCK_Msk  (0x1U << BKP_KEY_VALID_LOCK_Pos)                /*!< 0x00000001 */
#define BKP_KEY_VALID_LOCK      BKP_KEY_VALID_LOCK_Msk                          /*!<KEY_VALID_LOCK  */

#define BKP_KEY_WRITE_LOCK_LOCK_Pos (1U)
#define BKP_KEY_WRITE_LOCK_LOCK_Msk (0x1U << BKP_KEY_WRITE_LOCK_LOCK_Pos)       /*!< 0x00000002 */
#define BKP_KEY_WRITE_LOCK_LOCK     BKP_KEY_WRITE_LOCK_LOCK_Msk                 /*!<KEY_WRITE_LOCK_LOCK  */

#define BKP_KEY_READ_LOCK_LOCK_Pos  (2U)
#define BKP_KEY_READ_LOCK_LOCK_Msk  (0x1U << BKP_KEY_READ_LOCK_LOCK_Pos)        /*!< 0x00000004 */
#define BKP_KEY_READ_LOCK_LOCK      BKP_KEY_READ_LOCK_LOCK_Msk                  /*!<KEY_READ_LOCK_LOCK  */

#define BKP_KEY_LOCK_LOCK_Pos   (3U)
#define BKP_KEY_LOCK_LOCK_Msk   (0x1U << BKP_KEY_LOCK_LOCK_Pos)                 /*!< 0x00000008 */
#define BKP_KEY_LOCK_LOCK       BKP_KEY_LOCK_LOCK_Msk                           /*!<KEY_LOCK_LOCK  */

#define BKP_REGFILE_KEY_LOCK_Pos    (4U)
#define BKP_REGFILE_KEY_LOCK_Msk    (0x1U << BKP_REGFILE_KEY_LOCK_Pos)          /*!< 0x00000010 */
#define BKP_REGFILE_KEY_LOCK        BKP_REGFILE_KEY_LOCK_Msk                    /*!<REGFILE_KEY_LOCK  */

/*******************  Bit definition for BKP_REGFILE_KEY register  ********************/
#define BKP_REGFILE_KEY_Pos (0U)
#define BKP_REGFILE_KEY_Msk (0xFFFFFFFFU << BKP_REGFILE_KEY_Pos)                /*!< 0xFFFFFFFF */
#define BKP_REGFILE_KEY     BKP_REGFILE_KEY_Msk                                 /*!<REGFILE_KEY[31:0]  */

/*******************  Bit definition for BKP_KEY_REGFILEN register  ********************/
#define BKP_KEY_REGFILE_Pos (0U)
#define BKP_KEY_REGFILE_Msk (0xFFFFFFFFU << BKP_KEY_REGFILE_Pos)                /*!< 0xFFFFFFFF */
#define BKP_KEY_REGFILE     BKP_KEY_REGFILE_Msk                                 /*!<KEY_REGFILE[31:0]  */

#define BKP_KEY_REGFILEN_COUNT 8

/*******************  Bit definition for BKP_NORMAL_REGFILEN register  ********************/
#define BKP_NORMAL_REGFILE_Pos  (0U)
#define BKP_NORMAL_REGFILE_Msk  (0xFFFFFFFFU << BKP_NORMAL_REGFILE_Pos)         /*!< 0xFFFFFFFF */
#define BKP_NORMAL_REGFILE      BKP_NORMAL_REGFILE_Msk                          /*!<NORMAL_REGFILE[31:0]  */

#define BKP_NORMAL_REGFILEN_COUNT 32


//</h>
//<h>SPI

/******************************************************************************/
/*                                                                            */
/*                                    SPI_CSR                                    */
/*                                                                            */
/******************************************************************************/
/*******************   Bit definition for SPI_SPCR1 register   ********************/
#define SPI_SPCR1_CPHA_Pos  (0U)
#define SPI_SPCR1_CPHA_Msk  (0x1U << SPI_SPCR1_CPHA_Pos)                            /*!< 0x00000001 */
#define SPI_SPCR1_CPHA      SPI_SPCR1_CPHA_Msk                                      /*!<CPHA */

#define SPI_SPCR1_CPOL_Pos  (1U)
#define SPI_SPCR1_CPOL_Msk  (0x1U << SPI_SPCR1_CPOL_Pos)                            /*!< 0x00000002 */
#define SPI_SPCR1_CPOL      SPI_SPCR1_CPOL_Msk                                      /*!<CPOL */

#define SPI_SPCR1_MSTR_Pos  (2U)
#define SPI_SPCR1_MSTR_Msk  (0x1U << SPI_SPCR1_MSTR_Pos)                            /*!< 0x00000004 */
#define SPI_SPCR1_MSTR      SPI_SPCR1_MSTR_Msk                                      /*!<MSTR */

#define SPI_SPCR1_LSBFIRST_Pos  (3U)
#define SPI_SPCR1_LSBFIRST_Msk  (0x1U << SPI_SPCR1_LSBFIRST_Pos)                    /*!< 0x00000008 */
#define SPI_SPCR1_LSBFIRST      SPI_SPCR1_LSBFIRST_Msk                              /*!<LSBFIRST */

#define SPI_SPCR1_SSNMODE_Pos   (4U)
#define SPI_SPCR1_SSNMODE_Msk   (0x1U << SPI_SPCR1_SSNMODE_Pos)                     /*!< 0x00000010 */
#define SPI_SPCR1_SSNMODE       SPI_SPCR1_SSNMODE_Msk                               /*!<SSNMODE */

#define SPI_SPCR1_BR_Pos    (5U)
#define SPI_SPCR1_BR_Msk    (0x7U << SPI_SPCR1_BR_Pos)                              /*!< 0x000000E0 */
#define SPI_SPCR1_BR        SPI_SPCR1_BR_Msk                                        /*!<BR[2:0] */

#define SPI_SPCR1_TBYTE_Pos (8U)
#define SPI_SPCR1_TBYTE_Msk (0x3U << SPI_SPCR1_TBYTE_Pos)                           /*!< 0x00000300 */
#define SPI_SPCR1_TBYTE     SPI_SPCR1_TBYTE_Msk                                     /*!<TBYTE[1:0] */

#define SPI_SPCR1_WAITCNT_Pos   (10U)
#define SPI_SPCR1_WAITCNT_Msk   (0x3U << SPI_SPCR1_WAITCNT_Pos)                     /*!< 0x00000C00 */
#define SPI_SPCR1_WAITCNT       SPI_SPCR1_WAITCNT_Msk                               /*!<WAITCNT[1:0] */

#define SPI_SPCR1_SSNMCUEN_Pos  (12U)
#define SPI_SPCR1_SSNMCUEN_Msk  (0x1U << SPI_SPCR1_SSNMCUEN_Pos)                    /*!< 0x00001000 */
#define SPI_SPCR1_SSNMCUEN      SPI_SPCR1_SSNMCUEN_Msk                              /*!<SSNMCUEN */

#define SPI_SPCR1_SPIEN_Pos (13U)
#define SPI_SPCR1_SPIEN_Msk (0x1U << SPI_SPCR1_SPIEN_Pos)                           /*!< 0x00002000 */
#define SPI_SPCR1_SPIEN     SPI_SPCR1_SPIEN_Msk                                     /*!<SPIEN */

#define SPI_SPCR1_TXONLY_Pos    (14U)
#define SPI_SPCR1_TXONLY_Msk    (0x1U << SPI_SPCR1_TXONLY_Pos)                      /*!< 0x00004000 */
#define SPI_SPCR1_TXONLY        SPI_SPCR1_TXONLY_Msk                                /*!<TXONLY */

#define SPI_SPCR1_SAMPLEP_Pos   (15U)
#define SPI_SPCR1_SAMPLEP_Msk   (0x1U << SPI_SPCR1_SAMPLEP_Pos)                     /*!< 0x00008000 */
#define SPI_SPCR1_SAMPLEP       SPI_SPCR1_SAMPLEP_Msk                               /*!<SAMPLEP */

#define SPI_SPCR1_SDOUTSEND_Pos (16U)
#define SPI_SPCR1_SDOUTSEND_Msk (0x1U << SPI_SPCR1_SDOUTSEND_Pos)                   /*!< 0x00010000 */
#define SPI_SPCR1_SDOUTSEND     SPI_SPCR1_SDOUTSEND_Msk                             /*!<SDOUTSEND */

#define SPI_SPCR1_SSNFILT_Pos   (17U)
#define SPI_SPCR1_SSNFILT_Msk   (0x1U << SPI_SPCR1_SSNFILT_Pos)                     /*!< 0x00020000 */
#define SPI_SPCR1_SSNFILT       SPI_SPCR1_SSNFILT_Msk                               /*!<SSNFILT */

#define SPI_SPCR1_SCKFILT_Pos   (18U)
#define SPI_SPCR1_SCKFILT_Msk   (0x1U << SPI_SPCR1_SCKFILT_Pos)                     /*!< 0x00040000 */
#define SPI_SPCR1_SCKFILT       SPI_SPCR1_SCKFILT_Msk                               /*!<SCKFILT */

#define SPI_SPCR1_MOSIFILT_Pos  (19U)
#define SPI_SPCR1_MOSIFILT_Msk  (0x1U << SPI_SPCR1_MOSIFILT_Pos)                    /*!< 0x00080000 */
#define SPI_SPCR1_MOSIFILT      SPI_SPCR1_MOSIFILT_Msk                              /*!<MOSIFILT */

#define SPI_SPCR1_SSNODEN_Pos   (20U)
#define SPI_SPCR1_SSNODEN_Msk   (0x1U << SPI_SPCR1_SSNODEN_Pos)                     /*!< 0x00100000 */
#define SPI_SPCR1_SSNODEN       SPI_SPCR1_SSNODEN_Msk                               /*!<SSNODEN */

/*******************   Bit definition for SPI_SPCR2 register   ********************/
#define SPI_SPCR2_SSNMCU_Pos    (0U)
#define SPI_SPCR2_SSNMCU_Msk    (0x1U << SPI_SPCR2_SSNMCU_Pos)                      /*!< 0x00000001 */
#define SPI_SPCR2_SSNMCU        SPI_SPCR2_SSNMCU_Msk                                /*!<SSNMCU */

/*******************   Bit definition for SPI_SPCR3 register   ********************/
#define SPI_SPCR3_TXONLYEN_Pos  (0U)
#define SPI_SPCR3_TXONLYEN_Msk  (0x1U << SPI_SPCR3_TXONLYEN_Pos)                    /*!< 0x00000001 */
#define SPI_SPCR3_TXONLYEN      SPI_SPCR3_TXONLYEN_Msk                              /*!<TXONLYEN */

/*******************   Bit definition for SPI_SPCR4 register   ********************/
#define SPI_SPCR4_CTXWCOL_Pos   (0U)
#define SPI_SPCR4_CTXWCOL_Msk   (0x1U << SPI_SPCR4_CTXWCOL_Pos)                     /*!< 0x00000001 */
#define SPI_SPCR4_CTXWCOL       SPI_SPCR4_CTXWCOL_Msk                               /*!<CTXWCOL */

#define SPI_SPCR4_CRXWCOL_Pos   (1U)
#define SPI_SPCR4_CRXWCOL_Msk   (0x1U << SPI_SPCR4_CRXWCOL_Pos)                     /*!< 0x00000002 */
#define SPI_SPCR4_CRXWCOL       SPI_SPCR4_CRXWCOL_Msk                               /*!<CRXWCOL */

#define SPI_SPCR4_CSLAVEER_Pos  (2U)
#define SPI_SPCR4_CSLAVEER_Msk  (0x1U << SPI_SPCR4_CSLAVEER_Pos)                    /*!< 0x00000004 */
#define SPI_SPCR4_CSLAVEER      SPI_SPCR4_CSLAVEER_Msk                              /*!<CSLAVEER */

#define SPI_SPCR4_CMASTERER_Pos (3U)
#define SPI_SPCR4_CMASTERER_Msk (0x1U << SPI_SPCR4_CMASTERER_Pos)                   /*!< 0x00000008 */
#define SPI_SPCR4_CMASTERER     SPI_SPCR4_CMASTERER_Msk                             /*!<CMASTERER */

#define SPI_SPCR4_CRXFIFO_Pos   (4U)
#define SPI_SPCR4_CRXFIFO_Msk   (0x1U << SPI_SPCR4_CRXFIFO_Pos)                     /*!< 0x00000010 */
#define SPI_SPCR4_CRXFIFO       SPI_SPCR4_CRXFIFO_Msk                               /*!<CRXFIFO */

#define SPI_SPCR4_CTXFIFO_Pos   (5U)
#define SPI_SPCR4_CTXFIFO_Msk   (0x1U << SPI_SPCR4_CTXFIFO_Pos)                     /*!< 0x00000020 */
#define SPI_SPCR4_CTXFIFO       SPI_SPCR4_CTXFIFO_Msk                               /*!<CTXFIFO */

/*******************   Bit definition for SPIIE register   ********************/
#define SPI_SPIIE_RXNEIE_Pos    (0U)
#define SPI_SPIIE_RXNEIE_Msk    (0x1U << SPI_SPIIE_RXNEIE_Pos)                      /*!< 0x00000001 */
#define SPI_SPIIE_RXNEIE        SPI_SPIIE_RXNEIE_Msk                                /*!<RXNEIE */

#define SPI_SPIIE_TXEIE_Pos (1U)
#define SPI_SPIIE_TXEIE_Msk (0x1U << SPI_SPIIE_TXEIE_Pos)                           /*!< 0x00000002 */
#define SPI_SPIIE_TXEIE     SPI_SPIIE_TXEIE_Msk                                     /*!<TXEIE */

#define SPI_SPIIE_RXHFIE_Pos    (2U)
#define SPI_SPIIE_RXHFIE_Msk    (0x1U << SPI_SPIIE_RXHFIE_Pos)                      /*!< 0x00000004 */
#define SPI_SPIIE_RXHFIE        SPI_SPIIE_RXHFIE_Msk                                /*!<RXHFIE */

#define SPI_SPIIE_TXHEIE_Pos    (3U)
#define SPI_SPIIE_TXHEIE_Msk    (0x1U << SPI_SPIIE_TXHEIE_Pos)                      /*!< 0x00000008 */
#define SPI_SPIIE_TXHEIE        SPI_SPIIE_TXHEIE_Msk                                /*!<TXHEIE */

#define SPI_SPIIE_ERRIE_Pos (4U)
#define SPI_SPIIE_ERRIE_Msk (0x1U << SPI_SPIIE_ERRIE_Pos)                           /*!< 0x00000010 */
#define SPI_SPIIE_ERRIE     SPI_SPIIE_ERRIE_Msk                                     /*!<ERRIE */

#define SPI_SPIIE_RXNEDE_Pos    (8U)
#define SPI_SPIIE_RXNEDE_Msk    (0x1U << SPI_SPIIE_RXNEDE_Pos)                      /*!< 0x00000100 */
#define SPI_SPIIE_RXNEDE        SPI_SPIIE_RXNEDE_Msk                                /*!<RXNEDE */

#define SPI_SPIIE_TXEDE_Pos (9U)
#define SPI_SPIIE_TXEDE_Msk (0x1U << SPI_SPIIE_TXEDE_Pos)                           /*!< 0x00000200 */
#define SPI_SPIIE_TXEDE     SPI_SPIIE_TXEDE_Msk                                     /*!<TXEDE */

/*******************   Bit definition for SPDF register   ********************/
#define SPI_SPDF_RXFIFONE_Pos   (0U)
#define SPI_SPDF_RXFIFONE_Msk   (0x1U << SPI_SPDF_RXFIFONE_Pos)                     /*!< 0x00000001 */
#define SPI_SPDF_RXFIFONE       SPI_SPDF_RXFIFONE_Msk                               /*!<RXFIFONE */

#define SPI_SPDF_RXFIFOS_Pos    (1U)
#define SPI_SPDF_RXFIFOS_Msk    (0xFU << SPI_SPDF_RXFIFOS_Pos)                      /*!< 0x0000001E */
#define SPI_SPDF_RXFIFOS        SPI_SPDF_RXFIFOS_Msk                                /*!<RXFIFOS[3:0] */

#define SPI_SPDF_TXFIFOE_Pos    (5U)
#define SPI_SPDF_TXFIFOE_Msk    (0x1U << SPI_SPDF_TXFIFOE_Pos)                      /*!< 0x00000020 */
#define SPI_SPDF_TXFIFOE        SPI_SPDF_TXFIFOE_Msk                                /*!<TXFIFOE */

#define SPI_SPDF_TXFIFOS_Pos    (6U)
#define SPI_SPDF_TXFIFOS_Msk    (0xFU << SPI_SPDF_TXFIFOS_Pos)                      /*!< 0x000003C0 */
#define SPI_SPDF_TXFIFOS        SPI_SPDF_TXFIFOS_Msk                                /*!<TXFIFOS[3:0] */

/*******************   Bit definition for SPSR register   ********************/
#define SPI_SPSR_BUSY_Pos   (10U)
#define SPI_SPSR_BUSY_Msk   (0x1U << SPI_SPSR_BUSY_Pos)                             /*!< 0x00000200 */
#define SPI_SPSR_BUSY       SPI_SPSR_BUSY_Msk                                       /*!<BUSY */

#define SPI_SPSR_TXWCOL_Pos (11U)
#define SPI_SPSR_TXWCOL_Msk (0x1U << SPI_SPSR_TXWCOL_Pos)                           /*!< 0x00000400 */
#define SPI_SPSR_TXWCOL     SPI_SPSR_TXWCOL_Msk                                     /*!<TXWCOL */

#define SPI_SPSR_RXWCOL_Pos (12U)
#define SPI_SPSR_RXWCOL_Msk (0x1U << SPI_SPSR_RXWCOL_Pos)                           /*!< 0x00000800 */
#define SPI_SPSR_RXWCOL     SPI_SPSR_RXWCOL_Msk                                     /*!<RXWCOL */

#define SPI_SPSR_SLAVEERROR_Pos (13U)
#define SPI_SPSR_SLAVEERROR_Msk (0x1U << SPI_SPSR_SLAVEERROR_Pos)                   /*!< 0x00001000 */
#define SPI_SPSR_SLAVEERROR     SPI_SPSR_SLAVEERROR_Msk                             /*!<SLAVEERROR */

#define SPI_SPSR_MASTERERROR_Pos    (14U)
#define SPI_SPSR_MASTERERROR_Msk    (0x1U << SPI_SPSR_MASTERERROR_Pos)              /*!< 0x00002000 */
#define SPI_SPSR_MASTERERROR        SPI_SPSR_MASTERERROR_Msk                        /*!<MASTERERROR */

/*******************   Bit definition for SPI_TXFIFO register   ********************/
#define SPI_TXFIFO_WRDATA_Pos   (0U)
#define SPI_TXFIFO_WRDATA_Msk   (0xFFFFFFFFU << SPI_TXFIFO_WRDATA_Pos)              /*!< 0xFFFFFFFF */
#define SPI_TXFIFO_WRDATA       SPI_TXFIFO_WRDATA_Msk                               /*!<WRDATA */

/*******************   Bit definition for SPI_RXFIFO register   ********************/
#define SPI_RXFIFO_RXDATA_Pos   (0U)
#define SPI_RXFIFO_RXDATA_Msk   (0xFFU << SPI_RXFIFO_RXDATA_Pos)                    /*!< 0x000000FF */
#define SPI_RXFIFO_RXDATA       SPI_RXFIFO_RXDATA_Msk                               /*!<RXDATA */
//</h>
//<h>STIMER
/******************************************************************************/
/*                                                                            */
/*                                    Simple Timer                            */
/*                                                                            */
/******************************************************************************/
/*******************  Bit definition for ST_CTRL register  ********************/
#define ST_CTRL_STEN_Pos( n )   (n * 4U)
#define ST_CTRL_STEN_Msk( n )   (0x1U << ST_CTRL_STEN_Pos( n ) )            /*!< 0x00000001 */
#define ST_CTRL_STEN( n )       ST_CTRL_STEN_Msk( n )                       /*!< Simple timer n enable */

#define ST_CTRL_ST0EN_Pos   (0U)
#define ST_CTRL_ST0EN_Msk   (0x1U << ST_CTRL_ST0EN_Pos)                     /*!< 0x00000001 */
#define ST_CTRL_ST0EN       ST_CTRL_ST0EN_Msk                               /*!< Simple timer0 enable */

#define ST_CTRL_ST1EN_Pos   (4U)
#define ST_CTRL_ST1EN_Msk   (0x1U << ST_CTRL_ST1EN_Pos)                     /*!< 0x00000010 */
#define ST_CTRL_ST1EN       ST_CTRL_ST1EN_Msk                               /*!< Simple timer1 enable */

#define ST_CTRL_ST2EN_Pos   (8U)
#define ST_CTRL_ST2EN_Msk   (0x1U << ST_CTRL_ST2EN_Pos)                     /*!< 0x00000100 */
#define ST_CTRL_ST2EN       ST_CTRL_ST2EN_Msk                               /*!< Simple timer2 enable */

#define ST_CTRL_ST3EN_Pos   (12U)
#define ST_CTRL_ST3EN_Msk   (0x1U << ST_CTRL_ST3EN_Pos)                     /*!< 0x00001000 */
#define ST_CTRL_ST3EN       ST_CTRL_ST3EN_Msk                               /*!< Simple timer3 enable */

#define ST_CTRL_ST4EN_Pos   (16U)
#define ST_CTRL_ST4EN_Msk   (0x1U << ST_CTRL_ST4EN_Pos)                     /*!< 0x00010000 */
#define ST_CTRL_ST4EN       ST_CTRL_ST4EN_Msk                               /*!< Simple timer4 enable */

#define ST_CTRL_ST5EN_Pos   (20U)
#define ST_CTRL_ST5EN_Msk   (0x1U << ST_CTRL_ST5EN_Pos)                     /*!< 0x00100000 */
#define ST_CTRL_ST5EN       ST_CTRL_ST5EN_Msk                               /*!< Simple timer5 enable */

/*******************  Bit definition for ST_RESET register  ********************/
#define ST_RESET_STRESET_Pos( n )   (n * 4U)
#define ST_RESET_STRESET_Msk( n )   (0x1U << ST_RESET_STRESET_Pos( n ) )    /*!< 0x00000001 */
#define ST_RESET_STRESET( n )       ST_RESET_STRESET_Msk( n )               /*!< Simple timer n reset */

#define ST_RESET_ST0RESET_Pos   (0U)
#define ST_RESET_ST0RESET_Msk   (0x1U << ST_RESET_ST0RESET_Pos)             /*!< 0x00000001 */
#define ST_RESET_ST0RESET       ST_RESET_ST0RESET_Msk                       /*!< Simple timer0 reset */

#define ST_RESET_ST1RESET_Pos   (4U)
#define ST_RESET_ST1RESET_Msk   (0x1U << ST_RESET_ST1RESET_Pos)             /*!< 0x00000010 */
#define ST_RESET_ST1RESET       ST_RESET_ST1RESET_Msk                       /*!< Simple timer1 reset */

#define ST_RESET_ST2RESET_Pos   (8U)
#define ST_RESET_ST2RESET_Msk   (0x1U << ST_RESET_ST2RESET_Pos)             /*!< 0x00000100 */
#define ST_RESET_ST2RESET       ST_RESET_ST2RESET_Msk                       /*!< Simple timer2 reset */

#define ST_RESET_ST3RESET_Pos   (12U)
#define ST_RESET_ST3RESET_Msk   (0x1U << ST_RESET_ST3RESET_Pos)             /*!< 0x00001000 */
#define ST_RESET_ST3RESET       ST_RESET_ST3RESET_Msk                       /*!< Simple timer3 reset */

#define ST_RESET_ST4RESET_Pos   (16U)
#define ST_RESET_ST4RESET_Msk   (0x1U << ST_RESET_ST4RESET_Pos)             /*!< 0x00010000 */
#define ST_RESET_ST4RESET       ST_RESET_ST4RESET_Msk                       /*!< Simple timer4 reset */

#define ST_RESET_ST5RESET_Pos   (20U)
#define ST_RESET_ST5RESET_Msk   (0x1U << ST_RESET_ST5RESET_Pos)             /*!< 0x00100000 */
#define ST_RESET_ST5RESET       ST_RESET_ST5RESET_Msk                       /*!< Simple timer5 reset */

/*******************  Bit definition for ST_LOAD register  ********************/
#define ST_LOAD_STLOAD_Pos( n ) (n * 4U)
#define ST_LOAD_STLOAD_Msk( n ) (0x1U << ST_LOAD_STLOAD_Pos( n ) )          /*!< 0x00000001 */
#define ST_LOAD_STLOAD( n )     ST_LOAD_STLOAD_Msk( n )                     /*!< Simple timer n load */

#define ST_LOAD_ST0LOAD_Pos (0U)
#define ST_LOAD_ST0LOAD_Msk (0x1U << ST_LOAD_ST0LOAD_Pos)                   /*!< 0x00000001 */
#define ST_LOAD_ST0LOAD     ST_LOAD_ST0LOAD_Msk                             /*!< Simple timer0 load */

#define ST_LOAD_ST1LOAD_Pos (4U)
#define ST_LOAD_ST1LOAD_Msk (0x1U << ST_LOAD_ST1LOAD_Pos)                   /*!< 0x00000010 */
#define ST_LOAD_ST1LOAD     ST_LOAD_ST1LOAD_Msk                             /*!< Simple timer1 load */

#define ST_LOAD_ST2LOAD_Pos (8U)
#define ST_LOAD_ST2LOAD_Msk (0x1U << ST_LOAD_ST2LOAD_Pos)                   /*!< 0x00000100 */
#define ST_LOAD_ST2LOAD     ST_LOAD_ST2LOAD_Msk                             /*!< Simple timer2 load */

#define ST_LOAD_ST3LOAD_Pos (12U)
#define ST_LOAD_ST3LOAD_Msk (0x1U << ST_LOAD_ST3LOAD_Pos)                   /*!< 0x00001000 */
#define ST_LOAD_ST3LOAD     ST_LOAD_ST3LOAD_Msk                             /*!< Simple timer3 load */

#define ST_LOAD_ST4LOAD_Pos (16U)
#define ST_LOAD_ST4LOAD_Msk (0x1U << ST_LOAD_ST4LOAD_Pos)                   /*!< 0x00010000 */
#define ST_LOAD_ST4LOAD     ST_LOAD_ST4LOAD_Msk                             /*!< Simple timer4 load */

#define ST_LOAD_ST5LOAD_Pos (20U)
#define ST_LOAD_ST5LOAD_Msk (0x1U << ST_LOAD_ST5LOAD_Pos)                   /*!< 0x00100000 */
#define ST_LOAD_ST5LOAD     ST_LOAD_ST5LOAD_Msk                             /*!< Simple timer5 load */

/*******************  Bit definition for STn_PRESCALER register  ********************/
#define ST_PSC_STPSCEN_Pos  (0U)
#define ST_PSC_STPSCEN_Msk  (0x1U << ST_PSC_STPSCEN_Pos)                    /*!< 0x00000001 */
#define ST_PSC_STPSCEN      ST_PSC_STPSCEN_Msk                              /*!< stn_pres_en */

#define ST_PSC_STPSCCFG_Pos (4U)
#define ST_PSC_STPSCCFG_Msk (0xFU << ST_PSC_STPSCCFG_Pos)                   /*!< 0x000000F0 */
#define ST_PSC_STPSCCFG     ST_PSC_STPSCCFG_Msk                             /*!< stn_pres_cfg */

#define ST_PSC_COUNT 6                                                      //number of ST_PRESCALER registors

/*******************  Bit definition for ST_STCFG register  ********************/
#define ST_CFG_MODE_Pos (0U)
#define ST_CFG_MODE_Msk (0x3U << ST_CFG_MODE_Pos)                           /*!< 0x00000003 */
#define ST_CFG_MODE     ST_CFG_MODE_Msk                                     /*!< stx_mode[1:0] Stimerx work mode select */

#define ST_CFG_OSM_Pos  (2U)
#define ST_CFG_OSM_Msk  (0x1U << ST_CFG_OSM_Pos)                            /*!< 0x00000004 */
#define ST_CFG_OSM      ST_CFG_OSM_Msk                                      /*!< stx_one_shot mode */

#define ST_CFG_STOPFZ_Pos   (3U)
#define ST_CFG_STOPFZ_Msk   (0x1U << ST_CFG_STOPFZ_Pos)                     /*!< 0x00000008 */
#define ST_CFG_STOPFZ       ST_CFG_STOPFZ_Msk                               /*!< stx_stop_freeze */

#define ST_CFG_OUTINV_Pos   (4U)
#define ST_CFG_OUTINV_Msk   (0x1U << ST_CFG_OUTINV_Pos)                     /*!< 0x00000010 */
#define ST_CFG_OUTINV       ST_CFG_OUTINV_Msk                               /*!< stx_trout_inv:Stimerx out trigger inverter */

#define ST_CFG_OUTPULSE_Pos (5U)
#define ST_CFG_OUTPULSE_Msk (0x1U << ST_CFG_OUTPULSE_Pos)                   /*!< 0x00000020 */
#define ST_CFG_OUTPULSE     ST_CFG_OUTPULSE_Msk                             /*!< stx_trout_pulse */

#define ST_CFG_INTDMAE_Pos  (6U)
#define ST_CFG_INTDMAE_Msk  (0x1U << ST_CFG_INTDMAE_Pos)                    /*!< 0x00000040 */
#define ST_CFG_INTDMAE      ST_CFG_INTDMAE_Msk                              /*!< Stimerx interrupt DMA enable */

#define ST_CFG_DMASEL_Pos   (7U)
#define ST_CFG_DMASEL_Msk   (0x1U << ST_CFG_DMASEL_Pos)                     /*!< 0x00000080 */
#define ST_CFG_DMASEL       ST_CFG_DMASEL_Msk                               /*!< Stimerx DMA or interrupt select */

#define ST_CFG_CHAIN_Pos    (8U)
#define ST_CFG_CHAIN_Msk    (0x1U << ST_CFG_CHAIN_Pos)                      /*!< 0x00000080 */
#define ST_CFG_CHAIN        ST_CFG_CHAIN_Msk                                /*!< Stimerx st(1~5)_chain_en select */

#define ST_CFG_CPU_DBG_REQ_EN_Pos   (9U)
#define ST_CFG_CPU_DBG_REQ_EN_Msk   (0x1U << ST_CFG_CPU_DBG_REQ_EN_Pos)     /*!< 0x00000200 */
#define ST_CFG_CPU_DBG_REQ_EN       ST_CFG_CPU_DBG_REQ_EN_Msk               /*!< stx_cpu_debug_req_en */

#define ST_CFG_COUNT 6                                                      //number of ST_CFG registors

/*******************  Bit definition for ST_TGS_CHn register  ********************/
#define ST_TGSCH_INEN_Pos   (0U)
#define ST_TGSCH_INEN_Msk   (0x1U << ST_TGSCH_INEN_Pos)                     /*!< 0x00000001 */
#define ST_TGSCH_INEN       ST_TGSCH_INEN_Msk                               /*!< st_chn_in_en:Channel n input enable */

#define ST_TGSCH_PADINV_Pos (1U)
#define ST_TGSCH_PADINV_Msk (0x1U << ST_TGSCH_PADINV_Pos)                   /*!< 0x00000002 */
#define ST_TGSCH_PADINV     ST_TGSCH_PADINV_Msk                             /*!< st_chn_pad_inv:Channel n pad input inverter */

#define ST_TGSCH_PADEDGE_Pos    (2U)
#define ST_TGSCH_PADEDGE_Msk    (0x3U << ST_TGSCH_PADEDGE_Pos)              /*!< 0x0000000C */
#define ST_TGSCH_PADEDGE        ST_TGSCH_PADEDGE_Msk                        /*!<st_chn_pad_edge[1:0]；Channel n pad input edge */

#define ST_TGSCH_PADFILT_Pos    (4U)
#define ST_TGSCH_PADFILT_Msk    (0xFU << ST_TGSCH_PADFILT_Pos)              /*!< 0x000000F0 */
#define ST_TGSCH_PADFILT        ST_TGSCH_PADFILT_Msk                        /*!< st_chn_pad_filter[3:0]:Channel n pad input glitch filter time cycle */

#define ST_TGSCH_INCLKSEL_Pos   (8U)
#define ST_TGSCH_INCLKSEL_Msk   (0x7U << ST_TGSCH_INCLKSEL_Pos)             /*!< 0x00000700 */
#define ST_TGSCH_INCLKSEL       ST_TGSCH_INCLKSEL_Msk                       /*!< st_chn_in_clk_sel[2:0]:Channel n input clk select */

#define ST_TGSCH_INSOURCE_Pos   (11U)
#define ST_TGSCH_INSOURCE_Msk   (0x1FU << ST_TGSCH_INSOURCE_Pos)            /*!< 0x0000F800 */
#define ST_TGSCH_INSOURCE       ST_TGSCH_INSOURCE_Msk                       /*!< st_chn_in_source[4:0]:Channel n input source*/

#define ST_TGSCH_COUNT 8                                                    //number of ST_TGSCH registors

/*******************  Bit definition for ST_TGS_SW_TRIG register  *******************/
#define ST_TGSSWTRIG_CH0_Pos    (0U)
#define ST_TGSSWTRIG_CH0_Msk    (0x1U << ST_TGSSWTRIG_CH0_Pos)              /*!< 0x00000001 */
#define ST_TGSSWTRIG_CH0        ST_TGSSWTRIG_CH0_Msk                        /*!< ch0_sw_trig */

#define ST_TGSSWTRIG_CH1_Pos    (4U)
#define ST_TGSSWTRIG_CH1_Msk    (0x1U << ST_TGSSWTRIG_CH1_Pos)              /*!< 0x00000010 */
#define ST_TGSSWTRIG_CH1        ST_TGSSWTRIG_CH1_Msk                        /*!< ch1_sw_trig */

#define ST_TGSSWTRIG_CH2_Pos    (8U)
#define ST_TGSSWTRIG_CH2_Msk    (0x1U << ST_TGSSWTRIG_CH2_Pos)              /*!< 0x00000100 */
#define ST_TGSSWTRIG_CH2        ST_TGSSWTRIG_CH2_Msk                        /*!< ch2_sw_trig */

#define ST_TGSSWTRIG_CH3_Pos    (12U)
#define ST_TGSSWTRIG_CH3_Msk    (0x1U << ST_TGSSWTRIG_CH3_Pos)              /*!< 0x00001000 */
#define ST_TGSSWTRIG_CH3        ST_TGSSWTRIG_CH3_Msk                        /*!< ch3_sw_trig */

#define ST_TGSSWTRIG_CH4_Pos    (16U)
#define ST_TGSSWTRIG_CH4_Msk    (0x1U << ST_TGSSWTRIG_CH4_Pos)              /*!< 0x00010000 */
#define ST_TGSSWTRIG_CH4        ST_TGSSWTRIG_CH4_Msk                        /*!< ch4_sw_trig */

#define ST_TGSSWTRIG_CH5_Pos    (20U)
#define ST_TGSSWTRIG_CH5_Msk    (0x1U << ST_TGSSWTRIG_CH5_Pos)              /*!< 0x00100000 */
#define ST_TGSSWTRIG_CH5        ST_TGSSWTRIG_CH5_Msk                        /*!< ch5_sw_trig */

#define ST_TGSSWTRIG_CH6_Pos    (24U)
#define ST_TGSSWTRIG_CH6_Msk    (0x1U << ST_TGSSWTRIG_CH6_Pos)              /*!< 0x01000000 */
#define ST_TGSSWTRIG_CH6        ST_TGSSWTRIG_CH6_Msk                        /*!< ch6_sw_trig */

#define ST_TGSSWTRIG_CH7_Pos    (28U)
#define ST_TGSSWTRIG_CH7_Msk    (0x1U << ST_TGSSWTRIG_CH7_Pos)              /*!< 0x10000000 */
#define ST_TGSSWTRIG_CH7        ST_TGSSWTRIG_CH7_Msk                        /*!< ch7_sw_trig */

/*******************  Bit definition for ST_TRIN_CFG register  ********************/
#define ST_TRINCFG_EN_Pos   (0U)
#define ST_TRINCFG_EN_Msk   (0x1U << ST_TRINCFG_EN_Pos)                     /*!< 0x00000001 */
#define ST_TRINCFG_EN       ST_TRINCFG_EN_Msk                               /*!< STx_trig_en Stimerx trig enable */

#define ST_TRINCFG_TGSCH_Pos    (1U)
#define ST_TRINCFG_TGSCH_Msk    (0x7U << ST_TRINCFG_TGSCH_Pos)              /*!< 0x0000000E */
#define ST_TRINCFG_TGSCH        ST_TRINCFG_TGSCH_Msk                        /*!< STx_trig_tgs_ch Stimerx trig tgs channel */

#define ST_TRINCFG_ACTION_Pos   (4U)
#define ST_TRINCFG_ACTION_Msk   (0x3U << ST_TRINCFG_ACTION_Pos)             /*!< 0x00000030 */
#define ST_TRINCFG_ACTION       ST_TRINCFG_ACTION_Msk                       /*!< STx_trig_en Stimerx trig action */

#define ST_TRINCFG_COUNT 6                                                  //number of ST_TRINCFG registors

/*******************  Bit definition for ST_FAULT_CFG register  ********************/
#define ST_FAULTCFG_EN_Pos  (0U)
#define ST_FAULTCFG_EN_Msk  (0x1U << ST_FAULTCFG_EN_Pos)                    /*!< 0x00000001 */
#define ST_FAULTCFG_EN      ST_FAULTCFG_EN_Msk                              /*!< STx_fault_en */

#define ST_FAULTCFG_TGSCH_Pos   (1U)
#define ST_FAULTCFG_TGSCH_Msk   (0x7U << ST_FAULTCFG_TGSCH_Pos)             /*!< 0x0000000E */
#define ST_FAULTCFG_TGSCH       ST_FAULTCFG_TGSCH_Msk                       /*!< STx_fault_tgs_ch */

#define ST_FAULTCFG_RESET_Pos   (4U)
#define ST_FAULTCFG_RESET_Msk   (0x1U << ST_FAULTCFG_RESET_Pos)             /*!< 0x00000010 */
#define ST_FAULTCFG_RESET       ST_FAULTCFG_RESET_Msk                       /*!< STx_fault_reset */

#define ST_FAULTCFG_COUNT 6                                                 //number of ST_FAULTCFG registors

/*******************  Bit definition for ST_STMOD0 register  ********************/
#define ST_MOD0_STMOD0_Pos  (0U)
#define ST_MOD0_STMOD0_Msk  (0xFFFFFFFFU << ST_MOD0_STMOD0_Pos)             /*!< 0xFFFFFFFF */
#define ST_MOD0_STMOD0      ST_MOD0_STMOD0_Msk                              /*!< STxMOD0[31:0] Stimerx mod0 value */

#define ST_MOD0_COUNT 6                                                     //number of STMOD0 registors

/*******************  Bit definition for ST_STMOD1 register  ********************/
#define ST_MOD1_STMOD1_Pos  (0U)
#define ST_MOD1_STMOD1_Msk  (0xFFFFFFFFU << ST_MOD1_STMOD1_Pos)             /*!< 0xFFFFFFFF */
#define ST_MOD1_STMOD1      ST_MOD1_STMOD1_Msk                              /*!< STxMOD1[31:0] Stimerx mod1 value */

#define ST_MOD1_COUNT 6                                                     //number of ST_MOD1 registors

/*******************  Bit definition for ST_CURRENT_VALUE register  ********************/
#define ST_CVAL_STCVAL_Pos  (0U)
#define ST_CVAL_STCVAL_Msk  (0xFFFFFFFFU << ST_STCVAL_STCVAL_Pos)           /*!< 0xFFFFFFFF */
#define ST_CVAL_STCVAL      ST_STCVAL_STCVAL_Msk                            /*!< STxCVAL[31:0] Stimerx current value */

#define ST_CVAL_COUNT 6                                                     //number of ST_CVAL registors

/*******************  Bit definition for ST_FLAG register  ********************/
#define ST_FLAG_STFLAG_Pos( n ) (n * 4U)
#define ST_FLAG_STFLAG_Msk( n ) (0x1U << ST_FLAG_STFLAG_Pos( n ) )          /*!< 0x00000001 */
#define ST_FLAG_STFLAG( n )     ST_FLAG_STFLAG_Msk( n )                     /*!< Stimer n flag status */

#define ST_FLAG_ST0FLAG_Pos (0U)
#define ST_FLAG_ST0FLAG_Msk (0x1U << ST_FLAG_ST0FLAG_Pos)                   /*!< 0x00000001 */
#define ST_FLAG_ST0FLAG     ST_FLAG_ST0FLAG_Msk                             /*!< Stimer0 flag status */

#define ST_FLAG_ST1FLAG_Pos (4U)
#define ST_FLAG_ST1FLAG_Msk (0x1U << ST_FLAG_ST1FLAG_Pos)                   /*!< 0x00000010 */
#define ST_FLAG_ST1FLAG     ST_FLAG_ST1FLAG_Msk                             /*!< Stimer1 flag status */

#define ST_FLAG_ST2FLAG_Pos (8U)
#define ST_FLAG_ST2FLAG_Msk (0x1U << ST_FLAG_ST2FLAG_Pos)                   /*!< 0x00000100 */
#define ST_FLAG_ST2FLAG     ST_FLAG_ST2FLAG_Msk                             /*!< Stimer2 flag status */

#define ST_FLAG_ST3FLAG_Pos (12U)
#define ST_FLAG_ST3FLAG_Msk (0x1U << ST_FLAG_ST3FLAG_Pos)                   /*!< 0x00001000 */
#define ST_FLAG_ST3FLAG     ST_FLAG_ST3FLAG_Msk                             /*!< Stimer3 flag status */

#define ST_FLAG_ST4FLAG_Pos (16U)
#define ST_FLAG_ST4FLAG_Msk (0x1U << ST_FLAG_ST4FLAG_Pos)                   /*!< 0x00010000 */
#define ST_FLAG_ST4FLAG     ST_FLAG_ST4FLAG_Msk                             /*!< Stimer4 flag status */

#define ST_FLAG_ST5FLAG_Pos (20U)
#define ST_FLAG_ST5FLAG_Msk (0x1U << ST_FLAG_ST5FLAG_Pos)                   /*!< 0x00100000 */
#define ST_FLAG_ST5FLAG     ST_FLAG_ST5FLAG_Msk                             /*!< Stimer5 flag status */

/*******************  Bit definition for ST_IFO_CLEAR register  ********************/
#define ST_IFCL_STIFCL_Pos( n ) (n * 4U)
#define ST_IFCL_STIFCL_Msk( n ) (0x1U << ST_IFCL_STIFCL_Pos( n ) )          /*!< 0x00000001 */
#define ST_IFCL_STIFCL( n )     ST_IFCL_STIFCL_Msk( n )                     /*!< Stimer n flag/interrupt clear */

#define ST_IFCL_ST0IFCL_Pos (0U)
#define ST_IFCL_ST0IFCL_Msk (0x1U << ST_IFCL_ST0IFCL_Pos)                   /*!< 0x00000001 */
#define ST_IFCL_ST0IFCL     ST_IFCL_ST0IFCL_Msk                             /*!< Stimer0 flag/interrupt clear */

#define ST_IFCL_ST1IFCL_Pos (4U)
#define ST_IFCL_ST1IFCL_Msk (0x1U << ST_IFCL_ST1IFCL_Pos)                   /*!< 0x00000010 */
#define ST_IFCL_ST1IFCL     ST_IFCL_ST1IFCL_Msk                             /*!< Stimer1 flag/interrupt clear */

#define ST_IFCL_ST2IFCL_Pos (8U)
#define ST_IFCL_ST2IFCL_Msk (0x1U << ST_IFCL_ST2IFCL_Pos)                   /*!< 0x00000100 */
#define ST_IFCL_ST2IFCL     ST_IFCL_ST2IFCL_Msk                             /*!< Stimer2 flag/interrupt clear */

#define ST_IFCL_ST3IFCL_Pos (12U)
#define ST_IFCL_ST3IFCL_Msk (0x1U << ST_IFCL_ST3IFCL_Pos)                   /*!< 0x00001000 */
#define ST_IFCL_ST3IFCL     ST_IFCL_ST3IFCL_Msk                             /*!< Stimer3 flag/interrupt clear */

#define ST_IFCL_ST4IFCL_Pos (16U)
#define ST_IFCL_ST4IFCL_Msk (0x1U << ST_IFCL_ST4IFCL_Pos)                   /*!< 0x00010000 */
#define ST_IFCL_ST4IFCL     ST_IFCL_ST4IFCL_Msk                             /*!< Stimer4 flag/interrupt clear */

#define ST_IFCL_ST5IFCL_Pos (20U)
#define ST_IFCL_ST5IFCL_Msk (0x1U << ST_IFCL_ST5IFCL_Pos)                   /*!< 0x00100000 */
#define ST_IFCL_ST5IFCL     ST_IFCL_ST5IFCL_Msk                             /*!< Stimer5 flag/interrupt clear */
//</h>
//<h>TGS
/******************************************************************************/
/*                                                                            */
/*                                    TGS                                     */
/*                                                                            */
/******************************************************************************/
/*******************  Bit definition for TGS_CH0CTRL register  ********************/
#define TGS_CH0CTRL_PATHM_Pos   (0U)
#define TGS_CH0CTRL_PATHM_Msk   (0x3U << TGS_CH0CTRL_PATHM_Pos)                     /*!< 0x00000003 */
#define TGS_CH0CTRL_PATHM       TGS_CH0CTRL_PATHM_Msk                               /*!<PATHM[1:0] Channel 0 path mode select */

#define TGS_CH0CTRL_TROUTM_Pos  (2U)
#define TGS_CH0CTRL_TROUTM_Msk  (0x3U << TGS_CH0CTRL_TROUTM_Pos)                    /*!< 0x0000000C */
#define TGS_CH0CTRL_TROUTM      TGS_CH0CTRL_TROUTM_Msk                              /*!<TROUTM[1:0] Channel 0 trgger out mode select */

#define TGS_CH0CTRL_TROUTW_Pos  (8U)
#define TGS_CH0CTRL_TROUTW_Msk  (0xFFU << TGS_CH0CTRL_TROUTW_Pos)                   /*!< 0x0000FF00 */
#define TGS_CH0CTRL_TROUTW      TGS_CH0CTRL_TROUTW_Msk                              /*!<TROUTW[7:0] Channel 0 trgger out width select */

#define TGS_CH0CTRL_TRININV_Pos (16U)
#define TGS_CH0CTRL_TRININV_Msk (0x1U << TGS_CH0CTRL_TRININV_Pos)                   /*!< 0x00010000 */
#define TGS_CH0CTRL_TRININV     TGS_CH0CTRL_TRININV_Msk                             /*!< Channel 0 trgger input inverter */

#define TGS_CH0CTRL_TRINEDGE_Pos    (17U)
#define TGS_CH0CTRL_TRINEDGE_Msk    (0x3U << TGS_CH0CTRL_TRINEDGE_Pos)              /*!< 0x00060000 */
#define TGS_CH0CTRL_TRINEDGE        TGS_CH0CTRL_TRINEDGE_Msk                        /*!<TRINEDGE[1:0] Channel 0 trgger edge select */

#define TGS_CH0CTRL_TRINFILT_Pos    (20U)
#define TGS_CH0CTRL_TRINFILT_Msk    (0xFU << TGS_CH0CTRL_TRINFILT_Pos)              /*!< 0x00F00000 */
#define TGS_CH0CTRL_TRINFILT        TGS_CH0CTRL_TRINFILT_Msk                        /*!<TRINFILT[3:0] Channel 0 trgger filter configre */

#define TGS_CH0CTRL_INTEN_Pos   (24U)
#define TGS_CH0CTRL_INTEN_Msk   (0x1U << TGS_CH0CTRL_INTEN_Pos)                     /*!< 0x00010000 */
#define TGS_CH0CTRL_INTEN       TGS_CH0CTRL_INTEN_Msk                               /*!< Channel 0 interrupt enable */

/*******************  Bit definition for TGS_CH0GEN register  ********************/
#define TGS_CH0GEN_GENSEL_Pos   (0U)
#define TGS_CH0GEN_GENSEL_Msk   (0xFFU << TGS_CH0GEN_GENSEL_Pos)                    /*!< 0x000000FF */
#define TGS_CH0GEN_GENSEL       TGS_CH0GEN_GENSEL_Msk                               /*!<GENSEL[7:0] Channel 0 trigger generator source select */

/*******************  Bit definition for TGS_CH0SWCTRL register  ********************/
#define TGS_CH0SWCTRL_SWCTRL_Pos    (0U)
#define TGS_CH0SWCTRL_SWCTRL_Msk    (0x1U << TGS_CH0SWCTRL_SWCTRL_Pos)              /*!< 0x00000001 */
#define TGS_CH0SWCTRL_SWCTRL        TGS_CH0SWCTRL_SWCTRL_Msk                        /*!< Channel 0 software control value */

/*******************  Bit definition for TGS_CH1CTRL register  ********************/
#define TGS_CH1CTRL_PATHM_Pos   (0U)
#define TGS_CH1CTRL_PATHM_Msk   (0x3U << TGS_CH1CTRL_PATHM_Pos)                     /*!< 0x00000003 */
#define TGS_CH1CTRL_PATHM       TGS_CH1CTRL_PATHM_Msk                               /*!<PATHM[1:0] Channel 1 path mode select */

#define TGS_CH1CTRL_TROUTM_Pos  (2U)
#define TGS_CH1CTRL_TROUTM_Msk  (0x3U << TGS_CH1CTRL_TROUTM_Pos)                    /*!< 0x0000000C */
#define TGS_CH1CTRL_TROUTM      TGS_CH1CTRL_TROUTM_Msk                              /*!<TROUTM[1:0] Channel 1 trgger out mode select */

#define TGS_CH1CTRL_TROUTW_Pos  (8U)
#define TGS_CH1CTRL_TROUTW_Msk  (0xFFU << TGS_CH1CTRL_TROUTW_Pos)                   /*!< 0x0000FF00 */
#define TGS_CH1CTRL_TROUTW      TGS_CH1CTRL_TROUTW_Msk                              /*!<TROUTW[7:0] Channel 1 trgger out width select */

#define TGS_CH1CTRL_TRININV_Pos (16U)
#define TGS_CH1CTRL_TRININV_Msk (0x1U << TGS_CH1CTRL_TRININV_Pos)                   /*!< 0x00010000 */
#define TGS_CH1CTRL_TRININV     TGS_CH1CTRL_TRININV_Msk                             /*!< Channel 1 trgger input inverter */

#define TGS_CH1CTRL_TRINEDGE_Pos    (17U)
#define TGS_CH1CTRL_TRINEDGE_Msk    (0x3U << TGS_CH1CTRL_TRINEDGE_Pos)              /*!< 0x00060000 */
#define TGS_CH1CTRL_TRINEDGE        TGS_CH1CTRL_TRINEDGE_Msk                        /*!<TRINEDGE[1:0] Channel 1 trgger edge select */

#define TGS_CH1CTRL_TRINFILT_Pos    (20U)
#define TGS_CH1CTRL_TRINFILT_Msk    (0xFU << TGS_CH1CTRL_TRINFILT_Pos)              /*!< 0x00F00000 */
#define TGS_CH1CTRL_TRINFILT        TGS_CH1CTRL_TRINFILT_Msk                        /*!<TRINFILT[3:0] Channel 1 trgger filter configre */

#define TGS_CH1CTRL_INTEN_Pos   (24U)
#define TGS_CH1CTRL_INTEN_Msk   (0x1U << TGS_CH1CTRL_INTEN_Pos)                     /*!< 0x00010000 */
#define TGS_CH1CTRL_INTEN       TGS_CH1CTRL_INTEN_Msk                               /*!< Channel 1 interrupt enable */

/*******************  Bit definition for TGS_CH1GEN register  ********************/
#define TGS_CH1GEN_GENSEL_Pos   (0U)
#define TGS_CH1GEN_GENSEL_Msk   (0xFFU << TGS_CH1GEN_GENSEL_Pos)                    /*!< 0x000000FF */
#define TGS_CH1GEN_GENSEL       TGS_CH1GEN_GENSEL_Msk                               /*!<GENSEL[7:0] Channel 1 trigger generator source select */

/*******************  Bit definition for TGS_CH1SWCTRL register  ********************/
#define TGS_CH1SWCTRL_SWCTRL_Pos    (0U)
#define TGS_CH1SWCTRL_SWCTRL_Msk    (0x1U << TGS_CH1SWCTRL_SWCTRL_Pos)              /*!< 0x00000001 */
#define TGS_CH1SWCTRL_SWCTRL        TGS_CH1SWCTRL_SWCTRL_Msk                        /*!< Channel 1 software control value */

/*******************  Bit definition for TGS_CH2CTRL register  ********************/
#define TGS_CH2CTRL_PATHM_Pos   (0U)
#define TGS_CH2CTRL_PATHM_Msk   (0x3U << TGS_CH2CTRL_PATHM_Pos)                     /*!< 0x00000003 */
#define TGS_CH2CTRL_PATHM       TGS_CH2CTRL_PATHM_Msk                               /*!<PATHM[1:0] Channel 2 path mode select */

#define TGS_CH2CTRL_TROUTM_Pos  (2U)
#define TGS_CH2CTRL_TROUTM_Msk  (0x3U << TGS_CH2CTRL_TROUTM_Pos)                    /*!< 0x0000000C */
#define TGS_CH2CTRL_TROUTM      TGS_CH2CTRL_TROUTM_Msk                              /*!<TROUTM[1:0] Channel 2 trgger out mode select */

#define TGS_CH2CTRL_TROUTW_Pos  (8U)
#define TGS_CH2CTRL_TROUTW_Msk  (0xFFU << TGS_CH2CTRL_TROUTW_Pos)                   /*!< 0x0000FF00 */
#define TGS_CH2CTRL_TROUTW      TGS_CH2CTRL_TROUTW_Msk                              /*!<TROUTW[7:0] Channel 2 trgger out width select */

#define TGS_CH2CTRL_TRININV_Pos (16U)
#define TGS_CH2CTRL_TRININV_Msk (0x1U << TGS_CH2CTRL_TRININV_Pos)                   /*!< 0x00010000 */
#define TGS_CH2CTRL_TRININV     TGS_CH2CTRL_TRININV_Msk                             /*!< Channel 2 trgger input inverter */

#define TGS_CH2CTRL_TRINEDGE_Pos    (17U)
#define TGS_CH2CTRL_TRINEDGE_Msk    (0x3U << TGS_CH2CTRL_TRINEDGE_Pos)              /*!< 0x00060000 */
#define TGS_CH2CTRL_TRINEDGE        TGS_CH2CTRL_TRINEDGE_Msk                        /*!<TRINEDGE[1:0] Channel 2 trgger edge select */

#define TGS_CH2CTRL_TRINFILT_Pos    (20U)
#define TGS_CH2CTRL_TRINFILT_Msk    (0xFU << TGS_CH2CTRL_TRINFILT_Pos)              /*!< 0x00F00000 */
#define TGS_CH2CTRL_TRINFILT        TGS_CH2CTRL_TRINFILT_Msk                        /*!<TRINFILT[3:0] Channel 2 trgger filter configre */

#define TGS_CH2CTRL_INTEN_Pos   (24U)
#define TGS_CH2CTRL_INTEN_Msk   (0x1U << TGS_CH2CTRL_INTEN_Pos)                     /*!< 0x00010000 */
#define TGS_CH2CTRL_INTEN       TGS_CH2CTRL_INTEN_Msk                               /*!< Channel 2 interrupt enable */

/*******************  Bit definition for TGS_CH2GEN register  ********************/
#define TGS_CH2GEN_GENSEL_Pos   (0U)
#define TGS_CH2GEN_GENSEL_Msk   (0xFFU << TGS_CH2GEN_GENSEL_Pos)                    /*!< 0x000000FF */
#define TGS_CH2GEN_GENSEL       TGS_CH2GEN_GENSEL_Msk                               /*!<GENSEL[7:0] Channel 2 trigger generator source select */

/*******************  Bit definition for TGS_CH2SWCTRL register  ********************/
#define TGS_CH2SWCTRL_SWCTRL_Pos    (0U)
#define TGS_CH2SWCTRL_SWCTRL_Msk    (0x1U << TGS_CH2SWCTRL_SWCTRL_Pos)              /*!< 0x00000001 */
#define TGS_CH2SWCTRL_SWCTRL        TGS_CH2SWCTRL_SWCTRL_Msk                        /*!< Channel 2 software control value */

/*******************  Bit definition for TGS_CH3CTRL register  ********************/
#define TGS_CH3CTRL_PATHM_Pos   (0U)
#define TGS_CH3CTRL_PATHM_Msk   (0x3U << TGS_CH3CTRL_PATHM_Pos)                     /*!< 0x00000003 */
#define TGS_CH3CTRL_PATHM       TGS_CH3CTRL_PATHM_Msk                               /*!<PATHM[1:0] Channel 2 path mode select */

#define TGS_CH3CTRL_TROUTM_Pos  (2U)
#define TGS_CH3CTRL_TROUTM_Msk  (0x3U << TGS_CH3CTRL_TROUTM_Pos)                    /*!< 0x0000000C */
#define TGS_CH3CTRL_TROUTM      TGS_CH3CTRL_TROUTM_Msk                              /*!<TROUTM[1:0] Channel 2 trgger out mode select */

#define TGS_CH3CTRL_TROUTW_Pos  (8U)
#define TGS_CH3CTRL_TROUTW_Msk  (0xFFU << TGS_CH3CTRL_TROUTW_Pos)                   /*!< 0x0000FF00 */
#define TGS_CH3CTRL_TROUTW      TGS_CH3CTRL_TROUTW_Msk                              /*!<TROUTW[7:0] Channel 2 trgger out width select */

#define TGS_CH3CTRL_TRININV_Pos (16U)
#define TGS_CH3CTRL_TRININV_Msk (0x1U << TGS_CH3CTRL_TRININV_Pos)                   /*!< 0x00010000 */
#define TGS_CH3CTRL_TRININV     TGS_CH3CTRL_TRININV_Msk                             /*!< Channel 2 trgger input inverter */

#define TGS_CH3CTRL_TRINEDGE_Pos    (17U)
#define TGS_CH3CTRL_TRINEDGE_Msk    (0x3U << TGS_CH3CTRL_TRINEDGE_Pos)              /*!< 0x00060000 */
#define TGS_CH3CTRL_TRINEDGE        TGS_CH3CTRL_TRINEDGE_Msk                        /*!<TRINEDGE[1:0] Channel 2 trgger edge select */

#define TGS_CH3CTRL_TRINFILT_Pos    (20U)
#define TGS_CH3CTRL_TRINFILT_Msk    (0xFU << TGS_CH3CTRL_TRINFILT_Pos)              /*!< 0x00F00000 */
#define TGS_CH3CTRL_TRINFILT        TGS_CH3CTRL_TRINFILT_Msk                        /*!<TRINFILT[3:0] Channel 2 trgger filter configre */

#define TGS_CH3CTRL_INTEN_Pos   (24U)
#define TGS_CH3CTRL_INTEN_Msk   (0x1U << TGS_CH3CTRL_INTEN_Pos)                     /*!< 0x00010000 */
#define TGS_CH3CTRL_INTEN       TGS_CH3CTRL_INTEN_Msk                               /*!< Channel 2 interrupt enable */

/*******************  Bit definition for TGS_CH3GEN register  ********************/
#define TGS_CH3GEN_GENSEL_Pos   (0U)
#define TGS_CH3GEN_GENSEL_Msk   (0xFFU << TGS_CH3GEN_GENSEL_Pos)                    /*!< 0x000000FF */
#define TGS_CH3GEN_GENSEL       TGS_CH3GEN_GENSEL_Msk                               /*!<GENSEL[7:0] Channel 2 trigger generator source select */

/*******************  Bit definition for TGS_CH3SWCTRL register  ********************/
#define TGS_CH3SWCTRL_SWCTRL_Pos    (0U)
#define TGS_CH3SWCTRL_SWCTRL_Msk    (0x1U << TGS_CH3SWCTRL_SWCTRL_Pos)              /*!< 0x00000001 */
#define TGS_CH3SWCTRL_SWCTRL        TGS_CH3SWCTRL_SWCTRL_Msk                        /*!< Channel 2 software control value */

/*******************  Bit definition for TGS_UXXXCFG register  ********************/
#define TGS_UXXXCFG_TGSEN_Pos   (0U)
#define TGS_UXXXCFG_TGSEN_Msk   (0x1U << TGS_UXXXCFG_TGSEN_Pos)                     /*!< 0x00000001 */
#define TGS_UXXXCFG_TGSEN       TGS_UXXXCFG_TGSEN_Msk                               /*!< User xxx(pad...) channel enable */

#define TGS_UXXXCFG_TGSCH_Pos   (1U)
#define TGS_UXXXCFG_TGSCH_Msk   (0x3U << TGS_UXXXCFG_TGSCH_Pos)                     /*!< 0x00000006 */
#define TGS_UXXXCFG_TGSCH       TGS_UXXXCFG_TGSCH_Msk                               /*!<TGS[1:0] User xxx(pad...) channel select */

/* The count of TGS_UXXXCFG */
#define TGS_UXXXCFG_COUNT (44U)

#define TGS_FLAG_CH0FLAG_Pos    (0U)
#define TGS_FLAG_CH0FLAG_Msk    (0x1U << TGS_FLAG_CH0FLAG_Pos)                      /*!< 0x00000001 */
#define TGS_FLAG_CH0FLAG        TGS_FLAG_CH0FLAG_Msk                                /*!< Channel 0 flag */

#define TGS_FLAG_CH1FLAG_Pos    (1U)
#define TGS_FLAG_CH1FLAG_Msk    (0x1U << TGS_FLAG_CH1FLAG_Pos)                      /*!< 0x00000002 */
#define TGS_FLAG_CH1FLAG        TGS_FLAG_CH1FLAG_Msk                                /*!< Channel 1 flag */

#define TGS_FLAG_CH2FLAG_Pos    (2U)
#define TGS_FLAG_CH2FLAG_Msk    (0x1U << TGS_FLAG_CH2FLAG_Pos)                      /*!< 0x00000004 */
#define TGS_FLAG_CH2FLAG        TGS_FLAG_CH2FLAG_Msk                                /*!< Channel 2 flag */

#define TGS_FLAG_CH3FLAG_Pos    (3U)
#define TGS_FLAG_CH3FLAG_Msk    (0x1U << TGS_FLAG_CH3FLAG_Pos)                      /*!< 0x00000008 */
#define TGS_FLAG_CH3FLAG        TGS_FLAG_CH3FLAG_Msk                                /*!< Channel 3 flag */

/*******************  Bit definition for TGS_FLAGINT register  ********************/
#define TGS_FLAGINT_CH0INT_Pos  (0U)
#define TGS_FLAGINT_CH0INT_Msk  (0x1U << TGS_FLAGINT_CH0INT_Pos)                    /*!< 0x00000001 */
#define TGS_FLAGINT_CH0INT      TGS_FLAGINT_CH0INT_Msk                              /*!< Channel 0 interrupt flag */

#define TGS_FLAGINT_CH1INT_Pos  (1U)
#define TGS_FLAGINT_CH1INT_Msk  (0x1U << TGS_FLAGINT_CH1INT_Pos)                    /*!< 0x00000002 */
#define TGS_FLAGINT_CH1INT      TGS_FLAGINT_CH1INT_Msk                              /*!< Channel 1 interrupt flag */

#define TGS_FLAGINT_CH2INT_Pos  (2U)
#define TGS_FLAGINT_CH2INT_Msk  (0x1U << TGS_FLAGINT_CH2INT_Pos)                    /*!< 0x00000004 */
#define TGS_FLAGINT_CH2INT      TGS_FLAGINT_CH2INT_Msk                              /*!< Channel 2 interrupt flag */

#define TGS_FLAGINT_CH3INT_Pos  (3U)
#define TGS_FLAGINT_CH3INT_Msk  (0x1U << TGS_FLAGINT_CH3INT_Pos)                    /*!< 0x00000008 */
#define TGS_FLAGINT_CH3INT      TGS_FLAGINT_CH3INT_Msk                              /*!< Channel 3 interrupt flag */

/*******************  Bit definition for TGS_IFCLEAR register  ********************/
#define TGS_IFCLEAR_CH0IFC_Pos  (0U)
#define TGS_IFCLEAR_CH0IFC_Msk  (0x1U << TGS_IFCLEAR_CH0IFC_Pos)                    /*!< 0x00000001 */
#define TGS_IFCLEAR_CH0IFC      TGS_IFCLEAR_CH0IFC_Msk                              /*!< Channel 0 interrupt flag clear*/

#define TGS_IFCLEAR_CH1IFC_Pos  (1U)
#define TGS_IFCLEAR_CH1IFC_Msk  (0x1U << TGS_IFCLEAR_CH1IFC_Pos)                    /*!< 0x00000002 */
#define TGS_IFCLEAR_CH1IFC      TGS_IFCLEAR_CH1IFC_Msk                              /*!< Channel 1 interrupt flag clear*/

#define TGS_IFCLEAR_CH2IFC_Pos  (2U)
#define TGS_IFCLEAR_CH2IFC_Msk  (0x1U << TGS_IFCLEAR_CH2IFC_Pos)                    /*!< 0x00000004 */
#define TGS_IFCLEAR_CH2IFC      TGS_IFCLEAR_CH2IFC_Msk                              /*!< Channel 2 interrupt flag clear*/

#define TGS_IFCLEAR_CH3IFC_Pos  (3U)
#define TGS_IFCLEAR_CH3IFC_Msk  (0x1U << TGS_IFCLEAR_CH3IFC_Pos)                    /*!< 0x00000008 */
#define TGS_IFCLEAR_CH3IFC      TGS_IFCLEAR_CH3IFC_Msk                              /*!< Channel 3 interrupt flag clear*/
//</h>
//<h>UART

/******************************************************************************/
/*                                                                            */
/*                                    UART_CSR                                    */
/*                                                                            */
/******************************************************************************/
/*******************   Bit definition for UART_CTRL register   ********************/
#define UART_CR_RXEN_Pos    (0U)
#define UART_CR_RXEN_Msk    (0x1U << UART_CR_RXEN_Pos)                              /*!< 0x00000001 */
#define UART_CR_RXEN        UART_CR_RXEN_Msk                                        /*!<rxen registor */

#define UART_CR_TXEN_Pos    (1U)
#define UART_CR_TXEN_Msk    (0x1U << UART_CR_TXEN_Pos)                              /*!< 0x00000002 */
#define UART_CR_TXEN        UART_CR_TXEN_Msk                                        /*!<txen registor */

#define UART_CR_PDSEL_Pos   (2U)
#define UART_CR_PDSEL_Msk   (0x3U << UART_CR_PDSEL_Pos)                             /*!< 0x0000000C */
#define UART_CR_PDSEL       UART_CR_PDSEL_Msk                                       /*!<pdsel[1:0] registor */

#define UART_CR_STOPSEL_Pos (4U)
#define UART_CR_STOPSEL_Msk (0x1U << UART_CR_STOPSEL_Pos)                           /*!< 0x00000010 */
#define UART_CR_STOPSEL     UART_CR_STOPSEL_Msk                                     /*!<stopsel registor */

/*******************   Bit definition for UART_bit9CR register   ********************/
#define UART_BIT9CR_TX9D_Pos    (16U)
#define UART_BIT9CR_TX9D_Msk    (0x1U << UART_BIT9CR_TX9D_Pos)                      /*!< 0x00000001 */
#define UART_BIT9CR_TX9D        UART_BIT9CR_TX9D_Msk                                /*!<tx9d registor */

#define UART_BIT9CR_RX9D_Pos    (1U)
#define UART_BIT9CR_RX9D_Msk    (0x1U << UART_BIT9CR_RX9D_Pos)                      /*!< 0x00000002 */
#define UART_BIT9CR_RX9D        UART_BIT9CR_RX9D_Msk                                /*!<rx9d registor */

/*******************   Bit definition for UART_int_cfg register   ********************/
#define UART_INTCFG_RXFNEIE_Pos (0U)
#define UART_INTCFG_RXFNEIE_Msk (0x1U << UART_INTCFG_RXFNEIE_Pos)                   /*!< 0x00000001 */
#define UART_INTCFG_RXFNEIE     UART_INTCFG_RXFNEIE_Msk                             /*!<rxf_ne_ie registor */

#define UART_INTCFG_RXFHFIE_Pos (1U)
#define UART_INTCFG_RXFHFIE_Msk (0x1U << UART_INTCFG_RXFHFIE_Pos)                   /*!< 0x00000002 */
#define UART_INTCFG_RXFHFIE     UART_INTCFG_RXFHFIE_Msk                             /*!<rxf_hf_ie registor */

#define UART_INTCFG_TXFEIE_Pos  (2U)
#define UART_INTCFG_TXFEIE_Msk  (0x1U << UART_INTCFG_TXFEIE_Pos)                    /*!< 0x00000004 */
#define UART_INTCFG_TXFEIE      UART_INTCFG_TXFEIE_Msk                              /*!<txf_e_ie registor */

#define UART_INTCFG_TXFHEIE_Pos (3U)
#define UART_INTCFG_TXFHEIE_Msk (0x1U << UART_INTCFG_TXFHEIE_Pos)                   /*!< 0x00000008 */
#define UART_INTCFG_TXFHEIE     UART_INTCFG_TXFHEIE_Msk                             /*!<txf_he_ie registor */

#define UART_INTCFG_TXDONEIE_Pos    (4U)
#define UART_INTCFG_TXDONEIE_Msk    (0x1U << UART_INTCFG_TXDONEIE_Pos)              /*!< 0x00000010 */
#define UART_INTCFG_TXDONEIE        UART_INTCFG_TXDONEIE_Msk                        /*!<tx_done_ie registor */

#define UART_INTCFG_ERRIE_Pos   (5U)
#define UART_INTCFG_ERRIE_Msk   (0x1U << UART_INTCFG_ERRIE_Pos)                     /*!< 0x00000020 */
#define UART_INTCFG_ERRIE       UART_INTCFG_ERRIE_Msk                               /*!<err_ie registor */

#define UART_INTCFG_RXFNEDE_Pos (6U)
#define UART_INTCFG_RXFNEDE_Msk (0x1U << UART_INTCFG_RXFNEDE_Pos)                   /*!< 0x00000040 */
#define UART_INTCFG_RXFNEDE     UART_INTCFG_RXFNEDE_Msk                             /*!<txf_ne_de registor */

#define UART_INTCFG_TXFEDE_Pos  (7U)
#define UART_INTCFG_TXFEDE_Msk  (0x1U << UART_INTCFG_TXFEDE_Pos)                    /*!< 0x00000080 */
#define UART_INTCFG_TXFEDE      UART_INTCFG_TXFEDE_Msk                              /*!<txf_e_de registor */

/*******************   Bit definition for UART_status register   ********************/
#define UART_STATUS_TXDONE_Pos  (0U)
#define UART_STATUS_TXDONE_Msk  (0x1U << UART_STATUS_TXDONE_Pos)                    /*!< 0x00000001 */
#define UART_STATUS_TXDONE      UART_STATUS_TXDONE_Msk                              /* tx_done */

#define UART_STATUS_RXBUSY_Pos  (1U)
#define UART_STATUS_RXBUSY_Msk  (0x1U << UART_STATUS_RXBUSY_Pos)                    /*!< 0x00000002 */
#define UART_STATUS_RXBUSY      UART_STATUS_RXBUSY_Msk                              /* rx_busy */

#define UART_STATUS_TXFIFOE_Pos (2U)
#define UART_STATUS_TXFIFOE_Msk (0x1U << UART_STATUS_TXFIFOE_Pos)                   /*!< 0x00000004 */
#define UART_STATUS_TXFIFOE     UART_STATUS_TXFIFOE_Msk                             /* txfifo_e */

#define UART_STATUS_TXFIFOHE_Pos    (3U)
#define UART_STATUS_TXFIFOHE_Msk    (0x1U << UART_STATUS_TXFIFOHE_Pos)              /*!< 0x00000008 */
#define UART_STATUS_TXFIFOHE        UART_STATUS_TXFIFOHE_Msk                        /* txfifo_he */

#define UART_STATUS_TXFIFOS_Pos (4U)
#define UART_STATUS_TXFIFOS_Msk (0x7U << UART_STATUS_TXFIFOS_Pos)                   /*!< 0x00000070 */
#define UART_STATUS_TXFIFOS     UART_STATUS_TXFIFOS_Msk                             /* txfifo_s */

#define UART_STATUS_RXFIFONE_Pos    (7U)
#define UART_STATUS_RXFIFONE_Msk    (0x1U << UART_STATUS_RXFIFONE_Pos)              /*!< 0x00000080 */
#define UART_STATUS_RXFIFONE        UART_STATUS_RXFIFONE_Msk                        /* rxfifo_ne */

#define UART_STATUS_RXFIFOHF_Pos    (8U)
#define UART_STATUS_RXFIFOHF_Msk    (0x1U << UART_STATUS_RXFIFOHF_Pos)              /*!< 0x00000100 */
#define UART_STATUS_RXFIFOHF        UART_STATUS_RXFIFOHF_Msk                        /* rxfifo_hf */

#define UART_STATUS_RXFIFOS_Pos (9U)
#define UART_STATUS_RXFIFOS_Msk (0x7U << UART_STATUS_RXFIFOS_Pos)                   /*!< 0x00000e00 */
#define UART_STATUS_RXFIFOS     UART_STATUS_RXFIFOS_Msk                             /* rxfifo_s */

#define UART_STATUS_TXFOERR_Pos (12U)
#define UART_STATUS_TXFOERR_Msk (0x1U << UART_STATUS_TXFOERR_Pos)                   /*!< 0x00001000 */
#define UART_STATUS_TXFOERR     UART_STATUS_TXFOERR_Msk                             /* txf_oerr */

#define UART_STATUS_RXFOERR_Pos (13U)
#define UART_STATUS_RXFOERR_Msk (0x1U << UART_STATUS_RXFOERR_Pos)                   /*!< 0x00002000 */
#define UART_STATUS_RXFOERR     UART_STATUS_RXFOERR_Msk                             /* rxf_oerr */

#define UART_STATUS_FERR_Pos    (14U)
#define UART_STATUS_FERR_Msk    (0x1U << UART_STATUS_FERR_Pos)                      /*!< 0x00004000 */
#define UART_STATUS_FERR        UART_STATUS_FERR_Msk                                /* ferr */

#define UART_STATUS_PERR_Pos    (15U)
#define UART_STATUS_PERR_Msk    (0x1U << UART_STATUS_PERR_Pos)                      /*!< 0x00008000 */
#define UART_STATUS_PERR        UART_STATUS_PERR_Msk                                /* perr */

/*******************   Bit definition for UART_rx_reg register   ********************/
#define UART_RXREG_RXFIFO_Pos   (0U)
#define UART_RXREG_RXFIFO_Msk   (0xFFU << UART_RXREG_RXFIFO_Pos)                    /*!< 0x000000ff */
#define UART_RXREG_RXFIFO       UART_RXREG_RXFIFO_Msk                               /* rx_fifo */

/*******************   Bit definition for UART_tx_fifo register   ********************/
#define UART_TXREG_TXFIFO_Pos   (0U)
#define UART_TXREG_TXFIFO_Msk   (0xFFU << UART_TXREG_TXFIFO_Pos)                    /*!< 0x000000ff */
#define UART_TXREG_TXFIFO       UART_TXREG_TXFIFO_Msk                               /* tx_fifo */

/*******************   Bit definition for UART_acr register   ********************/
#define UART_ACR_AUTOMODE_Pos   (0U)
#define UART_ACR_AUTOMODE_Msk   (0x1U << UART_ACR_AUTOMODE_Pos)                     /*!< 0x00000001 */
#define UART_ACR_AUTOMODE       UART_ACR_AUTOMODE_Msk                               /* auto_mode */

#define UART_ACR_START_Pos  (1U)
#define UART_ACR_START_Msk  (0x1U << UART_ACR_START_Pos)                            /*!< 0x00000002 */
#define UART_ACR_START      UART_ACR_START_Msk                                      /* start*/

#define UART_ACR_AUTORST_Pos    (2U)
#define UART_ACR_AUTORST_Msk    (0x1U << UART_ACR_AUTORST_Pos)                      /*!< 0x00000004 */
#define UART_ACR_AUTORST        UART_ACR_AUTORST_Msk                                /* auto_restart */

/*******************   Bit definition for UART_speed_cfg register   ********************/
#define UART_SPEED_SPBRGL_Pos   (0U)
#define UART_SPEED_SPBRGL_Msk   (0xFFU << UART_SPEED_SPBRGL_Pos)                    /*!< 0x000000ff */
#define UART_SPEED_SPBRGL       UART_SPEED_SPBRGL_Msk                               /* spbrg_l */

#define UART_SPEED_SPBRGH_Pos   (8U)
#define UART_SPEED_SPBRGH_Msk   (0xFFU << UART_SPEED_SPBRGH_Pos)                    /*!< 0x0000ff00 */
#define UART_SPEED_SPBRGH       UART_SPEED_SPBRGH_Msk                               /* spbrg_h */

#define UART_SPEED_BRADJ_Pos    (16U)
#define UART_SPEED_BRADJ_Msk    (0xFU << UART_SPEED_BRADJ_Pos)                      /*!< 0x000f0000 */
#define UART_SPEED_BRADJ        UART_SPEED_BRADJ_Msk                                /* bradj */

#define UART_SPEED_BRGH_Pos (20U)
#define UART_SPEED_BRGH_Msk (0x3U << UART_SPEED_BRGH_Pos)                           /*!< 0x00300000 */
#define UART_SPEED_BRGH     UART_SPEED_BRGH_Msk                                     /* brgh */
//</h>
//<h>USB_PHY

/******************************************************************************/
/*                                                                            */
/*                                    USB_PHY                                 */
/*                                                                            */
/******************************************************************************/
/*******************  Bit definition for USB_PHY_RAMBISTCFG register  ********************/
#define USB_PHY_RAMBISTCFG_MARCHC_Pos   (0U)
#define USB_PHY_RAMBISTCFG_MARCHC_Msk   (0x1U << USB_PHY_RAMBISTCFG_MARCHC_Pos)             /*!< 0x00000001 */
#define USB_PHY_RAMBISTCFG_MARCHC       USB_PHY_RAMBISTCFG_MARCHC_Msk                       /*!<bist_marchc_start_usbram */

#define USB_PHY_RAMBISTCFG_DECODE_Pos   (1U)
#define USB_PHY_RAMBISTCFG_DECODE_Msk   (0x1U << USB_PHY_RAMBISTCFG_DECODE_Pos)             /*!< 0x00000002 */
#define USB_PHY_RAMBISTCFG_DECODE       USB_PHY_RAMBISTCFG_DECODE_Msk                       /*!<bist_decoder_start_usbram */

#define USB_PHY_RAMBISTCFG_BWE_Pos  (2U)
#define USB_PHY_RAMBISTCFG_BWE_Msk  (0x1U << USB_PHY_RAMBISTCFG_BWE_Pos)                    /*!< 0x00000004 */
#define USB_PHY_RAMBISTCFG_BWE      USB_PHY_RAMBISTCFG_BWE_Msk                              /*!<bist_bwe_start_usbram */

#define USB_PHY_RAMBISTCFG_EN_Pos   (3U)
#define USB_PHY_RAMBISTCFG_EN_Msk   (0x1U << USB_PHY_RAMBISTCFG_EN_Pos)                     /*!< 0x00000008 */
#define USB_PHY_RAMBISTCFG_EN       USB_PHY_RAMBISTCFG_EN_Msk                               /*!<usbram_bist_en */

/*******************  Bit definition for USB_PHY_RAMBISTS register  ********************/
#define USB_PHY_RAMBISTS_MARCHERR_Pos   (0U)
#define USB_PHY_RAMBISTS_MARCHERR_Msk   (0x1U << USB_PHY_RAMBISTS_MARCHERR_Pos)             /*!< 0x00000001 */
#define USB_PHY_RAMBISTS_MARCHERR       USB_PHY_RAMBISTS_MARCHERR_Msk                       /*!<marchc_error_usbram */

#define USB_PHY_RAMBISTS_UNMMARCH_Pos   (1U)
#define USB_PHY_RAMBISTS_UNMMARCH_Msk   (0x1U << USB_PHY_RAMBISTS_MARCHERR_Pos)             /*!< 0x00000002 */
#define USB_PHY_RAMBISTS_UNMMARCH       USB_PHY_RAMBISTS_MARCHERR_Msk                       /*!<unmatch_marchc_usbram */

#define USB_PHY_RAMBISTS_DECODERR_Pos   (2U)
#define USB_PHY_RAMBISTS_DECODERR_Msk   (0x1U << USB_PHY_RAMBISTS_DECODERR_Pos)             /*!< 0x00000004 */
#define USB_PHY_RAMBISTS_DECODERR       USB_PHY_RAMBISTS_DECODERR_Msk                       /*!<decoder_error_usbram */

#define USB_PHY_RAMBISTS_UNMDECOD_Pos   (3U)
#define USB_PHY_RAMBISTS_UNMDECOD_Msk   (0x1U << USB_PHY_RAMBISTS_UNMDECOD_Pos)             /*!< 0x00000008 */
#define USB_PHY_RAMBISTS_UNMDECOD       USB_PHY_RAMBISTS_UNMDECOD_Msk                       /*!<unmatch_decoder_usbram */

#define USB_PHY_RAMBISTS_BWEERR_Pos (4U)
#define USB_PHY_RAMBISTS_BWEERR_Msk (0x1U << USB_PHY_RAMBISTS_BWEERR_Pos)                   /*!< 0x00000010 */
#define USB_PHY_RAMBISTS_BWEERR     USB_PHY_RAMBISTS_BWEERR_Msk                             /*!<bwe_error_usbram */

#define USB_PHY_RAMBISTS_UNMBWE_Pos (5U)
#define USB_PHY_RAMBISTS_UNMBWE_Msk (0x1U << USB_PHY_RAMBISTS_UNMBWE_Pos)                   /*!< 0x00000020 */
#define USB_PHY_RAMBISTS_UNMBWE     USB_PHY_RAMBISTS_UNMBWE_Msk                             /*!<unmatch_bwe_usbram */

#define USB_PHY_RAMBISTS_MARCHEND_Pos   (6U)
#define USB_PHY_RAMBISTS_MARCHEND_Msk   (0x1U << USB_PHY_RAMBISTS_MARCHEND_Pos)             /*!< 0x00000040 */
#define USB_PHY_RAMBISTS_MARCHEND       USB_PHY_RAMBISTS_MARCHEND_Msk                       /*!<bist_marchc_end_usbram */

#define USB_PHY_RAMBISTS_DECODEND_Pos   (7U)
#define USB_PHY_RAMBISTS_DECODEND_Msk   (0x1U << USB_PHY_RAMBISTS_DECODEND_Pos)             /*!< 0x00000080 */
#define USB_PHY_RAMBISTS_DECODEND       USB_PHY_RAMBISTS_DECODEND_Msk                       /*!<bist_decoder_end_usbram */

#define USB_PHY_RAMBISTS_BWEEND_Pos (8U)
#define USB_PHY_RAMBISTS_BWEEND_Msk (0x1U << USB_PHY_RAMBISTS_BWEEND_Pos)                   /*!< 0x00000100 */
#define USB_PHY_RAMBISTS_BWEEND     USB_PHY_RAMBISTS_BWEEND_Msk                             /*!<bist_bwe_end_usbram */

/*******************  Bit definition for USB_PHY_HAMCNTTHR register  ********************/
#define USB_PHY_HAMCNTTHR_CNTTHR_Pos    (0U)
#define USB_PHY_HAMCNTTHR_CNTTHR_Msk    (0x1FFFFFFU << USB_PHY_HAMCNTTHR_CNTTHR_Pos)        /*!< 0x01FFFFFF */
#define USB_PHY_HAMCNTTHR_CNTTHR        USB_PHY_HAMCNTTHR_CNTTHR_Msk                        /*!<cnt_thresh_usbram[24:0] */

/*******************  Bit definition for USB_PHY_RAMEMACFG register  ********************/
#define USB_PHY_RAMEMACFG_EMAW_Pos  (0U)
#define USB_PHY_RAMEMACFG_EMAW_Msk  (0x3U << USB_PHY_RAMEMACFG_EMAW_Pos)                    /*!< 0x00000003 */
#define USB_PHY_RAMEMACFG_EMAW      USB_PHY_RAMEMACFG_EMAW_Msk                              /*!<EMAW_usbram[1:0] */

#define USB_PHY_RAMEMACFG_EMA_Pos   (2U)
#define USB_PHY_RAMEMACFG_EMA_Msk   (0x7U << USB_PHY_RAMEMACFG_EMA_Pos)                     /*!< 0x0000001C */
#define USB_PHY_RAMEMACFG_EMA       USB_PHY_RAMEMACFG_EMA_Msk                               /*!<EMAW_usbram[2:0] */

/*******************  Bit definition for USB_PHY_PHYCFG register  ********************/
#define USB_PHY_PHYCFG_PD_Pos   (0U)
#define USB_PHY_PHYCFG_PD_Msk   (0x1U << USB_PHY_PHYCFG_PD_Pos)                             /*!< 0x00000001 */
#define USB_PHY_PHYCFG_PD       USB_PHY_PHYCFG_PD_Msk                                       /*!<phy_pd */

#define USB_PHY_PHYCFG_SPEED_Pos    (1U)
#define USB_PHY_PHYCFG_SPEED_Msk    (0x1U << USB_PHY_PHYCFG_SPEED_Pos)                      /*!< 0x00000002 */
#define USB_PHY_PHYCFG_SPEED        USB_PHY_PHYCFG_SPEED_Msk                                /*!<phy_speed */

#define USB_PHY_PHYCFG_TXENN_Pos    (2U)
#define USB_PHY_PHYCFG_TXENN_Msk    (0x1U << USB_PHY_PHYCFG_TXENN_Pos)                      /*!< 0x00000004 */
#define USB_PHY_PHYCFG_TXENN        USB_PHY_PHYCFG_TXENN_Msk                                /*!<phy_tx_en_n */

#define USB_PHY_PHYCFG_RXRCVEN_Pos  (3U)
#define USB_PHY_PHYCFG_RXRCVEN_Msk  (0x1U << USB_PHY_PHYCFG_RXRCVEN_Pos)                    /*!< 0x00000008 */
#define USB_PHY_PHYCFG_RXRCVEN      USB_PHY_PHYCFG_RXRCVEN_Msk                              /*!<phy_rx_rcv_en */

#define USB_PHY_PHYCFG_TXDAT_Pos    (4U)
#define USB_PHY_PHYCFG_TXDAT_Msk    (0x1U << USB_PHY_PHYCFG_TXDAT_Pos)                      /*!< 0x00000010 */
#define USB_PHY_PHYCFG_TXDAT        USB_PHY_PHYCFG_TXDAT_Msk                                /*!<phy_tx_dat */

#define USB_PHY_PHYCFG_TXSE0_Pos    (5U)
#define USB_PHY_PHYCFG_TXSE0_Msk    (0x1U << USB_PHY_PHYCFG_TXSE0_Pos)                      /*!< 0x00000020 */
#define USB_PHY_PHYCFG_TXSE0        USB_PHY_PHYCFG_TXSE0_Msk                                /*!<phy_tx_se0 */

#define USB_PHY_PHYCFG_DPPU_Pos (6U)
#define USB_PHY_PHYCFG_DPPU_Msk (0x1U << USB_PHY_PHYCFG_DPPU_Pos)                           /*!< 0x00000040 */
#define USB_PHY_PHYCFG_DPPU     USB_PHY_PHYCFG_DPPU_Msk                                     /*!<phy_dppu */

#define USB_PHY_PHYCFG_DPPULO_Pos   (7U)
#define USB_PHY_PHYCFG_DPPULO_Msk   (0x1U << USB_PHY_PHYCFG_DPPULO_Pos)                     /*!< 0x00000080 */
#define USB_PHY_PHYCFG_DPPULO       USB_PHY_PHYCFG_DPPULO_Msk                               /*!<phy_dppu_lo */

#define USB_PHY_PHYCFG_DMPU_Pos (8U)
#define USB_PHY_PHYCFG_DMPU_Msk (0x1U << USB_PHY_PHYCFG_DMPU_Pos)                           /*!< 0x00000100 */
#define USB_PHY_PHYCFG_DMPU     USB_PHY_PHYCFG_DMPU_Msk                                     /*!<phy_dmpu */

#define USB_PHY_PHYCFG_DMPULO_Pos   (9U)
#define USB_PHY_PHYCFG_DMPULO_Msk   (0x1U << USB_PHY_PHYCFG_DMPULO_Pos)                     /*!< 0x00000200 */
#define USB_PHY_PHYCFG_DMPULO       USB_PHY_PHYCFG_DMPULO_Msk                               /*!<phy_dmpu_lo */

#define USB_PHY_PHYCFG_SUSPEND_Pos  (10U)
#define USB_PHY_PHYCFG_SUSPEND_Msk  (0x1U << USB_PHY_PHYCFG_SUSPEND_Pos)                    /*!< 0x00000400 */
#define USB_PHY_PHYCFG_SUSPEND      USB_PHY_PHYCFG_SUSPEND_Msk                              /*!<phy_suspend */

#define USB_PHY_PHYCFG_RESSEL0_Pos  (11U)
#define USB_PHY_PHYCFG_RESSEL0_Msk  (0x1U << USB_PHY_PHYCFG_RESSEL0_Pos)                    /*!< 0x00000800 */
#define USB_PHY_PHYCFG_RESSEL0      USB_PHY_PHYCFG_RESSEL0_Msk                              /*!<phy_res_sel0 */

#define USB_PHY_PHYCFG_RESSEL1_Pos  (12U)
#define USB_PHY_PHYCFG_RESSEL1_Msk  (0x1U << USB_PHY_PHYCFG_RESSEL1_Pos)                    /*!< 0x00001000 */
#define USB_PHY_PHYCFG_RESSEL1      USB_PHY_PHYCFG_RESSEL1_Msk                              /*!<phy_res_sel1 */

#define USB_PHY_PHYCFG_RESSEL2_Pos  (13U)
#define USB_PHY_PHYCFG_RESSEL2_Msk  (0x1U << USB_PHY_PHYCFG_RESSEL2_Pos)                    /*!< 0x00002000 */
#define USB_PHY_PHYCFG_RESSEL2      USB_PHY_PHYCFG_RESSEL2_Msk                              /*!<phy_res_sel2 */

#define USB_PHY_PHYCFG_ISOEN_Pos    (14U)
#define USB_PHY_PHYCFG_ISOEN_Msk    (0x1U << USB_PHY_PHYCFG_ISOEN_Pos)                      /*!< 0x00004000 */
#define USB_PHY_PHYCFG_ISOEN        USB_PHY_PHYCFG_ISOEN_Msk                                /*!<phy_isoen */

#define USB_PHY_PHYCFG_RSTN_Pos (15U)
#define USB_PHY_PHYCFG_RSTN_Msk (0x1U << USB_PHY_PHYCFG_RSTN_Pos)                           /*!< 0x00008000 */
#define USB_PHY_PHYCFG_RSTN     USB_PHY_PHYCFG_RSTN_Msk                                     /*!<phy_rst_n */

#define USB_PHY_PHYCFG_PLL_Pos  (16U)
#define USB_PHY_PHYCFG_PLL_Msk  (0x1U << USB_PHY_PHYCFG_PLL_Pos)                            /*!< 0x00010000 */
#define USB_PHY_PHYCFG_PLL      USB_PHY_PHYCFG_PLL_Msk                                      /*!<phy_pll_cfg_h60_l48 */

/*******************  Bit definition for USB_PHY_PHYSTAT register  ********************/
#define USB_PHY_PHYSTAT_RXDP_Pos    (0U)
#define USB_PHY_PHYSTAT_RXDP_Msk    (0x1U << USB_PHY_PHYSTAT_RXDP_Pos)                      /*!< 0x00000001 */
#define USB_PHY_PHYSTAT_RXDP        USB_PHY_PHYSTAT_RXDP_Msk                                /*!<phy_rx_dp */

#define USB_PHY_PHYSTAT_RXDM_Pos    (1U)
#define USB_PHY_PHYSTAT_RXDM_Msk    (0x1U << USB_PHY_PHYSTAT_RXDM_Pos)                      /*!< 0x00000002 */
#define USB_PHY_PHYSTAT_RXDM        USB_PHY_PHYSTAT_RXDM_Msk                                /*!<phy_rx_dm */

#define USB_PHY_PHYSTAT_RXRCV_Pos   (2U)
#define USB_PHY_PHYSTAT_RXRCV_Msk   (0x1U << USB_PHY_PHYSTAT_RXRCV_Pos)                     /*!< 0x00000004 */
#define USB_PHY_PHYSTAT_RXRCV       USB_PHY_PHYSTAT_RXRCV_Msk                               /*!<phy_rx_rcv */

#define USB_PHY_PHYSTAT_STOGO_Pos   (3U)
#define USB_PHY_PHYSTAT_STOGO_Msk   (0x1U << USB_PHY_PHYSTAT_STOGO_Pos)                     /*!< 0x00000004 */
#define USB_PHY_PHYSTAT_STOGO       USB_PHY_PHYSTAT_STOGO_Msk                               /*!<sof_toggle_out */
//</h>
//<h>VREF
/******************************************************************************/
/*                                                                            */
/*                                    VREF_CSR                                 */
/*                                                                            */
/******************************************************************************/
/*******************   Bit definition for VREF_CTRL register    ********************/
#define VREF_CTRL_VREFEN_Pos    (0U)
#define VREF_CTRL_VREFEN_Msk    (0x1U << VREF_CTRL_VREFEN_Pos)                          /*!< 0x00000001 */
#define VREF_CTRL_VREFEN        VREF_CTRL_VREFEN_Msk                                    /*!< vref_vref_en*/

#define VREF_CTRL_LDOEN_Pos (1U)
#define VREF_CTRL_LDOEN_Msk (0x1U << VREF_CTRL_LDOEN_Pos)                               /*!< 0x00000002 */
#define VREF_CTRL_LDOEN     VREF_CTRL_LDOEN_Msk                                         /*!< vref_ldo_en*/

#define VREF_CTRL_SCMODE_Pos    (2U)
#define VREF_CTRL_SCMODE_Msk    (0x3U << VREF_CTRL_SCMODE_Pos)                          /*!< 0x00000004 */
#define VREF_CTRL_SCMODE        VREF_CTRL_SCMODE_Msk                                    /*!< vref_sc_mode*/

/*******************   Bit definition for VREF_TRIM register    ********************/
#define VREF_CTRL_RES_Pos   (0U)
#define VREF_CTRL_RES_Msk   (0x1U << VREF_CTRL_RES_Pos)                                 /*!< 0x00000001 */
#define VREF_CTRL_RES       VREF_CTRL_RES_Msk                                           /*!< vref_res_trim*/

#define VREF_CTRL_BJT_Pos   (1U)
#define VREF_CTRL_BJT_Msk   (0x1U << VREF_CTRL_BJT_Pos)                                 /*!< 0x00000001 */
#define VREF_CTRL_BJT       VREF_CTRL_BJT_Msk                                           /*!< vref_bjt_trim*/

/*******************   Bit definition for VREF_BG_FLAG register    ********************/
#define VREF_BG_FLAG_Pos    (0U)
#define VREF_BG_FLAG_Msk    (0x1U << VREF_BG_FLAG_Pos)                                  /*!< 0x00000001 */
#define VREF_BG_FLAG        VREF_BG_FLAG_Msk                                            /*!< vref_bg_flag*/
//</h>
//<h>WDT
/******************************************************************************/
/*                                                                            */
/*                                    WDT_CSR                                 */
/*                                                                            */
/******************************************************************************/
/*******************   Bit definition for WDT_CFG register    ********************/
#define WDT_CFG_EN_Pos  (0U)
#define WDT_CFG_EN_Msk  (0x1U << WDT_CFG_EN_Pos)                                        /*!< 0x00000001 */
#define WDT_CFG_EN      WDT_CFG_EN_Msk                                                  /*!< wdt_ctrl_en,counter enable */

#define WDT_CFG_CNTRST_Pos  (2U)
#define WDT_CFG_CNTRST_Msk  (0x1U << WDT_CFG_CNTRST_Pos)                                /*!< 0x00000004 */
#define WDT_CFG_CNTRST      WDT_CFG_CNTRST_Msk                                          /*!< wdt_ctrl_cnt_rst */

#define WDT_CFG_PERIOD_Pos  (5U)
#define WDT_CFG_PERIOD_Msk  (0x7U << WDT_CFG_PERIOD_Pos)                                /*!< 0x000000E0 */
#define WDT_CFG_PERIOD      WDT_CFG_PERIOD_Msk                                          /*!< wdt_ctrl_period[2:0] */

/*******************   Bit definition for WDT_WRCTRL register    ********************/
#define WDT_WRCTRL_WRCTRL_Pos   (0U)
#define WDT_WRCTRL_WRCTRL_Msk   (0xFFFFFFFFU << WDT_WRCTRL_WRCTRL_Pos)                  /*!< 0xFFFFFFFF */
#define WDT_WRCTRL_WRCTRL       WDT_WRCTRL_WRCTRL_Msk                                   /*!< Wdt_wrctrl[31:0] */

/*******************   Bit definition for WDT_CNT register    ********************/
#define WDT_CNT_CNT_Pos (0U)
#define WDT_CNT_CNT_Msk (0x3FFFFFFF << WDT_CNT_CNT_Pos)                                 /*!< 0x3FFFFFFF */
#define WDT_CNT_CNT     WDT_CNT_CNT_Msk                                                 /*!< Wdt_cnt[29:0]*/
//</h>
//</h>
/** @} */ /* End of group Device_Peripheral_Registers */


/*
** End of section using anonymous unions
*/
/*------------------ ARM Compiler V6 -------------------*/
#if defined(__ARMCC_VERSION) && (__ARMCC_VERSION >= 6010050)
/* leave anonymous unions enabled */
#elif defined(__ARMCC_VERSION)
#pragma pop
#elif defined(__CWCC__)
#pragma pop
#elif defined(__GNUC__)
/* leave anonymous unions enabled */
#elif defined(__IAR_SYSTEMS_ICC__)
#pragma language=default
#else
#error Not supported compiler type
#endif

/** @} */ /* End of group fm15f3xx */
/** @} */ /* End of group Keil */

//<<< end of configration section >>>


#ifdef __cplusplus
}
#endif


#endif  /* __fm15f3xx_H */
