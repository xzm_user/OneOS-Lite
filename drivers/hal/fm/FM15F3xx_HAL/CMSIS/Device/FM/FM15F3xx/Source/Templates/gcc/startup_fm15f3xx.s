/**
  ******************************************************************************
  * @file      startup_fm399.s
  * @version   V1.0.0
  * @date      2019.10.29
  *            This module performs:
  *                - Set the initial SP
  *                - Set the initial PC == Reset_Handler,
  *                - Set the vector table entries with the exceptions ISR 
  *                  address.
  *                - Configure the clock system    
  *                - Branches to main in the C library (which eventually
  *                  calls main()).
  *            After Reset the Cortex-M3 processor is in Thread mode,
  *            priority is Privileged, and the Stack is set to Main.
  ******************************************************************************
  */
    
  .syntax unified
  .cpu cortex-m3
  .fpu softvfp
  .thumb

.global  g_pfnVectors
.global  Default_Handler

/* start address for the initialization values of the .data section. 
defined in linker script */
.word  _sidata
/* start address for the .data section. defined in linker script */  
.word  _sdata
/* end address for the .data section. defined in linker script */
.word  _edata
/* start address for the .bss section. defined in linker script */
.word  _sbss
/* end address for the .bss section. defined in linker script */
.word  _ebss

//.equ  BootRAM, 0xF1E0F85F
/**
 * @brief  This is the code that gets called when the processor first
 *          starts execution following a reset event. Only the absolutely
 *          necessary set is performed, after which the application
 *          supplied main() routine is called. 
 * @param  None
 * @retval : None
*/

    .section  .text.Reset_Handler
  .weak  Reset_Handler
  .type  Reset_Handler, %function
Reset_Handler:

/* Copy the data segment initializers from flash to SRAM */
  movs  r1, #0
  b     LoopCopyDataInit

CopyDataInit:
  ldr   r3, =_sidata
  ldr   r3, [r3, r1]
  str   r3, [r0, r1]
  adds  r1, r1, #4
    
LoopCopyDataInit:
  ldr   r0, =_sdata
  ldr   r3, =_edata
  adds  r2, r0, r1
  cmp   r2, r3
  bcc   CopyDataInit
  ldr   r2, =_sbss
  b     LoopFillZerobss

/* Zero fill the bss segment. */
FillZerobss:
  movs  r3, #0
  str   r3, [r2], #4
    
LoopFillZerobss:
  ldr   r3, = _ebss
  cmp   r2, r3
  bcc   FillZerobss
/* Call the clock system intitialization function.*/
  bl  SystemInit  
/* Call the application's entry point.*/
  bl    main
  bx    lr
.size   Reset_Handler, .-Reset_Handler

/**
 * @brief  This is the code that gets called when the processor receives an 
 *         unexpected interrupt. This simply enters an infinite loop, preserving
 *         the system state for examination by a debugger.
 * @param  None
 * @retval None
*/
    .section  .text.Default_Handler,"ax",%progbits
Default_Handler:
Infinite_Loop:
  b  Infinite_Loop
  .size  Default_Handler, .-Default_Handler

/******************************************************************************
*
* The minimal vector table for a Cortex M3. Note that the proper constructs
* must be placed on this to ensure that it ends up at physical address
* 0x0000.0000.
* 
*******************************************************************************/
  .section  .isr_vector,"a",%progbits
  .type  g_pfnVectors, %object
  .size  g_pfnVectors, .-g_pfnVectors
    
    
g_pfnVectors:
//  .word  _estack
  .word  Reset_Handler
  .word  NMI_Handler
  .word  HardFault_Handler
  .word  MemManage_Handler
  .word  BusFault_Handler
  .word  UsageFault_Handler
  .word  0
  .word  0
  .word  0
  .word  0
  .word  SVC_Handler
  .word  DebugMon_Handler
  .word  0
  .word  PendSV_Handler
  .word  SysTick_Handler
  .word  DSP_Error_Handler
  .word  DMA_Channel1_Handler
  .word  DMA_Channel2_Handler
  .word  DMA_Channel3_Handler
  .word  DMA_Channel4_Handler
  .word  DMA_Channel5_Handler
  .word  DMA_Channel6_Handler
  .word  DMA_Channel7_Handler
  .word  DMA_Error_Handler
  .word  PAE_Handler
  .word  HASH_Handler
  .word  BCA_Handler
  .word  DSP_Handler
  .word  FLASH_Int_Handler
  .word  FLASH_RdcolInt_Handler
  .word  FLASH_LvtInt_Handler
  .word  PM_Int_Handler
  .word  RANDOM_Handler
  .word  0
  .word  SPI0_Handler
  .word  SPI1_Handler
  .word  SPI2_Handler
  .word  UART0_Handler
  .word  UART1_Handler
  .word  UART2_Handler
  .word  0
  .word  I2C0_Handler
  .word  I2C1_Handler
  .word  0
  .word  0
  .word  CT0_Handler
  .word  CT1_Handler
  .word  FSMC_Handler
  .word  QSPI_Handler
  .word  USB_Handler
  .word  DCMI_Handler
  .word  DCMI_DMA_Handler
  .word  SDMA_Handler
  .word  STIMER0_Handler
  .word  STIMER1_Handler
  .word  STIMER2_Handler
  .word  STIMER3_Handler
  .word  STIMER4_Handler
  .word  STIMER5_Handler
  .word  LPTIMER0_Handler
  .word  0
  .word  PORTA_Handler
  .word  PORTB_Handler
  .word  PORTC_Handler
  .word  PORTD_Handler
  .word  PORTE_Handler
  .word  PORTF_Handler
  .word  RTC_Checkerr_Handler
  .word  RTC_Tamper_Handler
  .word  RTC_Time_Handler
  .word  RTC_Second_Handler
  .word  0
  .word  ADC0_Handler
  .word  CMP0_Handler
  .word  DAC_Watermark_Handler
  .word  DAC_Top_Handler
  .word  DAC_Bottom_Handler

/*******************************************************************************
*
* Provide weak aliases for each Exception handler to the Default_Handler. 
* As they are weak aliases, any function with the same name will override 
* this definition.
*
*******************************************************************************/
  .weak  NMI_Handler
  .thumb_set NMI_Handler,Default_Handler
  
  .weak  HardFault_Handler
  .thumb_set HardFault_Handler,Default_Handler
  
  .weak  MemManage_Handler
  .thumb_set MemManage_Handler,Default_Handler
  
  .weak  BusFault_Handler
  .thumb_set BusFault_Handler,Default_Handler

  .weak  UsageFault_Handler
  .thumb_set UsageFault_Handler,Default_Handler

  .weak  SVC_Handler
  .thumb_set SVC_Handler,Default_Handler

  .weak  DebugMon_Handler
  .thumb_set DebugMon_Handler,Default_Handler

  .weak  PendSV_Handler
  .thumb_set PendSV_Handler,Default_Handler

  .weak  SysTick_Handler
  .thumb_set SysTick_Handler,Default_Handler
  
  .weak  DMA_Channel0_Handler
  .thumb_set DMA_Channel0_Handler,Default_Handler

  .weak  DMA_Channel1_Handler
  .thumb_set DMA_Channel1_Handler,Default_Handler

  .weak  DMA_Channel2_Handler
  .thumb_set DMA_Channel2_Handler,Default_Handler

  .weak  DMA_Channel3_Handler
  .thumb_set DMA_Channel3_Handler,Default_Handler

  .weak  DMA_Channel4_Handler
  .thumb_set DMA_Channel4_Handler,Default_Handler

  .weak  DMA_Channel5_Handler
  .thumb_set DMA_Channel5_Handler,Default_Handler

  .weak  DMA_Channel6_Handler
  .thumb_set DMA_Channel6_Handler,Default_Handler

  .weak  DMA_Channel7_Handler
  .thumb_set DMA_Channel7_Handler,Default_Handler

  .weak  DMA_Error_Handler
  .thumb_set DMA_Error_Handler,Default_Handler

  .weak  PAE_Handler
  .thumb_set PAE_Handler,Default_Handler

  .weak  HASH_Handler
  .thumb_set HASH_Handler,Default_Handler

  .weak  BCA_Handler
  .thumb_set BCA_Handler,Default_Handler

  .weak  DSP_Handler
  .thumb_set DSP_Handler,Default_Handler

  .weak  DSP_Error_Handler
  .thumb_set DSP_Error_Handler,Default_Handler

  .weak  FLASH_Int_Handler
  .thumb_set FLASH_Int_Handler,Default_Handler

  .weak  FLASH_RdcolInt_Handler
  .thumb_set FLASH_RdcolInt_Handler,Default_Handler

  .weak  FLASH_LvtInt_Handler
  .thumb_set FLASH_LvtInt_Handler,Default_Handler

  .weak  PM_Int_Handler
  .thumb_set PM_Int_Handler,Default_Handler

  .weak  RANDOM_Handler
  .thumb_set RANDOM_Handler,Default_Handler

  .weak  SPI0_Handler
  .thumb_set SPI0_Handler,Default_Handler

  .weak  SPI1_Handler
  .thumb_set SPI1_Handler,Default_Handler

  .weak  SPI2_Handler
  .thumb_set SPI2_Handler,Default_Handler

  .weak  SPI3_Handler
  .thumb_set SPI3_Handler,Default_Handler

  .weak  UART0_Handler
  .thumb_set UART0_Handler,Default_Handler

  .weak  UART1_Handler
  .thumb_set UART1_Handler,Default_Handler

  .weak  UART2_Handler
  .thumb_set UART2_Handler,Default_Handler

  .weak  UART3_Handler
  .thumb_set UART3_Handler,Default_Handler

  .weak  I2C0_Handler
  .thumb_set I2C0_Handler,Default_Handler

  .weak  I2C1_Handler
  .thumb_set I2C1_Handler,Default_Handler

  .weak  CT0_Handler
  .thumb_set CT0_Handler,Default_Handler

  .weak  CT1_Handler
  .thumb_set CT1_Handler,Default_Handler

  .weak  FSMC_Handler
  .thumb_set FSMC_Handler,Default_Handler

  .weak  QSPI_Handler
  .thumb_set QSPI_Handler,Default_Handler

  .weak  USB_Handler
  .thumb_set USB_Handler,Default_Handler

  .weak  DCMI_Handler  
  .thumb_set DCMI_Handler,Default_Handler

  .weak  DCMI_DMA_Handler  
  .thumb_set DCMI_DMA_Handler,Default_Handler

  .weak  SDMA_Handler  
  .thumb_set SDMA_Handler,Default_Handler

  .weak  STIMER0_Handler  
  .thumb_set STIMER0_Handler,Default_Handler

  .weak  STIMER1_Handler  
  .thumb_set STIMER1_Handler,Default_Handler

  .weak  STIMER2_Handler  
  .thumb_set STIMER2_Handler,Default_Handler

  .weak  STIMER3_Handler  
  .thumb_set STIMER3_Handler,Default_Handler

  .weak  STIMER4_Handler  
  .thumb_set STIMER4_Handler,Default_Handler

  .weak  STIMER5_Handler  
  .thumb_set STIMER5_Handler,Default_Handler

  .weak  LPTIMER0_Handler  
  .thumb_set LPTIMER0_Handler,Default_Handler

  .weak  PORTA_Handler  
  .thumb_set PORTA_Handler,Default_Handler

  .weak  PORTB_Handler  
  .thumb_set PORTB_Handler,Default_Handler

  .weak  PORTC_Handler  
  .thumb_set PORTC_Handler,Default_Handler

  .weak  PORTD_Handler  
  .thumb_set PORTD_Handler,Default_Handler

  .weak  PORTE_Handler  
  .thumb_set PORTE_Handler,Default_Handler

  .weak  PORTF_Handler  
  .thumb_set PORTF_Handler,Default_Handler

  .weak  RTC_Checkerr_Handler  
  .thumb_set RTC_Checkerr_Handler ,Default_Handler
  
  .weak  RTC_Tamper_Handler  
  .thumb_set RTC_Tamper_Handler ,Default_Handler
  
  .weak  RTC_Time_Handler  
  .thumb_set RTC_Time_Handler ,Default_Handler
  
  .weak  RTC_Second_Handler  
  .thumb_set RTC_Second_Handler ,Default_Handler
  
  .weak  ADC0_Handler  
  .thumb_set ADC0_Handler ,Default_Handler
  
  .weak  CMP0_Handler  
  .thumb_set CMP0_Handler ,Default_Handler

  .weak  DAC_Watermark_Handler  
  .thumb_set DAC_Watermark_Handler ,Default_Handler

  .weak  DAC_Top_Handler  
  .thumb_set DAC_Top_Handler ,Default_Handler
  
  .weak  DAC_Bottom_Handler  
  .thumb_set DAC_Bottom_Handler ,Default_Handler
/******************* (C) COPYRIGHT 2019 Fudan Microelectronics *****END OF FILE****/
