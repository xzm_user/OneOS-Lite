;/******************** (C) COPYRIGHT 2019 Fudan Microelectronics ********************
;* File Name          : startup_fm15f3xx.s
;* Author             : FM Application Team
;* Description        : FM15F3xx devices vector table for EWARM toolchain.
;*                      This module performs:
;*                      - Set the initial SP
;*                      - Set the initial PC == _iar_program_start,
;*                      - Set the vector table entries with the exceptions ISR 
;*                        address.
;*                      - Branches to main in the C library (which eventually
;*                        calls main()).
;*                      After Reset the Cortex-M processor is in Thread mode,
;*                      priority is Privileged, and the Stack is set to Main.
;********************************************************************************
;* 
;* Redistribution and use in source and binary forms, with or without modification,
;* are permitted provided that the following conditions are met:
;*   1. Redistributions of source code must retain the above copyright notice,
;*      this list of conditions and the following disclaimer.
;*   2. Redistributions in binary form must reproduce the above copyright notice,
;*      this list of conditions and the following disclaimer in the documentation
;*      and/or other materials provided with the distribution.
;*   3. Neither the name of Fudan Microelectronics nor the names of its contributors
;*      may be used to endorse or promote products derived from this software
;*      without specific prior written permission.
;*
;* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
;* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
;* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
;* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
;* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
;* DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
;* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
;* CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
;* OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
;* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
;* 
;*******************************************************************************
;
;
; The modules in this file are included in the libraries, and may be replaced
; by any user-defined modules that define the PUBLIC symbol _program_start or
; a user defined start symbol.
; To override the cstartup defined in the library, simply add your modified
; version to the workbench project.
;
; The vector table is normally located at address 0.
; When debugging in RAM, it can be located in RAM, aligned to at least 2^6.
; The name "__vector_table" has special meaning for C-SPY:
; it is where the SP start value is found, and the NVIC vector
; table register (VTOR) is initialized to this address if != 0.
;
; Cortex-M version
;

        MODULE  ?cstartup

        ;; Forward declaration of sections.
        SECTION CSTACK:DATA:NOROOT(3)

        SECTION .intvec:CODE:NOROOT(2)

        EXTERN  __iar_program_start
        EXTERN  SystemInit
        PUBLIC  __vector_table

        DATA
__vector_table

        DCD     sfe(CSTACK)               ; Top of Stack
        DCD     Reset_Handler             ; Reset Handler
        
        DCD     NMI_Handler               ; NMI Handler
        DCD     HardFault_Handler         ; Hard Fault Handler
        DCD     MemManage_Handler         ; MPU Fault Handler
        DCD     BusFault_Handler          ; Bus Fault Handler
        DCD     UsageFault_Handler        ; Usage Fault Handler
        DCD     0                         ; Reserved
        DCD     0                         ; Reserved
        DCD     0                         ; Reserved
        DCD     0                         ; Reserved
        DCD     SVC_Handler               ; SVCall Handler
        DCD     DebugMon_Handler          ; Debug Monitor Handler
        DCD     0                         ; Reserved
        DCD     PendSV_Handler            ; PendSV Handler
        DCD     SysTick_Handler           ; SysTick Handler

        ; External Interrupts
        DCD     DMA_Channel0_Handler      ; DMA Channel 0 transfer complete
        DCD     DMA_Channel1_Handler      ; DMA Channel 1 transfer complete
        DCD     DMA_Channel2_Handler      ; DMA Channel 2 transfer complete
        DCD     DMA_Channel3_Handler      ; DMA Channel 3 transfer complete
        DCD     DMA_Channel4_Handler      ; DMA Channel 4 transfer complete
        DCD     DMA_Channel5_Handler      ; DMA Channel 5 transfer complete
        DCD     DMA_Channel6_Handler      ; DMA Channel 6 transfer complete
        DCD     DMA_Channel7_Handler      ; DMA Channel 7 transfer complete
        DCD     DMA_Error_Handler         ; DMA channel 0 - 7 error
        DCD     PAE_Handler               ; PAE Handler
        DCD     HASH_Handler              ; HASH Handler
        DCD     BCA_Handler               ; BCA Handler
        DCD     DSP_Handler               ; DSP Handler
        DCD     DSP_Error_Handler         ; DSP error Handler
        DCD     FLASH_Int_Handler         ; FLASH Handler
        DCD     FLASH_RdcolInt_Handler    ; Flash read collision Handler
        DCD     FLASH_LvtInt_Handler      ; Flash low V Handler
        DCD     PM_Int_Handler            ; PM Handler
        DCD     RANDOM_Handler            ; RANDOM Handler
        DCD     0                         ; Reserved
        DCD     SPI0_Handler              ; SPI0 Handler
        DCD     SPI1_Handler              ; SPI1 Handler
        DCD     SPI2_Handler              ; SPI2 Handler
        DCD     SPI3_Handler              ; SPI3 Handler
        DCD     UART0_Handler             ; UART0 Handler
        DCD     UART1_Handler             ; UART1 Handler
        DCD     UART2_Handler             ; UART2 Handler
        DCD     0                         ; Reserved
        DCD     I2C0_Handler              ; I2C0 Handler
        DCD     I2C1_Handler              ; I2C1 Handler
        DCD     0                         ; Reserved
        DCD     0                         ; Reserved
        DCD     CT0_Handler               ; CT0 Handler
        DCD     CT1_Handler               ; CT1 Handler
        DCD     FSMC_Handler              ; FSMC Handler
        DCD     QSPI_Handler              ; QSPI Handler
        DCD     USB_Handler               ; USB Handler
        DCD     DCMI_Handler              ; DCMI Handler
        DCD     DCMI_DMA_Handler          ; DCMI_DMA Handler
        DCD     SDMA_Handler              ; Security_DMA Handler
        DCD     STIMER0_Handler           ; STIMER0 Handler
        DCD     STIMER1_Handler           ; STIMER1 Handler
        DCD     STIMER2_Handler           ; STIMER2 Handler
        DCD     STIMER3_Handler           ; STIMER3 Handler
        DCD     STIMER4_Handler           ; STIMER4 Handler
        DCD     STIMER5_Handler           ; STIMER5 Handler
        DCD     LPTIMER0_Handler          ; LPTIMER0 Handler
        DCD     0                         ; Reserved
        DCD     GPIOA_Handler             ; GPIO A Handler
        DCD     GPIOB_Handler             ; GPIO B Handler
        DCD     GPIOC_Handler             ; GPIO C Handler
        DCD     GPIOD_Handler             ; GPIO D Handler
        DCD     GPIOE_Handler             ; GPIO E Handler
        DCD     GPIOF_Handler             ; GPIO F Handler
        DCD     RTC_Checkerr_Handler      ; RTC Check Error Handler
        DCD     BKP_Tamper_Handler        ; BKP Tamper Handler
        DCD     RTC_Alarm_Handler         ; RTC Alarm Handler
        DCD     RTC_1S_Handler            ; RTC 1s Handler
        DCD     0                         ; Reserved
        DCD     ADC0_Handler              ; ADC0 Handler
        DCD     CMP0_Handler              ; CMP0 Handler
        DCD     DAC_Watermark_Handler     ; DAC Watermark Handler
        DCD     DAC_Top_Handler           ; DAC Top Handler
        DCD     DAC_Bottom_Handler        ; DAC Bottom Handler
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; Default interrupt handlers.
;;
        THUMB
        PUBWEAK Reset_Handler
        SECTION .text:CODE:NOROOT:REORDER(2)
Reset_Handler

        LDR     R0, =SystemInit
        BLX     R0
        LDR     R0, =__iar_program_start
        BX      R0

        PUBWEAK NMI_Handler
        SECTION .text:CODE:NOROOT:REORDER(1)
NMI_Handler
        B NMI_Handler

        PUBWEAK HardFault_Handler
        SECTION .text:CODE:NOROOT:REORDER(1)
HardFault_Handler
        B HardFault_Handler

        PUBWEAK MemManage_Handler
        SECTION .text:CODE:NOROOT:REORDER(1)
MemManage_Handler
        B MemManage_Handler

        PUBWEAK BusFault_Handler
        SECTION .text:CODE:NOROOT:REORDER(1)
BusFault_Handler
        B BusFault_Handler

        PUBWEAK UsageFault_Handler
        SECTION .text:CODE:NOROOT:REORDER(1)
UsageFault_Handler
        B UsageFault_Handler

        PUBWEAK SVC_Handler
        SECTION .text:CODE:NOROOT:REORDER(1)
SVC_Handler
        B SVC_Handler

        PUBWEAK DebugMon_Handler
        SECTION .text:CODE:NOROOT:REORDER(1)
DebugMon_Handler
        B DebugMon_Handler

        PUBWEAK PendSV_Handler
        SECTION .text:CODE:NOROOT:REORDER(1)
PendSV_Handler
        B PendSV_Handler

        PUBWEAK SysTick_Handler
        SECTION .text:CODE:NOROOT:REORDER(1)
SysTick_Handler
        B SysTick_Handler

        PUBWEAK DMA_Channel0_Handler
        SECTION .text:CODE:NOROOT:REORDER(1)
DMA_Channel0_Handler  
        B DMA_Channel0_Handler

        PUBWEAK DMA_Channel1_Handler
        SECTION .text:CODE:NOROOT:REORDER(1)
DMA_Channel1_Handler  
        B DMA_Channel1_Handler

        PUBWEAK DMA_Channel2_Handler
        SECTION .text:CODE:NOROOT:REORDER(1)    
DMA_Channel2_Handler  
        B DMA_Channel2_Handler

        PUBWEAK DMA_Channel3_Handler
        SECTION .text:CODE:NOROOT:REORDER(1)  
DMA_Channel3_Handler  
        B DMA_Channel3_Handler

        PUBWEAK DMA_Channel4_Handler
        SECTION .text:CODE:NOROOT:REORDER(1)
DMA_Channel4_Handler  
        B DMA_Channel4_Handler

        PUBWEAK DMA_Channel5_Handler
        SECTION .text:CODE:NOROOT:REORDER(1)
DMA_Channel5_Handler  
        B DMA_Channel5_Handler

        PUBWEAK DMA_Channel6_Handler
        SECTION .text:CODE:NOROOT:REORDER(1)
DMA_Channel6_Handler  
        B DMA_Channel6_Handler

        PUBWEAK DMA_Channel7_Handler
        SECTION .text:CODE:NOROOT:REORDER(1)
DMA_Channel7_Handler  
        B DMA_Channel7_Handler

        PUBWEAK DMA_Error_Handler
        SECTION .text:CODE:NOROOT:REORDER(1)
DMA_Error_Handler  
        B DMA_Error_Handler

        PUBWEAK PAE_Handler
        SECTION .text:CODE:NOROOT:REORDER(1)
PAE_Handler
        B PAE_Handler

        PUBWEAK HASH_Handler
        SECTION .text:CODE:NOROOT:REORDER(1)    
HASH_Handler  
        B HASH_Handler

        PUBWEAK BCA_Handler
        SECTION .text:CODE:NOROOT:REORDER(1)    
BCA_Handler  
        B BCA_Handler

        PUBWEAK DSP_Handler
        SECTION .text:CODE:NOROOT:REORDER(1)    
DSP_Handler  
        B DSP_Handler

        PUBWEAK DSP_Error_Handler
        SECTION .text:CODE:NOROOT:REORDER(1)    
DSP_Error_Handler  
        B DSP_Error_Handler

        PUBWEAK FLASH_Int_Handler
        SECTION .text:CODE:NOROOT:REORDER(1)    
FLASH_Int_Handler  
        B FLASH_Int_Handler

        PUBWEAK FLASH_RdcolInt_Handler
        SECTION .text:CODE:NOROOT:REORDER(1)    
FLASH_RdcolInt_Handler  
        B FLASH_RdcolInt_Handler

        PUBWEAK FLASH_LvtInt_Handler
        SECTION .text:CODE:NOROOT:REORDER(1)    
FLASH_LvtInt_Handler  
        B FLASH_LvtInt_Handler

        PUBWEAK PM_Int_Handler
        SECTION .text:CODE:NOROOT:REORDER(1)    
PM_Int_Handler  
        B PM_Int_Handler

        PUBWEAK RANDOM_Handler
        SECTION .text:CODE:NOROOT:REORDER(1)
RANDOM_Handler  
        B RANDOM_Handler

        PUBWEAK SPI0_Handler
        SECTION .text:CODE:NOROOT:REORDER(1) 
SPI0_Handler  
        B SPI0_Handler

        PUBWEAK SPI1_Handler
        SECTION .text:CODE:NOROOT:REORDER(1)  
SPI1_Handler  
        B SPI1_Handler

        PUBWEAK SPI2_Handler
        SECTION .text:CODE:NOROOT:REORDER(1)  
SPI2_Handler  
        B SPI2_Handler

        PUBWEAK SPI3_Handler
        SECTION .text:CODE:NOROOT:REORDER(1)  
SPI3_Handler  
        B SPI3_Handler

        PUBWEAK UART0_Handler
        SECTION .text:CODE:NOROOT:REORDER(1) 
UART0_Handler  
        B UART0_Handler

        PUBWEAK UART1_Handler
        SECTION .text:CODE:NOROOT:REORDER(1)    
UART1_Handler  
        B UART1_Handler

        PUBWEAK UART2_Handler
        SECTION .text:CODE:NOROOT:REORDER(1)    
UART2_Handler  
        B UART2_Handler

        PUBWEAK I2C0_Handler
        SECTION .text:CODE:NOROOT:REORDER(1)    
I2C0_Handler  
        B I2C0_Handler
        
        PUBWEAK I2C1_Handler
        SECTION .text:CODE:NOROOT:REORDER(1)    
I2C1_Handler  
        B I2C1_Handler

        PUBWEAK CT0_Handler
        SECTION .text:CODE:NOROOT:REORDER(1)
CT0_Handler  
        B CT0_Handler

        PUBWEAK CT1_Handler
        SECTION .text:CODE:NOROOT:REORDER(1)
CT1_Handler  
        B CT1_Handler

        PUBWEAK FSMC_Handler
        SECTION .text:CODE:NOROOT:REORDER(1)
FSMC_Handler  
        B FSMC_Handler

        PUBWEAK QSPI_Handler
        SECTION .text:CODE:NOROOT:REORDER(1) 
QSPI_Handler  
        B QSPI_Handler

        PUBWEAK USB_Handler
        SECTION .text:CODE:NOROOT:REORDER(1) 
USB_Handler  
        B USB_Handler

        PUBWEAK DCMI_Handler
        SECTION .text:CODE:NOROOT:REORDER(1) 
DCMI_Handler  
        B DCMI_Handler

        PUBWEAK DCMI_DMA_Handler
        SECTION .text:CODE:NOROOT:REORDER(1) 
DCMI_DMA_Handler  
        B DCMI_DMA_Handler

        PUBWEAK SDMA_Handler
        SECTION .text:CODE:NOROOT:REORDER(1)
SDMA_Handler  
        B SDMA_Handler

        PUBWEAK STIMER0_Handler
        SECTION .text:CODE:NOROOT:REORDER(1)
STIMER0_Handler  
        B STIMER0_Handler

        PUBWEAK STIMER1_Handler
        SECTION .text:CODE:NOROOT:REORDER(1)
STIMER1_Handler  
        B STIMER1_Handler

        PUBWEAK STIMER2_Handler
        SECTION .text:CODE:NOROOT:REORDER(1)
STIMER2_Handler  
        B STIMER2_Handler

        PUBWEAK STIMER3_Handler
        SECTION .text:CODE:NOROOT:REORDER(1)
STIMER3_Handler  
        B STIMER3_Handler

        PUBWEAK STIMER4_Handler
        SECTION .text:CODE:NOROOT:REORDER(1)   
STIMER4_Handler  
        B STIMER4_Handler

        PUBWEAK STIMER5_Handler
        SECTION .text:CODE:NOROOT:REORDER(1)   
STIMER5_Handler  
        B STIMER5_Handler
      
        PUBWEAK LPTIMER0_Handler
        SECTION .text:CODE:NOROOT:REORDER(1)    
LPTIMER0_Handler  
        B LPTIMER0_Handler

        PUBWEAK GPIOA_Handler
        SECTION .text:CODE:NOROOT:REORDER(1)    
GPIOA_Handler  
        B GPIOA_Handler

        PUBWEAK GPIOB_Handler
        SECTION .text:CODE:NOROOT:REORDER(1)    
GPIOB_Handler  
        B GPIOB_Handler

        PUBWEAK GPIOC_Handler
        SECTION .text:CODE:NOROOT:REORDER(1) 
GPIOC_Handler  
        B GPIOC_Handler

        PUBWEAK GPIOD_Handler
        SECTION .text:CODE:NOROOT:REORDER(1)    
GPIOD_Handler  
        B GPIOD_Handler

        PUBWEAK GPIOE_Handler
        SECTION .text:CODE:NOROOT:REORDER(1)
GPIOE_Handler  
        B GPIOE_Handler

        PUBWEAK GPIOF_Handler
        SECTION .text:CODE:NOROOT:REORDER(1)
GPIOF_Handler  
        B GPIOF_Handler

        PUBWEAK RTC_Checkerr_Handler
        SECTION .text:CODE:NOROOT:REORDER(1)
RTC_Checkerr_Handler  
        B RTC_Checkerr_Handler

        PUBWEAK BKP_Tamper_Handler
        SECTION .text:CODE:NOROOT:REORDER(1)
BKP_Tamper_Handler  
        B BKP_Tamper_Handler

        PUBWEAK RTC_Alarm_Handler
        SECTION .text:CODE:NOROOT:REORDER(1)
RTC_Alarm_Handler  
        B RTC_Alarm_Handler

        PUBWEAK RTC_1S_Handler
        SECTION .text:CODE:NOROOT:REORDER(1)
RTC_1S_Handler  
        B RTC_1S_Handler

        PUBWEAK ADC0_Handler
        SECTION .text:CODE:NOROOT:REORDER(1)   
ADC0_Handler  
        B ADC0_Handler

        PUBWEAK CMP0_Handler
        SECTION .text:CODE:NOROOT:REORDER(1)   
CMP0_Handler  
        B CMP0_Handler

        PUBWEAK DAC_Watermark_Handler
        SECTION .text:CODE:NOROOT:REORDER(1)    
DAC_Watermark_Handler  
        B DAC_Watermark_Handler

        PUBWEAK DAC_Top_Handler
        SECTION .text:CODE:NOROOT:REORDER(1)    
DAC_Top_Handler  
        B DAC_Top_Handler

        PUBWEAK DAC_Bottom_Handler
        SECTION .text:CODE:NOROOT:REORDER(1)    
DAC_Bottom_Handler  
        B DAC_Bottom_Handler
        END
        
/******************** (C) COPYRIGHT Fudan Microelectronics *****END OF FILE****/
