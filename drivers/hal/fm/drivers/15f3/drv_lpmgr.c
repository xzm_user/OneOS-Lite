/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_lpmgr.c
 *
 * @brief       This file implements low power manager for stm32.
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include <board.h>

#define DBG_EXT_TAG "drv.lpmgr"
#define DBG_EXT_LVL DBG_EXT_INFO

void fm_MF_PMU_Init(os_uint32_t sleep_mode)
{
    /*IO CONFIG*/
    FL_PMU_SleepInitTypeDef defaultInitStruct;

    defaultInitStruct.deepSleep          = sleep_mode;
    defaultInitStruct.LDOLowPowerMode    = FL_PMU_LDO_LPM_DISABLE;
    defaultInitStruct.wakeupFrequency    = FL_PMU_RCHF_WAKEUP_FREQ_8MHZ;
    defaultInitStruct.wakeupDelay        = FL_PMU_WAKEUP_DELAY_2US;
    defaultInitStruct.coreVoltageScaling = FL_DISABLE;

    FL_PMU_Sleep_Init(PMU, &defaultInitStruct);
}

void fm_Sleep(void)
{
    FL_RCC_RCMF_Disable();               //�ر�RCMF
    FL_RMU_PDR_Enable(RMU);              //��PDR
    FL_RMU_BORPowerDown_Disable(RMU);    //�ر�BOR 2uA

    /* ʹ��ADCʱADCMonitor�����Լ�Vref��ͬʱ��ʼ��ͬʱ�ر� */
    FL_VREF_Disable(VREF);            //�ر�VREF1p2
    FL_SVD_DisableADCMonitor(SVD);    //�ر�ADC��Դ���
    FL_ADC_Disable(ADC);              //�ر�ADCʹ��

    FL_PMU_SetLowPowerMode(PMU, FL_PMU_POWER_MODE_SLEEP_OR_DEEPSLEEP);
    __WFI();
}

/**
 ***********************************************************************************************************************
 * @brief           Put device into sleep mode.
 *
 * @param[in]       lpm             Low power manager structure.
 * @param[in]       mode            Low power mode.
 *
 * @return          None.
 ***********************************************************************************************************************
 */
static os_err_t lpm_sleep(lpmgr_sleep_mode_e mode)
{
    switch (mode)
    {
    case SYS_SLEEP_MODE_NONE:
        break;

    case SYS_SLEEP_MODE_IDLE:
        // __WFI();
        break;

    case SYS_SLEEP_MODE_LIGHT:
        fm_MF_PMU_Init(FL_PMU_SLEEP_MODE_NORMAL); /* sleep mode */
        fm_Sleep();
        break;

    case SYS_SLEEP_MODE_DEEP:
        fm_MF_PMU_Init(FL_PMU_SLEEP_MODE_DEEP); /* DeepSleep mode */
        fm_Sleep();
        break;

    default:
        OS_ASSERT(0);
    }

    return OS_EOK;
}

int drv_lpmgr_hw_init(void)
{
    os_lpmgr_init(lpm_sleep);
    return 0;
}

OS_PREV_INIT(drv_lpmgr_hw_init, OS_INIT_SUBLEVEL_MIDDLE);
