/*
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_interrupt.c
 *
 * @brief       This file implements interrupt handler function for fm33
 *
 * @revision
 * Date         Author          Notes
 * 2021-07-29   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include "drv_interrupt.h"

void DMA_Channel1_Handler(void)
{
    LL_DMA_IRQHandler(DMA_CHANNEL_1);
}

void DMA_Channel3_Handler(void)
{
    LL_DMA_IRQHandler(DMA_CHANNEL_3);
}

void DMA_Channel5_Handler(void)
{
    LL_DMA_IRQHandler(DMA_CHANNEL_5);
}
