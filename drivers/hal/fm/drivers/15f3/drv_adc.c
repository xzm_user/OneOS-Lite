/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at

 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_adc.c
 *
 * @brief       This file implements adc driver for ht.
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include <board.h>
#include <os_stddef.h>
#include <os_memory.h>
#include <bus/bus.h>
#include <string.h>
#include <drv_cfg.h>

#include <drv_adc.h>

#define DBG_TAG "drv.adc"
#include <dlog.h>

#define FM33_USED_CHANNEL LL_ADC_CHANNEL_2

/*max external adc channel number*/
#define FM33_EXT_ADC_CHANNEL_MAX 11

/*default:period = 1s*/
#define ADC_WAIT_TIMEOUT ((uint32_t)0x8000)
#define ITEM_NUM(items)  sizeof(items) / sizeof(items[0])

/*adc channel info*/
struct fm33_adc_map
{
    os_uint8_t    index;
    os_uint32_t   name;
    GPIO_TypeDef *port;
    os_uint32_t   pin;
};

struct fm33_adc
{
    struct os_adc_device  adc;
    struct fm33_adc_info *info;
    os_uint32_t           timeout;
    os_uint8_t            status;
};

/* clang-format off */
const struct fm33_adc_map adcs[] = {
    /*index | channel              | port |      pin      */
    {0,     LL_ADC_CHxIN_DP0        , NULL,             NULL},
    {1,     LL_ADC_CHxIN_DP1        , NULL,             NULL},
    {2,     LL_ADC_CHxIN_DP2        , NULL,             NULL},
    {3,     LL_ADC_CHxIN_ADC3       , GPIOF,      GPIO_PIN_4},
    {4,     LL_ADC_CHxIN_ADC4       , GPIOF,      GPIO_PIN_3},
    {5,     LL_ADC_CHxIN_ADC5       , GPIOD,      GPIO_PIN_7},
    {6,     LL_ADC_CHxIN_ADC6       , GPIOD,      GPIO_PIN_6},
    {7,     LL_ADC_CHxIN_ADC7       , GPIOD,      GPIO_PIN_5},
    {8,     LL_ADC_CHxIN_ADC8       , GPIOD,      GPIO_PIN_4},
    {9,     LL_ADC_CHxIN_ADC9       , GPIOD,      GPIO_PIN_3},
    {10,    LL_ADC_CHxIN_ADC10      , GPIOD,      GPIO_PIN_2},
    {11,    LL_ADC_CHxIN_ADC11      , GPIOD,      GPIO_PIN_1},
    {12,    LL_ADC_CHxIN_ADC12      , GPIOD,      GPIO_PIN_0},
    {13,    LL_ADC_CHxIN_ADC13      , GPIOC,      GPIO_PIN_7},
    {14,    LL_ADC_CHxIN_ADC14      , GPIOC,      GPIO_PIN_6},
    {15,    LL_ADC_CHxIN_VCHARGE_IN , NULL,             NULL},
    {16,    LL_ADC_CHxIN_VCHARGE_OUT, NULL,             NULL},
    {17,    LL_ADC_CHxIN_12BITDAC   , NULL,             NULL},
    {18,    LL_ADC_CHxIN_TEMPERTURE , NULL,             NULL},
    {19,    LL_ADC_CHxIN_VREF       , NULL,             NULL},
    {20,    LL_ADC_CHxIN_VDDBKP     , NULL,             NULL},
    {21,    LL_ADC_CHxIN_6BITDAC    , NULL,             NULL},
};
/* clang-format on */

static const struct fm33_adc_map *get_adc_map_info(os_uint32_t channel)
{
    const struct fm33_adc_map *index = OS_NULL;

    if (channel < ITEM_NUM(adcs))
    {
        index = &adcs[channel];
    }

    return index;
}

static void hal_adc_pin_init(const struct fm33_adc_map *info)
{
    LL_GPIO_InitTypeDef GPIO_InitStruct;

    if ((info->index >= 3) && (info->index <= 14))
    {
        GPIO_InitStruct.Pin  = info->pin;
        GPIO_InitStruct.Alt  = LL_GPIO_ALT_0;
        GPIO_InitStruct.PuPd = LL_GPIO_PULL_NO;
        LL_GPIO_Init(info->port, &GPIO_InitStruct);
    }
}

static void hal_adc_init(ADC_TypeDef *ADCx)
{
    LL_ADC_InitTypeDef ADC_InitStruct = {0};

    ADC_InitStruct.ADCEN = LL_ADC_DISABLE;

    ADC_InitStruct.WorkClkSel = LL_ADC_WORKCLKSEL_16M;
    ADC_InitStruct.WorkClkDiv = LL_ADC_WORKCLKDIV_1;
    ADC_InitStruct.ClkSel     = LL_ADC_CLKSEL_WORKCLKDIV1;

    ADC_InitStruct.WorkMode     = LL_ADC_WORKMODE_SINGLE;
    ADC_InitStruct.OneShot      = LL_ADC_ONESHOT_ENABLE;
    ADC_InitStruct.LowPowerMode = LL_ADC_LOWPOWER_DISABLE;
    ADC_InitStruct.AutoSleep    = LL_ADC_AUTOSLEEP_DISABLE;

    ADC_InitStruct.VrefSel = LL_ADC_VREFH;
    ADC_InitStruct.VcomEn  = LL_ADC_VCOM_DISABLE;

    ADC_InitStruct.SleepWaitTime = 0x0008U;

    ADC_InitStruct.SampleTime     = 96U;
    ADC_InitStruct.ReadyTime      = 0x0U;
    ADC_InitStruct.ConvertTime    = 0x0AU;
    ADC_InitStruct.SampleHoldTime = 0x3U;

    LL_ADC_Init(ADCx, &ADC_InitStruct);
    LL_ADC_Enable(ADCx);

    LL_VREF12_Trim(0xab - 23);
    LL_VREF12_Enable();
}

static os_uint16_t __hal_get_adc(ADC_TypeDef *ADCx, os_uint8_t channel_name)
{
    os_uint32_t retval;

    LL_ADC_ClearAllStatus(ADCx);
    LL_ADC_SoftTrig(ADCx);

    while (1)
    {
        retval = LL_ADC_GetStatus(ADCx, (1 << channel_name) | LL_ADC_FLAG_DONEERR);
        if (LL_ADC_FLAG_DONEERR & retval)
        {
            LL_ADC_Disable(ADCx);
            os_task_msleep(5);
            LL_ADC_Enable(ADCx);
            continue;
        }
        if ((1 << channel_name) & retval)
            break;
    }

    return ((os_uint16_t)LL_ADC_GetChxResult(ADCx, channel_name));
}

static os_uint16_t hal_adc_read(ADC_TypeDef *ADCx, os_uint32_t channel_name)
{
    os_uint32_t adc_val;
    os_uint16_t adc_mv;

    if ((LL_ADC_GetChxIn(ADCx, FM33_USED_CHANNEL) != channel_name) ||
        (LL_ADC_CHx_DISABLE == LL_ADC_IsEnableChx(ADCx, FM33_USED_CHANNEL)))
    {
        LL_ADC_SetChxIn(ADCx, FM33_USED_CHANNEL, channel_name);
        LL_ADC_DisableAllChx(ADCx);
        LL_ADC_SetChxEn(ADCx, FM33_USED_CHANNEL, LL_ADC_CHx_ENABLE);

        __hal_get_adc(ADCx, FM33_USED_CHANNEL);
    }

    adc_val = __hal_get_adc(ADCx, FM33_USED_CHANNEL);
    adc_mv  = 3300 * adc_val / 4096;

    return adc_mv;
}

static os_err_t fm33_adc_channel_init(struct fm33_adc *dev, os_uint32_t channel_name)
{
    LL_ADC_ChannelInitTypeDef ADC_ChannelInitStruct;

    ADC_ChannelInitStruct.ChxEn          = LL_ADC_CHx_DISABLE;
    ADC_ChannelInitStruct.ChxDiff        = LL_ADC_CHxDIFF_DISABLE;
    ADC_ChannelInitStruct.ChxIn          = channel_name;
    ADC_ChannelInitStruct.ChxTrigSrc     = LL_ADC_CHxTRIGSRC_SOFTWARE;
    ADC_ChannelInitStruct.ChxTrigInvEn   = LL_ADC_CHxTRIGINV_DISABLE;
    ADC_ChannelInitStruct.ChxTrigEdge    = LL_ADC_CHxTRIGEDGE_RISING;
    ADC_ChannelInitStruct.ChxTrigDly     = 0x0U;
    ADC_ChannelInitStruct.ChxOffsetEn    = LL_ADC_CHxOFFSET_DISABLE;
    ADC_ChannelInitStruct.ChxAverageEn   = LL_ADC_CHxAVERAGE_DISABLE;
    ADC_ChannelInitStruct.ChxAverageNum  = LL_ADC_CHxAVERAGENUM_8;
    ADC_ChannelInitStruct.ChxCompareEn   = LL_ADC_CHxCOMPARE_DISABLE;
    ADC_ChannelInitStruct.ChxCompareMode = LL_ADC_CHxCOMPAREMODE_LARGER;

    LL_ADC_ChannelInit(dev->info->inst, FM33_USED_CHANNEL, &ADC_ChannelInitStruct);
    LL_ADC_SetChxEn(dev->info->inst, FM33_USED_CHANNEL, LL_ADC_CHx_ENABLE);

    return OS_EOK;
}

static os_err_t fm33_adc_init(struct fm33_adc *dev, os_uint32_t channel)
{
    const struct fm33_adc_map *map_info;

    if (channel >= ITEM_NUM(adcs))
    {
        LOG_E(DBG_TAG, " invalid adc channel[%d/%d]", channel, ITEM_NUM(adcs));
        return OS_ERROR;
    }

    map_info = get_adc_map_info(channel);
    if (map_info == OS_NULL)
    {
        LOG_E(DBG_TAG, "invalid adc info.");
        return OS_ERROR;
    }

    hal_adc_init(dev->info->inst);

    hal_adc_pin_init(map_info);

    fm33_adc_channel_init(dev, map_info->name);

    return OS_EOK;
}

/**
 ***********************************************************************************************************************
 * @brief           fm33_adc_poll_convert_then_read: start adc convert in poll
 *
 * @details         channel and order config in htcubeMX,"channell" is mapping of rank configed in cube,
 *
 * @attention       Attention_description_Optional
 *
 ***********************************************************************************************************************
 */
static os_err_t fm33_adc_read(struct os_adc_device *dev, os_uint32_t channel, os_int32_t *buff)
{
    struct fm33_adc           *dev_adc;
    const struct fm33_adc_map *map_info;

    OS_ASSERT(dev != OS_NULL);
    dev_adc = os_container_of(dev, struct fm33_adc, adc);

    if (channel >= ITEM_NUM(adcs))
    {
        LOG_E(DBG_TAG, "adc channel not support![max channel = %d]", ITEM_NUM(adcs));
        return OS_ERROR;
    }

    if (dev_adc->status != OS_ADC_ENABLE)
    {
        LOG_W(DBG_TAG, "adc disabled! please enable adc first!");
        return OS_ERROR;
    }

    fm33_adc_init(dev_adc, channel);

    map_info = get_adc_map_info(channel);

    *buff = (os_int32_t)hal_adc_read(dev_adc->info->inst, map_info->name);

    return OS_EOK;
}

static os_err_t fm33_adc_enabled(struct os_adc_device *dev, os_bool_t enable)
{
    struct fm33_adc *dev_adc;

    dev_adc = os_container_of(dev, struct fm33_adc, adc);

    if (!enable)
        dev_adc->status = OS_ADC_DISABLE;
    else
        dev_adc->status = OS_ADC_ENABLE;

    return OS_EOK;
}

static os_err_t fm33_adc_control(struct os_adc_device *dev, int cmd, void *arg)
{
    return OS_EOK;
}

static const struct os_adc_ops fm33_adc_ops = {
    .adc_enabled = fm33_adc_enabled,
    .adc_control = fm33_adc_control,
    .adc_read    = fm33_adc_read,
};

static int fm33_adc_probe(const os_driver_info_t *drv, const os_device_info_t *dev)
{
    os_err_t result = 0;

    struct fm33_adc *fm_adc = OS_NULL;
    fm_adc                  = os_calloc(1, sizeof(struct fm33_adc));
    OS_ASSERT(fm_adc);

    fm_adc->info   = (struct fm33_adc_info *)dev->info;
    fm_adc->status = OS_ADC_DISABLE;

    struct os_adc_device *dev_adc = &fm_adc->adc;
    dev_adc->ops                  = &fm33_adc_ops;
    dev_adc->max_value            = 0;
    dev_adc->ref_low              = 0; /* ref 0 - 3.3v */
    dev_adc->ref_hight            = 3300;

    result = os_hw_adc_register(dev_adc, dev->name, NULL);
    if (result != OS_EOK)
    {
        LOG_E(DBG_TAG, "%s register fialed!\r\n", dev->name);
        return OS_ERROR;
    }

    return OS_EOK;
}

OS_DRIVER_INFO fm33_adc_driver = {
    .name  = "ADC_Type",
    .probe = fm33_adc_probe,
};

OS_DRIVER_DEFINE(fm33_adc_driver, DEVICE, OS_INIT_SUBLEVEL_HIGH);
