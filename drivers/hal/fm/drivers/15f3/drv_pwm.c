/**
 ***********************************************************************************************************************
 * Copyright (c) 2020, China Mobile Communications Group Co.,Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 *
 * @file        drv_pwm.c
 *
 * @brief       This file implements PWM driver for stm32.
 *
 * @revision
 * Date         Author          Notes
 * 2020-02-20   OneOS Team      First Version
 ***********************************************************************************************************************
 */

#include <board.h>
#include <drv_hwtimer.h>
#include <string.h>
#include <os_memory.h>
#include "drv_pwm.h"

#define DBG_TAG "drv.pwm"
#include <dlog.h>

#define PWM_CHANNEL_MIN ((os_uint16_t)ST0)
#define PWM_CHANNEL_MAX ((os_uint16_t)ST5)

/*PWM CHANNEL0 ~PA1/PC7*/
#define TIM0_PWM_PORT GPIOA
#define TIM0_PWM_PIN  GPIO_PIN_1
/*
#define TIM0_PWM_PORT   GPIOC
#define TIM0_PWM_PIN    GPIO_PIN_7
*/

/*PWM CHANNEL1 ~PA3/PF4*/
#define TIM1_PWM_PORT GPIOA
#define TIM1_PWM_PIN  GPIO_PIN_3
/*
#define TIM1_PWM_PORT   GPIOF
#define TIM1_PWM_PIN    GPIO_PIN_4
*/

/*PWM CHANNEL2 ~PD1/PE1*/
#define TIM2_PWM_PORT GPIOD
#define TIM2_PWM_PIN  GPIO_PIN_1
/*
#define TIM2_PWM_PORT   GPIOE
#define TIM2_PWM_PIN    GPIO_PIN_1
*/

/*PWM CHANNEL3 ~PD3/PE3*/
#define TIM3_PWM_PORT GPIOD
#define TIM3_PWM_PIN  GPIO_PIN_3
/*
#define TIM3_PWM_PORT   GPIOE
#define TIM3_PWM_PIN    GPIO_PIN_3
*/

/*PWM CHANNEL4 ~PD5/PE5*/
#define TIM4_PWM_PORT GPIOD
#define TIM4_PWM_PIN  GPIO_PIN_5
/*
#define TIM4_PWM_PORT   GPIOE
#define TIM4_PWM_PIN    GPIO_PIN_5
*/

/*PWM CHANNEL4 ~PD5/PE5*/
#define TIM4_PWM_PORT GPIOD
#define TIM4_PWM_PIN  GPIO_PIN_5
/*
#define TIM4_PWM_PORT   GPIOE
#define TIM4_PWM_PIN    GPIO_PIN_5
*/

/*PWM CHANNEL5 ~PC5*/
#define TIM5_PWM_PORT GPIOC
#define TIM5_PWM_PIN  GPIO_PIN_5

struct fm_pwm_pin_map
{
    os_uint16_t   channel;
    GPIO_TypeDef *port;
    os_uint32_t   pin;
};

static const struct fm_pwm_pin_map pwm_info[] = {
#ifdef BSP_USING_PWM0
    {
        .channel = 0,
        .port    = TIM0_PWM_PORT,
        .pin     = TIM0_PWM_PIN,
    },
#endif

#ifdef BSP_USING_PWM1
    {
        .channel = 1,
        .port    = TIM1_PWM_PORT,
        .pin     = TIM1_PWM_PIN,
    },
#endif

#ifdef BSP_USING_PWM2
    {
        .channel = 2,
        .port    = TIM2_PWM_PORT,
        .pin     = TIM2_PWM_PIN,
    },
#endif

#ifdef BSP_USING_PWM3
    {
        .channel = 3,
        .port    = TIM3_PWM_PORT,
        .pin     = TIM3_PWM_PIN,
    },
#endif

#ifdef BSP_USING_PWM4
    {
        .channel = 4,
        .port    = TIM4_PWM_PORT,
        .pin     = TIM4_PWM_PIN,
    },
#endif

#ifdef BSP_USING_PWM5
    {
        .channel = 5,
        .port    = TIM5_PWM_PORT,
        .pin     = TIM5_PWM_PIN,
    },
#endif
};

static os_err_t get_pwm_channel(os_uint16_t channel)
{
    if (channel > PWM_CHANNEL_MAX)
    {
        return OS_ENOSYS;
    }
    else
    {
        return OS_EOK;
    }
}

static os_err_t get_pwm_channel_map(os_uint16_t channel, const struct fm_pwm_pin_map **info)
{
    int i;

    for (i = 0; i < OS_ARRAY_SIZE(pwm_info); i++)
    {
        if (pwm_info[i].channel == channel)
        {
            *info = &pwm_info[i];
            return OS_EOK;
        }
    }

    return OS_ERROR;
}

static void hal_pwm_pin_init(os_uint16_t channel)
{
    const struct fm_pwm_pin_map *pin_info;
    LL_GPIO_InitTypeDef          GPIO_InitStruct;

    if (get_pwm_channel_map(channel, &pin_info) != OS_EOK)
        return;

    GPIO_InitStruct.Pin           = pin_info->pin;
    GPIO_InitStruct.Alt           = LL_GPIO_ALT_6;
    GPIO_InitStruct.Dir           = LL_GPIO_DIRECTION_OUT;
    GPIO_InitStruct.DriveStrength = LL_GPIO_DRIVES_STRONG;
    GPIO_InitStruct.Irq           = LL_GPIO_INTorDMA_DISABLE;
    GPIO_InitStruct.Lock          = LL_GPIO_LK_UNLOCK;
    GPIO_InitStruct.OType         = LL_GPIO_OUTPUT_NOOPENDRAIN;
    GPIO_InitStruct.PuPd          = LL_GPIO_PULL_NO;
    GPIO_InitStruct.Speed         = LL_GPIO_SLEWRATE_HIGH;
    GPIO_InitStruct.WECconfig     = LL_GPIO_WKUP_CLOSED;

    LL_GPIO_Init(pin_info->port, &GPIO_InitStruct);
}

static os_err_t fm33_pwm_enabled(struct os_pwm_device *dev, os_uint32_t channel, os_bool_t enable)
{
    STn_Type fm_timer_channel;

    struct fm33_pwm *fm_pwm;

    fm_pwm = os_container_of(dev, struct fm33_pwm, pwm);

    if (get_pwm_channel(channel) != OS_EOK)
    {
        LOG_E(DBG_TAG, "pwm channel %d is illegal!\r\n", channel);
        return OS_ENOSYS;
    }

    fm_timer_channel = fm_pwm->tim->info->instance;

    if (!enable)
    {
        LL_STIM_Disable((STn_Type)fm_timer_channel);
        LL_STIM_Reset((STn_Type)fm_timer_channel);
    }
    else
    {
        hal_pwm_pin_init(channel);
        LL_STIM_SetRunMode((STn_Type)fm_timer_channel, LL_TIM_MODE_PWM);
        LL_STIM_DisableOneshot((STn_Type)fm_timer_channel);
        LL_STIM_Enable((STn_Type)fm_timer_channel);
    }

    return OS_EOK;
}

static os_err_t fm33_pwm_set_period(struct os_pwm_device *dev, os_uint32_t channel, os_uint32_t nsec)
{
    LOG_D(DBG_TAG, "pwm period and pulse are set together");

    return OS_EOK;
}

static os_err_t fm33_pwm_set_pulse(struct os_pwm_device *dev, os_uint32_t channel, os_uint32_t buffer)
{
    os_uint32_t pwm_pulse = 0;
    os_uint64_t tim_tick  = 0;

    struct fm33_pwm *fm_pwm;

    fm_pwm = os_container_of(dev, struct fm33_pwm, pwm);

    if (get_pwm_channel(channel) != OS_EOK)
    {
        LOG_E(DBG_TAG, "pwm channel %d is illegal!\r\n", channel);
        return OS_ENOSYS;
    }

    if (buffer > dev->period)
    {
        LOG_E(DBG_TAG, "pwm pulse value over range!\r\n");
        return OS_ERROR;
    }
    else
    {
        pwm_pulse = ((os_uint64_t)buffer * fm_pwm->tim_mult >> fm_pwm->tim_shift);
    }

    if (pwm_pulse > dev->max_value)
    {
        LOG_E(DBG_TAG, "pwm pulse value over range!\r\n");
        return OS_EFULL;
    }

    tim_tick = (os_uint64_t)dev->period * fm_pwm->tim_mult >> fm_pwm->tim_shift;

    LL_STIM_SetMod0Value(fm_pwm->tim->info->instance, tim_tick - pwm_pulse);
    LL_STIM_SetMod1Value(fm_pwm->tim->info->instance, pwm_pulse);

    return OS_EOK;
}

static const struct os_pwm_ops fm33_pwm_ops = {
    .enabled    = fm33_pwm_enabled,
    .set_period = fm33_pwm_set_period,
    .set_pulse  = fm33_pwm_set_pulse,
    .control    = OS_NULL,
};

os_err_t fm33_pwm_register(const char *name, struct fm33_timer *tim)
{
    char *pwm_name = os_calloc(1, sizeof(name) + 14);

    strcpy(pwm_name, "pwm_");

    pwm_name = strcat(pwm_name, name);

    struct fm33_pwm *fm_pwm = os_calloc(1, sizeof(struct fm33_pwm));

    OS_ASSERT(fm_pwm);

    fm_pwm->tim = tim;

    fm_pwm->freq = fm_pwm->tim->freq;

    fm_pwm->pwm.max_value = 0xFFFFFFFF;

    calc_mult_shift(&fm_pwm->tim_mult,
                    &fm_pwm->tim_shift,
                    NSEC_PER_SEC,
                    fm_pwm->tim->freq,
                    fm_pwm->pwm.max_value / fm_pwm->tim->freq);

    fm_pwm->pwm.ops = &fm33_pwm_ops;

    os_device_pwm_register(&fm_pwm->pwm, pwm_name);
    return OS_EOK;
}
